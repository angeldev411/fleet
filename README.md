# fleet_web
Decisiv front-end web application framework.  Multiple applications can be built and run
from this code base, depending on the selected build target.

Currently-supported applications are:
* Fleet Web
* Provider Web

The applications are configured as "build targets", under `config/build-targets`.  There
is also a _default_ build target with somewhat "generic" settings used by the test suite
and linting.

## Getting started

### Prerequisites

* [node](https://nodejs.org/en/) - version 6 or later
  * can use [nvm](https://github.com/creationix/nvm) to manage node versions
* [yarn](https://yarnpkg.com/lang/en/docs/install/)

### Up and running

1. Clone this repo:
    ```
    git clone https://github.decisiv.net/Development/fleet_web.git
    ```
1. Install dependencies:
    ```
    yarn
    ```
1. The different build targets (Fleet Web, Provider Web, ...) are all configured via files
   under `config/build-targets`.  Within each build target, there are several files which 
   define environment-specific configuration settings, e.g. for _PREVIEW_, _TEST_, and 
   _LOCALDEV_. Development should probably happen with the _LOCALDEV_ version except in 
   special situations, in order to use the correct configuration keys and to properly disable 
   Bugsnag error reporting.
   Create a symlink from the appropriate source file for your target environment, e.g. 
   for the Fleet Web build target:
    ```
    ln -s config/build-targets/fleet-web/env.LOCALDEV config/build-targets/fleet-web/env
    ```
1. Run the development server for the desired application:
    ```
    yarn start:fleet
    ```
    ...or...
    ```
    yarn start:provider
    ```
1. Connect to http://localhost:8080

## Testing

The application test suite includes unit tests as well as linting.  The full suite of
tests and other checks can be run via...
```
yarn review
```
... which in turn runs the linting as well as the unit tests, and displays the results
of test coverage reporting.

## Git workflow

* Make modifications in a feature branch (typically branching off of `master`).  
  * Name your feature branch based on the Jira ticket, if available (e.g. `git checkout -b UI-123`)
  * Each feature branch should contain a limited scope of related changes.
* Push your feature branch to the remote GitHub repository when it is ready for
  merge.
* Author well-worded commit messages.
  * Commit messages should be present-tense and brief.
  * Commit messages must be prefixed with any ticket or issue identifiers.  
    * Use the Jira ticket ID if available - `[UI-123] Fix the broken foo widget`
    * If work is _not_ associated with a Jira ticket (hmm... usually a bad idea?), then
      use a meaningful prefix for the commit message. For example:
      * `[TEST]`
      * `[CONFIG]`
      * `[MISC]`
* Open a [pull request](https://github.decisiv.net/Development/fleet_web/pulls)
  targeting the `master` branch (or another base if appropriate).
  * Include a link back to the original Jira issue within the summary of the pull request.
  * Include a short narrative description of even "simple" work.
  * Include screenshots in the pull request description to illustrate all changes in
    the UI that are included in the PR.
  * _Note:_ all of these and more are included in our [PR template](./.github/PULL_REQUEST_TEMPLATE.md)
  * Make use of PR labels:
    * `work in progress` - the developer is planning on pushing additional changes
      and the PR is not ready to be reviewed
    * `help wanted`, `question` - can be used to indicate a work-in-progress PR
      where the developer needs some assistance.
    * `don't merge` - this tag will indicate that the PR may (or may not) be ready
      for review, but should not be merged yet for some reason.  Document the reason
      in the PR comments.
    * `ready for review` - indicates that a PR is considered to be complete by the
      developer and ready for active review
    * `needs 2nd review` - all PRs should have at least two reviewers.  If you are 
      the first reviewer, and you are satisfied with the code, you should then label
      the PR with `needs 2nd review`
    * `ready to merge` - the code has been reviewed and approved by at least two reviewers
      and is ready to be merged, but has not yet been merged.  If you are the second
      reviewer, and you are satisfied with the code, you should then label the PR
      with `ready to merge`.
  * Leave a succinct, clear commit history to differentiate logical chunks of work
    that went into the feature.
  * Additional commits on the PR in response to the review feedback should be pushed
    as separate commits to reduce churn and rework for the reviewer, and to maintain
    clarity through the evolution of the PR.
  * PRs will usually be squashed before merging, unless there is some reason not to.
    If the PR should _not_ be squashed, this should be noted in the PR description.

## Developer Notes on translations

* Keys should be name-spaced (case sensitive) to component.
* Keys should be descriptive of location within component, not necessarily text.
* Keys should be Camel Cased.
* Keys should be organized alphabetically.
