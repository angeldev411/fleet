import manageTranslations from 'react-intl-translations-manager';

manageTranslations({
  messagesDirectory: 'extracted-messages',
  translationsDirectory: 'src/translations/',
  languages: ['en'],
});
