#!/bin/bash

# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [ENVIRONMENT]

Setup the .env environment configuration file for a certain environment.

This script should be run from within the project root directory.

Valid values for ENVIRONMENT are:
  LOCALDEV
  PREVIEW
  TEST
  DEMO
  PRODUCTION
EOF
}

ENVIRONMENT=$1
ROOT=$PWD
COPY_COUNT=0

if [ $# -ne 1 ]; then
  show_help
  exit 1
fi

echo "Setting up environment configuration files..."

# Env files for build targets, introduced in UI-357:
if [ -d "$ROOT/config/build-targets" ]; then
  TARGETS=$ROOT/config/build-targets/*
  for TARGET in $TARGETS
  do
    ENV_FILE="$TARGET/env.$ENVIRONMENT"
    if [ -f $ENV_FILE ]; then
      echo "Copying $ENV_FILE to $TARGET/env"
      cp $ENV_FILE "$TARGET/env"
      ((COPY_COUNT++))
    fi
  done
fi

echo "Done. ${COPY_COUNT} file(s) copied."
