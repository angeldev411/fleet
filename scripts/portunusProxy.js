/*------------------------------------------------------------------------------------
 * This is a simple HTTP proxy to be used in development for amending any
 * requests to the Portunus SSO server with the necessary CORS decorations
 * required for cross-origin access from the browser.
 *
 * This script is obsolete now that https://decisiv.atlassian.net/browse/CS-216 has
 * been resolved. It is being kept here for potential future repurposing.
 *
 * Run this script via `yarn portunus` from the project root.
 *------------------------------------------------------------------------------------*/

import express from 'express';
import httpProxy from 'http-proxy';
import logger from 'morgan';
import cors from 'cors';

const PORTUNUS_URL = 'https://portunus.preview.decisivapps.com';

const app = express();
app.set('port', 3000);
app.use(logger('dev'));

app.use(cors());

const apiProxy = httpProxy.createProxyServer({
  changeOrigin: true, // to allow HTTP->HTTPS transition
});

app.options('*', cors());

app.all('*', (req, res) => {
  if (req.method === 'OPTIONS') {
    // CORS pre-flight
    res.send(null);
  } else {
    apiProxy.web(req, res, { target: PORTUNUS_URL });
  }
});

app.listen(app.get('port'), () => {
  console.log(`👂 Portunus Proxy listening on port ${app.get('port')}`);
  console.log(`👉 Forwarding Portunus API requests to ${PORTUNUS_URL}`);
});
