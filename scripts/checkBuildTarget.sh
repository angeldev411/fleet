#!/bin/bash

# Make sure BUILD_TARGET is set to a non-empty value:
if [ -z ${BUILD_TARGET+x} ]; then
  echo "Aborting: BUILD_TARGET is not set."
  exit 1
fi

# Make sure BUILD_TARGET points at a valid build-target configuration directory:
ROOT=$PWD
TARGET_DIR="$ROOT/config/build-targets/$BUILD_TARGET"
if ! [ -d $TARGET_DIR ]; then
  echo "Aborting: BUILD_TARGET '$BUILD_TARGET' is not a valid directory within $ROOT/config/build-targets/"
  exit 2
fi

echo "Using build target configuration from config/build-targets/$BUILD_TARGET"
