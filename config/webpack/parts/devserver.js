/**
 * Configuration for the webpack development server.
 */

import webpack from 'webpack';
import path from 'path';

// Full list of stats options for granular control:
// const stats = {
//   // Add asset Information
//   assets: true,
//
//   // Sort assets by a field
//   assetsSort: 'field',
//
//   // Add information about cached (not built) modules
//   cached: true,
//
//   // Add children information
//   children: true,
//
//   // Add chunk information (setting this to `false` allows for a less verbose output)
//   chunks: true,
//
//   // Add built modules information to chunk information
//   chunkModules: true,
//
//   // Add the origins of chunks and chunk merging info
//   chunkOrigins: true,
//
//   // Sort the chunks by a field
//   chunksSort: 'field',
//
//   // Context directory for request shortening
//   context: '../src/',
//
//   // `webpack --colors` equivalent
//   colors: true,
//
//   // Add errors
//   errors: true,
//
//   // Add details to errors (like resolving log)
//   errorDetails: true,
//
//   // Add the hash of the compilation
//   hash: true,
//
//   // Add built modules information
//   modules: true,
//
//   // Sort the modules by a field
//   modulesSort: 'field',
//
//   // Add public path information
//   publicPath: true,
//
//   // Add information about the reasons why modules are included
//   reasons: true,
//
//   // Add the source code of modules
//   source: true,
//
//   // Add timing information
//   timings: true,
//
//   // Add webpack version information
//   version: true,
//
//   // Add warnings
//   warnings: true,
// };

// We only need minimal statistics reporting in the dev server:
const stats = 'minimal';

function devServer({ host, port }) {
  return {
    devServer: {
      contentBase: path.resolve(process.cwd(), 'src'),

      // Enable history API fallback so HTML5 History API based
      // routing works. This is a good default that will come
      // in handy in more complicated setups.
      historyApiFallback: true,

      // Don't refresh if hot loading fails.
      // hotOnly: true,

      // Alternatively, if you want refresh behavior, set this:
      hot: true,

      // Regulate the statistics that are displayed to
      // manage the amount of output (see above).
      stats,

      host, // Defaults to `localhost`
      port, // Defaults to 8080

      // contentBase: path.resolve(process.cwd(), 'src'),
      // compress: false,
      // inline: true,
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
    ],
  };
}

export default devServer;
