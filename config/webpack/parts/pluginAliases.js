/**
 * Extract the `plugins` section of the build configuration (build.yml) and map
 * each plugin implementation path to a webpack resolver alias that can be used
 * by the application.
 *
 * @param buildConfig
 * @returns {*}
 */
function pluginAliases(buildConfig) {
  const plugins = buildConfig.plugins;
  if (!plugins) {
    return [];
  }

  return Object.keys(plugins).reduce((acc, key) => {
    acc[`plugins/${key}`] = `plugins/${key}/${plugins[key]}`;
    return acc;
  }, {});
}

export default pluginAliases;
