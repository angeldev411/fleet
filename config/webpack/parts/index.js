/**
 * Collects all the webpack config parts into a single object for convenience.
 */

// TODO: maybe `shared` doesn't belong under `parts`?
import shared from './shared';
import devServer from './devserver';
import indexHtml from './indexHtml';
import images from './images';

export default {
  shared,
  devServer,
  indexHtml,
  images,
  // TODO: add parts for linting, etc...
};
