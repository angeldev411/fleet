/**
 * Development webpack configuration.
 */

import webpack from 'webpack';
import merge from 'webpack-merge';
import parts from './parts';

module.exports = function development(config) {
  return merge([
    {
      entry: {
        app: [
          'react-hot-loader/patch',
        ],
      },
    },
    parts.shared(config),
    parts.images(config),
    {
      devtool: 'eval-source-map',
      plugins: [
        new webpack.optimize.CommonsChunkPlugin({
          name: 'vendor',
          minChunks: Infinity,
          filename: 'vendor-[hash].js',
        }),
      ],
    },
    parts.devServer({
      host: process.env.HOST,
      port: process.env.PORT,
      ...config,
    }),
  ]);
};
