import yaml from 'js-yaml';
import fs from 'fs';
import path from 'path';

/**
 * Loads a YAML file for a specific build target.
 * @param {string} configRoot - the root folder for the target configurations
 * @param {string} targetName - the build target being targeted
 * @param {string} fileName - the base name (without the .yml) of the YAML file to load
 * @returns {object} the configuration loaded from the YAML file
 */
function loadYaml(configRoot, targetName = 'default', fileName) {
  const rootPath = process.cwd();
  const buildTargetPath = path.join(rootPath, configRoot, targetName);

  const buildConfigPath = `${buildTargetPath}/${fileName}.yml`;
  const configFile = fs.readFileSync(buildConfigPath, 'utf8');
  const config = yaml.safeLoad(configFile);
  return {
    target: targetName,
    configDir: buildTargetPath,
    ...config,
  };
}

const CONFIG_ROOT = 'config/build-targets';

/**
 * Determine the current build target name from the current `BUILD_TARGET` environment
 * variable.
 * @returns {string}
 */
function buildTarget() {
  return process.env.BUILD_TARGET || 'default';
}

/**
 * Loads the build target configuration from `config/build-targets/{buildTarget}/build.yml`
 * @returns {object} the build configuration
 */
function loadBuildTarget() {
  const targetName = buildTarget();
  try {
    return loadYaml(CONFIG_ROOT, targetName, 'build');
  } catch (e) {
    console.error(`Failed to load build target configuration for '${targetName}':`, e);
    process.exit(2);
  }

  return {}; // fallback, but we'll never really get here
}

/**
 * Loads the runtime configuration from config/build-targets/{buildTarget}/runtime.yml
 * @returns {object} the application runtime configuration
 */
function loadRuntimeConfig() {
  const targetName = buildTarget();
  try {
    return loadYaml(CONFIG_ROOT, targetName, 'runtime');
  } catch (e) {
    console.error(`Failed to load runtime configuration for '${targetName}':`, e);
    process.exit(2);
  }

  return {}; // fallback, but we'll never really get here
}

module.exports = {
  loadRuntimeConfig,
  loadBuildTarget,
};
