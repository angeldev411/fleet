/**
 * Production webpack configuration.
 */

import merge from 'webpack-merge';
import webpack from 'webpack';
import path from 'path';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

import parts from './parts';

module.exports = function production(config) {
  return merge([
    parts.shared(config),
    parts.images({ limit: 15000, name: '[hash:8].[ext]' }),
    {
      devtool: 'cheap-module-source-map',
      output: {
        filename: '[name]-[chunkhash].js',
      },
      plugins: [
        new webpack.DefinePlugin({
          'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV),
          },
        }),

        // ----------------------------------------------------------------------------------------
        // This production webpack config is modeled on the tips found here:
        // https://medium.com/webpack/predictable-long-term-caching-with-webpack-d3eee1d3fa31

        new webpack.NamedModulesPlugin(),

        new webpack.NamedChunksPlugin((chunk) => {
          if (chunk.name) {
            return chunk.name;
          }
          return chunk.modules.map(m => path.relative(m.context, m.request)).join('_');
        }),
        new webpack.optimize.CommonsChunkPlugin({
          name: 'vendor',
          minChunks: Infinity,
        }),
        new webpack.optimize.CommonsChunkPlugin({
          name: 'runtime',
        }),
        // new NameAllModulesPlugin(), -- if we ever need to include "external" modules like jQuery

        // ----------------------------------------------------------------------------------------

        // https://github.com/th0r/webpack-bundle-analyzer
        new BundleAnalyzerPlugin({
          analyzerMode: 'static',
          reportFilename: '../bundle-analyzer/report.html', // relative to `/dist`
          openAnalyzer: false,
          generateStatsFile: true,
          statsFilename: '../bundle-analyzer/stats.json',
        }),

        new webpack.LoaderOptionsPlugin({
          minimize: true,
          debug: false,
        }),

        new webpack.optimize.UglifyJsPlugin({
          beautify: false,
          mangle: {
            screw_ie8: true,
            keep_fnames: false,
          },
          compress: {
            screw_ie8: true,
          },
          comments: false,
        }),
      ],
    },
  ]);
};
