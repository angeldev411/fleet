/**
 * Top-level entry point for webpack configuration.  This
 * simply loads the environment-specific configuration file
 * from this directory, based on the current environment name.
 */

// So we can dynamically require the correct environment config:
/* eslint-disable global-require */

// This allows us to use ES6 in the webpack config files:
require('babel-register');

const utils = require('./utils');

module.exports = function webpackConfig(env = {}) {
  const config = utils.loadBuildTarget();

  if (env.production) {
    console.log('Using WebPack PRODUCTION configuration');
    return require('./production')(config);
  }

  // Default to loading the dev environment:
  return require('./development')(config);
};
