/**
 * Handles required setup before running AVA tests.
 */
import { jsdom } from 'jsdom';
import MemoryStorage from 'memorystorage';
import mockRequire from 'mock-require';

import testRuntimeConfiguration from './testRuntimeConfig.json';
import testAuthPlugin from '../../src/plugins/Authentication/Default';

/**
 * Setup application globals.
 */

global.document = jsdom('<body></body>');
global.window = document.defaultView;
global.navigator = window.navigator;

global.localStorage = new MemoryStorage('LOCALSTORAGE-MOCK');
global.memoryDB = new MemoryStorage('MEMORYDB-MOCK');

/**
 * The tests do not get access to the environment variables setup via Dotenv,
 * so define any globally-required environment variables here.
 */
process.env.API_BASE_URL = 'http://localhost:8080';
process.env.GOOGLE_MAPS_API_KEY = 'dummyApiKey';

/**
 * AVA is unable to process these file types and will raise errors if they are not null.
 * These file types should have no bearing on our tests anyways.
 */

function noop() {
  return null;
}

require.extensions['.css'] = noop;
require.extensions['.scss'] = noop;
require.extensions['.md'] = noop;
require.extensions['.png'] = noop;
require.extensions['.svg'] = noop;
require.extensions['.jpg'] = noop;
require.extensions['.jpeg'] = noop;
require.extensions['.gif'] = noop;

/*
 * The runtime config is a "virtual module" provided by the Webpack VirtualModulePlugin. As
 * such, it is not available when running in test mode, outside of the Webpack context. This
 * mocks out the minimal contents of a runtime configuration needed for testing.
 */
mockRequire('runtimeConfig.json', testRuntimeConfiguration);

/*
 * Similarly, the authentication plugin is configured via the Webpack resolver, based
 * on the build configuration - so, also not available during testing.  We'll use the
 * stubbed-out Default implementation for test purposes.
 */
mockRequire('plugins/Authentication', testAuthPlugin);
