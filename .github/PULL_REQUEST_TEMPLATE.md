## JIRA TICKET LINK
_[link to the JIRA ticket](https://decisiv.atlassian.net/browse/add-your-ticket-id-here)_

## OVERVIEW
_give a brief description of what this PR does_

## WHERE SHOULD THE REVIEWER START?
_e.g. `/src/components/SomeComponent.js`_

## HOW CAN THIS BE MANUALLY TESTED?
_list steps to test this locally_

## ANY NEW REUSABLE COMPONENTS || ELEMENTS ADDED?
_list any new reusable components or elements_

## ANY NEW DEPENDENCIES ADDED?
_list any new dependencies added_

* [ ] Does it work in IE >= 10?
* [ ]  _Does it work in other supported browsers?_

## SCREENSHOTS (if applicable)
🚨 _If this PR involves any changes to the rendered UI, include one or more screenshot illustrating the modifications. Additionally, if there are any changes to build scripts, the Webpack config, or third party dependencies, include screenshot(s) from IE10 (via Browserstack)._

## CHECKLIST
_Be sure all items are_ ✅ _before adding the 'Ready for Review' label_
* [ ] Run `$ yarn translate`
* [ ] Run `$ yarn review`
* [ ] Verify ESLint passes
* [ ] Verify Style Lint passes
* [ ] Verify all tests pass
* [ ] Verify code coverage is above required minimums
* [ ] Verify this branch is rebased with the latest master

## GIF
_Share a fun GIF to say thanks to your reviewer:_
https://giphy.com

![](https://media.giphy.com/media/ey7aT0Fm64ecw/giphy.gif)
