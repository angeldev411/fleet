import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = `
  flex-grow: 1;
  padding: ${px2rem(8)};
`;

export default buildStyledComponent(
  'PageContentPanel',
  styled.section,
  styles,
);
