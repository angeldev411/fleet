import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = `
  padding: 0 0.5rem;
`;

export default buildStyledComponent(
  'PageHeadingPanel',
  styled.section,
  styles,
);
