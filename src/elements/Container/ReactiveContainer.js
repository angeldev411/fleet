import { makeReactiveContainer } from 'reactive-container';

import Container from './Container';

export default makeReactiveContainer()(Container);
