import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  fullWidth: () => ({
    styles: `
      width: 100%;
    `,
  }),
  halfWidth: () => ({
    styles: `
      width: 50%;
    `,
  }),
  noPad: () => ({
    styles: `
      padding: 0;
    `,
  }),
};

/* istanbul ignore next */
const styles = () => `
  box-sizing: border-box;
  display: inline-block;
  padding: ${px2rem(8)};
  position: relative;
`;

export default buildStyledComponent(
  'Container',
  styled.div,
  styles,
  { modifierConfig, responsive: true },
);
