import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  approved: ({ theme }) => ({ styles: `color: ${theme.colors.status.success};` }),
  bold: () => ({ styles: 'font-weight: 500;' }),
  capitalize: () => ({ styles: 'text-transform: capitalize;' }),
  declined: ({ theme }) => ({ styles: `color: ${theme.colors.status.danger};` }),
  green: ({ theme }) => ({ styles: `color: ${theme.colors.status.success};` }),
  grey: ({ theme }) => ({ styles: `color: ${theme.colors.status.default};` }),
  partiallyApproved: ({ theme }) => ({ styles: `color: ${theme.colors.status.warning};` }),
  pending: ({ theme }) => ({ styles: `color: ${theme.colors.status.warning};` }),
  red: ({ theme }) => ({ styles: `color: ${theme.colors.status.danger};` }),
  requested: ({ theme }) => ({ styles: `color: ${theme.colors.status.warning};` }),
  tall: () => ({ styles: `display: block; height: ${px2rem(24)};` }),
  uppercase: () => ({ styles: 'text-transform: uppercase;' }),
  yellow: ({ theme }) => ({ styles: `color: ${theme.colors.status.warning};` }),
};

/* istanbul ignore next */
const styles = `
  display: inline-block;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
      default: PropTypes.string.isRequired,
      success: PropTypes.string.isRequired,
      warning: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'StatusSpan',
  styled.span,
  styles,
  { modifierConfig, themePropTypes },
);
