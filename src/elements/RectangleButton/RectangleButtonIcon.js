import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  padLeft: () => ({ styles: `padding-left: ${px2rem(8)};` }),
  padRight: () => ({ styles: `padding-right: ${px2rem(8)};` }),
};


/* istanbul ignore next */
const styles = () => `
  display: inline-block;
  .fa {
    line-height: 0;
  }
`;

export default buildStyledComponent(
  'RectangleButtonIcon',
  styled.div,
  styles,
  { modifierConfig },
);
