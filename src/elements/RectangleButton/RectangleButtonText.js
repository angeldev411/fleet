import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  small: () => ({
    styles: `
      font-weight: 300;
      font-size: ${px2rem(12)};
    `,
  }),
  padLeft: () => ({ styles: `padding-left: ${px2rem(8)};` }),
  padRight: () => ({ styles: `padding-right: ${px2rem(8)};` }),
  uppercase: () => ({ styles: 'text-transform: uppercase;' }),
};

/* istanbul ignore next */
const styles = () => `
  font-size: ${px2rem(14)};
  line-height: initial;
`;

export default buildStyledComponent(
  'RectangleButtonText',
  styled.span,
  styles,
  { modifierConfig },
);
