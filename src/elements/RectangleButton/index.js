import { noop } from 'lodash';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  blue: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.brand.primary};
      border: none;
      color: ${theme.colors.base.chrome100};
    `,
  }),

  disabled: () => ({
    styles: `
      cursor: default;
      opacity: 0.5;
      pointer-events: none;
      &:hover {
        box-shadow: inherit !important;
        text-decoration: inherit !important;
      }
    `,
  }),

  fullWidth: () => ({
    styles: `
      width: 100%;
    `,
  }),

  green: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.status.success};
      border: 1px solid ${theme.colors.status.success};
      color: ${theme.colors.base.chrome100};
    `,
  }),

  hoverBrandBright: ({ theme }) => ({
    styles: `
      &:hover {
        background-color: ${theme.colors.brand.primary};
        border-color: transparent;
        color: ${theme.colors.base.chrome000};
      }
    `,
  }),

  hoverSuccess: ({ theme }) => ({
    styles: `
      &:hover {
        background-color: ${theme.colors.status.success};
        border-color: transparent;
        color: ${theme.colors.base.chrome000};
      }
    `,
  }),

  hoverShadow: ({ theme }) => ({
    styles: `
      &:hover {
        box-shadow: 0 1px 2px 0 ${theme.colors.base.shadow};
      }
    `,
  }),

  hoverUnderline: () => ({
    styles: `
      &:hover {
        text-decoration: underline;
      }
    `,
  }),

  large: () => ({
    styles: `
      line-height: ${px2rem(60)};
      padding: 0 ${px2rem(24)};
    `,
  }),

  medium: () => ({
    styles: `
      line-height: ${px2rem(48)};
      padding: 0 ${px2rem(18)};
    `,
  }),

  normalWidth: () => ({
    styles: `
      width: ${px2rem(127)};
    `,
  }),

  offWhite: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.base.chrome100};
      border: 1px solid ${theme.colors.base.chrome200};
      color: ${theme.colors.base.textLight};
      &:disabled {
        background-color: ${theme.colors.base.chrome100} !important;
        border: 1px solid ${theme.colors.base.chrome200} !important;
        color: ${theme.colors.base.textLight} !important;
      }
    `,
  }),

  padLeft: () => ({
    styles: `
      margin-left: ${px2rem(10)};
    `,
  }),

  pressed: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.brand.primary};
      border-color: transparent;
      border: 1px solid ${theme.colors.base.chrome200};
      box-shadow: 0 1px 2px 0 ${theme.colors.base.shadow};
      color: ${theme.colors.base.chrome000};
      text-decoration: underline;
    `,
  }),

  pureWhite: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.base.chrome100};
      border: 1px solid ${theme.colors.base.chrome200};
      color: ${theme.colors.base.textLight};
    `,
  }),

  small: () => ({
    styles: `
      line-height: ${px2rem(38)};
      padding: 0 ${px2rem(12)};
    `,
  }),

  white: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.base.chrome200};
      border: none;
      color: ${theme.colors.base.textLight};
    `,
  }),

  wide: () => ({
    styles: `
      padding-left: ${px2rem(24)};
      padding-right: ${px2rem(24)};
    `,
  }),

  xtraWide: () => ({
    styles: `
      padding-left: ${px2rem(48)};
      padding-right: ${px2rem(48)};
    `,
  }),

};

/* istanbul ignore next */
const styles = ({ theme }) => `
  background-color: ${theme.colors.base.chrome200};
  border-radius: 2px;
  border: none;
  color: ${theme.colors.base.textLight};
  cursor: pointer;
  outline: none;
  transition: background-color 0.2s,
              border-color 0.2s,
              box-shadow 0.2s,
              color 0.2s,
              opacity 0.2s;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome000: PropTypes.string.isRequired,
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      shadow: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
    brand: PropTypes.shape({
      primary: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      success: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  onClick: PropTypes.func.isRequired,
};

const defaultProps = {
  onClick: noop,
};


export default buildStyledComponent(
  'RectangleButton',
  styled.button,
  styles,
  { defaultProps, modifierConfig, propTypes, themePropTypes },
);
export RectangleButtonIcon from './RectangleButtonIcon';
export RectangleButtonText from './RectangleButtonText';
