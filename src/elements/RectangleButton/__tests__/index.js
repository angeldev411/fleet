import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import RectangleButton from '../index';

const onClickHandler = noop;

const defaultProps = {
  onClick: onClickHandler,
};

function shallowRender(props = defaultProps) {
  return shallow(<RectangleButton {...props} />);
}

test('Passes the prop onClick down', () => {
  const component = shallowRender({ onClick: onClickHandler });
  expect(component.find('button')).toHaveProp('onClick', onClickHandler);
});
