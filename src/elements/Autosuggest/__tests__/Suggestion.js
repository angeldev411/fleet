import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import Suggestion from '../Suggestion';
import messages from '../messages';

const suggestion = {
  id: '',
  type: 'case',
};

const defaultProps = {
  suggestion,
};

function shallowRender(props = defaultProps) {
  return shallow(<Suggestion {...props} />);
}

test('renders cases message if suggestion type = case', () => {
  const component = shallowRender();
  expect(component).toContain('.typeahead-type');
  const type = component.find('.typeahead-type');
  expect(type).toContain('FormattedMessage');
  expect(type.find('FormattedMessage')).toHaveProps({ ...messages.cases });
});

test('renders assets message if suggestion type = asset', () => {
  const testProps = { suggestion: { id: '', type: 'asset' } };
  const component = shallowRender(testProps);
  expect(component).toContain('.typeahead-type');
  const type = component.find('.typeahead-type');
  expect(type).toContain('FormattedMessage');
  expect(type.find('FormattedMessage')).toHaveProps({ ...messages.assets });
});
