import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import Autosuggest from '../index';

const autosuggestRef = noop;
const onSuggestionsFetchRequested = noop;
const onSuggestionsClearRequested = noop;
const getSuggestionValue = noop;
const renderSuggestion = noop;
const suggestions = [];
const inputProps = {
  value: 1,
  onChange: noop,
};

const defaultProps = {
  autosuggestRef,
  onSuggestionsFetchRequested,
  onSuggestionsClearRequested,
  getSuggestionValue,
  renderSuggestion,
  suggestions,
  inputProps,
};

function shallowRender(props = defaultProps) {
  return shallow(<Autosuggest {...props} />);
}

test('renders a AutosuggestWrapper', () => {
  const component = shallowRender();
  expect(component).toBeA('AutosuggestWrapper');
});
