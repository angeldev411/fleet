import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import messages from './messages';

function Suggestion({ suggestion }) {
  return (
    <div>
      <span className="typeahead-value">{suggestion.id}</span>
      <span className="typeahead-in"><FormattedMessage {...messages.in} /></span>
      <span className="typeahead-type">
        {suggestion.type === 'case' ?
          <FormattedMessage {...messages.cases} /> :
          <FormattedMessage {...messages.assets} />
        }
      </span>
    </div>
  );
}

Suggestion.propTypes = {
  suggestion: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
};
export default Suggestion;
