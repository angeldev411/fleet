import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  focused: () => ({
    styles: `
      height: 100%;
      transition-delay: 0s;
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  flex: 1;
  background: ${props.theme.colors.base.chrome200};
  line-height: ${px2rem(34)};
  margin-right: ${px2rem(-17)};
  overflow: visible;
  width: 0;
  height: 0;
  transition: height 0s;
  transition-delay: 0.2s;

  .react-autosuggest__container {
    font-size: ${props.theme.dimensions.fontSizeMedium};
    line-height: ${px2rem(18)};
    font-weight: 500;
    color: ${props.theme.colors.base.text};
    cursor: pointer;
    height: 100%;
    padding-left: ${px2rem(8)};
    padding-right: ${px2rem(17)};
  }

  .react-autosuggest__input {
    font-size: ${props.theme.dimensions.fontSizeMedium};
    line-height: ${px2rem(18)};
    font-weight: 500;
    outline: none;
    border: none;
    width: 100%;
    height: 100%;
    padding: 0;
    background-color: ${props.theme.colors.base.chrome200};

    &::-webkit-input-placeholder {
      font-style: italic;
      font-weight: 300;
      color: ${props.theme.colors.base.chrome500};
    }
    &:-moz-placeholder {
      font-style: italic;
      font-weight: 300;
      color: ${props.theme.colors.base.chrome500};
    }
    &::-moz-placeholder {
      font-style: italic;
      font-weight: 300;
      color: ${props.theme.colors.base.chrome500};
    }
    &:-ms-input-placeholder {
      font-style: italic;
      font-weight: 300;
      color: ${props.theme.colors.base.chrome500};
    }
  }

  .react-autosuggest__suggestions-list {
    list-style: none;
    background: ${props.theme.colors.base.chrome100};
    box-shadow: 0 3px 7px 0 ${props.theme.colors.base.shadow};
    margin-left: ${px2rem(-8)};
    margin-right: ${px2rem(-34)};
    z-index: -1;
  }

  .react-autosuggest__suggestion {
    margin-left: ${px2rem(-40)};
    padding: ${px2rem(8)};

    .typeahead-in {
      margin: 0 ${px2rem(4)};
      font-weight: 400;
    }
    .typeahead-type {
      color: ${props.theme.colors.status.warning};
    }
  }

  .react-autosuggest__suggestion--highlighted {
    background-color: ${props.theme.colors.base.linkHover};
    color: ${props.theme.colors.base.chrome100};
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      chrome500: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
      shadow: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      warning: PropTypes.string.isRequired,
    }).isRequired,
  }),
  dimensions: PropTypes.shape({
    fontSizeMedium: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'AutosuggestWrapper',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
