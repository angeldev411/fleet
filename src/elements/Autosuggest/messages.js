import { defineMessages } from 'react-intl';

const messages = defineMessages({
  assets: {
    id: 'elements.Autosuggest.Suggestion.assets',
    defaultMessage: 'Assets',
  },
  cases: {
    id: 'elements.Autosuggest.Suggestion.cases',
    defaultMessage: 'Cases',
  },
  in: {
    id: 'elements.Autosuggest.Suggestion.in',
    defaultMessage: 'in',
  },
});

export default messages;
