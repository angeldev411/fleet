import React from 'react';
import PropTypes from 'prop-types';
import UnstyledAutosuggest from 'react-autosuggest';

import AutosuggestWrapper from './AutosuggestWrapper';

function Autosuggest({ autosuggestRef, ...props }) {
  return (
    <AutosuggestWrapper {...props} >
      <UnstyledAutosuggest {...props} ref={autosuggestRef} />
    </AutosuggestWrapper>
  );
}

Autosuggest.propTypes = {
  autosuggestRef: PropTypes.func.isRequired,
};

export default Autosuggest;
export Suggestion from './Suggestion';
