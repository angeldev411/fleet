import React from 'react';

import {
  test,
  expect,
  mount,
} from '__tests__/helpers/test-setup';

import ListTable from '../index';

const defaultProps = {
  modifiers: ['borderCollapse', 'fullWidth'],
  tdModifiers: ['tall'],
  thModifiers: ['chrome500', 'leftAlign', 'regular'],
};

function mountRender(props = defaultProps) {
  return mount(<ListTable {...props} />);
}

test('renders a ListTable having all modifiers without any problem.', () => {
  const component = mountRender();
  expect(component).toBeA('ListTable');
});
