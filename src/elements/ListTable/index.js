import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  applyStyleModifiers,
  styleModifierPropTypes,
} from 'styled-components-modifiers';
import {
  buildThemePropTypes,
  validateTheme,
} from 'styled-components-theme-validator';
import { px2rem } from 'decisiv-ui-utils';

import { withTestTheme } from 'utils/styles';

import Row from './Row';

const COMPONENT_NAME = 'ListTable';

const TABLE_MODIFIER_CONFIG = {
  borderCollapse: () => ({ styles: 'border-collapse: collapse;' }),
  fullWidth: () => ({ styles: 'width: 100%;' }),
};

const TD_MODIFIER_CONFIG = {
  tall: () => ({ styles: 'height: 60px;' }),
};

const TH_MODIFIER_CONFIG = {
  chrome500: ({ theme }) => ({ styles: `color: ${theme.colors.base.chrome500};` }),
  leftAlign: () => ({ styles: 'text-align: left;' }),
  regular: () => ({ styles: 'font-weight: 400;' }),
};

const THEME_PROPTYPES = buildThemePropTypes({
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      chrome500: PropTypes.string.isRequired,
      link: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
});

/* istanbul ignore next */
const ListTable = styled.table`
  ${validateTheme(COMPONENT_NAME, THEME_PROPTYPES)}
  ${applyStyleModifiers(TABLE_MODIFIER_CONFIG)}
  font-size: ${props => props.fontSize};

  th {
    ${applyStyleModifiers(TH_MODIFIER_CONFIG, 'thModifiers')}
    padding: ${px2rem(10)} ${px2rem(5)};
  }

  td {
    ${applyStyleModifiers(TD_MODIFIER_CONFIG, 'tdModifiers')}
    color: ${({ theme }) => theme.colors.base.textLight};
    min-width: ${px2rem(80)};
    padding: ${px2rem(10)} ${px2rem(5)};
    a {
      color: ${({ theme }) => theme.colors.base.link};
      text-decoration: none;
      &:hover {
        text-decoration: underline;
      }
    }
  }
`;

ListTable.propTypes = {
  fontSize: PropTypes.string,
  modifiers: styleModifierPropTypes(TABLE_MODIFIER_CONFIG),
  tdModifiers: styleModifierPropTypes(TD_MODIFIER_CONFIG),
  thModifiers: styleModifierPropTypes(TH_MODIFIER_CONFIG),
};

ListTable.defaultProps = {
  fontSize: px2rem(12),
};

ListTable.displayName = COMPONENT_NAME;
ListTable.Row = Row;

export default withTestTheme(ListTable);
