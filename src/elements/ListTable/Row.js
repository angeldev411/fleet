import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  hoverHighlight: ({ theme }) => ({
    styles: `
      transition: background-color 250ms ease;
      &:hover {
        background-color: ${theme.colors.base.chrome100};
      }
    `,
  }),
  lined: ({ theme }) => ({
    styles: `border-bottom: solid 1px ${theme.colors.base.chrome200};`,
  }),
  selectable: ({ theme }) => ({
    styles: `
      &:hover {
        background-color: ${theme.colors.base.selectableHover};
        box-shadow: 0 2px 1px 0 ${theme.colors.base.shadowLight};
      }
    `,
  }),
  selected: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.base.selectableActive};
      box-shadow: 0 2px 1px 0 ${theme.colors.base.shadow};
      &:hover {
        background-color: ${theme.colors.base.selectableActive};
      }
    `,
  }),
};

const styles = `
  cursor: pointer;
  vertical-align: top;
  &.list-row-loading:hover {
    background-color: inherit;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      selectableActive: PropTypes.string.isRequired,
      selectableHover: PropTypes.string.isRequired,
      shadow: PropTypes.string.isRequired,
      shadowLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'ListTable.Row',
  styled.tr,
  styles,
  { modifierConfig, themePropTypes },
);
