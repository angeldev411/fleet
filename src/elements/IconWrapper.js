import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.textLight};
  margin-right: ${px2rem(5)};

  .fa {
    font-size: ${px2rem(18)};
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  type: PropTypes.string,
};

export default buildStyledComponent(
  'IconWrapper',
  styled.span,
  styles,
  { propTypes, themePropTypes },
);
