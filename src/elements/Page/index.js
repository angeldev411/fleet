import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const styles = `
  flex-grow: 1;
`;

export default buildStyledComponent(
  'Page',
  styled.section,
  styles,
);
