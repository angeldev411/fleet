import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const leftPaddingInPixel = 8;

/* istanbul ignore next */
const modifierConfig = {
  leftNavExpanded: ({ theme }) => ({
    styles: `
      left: ${px2rem(theme.dimensions.leftNavWidthInPixel + leftPaddingInPixel)} !important;
    `,
  }),
  noTransition: () => ({ styles: 'transition: inherit;' }),
  visible: ({ theme }) => ({
    styles: `
      height: ${theme.dimensions.floatingHeaderHeight};
      overflow: visible;
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  background: ${props.theme.colors.base.background};
  box-shadow: 0 2px 3px 0 ${props.theme.colors.base.shadow};
  height: 0;
  left: ${px2rem(props.theme.dimensions.leftNavWidthCollapsedInPixel + leftPaddingInPixel)};
  margin: 0 ${px2rem(-8)};
  overflow: hidden;
  padding: 0 ${px2rem(8)};
  position: fixed;
  right: ${px2rem(8)};
  transition: ${props.theme.transitions.heightExpand};
  top: ${props.theme.dimensions.topNavHeight};
  z-index: 1;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
      shadow: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    leftNavWidthInPixel: PropTypes.number.isRequired,
    leftNavWidthCollapsedInPixel: PropTypes.number.isRequired,
    topNavHeight: PropTypes.string.isRequired,
  }).isRequired,
  transitions: PropTypes.shape({
    heightExpand: PropTypes.string.isRequired,
  }),
};

export default buildStyledComponent(
  'FloatingHeader',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
