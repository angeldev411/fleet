import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  smallText: () => ({
    styles: `
      font-size: ${px2rem(12)};
      line-height: ${px2rem(18)};
    `,
  }),
};

/* istanbul ignore next */
const styles = () => `
  max-height: ${px2rem(300)};
  overflow: auto;
  padding: ${px2rem(8)};
  width: ${px2rem(475)};
`;

export default buildStyledComponent(
  'ScrollingPopoverContentWrapper',
  styled.div,
  styles,
  { modifierConfig },
);
