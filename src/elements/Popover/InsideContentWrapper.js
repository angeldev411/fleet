import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import oldIEVersion from 'utils/oldIEVersion';

export function backgroundColor({ bgColor, theme }) {
  return `background-color: ${bgColor || theme.colors.base.background};`;
}

function borderStyles({ theme }) {
  if (oldIEVersion()) { return `border: solid 1px ${theme.colors.base.chrome300};`; }
  return 'border: none;';
}

const modifierConfig = {
  minSize: ({ theme }) => ({
    styles: `
      min-height: ${theme.dimensions.popOverMinHeight};
      min-width: ${theme.dimensions.popOverMinWidth};
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  ${borderStyles(props)}
  ${backgroundColor(props)}
  font-size: ${props.fontSize ? px2rem(props.fontSize) : 'inherit'};
  color: ${props.fontColor || 'inherit'};
  padding: ${px2rem(5)};
  position: relative;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    popOverMinHeight: PropTypes.string.isRequired,
    popOverMinWidth: PropTypes.string.isRequired,
  }).isRequired,
};

const propTypes = {
  bgColor: PropTypes.string,
  fontSize: PropTypes.string,
  fontColor: PropTypes.string,
};

const defaultProps = {
  bgColor: '',
  fontSize: '',
  fontColor: '',
};

export default buildStyledComponent(
  'InsideContentWrapper',
  styled.div,
  styles,
  { defaultProps, modifierConfig, propTypes, themePropTypes },
);
