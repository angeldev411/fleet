import { compact, noop } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import enhanceWithClickOutside from 'react-click-outside';
import { compose, setDisplayName } from 'recompose';

import Wrapper from './Wrapper';
import ContentWrapper from './ContentWrapper';
import TargetWrapper from './TargetWrapper';
import InsideContentWrapper from './InsideContentWrapper';
import Arrow from './Arrow';

/*
  Overview: This is a popover component
  Basic Use:
    import Popover, { PopoverContent, PopoverTarget } from 'elements/Popover';
    <Popover>
      <PopoverTarget>
        target where your mouse is clicked on or hovered on.
      </PopoverTarget>
      <PopoverContent>
        content to be shown on Popover
      </PopoverContent>
    </Popover>
  Props:
    showOnHover: show Popover on mouse hover (not click) if set to true.
    onShow: callback function called when the popover is shown.
    onHide: callback function called when the popover is hidden.
    noOffset: Arrow of popover goes to start or end of the border if set to true.
    position: can be one of ['left', 'top', 'right', 'bottom'].
              the relative position of popover to target
    positionOffset: if provided, defines the offset of the popover position (in px) from its
                    target element. if not provided, it defaults to 80%.
    arrowCenter: place the Arrow at the center of the border.
                 noOffset value is ignored if arrowCenter is set to true.
    bgColor: background Color of Popover
    border: if border is set to true it shows the border around Popover (solid 3px gray)
    arrowSize: size of the arrow of Popover. default is 18
    showAlways: show Popover always no matter you hover/click on it or not if set to true.
    noShadow: remove shadow if set to true.
    minSize: make minWidth and minHeight enabled. default: true
    delay: number of ms to delay showing of the popover
    fontSize: change font size in the popover
    fontColor: change the color of the font
*/

export class Popover extends Component {
  static propTypes = {
    children: PropTypes.node,
    arrowCenter: PropTypes.bool,
    arrowSize: PropTypes.number,
    arrowVisible: PropTypes.bool,
    bgColor: PropTypes.string,
    border: PropTypes.bool,
    clickOutside: PropTypes.func,
    delay: PropTypes.number,
    minSize: PropTypes.bool,
    noOffset: PropTypes.bool,
    noShadow: PropTypes.bool,
    onHide: PropTypes.func,
    onShow: PropTypes.func,
    position: PropTypes.oneOf(['top', 'bottom', 'left', 'right']),
    positionOffset: PropTypes.number,
    showAlways: PropTypes.bool,
    showOnHover: PropTypes.bool,
    fontSize: PropTypes.string,
    fontColor: PropTypes.string,
    inlineBlock: PropTypes.bool,
  };

  static defaultProps = {
    children: null,
    arrowCenter: false,
    arrowSize: 18,
    arrowVisible: true,
    bgColor: '',
    border: false,
    clickOutside: noop,
    delay: 100,
    minSize: true,
    noOffset: false,
    noShadow: false,
    onHide: noop,
    onShow: noop,
    position: 'bottom',
    positionOffset: 0,
    showAlways: false,
    showOnHover: false,
    target: null,
    fontSize: '',
    fontColor: '',
    inlineBlock: false,
  };

  state = {
    isPopoverShown: false,
    bound: {},
    position: 'bottom',
    arrowEnd: true,
  };

  getTarget = () => {
    const { children } = this.props;
    const targetComponent = React.Children
      .toArray(children)
      .filter(c => c.type.displayName === 'PopoverTarget')[0];
    return targetComponent && targetComponent.props.children;
  };

  getContent = () => {
    const { children } = this.props;
    const contentComponent = React.Children
      .toArray(children)
      .filter(c => c.type.displayName === 'PopoverContent')[0];
    const childComponent = contentComponent && contentComponent.props.children;
    return childComponent && React.cloneElement(childComponent, { hidePopover: this.hide });
  };

  handleClickOutside = () => {
    this.hide();
    this.props.clickOutside();
  };

  handleClickTarget = (e) => {
    if (this.state.isPopoverShown) {
      this.hide(e);
    } else {
      this.show(e);
    }
  }

  hide = (e) => {
    if (!this.state.isPopoverShown) return;
    this.setState({ isPopoverShown: false });
    this.props.onHide(e);
  };

  show = (e) => {
    if (this.state.isPopoverShown) return;
    const w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    const h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    const bound = e.currentTarget.getBoundingClientRect();
    const xLeft = bound.left + bound.right < w; // true if the object is on left side of viewport
    const yTop = bound.top + bound.bottom < h; // true if the object is on top side of viewport
    this.setState({
      isPopoverShown: true,
      position: yTop ? 'bottom' : 'top',
      arrowEnd: !xLeft,
    });
    this.props.onShow(e);
  };

  render() {
    const {
      children,
      arrowCenter,
      arrowSize,
      arrowVisible,
      bgColor,
      border,
      delay,
      minSize,
      noOffset,
      noShadow,
      positionOffset,
      showAlways,
      showOnHover,
      fontSize,
      fontColor,
      inlineBlock,
    } = this.props;
    const { isPopoverShown, bound, position, arrowEnd } = this.state;
    return (
      <Wrapper
        onMouseLeave={showOnHover ? this.hide : noop}
        onMouseEnter={showOnHover ? this.show : noop}
        modifiers={compact([inlineBlock && 'inlineBlock'])}
      >
        <TargetWrapper onClick={this.handleClickTarget}>
          {this.getTarget()}
        </TargetWrapper>
        <ContentWrapper
          active={!!((showAlways || isPopoverShown) && children)}
          arrowCenter={arrowCenter}
          arrowEnd={arrowEnd}
          arrowSize={arrowSize}
          border={border}
          bound={bound}
          delay={delay}
          noOffset={noOffset}
          noShadow={noShadow}
          position={this.props.position || position}
          positionOffset={positionOffset}
        >
          <Arrow
            position={this.props.position || position}
            arrowCenter={arrowCenter}
            arrowEnd={arrowEnd}
            noOffset={noOffset}
            bgColor={bgColor}
            border={border}
            arrowSize={arrowSize}
            arrowVisible={arrowVisible}
          />
          <InsideContentWrapper
            bgColor={bgColor}
            fontSize={fontSize}
            fontColor={fontColor}
            modifiers={compact([minSize && 'minSize'])}
          >
            {this.getContent()}
          </InsideContentWrapper>
        </ContentWrapper>
      </Wrapper>
    );
  }
}

// Popover.displayName = 'Popover';

export default compose(
  setDisplayName('Popover'),
  enhanceWithClickOutside,
)(Popover);
