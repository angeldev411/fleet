import styled from 'styled-components';

const TargetWrapper = styled.div`
  flex: 1;
  cursor: pointer;
`;

export default TargetWrapper;
