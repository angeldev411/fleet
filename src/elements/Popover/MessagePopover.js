import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  line-height: ${props.theme.dimensions.popOverMinHeight};
  text-transform: uppercase;
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    popOverMinHeight: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'MessagePopover',
  styled.div,
  styles,
  { themePropTypes },
);
