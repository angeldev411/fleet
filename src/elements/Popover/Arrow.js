import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import oldIEVersion from 'utils/oldIEVersion';

function styleValues({ position, arrowEnd }) {
  if (position === 'left' || position === 'right') {
    return {
      posAttrName: arrowEnd ? 'bottom' : 'top',
      posOtherAttrName: position === 'right' ? 'left' : 'right',
      offsetX: position === 'right' ? '50%' : '-50%',
      offsetY: arrowEnd ? '50%' : '-50%',
      rotation: position === 'right' ? -135 : 45,
    };
  }
  return {
    posAttrName: arrowEnd ? 'right' : 'left',
    posOtherAttrName: position === 'bottom' ? 'top' : 'bottom',
    offsetX: arrowEnd ? '50%' : '-50%',
    offsetY: position === 'bottom' ? '50%' : '-50%',
    rotation: position === 'bottom' ? -45 : 135,
  };
}

function arrowPosition({ noOffset, arrowSize, arrowCenter, theme }) {
  switch (true) {
    case noOffset:
      return px2rem(arrowSize);
    case arrowCenter:
      return '50%';
    default:
      return theme.dimensions.popOverArrowOffset;
  }
}

export function stylePosition(props) {
  const {
    posAttrName,
    posOtherAttrName,
    offsetX,
    offsetY,
    rotation,
  } = styleValues(props);

  const arrowPos = arrowPosition(props);

  return `
    ${posAttrName}: ${arrowPos};
    ${posOtherAttrName}: 0;
    margin-${posOtherAttrName}: 0;
    transform: translateX(${offsetX}) translateY(${offsetY}) rotate(${rotation}deg) scale(1.3);
  `;
}

export function borderStyle({ border, theme }) {
  if (border) {
    return `
      border-width: 2px 2px 0 0;
      border-color: ${theme.colors.base.chrome300};
      border-style: solid;
    `;
  }
  return '';
}

/* istanbul ignore next */
const styles = props => `
  ${borderStyle(props)}
  ${stylePosition(props)}
  background-color: ${props.bgColor || props.theme.colors.base.background};
  content: "";
  height: ${
  props.arrowSize
    ? px2rem(props.arrowSize)
    : props.theme.dimensions.popOverArrowSize
};
  position: absolute;
  transition: all 0.3s ease 0ms;
  width: ${
  props.arrowSize
    ? px2rem(props.arrowSize)
    : props.theme.dimensions.popOverArrowSize
};
  display: ${!oldIEVersion() && props.arrowVisible ? 'initial' : 'none'}
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    popOverArrowOffset: PropTypes.string.isRequired,
    popOverArrowSize: PropTypes.string.isRequired,
  }).isRequired,
};

const propTypes = {
  position: PropTypes.string,
  arrowEnd: PropTypes.bool,
  arrowCenter: PropTypes.bool,
  noOffset: PropTypes.bool,
  bgColor: PropTypes.string,
  border: PropTypes.bool,
  arrowSize: PropTypes.number,
  arrowVisible: PropTypes.bool,
};

const defaultProps = {
  position: 'top',
  arrowEnd: false,
  arrowCenter: false,
  noOffset: false,
  bgColor: '',
  border: false,
  arrowSize: 18,
  arrowVisible: true,
};

export default buildStyledComponent(
  'Arrow',
  styled.div,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
