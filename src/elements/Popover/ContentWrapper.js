import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

function getSizeOffset({
  arrowSize,
  noOffset,
  theme: {
    dimensions,
  },
}) {
  return {
    arrowOffset: noOffset ? dimensions.popOverArrowSize : dimensions.popOverArrowOffset,
    arrowSizeDefault: arrowSize ? px2rem(arrowSize) : dimensions.popOverArrowSize,
  };
}

function styleValues({ position, arrowEnd, arrowCenter, arrowOffset }) {
  if (position === 'left' || position === 'right') {
    return {
      offsetX: 0,
      offsetY: arrowCenter ? '50%' : `${arrowEnd ? '' : '-'}${arrowOffset}`,
      paddingAttrName: `padding-${position === 'right' ? 'left' : 'right'}`,
      posAttrName: position === 'right' ? 'left' : 'right',
      posOtherAttrName: arrowEnd ? 'bottom' : 'top',
    };
  }
  return {
    offsetX: arrowCenter ? '50%' : `${arrowEnd ? '' : '-'}${arrowOffset}`,
    offsetY: 0,
    paddingAttrName: `padding-${position === 'bottom' ? 'top' : 'bottom'}`,
    posAttrName: position === 'bottom' ? 'top' : 'bottom',
    posOtherAttrName: arrowEnd ? 'right' : 'left',
  };
}

export function stylePosition(props) {
  const {
    arrowOffset,
    arrowSizeDefault,
  } = getSizeOffset(props);

  const {
    offsetX,
    offsetY,
    paddingAttrName,
    posAttrName,
    posOtherAttrName,
  } = styleValues({
    ...props,
    arrowOffset,
    arrowSizeDefault,
  });

  const { positionOffset } = props;

  return `
    ${paddingAttrName}: ${arrowSizeDefault};
    ${posAttrName}: ${positionOffset ? px2rem(positionOffset) : '80%'};
    ${posOtherAttrName}: 50%;
    transform: translateX(${offsetX}) translateY(${offsetY});
  `;
}

export function styleActive({ active, delay }) {
  if (active) {
    return `
      transition: opacity 0.5s;
      opacity: 1;
      transition-delay: ${delay}ms;
    `;
  }
  return `
    opacity: 0;
    pointer-events: none;
  `;
}

export function styleShadow({ noShadow, theme }) {
  if (noShadow) {
    return '';
  }
  return `
    filter: drop-shadow(0 3px 7px ${theme.colors.base.shadow});
  `;
}

export function borderStyle({ border, theme }) {
  if (border) {
    return `
      border: 3px solid ${theme.colors.base.chrome300};
    `;
  }
  return '';
}

/* istanbul ignore next */
const styles = props => `
  ${borderStyle(props)}
  ${styleActive(props)}
  ${stylePosition(props)}
  ${styleShadow(props)}
  flex-flow: column nowrap;
  position: absolute;
  z-index: 10;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
      shadow: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  arrowCenter: PropTypes.bool,
  arrowEnd: PropTypes.bool,
  arrowSize: PropTypes.number,
  border: PropTypes.bool,
  delay: PropTypes.number.isRequired,
  noOffset: PropTypes.bool,
  noShadow: PropTypes.bool,
  position: PropTypes.string,
  positionOffset: PropTypes.number,
};

const defaultProps = {
  arrowCenter: false,
  arrowEnd: false,
  arrowSize: 18,
  border: false,
  noOffset: false,
  noShadow: false,
  position: 'top',
  positionOffset: 0,
};

export default buildStyledComponent(
  'ContentWrapper',
  styled.div,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
