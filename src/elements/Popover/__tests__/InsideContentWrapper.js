import { test, expect } from '__tests__/helpers/test-setup';

import { backgroundColor } from '../InsideContentWrapper';

// -------------------- backgroundColor --------------------

test('show bgColor as a backgroundColor when it is not empty or null', () => {
  const theme = {
    colors: {
      base: {
        background: 'white',
      },
    },
  };
  const bgColor = 'bgTestColor';
  expect(backgroundColor({ bgColor, theme })).toContain('background-color: bgTestColor');
  expect(backgroundColor({ bgColor: null, theme })).toContain('background-color: white');
});
