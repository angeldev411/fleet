import { px2rem, theme } from 'decisiv-ui-utils';

import { test, expect } from '__tests__/helpers/test-setup';

import { stylePosition, borderStyle } from '../Arrow';

// -------------------- stylePosition --------------------

test('Check if properly positioned', () => {
  // check if primary positioning attribute is applied right
  expect(stylePosition({ theme, position: 'left' })).toInclude('right: 0');
  expect(stylePosition({ theme, position: 'top' })).toInclude('bottom: 0');
  expect(stylePosition({ theme, position: 'right' })).toInclude('left: 0');
  expect(stylePosition({ theme, position: 'bottom' })).toInclude('top: 0');

  // check if secondary positioning attribute is applied right
  expect(
    stylePosition({ theme, position: 'left', arrowEnd: false }),
  ).toInclude(
    `top: ${theme.dimensions.popOverArrowOffset}`,
  );
  expect(
    stylePosition({ theme, position: 'left', arrowEnd: true }),
  ).toInclude(
    `bottom: ${theme.dimensions.popOverArrowOffset}`,
  );
  expect(
    stylePosition({ theme, position: 'right', arrowEnd: false }),
  ).toInclude(
    `top: ${theme.dimensions.popOverArrowOffset}`,
  );
  expect(
    stylePosition({ theme, position: 'right', arrowEnd: true }),
  ).toInclude(
    `bottom: ${theme.dimensions.popOverArrowOffset}`,
  );
  expect(
    stylePosition({ theme, position: 'top', arrowEnd: false }),
  ).toInclude(
    `left: ${theme.dimensions.popOverArrowOffset}`,
  );
  expect(
    stylePosition({ theme, position: 'top', arrowEnd: true }),
  ).toInclude(
    `right: ${theme.dimensions.popOverArrowOffset}`,
  );
  expect(
    stylePosition({ theme, position: 'bottom', arrowEnd: false }),
  ).toInclude(
    `left: ${theme.dimensions.popOverArrowOffset}`,
  );
  expect(
    stylePosition({ theme, position: 'bottom', arrowEnd: true }),
  ).toInclude(
    `right: ${theme.dimensions.popOverArrowOffset}`,
  );

  // check when arrowCenter is applied
  let style = { arrowCenter: true };
  expect(stylePosition({ theme, ...style, position: 'left' })).toInclude('top: 50%');
  expect(stylePosition({ theme, ...style, position: 'right' })).toInclude('top: 50%');
  expect(stylePosition({ theme, ...style, position: 'top' })).toInclude('left: 50%');
  expect(stylePosition({ theme, ...style, position: 'bottom' })).toInclude('left: 50%');

  // check when noOffset is set to true
  style = { noOffset: true, arrowSize: 12 };
  expect(
    stylePosition({ ...style, position: 'left', arrowEnd: false }),
  ).toInclude(
    `top: ${px2rem(12)}`,
  );
  expect(
    stylePosition({ ...style, position: 'left', arrowEnd: true }),
  ).toInclude(
    `bottom: ${px2rem(12)}`,
  );
  expect(
    stylePosition({ ...style, position: 'right', arrowEnd: false }),
  ).toInclude(
    `top: ${px2rem(12)}`,
  );
  expect(
    stylePosition({ ...style, position: 'right', arrowEnd: true }),
  ).toInclude(
    `bottom: ${px2rem(12)}`,
  );
  expect(
    stylePosition({ ...style, position: 'top', arrowEnd: false }),
  ).toInclude(
    `left: ${px2rem(12)}`,
  );
  expect(
    stylePosition({ ...style, position: 'top', arrowEnd: true }),
  ).toInclude(
    `right: ${px2rem(12)}`,
  );
  expect(
    stylePosition({ ...style, position: 'bottom', arrowEnd: false }),
  ).toInclude(
    `left: ${px2rem(12)}`,
  );
  expect(
    stylePosition({ ...style, position: 'bottom', arrowEnd: true }),
  ).toInclude(
    `right: ${px2rem(12)}`,
  );
});

// -------------------- borderStyle --------------------
test('borderStyle contains styles(border-width, border-color, border-style) when border is true', () => {
  const style = borderStyle({ theme, border: true });
  expect(style).toInclude('border-width: 2px 2px 0 0');
  expect(style).toInclude(`border-color: ${theme.colors.base.chrome300}`);
  expect(style).toInclude('border-style: solid;');
});
