import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import PopoverTarget from '../PopoverTarget';

function shallowRender() {
  return shallow(
    <PopoverTarget>
      <div>Test Children</div>
    </PopoverTarget>,
  );
}

test('renders null with itself only. it renders something only when it is under Popover.', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.render()).toEqual(null);
});
