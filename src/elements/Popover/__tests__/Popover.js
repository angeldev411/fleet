import React from 'react';
import { test, expect, shallow, createSpy, spyOn } from '__tests__/helpers/test-setup';

import { Popover } from '../Popover';
import { PopoverTarget, PopoverContent } from '../index';

const event = {
  currentTarget: {
    getBoundingClientRect: () => ({}),
  },
};

const defaultProps = {
  showOnHover: false,
  noOffset: false,
  position: 'top',
  arrowCenter: false,
  bgColor: '',
  border: false,
  arrowSize: 18,
  showAlways: false,
  noShadow: false,
  minSize: true,
  positionOffset: 10,
  inlineBlock: false,
};

function buildPopover({ target, content, ignored }, props = defaultProps) {
  return shallow(
    <Popover {...props}>
      <PopoverTarget>
        {target}
      </PopoverTarget>
      <PopoverContent>
        {content}
      </PopoverContent>
      <PopoverTarget>{ignored}</PopoverTarget>
      <PopoverContent>{ignored}</PopoverContent>
      <div>{ignored}</div>
    </Popover>,
  );
}

const target = <div id="targetWrapper">TargetString</div>;

test('Renders a Wrapper with right props', () => {
  const testProps = {
    ...defaultProps,
    inlineBlock: true,
  };

  const component = shallow(<Popover {...testProps} />);
  const wrapper = component.find('Wrapper');

  expect(wrapper.props().modifiers).toContain('inlineBlock');
});

test('Renders a ContentWrapper', () => {
  const component = shallow(
    <Popover>
      <PopoverTarget>target</PopoverTarget>
      <PopoverContent>content</PopoverContent>
    </Popover>,
  );
  const instance = component.instance();
  instance.show(event);
  expect(component.find('ContentWrapper').props().active).toEqual(true);
  instance.hide(event);
  expect(component.find('ContentWrapper').props().active).toEqual(false);
  window.innerWidth = 10;
  window.innerHeight = 10;
  event.currentTarget.getBoundingClientRect = () => ({ left: 0, right: 0, top: 0, bottom: 0 });
  instance.show(event);
  instance.handleClickOutside();
  expect(component.find('ContentWrapper').props().active).toEqual(false);
});

test('Run onShow function', () => {
  const onShow = createSpy();
  const component = shallow(<Popover onShow={onShow} />);
  const instance = component.instance();
  component.setState({ isPopoverShown: false });
  window.innerWidth = 0;
  window.innerHeight = 0;
  instance.show(event);
  expect(onShow).toHaveBeenCalled();
});

test('Not Run onShow function when the popover is alreay shown', () => {
  const onShow = createSpy();
  const component = shallow(<Popover onShow={onShow} />);
  const instance = component.instance();
  component.setState({ isPopoverShown: true });
  instance.show(event);
  expect(onShow).toNotHaveBeenCalled();
});

test('Run onHide function', () => {
  const onHide = createSpy();
  const component = shallow(<Popover onHide={onHide} />);
  const instance = component.instance();
  component.setState({ isPopoverShown: true });
  instance.hide(event);
  expect(onHide).toHaveBeenCalled();
});

test('Not Run onHide function when the popover is already hidden', () => {
  const onHide = createSpy();
  const component = shallow(<Popover onHide={onHide} />);
  const instance = component.instance();
  component.setState({ isPopoverShown: false });
  instance.hide(event);
  expect(onHide).toNotHaveBeenCalled();
});

test('Render children inside the first PopoverTarget', () => {
  expect(buildPopover({ target })).toContain('div#targetWrapper');
});

test('Render children inside the first PopoverContent', () => {
  const content = <span id="contentWrapper">Content</span>;
  expect(buildPopover({ content })).toContain('span#contentWrapper');
});

test('Does not render children inside second PopoverContent/PopoverTarget or another component.', () => {
  const ignored = <p id="ignoredWrapper">Ignored</p>;
  expect(buildPopover({ ignored })).toNotContain('p#ignoredWrapper');
});

test('Render TargetWrapper', () => {
  expect(buildPopover({ target })).toContain('TargetWrapper');
});

test('clicking the target hides the popover if it is shown', () => {
  const testTarget = <div>test target</div>;
  const component = buildPopover({ target: testTarget });
  const instance = component.instance();
  component.setState({ isPopoverShown: true });
  const hidePopoverSpy = spyOn(instance, 'hide');
  const targetWrapper = component.find('TargetWrapper');
  targetWrapper.simulate('click');
  expect(hidePopoverSpy).toHaveBeenCalled();
});

test('Render ContentWrapper with right props', () => {
  const popOver = buildPopover({ target });
  popOver.instance().setState({
    isPopoverShown: true,
    position: 'bottom',
    arrowEnd: true,
  });
  const contentWrapper = popOver.find('ContentWrapper');
  const insideContentWrapper = popOver.find('InsideContentWrapper');
  expect(contentWrapper).toHaveProps({
    position: 'top',
    arrowCenter: defaultProps.arrowCenter,
    arrowEnd: true,
    active: true,
    noOffset: defaultProps.noOffset,
    border: defaultProps.border,
    arrowSize: defaultProps.arrowSize,
    noShadow: defaultProps.noShadow,
    positionOffset: defaultProps.positionOffset,
  });
  expect(insideContentWrapper.props().modifiers).toContain('minSize');
});

test('Render ContentWrapper and InsideContentWrapper with right props', () => {
  const popOver = buildPopover({ target }, {
    ...defaultProps, showAlways: true, showOnHover: true, minSize: false,
  });
  popOver.instance().setState({
    isPopoverShown: true,
    position: 'bottom',
    arrowEnd: false,
  });
  const contentWrapper = popOver.find('ContentWrapper');
  const insideContentWrapper = popOver.find('InsideContentWrapper');
  expect(contentWrapper).toHaveProps({
    position: 'top',
    arrowCenter: defaultProps.arrowCenter,
    arrowEnd: false,
    active: true,
    noOffset: defaultProps.noOffset,
    border: defaultProps.border,
    arrowSize: defaultProps.arrowSize,
    noShadow: defaultProps.noShadow,
    positionOffset: defaultProps.positionOffset,
  });
  expect(insideContentWrapper.props().modifiers).toNotContain('minSize');
});
