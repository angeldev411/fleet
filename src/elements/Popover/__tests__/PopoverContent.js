import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import PopoverContent from '../PopoverContent';

function shallowRender() {
  return shallow(
    <PopoverContent>
      <div>Test Children</div>
    </PopoverContent>,
  );
}

test('renders null with itself only. it renders something only when it is under Popover.', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.render()).toEqual(null);
});
