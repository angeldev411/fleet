import { test, expect } from '__tests__/helpers/test-setup';
import { px2rem, theme } from 'decisiv-ui-utils';

import { styleActive, stylePosition, borderStyle, styleShadow } from '../ContentWrapper';

// -------------------- styleActive --------------------
test('shows component only when it is active.', () => {
  expect(styleActive({ active: true })).toInclude('opacity: 1');
});

test('hides component only when it is active.', () => {
  expect(styleActive({ active: false })).toInclude('opacity: 0');
  expect(styleActive({ active: false })).toInclude('pointer-events: none');
});

// -------------------- styleShadow --------------------
test('shows shadow when noShadow is false.', () => {
  expect(styleShadow({ theme, noShadow: false })).toInclude('filter: drop-shadow(0 3px 7px ');
});

test('hides shadow when noShadow is true.', () => {
  expect(styleShadow({ theme, noShadow: true })).toBe('');
});

// -------------------- stylePosition --------------------
test('sets the correct position offset when positionOffset is not provided', () => {
  // check if primary positioning attribute is applied right
  expect(stylePosition({ theme, position: 'left' })).toInclude('right: 80%');
  expect(stylePosition({ theme, position: 'top' })).toInclude('bottom: 80%');
  expect(stylePosition({ theme, position: 'right' })).toInclude('left: 80%');
  expect(stylePosition({ theme, position: 'bottom' })).toInclude('top: 80%');

  // check if secondary positioning attribute is applied right
  expect(stylePosition({ theme, position: 'left', arrowEnd: false })).toInclude('top: 50%');
  expect(stylePosition({ theme, position: 'left', arrowEnd: true })).toInclude('bottom: 50%');
  expect(stylePosition({ theme, position: 'right', arrowEnd: false })).toInclude('top: 50%');
  expect(stylePosition({ theme, position: 'right', arrowEnd: true })).toInclude('bottom: 50%');
  expect(stylePosition({ theme, position: 'top', arrowEnd: false })).toInclude('left: 50%');
  expect(stylePosition({ theme, position: 'top', arrowEnd: true })).toInclude('right: 50%');
  expect(stylePosition({ theme, position: 'bottom', arrowEnd: false })).toInclude('left: 50%');
  expect(stylePosition({ theme, position: 'bottom', arrowEnd: true })).toInclude('right: 50%');

  // check if arrowCenter works
  expect(stylePosition({ theme, position: 'left', arrowEnd: false, arrowCenter: true })).toInclude('top: 50%');
});

test('sets the correct position offset if provided', () => {
  const positionOffset = 10;
  const testProps = {
    theme,
    positionOffset,
  };
  expect(stylePosition({ ...testProps, position: 'left' })).toInclude(`right: ${px2rem(10)}`);
  expect(stylePosition({ ...testProps, position: 'right' })).toInclude(`left: ${px2rem(10)}`);
  expect(stylePosition({ ...testProps, position: 'top' })).toInclude(`bottom: ${px2rem(10)}`);
  expect(stylePosition({ ...testProps, position: 'bottom' })).toInclude(`top: ${px2rem(10)}`);
});

// -------------------- borderStyle --------------------
test('borderStyle contains "border" style when property "border" is true', () => {
  const style = borderStyle({ theme, border: true });
  expect(style).toInclude(`border: 3px solid ${theme.colors.base.chrome300}`);
});
