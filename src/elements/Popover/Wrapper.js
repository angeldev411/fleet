import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const modifierConfig = {
  inlineBlock: () => ({ styles: 'display: inline-block;' }),
};

const styles = `
  position: relative;
  box-sizing: border-box;
`;

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
  { modifierConfig },
);
