import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  applyStyleModifiers,
  styleModifierPropTypes,
} from 'styled-components-modifiers';
import {
  buildThemePropTypes,
  validateTheme,
} from 'styled-components-theme-validator';
import { px2rem } from 'decisiv-ui-utils';

import { withTestTheme } from 'utils/styles';

const COMPONENT_NAME = 'Table';

const TABLE_MODIFIER_CONFIG = {
  brand: ({ theme }) => ({
    styles: `
      color: ${theme.colors.brand.secondary},
      font-size: ${px2rem(20)},
    `,
  }),
  mediumGrey: ({ theme }) => ({
    styles: `color: ${theme.colors.base.chrome500};`,
  }),
  smallText: () => ({
    styles: `
      font-size: ${px2rem(12)};
      th {
        padding-left: ${px2rem(8)} ${px2rem(8)} 0;
      }
      td {
        padding: ${px2rem(8)} ${px2rem(16)} ${px2rem(8)} 0;
      }
    `,
  }),
  xLargeText: () => ({
    styles: `
      font-size: ${px2rem(24)};
      th {
        padding-right: ${px2rem(12)};
      }
      td {
        padding-right: ${px2rem(24)};
      }
    `,
  }),
};

const TR_MODIFIER_CONFIG = {
  flex: () => ({
    styles: `
      display: flex;
      flex-flow: row wrap;
    `,
  }),
  wide: () => ({
    styles: `
      th {
        padding: 0;
      }
      td {
        padding: 0 ${px2rem(50)} 0 ${px2rem(20)};
      }
    `,
  }),
};

const THEME_PROPTYPES = buildThemePropTypes({
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome500: PropTypes.string.isRequired,
      chrome700: PropTypes.string.isRequired,
    }).isRequired,
    brand: PropTypes.shape({
      secondary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
});

const Table = styled.table`
  ${validateTheme(COMPONENT_NAME, THEME_PROPTYPES)}

  border-spacing: 0;
  ${applyStyleModifiers(TABLE_MODIFIER_CONFIG)}

  tbody {
    color: ${({ theme }) => theme.colors.base.chrome700};

    tr {
      text-align: left;

      th {
        font-weight: 500;
        text-align: left;
      }

      td {
        font-weight: 300;
        vertical-align: text-top;
      }

      ${applyStyleModifiers(TR_MODIFIER_CONFIG, 'trModifiers')}
    }
  }
`;

Table.propTypes = {
  modifiers: styleModifierPropTypes(TABLE_MODIFIER_CONFIG),
  trModifiers: styleModifierPropTypes(TR_MODIFIER_CONFIG),
};

Table.displayName = COMPONENT_NAME;

export default withTestTheme(Table);
