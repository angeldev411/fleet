import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import IconWarning from '../index';

function buildElement(props = {}) {
  return shallow(<IconWarning {...props} />);
}

test('Renders an SVG', () => {
  expect(buildElement()).toBeA('svg');
});

test('Passes size to the svg width style', () => {
  const size = 88;
  const element = buildElement({ size });
  expect(element.find('svg').props().style.width).toEqual(size);
});

test('Passes size to the SVG height style', () => {
  const size = 42;
  const element = buildElement({ size });
  expect(element.find('svg').props().style.height).toEqual(size);
});

test('Passes color to the SVG fill style', () => {
  const color = 'burntsienna';
  const element = buildElement({ color });
  expect(element.find('svg').props().style.fill).toEqual(color);
});

test('Defaults to 18px height/width', () => {
  const element = buildElement();
  expect(element.find('svg').props().style.width).toEqual(18);
  expect(element.find('svg').props().style.height).toEqual(18);
});

test('Defaults to grey fill', () => {
  const element = buildElement();
  expect(element.find('svg').props().style.fill).toEqual('grey');
});
