import React from 'react';
import PropTypes from 'prop-types';

/**
 * Renders a triangle icon (SVG) with rounded corners.
 */
function IconWarning({ size, color }) {
  const styles = {
    fill: color,
    width: size, // CSS instead of the width attr to support non-pixel units
    height: size, // Prevents scaling issue in IE
  };

  const svgPath = 'M22.1 2.8C24.3-1 27.7-1 29.9 2.8L51.2 41C53.3 44.9 51.5 48 47 48L5 48C0.5 48-1.3 44.9 0.8 41L22.1 2.8Z';

  return (
    <svg viewBox="-1 -1 53 49" preserveAspectRatio="xMidYMid meet" style={styles}>
      <path d={svgPath} />
    </svg>
  );
}

IconWarning.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

IconWarning.defaultProps = {
  size: 18,
  color: 'grey',
};

export default IconWarning;
