import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  capitalize: () => ({ styles: 'text-transform: capitalize;' }),
  textLight: ({ theme }) => ({ styles: `color: ${theme.colors.base.textLight};` }),
  weightNormal: () => ({ styles: 'font-weight: normal;' }),
};

/* istanbul ignore next */
const styles = '';

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      textLight: PropTypes.string.isRequired,
    }),
  }).isRequired,
};

export default buildStyledComponent(
  'Span',
  styled.span,
  styles,
  { modifierConfig, themePropTypes },
);
