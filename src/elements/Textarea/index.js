import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  border-radius: 2px;
  border: 1px solid ${props.theme.colors.base.chrome300};
  color: ${props.theme.colors.base.text};
  flex: 1;
  font-size: ${px2rem(12)};
  height: ${px2rem(90)};
  line-height: ${px2rem(18)};
  padding: ${px2rem(6)} ${px2rem(9)};
  resize: none;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Textarea',
  styled.textarea,
  styles,
  { themePropTypes },
);
