import { omit } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { Link as UnstyledLink } from 'react-router-dom';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/**
 * LinkWithoutStyle removes the modifier and theme props from react-router-dom's `Link` component,
 * allowing us to use styled-components without prop errors in the console.
 */
function LinkWithoutStyleProps(props) {
  return <UnstyledLink {...omit(props, ['theme', 'modifiers'])} />;
}

/* istanbul ignore next */
const modifierConfig = {
  bold: () => ({ styles: 'font-weight: bold;' }),
  heavy: () => ({ styles: 'font-weight: 500;' }),
  hoverCaret: () => ({
    styles: `
      &:hover::after {
        content: "\\f0da";
        font-family: FontAwesome;
        display: inline-block;
        text-decoration: none;
        vertical-align: middle;
        line-height: 0;
        margin-left: 0.5rem;
        position: relative;
        bottom: 0.05rem;
      }
    `,
  }),
  padded: () => ({ styles: `padding: ${px2rem(6)};` }),
  small: () => ({ styles: `font-size: ${px2rem(14)};` }),
  textColor: ({ theme }) => ({
    styles: `
      color: ${theme.colors.base.text};
      &:hover {
        color: ${theme.colors.base.linkHover} !important;
        text-decoration: underline;
      }
      &:visited {
        color: ${theme.colors.base.text};
      }
    `,
  }),
  uppercase: () => ({ styles: 'text-transform: uppercase;' }),
};

/* istanbul ignore next */
const styles = ({ theme }) => `
  color: ${theme.colors.base.link};
  text-decoration: none;
  &:hover {
    color: ${theme.colors.base.linkHover};
    text-decoration: underline;
  }
  &:visited {
    color: ${theme.colors.base.linkVisited};
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      link: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
      linkVisited: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Link',
  styled(LinkWithoutStyleProps),
  styles,
  { modifierConfig, themePropTypes },
);
