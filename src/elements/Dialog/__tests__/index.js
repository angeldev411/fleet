import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import Dialog from '../index';

const defaultProps = {
  top: 10,
  right: 10,
  children: <div>TestChildren</div>,
};

function shallowRender(props = defaultProps) {
  return shallow(<Dialog {...props} />);
}

test('renders a DialogWrapper with the right props.', () => {
  const component = shallowRender();
  expect(component).toBeA('DialogWrapper');

  expect(component.find('DialogWrapper')).toHaveProps({ top: defaultProps.top, right: defaultProps.right });
});
