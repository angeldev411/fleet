import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './Wrapper';

function Dialog({ top, right, children }) {
  return (
    <Wrapper top={top} right={right}>
      { children }
    </Wrapper>
  );
}

Dialog.propTypes = {
  top: PropTypes.number,
  right: PropTypes.number,
  children: PropTypes.node.isRequired,
};

Dialog.defaultProps = {
  top: 0,
  right: 0,
};

export default Dialog;
