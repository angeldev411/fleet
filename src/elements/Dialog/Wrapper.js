import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const styles = props => `
  position: absolute;
  top: ${px2rem(props.top)};
  right: ${px2rem(props.right)};
  filter: drop-shadow(0 3px 7px rgba(0, 0, 0, 0.4));
  z-index: 10;
`;

const propTypes = {
  top: PropTypes.number.isRequired,
  right: PropTypes.number.isRequired,
};

export default buildStyledComponent(
  'DialogWrapper',
  styled.div,
  styles,
  { propTypes },
);
