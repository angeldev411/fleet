import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { BlueDot } from '../index';

const defaultProps = {
  theme: {
    colors: {
      brand: {
        primary: 'primary',
      },
    },
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<BlueDot {...props} />);
}

test('renders a svg', () => {
  expect(shallowRender()).toBeA('svg');
});
