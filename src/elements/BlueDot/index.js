import PropTypes from 'prop-types';
import React from 'react';
import { withTheme } from 'styled-components';

export function BlueDot({
  diameter,
  theme,
}) {
  return (
    <svg
      height={diameter}
      width={diameter}
      viewBox={`0 0 ${diameter} ${diameter}`}
    >
      <circle
        cx={diameter / 2}
        cy={diameter / 2}
        r={diameter / 2}
        fill={theme.colors.brand.primary}
      />
    </svg>
  );
}

BlueDot.propTypes = {
  diameter: PropTypes.number.isRequired,
  theme: PropTypes.shape({
    colors: PropTypes.shape({
      brand: PropTypes.shape({
        primary: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};

BlueDot.defaultProps = {
  diameter: 10,
};

export default withTheme(BlueDot);
