import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const modifierConfig = {
  dark: ({ theme }) => ({
    styles: `background-color: ${theme.colors.base.chrome300};`,
  }),
};

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.chrome200};
  height: 100%;
  width: 1px;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome200: PropTypes.string.isRequired,
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'VerticalDivider',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
