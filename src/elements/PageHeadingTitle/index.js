import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  large: () => ({
    styles: `
      font-size: ${px2rem(25)};
      padding-top: ${px2rem(16)};
    `,
  }),
  padLeft: () => ({
    styles: `padding-left: ${px2rem(8)};`,
  }),
};

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.text};
  font-size: ${px2rem(15)};
  font-weight: 600;
  margin: 0;
  text-transform: uppercase;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'PageHeadingTitle',
  styled.h1,
  styles,
  { modifierConfig, themePropTypes },
);
