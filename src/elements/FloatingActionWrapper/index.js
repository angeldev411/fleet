import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const modifierConfig = {
  bottomRight: () => ({
    styles: `
      > * {
        bottom: 20vh;
        right: 1rem;
      }
    `,
  }),

  bounceIn: ({ theme }) => ({
    styles: `
      > * {
        animation: ${theme.animations.bounceOut} 200ms ease;
      }
    `,
  }),

  boxShadow: ({ theme }) => ({
    styles: `
      > * {
        box-shadow: 0 1px 2px 0 ${theme.colors.base.shadow};
      }
    `,
  }),

  hide: () => ({
    styles: `
      > * {
        opacity: 0;
      }
    `,
  }),
};

/* istanbul ignore next */
const styles = ({ theme }) => `
  > * {
    animation: ${theme.animations.bounceIn} 200ms ease;
    opacity: 1;
    position: fixed;
  }
`;

const themePropTypes = {
  animations: PropTypes.shape({
    bounceIn: PropTypes.string.isRequired,
    bounceOut: PropTypes.string.isRequired,
  }).isRequired,
  colors: PropTypes.shape({
    base: PropTypes.shape({
      shadow: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'FloatingActionWrapper',
  styled.span,
  styles,
  { modifierConfig, themePropTypes },
);
