import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import FloatingActionWrapper from '../index';

function buildComponent() {
  return shallow(
    <FloatingActionWrapper />,
  );
}

test('Renders div', () => {
  expect(buildComponent()).toBeA('span');
});
