import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  column: () => ({ styles: 'flex-direction: column;' }),
  left: () => ({ styles: 'justify-content: flex-start;' }),
  narrow: () => ({ styles: 'flex: 0.5;' }),
  right: () => ({ styles: 'justify-content: flex-end;' }),
  wide: () => ({ styles: 'flex: 2;' }),
  xNarrow: () => ({ styles: 'flex: 0.25;' }),
};

/* istanbul ignore next */
const styles = () => `
  display: flex;
  flex: 1;
`;

export default buildStyledComponent(
  'SplitBlockPart',
  styled.div,
  styles,
  { modifierConfig },
);
