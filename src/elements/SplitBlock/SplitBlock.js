import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  flexColumn: () => ({ styles: 'flex-flow: column wrap;' }),
  flexRow: () => ({ styles: 'flex-flow: row wrap;' }),
  pad: () => ({ styles: `padding: ${px2rem(8)};` }),
  padBottom: () => ({ styles: `padding-bottom: ${px2rem(8)};` }),
  padLeft: () => ({ styles: `padding-left: ${px2rem(8)};` }),
  padRight: () => ({ styles: `padding-right: ${px2rem(8)};` }),
  padTop: () => ({ styles: `padding-top: ${px2rem(8)};` }),
};

/* istanbul ignore next */
const styles = () => `
  display: flex;
  flex: 1;
`;

export default buildStyledComponent(
  'SplitBlock',
  styled.div,
  styles,
  { modifierConfig },
);
