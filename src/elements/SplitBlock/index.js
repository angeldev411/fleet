/**
 * The SplitBlock elements provide a reusable set of components that allow us to split a full
 * width <div> into left aligned and right aligned sections.
 *
 * Expected use is as follows:
 *
 * <SplitBlock modifiers={['pad']}>
 *   <SplitBlockPart modifiers={['left']} > // indicates that this part of the SplitBlock should
 *                                          // align left
 *     <SplitBlockElement modifiers={['padRight']}> // indicates that padding should be
 *                                          // applied to the right side of this element.
 *       {content}
 *     </SplitBlockElement>
 *   </SplitBlockPart>
 *   <SplitBlockPart modifiers={['right']} > // indicates that this part of the SplitBlock should
 *                                           // align right
 *     <SplitBlockElement modifiers={['padLeft']}>
 *       {content}
 *     </SplitBlockElement>
 *   </SplitBlockPart>
 * </SplitBlock>
 */

export SplitBlock from './SplitBlock';
export SplitBlockPart from './SplitBlockPart';
export SplitBlockElement from './SplitBlockElement';
