import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  pad: () => ({ styles: `padding: 0 ${px2rem(8)};` }),
  padLeft: () => ({ styles: `padding-left: ${px2rem(8)};` }),
  padRight: () => ({ styles: `padding-right: ${px2rem(8)};` }),
};


/* istanbul ignore next */
const styles = () => `
  align-items: center;
  display: flex;
  h2 {
    margin: 0;
    padding-right: 0.5rem;
  }
  h3 {
    margin: 0;
    padding: 0.5rem;
  }
`;

export default buildStyledComponent(
  'SplitBlockElement',
  styled.div,
  styles,
  { modifierConfig },
);
