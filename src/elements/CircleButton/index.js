/**
 * Component: CircleButton
 *
 * Produces a button tag styled to be circular.
 *
 * modifiers:
 *   darkColor: The color and hover color for background and font.
                If darkColor is not specified, normal bright color will be rendered as default.
 *   normalFont: The font size of the CircleButton text/icon
 */

import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  brandPrimary: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.brand.primary};
      color: ${theme.colors.base.chrome000};
    `,
  }),

  brandSecondary: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.brand.secondary};
      color: ${theme.colors.base.chrome000};
    `,
  }),

  hoverBrand: ({ theme }) => ({
    styles: `
      &:hover {
        background-color: ${theme.colors.brand.hover};
        color: ${theme.colors.base.chrome000};
      }
    `,
  }),

  hoverShadow: ({ theme }) => ({
    styles: `
      &:hover {
        box-shadow: 0 2px 4px ${theme.colors.base.shadow};
      }
    `,
  }),
};

/* istanbul ignore next */
const styles = ({ theme }) => `
  background-color: ${theme.colors.base.chrome200};
  border-radius: ${px2rem(34 / 2)};
  border: none;
  color: ${theme.colors.base.chrome500};
  cursor: pointer;
  font-size: inherit;
  height: ${px2rem(34)};
  line-height: normal;
  outline: none;
  width: ${px2rem(34)};
  &:hover {
    background-color: ${theme.colors.base.chrome200};
    color: ${theme.colors.brand.hover};
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome000: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      chrome500: PropTypes.string.isRequired,
      shadow: PropTypes.string.isRequired,
    }).isRequired,
    brand: PropTypes.shape({
      hover: PropTypes.string.isRequired,
      secondary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    fontSizeNormal: PropTypes.string.isRequired,
  }).isRequired,
};


export default buildStyledComponent(
  'CircleButton',
  styled.button,
  styles,
  { modifierConfig, themePropTypes },
);
