import styled from 'styled-components';

export default styled.div`
  flex: 1;
  margin-bottom: 0.5em;
`;
