import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  flex: 1;
`;

export default buildStyledComponent(
  'FormRadioGroup',
  styled.div,
  styles,
);
