import styled from 'styled-components';

export default styled.input`
  flex: 1;
  line-height: 2em;
`;
