import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  display: flex;
  padding: ${px2rem(20)} 0;
`;

export default buildStyledComponent(
  'FormSection',
  styled.div,
  styles,
);
