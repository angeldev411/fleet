import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  flex: 1;
  display: flex;
`;

export default buildStyledComponent(
  'FormInlineRadioSection',
  styled.div,
  styles,
);
