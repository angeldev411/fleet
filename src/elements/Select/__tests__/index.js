import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import Select from '../index';

const defaultProps = {
  key1: 'key1',
  key2: 'key2',
};

function shallowRender(props = defaultProps) {
  return shallow(<Select {...props} />);
}

test('renders a SelectWrapper.', () => {
  const component = shallowRender();
  expect(component).toBeA('SelectWrapper');
});

test('renders first children with the props passed', () => {
  const component = shallowRender();
  expect(component.children().first()).toHaveProps(defaultProps);
});
