import React from 'react';
import ReactSelectPlus from 'react-select-plus';

import SelectWrapper from './SelectWrapper';

function Select(props) {
  return (
    <SelectWrapper>
      <ReactSelectPlus {...props} />
    </SelectWrapper>
  );
}

export default Select;
