/**
 * The WidgetTableRow component is used to apply a gap above table rows as is shown in the designs.
 * As this gap is not always present, it must be passed in as a prop. For consistency within the
 * component, all rows should use the WidgetTableRow component.
 */
import styled from 'styled-components';
import {
  applyStyleModifiers,
  styleModifierPropTypes,
} from 'styled-components-modifiers';
import { px2rem } from 'decisiv-ui-utils';

const COMPONENT_NAME = 'WidgetTableRow';

const TD_MODIFIER_CONFIG = {
  topGap: () => ({
    styles: `padding-top: ${px2rem(14)};`,
  }),
  topGapSmall: () => ({
    styles: `padding-top: ${px2rem(4)};`,
  }),
};

const WidgetTableRow = styled.tr`
  & > th,
  & > td {
    padding-top: 0;
    ${applyStyleModifiers(TD_MODIFIER_CONFIG, 'tdModifiers')}
  }
`;

WidgetTableRow.propTypes = {
  tdModifiers: styleModifierPropTypes(TD_MODIFIER_CONFIG),
};

WidgetTableRow.displayName = COMPONENT_NAME;

export default WidgetTableRow;
