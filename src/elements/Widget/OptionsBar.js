import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Container } from 'styled-components-reactive-grid';

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.chrome100};
  border: 1px solid ${props.theme.colors.base.chrome200};
  color: ${props.theme.colors.base.textLight};
  font-size: ${px2rem(12)};
  line-height: ${px2rem(15)};
  margin-top: ${px2rem(8)};
  padding-bottom: 0;
  padding-top: 0;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'OptionsBar',
  styled(Container),
  styles,
  { themePropTypes },
);
