import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/**
 * Wrapper component for Grid
 */
/* istanbul ignore next */
const styles = props => `
  padding: ${props.theme.dimensions.gridGutterSizeHalf};
  box-sizing: border-box;
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    gridGutterSizeHalf: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'WidgetWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
