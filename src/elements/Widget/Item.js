/**
 * The WidgetItem component provides a padded block within the body of the card. It encapsulates
 * sections to provide the expected gaps on all sides.
 */

import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Container } from 'styled-components-reactive-grid';

const styles = () => `
  padding: ${px2rem(11)} ${px2rem(14)};
  display: ${({ horizontal }) => (horizontal === true ? 'flex' : 'block')};
`;

const propTypes = {
  horizontal: PropTypes.bool,
};

const defaultProps = {
  horizontal: false,
};

const Item = buildStyledComponent(
  'Item',
  styled(Container),
  styles,
  { defaultProps, propTypes },
);

export default Item;
