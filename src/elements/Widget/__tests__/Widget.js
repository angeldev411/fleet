import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import Widget from '../index';

const defaultProps = {
  expandKey: 'test-widget',
};

function shallowRender(props = defaultProps) {
  return shallow(<Widget {...props} />);
}

test('Renders Widget with an Expandable', () => {
  expect(shallowRender()).toContain('Connect(Expandable)');
});

test('Contains an ExpandableHeader', () => {
  expect(shallowRender()).toContain('ExpandableHeader');
});

test('Contains an ExpandableContent', () => {
  expect(shallowRender()).toContain('ExpandableContent');
});

test('Renders an Expandable with props expandOnHeaderClick', () => {
  expect(shallowRender().find('Connect(Expandable)')).toHaveProp('expandOnHeaderClick', true);
});

test('Renders Widget as an Expandable with the right expandKey', () => {
  const props = {
    ...defaultProps,
    expandKey: 'test',
  };
  expect(shallowRender(props).find('Connect(Expandable)'))
    .toHaveProps({ expandKey: `widget-${props.expandKey}` });
});

test('Renders WidgetHeader into an ExpandableHeader and Others into and ExpandableContent', () => {
  const header = <Widget.Header>header</Widget.Header>;
  const content = <div>Content</div>;
  const component = shallow(
    <Widget expandKey="test">
      {header}
      {content}
    </Widget>,
  );
  expect(component.find('ExpandableHeader').contains(header));
  expect(component.find('ExpandableContent').contains(content));
});
