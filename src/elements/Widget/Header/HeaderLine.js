import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  border-bottom: 1px solid ${props.theme.colors.base.chrome400};
  flex: 1;
  margin-bottom: ${px2rem(10)};
  margin-left: ${px2rem(10)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome400: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'HeaderLine',
  styled.div,
  styles,
  { themePropTypes },
);
