import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import Header from '../index';

const children = <p>Default Title</p>;

const defaultProps = {
  children,
};

function shallowRender(props = defaultProps) {
  return shallow(<Header {...props} />);
}

test('Renders wrapper component', () => {
  expect(shallowRender()).toContain('Wrapper');
});

test('Renders HeaderTitle component with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('HeaderTitle');
  expect(component.find('HeaderTitle')).toHaveProp('children', children);
});

test('Does not render HeaderNote with no note passed in', () => {
  const component = shallowRender();
  expect(component).toNotContain('HeaderNote');
});

test('Renders HeaderNote with expected props if note is passed in', () => {
  const note = 'Test Note';
  const testProps = {
    children,
    note,
  };
  const component = shallowRender(testProps);
  expect(component).toContain('HeaderNote');
  expect(component.find('HeaderNote')).toHaveProp('children', note);
});
