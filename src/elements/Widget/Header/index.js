import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './Wrapper';
import HeaderTitle from './HeaderTitle';
import HeaderNote from './HeaderNote';
import HeaderLine from './HeaderLine';

function Header({ children, note }) {
  return (
    <Wrapper>
      <HeaderTitle>{children}</HeaderTitle>
      {note && <HeaderNote>{note}</HeaderNote>}
      <HeaderLine />
    </Wrapper>
  );
}

Header.propTypes = {
  children: PropTypes.node,
  note: PropTypes.string,
};

Header.defaultProps = {
  children: null,
  note: '',
};

Header.displayName = 'Header';

export default Header;
