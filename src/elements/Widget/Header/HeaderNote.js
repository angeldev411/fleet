/**
 * The HeaderNote provides a styled <h2> element that is used at the top of the widget only.
 */

import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.textLight};
  font-size: ${px2rem(18)};
  height: ${px2rem(22)};
  line-height: ${px2rem(22)};
  margin-left: ${px2rem(8)};
  padding: 0;
  text-transform: uppercase;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'HeaderNote',
  styled.span,
  styles,
  { themePropTypes },
);
