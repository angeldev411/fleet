import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

import Wrapper from './Wrapper';
import Paginator from './Paginator';
import PageItem from './PageItem';

function Pagination({
  totalPages,
  currentPage,
  onPrevClicked,
  onNextClicked,
  onPageClicked,
  onAddClicked,
}) {
  return (
    <Wrapper>
      <Paginator
        totalPages={totalPages}
        currentPage={currentPage}
        onPrevClicked={onPrevClicked}
        onNextClicked={onNextClicked}
        onPageClicked={onPageClicked}
      />
      <PageItem href="#" onClick={() => onAddClicked()}>
        <FontAwesome name="plus" />
      </PageItem>
    </Wrapper>
  );
}

Pagination.propTypes = {
  totalPages: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPrevClicked: PropTypes.func.isRequired,
  onNextClicked: PropTypes.func.isRequired,
  onPageClicked: PropTypes.func.isRequired,
  onAddClicked: PropTypes.func.isRequired,
};

export default Pagination;
