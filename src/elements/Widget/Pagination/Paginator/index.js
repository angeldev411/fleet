import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

import PaginatorWrapper from './PaginatorWrapper';
import PageItem from '../PageItem';

function buildPageLinks(totalPages, currentPage, onPageClicked) {
  const rawArray = [...Array(totalPages).keys()];
  const pageLinkArray = rawArray.map(v => v + 1);
  return pageLinkArray.map(pageNum => (
    <PageItem
      key={pageNum}
      href="#"
      selected={pageNum === currentPage}
      onClick={(e) => {
        e.preventDefault();
        onPageClicked(pageNum);
      }}
    >
      {pageNum}
    </PageItem>
  ));
}

function Paginator({
  totalPages,
  currentPage,
  onPrevClicked,
  onNextClicked,
  onPageClicked,
}) {
  return (
    <PaginatorWrapper>
      <PageItem
        href="#"
        onClick={onPrevClicked}
      >
        <FontAwesome name="chevron-left" />
      </PageItem>
      { buildPageLinks(totalPages, currentPage, onPageClicked) }
      <PageItem
        href="#"
        onClick={onNextClicked}
      >
        <FontAwesome name="chevron-right" />
      </PageItem>
    </PaginatorWrapper>
  );
}

Paginator.propTypes = {
  totalPages: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPrevClicked: PropTypes.func.isRequired,
  onNextClicked: PropTypes.func.isRequired,
  onPageClicked: PropTypes.func.isRequired,
};

export default Paginator;
