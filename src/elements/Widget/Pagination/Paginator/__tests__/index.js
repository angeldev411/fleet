import React from 'react';
import { noop } from 'lodash';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import Paginator from '../index';

const totalPages = 5;
const currentPage = 1;

function renderComponent({
  onPrevClicked = noop,
  onNextClicked = noop,
  onPageClicked = noop,
  onAddClicked = noop,
} = {}) {
  return shallow(
    <Paginator
      totalPages={totalPages}
      currentPage={currentPage}
      onPrevClicked={onPrevClicked}
      onNextClicked={onNextClicked}
      onPageClicked={onPageClicked}
      onAddClicked={onAddClicked}
    />);
}

test('Renders correct PageItem components', () => {
  const paginatorWrapper = renderComponent().find('PaginatorWrapper');
  expect(paginatorWrapper.find('PageItem').length).toEqual(2 + totalPages);
});

test('Renders Prev arrow FontAwesome components', () => {
  const paginatorWrapper = renderComponent().find('PaginatorWrapper');
  const firstChild = paginatorWrapper.find('PageItem').first();
  expect(firstChild).toContain('FontAwesome[name="chevron-left"]');
});

test('Renders Next arrow FontAwesome components', () => {
  const paginatorWrapper = renderComponent().find('PaginatorWrapper');
  const lastChild = paginatorWrapper.find('PageItem').last();
  expect(lastChild).toContain('FontAwesome[name="chevron-right"]');
});

test('Clicking Prev Arrow button', () => {
  const onPrevClicked = expect.createSpy();
  const paginatorWrapper = renderComponent({ onPrevClicked }).find('PaginatorWrapper');
  const prevIcon = paginatorWrapper.find('PageItem').first();
  prevIcon.simulate('click');
  expect(onPrevClicked).toHaveBeenCalled();
});

test('Clicking Next Arrow button', () => {
  const onNextClicked = expect.createSpy();
  const paginatorWrapper = renderComponent({ onNextClicked }).find('PaginatorWrapper');
  const nextIcon = paginatorWrapper.find('PageItem').last();
  nextIcon.simulate('click');
  expect(onNextClicked).toHaveBeenCalled();
});

test('Clicking the first page link button', () => {
  const onPageClicked = expect.createSpy();
  const paginatorWrapper = renderComponent({ onPageClicked }).find('PaginatorWrapper');
  const nextIcon = paginatorWrapper.find('PageItem').at(1);
  nextIcon.simulate('click', { preventDefault: noop });
  expect(onPageClicked).toHaveBeenCalled();
});
