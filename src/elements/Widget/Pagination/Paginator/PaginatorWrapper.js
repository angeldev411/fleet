import styled from 'styled-components';

const PaginatorWrapper = styled.div`
  display: flex;
`;

export default PaginatorWrapper;
