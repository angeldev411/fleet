import React from 'react';
import { noop } from 'lodash';
import { test, expect, shallow, createSpy } from '__tests__/helpers/test-setup';

import CardPaginator from '../index';

const defaultTotalPages = 5;
const defaultCurrentPage = 1;

function renderComponent({
  totalPages = defaultTotalPages,
  currentPage = defaultCurrentPage,
  onPrevClicked = noop,
  onNextClicked = noop,
  onPageClicked = noop,
  onAddClicked = noop,
} = {}) {
  return shallow(<CardPaginator
    totalPages={totalPages}
    currentPage={currentPage}
    onPrevClicked={onPrevClicked}
    onNextClicked={onNextClicked}
    onPageClicked={onPageClicked}
    onAddClicked={onAddClicked}
  />);
}

test('Renders Paginator component with props', () => {
  const onPrevClicked = noop;
  const onNextClicked = noop;
  const onPageClicked = noop;
  const paginator = renderComponent({
    onPrevClicked,
    onNextClicked,
    onPageClicked,
  }).find('Paginator');
  expect(paginator).toExist();
  expect(paginator).toHaveProp('totalPages', defaultTotalPages);
  expect(paginator).toHaveProp('currentPage', defaultCurrentPage);
  expect(paginator).toHaveProp('onPrevClicked', onPrevClicked);
  expect(paginator).toHaveProp('onNextClicked', onNextClicked);
  expect(paginator).toHaveProp('onPageClicked', onPageClicked);
});

test('Renders PageItem component with props', () => {
  expect(renderComponent()).toContain('PageItem');
});

test('Renders FontAwesome component with `plus` name', () => {
  const pageItem = renderComponent().find('PageItem');
  expect(pageItem).toContain('FontAwesome[name="plus"]');
});

test('Calls handler when plus is clicked', () => {
  const onAddClicked = createSpy();
  const component = renderComponent({ onAddClicked });
  const plus = component.find('PageItem');
  plus.simulate('click');
  expect(onAddClicked).toHaveBeenCalled();
});
