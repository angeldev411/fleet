import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  selected: ({ theme }) => ({
    styles: `color: ${theme.colors.base.text};`,
  }),
};

/* istanbul ignore next */
const styles = ({ theme }) => `
  color: ${theme.colors.base.chrome500};
  font-size: ${px2rem(15)};
  font-weight: normal;
  padding-right: ${px2rem(16)};
  text-decoration: none;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome500: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  selected: PropTypes.bool,
};

const defaultProps = {
  selected: false,
};

export default buildStyledComponent(
  'PageItem',
  styled.a,
  styles,
  { defaultProps, modifierConfig, propTypes, themePropTypes },
);
