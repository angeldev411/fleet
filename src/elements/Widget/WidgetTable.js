/**
 * The WidgetTable component creates some specific styling around the table structure unique to
 * widgets. Specifically, there is a gap between the header cells and the display cells, which
 * are aligned vertically. Use the WidgetTable to build any table within a widget.
 */
import styled from 'styled-components';
import {
  applyStyleModifiers,
  styleModifierPropTypes,
} from 'styled-components-modifiers';
import { px2rem } from 'decisiv-ui-utils';

const COMPONENT_NAME = 'WidgetTable';

const TABLE_MODIFIER_CONFIG = {
  noFlex: () => ({
    styles: `
      flex: none;
      margin-right: ${px2rem(40)};
    `,
  }),
};

const TH_MODIFIER_CONFIG = {
  padRight: () => ({
    styles: `padding-right: ${px2rem(26)};`,
  }),
};

const WidgetTable = styled.table`
  font-size: ${px2rem(12)};
  flex: 1;
  display: block;
  ${applyStyleModifiers(TABLE_MODIFIER_CONFIG)}

  th {
    padding-right: 1em;
    text-align: left;
    vertical-align: top;
    ${applyStyleModifiers(TH_MODIFIER_CONFIG, 'thModifiers')}
  }

  td {
    vertical-align: top;
  }
`;

WidgetTable.propTypes = {
  modifiers: styleModifierPropTypes(TABLE_MODIFIER_CONFIG),
  thModifiers: styleModifierPropTypes(TH_MODIFIER_CONFIG),
};

WidgetTable.displayName = COMPONENT_NAME;

export default WidgetTable;
