/**
 * The WidgetTable component creates some specific styling around the table structure unique to
 * widgets. Specifically, there is a gap between the header cells and the display cells, which
 * are aligned vertically. Use the WidgetTable to build any table within a widget.
 */

import styled from 'styled-components';

import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  noFlex: () => ({
    styles: `
      flex: none;
      margin-right: ${px2rem(40)};
    `,
  }),
  padRight: () => ({
    styles: `
      th {
        padding-right: ${px2rem(26)}
      }
    `,
  }),
};

const styles = () => `
  font-size: ${px2rem(12)};
  flex: 1;
  display: block;

  th {
    padding-right: 1em;
    text-align: left;
    vertical-align: top;
  }

  td {
    vertical-align: top;
  }
`;

const Table = buildStyledComponent(
  'Table',
  styled.table,
  styles,
  { modifierConfig },
);

export default Table;
