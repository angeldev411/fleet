import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Expandable, { ExpandableContent, ExpandableHeader } from 'containers/Expandable';

import { Divider } from 'base-components';
import Header from './Header';
import Item from './Item';
import OptionsBar from './OptionsBar';
import Pagination from './Pagination';
import SubHeader from './SubHeader';
import Table from './Table';
import TableRow from './TableRow';
import Wrapper from './Wrapper';

/*
  Props:
    expandKey: It is a unique key per widget so that it's expand/collapse status
      is stored in local storage.
      Prefix is there "widget-" so if expandKey is "test" actual key stored in local storage
      will be "widget-test"
      It is important not to use same name (you can pass this prop like sending id)
      between different widgets.
*/

class Widget extends Component {
  static propTypes = {
    children: PropTypes.node,
    expandKey: PropTypes.string.isRequired,
  };

  static defaultProps = {
    children: null,
    expandKey: '',
  };

  getHeader = () => {
    const { children } = this.props;
    const targetComponent = React.Children
      .toArray(children)
      .filter(c => c.type.displayName === 'Header')[0];
    return targetComponent;
  };

  getContent = () => {
    const { children } = this.props;
    const contentComponents = React.Children
      .toArray(children)
      .filter(c => c.type.displayName !== 'Header');
    return contentComponents;
  };

  render() {
    const { expandKey } = this.props;
    return (
      <Wrapper>
        <Expandable expandKey={`widget-${expandKey}`} expandOnHeaderClick>
          <ExpandableHeader>
            {this.getHeader()}
          </ExpandableHeader>
          <ExpandableContent>
            {this.getContent()}
          </ExpandableContent>
        </Expandable>
      </Wrapper>
    );
  }
}

Widget.Divider = Divider;
Widget.Header = Header;
Widget.Item = Item;
Widget.OptionsBar = OptionsBar;
Widget.Pagination = Pagination;
Widget.SubHeader = SubHeader;
Widget.Table = Table;
Widget.TableRow = TableRow;


export default Widget;
