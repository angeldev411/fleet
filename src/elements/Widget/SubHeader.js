/**
 * The WidgetSubHeader provides a styled <h3> element typically used to declare a status or name a
 * specific value within the Widget. It is typically encapsulated in its own WidgetItem component.
 */

import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const modifierConfig = {
  small: () => ({ styles: `font-size: ${px2rem(12)};` }),
  statusDanger: ({ theme }) => ({ styles: `color: ${theme.colors.status.danger};` }),
  statusInfo: ({ theme }) => ({ styles: `color: ${theme.colors.status.info};` }),
  statusSuccess: ({ theme }) => ({ styles: `color: ${theme.colors.status.success};` }),
  statusWarning: ({ theme }) => ({ styles: `color: ${theme.colors.status.warning};` }),
};

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.text};
  font-size: ${px2rem(16)};
  font-weight: 600;
  margin: 0;
  padding: 0;
  text-transform: uppercase;

  .value {
    font-weight: 300;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
      info: PropTypes.string.isRequired,
      success: PropTypes.string.isRequired,
      warning: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'SubHeader',
  styled.h3,
  styles,
  { modifierConfig, themePropTypes },
);
