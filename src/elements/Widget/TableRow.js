/**
 * The TableRow component is used to apply a gap above table rows as is shown in the designs.
 * As this gap is not always present, it must be passed in as a prop. For consistency within the
 * component, all rows should use the WidgetTableRow component.
 */

import styled from 'styled-components';

import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  topGap: () => ({
    styles: `
      & > th,
      & > td {
        padding-top: ${px2rem(14)};
      }
    `,
  }),
  topGapSmall: () => ({
    styles: `
      & > th,
      & > td {
        padding-top: ${px2rem(4)};
      }
    `,
  }),
};

const styles = () => `
  & > th,
  & > td {
    padding-top: 0;
  }
`;

const TableRow = buildStyledComponent(
  'TableRow',
  styled.tr,
  styles,
  { modifierConfig },
);

export default TableRow;
