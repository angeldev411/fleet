import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const Grid = styled.div`
  margin: ${px2rem(8)} ${px2rem(-9)};
`;

export default Grid;
