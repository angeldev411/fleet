import PropTypes from 'prop-types';
import styled from 'styled-components';

function getWidth(position) {
  switch (position) {
    case 'left':
      return '66.67%';
    case 'right':
      return '33.33%';
    case 'top':
    default:
      return '100%';
  }
}

/**
 * GridBlock component that will sit inside Grid
 * props:
 *   position (string): Defines where it will sit, namely: top, left and right.
 */
const GridBlock = styled.div`
  display: inline-block;
  vertical-align: top;
  width: ${props => getWidth(props.position)};
`;

GridBlock.propTypes = {
  position: PropTypes.oneOf([
    'top',
    'left',
    'right',
  ]),
};

GridBlock.defaultProps = {
  position: 'top',
};

export default GridBlock;
