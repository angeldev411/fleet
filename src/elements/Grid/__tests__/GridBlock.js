import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import GridBlock from '../GridBlock';

test('Renders the children inside GridBlock', () => {
  const children = (<p id="testChild">Test child</p>);
  const wrapper = shallow(
    <GridBlock >
      {children}
    </GridBlock>,
  );
  expect(wrapper).toContain('p#testChild');
});
