import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import Grid from '../Grid';

test('Renders the children inside Grid', () => {
  const children = (<p id="testChild">Test child</p>);
  const wrapper = shallow(
    <Grid >
      {children}
    </Grid>,
  );
  expect(wrapper).toContain('p#testChild');
});
