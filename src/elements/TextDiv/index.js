import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  active: ({ theme }) => ({ styles: `color: ${theme.colors.base.link};` }),
  bold: () => ({ styles: 'font-weight: bold;' }),
  brand: ({ theme }) => ({ styles: `color: ${theme.colors.brand.secondary};` }),
  bright: ({ theme }) => ({ styles: `color: ${theme.colors.brand.primary};` }),
  bottomGap: () => ({ styles: `margin-bottom: ${px2rem(4)};` }),
  capitalize: () => ({ styles: 'text-transform: capitalize;' }),
  darkGreyText: ({ theme }) => ({ styles: `color: ${theme.colors.base.text};` }),
  ellipsis: () => ({
    styles: `
      overflow: hidden;
      text-overflow: ellipsis;
    `,
  }),
  heavyText: () => ({ styles: 'font-weight: 500;' }),
  inline: () => ({ styles: 'display: inline;' }),
  inlineBlock: () => ({ styles: 'display: inline-block;' }),
  italic: () => ({ styles: 'font-style: italic;' }),
  largeText: () => ({ styles: `font-size: ${px2rem(24)};` }),
  lightGreyText: ({ theme }) => ({ styles: `color: ${theme.colors.base.chrome300};` }),
  lightText: () => ({ styles: 'font-weight: 300;' }),
  mediumText: () => ({
    styles: `
      font-size: ${px2rem(14)};
      line-height: ${px2rem(18)};
    `,
  }),
  mediumLargeText: () => ({ styles: `font-size: ${px2rem(20)};` }),
  midGreyText: ({ theme }) => ({ styles: `color: ${theme.colors.base.textLight};` }),
  multiLine: () => ({ styles: 'white-space: pre-line;' }),
  oneLine: () => ({ styles: 'white-space: nowrap;' }),
  padRight: () => ({ styles: `padding-right: ${px2rem(8)};` }),
  short: () => ({
    styles: `
      -webkit-box-orient: vertical;
      -webkit-line-clamp: 3;
      display: -webkit-box;
      height: ${px2rem(45)};
      line-height: ${px2rem(15)};
    `,
  }),
  smallText: () => ({
    styles: `
      font-size: ${px2rem(12)};
      line-height: ${px2rem(18)};
    `,
  }),
  smallWidth: () => ({
    styles: 'max-width: 15rem;',
  }),
  statusDefault: ({ theme }) => ({ styles: `color: ${theme.colors.status.default};` }),
  statusInfo: ({ theme }) => ({ styles: `color: ${theme.colors.status.info};` }),
  uppercase: () => ({ styles: 'text-transform: uppercase;' }),
  xLargeText: () => ({
    styles: `
      font-size: ${px2rem(30)};
      line-height: ${px2rem(26)};
      .fa {
        font-size: ${px2rem(30)};
        line-height: ${px2rem(26)};
      }
    `,
  }),
  xSmallText: () => ({
    styles: `
      font-size: ${px2rem(10)};
    `,
  }),
  textAlignRight: () => ({
    styles: `
      text-align: right;
    `,
  }),
  textAlignCenter: () => ({
    styles: `
      text-align: center;
    `,
  }),
};

/* istanbul ignore next */
const styles = `
  position: relative;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
      link: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }),
    brand: PropTypes.shape({
      primary: PropTypes.string.isRequired,
      secondary: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      default: PropTypes.string.isRequired,
      info: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'TextDiv',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
