import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

export function defineBorderRadius({ round }) {
  if (round) {
    return `border-radius: ${px2rem(12)};`;
  }
  return '';
}

export function defineDimensions({ round }) {
  if (round) {
    return `
      height: ${px2rem(24)};
      width: ${px2rem(24)};
    `;
  }
  return '';
}

const GravatarWrapper = styled.div`
  img {
    ${defineBorderRadius}
    ${defineDimensions}
    position: relative;
  }
`;

export default GravatarWrapper;
