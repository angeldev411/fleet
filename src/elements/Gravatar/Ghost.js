import { GhostIndicator } from 'base-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';
import styled from 'styled-components';

const styles = `
  border-radius: ${px2rem(12)};
  height: ${px2rem(24)};
  width: ${px2rem(24)};
`;

export default buildStyledComponent(
  'GravatarGhost',
  styled(GhostIndicator),
  styles,
);
