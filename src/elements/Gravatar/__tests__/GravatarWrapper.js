import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import {
  defineBorderRadius,
  defineDimensions,
} from '../GravatarWrapper';

test('defineDimensions returns empty string if round = falsey', () => {
  expect(defineDimensions({})).toEqual('');
});

test('defineDimensions sets height and width if round = true', () => {
  expect(defineDimensions({ round: true })).toContain('height:');
  expect(defineDimensions({ round: true })).toContain('width:');
});

test('defineBorderRadius returns empty string if round = falsey', () => {
  expect(defineBorderRadius({})).toEqual('');
});

test('defineBorderRadius sets border-radius if round = true', () => {
  expect(defineBorderRadius({ round: true })).toContain('border-radius:');
});
