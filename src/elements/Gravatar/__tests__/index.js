import React from 'react';
import UnstyledGravatar from 'react-gravatar';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import Gravatar from '../index';

function shallowRender(props = {}) {
  return shallow(<Gravatar {...props} />);
}

test('passes style props to GravatarWrapper', () => {
  const props = { round: true };
  const component = shallowRender(props);
  expect(component.find('GravatarWrapper')).toHaveProps(props);
});

test('contains an UnstyledGravatar', () => {
  expect(shallowRender()).toContain('Gravatar');
});

test('does not pass style props to UnstyledGravatar', () => {
  const props = { round: true, email: 'bye@felicia.com' };
  const component = shallowRender(props);
  expect(component.find(UnstyledGravatar).props()).toNotContain({ round: true });
});

test('passes all non-style props to UnstyledGravatar', () => {
  const props = { email: 'bye@felicia.com' };
  const component = shallowRender(props);
  expect(component.find(UnstyledGravatar)).toHaveProp('email', 'bye@felicia.com');
});

test('passes `invalid` as the email prop to UnstyledGravatar if it is not provided', () => {
  const props = { email: undefined };
  const component = shallowRender(props);
  expect(component.find(UnstyledGravatar)).toHaveProp('email', 'invalid');
});

test('sets the default style to identicon if email is provided', () => {
  const component = shallowRender({ email: 'bye@felicia.com' });
  expect(component.find(UnstyledGravatar)).toHaveProp('default', 'identicon');
});

test('sets the default style to mm if email is not provided', () => {
  const component = shallowRender();
  expect(component.find(UnstyledGravatar)).toHaveProp('default', 'mm');
});
