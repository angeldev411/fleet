import React from 'react';
import UnstyledGravatar from 'react-gravatar';
import PropTypes from 'prop-types';
import { omit } from 'lodash';

import GravatarGhost from './Ghost';
import GravatarWrapper from './GravatarWrapper';

function Gravatar(props) {
  const gravatarProps = {
    ...omit(props, ['ghost', 'round']),
    default: props.email ? 'identicon' : 'mm',
    email: props.email || 'invalid',
  };

  return (
    <GravatarWrapper {...props} >
      {props.ghost ?
        <GravatarGhost /> :
        <UnstyledGravatar {...gravatarProps} />
      }
    </GravatarWrapper>
  );
}

Gravatar.propTypes = {
  email: PropTypes.string,
  ghost: PropTypes.bool,
};

Gravatar.defaultProps = {
  email: '',
  ghost: false,
};

export default Gravatar;
