import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  extraLightGray: ({ theme }) => ({
    styles: `
      border-bottom: 2px solid ${theme.colors.base.chrome200};
    `,
  }),
  extraWide: () => ({
    styles: `
      margin-left: -${px2rem(8)};
      margin-right: -${px2rem(8)};
    `,
  }),
  gutter: () => ({
    styles: `
      margin-top: ${px2rem(4)};
      margin-bottom: ${px2rem(4)};
    `,
  }),
  heavy: ({ theme }) => ({
    styles: `
      border-bottom: 2px solid ${theme.colors.base.chrome300};
    `,
  }),
  heavyMidGray: ({ theme }) => ({
    styles: `
      border-bottom: 2px solid ${theme.colors.base.chrome500};

    `,
  }),
  light: ({ theme }) => ({
    styles: `
      border-bottom: 2px solid ${theme.colors.base.chrome200};
    `,
  }),
  narrow: () => ({
    styles: `margin: 0 ${px2rem(4)};`,
  }),
};

/* istanbul ignore next */
const styles = props => `
  border-bottom: 1px solid ${props.color || props.theme.colors.base.chrome300};
  margin-bottom: ${px2rem(props.marginBottom)};
  margin-top: ${px2rem(props.marginTop)};
  position: relative;
  width: auto;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome200: PropTypes.string.isRequired,
      chrome300: PropTypes.string.isRequired,
      chrome500: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  color: PropTypes.string,
};

export default buildStyledComponent(
  'Divider',
  styled.div,
  styles,
  { modifierConfig, propTypes, themePropTypes },
);
