import styled from 'styled-components';

// NOTE: To enable horizontal scrolling,
// you'll also need to set a min-width on td elements

const TableWrapper = styled.div`
  overflow-x: scroll;
  overflow-y: hidden;
  padding: 0.5rem;
`;

export default TableWrapper;
