import PropTypes from 'prop-types';
import styled from 'styled-components';
import UnstyledTextareaAutosize from 'react-autosize-textarea';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {};

/* istanbul ignore next */
const styles = ({ theme }) => `
  border: none;
  box-sizing: border-box;
  color: ${theme.colors.base.text};
  font-size: ${px2rem(12)};
  line-height: ${px2rem(19)};
  padding-left: 0;
  resize: none;
  width: 100%;
  &::placeholder {
    color: ${theme.colors.base.chrome300};
    font-weight: 300;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'TextareaAutosize',
  styled(UnstyledTextareaAutosize),
  styles,
  { modifierConfig, themePropTypes },
);
