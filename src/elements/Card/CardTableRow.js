import PropTypes from 'prop-types';
import styled from 'styled-components';

const CardTableRow = styled.tr`
  & > th,
  & > td {
    padding-top: ${({ type }) => (type === 'topGap' ? '0.6em' : '0')};
  }
`;

CardTableRow.propTypes = {
  type: PropTypes.oneOf(['topGap', '']),
};

CardTableRow.defaultProps = {
  type: '',
};
export default CardTableRow;
