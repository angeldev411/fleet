import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  flex: () => ({
    styles: `
      align-items: center;
      display: flex;
    `,
  }),
};

/* istanbul ignore next */
const styles = `
  padding: ${px2rem(8)};
`;

export default buildStyledComponent(
  'CardElement',
  styled.div,
  styles,
  { modifierConfig },
);
