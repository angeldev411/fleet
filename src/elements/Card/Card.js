import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  loading: ({ theme }) => ({
    styles: `
      box-shadow: 0 2px 3px 2px ${theme.colors.base.shadowLight};
      cursor: default;
      height: ${px2rem(300)};
    `,
  }),
  noPad: () => ({
    styles: 'padding: 0;',
  }),
  selected: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.base.selectableActive};
      box-shadow: 0 2px 3px 2px ${theme.colors.base.shadow};
      &:hover {
        background-color: ${theme.colors.base.selectableActive};
      }
    `,
  }),
  selectable: ({ theme }) => ({
    styles: `
      box-shadow: 0 2px 3px 2px ${theme.colors.base.shadowLight};
      &:hover {
        background-color: ${theme.colors.base.selectableHover};
      }
    `,
  }),
};

/* istanbul ignore next */
const styles = ({ theme }) => `
  background-color: ${theme.colors.base.background};
  border-radius: 2px;
  box-shadow: 0 2px 3px 2px ${theme.colors.base.shadowLight};
  cursor: pointer;
  margin: ${px2rem(4)};
  min-height: 50px;
  padding: ${px2rem(8)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
      shadow: PropTypes.string.isRequired,
      shadowLight: PropTypes.string.isRequired,
      selectableActive: PropTypes.string.isRequired,
      selectableHover: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Card',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
