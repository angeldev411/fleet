import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  '1_per_row': () => ({
    styles: `
      width: 100%;
    `,
  }),
  '2_per_row': () => ({
    styles: `
      width: 50%;
    `,
  }),
  '3_per_row': () => ({
    styles: `
      width: 33.33%;
    `,
  }),
  '4_per_row': () => ({
    styles: `
      width: 25%;
    `,
  }),
  '5_per_row': () => ({
    styles: `
      width: 20%;
    `,
  }),
};

/* istanbul ignore next */
const styles = () => `
  box-sizing: border-box;
  display: inline-block;
  vertical-align: top;
`;

export default buildStyledComponent(
  'CardWrapper',
  styled.div,
  styles,
  { modifierConfig, responsive: true },
);
