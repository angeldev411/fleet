import PropTypes from 'prop-types';
import styled from 'styled-components';
import { makeReactiveContainer, sizes } from 'reactive-container';
import { buildStyledComponent, px2rem, theme } from 'decisiv-ui-utils';

function getMinWidth(numberOfCards) {
  return (theme.dimensions.cardMinWidth * numberOfCards) + (theme.dimensions.cardGridPadding * 2);
}

export const CARD_GRID_BREAKPOINTS = [
  { name: sizes.SM, minWidth: getMinWidth(2) },
  { name: sizes.MD, minWidth: getMinWidth(3) },
  { name: sizes.LG, minWidth: getMinWidth(4) },
  { name: sizes.XL, minWidth: getMinWidth(5) },
];

const styles = props => `
  padding: ${px2rem(props.theme.dimensions.cardGridPadding)};
  box-sizing: border-box;
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    cardGridPadding: PropTypes.string.isRequired,
  }).isRequired,
};

const CardGrid = buildStyledComponent(
  'CardGrid',
  styled.div,
  styles,
  { themePropTypes },
);

export default makeReactiveContainer()(CardGrid);

