import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  applyStyleModifiers,
  styleModifierPropTypes,
} from 'styled-components-modifiers';
import {
  buildThemePropTypes,
  validateTheme,
} from 'styled-components-theme-validator';
import { px2rem } from 'decisiv-ui-utils';

import { withTestTheme } from 'utils/styles';

const COMPONENT_NAME = 'CardTable';

const TABLE_MODIFIER_CONFIG = {
  capitalize: () => ({ styles: 'text-transform: capitalize;' }),
  uppercase: () => ({ styles: 'text-transform: uppercase;' }),
};

const TH_MODIFIER_CONFIG = {
  extraWide: () => ({ styles: 'width: 7rem;' }),
  wide: () => ({ styles: `width: ${px2rem(75)};` }),
};

const TD_MODIFIER_CONFIG = {
  active: ({ theme }) => ({ styles: `color: ${theme.colors.base.link};` }),
  leftAlign: () => ({
    styles: 'text-align: left;',
  }),
  midGreyText: ({ theme }) => ({ styles: `color: ${theme.colors.base.textLight};` }),
};

const THEME_PROPTYPES = buildThemePropTypes({
  colors: PropTypes.shape({
    base: PropTypes.shape({
      link: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
});

const CardTable = styled.table`
  ${validateTheme(COMPONENT_NAME, THEME_PROPTYPES)}

  border-spacing: 0;
  font-size: ${px2rem(12)};
  width: 100%;
  ${applyStyleModifiers(TABLE_MODIFIER_CONFIG)}

  th {
    text-align: left;
    vertical-align: text-top;
    ${applyStyleModifiers(TH_MODIFIER_CONFIG, 'thModifiers')}
  }

  td {
    padding-left: ${px2rem(16)};
    text-align: right;
    vertical-align: text-top;
    ${applyStyleModifiers(TD_MODIFIER_CONFIG, 'tdModifiers')}
  }
`;

CardTable.propTypes = {
  modifiers: styleModifierPropTypes(TABLE_MODIFIER_CONFIG),
  tdModifiers: styleModifierPropTypes(TD_MODIFIER_CONFIG),
  thModifiers: styleModifierPropTypes(TH_MODIFIER_CONFIG),
};

CardTable.displayName = COMPONENT_NAME;

export default withTestTheme(CardTable);
