/*
 * This wrapper is built to surround the Dropdown component provided by `react-dropdown`.
 * The classnames used in the styling here are built into this library and are out of our
 * control, thus they must be used to style the component.
 *
 * Much of the styling here originates from the default styling of the component:
 * https://github.com/fraserxu/react-dropdown/blob/master/style.css
 */

import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

export function getSelectedStyles({ theme, value }) {
  if (!value) {
    return `
      font-style: italic;
      color: ${theme.colors.base.chrome500};
    `;
  }
  return undefined;
}

/* istanbul ignore next */
const styles = props => `
  display: inline-block;

  .Dropdown-root {
    position: relative;
  }

  .Dropdown-control {
    align-items: center;
    background-color: ${props.widgetBGColor};
    border-radius: 2px;
    border: 1px solid ${props.theme.colors.base.chrome200};
    box-sizing: border-box;
    color: ${props.widgetFGColor};
    cursor: pointer;
    display: flex;
    min-width: ${px2rem(150)};
    outline: none;
    overflow: hidden;
    padding: ${px2rem(4)} ${px2rem(4)} ${px2rem(4)} ${px2rem(9)};
    position: relative;
    transition: all 200ms ease;

    .fa {
      display: ${(props.showIcon ? 'inherit' : 'none')};
    }
  }

  .Dropdown-control:hover {
    box-shadow: 0 1px 0 ${props.theme.colors.base.shadow});
    .Dropdown-arrow {
      border-color: ${props.widgetFGColor} transparent transparent;
    }
  }

  .Dropdown-placeholder {
    ${getSelectedStyles(props)}
    position: relative;
  }

  .Dropdown-arrow {
    border-color: ${props.widgetFGColor} transparent transparent;
    border-style: solid;
    border-width: 5px 5px 0;
    display: block;
    height: 0;
    margin-left: auto;
    margin-right: ${px2rem(4)};
    width: 0;
  }

  .is-open .Dropdown-control:hover {
    .Dropdown-arrow {
      border-color: transparent transparent ${props.widgetFGColor};
    }
  }

  .is-open .Dropdown-arrow {
    border-color: transparent transparent ${props.widgetFGColor};
    border-width: 0 5px 5px;
  }

  .Dropdown-menu {
    -webkit-overflow-scrolling: touch;
    background-color: ${props.theme.colors.base.chrome100};
    border-top: none;
    border: 1px solid ${props.theme.colors.base.chrome200};
    box-sizing: border-box;
    margin-top: -1px;
    overflow-y: auto;
    position: absolute;
    top: 100%;
    width: 100%;
    z-index: 1000;
  }

  .Dropdown-menu .Dropdown-group > .Dropdown-title {
    font-weight: bold;
    padding: 8px 10px;
    text-transform: capitalize;
  }

  .Dropdown-option {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    padding: ${px2rem(9)};
  }

  .Dropdown-option:last-child {
    border-bottom-left-radius: 2px;
    border-bottom-right-radius: 2px;
  }

  .Dropdown-option:hover {
    background-color: ${props.theme.colors.base.chrome100};
    color: ${props.theme.colors.base.linkHover};
    text-decoration: underline;
  }

  .Dropdown-option.is-selected {
    background-color: ${props.theme.colors.base.linkHover};
    color: ${props.theme.colors.base.chrome100};
    text-decoration: none;
  }

  .Dropdown-noresults {
    box-sizing: border-box;
    cursor: default;
    display: block;
    padding: ${px2rem(4)};
  }

  .fa {
    margin-left: ${px2rem(5)};
  }

  .Dropdown-menu .fa {
    display: none;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      chrome500: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
      shadow: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  value: PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.node,
  }),
  widgetBGColor: PropTypes.string.isRequired,
  widgetFGColor: PropTypes.string.isRequired,
  showIcon: PropTypes.bool.isRequired,
};

const defaultProps = {
  value: undefined,
};

export default buildStyledComponent(
  'DropDownWrapper',
  styled.div,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
