import PropTypes from 'prop-types';
import React from 'react';
import UnstyledDropDown from 'react-dropdown';
import { theme } from 'decisiv-ui-utils';

import DropDownWrapper from './DropDownWrapper';

// TODO: Refactor this to use modifiers and stop passing color values down directly.
function DropDown({
  widgetBGColor,
  widgetFGColor,
  showIcon,
  ...props
}) {
  return (
    <DropDownWrapper
      value={props.value}
      widgetBGColor={widgetBGColor}
      widgetFGColor={widgetFGColor}
      showIcon={showIcon}
    >
      <UnstyledDropDown {...props} />
    </DropDownWrapper>
  );
}

DropDown.propTypes = {
  value: PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.node.isRequired,
  }),
  widgetBGColor: PropTypes.string,
  widgetFGColor: PropTypes.string,
  showIcon: PropTypes.bool,
};

DropDown.defaultProps = {
  value: undefined,
  widgetBGColor: theme.colors.base.background,
  widgetFGColor: theme.colors.base.chrome600,
  showIcon: false,
};

export default DropDown;
