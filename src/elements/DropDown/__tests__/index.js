import React from 'react';
import UnstyledDropDown from 'react-dropdown';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import DropDown from '../index';

const options = [
  { value: 'first', label: 'First' },
  { value: 'second', label: 'Second' },
];

const value = options[0];

const defaultProps = {
  options,
  value,
};

function shallowRender(props = defaultProps) {
  return shallow(<DropDown {...props} />);
}

test('renders a DropDownWrapper with the expected props', () => {
  const component = shallowRender({ ...defaultProps, showIcon: true });
  expect(component).toBeA('DropDownWrapper');
  expect(component).toHaveProps({ value, showIcon: true });
});

test('renders an UnstyledDropDown with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain(UnstyledDropDown);
  expect(component.find(UnstyledDropDown)).toHaveProps(defaultProps);
});
