import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import { getSelectedStyles } from '../DropDownWrapper';

/* --------------------- getSelectedStyles() --------------------------- */

const theme = {
  colors: {
    base: {
      chrome500: 'mid-grey',
    },
  },
};

test('getSelectedStyles returns the expected style string with no value', () => {
  const styleString = getSelectedStyles({ theme });
  expect(styleString).toContain('font-style: italic;');
  expect(styleString).toContain('color: mid-grey;');
});

test('getSelectedStyles returns undefined if a value is passed in', () => {
  const styleString = getSelectedStyles({ value: 'selected' });
  expect(styleString).toEqual(undefined);
});
