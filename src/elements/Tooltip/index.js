import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { compact } from 'lodash';

import Wrapper from './Wrapper';
import ContentWrapper from './ContentWrapper';
import TargetWrapper from './TargetWrapper';
import InsideContentWrapper from './InsideContentWrapper';
import Arrow from './Arrow';

/**
  * Creates a tooltip component, ie '<Tooltip>{children}</Tooltip>'
  * @prop {position} Position of tooltip to target; must be one of ['left','top','right','bottom']
  * @prop {message} The formatted message to show in the tooltip
  * @prop {children} The target of where the hover will be shown
  * @return {String} The tooltip component
*/
export class Tooltip extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    position: PropTypes.oneOf(['bottom', 'left', 'right', 'top']),
    message: PropTypes.shape().isRequired,
  };

  static defaultProps = { position: 'bottom' };

  state = { isTooltipShown: false };

  hide = () => {
    if (!this.state.isTooltipShown) return;
    this.setState({ isTooltipShown: false });
  };

  show = () => {
    if (this.state.isTooltipShown) return;
    this.setState({ isTooltipShown: true });
  };

  render() {
    const { children, message, position } = this.props;
    const { isTooltipShown } = this.state;

    return (
      <Wrapper
        onMouseLeave={this.hide}
        onMouseEnter={this.show}
      >
        <TargetWrapper>
          {children}
        </TargetWrapper>
        <ContentWrapper
          modifiers={compact([
            !!isTooltipShown && 'active',
            position,
          ])}
        >
          <Arrow modifiers={[position]} />
          <InsideContentWrapper>
            <FormattedMessage {...message} />
          </InsideContentWrapper>
        </ContentWrapper>
      </Wrapper>
    );
  }
}

Tooltip.displayName = 'Tooltip';

export default Tooltip;
