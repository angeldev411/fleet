import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const styles = () => `
  flex: 1;
  cursor: pointer;
`;

export default buildStyledComponent(
  'TargetWrapper',
  styled.div,
  styles,
);
