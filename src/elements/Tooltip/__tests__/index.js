import React from 'react';
import { test, expect, shallow, createSpy } from '__tests__/helpers/test-setup';

import Tooltip from '../index';

const children = (<div>This is some targetted div.</div>);

const message = {
  id: 'test.tooltip',
  defaultMessage: 'tooltip',
};

const defaultProps = {
  children,
  message,
};

function buildTooltip(props = defaultProps) {
  return shallow(
    <Tooltip {...props} />,
  );
}

test('Renders a Tooltip.Wrapper', () => {
  expect(buildTooltip()).toHaveProp('className', 'Tooltip.Wrapper');
});

test('Render TargetWrapper with the children', () => {
  const component = buildTooltip();
  expect(component).toContain('TargetWrapper');
  expect(component.find('TargetWrapper').html()).toContain('This is some targetted div.');
});

test('Renders a ContentWrapper with correct modifiers', () => {
  const component = buildTooltip({ ...defaultProps, position: 'top' });
  const instance = component.instance();
  instance.show();
  expect(component.find('ContentWrapper').props().modifiers).toContain('top');
  expect(component.find('ContentWrapper').props().modifiers).toContain('active');
  instance.hide();
  expect(component.find('ContentWrapper').props().modifiers).toNotContain('active');
});

test('show function should set state if isTooltipShown is false', () => {
  const setState = createSpy();
  const component = buildTooltip();
  const instance = component.instance();
  instance.setState = setState;
  instance.show();
  expect(setState).toHaveBeenCalled();
});

test('show function should not set state if isTooltipShown is true', () => {
  const setState = createSpy();
  const component = buildTooltip();
  component.setState({ isTooltipShown: true });
  const instance = component.instance();
  instance.setState = setState;
  instance.show();
  expect(setState).toNotHaveBeenCalled();
});

test('hide function should set state if isTooltipShown is true', () => {
  const setState = createSpy();
  const component = buildTooltip();
  component.setState({ isTooltipShown: true });
  const instance = component.instance();
  instance.setState = setState;
  instance.hide();
  expect(setState).toHaveBeenCalled();
});

test('hide function should not set state if isTooltipShown is false', () => {
  const setState = createSpy();
  const component = buildTooltip();
  const instance = component.instance();
  instance.setState = setState;
  instance.hide();
  expect(setState).toNotHaveBeenCalled();
});
