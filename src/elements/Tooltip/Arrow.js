import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  bottom: () => ({
    styles: `
      transform: translateX(50%) translateY(50%) rotate(-45deg) scale(1.3);
      right: 50%;
      top: 0;
      margin-top: 0;
    `,
  }),
  left: () => ({
    styles: `
      transform: translateX(-50%) translateY(50%) rotate(45deg) scale(1.3);
      bottom: 50%;
      right: 0;
      margin-right: 0;
    `,
  }),
  right: () => ({
    styles: `
      transform: translateX(50%) translateY(50%) rotate(-135deg) scale(1.3);
      bottom: 50%;
      left: 0;
      margin-left: 0;
    `,
  }),
  top: () => ({
    styles: `
      transform: translateX(50%) translateY(-50%) rotate(135deg) scale(1.3);
      right: 50%;
      bottom: 0;
      margin-bottom: 0;
    `,
  }),
};

/* istanbul ignore next */
const styles = ({ theme: { colors, dimensions } }) => `
  background-color: ${colors.base.chrome700};
  height: ${dimensions.tooltipArrowSize};
  position: absolute;
  transition: all 0.3s ease 0ms;
  width: ${dimensions.tooltipArrowSize};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome700: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    tooltipArrowSize: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Arrow',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
