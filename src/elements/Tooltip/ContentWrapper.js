import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  active: () => ({
    styles: `
      transition: opacity 0.5s;
      opacity: 1;
    `,
  }),
  bottom: ({ theme }) => ({
    styles: `
      transform: translateX(50%) translateY(0);
      padding-top: ${theme.dimensions.tooltipArrowSize};
      top: 80%;
      right: 50%;
    `,
  }),
  left: ({ theme }) => ({
    styles: `
      transform: translateX(0) translateY(50%);
      padding-right: ${theme.dimensions.tooltipArrowSize};
      right: 80%;
      bottom: 50%;
    `,
  }),
  right: ({ theme }) => ({
    styles: `
      transform: translateX(0) translateY(50%);
      padding-left: ${theme.dimensions.tooltipArrowSize};
      left: 80%;
      bottom: 50%;
    `,
  }),
  top: ({ theme }) => ({
    styles: `
      transform: translateX(50%) translateY(0);
      padding-bottom: ${theme.dimensions.tooltipArrowSize};
      bottom: 80%;
      right: 50%;
    `,
  }),
};

/* istanbul ignore next */
const styles = ({ theme: { colors } }) => `
  filter: drop-shadow(0 3px 7px ${colors.base.shadow});
  flex-flow: column nowrap;
  position: absolute;
  z-index: 10;
  opacity: 0;
  pointer-events: none;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      shadow: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    tooltipArrowSize: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'ContentWrapper',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
