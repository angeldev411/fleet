import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const styles = `
  position: relative;
  box-sizing: border-box;
`;

export default buildStyledComponent(
  'Tooltip.Wrapper',
  styled.div,
  styles,
);
