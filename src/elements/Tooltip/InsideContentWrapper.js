import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = ({ theme: { colors, fonts } }) => `
  background-color: ${colors.base.chrome700};
  border-radius: ${px2rem(2)};
  color: ${colors.base.chrome000};
  font-size: ${px2rem(12)};
  line-height: ${px2rem(22)};
  padding: ${px2rem(2)} ${px2rem(20)};
  position: relative;
  white-space: nowrap;
  font-family: ${fonts.primary}
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome000: PropTypes.string.isRequired,
      chrome700: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  fonts: PropTypes.shape({
    primary: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'InsideContentWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
