import { test, expect } from '__tests__/helpers/test-setup';

import { getExpandedStyles } from '../SelectorButtonAction';

const theme = {
  colors: {
    base: {
      linkHover: 'linkHover',
    },
  },
};

test('getExpandedStyles returns empty string if expanded = false', () => {
  expect(getExpandedStyles({
    expanded: false,
    theme,
  })).toEqual('');
});

test(
  'getExpandedStyles returns a string with text-decoration and color styles if expanded = true',
  () => {
    const styles = getExpandedStyles({ expanded: true, theme });
    expect(styles).toInclude('text-decoration: underline');
    expect(styles).toInclude('color: linkHover');
  },
);
