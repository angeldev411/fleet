import styled from 'styled-components';

const SelectorOptionsList = styled.ul`
  margin: 0;
  padding: 0;
`;

export default SelectorOptionsList;
