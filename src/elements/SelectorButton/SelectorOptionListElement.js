import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  selected: ({ theme }) => ({
    styles: `
      font-weight: bold;
      color: ${theme.colors.base.linkHover};
    `,
  }),
  small: () => ({
    styles: `
      font-size: ${px2rem(12)};
    `,
  }),
};

/* istanbul ignore next */
const styles = ({ theme }) => `
  color: ${theme.colors.base.textLight};
  cursor: pointer;
  font-weight: normal;
  list-style-type: none;
  padding: ${px2rem(8)};
  text-transform: capitalize;
  &:hover {
    color: ${theme.colors.base.linkHover};
    text-decoration: underline;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      linkHover: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'SelectorOptionListElement',
  styled.li,
  styles,
  { modifierConfig, themePropTypes },
);
