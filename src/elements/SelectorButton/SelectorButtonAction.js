import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

export function getExpandedStyles({ expanded, theme }) {
  if (expanded) {
    return `
      color: ${theme.colors.base.linkHover};
      text-decoration: underline;
    `;
  }
  return '';
}

/* istanbul ignore next */
const styles = props => `
  ${getExpandedStyles(props)}
  cursor: pointer;
  font-weight: 500;
  padding-left: ${px2rem(8)};
  text-transform: capitalize;
  display: flex;
  flex: 1;
  align-items: center;

  &:hover {
    color: ${props.theme.colors.base.linkHover};
    text-decoration: underline;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      linkHover: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'SelectorButtonAction',
  styled.div,
  styles,
  { themePropTypes },
);
