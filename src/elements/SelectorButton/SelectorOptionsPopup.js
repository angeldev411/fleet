import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import { Card } from 'elements/Card';

/* istanbul ignore next */
const modifierConfig = {
  noPad: () => ({
    styles: `
      padding: 0;
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.background};
  min-height: 0;
  min-width: ${px2rem(props.minWidth)};
  padding: 0 ${px2rem(8)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  minWidth: PropTypes.string,
};

const defaultProps = {
  minWidth: px2rem(180),
};

export default buildStyledComponent(
  'SelectorOptionsPopup',
  styled(Card),
  styles,
  { defaultProps, modifierConfig, propTypes, themePropTypes },
);
