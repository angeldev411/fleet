import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  align-items: center;
  background-color: ${props.theme.colors.base.chrome100};
  border-radius: 2px;
  border: 1px solid ${props.theme.colors.base.chrome200};
  color: ${props.theme.colors.base.textLight};
  display: flex;
  font-size: ${px2rem(12)};
  height: ${px2rem(25)};
  min-width: ${px2rem(130)};
  padding-left: ${px2rem(8)};
  position: relative;
  text-align: left;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'SelectorButton',
  styled.div,
  styles,
  { themePropTypes },
);
