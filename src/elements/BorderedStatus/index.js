import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  closed: ({ theme }) => ({
    styles: `
      color: ${theme.colors.status.success};
    `,
  }),
  open: ({ theme }) => ({
    styles: `
      color: ${theme.colors.base.text};
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.background};
  border: 1px solid ${props.theme.colors.base.chrome200};
  border-radius: 2px;
  font-size: ${px2rem(10)};
  font-weight: 500;
  padding: 0 ${px2rem(5)};
  text-transform: capitalize;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      success: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'BorderedStatus',
  styled.span,
  styles,
  { modifierConfig, themePropTypes },
);
