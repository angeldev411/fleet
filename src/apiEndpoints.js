export const apiBaseRoute = process.env.API_BASE_URL;

export default {
  //---------------------------------------------------------------------------

  // /api/assets GET
  assets: () =>
    `${apiBaseRoute}/assets`,

  // /api/assets/{assetId} GET PATCH
  asset: ({ assetId }) =>
    `${apiBaseRoute}/assets/${assetId}`,

  // /api/assets/{assetId}/scheduled_maintenances GET PATCH
  assetScheduledMaintenances: ({ assetId }) =>
    `${apiBaseRoute}/assets/${assetId}/scheduled_maintenances`,

  // /api/assets/{assetId}/cases GET
  assetCases: ({ assetId }) =>
    `${apiBaseRoute}/assets/${assetId}/cases`,

  // /api/assets/{assetId}/warranty GET
  assetWarranty: ({ assetId }) =>
    `${apiBaseRoute}/assets/${assetId}/warranty`,

  // /api/assets/{assetId}/faults GET
  assetFaults: ({ assetId }) =>
    `${apiBaseRoute}/assets/${assetId}/faults`,

  //---------------------------------------------------------------------------

  // /api/cases GET POST
  cases: () =>
    `${apiBaseRoute}/cases`,

  // /api/cases/{caseId} GET
  case: ({ caseId }) =>
    `${apiBaseRoute}/cases/${caseId}`,

  // /api/cases/{caseId}/versions/{versionId} PATCH
  caseVersion: ({ caseId, versionId }) =>
    `${apiBaseRoute}/cases/${caseId}/versions/${versionId}`,

  // /api/cases/{caseId}/recipients GET
  caseRecipients: ({ caseId }) =>
    `${apiBaseRoute}/cases/${caseId}/recipients`,

  // /api/cases/{caseId}/notes GET POST
  caseNotes: ({ caseId }) =>
    `${apiBaseRoute}/cases/${caseId}/notes`,

  // /api/cases/{caseId}/notes/{noteId} GET PATCH
  caseNote: ({ caseId, noteId }) =>
    `${apiBaseRoute}/cases/${caseId}/notes/${noteId}`,

  // /api/cases/{caseId}/faults GET POST
  caseFaults: ({ caseId }) =>
    `${apiBaseRoute}/cases/${caseId}/faults`,

  //---------------------------------------------------------------------------

  // /api/messages/status GET
  messagesStatus: () =>
    `${apiBaseRoute}/messages/status`,

  // /api/profile GET POST
  profile: () =>
    `${apiBaseRoute}/profile`,

  // /api/password POST
  password: () =>
    `${apiBaseRoute}/password`,

  // /api/password/reset POST
  passwordReset: () =>
    `${apiBaseRoute}/password/reset`,

  // /api/dashboard GET
  dashboard: () =>
    `${apiBaseRoute}/dashboard`,

  //---------------------------------------------------------------------------

  // /api/service_providers GET
  serviceProviders: () =>
    `${apiBaseRoute}/service_providers`,

  // /api/service_providers/{serviceProviderId} GET
  serviceProvider: ({ serviceProviderId }) =>
    `${apiBaseRoute}/service_providers/${serviceProviderId}`,

  //---------------------------------------------------------------------------

  // /api/service_requests GET
  serviceRequests: () =>
    `${apiBaseRoute}/service_requests`,

  // /api/service_requests/{serviceRequestId} GET PATCH
  serviceRequest: ({ serviceRequestId }) =>
    `${apiBaseRoute}/service_requests/${serviceRequestId}`,

  //---------------------------------------------------------------------------

  // /api/data_lists GET
  dataLists: () =>
    `${apiBaseRoute}/data_lists`,

  //---------------------------------------------------------------------------

  login: oem =>
    `${apiBaseRoute}/login/${oem}`,

  refreshToken: oem =>
    `${apiBaseRoute}/refresh_token/${oem}`,
};
