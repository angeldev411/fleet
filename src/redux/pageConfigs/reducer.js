import { fromJS } from 'immutable';
import { handleActions } from 'redux-actions';

// eslint-disable-next-line import/no-unresolved
import runtimeConfig from 'runtimeConfig.json';

import {
  LOGOUT,
  SET_CURRENT_PAGE_CONFIG_KEY,
} from '../constants';

export function loadInitialState(source = runtimeConfig) {
  const baseState = {
    currentPageConfigKey: null,
  };
  if (!source.page_configs) {
    console.error('CONFIG ERROR: no `page_configs` key found in runtime configuration');
    return fromJS(baseState);
  }
  return fromJS({
    ...baseState,
    ...source.page_configs,
  });
}

function resetPageConfig() {
  return loadInitialState();
}

export function setCurrentPageConfigKeyHandler(state, { payload }) {
  return state
    .set('currentPageConfigKey', payload || null);
}

export default handleActions(
  {
    [LOGOUT]: resetPageConfig,
    [SET_CURRENT_PAGE_CONFIG_KEY]: setCurrentPageConfigKeyHandler,
  },
  loadInitialState(),
);
