import { createAction } from 'redux-actions';

import { SET_CURRENT_PAGE_CONFIG_KEY } from '../constants';

// eslint-disable-next-line import/prefer-default-export
export const setCurrentPageConfigKey = createAction(SET_CURRENT_PAGE_CONFIG_KEY);
