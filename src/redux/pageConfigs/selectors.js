import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const pageConfigStoreSelector = state => state.get('pageConfigs');

export const currentPageConfigKeySelector = createSelector(
  pageConfigStoreSelector,
  pageConfigStore => pageConfigStore.get('currentPageConfigKey'),
);

export const currentPageConfigSelector = createSelector(
  pageConfigStoreSelector,
  currentPageConfigKeySelector,
  (
    pageConfigStore,
    currentPageConfigKey,
  ) => (currentPageConfigKey && pageConfigStore.get(currentPageConfigKey)) || Map(),
);

export const getPageConfigByKey = (state, configKey) =>
  pageConfigStoreSelector(state).get(configKey) || Map();

export const makeGetPageConfigByKey = () => createSelector(
  getPageConfigByKey,
  pageConfig => pageConfig,
);
