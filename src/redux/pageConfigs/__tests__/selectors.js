import { fromJS, Map } from 'immutable';

import { test, expect } from '__tests__/helpers/test-setup';

import {
  currentPageConfigKeySelector,
  currentPageConfigSelector,
  pageConfigStoreSelector,
  getPageConfigByKey,
} from '../selectors';

const currentPageConfigKey = 'ASSET_DETAIL';

const ASSET_DETAIL = { fixed: [] };

const pageConfigs = fromJS({ ASSET_DETAIL });

// ------------------------- pageConfigStoreSelector -------------------------

test('pageConfigStoreSelector returns the ui slice from the state', () => {
  const state = fromJS({ pageConfigs });
  expect(pageConfigStoreSelector(state)).toEqual(pageConfigs);
});

// ------------------------- currentPageConfigKeySelector -------------------------

test('currentPageConfigKeySelector returns the expected data from state', () => {
  const state = fromJS({ pageConfigs: { currentPageConfigKey } });
  expect(currentPageConfigKeySelector(state)).toEqual(currentPageConfigKey);
});

// ------------------------- currentPageConfigSelector -------------------------

test('currentPageConfigSelector returns the expected data from state', () => {
  const state = fromJS({ pageConfigs: { currentPageConfigKey, ASSET_DETAIL } });
  expect(currentPageConfigSelector(state).toJS()).toEqual(ASSET_DETAIL);
});

test('currentPageConfigSelector returns empty Map if currentPageConfigKey is null', () => {
  const state = fromJS({ pageConfigs: { currentPageConfigKey: null, ASSET_DETAIL } });
  expect(currentPageConfigSelector(state).toJS()).toEqual({});
});

test('currentPageConfigSelector returns empty Map if no config is found to match key', () => {
  const state = fromJS({ pageConfigs: { currentPageConfigKey: 'NO_MATCH', ASSET_DETAIL } });
  expect(currentPageConfigSelector(state).toJS()).toEqual({});
});

// ------------------------- getPageConfigByKey -------------------------

test('getPageConfigByKey returns the data matching the configKey', () => {
  const state = fromJS({ pageConfigs });
  expect(getPageConfigByKey(state, 'ASSET_DETAIL').toJS()).toEqual(ASSET_DETAIL);
});

test('getPageConfigByKey return empty Map if no configKey matches', () => {
  const state = fromJS({ pageConfigs });
  expect(getPageConfigByKey(state, 'NO_MATCH')).toEqual(Map());
});
