import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import * as actions from '../actions';
import reducer, { loadInitialState } from '../reducer';

const initialState = loadInitialState();

test('dispatching a setCurrentPageConfigKey action with a payload updates state', () => {
  const testConfigKey = 'TEST_PAGE';
  const action = actions.setCurrentPageConfigKey(testConfigKey);
  const newState = reducer(initialState, action);
  expect(newState.get('currentPageConfigKey')).toEqual(testConfigKey);
});

test('dispatching empty setCurrentPageConfigKey action sets currentPageConfigKey to null', () => {
  const testState = initialState.set('currentPageConfigKey', 'TEST_PAGE');
  const action = actions.setCurrentPageConfigKey();
  const newState = reducer(testState, action);
  expect(newState.get('currentPageConfigKey')).toEqual(null);
});
