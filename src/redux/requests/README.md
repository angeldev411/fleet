# Redux Request Actions
**Managing requests with less headaches (and typing).**

This document outlines how to create api requests that are managed within the `requests` slice of the redux store, and some of the advantages of the new management system.

## How it works

1. Creating a request action:

  Request actions are created with the `createRequestAction` helper. This helper is called with two arguments, `requestType` and `request`. `requestType` is the action type that will be dispatched with a successful response payload, and the key a failed request's response will be stored under. `request` is a function that accepts the request's payload and returns a promise that encapsulates the network request.

2. Queueing a request:

  This is done by dispatching a request action the same way as all other action dispatches, but is processed much differently. Dispatching this action will add a request action to `state.requests.queue`.

3. Processing the request:

  A subscriber now monitors the redux store for queued requests, performs deduping logic to ensure we don't have multiple queued or pending requests active with the same endpoint and details within a 16ms(ish) window, and batch processes those requests.

4. Handling successful responses:

  When a request is successful, an action will be dispatched with the response as the payload and a type that was the `requestType` in the `createRequestAction` helper. This can be used to trigger sagas for data normalization or an update to the redux store.

5. Handling failed responses:

  When a request fails, an error action is generated and added to `state.requests.failed`. The key that the error action is kept under is the `requestType` from the `createRequestAction` helper. The original payload is kept in the meta of that request, and the payload contains the response body.

## How do I...

  - **create a request action?**

    ```
    import { createRequestAction } from 'utils/redux-request-actions';

    import {
      YOUR_REQUEST_TYPE,
    } from '../constants';

    import {
      yourApiCallThatReturnsAPromise,
    } from './api';

    export const yourRequestAction = createRequestAction(YOUR_REQUEST_TYPE, yourApiCallThatReturnsAPromise);
    ```

  - **handle sending the same request from multiple compositions?**

    Just dispatch the actions! This request management system has basic deduping functionality built in. While processing the queue, the subscriber will block the processing of any requests that are exact duplicates of already queued or pending requests (based on comparing both the requestType and the payload).

    If you have multiple components that could be rendered at the same time and need the same data, but you want them to be fully independent and able to trigger a request, just include a bit of logic in your component to ensure you don't trigger a request for data you already have.

  - **access error data?**

    Use a selector to retrieve the data from the failed section of the redux store. A failed request for `YOUR_REQUEST_TYPE` will be stored at `requests.failed[YOUR_REQUEST_TYPE]`.
