import { fromJS } from 'immutable';
import { handleActions } from 'redux-actions';

import {
  QUEUE_REQUEST,
  ADD_PENDING_REQUEST,
  ADD_FAILED_REQUEST,
  FINALIZE_REQUEST,
  CLEAR_FAILED_REQUEST,
  CLEAR_QUEUED_REQUEST,
} from '../constants';

export const initialState = fromJS({
  queue: [],
  pending: [],
  failed: {},
});

/**
 * removeActionFromList is called in several of the action handlers. It creates a new Immutable
 * List with all elements except for the matching request(s). In the case of multiple duplicates,
 * it removes only the first instance.
 */
function removeActionFromList(state, key, action) {
  const list = state.get(key);
  const index = list.findIndex(request =>
    request.meta.requestType === action.meta.requestType &&
    request.payload === action.payload,
  );
  // If no matching request is found the index will be -1.
  if (index < 0) {
    return list;
  }
  return list.delete(index);
}

/**
 * queueRequestHandler is the actual action dispatched wherever an api request is triggered. Its
 * purpose is to add the request to a queue, which is then efficiently batch processed later.
 */
function queueRequestHandler(state, action) {
  return state
    .set('queue', state.get('queue').concat(action));
}

/**
 * addPendingRequestHandler is called after some deduping logic is performed. This action removes
 * a request from the queue, adds it to pending, and (if the request has previously failed)
 * removes the failure from the store.
 */
function addPendingRequestHandler(state, action) {
  return state
    .set('queue', removeActionFromList(state, 'queue', action))
    .set('pending', state.get('pending').concat(action))
    .set('failed', state.get('failed').delete(action.meta.requestType));
}

/**
 * addFailedRequestHandler is called in the event of a request failing. It saves the action under
 * the key of the requestType.
 */
function addFailedRequestHandler(state, action) {
  return state
    .set('failed', state.get('failed').set(action.meta.requestType, action));
}

/**
 * finalizeRequestHandler is called when a request has completed, regardless of success or failure.
 * It removes the request action from pending.
 */
function finalizeRequestHandler(state, action) {
  return state
    .set('pending', removeActionFromList(state, 'pending', action));
}

/**
 * clearFailedRequestHandler removes a failed request object from the store. It is not a part of
 * the normal request processing lifecycle, but a separate action that can be called.
 */
function clearFailedRequestHandler(state, action) {
  return state
    .set('failed', state.get('failed').delete(action.payload.requestType));
}

/**
 * clearQueuedRequestHandler is called in the event a duplicated request is found, before the
 * request is added to pending. This effectively cancels the request before it is submitted.
 */
function clearQueuedRequestHandler(state, action) {
  return state
    .set('queue', removeActionFromList(state, 'queue', action));
}

export default handleActions({
  [QUEUE_REQUEST]: queueRequestHandler,
  [ADD_PENDING_REQUEST]: addPendingRequestHandler,
  [ADD_FAILED_REQUEST]: addFailedRequestHandler,
  [FINALIZE_REQUEST]: finalizeRequestHandler,
  [CLEAR_FAILED_REQUEST]: clearFailedRequestHandler,
  [CLEAR_QUEUED_REQUEST]: clearQueuedRequestHandler,
}, initialState);
