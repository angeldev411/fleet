import { createAction } from 'redux-actions';

import {
  QUEUE_REQUEST,
} from '../constants';

// eslint-disable-next-line import/prefer-default-export
export const queueRequest = createAction(QUEUE_REQUEST);

