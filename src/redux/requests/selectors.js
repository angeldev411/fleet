import { curry } from 'lodash';
import { createSelector } from 'reselect';

import {
  CANCEL_SERVICE_REQUEST,
  LOAD_ASSET,
  LOAD_CASE,
  LOAD_SERVICE_REQUEST,
  LOAD_USER_PROFILE,
} from '../constants';

const requestsStoreSelector = state => state.get('requests');

export const queuedRequestsSelector = createSelector(
  requestsStoreSelector,
  requestsStore => requestsStore.get('queue'),
);

export const pendingRequestsSelector = createSelector(
  requestsStoreSelector,
  requestsStore => requestsStore.get('pending'),
);

/**
 * A factory function that creates a selector for the pending request of a given type
 */
const createPendingRequestSelectorFor = curry((baseSelector, REQUEST_TYPE) =>
  createSelector(
    baseSelector,
    pendingRequests => pendingRequests
      .some(pendingRequest => pendingRequest.meta.requestType === REQUEST_TYPE),
  ),
)(pendingRequestsSelector);

const failedRequestsSelector = createSelector(
  requestsStoreSelector,
  requestsStore => requestsStore.get('failed'),
);

/**
 * A factory function that creates a selector for the failed request of a given type
 */
const createFailedRequestSelectorFor = curry((baseSelector, REQUEST_TYPE) =>
  createSelector(
    baseSelector,
    failedRequests => failedRequests.get(REQUEST_TYPE),
  ),
)(failedRequestsSelector);

/* --------------------------- CANCEL_SERVICE_REQUEST --------------------------- */

export const cancelServiceRequestPendingSelector =
  createPendingRequestSelectorFor(CANCEL_SERVICE_REQUEST);

export const cancelServiceRequestErrorSelector =
  createFailedRequestSelectorFor(CANCEL_SERVICE_REQUEST);

/* --------------------------- LOAD_ASSET --------------------------- */

export const loadAssetPendingSelector =
  createPendingRequestSelectorFor(LOAD_ASSET);

export const loadAssetRequestErrorSelector =
  createFailedRequestSelectorFor(LOAD_ASSET);

/* --------------------------- LOAD_CASE --------------------------- */

export const loadCasePendingSelector =
  createPendingRequestSelectorFor(LOAD_CASE);

export const loadCaseRequestErrorSelector =
  createFailedRequestSelectorFor(LOAD_CASE);

/* --------------------------- LOAD_SERVICE_REQUEST --------------------------- */

export const loadServiceRequestPendingSelector =
  createPendingRequestSelectorFor(LOAD_SERVICE_REQUEST);

export const loadServiceRequestRequestErrorSelector =
  createFailedRequestSelectorFor(LOAD_SERVICE_REQUEST);

/* --------------------------- LOAD_USER_PROFILE --------------------------- */

export const loadUserProfileRequestErrorSelector =
  createFailedRequestSelectorFor(LOAD_USER_PROFILE);
