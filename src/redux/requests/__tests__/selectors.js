import { Map, List } from 'immutable';
import { noop } from 'lodash';

import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import * as selectors from '../selectors';

import {
  ADD_PENDING_REQUEST,
  CANCEL_SERVICE_REQUEST,
  LOAD_ASSET,
  LOAD_CASE,
  LOAD_SERVICE_REQUEST,
  LOAD_USER_PROFILE,
  QUEUE_REQUEST,
} from '../../constants';

const requestType = LOAD_USER_PROFILE;
const payload = { id: 3 };

const queuedRequestAction = {
  type: QUEUE_REQUEST,
  meta: {
    request: noop,
    requestType,
  },
  payload,
};

const pendingRequestAction = {
  type: ADD_PENDING_REQUEST,
  meta: {
    request: noop,
    requestType,
  },
  payload,
};

const state = Map({
  requests: Map({
    queue: List([queuedRequestAction]),
    pending: List([pendingRequestAction]),
    failed: Map({
      [LOAD_USER_PROFILE]: pendingRequestAction,
    }),
  }),
});

test('queuedRequestsSelector returns the queued requests', () => {
  expect(
    selectors.queuedRequestsSelector(state).toJS(),
  ).toEqual([queuedRequestAction]);
});

test('pendingRequestsSelector returns the pending requests', () => {
  expect(
    selectors.pendingRequestsSelector(state).toJS(),
  ).toEqual([pendingRequestAction]);
});

/* --------------------------- CANCEL_SERVICE_REQUEST --------------------------- */

test('cancelServiceRequestPendingSelector returns true if there is a pending cancelServiceRequest request', () => {
  const testRequestType = CANCEL_SERVICE_REQUEST;
  const testPendingRequestAction = {
    ...pendingRequestAction,
    meta: { requestType: testRequestType },
  };
  const testState = Map({
    requests: Map({
      queue: List([queuedRequestAction]),
      pending: List([testPendingRequestAction]),
      failed: Map(),
    }),
  });
  expect(selectors.cancelServiceRequestPendingSelector(testState)).toEqual(true);
});

test('cancelServiceRequestPendingSelector returns false if there is no pending cancelServiceRequest request', () => {
  const testState = Map({
    requests: Map({
      queue: List([queuedRequestAction]),
      pending: List([pendingRequestAction]),
      failed: Map(),
    }),
  });
  expect(selectors.cancelServiceRequestPendingSelector(testState)).toEqual(false);
});

test(
  'cancelServiceRequestErrorSelector returns error if failed request present',
  () => {
    const errorObj = { error: true };
    const testState = Map({
      requests: Map({
        queue: List([queuedRequestAction]),
        pending: List([pendingRequestAction]),
        failed: Map({ [CANCEL_SERVICE_REQUEST]: errorObj }),
      }),
    });
    expect(
      selectors.cancelServiceRequestErrorSelector(testState),
    ).toEqual(errorObj);
  },
);

test(
  'cancelServiceRequestErrorSelector returns undefined if no failed request present',
  () => {
    const testState = Map({
      requests: Map({
        queue: List([queuedRequestAction]),
        pending: List([pendingRequestAction]),
        failed: Map(),
      }),
    });
    expect(
      selectors.cancelServiceRequestErrorSelector(testState),
    ).toEqual(undefined);
  },
);

/* --------------------------- LOAD_ASSET --------------------------- */

test('loadAssetPendingSelector returns true if there is a pending loadAsset request', () => {
  const testRequestType = LOAD_ASSET;
  const testPendingRequestAction = {
    ...pendingRequestAction,
    meta: { requestType: testRequestType },
  };
  const testState = Map({
    requests: Map({
      queue: List([queuedRequestAction]),
      pending: List([testPendingRequestAction]),
      failed: Map(),
    }),
  });
  expect(selectors.loadAssetPendingSelector(testState)).toEqual(true);
});

test('loadAssetPendingSelector returns false if there is no pending loadAsset request', () => {
  const testState = Map({
    requests: Map({
      queue: List([queuedRequestAction]),
      pending: List([pendingRequestAction]),
      failed: Map(),
    }),
  });
  expect(selectors.loadAssetPendingSelector(testState)).toEqual(false);
});

test(
  'loadAssetRequestErrorSelector returns error if failed request present',
  () => {
    const errorObj = { error: true };
    const testState = Map({
      requests: Map({
        queue: List([queuedRequestAction]),
        pending: List([pendingRequestAction]),
        failed: Map({ [LOAD_ASSET]: errorObj }),
      }),
    });
    expect(
      selectors.loadAssetRequestErrorSelector(testState),
    ).toEqual(errorObj);
  },
);

test(
  'loadAssetRequestErrorSelector returns undefined if no failed request present',
  () => {
    const testState = Map({
      requests: Map({
        queue: List([queuedRequestAction]),
        pending: List([pendingRequestAction]),
        failed: Map(),
      }),
    });
    expect(
      selectors.loadAssetRequestErrorSelector(testState),
    ).toEqual(undefined);
  },
);

/* --------------------------- LOAD_CASE --------------------------- */

test('loadCasePendingSelector returns true if there is a pending loadCase request', () => {
  const testRequestType = LOAD_CASE;
  const testPendingRequestAction = {
    ...pendingRequestAction,
    meta: { requestType: testRequestType },
  };
  const testState = Map({
    requests: Map({
      queue: List([queuedRequestAction]),
      pending: List([testPendingRequestAction]),
      failed: Map(),
    }),
  });
  expect(selectors.loadCasePendingSelector(testState)).toEqual(true);
});

test('loadCasePendingSelector returns false if there is no pending loadCase request', () => {
  const testState = Map({
    requests: Map({
      queue: List([queuedRequestAction]),
      pending: List([pendingRequestAction]),
      failed: Map(),
    }),
  });
  expect(selectors.loadCasePendingSelector(testState)).toEqual(false);
});

test(
  'loadCaseRequestErrorSelector returns error if failed request present',
  () => {
    const errorObj = { error: true };
    const testState = Map({
      requests: Map({
        queue: List([queuedRequestAction]),
        pending: List([pendingRequestAction]),
        failed: Map({ [LOAD_CASE]: errorObj }),
      }),
    });
    expect(
      selectors.loadCaseRequestErrorSelector(testState),
    ).toEqual(errorObj);
  },
);

test(
  'loadCaseRequestErrorSelector returns undefined if no failed request present',
  () => {
    const testState = Map({
      requests: Map({
        queue: List([queuedRequestAction]),
        pending: List([pendingRequestAction]),
        failed: Map(),
      }),
    });
    expect(
      selectors.loadCaseRequestErrorSelector(testState),
    ).toEqual(undefined);
  },
);

/* --------------------------- LOAD_SERVICE_REQUEST --------------------------- */

test('loadServiceRequestPendingSelector returns true if there is a pending loadServiceRequest request', () => {
  const testRequestType = LOAD_SERVICE_REQUEST;
  const testPendingRequestAction = {
    ...pendingRequestAction,
    meta: { requestType: testRequestType },
  };
  const testState = Map({
    requests: Map({
      queue: List([queuedRequestAction]),
      pending: List([testPendingRequestAction]),
      failed: Map(),
    }),
  });
  expect(selectors.loadServiceRequestPendingSelector(testState)).toEqual(true);
});

test('loadServiceRequestPendingSelector returns false if there is no pending loadServiceRequest request', () => {
  const testState = Map({
    requests: Map({
      queue: List([queuedRequestAction]),
      pending: List([pendingRequestAction]),
      failed: Map(),
    }),
  });
  expect(selectors.loadServiceRequestPendingSelector(testState)).toEqual(false);
});

test(
  'loadServiceRequestRequestErrorSelector returns error if failed request present',
  () => {
    const errorObj = { error: true };
    const testState = Map({
      requests: Map({
        queue: List([queuedRequestAction]),
        pending: List([pendingRequestAction]),
        failed: Map({ [LOAD_SERVICE_REQUEST]: errorObj }),
      }),
    });
    expect(
      selectors.loadServiceRequestRequestErrorSelector(testState),
    ).toEqual(errorObj);
  },
);

test(
  'loadServiceRequestRequestErrorSelector returns undefined if no failed request present',
  () => {
    const testState = Map({
      requests: Map({
        queue: List([queuedRequestAction]),
        pending: List([pendingRequestAction]),
        failed: Map(),
      }),
    });
    expect(
      selectors.loadServiceRequestRequestErrorSelector(testState),
    ).toEqual(undefined);
  },
);

/* --------------------------- LOAD_USER_PROFILE --------------------------- */

test('loadUserProfileRequestErrorSelector returns the failed request action if present', () => {
  const errorObj = { error: true };
  const testState = Map({
    requests: Map({
      queue: List([queuedRequestAction]),
      pending: List([pendingRequestAction]),
      failed: Map({ [LOAD_USER_PROFILE]: errorObj }),
    }),
  });
  expect(
    selectors.loadUserProfileRequestErrorSelector(testState),
  ).toEqual(errorObj);
});

test(
  'loadUserProfileRequestErrorSelector returns undefined if no failed request present',
  () => {
    const testState = Map({
      requests: Map({
        queue: List([queuedRequestAction]),
        pending: List([pendingRequestAction]),
        failed: Map(),
      }),
    });
    expect(
      selectors.loadUserProfileRequestErrorSelector(testState),
    ).toEqual(undefined);
  },
);
