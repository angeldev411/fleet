import { createRequestAction } from 'utils/redux-request-actions';

import {
  getDataLists,
} from './api';

import {
  LOAD_DATA_LISTS,
} from '../constants';

// eslint-disable-next-line
export const loadDataLists = createRequestAction(LOAD_DATA_LISTS, getDataLists);
