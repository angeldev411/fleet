import { Map, fromJS } from 'immutable';
import { handleActions } from 'redux-actions';

import {
  LOAD_DATA_LISTS,
} from '../constants';

export const initialState = Map();

function loadDataListsSuccessHandler(state, action) {
  return fromJS(action.payload.body);
}

export default handleActions({
  [LOAD_DATA_LISTS]: loadDataListsSuccessHandler,
}, initialState);
