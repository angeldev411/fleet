import endpoints from 'apiEndpoints';

import {
  get,
} from 'utils/fetch';

// eslint-disable-next-line import/prefer-default-export
export function getDataLists() {
  return get(endpoints.dataLists());
}
