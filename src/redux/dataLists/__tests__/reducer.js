import { Map } from 'immutable';
import { test, expect } from '__tests__/helpers/test-setup';

import reducer, { initialState } from '../reducer';

import {
  LOAD_DATA_LISTS,
} from '../../constants';

test('action with type LOAD_DATA_LISTS sets data lists in state', () => {
  const dataLists = Map({
    searchFilters: [
      {
        category: 'network',
        options: [
          'Utility',
          'Volvo',
          'Wabash',
          'WheelTime',
          'Wingfoot',
        ],
      },
    ],
    reasonsForRepair: [
      {
        code: '00',
        description: 'Unknown - Unknown',
      },
      {
        code: '01',
        description: 'Maintenance - Breakdown',
      },
      {
        code: '02',
        description: 'Maintenance - Consumption, Fuel',
      },
    ],
  });

  const action = { type: LOAD_DATA_LISTS, payload: { body: dataLists } };
  const newState = reducer(initialState, action);

  expect(newState).toEqual(dataLists);
});
