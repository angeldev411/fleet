import { fromJS } from 'immutable';

import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import * as selectors from '../selectors';

const reasonsForRepair = [
  {
    code: '00',
    description: 'Unknown - Unknown',
  },
  {
    code: '01',
    description: 'Maintenance - Breakdown',
  },
  {
    code: '02',
    description: 'Maintenance - Consumption, Fuel',
  },
];

const complaints = [
  {
    code: '190',
    description: 'ABS, Light Stays On',
  },
  {
    code: '191',
    description: 'ABS, System Cycles Constantly',
  },
  {
    code: '192',
    description: 'ABS, System Does Not Self Check',
  },
];

const state = fromJS({
  dataLists: {
    searchFilters: [
      {
        category: 'network',
        options: [
          'Utility',
          'Volvo',
          'Wabash',
          'WheelTime',
          'Wingfoot',
        ],
      },
    ],
    reasonsForRepair,
    complaints,
  },
});

test('reasonsForRepairSelector returns the reasonsForRepair slice from the state', () => {
  expect(selectors.reasonsForRepairSelector(state)).toEqual(fromJS(reasonsForRepair));
});

test('complaintsSelector returns the complaints slice from the state', () => {
  expect(selectors.complaintsSelector(state)).toEqual(fromJS(complaints));
});
