import nock from 'nock';
import { test, expect } from '__tests__/helpers/test-setup';

import * as api from '../api';

const testDomain = process.env.API_BASE_URL;

test('getDataLists reaches the expected route and returns the response', async () => {
  const response = [{ response: 'success' }];
  const httpMock = nock(testDomain);
  httpMock.get('/data_lists')
    .query()
    .reply(200, response);

  const { response: httpResponse } = await api.getDataLists();

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});
