import { createSelector } from 'reselect';

const dataListsStoreSelector = state => state.get('dataLists');

export const reasonsForRepairSelector = createSelector(
  dataListsStoreSelector,
  dataListsStore => dataListsStore.get('reasonsForRepair'),
);

export const complaintsSelector = createSelector(
  dataListsStoreSelector,
  dataListsStore => dataListsStore.get('complaints'),
);
