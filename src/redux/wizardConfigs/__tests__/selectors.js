import { fromJS, Map } from 'immutable';

import { test, expect } from '__tests__/helpers/test-setup';

import {
  wizardConfigStoreSelector,
  getWizardConfigByKey,
} from '../selectors';

const SERVICE_REQUEST_WIZARD = {
  start: 'REVIEW',
  steps: {
    REVIEW: { component: () => {} },
  },
};

const wizardConfigs = fromJS({ SERVICE_REQUEST_WIZARD });

// ------------------------- wizardConfigStoreSelector -------------------------

test('wizardConfigStoreSelector returns the ui slice from the state', () => {
  const state = fromJS({ wizardConfigs });
  expect(wizardConfigStoreSelector(state)).toEqual(wizardConfigs);
});

// ------------------------- getWizardConfigByKey -------------------------

test('getWizardConfigByKey returns the data matching the configKey', () => {
  const state = fromJS({ wizardConfigs });
  expect(getWizardConfigByKey(state, 'SERVICE_REQUEST_WIZARD').toJS())
    .toEqual(SERVICE_REQUEST_WIZARD);
});

test('getWizardConfigByKey return empty Map if no configKey matches', () => {
  const state = fromJS({ wizardConfigs });
  expect(getWizardConfigByKey(state, 'NO_MATCH')).toEqual(Map());
});
