
import { test, expect, spyOn } from '__tests__/helpers/test-setup';

import { loadInitialState } from '../reducer';

test('loadInitialState logs a warning and returns an empty map with no wizard_configs', () => {
  const errorSpy = spyOn(console, 'warn');
  const initialState = loadInitialState({});
  expect(errorSpy).toHaveBeenCalled();
  expect(initialState.toJS()).toEqual({});
});

test('loadInitialState returns the wizard_configs', () => {
  const wizardConfigs = { test: 'values' };
  const initialState = loadInitialState({ wizard_configs: wizardConfigs });
  expect(initialState.toJS()).toEqual(wizardConfigs);
});
