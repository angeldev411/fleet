import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const wizardConfigStoreSelector = state => state.get('wizardConfigs');

export const getWizardConfigByKey = (state, configKey) =>
  wizardConfigStoreSelector(state).get(configKey) || Map();

export const makeGetWizardConfigByKey = () => createSelector(
  getWizardConfigByKey,
  wizardConfig => wizardConfig,
);
