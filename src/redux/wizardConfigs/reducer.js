import { fromJS } from 'immutable';
import { has } from 'lodash';
import { handleActions } from 'redux-actions';

// eslint-disable-next-line import/no-unresolved
import runtimeConfig from 'runtimeConfig.json';

export function loadInitialState(source = runtimeConfig) {
  if (!has(source, 'wizard_configs')) {
    console.warn('Warning: no `wizard_configs` key found in runtime configuration');
    return fromJS({});
  }
  return fromJS(source.wizard_configs);
}

export default handleActions(
  {},
  loadInitialState(),
);
