import { put, takeLatest } from 'redux-saga/effects';

import { requestFullScreenMode, requestPrintPage } from './actions';
import { HANDLE_ACTION_BAR_ITEM_CLICK } from '../constants';

function* handleActionBarItemClickWorker({ payload }) {
  if (payload.buttonId === 'OPTIONS_BUTTON') {
    switch (payload.dropdownItemId) {
      case 'viewInFullScreenMode':
        yield put(requestFullScreenMode(true));
        break;
      case 'printPage':
        yield put(requestPrintPage(true));
        break;
      default:
    }
  }
}

export function* handleActionBarItemClickWatcher() {
  yield takeLatest(HANDLE_ACTION_BAR_ITEM_CLICK, handleActionBarItemClickWorker);
}

export default [
  handleActionBarItemClickWatcher(),
];
