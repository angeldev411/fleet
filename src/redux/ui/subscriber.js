import {
  LOCALSTORAGE_APP_STATE_KEY,
} from '../constants';

import { uiStoreSelector } from './selectors';

function addSubscriber(store) {
  function onStateChange() {
    if (memoryDB && memoryDB.setItem) {
      const appState = uiStoreSelector(store.getState());
      const appStateAsString = JSON.stringify(appState);
      memoryDB.setItem(LOCALSTORAGE_APP_STATE_KEY, appStateAsString);
    }
  }

  return store.subscribe(onStateChange);
}

export default addSubscriber;
