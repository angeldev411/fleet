import { test, expect } from '__tests__/helpers/test-setup';
import { fromJS } from 'immutable';

import {
  uiStoreSelector,
  leftNavExpandedSelector,
  leftNavExpandedParentsSelector,
  componentsExpandedSelector,
  scrollToUnreadNoteSelector,
  requestPrintPageSelector,
} from '../selectors';

// ------------------------- uiStoreSelector -------------------------

test('uiStoreSelector returns the ui slice from the state', () => {
  const uiState = 'bits and bobs';
  const state = fromJS({ ui: uiState });
  expect(uiStoreSelector(state)).toEqual(uiState);
});

// ------------------------- leftNavExpandedSelector -------------------------

test('leftNavExpandedSelector returns the leftNavExpanded slice from the ui state', () => {
  const leftNavExpanded = 'is it expanded?';
  const state = fromJS({ ui: { leftNavExpanded } });
  expect(leftNavExpandedSelector(state)).toEqual(leftNavExpanded);
});

// ------------------------- leftNavExpandedParentsSelector -------------------------

test('leftNavExpandedParentsSelector returns the leftNavExpandedParents slice from the ui state',
  () => {
    const leftNavExpandedParents = 'all the expanded parents';
    const state = fromJS({ ui: { leftNavExpandedParents } });
    expect(leftNavExpandedParentsSelector(state)).toEqual(leftNavExpandedParents);
  });

// ------------------------- componentsExpandedSelector -------------------------
test('componentsExpandedSelector returns the componentsExpanded slice from the ui state', () => {
  const componentsExpanded = 'is it expanded?';
  const state = fromJS({ ui: { componentsExpanded } });
  expect(componentsExpandedSelector(state)).toEqual(componentsExpanded);
});

// ------------------------- componentsExpandedSelector -------------------------
test('scrollToUnreadNoteSelector returns a bool', () => {
  const state = fromJS({ ui: { scrollToUnreadNote: true } });
  expect(scrollToUnreadNoteSelector(state)).toEqual(true);
});

// ------------------------- requestPrintPageSelector -------------------------
test('requestPrintPageSelector returns a Map', () => {
  const state = fromJS({ ui: { requestPrintPage: false } });
  expect(requestPrintPageSelector(state)).toEqual(false);
});
