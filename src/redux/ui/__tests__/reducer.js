import { test, expect } from '__tests__/helpers/test-setup';
import { Map, Set } from 'immutable';

import { LOCALSTORAGE_APP_STATE_KEY } from '../../constants';

import {
  expandLeftNavParent,
  handleRequestPrintPage,
  handleRequestFullScreenMode,
  loadInitialState,
  setAssetsView,
  setCasesView,
  setComponentExpanded,
  setLeftNavExpanded,
  setServiceRequestsView,
  updateScrollToUnreadNote,
} from '../reducer';

// ------------------------- loadInitialState -------------------------

const nullStorage = {
  getItem: () => null,
};

test('loadInitialState defaults casesView to cards', () => {
  expect(loadInitialState(nullStorage).get('casesView')).toEqual('cards');
});

test('loadInitialState defaults assetsView to cards', () => {
  expect(loadInitialState(nullStorage).get('assetsView')).toEqual('cards');
});

test('loadInitialState defaults serviceRequestsView to cards', () => {
  expect(loadInitialState(nullStorage).get('serviceRequestsView')).toEqual('cards');
});

test('loadInitialState returns a Map', () => {
  expect(loadInitialState(nullStorage)).toBeA(Map);
});

test('loadInitialState defaults leftNavExpanded to true', () => {
  expect(loadInitialState(nullStorage).get('leftNavExpanded')).toEqual(true);
});

test('loadInitialState defaults leftNavExpandedParents to an empty Set', () => {
  const state = loadInitialState(nullStorage);
  expect(state.get('leftNavExpandedParents')).toBeA(Set);
  expect(state.get('leftNavExpandedParents').size).toEqual(0);
});

test('loadInitialState deserializes info from storage', () => {
  const state = {
    assetsView: 'cards',
    casesView: 'list',
    serviceRequestsView: 'cards',
    leftNavExpanded: false,
    leftNavExpandedParents: ['foo', 'bar'],
    requestPrintPage: false,
    requestFullScreenMode: false,
    componentsExpanded: {},
    actionBarItemClicked: { buttonId: '', dropdownItemId: '', time: null },
  };
  const serializedState = JSON.stringify(state);
  const mockStorage = {
    getItem: expect.createSpy().andReturn(serializedState),
  };
  const result = loadInitialState(mockStorage);
  expect(mockStorage.getItem).toHaveBeenCalledWith(LOCALSTORAGE_APP_STATE_KEY);
  expect(result.toJS()).toEqual(state);
});

test('loadInitialState defaults componentsExpanded to empty Map', () => {
  const state = loadInitialState(nullStorage);
  expect(state.get('componentsExpanded')).toBeA(Map);
  expect(state.get('componentsExpanded').size).toEqual(0);
});

// ---------------------- setComponentExpanded ---------------------
test('setComponentExpanded reducer sets expanded status of component', () => {
  const originalState = Map({ componentsExpanded: Map() });
  const newState = setComponentExpanded(originalState, { payload: { expandKey: 'testComponent', expanded: true } });
  expect(newState.getIn(['componentsExpanded', 'testComponent'])).toBe(true);
});

// ------------------------- expandLeftNav -------------------------

test('expandLeftNav reducer sets leftNavExpanded', () => {
  const originalState = Map({ leftNavExpanded: false });
  const newState = setLeftNavExpanded(originalState, { payload: true });
  expect(newState.get('leftNavExpanded')).toEqual(true);
});

// ------------------------- expandLeftNavParent -------------------------

test(
  'expandLeftNavParent removes the parent key from leftNavExpandedParents when collapsing',
  () => {
    const originalState = Map({ leftNavExpandedParents: Set(['wombat', 'platypus']) });
    const newState = expandLeftNavParent(
      originalState,
      { payload: { key: 'wombat', expanded: false } },
    );
    expect(newState.get('leftNavExpandedParents')).toEqual(Set(['platypus']));
  });

test(
  'expandLeftNavParent adds the parent key to leftNavExpandedParents when expanding',
  () => {
    const originalState = Map({ leftNavExpandedParents: Set(['rhino', 'hippo']) });
    const newState = expandLeftNavParent(
      originalState,
      { payload: { key: 'giraffe', expanded: true } },
    );
    expect(newState.get('leftNavExpandedParents')).toEqual(Set(['rhino', 'hippo', 'giraffe']));
  });

// ------------------------- handleRequestFullScreenMode -------------------------

test('handleRequestFullScreenMode sets value if provided', () => {
  const originalState = Map({ requestFullScreenMode: false });
  const newState = handleRequestFullScreenMode(
    originalState,
    { payload: true },
  );
  expect(newState.get('requestFullScreenMode')).toEqual(true);
});

test('handleRequestFullScreenMode sets value to false if payload empty', () => {
  const originalState = Map({ requestFullScreenMode: true });
  const newState = handleRequestFullScreenMode(
    originalState,
    {},
  );
  expect(newState.get('requestFullScreenMode')).toEqual(false);
});

// ------------------------- handleRequestPrintPage -------------------------

test('handleRequestPrintPage sets a value if provided', () => {
  const originalState = Map({ requestPrintPage: false });
  const newState = handleRequestPrintPage(
    originalState,
    { payload: true },
  );
  expect(newState.get('requestPrintPage')).toEqual(true);
});

test('handleRequestPrintPage sets value to false if payload empty', () => {
  const originalState = Map({ requestPrintPage: true });
  const newState = handleRequestPrintPage(
    originalState,
    {},
  );
  expect(newState.get('requestPrintPage')).toEqual(false);
});

// ------------------------- setCasesView -------------------------

test('setCasesView reducer sets casesView', () => {
  const originalState = Map({ casesView: 'cards' });
  const newState = setCasesView(originalState, { payload: 'list' });
  expect(newState.get('casesView')).toEqual('list');
});

// ------------------------- setAssetsView -------------------------

test('setAssetsView reducer sets assetsView', () => {
  const originalState = Map({ assetsView: 'cards' });
  const newState = setAssetsView(originalState, { payload: 'list' });
  expect(newState.get('assetsView')).toEqual('list');
});

// ------------------------- setServiceRequestsView -------------------------

test('setServiceRequestsView reducer sets serviceRequestsView', () => {
  const originalState = Map({ serviceRequestsView: 'cards' });
  const newState = setServiceRequestsView(originalState, { payload: 'list' });
  expect(newState.get('serviceRequestsView')).toEqual('list');
});

// ------------------------- updateScrollToUnreadNote -------------------------

test('updateScrollToUnreadNote reducer sets scrollToUnreadNote to bool', () => {
  const originalState = Map({ scrollToUnreadNote: false });
  const newState = updateScrollToUnreadNote(originalState, { payload: true });
  expect(newState.get('scrollToUnreadNote')).toEqual(true);
});
