import { fromJS } from 'immutable';
import SagaTester from 'redux-saga-tester';

import { test, expect } from '__tests__/helpers/test-setup';

import createReducer from 'setup/reducer';

import { HANDLE_ACTION_BAR_ITEM_CLICK } from '../../constants';

import * as sagas from '../sagas';

const initialState = fromJS({});
const reducers = createReducer();

test('requestFullScreenModeWorker', () => {
  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.handleActionBarItemClickWatcher);
  sagaTester.dispatch({
    type: HANDLE_ACTION_BAR_ITEM_CLICK,
    payload: {
      buttonId: 'OPTIONS_BUTTON',
      dropdownItemId: 'viewInFullScreenMode',
    },
  });
  const appState = sagaTester.store.getState();

  expect(appState.getIn(['ui', 'requestFullScreenMode'])).toEqual(true);
});

test('requestPrintPageWorker', () => {
  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.handleActionBarItemClickWatcher);
  sagaTester.dispatch({
    type: HANDLE_ACTION_BAR_ITEM_CLICK,
    payload: {
      buttonId: 'OPTIONS_BUTTON',
      dropdownItemId: 'printPage',
    },
  });
  const appState = sagaTester.store.getState();

  expect(appState.getIn(['ui', 'requestPrintPage'])).toEqual(true);
});
