import { createSelector } from 'reselect';

// For convenience, extract the ui section of the store.
const uiStoreSelector = state => state.get('ui');

const actionBarItemClickedSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('actionBarItemClicked'),
);

// determine the type of assets favorites view the user has set
const assetsViewSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('assetsView'),
);

// determine the type of cases favorites view the user has set
const casesViewSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('casesView'),
);

// get all the statuses of components if they are expanded
const componentsExpandedSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('componentsExpanded'),
);

// determine if the application should be rendered in full screen mode
const requestFullScreenModeSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('requestFullScreenMode'),
);

// determine if print page is requested
const requestPrintPageSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('requestPrintPage'),
);

// determine which (if any) left nav parents are currently expanded
const leftNavExpandedParentsSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('leftNavExpandedParents'),
);

// get the status of left nav panel if it is expanded.
const leftNavExpandedSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('leftNavExpanded'),
);

// determine if scrolling to unread note is necessary
const scrollToUnreadNoteSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('scrollToUnreadNote'),
);

// determine the type of serviceRequests favorites view the user has set
const serviceRequestsViewSelector = createSelector(
  uiStoreSelector,
  uiStore => uiStore.get('serviceRequestsView'),
);

export {
  actionBarItemClickedSelector,
  assetsViewSelector,
  casesViewSelector,
  componentsExpandedSelector,
  leftNavExpandedParentsSelector,
  leftNavExpandedSelector,
  requestPrintPageSelector,
  requestFullScreenModeSelector,
  scrollToUnreadNoteSelector,
  serviceRequestsViewSelector,
  uiStoreSelector,
};
