import { Map, Set } from 'immutable';
import { handleActions } from 'redux-actions';

import {
  EXPAND_LEFTNAV_PARENT,
  HANDLE_ACTION_BAR_ITEM_CLICK,
  LOCALSTORAGE_APP_STATE_KEY,
  REQUEST_PRINT_PAGE,
  REQUEST_FULL_SCREEN_MODE,
  SET_ASSETS_VIEW,
  SET_CASES_VIEW,
  SET_COMPONENT_EXPANDED,
  SET_LEFTNAV_EXPANDED,
  SET_SERVICE_REQUESTS_VIEW,
  UPDATE_SCROLL_TO_UNREAD_NOTE,
} from '../constants';

const defaultAppState = {
  // `actionBarItemClicked` has button id and dropdown item id for clicked button/dropdown
  actionBarItemClicked: {},

  // it indicates the users preference for rendering the assets on the favorites page
  assetsView: 'cards',

  // it indicates the users preference for rendering the cases on the favorites page
  casesView: 'cards',

  // `componentsExpanded` has all the statuses of registered components if they are expanded or not
  componentsExpanded: {},

  // `leftNavExpanded` is true if the overall left nav is expanded, false if collapsed
  leftNavExpanded: true,

  // `leftNavExpandedParents` is a Set of menu keys indicating which parent menus are
  // currently expanded
  leftNavExpandedParents: [],

  // `requestPrintPage` is true when the user requests print page open.
  requestPrintPage: false,

  // `requestFullScreenMode` is true when the user has opted to view the body in full screen.
  requestFullScreenMode: false,

  // it indicates the users preference for rendering the assets on the favorites page
  serviceRequestsView: 'cards',

  // 'scrollToUnreadNote' indicates if scrolling to unread note is needed
  scrollToUnreadNote: false,
};

/**
 * Attempt to load the initial app UI state from the localStorage.  Note that this is
 * purposefully using localStorage and not the memoryDB proxy because...
 *  * memoryDB is not available at app bootstrap
 *  * the only reason for this loadInitialState to exist is to re-hydrate the persisted
 *    app UI state; memoryDB is not persisted
 *  * if localStorage is not available (e.g. Safari Private Browsing) then getItem just
 *    returns null, so the defaults above will be used instead, and that's fine.
 */
export function loadInitialState(source = localStorage) {
  let appState =
    source && source.getItem && source.getItem(LOCALSTORAGE_APP_STATE_KEY);
  if (appState) {
    try {
      appState = JSON.parse(appState);

      // iterate over the default appstate, and add the default key/value if not currently present.
      Object.keys(defaultAppState).forEach((k) => {
        if (appState[k] === undefined) {
          appState[k] = defaultAppState[k];
        }
      });
    } catch (e) {
      console.error(
        `error parsing ${LOCALSTORAGE_APP_STATE_KEY} value from localStorage:`,
        e,
      );
      appState = null;
    }
  }

  if (!appState) {
    appState = defaultAppState;
  }

  return Map({
    actionBarItemClicked: Map({
      buttonId: '',
      dropdownItemId: '',
      time: null,
    }),
    assetsView: appState.assetsView,
    casesView: appState.casesView,
    componentsExpanded: Map(appState.componentsExpanded),
    requestPrintPage: appState.requestPrintPage,
    requestFullScreenMode: appState.requestFullScreenMode,
    leftNavExpanded: appState.leftNavExpanded,
    leftNavExpandedParents: Set(appState.leftNavExpandedParents),
    serviceRequestsView: appState.serviceRequestsView,
  });
}

export const expandLeftNavParent = (state, { payload: { key, expanded } }) => {
  const wasExpanded = state.get('leftNavExpandedParents');
  const isExpanded = expanded ? wasExpanded.add(key) : wasExpanded.delete(key);
  return state.set('leftNavExpandedParents', isExpanded);
};

export const handleActionBarItemClicked = (
  state,
  { payload: { buttonId, dropdownItemId } },
) =>
  state
    .set('actionBarItemClicked', new Map({ buttonId, dropdownItemId, time: Date.now() }));

export const handleRequestFullScreenMode = (
  state,
  { payload },
) =>
  state.set('requestFullScreenMode', payload || false);

export const handleRequestPrintPage = (
  state,
  { payload },
) =>
  state.set('requestPrintPage', payload || false);

export const setAssetsView = (
  state,
  { payload: assetsView = defaultAppState.assetsView },
) => state.set('assetsView', assetsView);

export const setCasesView = (
  state,
  { payload: casesView = defaultAppState.casesView },
) => state.set('casesView', casesView);

export const setComponentExpanded = (
  state,
  { payload: { expandKey, expanded } },
) =>
  state.updateIn(['componentsExpanded'], componentsExpanded =>
    componentsExpanded.set(expandKey, expanded),
  );

export const setLeftNavExpanded = (state, { payload: expanded }) =>
  state.set('leftNavExpanded', expanded);

export const setServiceRequestsView = (
  state,
  { payload: serviceRequestsView = defaultAppState.setServiceRequestsView },
) => state.set('serviceRequestsView', serviceRequestsView);


export const updateScrollToUnreadNote = (state, { payload: bool }) =>
  state.set('scrollToUnreadNote', bool);

export default handleActions(
  {
    [EXPAND_LEFTNAV_PARENT]: expandLeftNavParent,
    [HANDLE_ACTION_BAR_ITEM_CLICK]: handleActionBarItemClicked,
    [REQUEST_PRINT_PAGE]: handleRequestPrintPage,
    [REQUEST_FULL_SCREEN_MODE]: handleRequestFullScreenMode,
    [SET_ASSETS_VIEW]: setAssetsView,
    [SET_CASES_VIEW]: setCasesView,
    [SET_COMPONENT_EXPANDED]: setComponentExpanded,
    [SET_LEFTNAV_EXPANDED]: setLeftNavExpanded,
    [SET_SERVICE_REQUESTS_VIEW]: setServiceRequestsView,
    [UPDATE_SCROLL_TO_UNREAD_NOTE]: updateScrollToUnreadNote,
  },
  loadInitialState(),
);
