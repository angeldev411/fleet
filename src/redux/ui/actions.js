import { createAction } from 'redux-actions';

import {
  EXPAND_LEFTNAV_PARENT,
  HANDLE_ACTION_BAR_ITEM_CLICK,
  REQUEST_PRINT_PAGE,
  REQUEST_FULL_SCREEN_MODE,
  SET_ASSETS_VIEW,
  SET_CASES_VIEW,
  SET_COMPONENT_EXPANDED,
  SET_LEFTNAV_EXPANDED,
  SET_SERVICE_REQUESTS_VIEW,
  UPDATE_SCROLL_TO_UNREAD_NOTE,
} from '../constants';

export const expandLeftNavParent = createAction(EXPAND_LEFTNAV_PARENT);

export const handleActionBarItemClick = createAction(HANDLE_ACTION_BAR_ITEM_CLICK);

export const requestPrintPage = createAction(REQUEST_PRINT_PAGE);

export const requestFullScreenMode = createAction(REQUEST_FULL_SCREEN_MODE);

export const setAssetsView = createAction(SET_ASSETS_VIEW);

export const setCasesView = createAction(SET_CASES_VIEW);

export const setComponentExpanded = createAction(SET_COMPONENT_EXPANDED);

export const setLeftNavExpanded = createAction(SET_LEFTNAV_EXPANDED);

export const setServiceRequestsView = createAction(SET_SERVICE_REQUESTS_VIEW);

export const updateScrollToUnreadNote = createAction(UPDATE_SCROLL_TO_UNREAD_NOTE);
