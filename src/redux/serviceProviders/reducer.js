import { fromJS, OrderedSet, Map } from 'immutable';
import { handleActions, combineActions } from 'redux-actions';

import {
  ADD_OR_UPDATE_CASES,
  ADD_OR_UPDATE_CURRENT_CASE,
  LOGOUT,
  RESET_FAVORITES,
} from '../constants';

export const initialState = fromJS({
  responseIds: OrderedSet(),
  byId: {},
});

function updateFromCasesHandler(state, action) {
  const {
    entities: {
      serviceProvider: newServiceProviders = {},
    } = {},
  } = action.payload;

  const currentById = state.get('byId');
  const newById = currentById.merge(currentById, { ...newServiceProviders });
  return state
    .set('byId', newById);
}

function resetState(state) {
  return state
    .set('responseIds', OrderedSet())
    .set('byId', Map());
}

export default handleActions({
  [combineActions(ADD_OR_UPDATE_CASES, ADD_OR_UPDATE_CURRENT_CASE)]: updateFromCasesHandler,
  [combineActions(RESET_FAVORITES, LOGOUT)]: resetState,
}, initialState);
