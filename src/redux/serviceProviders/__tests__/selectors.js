import { Map } from 'immutable';

import { test, expect } from '__tests__/helpers/test-setup';

import {
  serviceProviderStoreSelector,
  getServiceProviderById,
} from '../selectors';

const providerOne = Map({ 1: 'test' });
const providerTwo = Map({ 2: 'another' });

const byId = Map({ 1: providerOne, 2: providerTwo });
const serviceProviders = Map({ byId });

const store = Map({ serviceProviders });

test('serviceProviderStoreSelector gets the serviceProviders slice of the store', () => {
  expect(serviceProviderStoreSelector(store)).toEqual(serviceProviders);
});

test('getServiceProviderById finds the requested service provider', () => {
  expect(getServiceProviderById(store, '1')).toEqual(providerOne);
  expect(getServiceProviderById(store, '2')).toEqual(providerTwo);
});

test('getServiceProviderById returns an empty map if no match is found', () => {
  expect(getServiceProviderById(store, '42')).toEqual(Map());
});
