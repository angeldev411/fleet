import { test, expect } from '__tests__/helpers/test-setup';
import * as casesActions from '../../cases/actions';

import reducer, { initialState } from '../reducer';

test('calling addOrUpdateCases action updates serviceProvider in state', () => {
  const serviceProvider = {
    id: '123',
  };
  const payload = {
    entities: {
      serviceProvider: {
        123: serviceProvider,
      },
    },
  };

  const action = casesActions.addOrUpdateCases(payload);
  const newState = reducer(initialState, action);

  expect(newState.get('byId').toJS()).toContain({ 123: serviceProvider });
});

test('calling addOrUpdateCases with no serviceProvider data does not alter state', () => {
  const payload = {
    entities: {},
  };

  const action = casesActions.addOrUpdateCases(payload);
  const newState = reducer(initialState, action);

  expect(newState.get('responseIds')).toEqual(initialState.get('responseIds'));
  expect(newState.get('byId')).toEqual(initialState.get('byId'));
});
