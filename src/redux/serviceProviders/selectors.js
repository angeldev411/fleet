import { Map } from 'immutable';
import { createSelector } from 'reselect';

export const serviceProviderStoreSelector = state => state.get('serviceProviders');

export const getServiceProviderById = (state, serviceProviderId) =>
  serviceProviderStoreSelector(state).getIn(['byId', serviceProviderId]) || Map();

export const makeGetServiceProviderById = () => createSelector(
  getServiceProviderById,
  serviceProvider => serviceProvider,
);
