import { createSelector } from 'reselect';

import { dashboardRequestingSelector } from 'redux/app/selectors';

import {
  assetCasesRequestingSelector,
  assetsRequestingSelector,
  assetsSelector,
} from 'redux/assets/selectors';

import {
  caseFaultsRequestingSelector,
  caseNotesRequestingSelector,
  casesRequestingSelector,
  casesSelector,
} from 'redux/cases/selectors';

import {
  loadAssetPendingSelector,
  loadCasePendingSelector,
  loadServiceRequestPendingSelector,
} from 'redux/requests/selectors';

import {
  serviceRequestsRequestingSelector,
} from 'redux/serviceRequests/selectors';

const requestingSelector = createSelector(
  assetsRequestingSelector,
  casesRequestingSelector,
  dashboardRequestingSelector,
  serviceRequestsRequestingSelector,
  (
    assetsRequesting,
    casesRequesting,
    dashboardRequesting,
    serviceRequestsRequesting,
  ) => assetsRequesting || casesRequesting || dashboardRequesting || serviceRequestsRequesting,
);

const noAssetsCasesSelector = createSelector(
  assetsSelector,
  casesSelector,
  (assets, cases) => assets.size + cases.size === 0,
);

const noAssetsCasesFoundSelector = createSelector(
  noAssetsCasesSelector,
  requestingSelector,
  (noAssetsCases, requesting) => !requesting && noAssetsCases,
);

const loadingSelector = createSelector(
  assetCasesRequestingSelector,
  assetsRequestingSelector,
  caseFaultsRequestingSelector,
  caseNotesRequestingSelector,
  casesRequestingSelector,
  dashboardRequestingSelector,
  loadAssetPendingSelector,
  loadCasePendingSelector,
  loadServiceRequestPendingSelector,
  serviceRequestsRequestingSelector,
  (
    assetCasesRequesting,
    assetsRequesting,
    caseFaultsRequesting,
    caseNotesRequesting,
    casesRequesting,
    dashboardRequesting,
    loadAssetPending,
    loadCasePending,
    loadServiceRequestPending,
    serviceRequestsRequesting,
  ) => (
    assetCasesRequesting ||
    assetsRequesting ||
    caseFaultsRequesting ||
    caseNotesRequesting ||
    casesRequesting ||
    dashboardRequesting ||
    loadAssetPending ||
    loadCasePending ||
    loadServiceRequestPending ||
    serviceRequestsRequesting
  ),
);

export {
  requestingSelector,
  noAssetsCasesSelector,
  noAssetsCasesFoundSelector,
  loadingSelector,
};
