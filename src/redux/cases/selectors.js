import { List, Map } from 'immutable';
import { createSelector } from 'reselect';

import {
  assetsStoreSelector,
  currentAssetCaseIdSelector,
} from 'redux/assets/selectors';
import { serviceProviderStoreSelector } from 'redux/serviceProviders/selectors';

// For convenience, extract the cases section of the store.
const casesStoreSelector = state => state.get('cases');

// export const caseRequestingSelector = createSelector(
//   casesStoreSelector,
//   casesStore => casesStore.get('caseRequesting'),
// );

export const caseFaultsRequestingSelector = createSelector(
  casesStoreSelector,
  casesStore => casesStore.get('caseFaultsRequesting'),
);

export const caseNotesRequestingSelector = createSelector(
  casesStoreSelector,
  casesStore => casesStore.getIn(['caseNotes', 'requesting']),
);

export const casesRequestingSelector = createSelector(
  casesStoreSelector,
  casesStore => casesStore.get('requesting'),
);

export const casesErrorSelector = createSelector(
  casesStoreSelector,
  casesStore => casesStore.get('error'),
);

// When all of the cases are required we convert cases.byId into an immutable list.
export const casesSelector = createSelector(
  casesStoreSelector,
  casesStore => casesStore.get('responseIds').map(id => casesStore.getIn(['byId', id])).toList(),
);

// This returns the cases from the currentAssetCases state of assets store
export const currentAssetCaseSelector = createSelector(
  casesStoreSelector,
  currentAssetCaseIdSelector,
  (casesStore, currentAssetCaseId) =>
    (currentAssetCaseId && casesStore.getIn(['byId', currentAssetCaseId])) || Map(),
);

export const currentAssetCaseServiceProviderSelector = createSelector(
  currentAssetCaseSelector,
  serviceProviderStoreSelector,
  (currentAssetCase, serviceProviderStore) =>
    (currentAssetCase &&
      serviceProviderStore.getIn(['byId', currentAssetCase.get('serviceProvider')])
    ) || Map(),
);

export const currentCaseIdSelector = createSelector(
  casesStoreSelector,
  casesStore => casesStore.get('currentCaseId'),
);

// When just the current case is required, we retrieve the case data matching the current case id.
export const currentCaseSelector = createSelector(
  casesStoreSelector,
  currentCaseIdSelector,
  (casesStore, currentCaseId) =>
    (currentCaseId &&
      casesStore.getIn(['byId', currentCaseId])
    ) || Map(),
);

export const currentCaseAssetSelector = createSelector(
  currentCaseSelector,
  assetsStoreSelector,
  (currentCase, assetsStore) =>
    (currentCase && assetsStore.getIn(['byId', currentCase.get('asset')])) || Map(),
);

export const currentCaseFaultsSelector = createSelector(
  [casesStoreSelector],
  cases => cases.get('currentCaseFaults') || List(),
);

export const caseNotesSelector = createSelector(
  [casesStoreSelector],
  cases => cases.get('caseNotes'),
);

export const caseRecipientsSelector = createSelector(
  [casesStoreSelector],
  cases => cases.get('caseRecipients'),
);

export const selectedCaseSelector = createSelector(
  [casesStoreSelector],
  cases => cases.get('selectedCaseId'),
);

export const currentCaseServiceProviderSelector = createSelector(
  currentCaseSelector,
  serviceProviderStoreSelector,
  (currentCase, serviceProviderStore) =>
    (currentCase && serviceProviderStore.getIn(['byId', currentCase.get('serviceProvider')])) ||
    Map(),
);
