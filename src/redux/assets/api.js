import endpoints from 'apiEndpoints';
import { get } from 'utils/fetch';

export function getAsset({ assetId }) {
  return get(endpoints.asset({ assetId }));
}

export function getAssets(query) {
  return get(endpoints.assets(), { query });
}

export function getAssetCases({ assetId }) {
  return get(endpoints.assetCases({ assetId }));
}

export function getAssetFaults({ assetId }) {
  return get(endpoints.assetFaults({ assetId }));
}
