import { fromJS, OrderedSet, Map } from 'immutable';
import { test, expect } from '__tests__/helpers/test-setup';

import * as actions from '../actions';
import * as caseActions from '../../cases/actions';
import {
  LOAD_ASSET_CASES_REQUEST,
  LOAD_ASSET_CASES_SUCCESS,
  LOAD_ASSET_CASES_FAILURE,
  LOAD_ASSETS_REQUEST,
  LOAD_ASSETS_SUCCESS,
  LOAD_ASSETS_FAILURE,
} from '../../constants';

import reducer, { initialState } from '../reducer';

test('calling addOrUpdateCases action with no data does not change responseIds or byId', () => {
  const action = caseActions.addOrUpdateCases({ payload: {} });
  const state = fromJS({
    responseIds: OrderedSet(['123', '321']),
    byId: {
      123: { id: 123 },
      321: { id: 321 },
    },
  });
  const newState = reducer(state, action);
  expect(newState.get('responseIds')).toEqual(state.get('responseIds'));
  expect(newState.get('byId')).toEqual(state.get('byId'));
});

test('calling addOrUpdateAssets action with no data does not change responseIds or byId', () => {
  const action = actions.addOrUpdateAssets({ payload: {} });
  const state = fromJS({
    responseIds: OrderedSet(['123', '321']),
    byId: {
      123: { id: 123 },
      321: { id: 321 },
    },
  });
  const newState = reducer(state, action);
  expect(newState.get('responseIds')).toEqual(state.get('responseIds'));
  expect(newState.get('byId')).toEqual(state.get('byId'));
});

test('calling addOrUpdateAssets action with entities/asset changes byId', () => {
  const action = actions.addOrUpdateAssets({ entities: { asset: { 456: Map({ id: 456 }) } } });
  const state = fromJS({
    responseIds: OrderedSet(['123', '321']),
    byId: {
      123: { id: 123 },
      321: { id: 321 },
    },
  });
  const newState = reducer(state, action);
  expect(newState.get('byId')).toEqual(fromJS({
    123: { id: 123 },
    321: { id: 321 },
    456: { id: 456 },
  }));
});

test('calling setCurrentAsset action updates asset.currentAssetId in state', () => {
  const currentAssetId = '123';

  const action = actions.setCurrentAsset(currentAssetId);
  const newState = reducer(initialState, action);

  expect(newState.get('currentAssetId')).toEqual(currentAssetId);
});

test('calling setCurrentAssetCaseId action updates asset.currentAssetCaseId in state', () => {
  const currentAssetCaseId = '333';

  const action = actions.setCurrentAssetCaseId(currentAssetCaseId);
  const newState = reducer(initialState, action);

  expect(newState.get('currentAssetCaseId')).toEqual(currentAssetCaseId);
});

test('calling setCurrentAssetCases action updates asset.currentAssetCases in state', () => {
  const assetCases = ['123', '234', '345'];
  const payload = { result: assetCases };

  const action = actions.setCurrentAssetCases(payload);
  const newState = reducer(initialState, action);

  expect(newState.get('currentAssetCases')).toEqual(fromJS(assetCases));
});

// --------------------- Asset ----------------------

test('action with type SET_CURRENT_ASSET_FAULTS sets currentAssetFaults in state', () => {
  const assetFaults = Map(
    { id: '1376418', faultType: 'VehicleNotification::OsVolvoLinkFault', faultCode: '111' },
  );

  const action = actions.setCurrentAssetFaults(assetFaults);
  const newState = reducer(initialState, action);

  expect(newState.get('currentAssetFaults')).toEqual(Map(
    { id: '1376418', faultType: 'VehicleNotification::OsVolvoLinkFault', faultCode: '111' },
  ));
});

// --------------------- Asset cases ----------------------

test('action with type LOAD_ASSET_CASE_REQUEST sets assetCasesRequesting to true', () => {
  const newState = reducer(initialState, { type: LOAD_ASSET_CASES_REQUEST });
  expect(newState.get('assetCasesRequesting')).toEqual(true);
});

test('action with type LOAD_ASSET_CASES_SUCCESS sets assetCasesRequesting to false in state', () => {
  const testState = initialState.set('assetRequesting', true);
  const newState = reducer(testState, { type: LOAD_ASSET_CASES_SUCCESS });
  expect(newState.get('assetCasesRequesting')).toEqual(false);
});

test('action with type LOAD_ASSET_CASES_FAILURE sets requesting to false and saves error', () => {
  const testState = initialState.set('assetCasesRequesting', true);
  const action = {
    type: LOAD_ASSET_CASES_FAILURE,
    error: true,
    payload: { error: 'load asset failure' },
  };
  const newState = reducer(testState, action);
  expect(newState.get('assetCasesRequesting')).toEqual(false);
  expect(newState.get('error')).toEqual(action.payload.response);
});

// --------------------- Assets ----------------------

test('action with type LOAD_ASSETS_REQUEST sets requesting to true', () => {
  const newState = reducer(initialState, { type: LOAD_ASSETS_REQUEST });
  expect(newState.get('requesting')).toEqual(true);
});

test('action with type LOAD_ASSETS_SUCCESS sets requesting to false in state', () => {
  const testState = initialState.set('requesting', true);
  expect(testState.get('requesting')).toEqual(true);
  const newState = reducer(testState, { type: LOAD_ASSETS_SUCCESS });
  expect(newState.get('requesting')).toEqual(false);
});

test('action with type LOAD_ASSETS_FAILURE sets requesting to false and saves error', () => {
  const testState = initialState.set('requesting', true);
  expect(testState.get('requesting')).toEqual(true);
  const action = {
    type: LOAD_ASSETS_FAILURE,
    error: true,
    payload: { error: 'invalid query' },
  };
  const newState = reducer(testState, action);
  expect(newState.get('requesting')).toEqual(false);
  expect(newState.get('error')).toEqual(action.payload.response);
});
