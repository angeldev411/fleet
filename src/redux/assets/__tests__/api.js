import nock from 'nock';
import { test, expect } from '__tests__/helpers/test-setup';

import * as api from '../api';

const testDomain = process.env.API_BASE_URL;

test('getAsset reaches the expected route and returns the response', async () => {
  const assetId = '12345';
  const response = { response: 'success' };
  const httpMock = nock(testDomain);
  httpMock.get(`/assets/${assetId}`).reply(200, response);

  const { response: httpResponse } = await api.getAsset({ assetId });

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});

test('getAssetCases reaches the expected route and returns the response', async () => {
  const assetId = '23456';
  const response = { response: 'success' };
  const httpMock = nock(testDomain);
  httpMock.get(`/assets/${assetId}/cases`).reply(200, response);

  const { response: httpResponse } = await api.getAssetCases({ assetId });

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});

test('getAssets reaches the expected route and returns the response', async () => {
  const response = [{ response: 'success' }];
  const httpMock = nock(testDomain);
  httpMock.get('/assets')
    .query()
    .reply(200, response);

  const { response: httpResponse } = await api.getAssets();

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});

test('getAssets reaches the expected route and returns the response', async () => {
  const scope = 'test-scope';
  const perPage = 42;
  const response = [{ response: 'success' }];
  const httpMock = nock(testDomain);
  httpMock.get('/assets')
    .query({
      scope,
      per_page: perPage,
    })
    .reply(200, response);

  const { response: httpResponse } = await api.getAssets({ scope, per_page: perPage });

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});

test('getAssetFaults reaches the expected route and returns the response', async () => {
  const assetId = '12345';
  const response = [{ response: 'success' }];
  const httpMock = nock(testDomain);
  httpMock.get(`/assets/${assetId}/faults`).reply(200, response);

  const { response: httpResponse } = await api.getAssetFaults({ assetId });

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});
