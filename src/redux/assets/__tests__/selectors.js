import { fromJS, OrderedSet, Map } from 'immutable';

import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import * as selectors from '../selectors';

const state = fromJS({
  assets: {
    currentAssetId: '1234',
    currentAssetFaults: [
      { id: '1376418', faultType: 'VehicleNotification::OsVolvoLinkFault', faultCode: '111' },
      { id: '1376343', faultType: 'VehicleNotification::OsVolvoLinkFault', faultCode: '112' },
    ],
    responseIds: OrderedSet(['123', '456', '789']),
    byId: {
      123: { id: '123' },
      456: { id: '456' },
      789: { id: '789' },
    },
  },
});

test('assetsSelector returns an Immutable list of maps', () => {
  expect(selectors.assetsSelector(state)).toEqual(fromJS([
    { id: '123' },
    { id: '456' },
    { id: '789' },
  ]));
});

test('getAssetById returns an empty Map if no id matches', () => {
  expect(selectors.getAssetById(state, '9182')).toEqual(Map());
});

test('currentAssetIdSelector returns the current asset ID', () => {
  expect(selectors.currentAssetIdSelector(state, '1234')).toEqual('1234');
});

test('currentAssetFaultsSelector return array of faults', () => {
  expect(selectors.currentAssetFaultsSelector(state)).toEqual(fromJS([
    { id: '1376418', faultType: 'VehicleNotification::OsVolvoLinkFault', faultCode: '111' },
    { id: '1376343', faultType: 'VehicleNotification::OsVolvoLinkFault', faultCode: '112' },
  ]));
});

test('currentAssetSelector returns the byId slice for the currentAssetId', () => {
  const currentAssetId = '123';
  const newState = state.toJS();
  newState.assets.currentAssetId = currentAssetId;
  expect(selectors.currentAssetSelector(fromJS(newState)))
    .toEqual(fromJS(newState.assets.byId[currentAssetId]));
});

test('currentAssetSelector returns an empty object if cannot find byId slice for the currentAssetId', () => {
  expect(selectors.currentAssetSelector(state)).toEqual(fromJS({}));
});
