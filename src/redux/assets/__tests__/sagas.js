import { fromJS, OrderedSet, Map, List } from 'immutable';
import SagaTester from 'redux-saga-tester';

import { test, expect } from '__tests__/helpers/test-setup';

import createReducer from 'setup/reducer';

import {
  ADD_OR_UPDATE_CASES,
  LOAD_ASSET,
  LOAD_ASSET_CASES_SUCCESS,
  LOAD_ASSETS_SUCCESS,
  LOAD_ASSET_FAULTS,
} from '../../constants';
import * as sagas from '../sagas';

const initialState = fromJS({});
const reducers = createReducer();

function buildAsset(assetId) {
  return {
    id: assetId,
  };
}

function buildAssetFaults() {
  return fromJS([
    {
      componentId: 'Component1',
      status: 'Status',
      reportedAt: 'DateTime3',
      ecu: 'ECU',
      fmi: 'FMI',
      count: 10,
      severity: {
        color: 'red',
        level: 1,
      },
    },
    {
      componentId: 'Component2',
      status: 'Status2',
      reportedAt: 'DateTime1',
      ecu: 'ECU2',
      fmi: 'FMI2',
      count: 11,
      severity: {
        color: 'yellow',
        level: 2,
      },
    },
    {
      componentId: 'Component3',
      status: 'Status3',
      reportedAt: 'DateTime2',
      ecu: 'ECU2',
      fmi: 'FMI2',
      count: 11,
      severity: {
        color: 'yellow',
        level: 2,
      },
    },
  ]);
}

/* ------------------ Asset ------------------- */

test('loadAssetSuccess', () => {
  const assetId = '456';
  const currentAsset = buildAsset(assetId);

  const response = {
    body: currentAsset,
  };

  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.loadAssetWatcher);
  sagaTester.dispatch({
    type: LOAD_ASSET,
    payload: response,
  });

  const appState = sagaTester.store.getState();

  expect(appState.getIn(['assets', 'responseIds'])).toEqual(OrderedSet([assetId]));
  expect(appState.getIn(['assets', 'byId', assetId])).toEqual(Map({ id: assetId }));
  expect(appState.getIn(['assets', 'currentAssetId'])).toEqual(assetId);
});

test('loadAssetSuccess with no response', () => {
  const assetId = null;
  const currentAsset = buildAsset(assetId);

  const response = {
    body: currentAsset,
  };

  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.loadAssetWatcher);
  sagaTester.dispatch({
    type: LOAD_ASSET,
    payload: response,
  });

  const appState = sagaTester.store.getState();

  expect(appState.getIn(['assets', 'responseIds'])).toEqual(OrderedSet());
  expect(appState.getIn(['assets', 'byId'])).toEqual(Map());
  expect(appState.getIn(['assets', 'currentAssetId'])).toEqual('');
});

test('loadAssetFaults Success', async () => {
  const currentAssetFaults = buildAssetFaults();
  const response = {
    body: currentAssetFaults,
  };
  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.loadAssetFaultsSuccessWatcher);
  sagaTester.dispatch({
    type: LOAD_ASSET_FAULTS,
    payload: response,
  });

  const appState = sagaTester.store.getState();
  expect(appState.getIn(['assets', 'currentAssetFaults'])).toEqual(currentAssetFaults);
});

/* ------------------ Asset cases ------------------- */

test('loadAssetCasesSuccess', () => {
  const currentAssetCases = [
    { id: '123' },
    { id: '234' },
    { id: '345' },
  ];

  const response = {
    body: currentAssetCases,
  };

  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.loadAssetCasesSuccessWatcher);
  sagaTester.dispatch({
    type: LOAD_ASSET_CASES_SUCCESS,
    payload: response,
  });

  // This confirms that the asset cases are added/updated to the cases store
  sagaTester.waitFor(ADD_OR_UPDATE_CASES);

  const appState = sagaTester.store.getState();
  expect(appState.getIn(['assets', 'currentAssetCases'])).toEqual(List(['123', '234', '345']));
});

/* ------------------ Assets ------------------- */

test('loadAssetsSuccess', () => {
  const assetId1 = '123';
  const assetId2 = 'abc';
  const asset1 = buildAsset(assetId1);
  const asset2 = buildAsset(assetId2);

  const assets = [asset1, asset2];

  const response = {
    body: assets,
    headers: Map({ 'x-total-count': '2' }),
  };

  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.loadAssetsSuccessWatcher);
  sagaTester.dispatch({
    type: LOAD_ASSETS_SUCCESS,
    payload: response,
  });

  const appState = sagaTester.store.getState();

  expect(appState.getIn(['assets', 'responseIds'])).toEqual(OrderedSet([assetId1, assetId2]));
  expect(appState.getIn(['assets', 'byId', assetId1]))
    .toEqual(Map({ id: assetId1 }));
  expect(appState.getIn(['assets', 'byId', assetId2]))
    .toEqual(Map({ id: assetId2 }));
  expect(appState.getIn(['app', 'favoritePagination', 'totalCount']))
    .toEqual(2);
  expect(appState.getIn(['app', 'searchPagination', 'assets', 'totalCount']))
    .toEqual(2);
});
