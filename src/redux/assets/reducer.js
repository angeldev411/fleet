import { fromJS, OrderedSet, Map } from 'immutable';
import { handleActions, combineActions } from 'redux-actions';

import {
  ADD_OR_UPDATE_ASSETS,
  ADD_OR_UPDATE_CASES,
  ADD_OR_UPDATE_CURRENT_ASSET,
  ADD_OR_UPDATE_CURRENT_CASE,
  ADD_OR_UPDATE_CURRENT_SERVICE_REQUEST,
  ADD_OR_UPDATE_SERVICE_REQUESTS,
  LOAD_ASSET_CASES_REQUEST,
  LOAD_ASSET_CASES_SUCCESS,
  LOAD_ASSET_CASES_FAILURE,
  LOAD_ASSETS_REQUEST,
  LOAD_ASSETS_SUCCESS,
  LOAD_ASSETS_FAILURE,
  SELECT_ASSET,
  SET_CURRENT_ASSET_FAULTS,
  LOGOUT,
  RESET_FAVORITES,
  SET_CURRENT_ASSET,
  SET_CURRENT_ASSET_CASE_ID,
  SET_CURRENT_ASSET_CASES,
} from '../constants';

export const initialState = fromJS({
  responseIds: OrderedSet(),
  byId: {},
  currentAssetId: '',
  currentAssetCases: [],
  currentAssetCase: '',
  currentAssetFaults: [],
  selectedAssetId: '',
  requesting: false,
  error: Map(),
});

function updateFromOtherHandler(state, action) {
  const {
    entities: {
      asset: newAssets = {},
    } = {},
  } = action.payload;

  const currentById = state.get('byId');
  const newById = currentById.merge(currentById, { ...newAssets });

  return state
    .set('byId', newById);
}

function addOrUpdateAssetsHandler(state, action) {
  const {
    entities: {
      asset: newAssets = {},
    } = {},
    result = [],
  } = action.payload;
  const currentAllIds = state.get('responseIds');
  const newAllIds = currentAllIds.concat(result);

  const currentById = state.get('byId');
  const newById = currentById.merge(currentById, { ...newAssets });

  return state
    .set('responseIds', newAllIds)
    .set('byId', newById);
}

function addOrUpdateCurrentAssetHandler(state, action) {
  const {
    entities: {
      asset: newAssets = {},
    } = {},
  } = action.payload;
  const assetId = Object.keys(newAssets)[0];

  const currentAssetInfo = state.getIn(['byId', assetId]) || Map();
  const newAssetInfo = currentAssetInfo.merge(currentAssetInfo, newAssets[assetId]);

  return state
    .update('responseIds', orderedSet => orderedSet.add(assetId))
    .updateIn(['byId', assetId], () => newAssetInfo)
    .set('currentAssetId', assetId);
}

// Asset cases

function loadAssetCasesRequestHandler(state) {
  return state
    .set('assetCasesRequesting', true)
    .set('error', Map());
}

function loadAssetCasesSuccessHandler(state) {
  return state
    .set('assetCasesRequesting', false)
    .set('error', Map());
}

function loadAssetCasesFailureHandler(state, action) {
  return state
    .set('assetCasesRequesting', false)
    .set('error', action.payload.response);
}

function setCurrentAssetFaultsHandler(state, action) {
  return state.set('currentAssetFaults', fromJS(action.payload.currentAssetFaults));
}

// Assets

function loadAssetsRequestHandler(state) {
  return state
    .set('requesting', true)
    .set('error', Map());
}

function loadAssetsSuccessHandler(state) {
  return state
    .set('requesting', false)
    .set('error', Map());
}

function loadAssetsFailureHandler(state, action) {
  return state
    .set('requesting', false)
    .set('error', action.payload.response);
}

function resetState(state) {
  return state
    .set('responseIds', OrderedSet())
    .set('byId', Map())
    .set('requesting', false);
}

function selectAsset(state, action) {
  const selectedAssetId = state.get('selectedAssetId');
  const id = selectedAssetId === action.payload ? '' : action.payload;
  return state
    .set('selectedAssetId', id);
}

function setCurrentAssetHandler(state, action) {
  return state.set('currentAssetId', action.payload);
}

function setCurrentAssetCaseIdHandler(state, action) {
  return state.set('currentAssetCaseId', action.payload);
}

function setCurrentAssetCasesHandler(state, action) {
  const { result = [] } = action.payload;
  return state.set('currentAssetCases', fromJS(result));
}

export default handleActions({
  // With our data normalized, we will have an `entities` object in the action's payload.
  // This object will have data to store in several locations. Notice that ADD_OR_UPDATE_ASSETS
  // is called in at least 3 reducers. There is no limit to how many changes a single
  // action can trigger.
  // Because the asset data looks the exact same for one or more assets,
  // we can combine these actions and re-use the same reducer logic.
  [ADD_OR_UPDATE_ASSETS]: addOrUpdateAssetsHandler,
  [ADD_OR_UPDATE_CURRENT_ASSET]: addOrUpdateCurrentAssetHandler,
  [combineActions(
    ADD_OR_UPDATE_CASES,
    ADD_OR_UPDATE_CURRENT_CASE,
    ADD_OR_UPDATE_CURRENT_SERVICE_REQUEST,
    ADD_OR_UPDATE_SERVICE_REQUESTS,
  )]: updateFromOtherHandler,
  [combineActions(RESET_FAVORITES, LOGOUT)]: resetState,
  [LOAD_ASSET_CASES_REQUEST]: loadAssetCasesRequestHandler,
  [LOAD_ASSET_CASES_SUCCESS]: loadAssetCasesSuccessHandler,
  [LOAD_ASSET_CASES_FAILURE]: loadAssetCasesFailureHandler,
  [LOAD_ASSETS_REQUEST]: loadAssetsRequestHandler,
  [LOAD_ASSETS_SUCCESS]: loadAssetsSuccessHandler,
  [LOAD_ASSETS_FAILURE]: loadAssetsFailureHandler,
  [SELECT_ASSET]: selectAsset,
  [SET_CURRENT_ASSET_FAULTS]: setCurrentAssetFaultsHandler,
  [SET_CURRENT_ASSET]: setCurrentAssetHandler,
  [SET_CURRENT_ASSET_CASES]: setCurrentAssetCasesHandler,
  [SET_CURRENT_ASSET_CASE_ID]: setCurrentAssetCaseIdHandler,
}, initialState);
