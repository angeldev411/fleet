import { List, Map } from 'immutable';
import { createSelector } from 'reselect';

export const assetsStoreSelector = state => state.get('assets');

export const assetCasesRequestingSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.get('assetCasesRequesting'),
);

export const assetsRequestingSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.get('requesting'),
);

export const assetsErrorSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.get('error'),
);

// When all of the assets are required we convert assets.byId into an immutable list.
export const assetsSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.get('responseIds').map(id => assetsStore.getIn(['byId', id])).toList(),
);

export const currentAssetFaultsSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.get('currentAssetFaults') || List(),
);

export const currentAssetIdSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.get('currentAssetId'),
);
// When just the current asset is required, we retrieve the asset data matching
// the current asset id.
export const currentAssetSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.getIn(['byId', assetsStore.get('currentAssetId')]) || Map(),
);

export const currentAssetCasesSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.get('currentAssetCases'),
);

export const currentAssetCaseIdSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.get('currentAssetCaseId'),
);

export const getAssetById = (state, assetId) =>
  assetsStoreSelector(state).getIn(['byId', assetId]) || Map();

export const makeGetAssetById = () => createSelector(
  getAssetById,
  asset => asset,
);

export const selectedAssetSelector = createSelector(
  assetsStoreSelector,
  assetsStore => assetsStore.get('selectedAssetId'),
);
