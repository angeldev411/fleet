import { normalize } from 'normalizr';
import { put, call, takeLatest } from 'redux-saga/effects';

import { setFavoritePagination, setSearchPagination } from 'redux/app/actions';
import { addOrUpdateCases } from 'redux/cases/actions';

import {
  assetSchema,
  assetsSchema,
  casesSchema,
} from '../schemas';

import {
  LOAD_ASSET,
  LOAD_ASSET_CASES_SUCCESS,
  LOAD_ASSETS_SUCCESS,
  LOAD_ASSET_FAULTS,
} from '../constants';

import {
  addOrUpdateAssets,
  addOrUpdateCurrentAsset,
  setCurrentAssetCases,
  setCurrentAssetFaults,
} from './actions';

// worker sagas
function* loadAssetWorker({ payload: response }) {
  const normalizedData = yield call(normalize, response.body, assetSchema);

  if (normalizedData.result) {
    yield put(addOrUpdateCurrentAsset(normalizedData));
  }
}

function* loadAssetCasesSuccessWorker({ payload: response }) {
  const normalizedData = yield call(normalize, response.body, casesSchema);
  yield put(addOrUpdateCases(normalizedData));
  yield put(setCurrentAssetCases(normalizedData));
}

function* loadAssetsSuccessWorker({ payload: response }) {
  const normalizedData = yield call(normalize, response.body, assetsSchema);
  const pagination = {
    latestPage: response.headers.get('x-page'),
    perPage: response.headers.get('x-per-page'),
    totalCount: response.headers.get('x-total-count'),
    totalPages: response.headers.get('x-total-pages'),
  };

  yield put(addOrUpdateAssets(normalizedData));
  yield put(setFavoritePagination(pagination));
  yield put(setSearchPagination({
    type: 'assets',
    pagination,
  }));
}

export function* loadAssetFaultsSuccessWorker({ payload: response }) {
  const sortedData = response.body.sort((a, b) => (a.reportedAt > b.reportedAt ? 1 : -1));
  yield put(setCurrentAssetFaults(sortedData));
}

// watcher sagas
export function* loadAssetWatcher() {
  yield takeLatest(LOAD_ASSET, loadAssetWorker);
}

export function* loadAssetCasesSuccessWatcher() {
  yield takeLatest(LOAD_ASSET_CASES_SUCCESS, loadAssetCasesSuccessWorker);
}

export function* loadAssetsSuccessWatcher() {
  yield takeLatest(LOAD_ASSETS_SUCCESS, loadAssetsSuccessWorker);
}

export function* loadAssetFaultsSuccessWatcher() {
  yield takeLatest(LOAD_ASSET_FAULTS, loadAssetFaultsSuccessWorker);
}

// export all of the sagas.
export default [
  loadAssetWatcher(),
  loadAssetCasesSuccessWatcher(),
  loadAssetsSuccessWatcher(),
  loadAssetFaultsSuccessWatcher(),
];
