import { createAction } from 'redux-actions';
import { createApiAction } from 'utils/redux-api-actions';
import { createRequestAction } from 'utils/redux-request-actions';

import {
  getAsset,
  getAssetCases,
  getAssets,
  getAssetFaults,
} from './api';

import {
  ADD_OR_UPDATE_ASSETS,
  ADD_OR_UPDATE_CURRENT_ASSET,
  LOAD_ASSET,
  LOAD_ASSET_CASES_REQUEST,
  LOAD_ASSET_CASES_SUCCESS,
  LOAD_ASSET_CASES_FAILURE,
  LOAD_ASSETS_REQUEST,
  LOAD_ASSETS_SUCCESS,
  LOAD_ASSETS_FAILURE,
  LOAD_ASSET_FAULTS,
  SELECT_ASSET,
  SET_CURRENT_ASSET_FAULTS,
  RESET_FAVORITES,
  SET_CURRENT_ASSET,
  SET_CURRENT_ASSET_CASE_ID,
  SET_CURRENT_ASSET_CASES,
} from '../constants';

export const addOrUpdateAssets = createAction(ADD_OR_UPDATE_ASSETS);

export const addOrUpdateCurrentAsset = createAction(ADD_OR_UPDATE_CURRENT_ASSET);

export const clearAssets = createAction(RESET_FAVORITES);

export const loadAsset = createRequestAction(
  LOAD_ASSET,
  getAsset,
);

export const loadAssetCases = createApiAction(
  [LOAD_ASSET_CASES_REQUEST, LOAD_ASSET_CASES_SUCCESS, LOAD_ASSET_CASES_FAILURE],
  getAssetCases,
);

export const loadAssets = createApiAction(
  [LOAD_ASSETS_REQUEST, LOAD_ASSETS_SUCCESS, LOAD_ASSETS_FAILURE],
  getAssets,
);

export const loadAssetFaults = createRequestAction(LOAD_ASSET_FAULTS, getAssetFaults);

export const selectAsset = createAction(SELECT_ASSET);

export const setCurrentAssetFaults = createAction(
  SET_CURRENT_ASSET_FAULTS,
  currentAssetFaults => ({ currentAssetFaults }),
);

export const setCurrentAsset = createAction(SET_CURRENT_ASSET);

export const setCurrentAssetCases = createAction(SET_CURRENT_ASSET_CASES);

export const setCurrentAssetCaseId = createAction(SET_CURRENT_ASSET_CASE_ID);
