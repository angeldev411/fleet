/**
 * These are the constants used throughout the Redux store. They should be kept at this level
 * because constants may be called anywhere within the store, regardless of namespacing.
 */

export const EXPAND_LEFTNAV_PARENT = 'fleet_web/ui/EXPAND_LEFTNAV_PARENT';
export const HANDLE_ACTION_BAR_ITEM_CLICK = 'fleet_web/ui/HANDLE_ACTION_BAR_ITEM_CLICK';
export const REQUEST_FULL_SCREEN_MODE = 'fleet_web/ui/REQUEST_FULL_SCREEN_MODE';
export const REQUEST_PRINT_PAGE = 'fleet_web/ui/REQUEST_PRINT_PAGE';
export const SET_ASSETS_VIEW = 'fleet_web/ui/SET_ASSETS_VIEW';
export const SET_CASES_VIEW = 'fleet_web/ui/SET_CASES_VIEW';
export const SET_COMPONENT_EXPANDED = 'fleet_web/ui/SET_COMPONENT_EXPANDED';
export const SET_LEFTNAV_EXPANDED = 'fleet_web/ui/SET_LEFTNAV_EXPANDED';
export const SET_LOADING_STARTED = 'fleet_web/ui/SET_LOADING_STARTED';
export const SET_SERVICE_REQUESTS_VIEW = 'fleet_web/ui/SET_SERVICE_REQUESTS_VIEW';
export const UPDATE_SCROLL_TO_UNREAD_NOTE = 'fleet_web/ui/UPDATE_SCROLL_TO_UNREAD_NOTE';

// key for the persisted app UI state in memoryDB:
export const LOCALSTORAGE_APP_STATE_KEY = 'DECISIV/fleet_web/ui-state';

// PageConfig constants
export const SET_CURRENT_PAGE_CONFIG_KEY = 'fleet_web/pageConfig/SET_CURRENT_PAGE_CONFIG_KEY';

// export const INIT_FAVORITES = 'fleet_web/app/INIT_FAVORITES';
export const LOAD_DASHBOARD_REQUEST = 'fleet_web/app/LOAD_DASHBOARD_REQUEST';
export const LOAD_DASHBOARD_SUCCESS = 'fleet_web/app/LOAD_DASHBOARD_SUCCESS';
export const LOAD_DASHBOARD_FAILURE = 'fleet_web/app/LOAD_DASHBOARD_FAILURE';
export const PERFORM_SEARCH = 'fleet_web/app/PERFORM_SEARCH';
export const SET_DASHBOARD_FAVORITES = 'fleet_web/app/SET_DASHBOARD_FAVORITES';
export const SET_FAVORITE_PAGINATION = 'fleet_web/app/SET_FAVORITE_PAGINATION';
export const SET_LATEST_FAVORITE = 'fleet_web/app/SET_LATEST_FAVORITE';
// export const SET_LEFT_NAV_FAVORITES = 'fleet_web/app/SET_LEFT_NAV_FAVORITES';
export const SET_SEARCH_PARAMS = 'fleet_web/app/SET_SEARCH_PARAMS';
export const SET_SEARCH_PAGINATION = 'fleet_web/app/SET_SEARCH_PAGINATION';
export const CLEAR_LOCATION_WIDGET = 'fleet_web/app/CLEAR_LOCATION_WIDGET';
export const ADD_PAGE_TO_BREADCRUMBS = 'fleet_web/app/ADD_PAGE_TO_BREADCRUMBS';

// FAVORITES constants
export const RESET_FAVORITES = 'fleet_web/favorites/RESET_FAVORITES';

// Cases constants
export const ADD_OR_UPDATE_CASES = 'fleet_web/cases/ADD_OR_UPDATE_CASES';
export const ADD_OR_UPDATE_CURRENT_CASE = 'fleet_web/cases/ADD_OR_UPDATE_CURRENT_CASE';
export const ADD_OR_UPDATE_CURRENT_ASSET_CASES = 'fleet_web/cases/ADD_OR_UPDATE_CURRENT_ASSET_CASES';
export const CLEAR_CASE_RECIPIENTS = 'fleet_web/cases/CLEAR_CASE_RECIPIENTS';
export const LOAD_CASE = 'fleet_web/cases/LOAD_CASE';
export const LOAD_CASE_FAULTS_REQUEST = 'fleet_web/cases/LOAD_CASE_FAULTS_REQUEST';
export const LOAD_CASE_FAULTS_SUCCESS = 'fleet_web/cases/LOAD_CASE_FAULTS_SUCCESS';
export const LOAD_CASE_FAULTS_FAILURE = 'fleet_web/cases/LOAD_CASE_FAULTS_FAILURE';
export const LOAD_CASE_NOTES_REQUEST = 'fleet_web/cases/LOAD_CASE_NOTES_REQUEST';
export const LOAD_CASE_NOTES_SUCCESS = 'fleet_web/cases/LOAD_CASE_NOTES_SUCCESS';
export const LOAD_CASE_NOTES_FAILURE = 'fleet_web/cases/LOAD_CASE_NOTES_FAILURE';
export const LOAD_CASE_RECIPIENTS_REQUEST = 'fleet_web/cases/LOAD_CASE_RECIPIENTS_REQUEST';
export const LOAD_CASE_RECIPIENTS_SUCCESS = 'fleet_web/cases/LOAD_CASE_RECIPIENTS_SUCCESS';
export const LOAD_CASE_RECIPIENTS_FAILURE = 'fleet_web/cases/LOAD_CASE_RECIPIENTS_FAILURE';
export const LOAD_CASES_REQUEST = 'fleet_web/cases/LOAD_CASES_REQUEST';
export const LOAD_CASES_SUCCESS = 'fleet_web/cases/LOAD_CASES_SUCCESS';
export const LOAD_CASES_FAILURE = 'fleet_web/cases/LOAD_CASES_FAILURE';
export const POST_CASE_NOTE = 'fleet_web/cases/POST_CASE_NOTE';
export const SELECT_CASE = 'fleet_web/cases/SELECT_CASE';
export const SET_CURRENT_CASE = 'fleet_web/cases/SET_CURRENT_CASE';
export const SET_CURRENT_CASE_FAULTS = 'fleet_web/cases/SET_CURRENT_CASE_FAULTS';
export const SET_CASE_NOTES = 'fleet_web/cases/SET_CASE_NOTES';
export const UPDATE_CASE_NOTE = 'fleet_web/cases/UPDATE_CASE_NOTE';
export const UPDATE_CASE_REQUEST = 'fleet_web/cases/UPDATE_CASE_REQUEST';
export const UPDATE_CASE_SUCCESS = 'fleet_web/cases/UPDATE_CASE_SUCCESS';
export const UPDATE_CASE_FAILURE = 'fleet_web/cases/UPDATE_CASE_FAILURE';

// Assets constants
export const ADD_OR_UPDATE_ASSETS = 'fleet_web/assets/ADD_OR_UPDATE_ASSETS';
export const ADD_OR_UPDATE_CURRENT_ASSET = 'fleet_web/assets/ADD_OR_UPDATE_CURRENT_ASSET';
export const LOAD_ASSET = 'fleet_web/assets/LOAD_ASSET';
export const LOAD_ASSET_CASES_REQUEST = 'fleet_web/assets/LOAD_ASSET_CASES_REQUEST';
export const LOAD_ASSET_CASES_SUCCESS = 'fleet_web/assets/LOAD_ASSET_CASES_SUCCESS';
export const LOAD_ASSET_CASES_FAILURE = 'fleet_web/assets/LOAD_ASSET_CASES_FAILURE';
export const LOAD_ASSETS_REQUEST = 'fleet_web/assets/LOAD_ASSETS_REQUEST';
export const LOAD_ASSETS_SUCCESS = 'fleet_web/assets/LOAD_ASSETS_SUCCESS';
export const LOAD_ASSETS_FAILURE = 'fleet_web/assets/LOAD_ASSETS_FAILURE';
export const LOAD_ASSET_FAULTS = 'fleet_web/assets/LOAD_ASSET_FAULTS';
export const SELECT_ASSET = 'fleet_web/assets/SELECT_ASSET';
export const SET_CURRENT_ASSET_FAULTS = 'fleet_web/assets/SET_CURRENT_ASSET_FAULTS';
export const SET_CURRENT_ASSET = 'fleet_web/cases/SET_CURRENT_ASSET';
export const SET_CURRENT_ASSET_CASE_ID = 'fleet_web/cases/SET_CURRENT_ASSET_CASE_ID';
export const SET_CURRENT_ASSET_CASES = 'fleet_web/cases/SET_CURRENT_ASSET_CASES';

export const ADD_OR_UPDATE_AUTH_DATA = 'fleet_web/user/ADD_OR_UPDATE_AUTH_DATA';
export const CLEAR_TOKENS = 'fleet_web/user/CLEAR_TOKENS';
export const CLEAR_LOGIN_REQUEST_ERROR = 'fleet_web/user/CLEAR_LOGIN_REQUEST_ERROR';
export const LOAD_USER_PROFILE = 'fleet_web/user/LOAD_USER_PROFILE';
export const LOGIN = 'fleet_web/user/LOGIN';
export const LOGIN_SUCCESS = 'fleet_web/user/LOGIN_SUCCESS';
export const LOGOUT = 'fleet_web/user/LOGOUT';
export const VERIFY_STORED_AUTH_DATA = 'fleet_web/user/VERIFY_STORED_AUTH_DATA';
export const UPDATE_AUTH_ERROR = 'fleet_web/user/UPDATE_AUTH_ERROR';

// Service Requests constants
export const ADD_OR_UPDATE_CURRENT_SERVICE_REQUEST = 'fleet_web/serviceRequests/ADD_OR_UPDATE_CURRENT_SERVICE_REQUEST';
export const ADD_OR_UPDATE_SERVICE_REQUESTS = 'fleet_web/serviceRequests/ADD_OR_UPDATE_SERVICE_REQUESTS';
export const CANCEL_SERVICE_REQUEST = 'fleet_web/serviceRequests/CANCEL_SERVICE_REQUEST';
export const CREATE_SERVICE_REQUEST = 'fleet_web/serviceRequests/CREATE_SERVICE_REQUEST';
export const LOAD_SERVICE_REQUEST = 'fleet_web/serviceRequests/LOAD_SERVICE_REQUEST';
export const LOAD_SERVICE_REQUESTS_REQUEST = 'fleet_web/serviceRequests/LOAD_SERVICE_REQUESTS_REQUEST';
export const LOAD_SERVICE_REQUESTS_SUCCESS = 'fleet_web/serviceRequests/LOAD_SERVICE_REQUESTS_SUCCESS';
export const LOAD_SERVICE_REQUESTS_FAILURE = 'fleet_web/serviceRequests/LOAD_SERVICE_REQUESTS_FAILURE';
export const RESET_SERVICE_REQUESTS_FAVORITES = 'fleet_web/serviceRequests/RESET_SERVICE_REQUESTS_FAVORITES';
export const SELECT_SERVICE_REQUEST = 'fleet_web/serviceRequests/SELECT_SERVICE_REQUEST';
export const SET_CURRENT_SERVICE_REQUEST = 'fleet_web/cases/SET_CURRENT_SERVICE_REQUEST';

// Data Lists constants (reasons for repair, search filters, etc)
export const LOAD_DATA_LISTS = 'fleet_web/dataLists/LOAD_DATA_LISTS';

// Request Management
export const QUEUE_REQUEST = 'fleet_web/requests/QUEUE_REQUEST';
export const ADD_PENDING_REQUEST = 'fleet_web/requests/ADD_PENDING_REQUEST';
export const FINALIZE_REQUEST = 'fleet_web/requests/FINALIZE_REQUEST';
export const ADD_FAILED_REQUEST = 'fleet_web/requests/ADD_FAILED_REQUEST';
export const CLEAR_FAILED_REQUEST = 'fleet_web/requests/CLEAR_FAILED_REQUEST';
export const CLEAR_QUEUED_REQUEST = 'fleet_web/requests/CLEAR_QUEUED_REQUEST';
