import nock from 'nock';

import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import * as api from '../api';

const testDomain = process.env.API_BASE_URL;

test('getUserProfile fetches the profile from the expected API endpoint', async () => {
  const response = [{ response: 'success' }];
  const httpMock = nock(testDomain);
  httpMock
    .get('/profile')
    .reply(200, response);

  const { response: httpResponse } = await api.getUserProfile();

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});
