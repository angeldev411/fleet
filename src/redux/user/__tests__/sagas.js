import { fromJS } from 'immutable';
import SagaTester from 'redux-saga-tester';
import { test, expect, spyOn } from '__tests__/helpers/test-setup';

import Authentication from 'plugins/Authentication';
import createReducer from 'setup/reducer';

import * as actions from '../actions';
import * as sagas from '../sagas';

const initialState = fromJS({});
const reducers = createReducer();

test('verifyStoredAuthData when not authorized', async () => {
  spyOn(Authentication, 'authData').andReturn({ isAuthorized: false });
  const logoutSpy = spyOn(Authentication, 'logout');

  const sagaTester = new SagaTester({ initialState });
  sagaTester.start(sagas.verifyStoredAuthDataWatcher);
  sagaTester.dispatch(actions.verifyStoredAuthData());

  await sagaTester.waitFor(actions.addOrUpdateAuthData);

  expect(logoutSpy).toHaveBeenCalled();
  expect(sagaTester.getLatestCalledAction())
    .toEqual(actions.addOrUpdateAuthData({ isAuthorized: false }));
});

test('verifyStoredAuthData with soon-to-expire tokens', async () => {
  const tokenExpiration = Date.now() + 10000;
  spyOn(Authentication, 'tokenExpiration').andReturn(tokenExpiration);

  spyOn(Authentication, 'authData').andReturn({
    fromAuthData: true,
    isAuthorized: true,
  });
  const refreshTokenSpy = spyOn(Authentication, 'refreshToken').andReturn({
    fromRefresh: true,
    isAuthorized: true,
  });

  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.verifyStoredAuthDataWatcher);
  sagaTester.dispatch(actions.verifyStoredAuthData());

  await sagaTester.waitFor(actions.addOrUpdateAuthData);
  await sagaTester.waitFor(actions.addOrUpdateAuthData);

  expect(refreshTokenSpy).toHaveBeenCalled();

  const appState = sagaTester.store.getState();
  expect(appState.getIn(['user', 'isAuthorized'])).toBe(true);
});

test('login authorizes user with good response, logout unauthorizes user', async () => {
  const username = 'test_username';
  const password = 'test_password';
  const oem = 'test_oem';

  const loginSpy = spyOn(Authentication, 'login').andReturn({
    isAuthorized: true,
    fromLogin: true,
  });

  const logoutSpy = spyOn(Authentication, 'logout');

  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.loginWatcher);
  const loginPayload = { username, password, oem };
  sagaTester.dispatch(actions.login(loginPayload));

  await sagaTester.waitFor(actions.addOrUpdateAuthData);

  expect(loginSpy).toHaveBeenCalledWith(loginPayload);

  const appState = sagaTester.store.getState();
  expect(appState.getIn(['user', 'isAuthorized'])).toBe(true);

  sagaTester.dispatch(actions.logout());

  await sagaTester.waitFor(actions.addOrUpdateAuthData);

  expect(logoutSpy).toHaveBeenCalled();

  const nextAppState = sagaTester.store.getState();
  expect(nextAppState.getIn(['user', 'isAuthorized'])).toBe(false);
});

test('login does not authorize user with error response', async () => {
  const username = 'test_username';
  const password = 'test_password';
  const oem = 'test_oem';

  const loginSpy = spyOn(Authentication, 'login').andReturn({
    isAuthorized: false,
    fromLogin: true,
  });
  const logoutSpy = spyOn(Authentication, 'logout');

  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.loginWatcher);
  const loginPayload = { username, password, oem };
  sagaTester.dispatch(actions.login(loginPayload));

  await sagaTester.waitFor(actions.addOrUpdateAuthData);

  expect(logoutSpy).toHaveBeenCalled();
  expect(loginSpy).toHaveBeenCalledWith(loginPayload);

  const appState = sagaTester.store.getState();
  expect(appState.getIn(['user', 'isAuthorized'])).toBe(false);
});
