import { createAction } from 'redux-actions';

import { createRequestAction } from 'utils/redux-request-actions';

import {
  getUserProfile,
} from './api';

import {
  ADD_OR_UPDATE_AUTH_DATA,
  CLEAR_FAILED_REQUEST,
  CLEAR_LOGIN_REQUEST_ERROR,
  LOAD_USER_PROFILE,
  LOGIN,
  LOGIN_SUCCESS,
  LOGOUT,
  VERIFY_STORED_AUTH_DATA,
} from '../constants';

export const addOrUpdateAuthData = createAction(ADD_OR_UPDATE_AUTH_DATA);

export const loginSuccess = createAction(LOGIN_SUCCESS);

export const clearLoginRequestError = createAction(CLEAR_LOGIN_REQUEST_ERROR);

export const login = createAction(LOGIN);

export const logout = createAction(LOGOUT);

export const verifyStoredAuthData = createAction(VERIFY_STORED_AUTH_DATA);

export const loadUserProfile = createRequestAction(LOAD_USER_PROFILE, getUserProfile);

export const clearAuthRequestError = createAction(
  CLEAR_FAILED_REQUEST,
  () => ({ requestType: LOAD_USER_PROFILE }),
);
