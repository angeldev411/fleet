import { createSelector } from 'reselect';

const userStoreSelector = state => state.get('user');

export const isAuthorizedSelector = createSelector(
  userStoreSelector,
  userStore => userStore.get('isAuthorized'),
);

export const loginRequestErrorSelector = createSelector(
  userStoreSelector,
  userStore => userStore.get('loginRequestError'),
);


export const userProfileSelector = createSelector(
  userStoreSelector,
  userStore => userStore.get('profile'),
);

export const usernameSelector = createSelector(
  userStoreSelector,
  userStore => userStore.get('username'),
);
