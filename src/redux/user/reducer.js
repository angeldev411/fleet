import { fromJS } from 'immutable';
import { handleActions } from 'redux-actions';

import {
  ADD_OR_UPDATE_AUTH_DATA,
  CLEAR_LOGIN_REQUEST_ERROR,
  LOAD_USER_PROFILE,
  LOGIN_SUCCESS,
} from '../constants';

export const initialState = fromJS({
  isAuthorized: false,
  loginRequestError: false,
  profile: {},
});

function addOrUpdateAuthDataHandler(state, action) {
  const {
    isAuthorized,
  } = action.payload;
  return state
    .update('isAuthorized', () => isAuthorized)
    .update('loginRequestError', () => !isAuthorized);
}

function clearLoginRequestError(state) {
  return state.update('loginRequestError', () => false);
}

function loadUserProfileHandler(state, action) {
  return state
    .set('profile', fromJS(action.payload.body));
}

function updateLoginData(state, action) {
  return state.update('username', () => action.payload.username);
}

export default handleActions({
  [ADD_OR_UPDATE_AUTH_DATA]: addOrUpdateAuthDataHandler,
  [CLEAR_LOGIN_REQUEST_ERROR]: clearLoginRequestError,
  [LOAD_USER_PROFILE]: loadUserProfileHandler,
  [LOGIN_SUCCESS]: updateLoginData,
}, initialState);
