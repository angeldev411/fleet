import { put, call, cancel, fork, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import { initializePendo } from 'setup/Pendo';

import Authentication from 'plugins/Authentication';

import {
  LOAD_USER_PROFILE,
  LOGIN,
  LOGOUT,
  VERIFY_STORED_AUTH_DATA,
} from '../constants';

import {
  addOrUpdateAuthData,
  loginSuccess,
} from './actions';

const TOKEN_REFRESH_WINDOW_MS = 60000;

// Terminates the token refresh service and clears all stored auth token data.
function* logout(service) {
  console.log('LOGGING OUT');
  try {
    yield cancel(service);
  } catch (e) {
    console.log('NO ACTIVE TOKEN REFRESH SERVICE');
  } finally {
    yield call(Authentication.logout);
    yield put(addOrUpdateAuthData({ isAuthorized: false }));
  }
}

// Refreshes token approximately 60 seconds before it expires.
// Updates auth data in local storage and in state.
// Runs continuously until the user logs out. The infinite loop pattern you see below
// does not cause issues within a generator and is considered normal practice.
function* tokenRefreshService(authData) {
  let currentAuthData = authData;
  while (true) {
    const remainingTokenTime = Authentication.tokenExpiration() - Date.now();
    const tokenIsExpiring = remainingTokenTime <= TOKEN_REFRESH_WINDOW_MS;
    if (tokenIsExpiring) {
      currentAuthData = yield call(Authentication.refreshToken);

      if (!currentAuthData.isAuthorized) {
        yield cancel();
        // cancel() will abort the service and nothing below will execute
      }

      // eslint-disable-next-line no-use-before-define
      yield call(processAuthData, currentAuthData, { startRefreshService: false });
    }

    const expiresAt = Authentication.tokenExpiration();
    const timeToRefresh = (expiresAt - Date.now() - 30000);
    yield delay(timeToRefresh);
  }
}

// Spawns the token refresh service. Terminates the service when user logs out.
function* startTokenRefreshService(authData) {
  const service = yield fork(tokenRefreshService, authData);
  yield takeLatest(LOGOUT, logout, service);
}

// Examines expiration data of tokens to determine if tokens are expired.
// May trigger clearing auth data if user is not authorized.
// May trigger saving auth data if user is authorized.
// May trigger spawning a token refresh service if the correct option is provided.
function* processAuthData(data, { startRefreshService = true } = {}) {
  if (data.isAuthorized) {
    if (startRefreshService) {
      yield fork(startTokenRefreshService, data);
      yield put(addOrUpdateAuthData(data));
      yield put(loginSuccess(data));
    }
  } else {
    yield call(Authentication.logout);
    yield put(addOrUpdateAuthData({ isAuthorized: false }));
  }
}

/* ------------------------------- worker sagas ------------------------------- */

function* loadUserProfileWorker({ payload }) {
  yield call(initializePendo, payload.body);
}

// Takes user inputs and retrieves new auth tokens via the api.
function* loginWorker({ payload }) {
  const response = yield call(Authentication.login, payload);
  if (response.isAuthorized) {
    yield call(processAuthData, response);
  } else {
    yield call(Authentication.logout);
    yield put(addOrUpdateAuthData(response));
  }
}

// Gets and processes available token data from local storage.
// This runs on every full page load.
function* verifyStoredAuthDataWorker() {
  const authData = yield call(Authentication.authData);
  yield call(processAuthData, authData);
}

/* ------------------------------- watcher sagas ------------------------------- */

function* loadUserProfileWatcher() {
  yield takeLatest(LOAD_USER_PROFILE, loadUserProfileWorker);
}

export function* loginWatcher() {
  yield takeLatest(LOGIN, loginWorker);
}

export function* verifyStoredAuthDataWatcher() {
  yield takeLatest(VERIFY_STORED_AUTH_DATA, verifyStoredAuthDataWorker);
}

// export all of the watcher sagas.
export default [
  loadUserProfileWatcher(),
  loginWatcher(),
  verifyStoredAuthDataWatcher(),
];
