import { createApiAction } from 'utils/redux-api-actions';
import { createAction } from 'redux-actions';

import { createRequestAction } from 'utils/redux-request-actions';

import {
  getServiceRequest,
  getServiceRequests,
  patchServiceRequest,
  postServiceRequest,
} from './api';

import {
  ADD_OR_UPDATE_CURRENT_SERVICE_REQUEST,
  ADD_OR_UPDATE_SERVICE_REQUESTS,
  CANCEL_SERVICE_REQUEST,
  CREATE_SERVICE_REQUEST,
  LOAD_SERVICE_REQUEST,
  LOAD_SERVICE_REQUESTS_REQUEST,
  LOAD_SERVICE_REQUESTS_SUCCESS,
  LOAD_SERVICE_REQUESTS_FAILURE,
  RESET_SERVICE_REQUESTS_FAVORITES,
  SET_CURRENT_SERVICE_REQUEST,
  SELECT_SERVICE_REQUEST,
} from '../constants';

// redux-actions allows us to create very versatile action creators in a single line.
export const addOrUpdateCurrentServiceRequest =
  createAction(ADD_OR_UPDATE_CURRENT_SERVICE_REQUEST);

export const addOrUpdateServiceRequests = createAction(ADD_OR_UPDATE_SERVICE_REQUESTS);

export const cancelServiceRequest = createRequestAction(
  CANCEL_SERVICE_REQUEST,
  patchServiceRequest,
);

export const createServiceRequest = createRequestAction(
  CREATE_SERVICE_REQUEST,
  postServiceRequest,
);

export const clearServiceRequests = createAction(RESET_SERVICE_REQUESTS_FAVORITES);

export const loadServiceRequest = createRequestAction(
  LOAD_SERVICE_REQUEST,
  getServiceRequest,
);

export const loadServiceRequests = createApiAction(
  [LOAD_SERVICE_REQUESTS_REQUEST, LOAD_SERVICE_REQUESTS_SUCCESS, LOAD_SERVICE_REQUESTS_FAILURE],
  getServiceRequests,
);

export const setCurrentServiceRequest = createAction(SET_CURRENT_SERVICE_REQUEST);

export const selectServiceRequest = createAction(SELECT_SERVICE_REQUEST);
