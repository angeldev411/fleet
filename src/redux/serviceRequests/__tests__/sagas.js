import { fromJS, OrderedSet, Map } from 'immutable';
import SagaTester from 'redux-saga-tester';
import { test, expect } from '__tests__/helpers/test-setup';

import createReducer from 'setup/reducer';

import {
  LOAD_SERVICE_REQUEST,
  LOAD_SERVICE_REQUESTS_SUCCESS,
} from '../../constants';
import * as sagas from '../sagas';

const initialState = fromJS({});
const reducers = createReducer();

function buildServiceRequest(serviceRequestId) {
  return {
    id: serviceRequestId,
  };
}

test('loadServiceRequestSuccessWorker', () => {
  const serviceRequestId = '456';
  const currentServiceRequest = buildServiceRequest(serviceRequestId);

  const response = {
    body: currentServiceRequest,
  };

  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.loadServiceRequestWatcher);
  sagaTester.dispatch({
    type: LOAD_SERVICE_REQUEST,
    payload: response,
  });

  const appState = sagaTester.store.getState();

  expect(
    appState.getIn(['serviceRequests', 'responseIds']),
  ).toEqual(OrderedSet([serviceRequestId]));
  expect(
    appState.getIn(['serviceRequests', 'byId', serviceRequestId]),
  ).toEqual(Map({ id: serviceRequestId }));
  expect(
    appState.getIn(['serviceRequests', 'currentServiceRequestId']),
  ).toEqual(serviceRequestId);
});

test('loadServiceRequestsSuccessWorker', () => {
  const serviceRequestId1 = '789';
  const serviceRequest1 = buildServiceRequest(serviceRequestId1);

  const serviceRequestId2 = 'ghi';
  const serviceRequest2 = buildServiceRequest(serviceRequestId2);

  const serviceRequests = [serviceRequest1, serviceRequest2];

  const response = {
    body: serviceRequests,
    headers: Map({ 'x-total-count': '2' }),
  };

  const sagaTester = new SagaTester({ initialState, reducers });
  sagaTester.start(sagas.loadServiceRequestsSuccessWatcher);
  sagaTester.dispatch({
    type: LOAD_SERVICE_REQUESTS_SUCCESS,
    payload: response,
  });

  const appState = sagaTester.store.getState();
  expect(appState.getIn(['serviceRequests', 'responseIds']))
    .toEqual(OrderedSet([serviceRequestId1, serviceRequestId2]));
  expect(appState.getIn(['serviceRequests', 'byId', serviceRequestId1]))
    .toEqual(Map({ id: serviceRequestId1 }));
  expect(appState.getIn(['serviceRequests', 'byId', serviceRequestId2]))
    .toEqual(Map({ id: serviceRequestId2 }));
  expect(appState.getIn(['serviceRequests', 'currentServiceRequestId'])).toEqual('');
  expect(appState.getIn(['app', 'favoritePagination', 'totalCount'])).toEqual(2);
});
