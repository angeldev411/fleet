import { fromJS } from 'immutable';

import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import * as selectors from '../selectors';

test('currentServiceRequestSelector returns the data for the correct id', () => {
  const testState = fromJS({
    serviceRequests: {
      currentServiceRequestId: '2',
      byId: {
        3: { id: '3' },
        1: { id: '1' },
        2: { id: '2' },
      },
    },
  });
  expect(selectors.currentServiceRequestSelector(testState).toJS()).toEqual({ id: '2' });
});

test('currentServiceRequestSelector returns Map if id not found', () => {
  const testState = fromJS({
    serviceRequests: {
      currentServiceRequestId: '2',
      byId: {
        3: { id: '3' },
        1: { id: '1' },
      },
    },
  });
  expect(selectors.currentServiceRequestSelector(testState).toJS()).toEqual({});
});

test('currentServiceRequestAssetSelector returns the data for the correct id', () => {
  const testState = fromJS({
    assets: {
      byId: {
        6: { id: '6' },
        4: { id: '4' },
        5: { id: '5' },
      },
    },
    serviceRequests: {
      currentServiceRequestId: '2',
      byId: {
        3: { id: '3', asset: '6' },
        1: { id: '1', asset: '4' },
        2: { id: '2', asset: '5' },
      },
    },
  });
  expect(selectors.currentServiceRequestAssetSelector(testState).toJS()).toEqual({ id: '5' });
});

test('currentServiceRequestAssetSelector returns Map if id not found', () => {
  const testState = fromJS({
    assets: {
      byId: {
        6: { id: '6' },
        4: { id: '4' },
      },
    },
    serviceRequests: {
      currentServiceRequestId: '2',
      byId: {
        3: { id: '3', asset: '6' },
        1: { id: '1', asset: '4' },
        2: { id: '2', asset: '5' },
      },
    },
  });
  expect(selectors.currentServiceRequestAssetSelector(testState).toJS()).toEqual({});
});

test('currentServReqServProvSelector returns the correct service provider info', () => {
  const testState = fromJS({
    serviceRequests: {
      currentServiceRequestId: '2',
      byId: {
        3: {
          id: '3',
          asset: '6',
          serviceProvider: {
            id: 'VASCC3',
            companyName: 'Volvo',
            phone: '(000) 222-2222',
          },
        },
        1: {
          id: '1',
          asset: '4',
          serviceProvider: {
            id: 'VASCC1',
            companyName: 'Mark',
            phone: '(000) 111-1111',
          },
        },
        2: {
          id: '2',
          asset: '5',
          serviceProvider: {
            id: 'VASCC1',
            companyName: 'Uptime Center',
            phone: '(000) 111-1111',
          },
        },
      },
    },
  });
  expect(selectors.currentServReqServProvSelector(testState))
    .toEqual(testState.getIn(['serviceRequests', 'byId', '2', 'serviceProvider']));
});

test('serviceRequestsRequestingSelector returns requesting', () => {
  const testState = fromJS({
    serviceRequests: {
      byId: {
        3: { id: '3', asset: '6' },
        1: { id: '1', asset: '4' },
        2: { id: '2', asset: '5' },
      },
      requesting: true,
    },
  });
  expect(selectors.serviceRequestsRequestingSelector(testState)).toEqual(true);
});
