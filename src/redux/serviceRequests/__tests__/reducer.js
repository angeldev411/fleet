import { fromJS, OrderedSet } from 'immutable';

import { test, expect } from '__tests__/helpers/test-setup';

import * as actions from '../actions';
import reducer, { initialState } from '../reducer';
import {
  LOAD_SERVICE_REQUESTS_REQUEST,
  LOAD_SERVICE_REQUESTS_SUCCESS,
  LOAD_SERVICE_REQUESTS_FAILURE,
} from '../../constants';

test('calling addOrUpdateServiceRequests action with no data does not change responseIds or byId', () => {
  const action = actions.addOrUpdateServiceRequests({ payload: {} });
  const state = fromJS({
    responseIds: OrderedSet(['123', '321']),
    byId: {
      123: { id: 123 },
      321: { id: 321 },
    },
  });
  const newState = reducer(state, action);
  expect(newState.get('responseIds')).toEqual(state.get('responseIds'));
  expect(newState.get('byId')).toEqual(state.get('byId'));
});

test(
  'calling addOrUpdateCurrentServiceRequest action with no data does not change responseIds or byId',
  () => {
    const action = actions.addOrUpdateCurrentServiceRequest({ payload: {} });
    const state = fromJS({
      responseIds: OrderedSet(['123', '321']),
      byId: {
        123: { id: 123 },
        321: { id: 321 },
      },
    });
    const newState = reducer(state, action);
    expect(newState.get('responseIds')).toEqual(state.get('responseIds'));
    expect(newState.get('byId')).toEqual(state.get('byId'));
  },
);

test('action with type LOAD_SERVICE_REQUESTS_REQUEST sets requesting to true', () => {
  const newState = reducer(initialState, { type: LOAD_SERVICE_REQUESTS_REQUEST });
  expect(newState.get('requesting')).toEqual(true);
});

test('action with type LOAD_SERVICE_REQUESTS_SUCCESS sets requesting to false in state', () => {
  const testState = initialState.set('requesting', true);
  expect(testState.get('requesting')).toEqual(true);
  const newState = reducer(testState, { type: LOAD_SERVICE_REQUESTS_SUCCESS });
  expect(newState.get('requesting')).toEqual(false);
});

test('action with type LOAD_SERVICE_REQUESTS_FAILURE sets requesting to false and saves error', () => {
  const testState = initialState.set('requesting', true);
  expect(testState.get('requesting')).toEqual(true);
  const action = {
    type: LOAD_SERVICE_REQUESTS_FAILURE,
    error: true,
    payload: { error: 'invalid query' },
  };
  const newState = reducer(testState, action);
  expect(newState.get('requesting')).toEqual(false);
  expect(newState.get('error')).toEqual(action.payload.response);
});

test('calling setCurrentServiceRequest action updates serviceRequests.currentServiceRequestId in state', () => {
  const currentServiceRequestId = '123';

  const action = actions.setCurrentServiceRequest(currentServiceRequestId);
  const newState = reducer(initialState, action);

  expect(newState.get('currentServiceRequestId')).toEqual(currentServiceRequestId);
});
