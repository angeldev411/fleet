import nock from 'nock';
import { test, expect } from '__tests__/helpers/test-setup';

import * as api from '../api';

const testDomain = process.env.API_BASE_URL;

test('getServiceRequests reaches the expected route and returns the response', async () => {
  const scope = 'test-scope';
  const perPage = 42;
  const response = [{ response: 'success' }];
  const httpMock = nock(testDomain);
  httpMock.get('/service_requests')
    .query({
      scope,
      per_page: perPage,
    })
    .reply(200, response);

  const { response: httpResponse } = await api.getServiceRequests({ scope, per_page: perPage });

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});

test('getServiceRequest reaches the expected route and returns the response', async () => {
  const serviceRequestId = '12345';
  const response = { response: 'success' };
  const httpMock = nock(testDomain);
  httpMock.get(`/service_requests/${serviceRequestId}`).reply(200, response);

  const { response: httpResponse } = await api.getServiceRequest({ serviceRequestId });

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});

test('patchServiceRequest reaches the expected route and returns the response', async () => {
  const serviceRequestId = '12345';
  const response = { status: 'success' };
  const httpMock = nock(testDomain);

  httpMock.patch(`/service_requests/${serviceRequestId}`).reply(200, response);

  const { response: httpResponse } = await api.patchServiceRequest({ serviceRequestId });

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});

test('patchServiceRequest reaches the expected route and returns the response', async () => {
  const requestBody = { assetId: '1' };
  const response = { status: 'success' };
  const httpMock = nock(testDomain);

  httpMock.post('/service_requests', requestBody).reply(200, response);

  const { response: httpResponse } = await api.postServiceRequest(requestBody);

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});
