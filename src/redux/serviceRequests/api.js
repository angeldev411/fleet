import endpoints from 'apiEndpoints';
import {
  get,
  patch,
  post,
} from 'utils/fetch';

export function getServiceRequest({ serviceRequestId }) {
  return get(endpoints.serviceRequest({ serviceRequestId }));
}

export function getServiceRequests(query) {
  return get(endpoints.serviceRequests(), { query });
}

export function patchServiceRequest({ serviceRequestId }) {
  return patch(endpoints.serviceRequest({ serviceRequestId }));
}

export function postServiceRequest(requestBody) {
  return post(endpoints.serviceRequests(), { requestBody });
}
