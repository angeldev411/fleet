import { fromJS, Map, OrderedSet } from 'immutable';
import { handleActions, combineActions } from 'redux-actions';

import {
  ADD_OR_UPDATE_CURRENT_SERVICE_REQUEST,
  ADD_OR_UPDATE_SERVICE_REQUESTS,
  LOAD_SERVICE_REQUESTS_REQUEST,
  LOAD_SERVICE_REQUESTS_SUCCESS,
  LOAD_SERVICE_REQUESTS_FAILURE,
  LOGOUT,
  RESET_SERVICE_REQUESTS_FAVORITES,
  SET_CURRENT_SERVICE_REQUEST,
  SELECT_SERVICE_REQUEST,
} from '../constants';

export const initialState = fromJS({
  responseIds: OrderedSet(),
  byId: {},
  currentServiceRequestId: '',
  selectedServiceRequestId: '',
  requesting: false,
  error: {},
});

function addOrUpdateServiceRequestsHandler(state, action) {
  const {
    entities: {
      serviceRequest: newServiceRequests = {},
    } = {},
    result = [],
  } = action.payload;

  const currentAllIds = state.get('responseIds');
  const newAllIds = currentAllIds.concat(result);

  const currentById = state.get('byId');
  const newById = currentById.merge(currentById, { ...newServiceRequests });

  return state
    .set('responseIds', newAllIds)
    .set('byId', newById);
}

function addOrUpdateCurrentServiceRequestHandler(state, action) {
  const {
    entities: {
      serviceRequest: newServiceRequests = {},
    } = {},
  } = action.payload;
  const serviceRequestId = Object.keys(newServiceRequests)[0];

  const currentServiceRequestInfo = state.getIn(['byId', serviceRequestId]) || Map();
  const newServiceRequestInfo = currentServiceRequestInfo.merge(
    currentServiceRequestInfo,
    newServiceRequests[serviceRequestId],
  );

  return state
    .update(
      'responseIds',
      orderedSet => orderedSet.add(serviceRequestId).filter(id => !!id),
    )
    .update(
      'byId',
      map => map.set(serviceRequestId, newServiceRequestInfo).filter((val, key) => !!key),
    )
    .set('currentServiceRequestId', serviceRequestId)
    .set('requesting', false);
}

function loadServiceRequestsRequestHandler(state) {
  return state
    .set('requesting', true)
    .set('error', Map());
}

function loadServiceRequestsSuccessHandler(state) {
  return state
    .set('requesting', false)
    .set('error', Map());
}

function loadServiceRequestsFailureHandler(state, action) {
  return state
    .set('requesting', false)
    .set('error', action.payload.response);
}

function resetState(state) {
  return state
    .set('responseIds', OrderedSet())
    .set('byId', Map())
    .set('requesting', false);
}

function setCurrentServiceRequestHandler(state, action) {
  return state
    .set('currentServiceRequestId', action.payload)
    .set('error', Map());
}

/**
 *  selectServiceRequestHandler is called when the Card or ListRow is selected in
 *  ServiceRequest favorite page
 */
function selectServiceRequestHandler(state, action) {
  const selectedServiceRequestId = state.get('selectedServiceRequestId');
  const id = selectedServiceRequestId === action.payload ? '' : action.payload;
  return state
    .set('selectedServiceRequestId', id);
}

export default handleActions({
  [ADD_OR_UPDATE_CURRENT_SERVICE_REQUEST]: addOrUpdateCurrentServiceRequestHandler,
  [ADD_OR_UPDATE_SERVICE_REQUESTS]: addOrUpdateServiceRequestsHandler,
  [combineActions(RESET_SERVICE_REQUESTS_FAVORITES, LOGOUT)]: resetState,
  [LOAD_SERVICE_REQUESTS_REQUEST]: loadServiceRequestsRequestHandler,
  [LOAD_SERVICE_REQUESTS_SUCCESS]: loadServiceRequestsSuccessHandler,
  [LOAD_SERVICE_REQUESTS_FAILURE]: loadServiceRequestsFailureHandler,
  [SET_CURRENT_SERVICE_REQUEST]: setCurrentServiceRequestHandler,
  [SELECT_SERVICE_REQUEST]: selectServiceRequestHandler,
}, initialState);
