import { normalize } from 'normalizr';
import { put, call, takeLatest } from 'redux-saga/effects';

import { setFavoritePagination } from 'redux/app/actions';

import {
  serviceRequestSchema,
  serviceRequestsSchema,
} from '../schemas';

import {
  CREATE_SERVICE_REQUEST,
  LOAD_SERVICE_REQUEST,
  LOAD_SERVICE_REQUESTS_SUCCESS,
} from '../constants';

import {
  addOrUpdateCurrentServiceRequest,
  addOrUpdateServiceRequests,
  loadServiceRequest,
} from './actions';

// worker sagas
function* createServiceRequestWorker({ payload: response }) {
  yield put(loadServiceRequest({ serviceRequestId: response.body.id }));
}

function* loadServiceRequestWorker({ payload: response }) {
  const normalizedData = yield call(normalize, response.body, serviceRequestSchema);
  yield put(addOrUpdateCurrentServiceRequest(normalizedData));
}

function* loadServiceRequestsSuccessWorker({ payload: response }) {
  const normalizedData = yield call(normalize, response.body, serviceRequestsSchema);
  const pagination = {
    latestPage: response.headers.get('x-page'),
    perPage: response.headers.get('x-per-page'),
    totalCount: response.headers.get('x-total-count'),
    totalPages: response.headers.get('x-total-pages'),
  };

  yield put(addOrUpdateServiceRequests(normalizedData));
  yield put(setFavoritePagination(pagination));
}

// watcher sagas
export function* createServiceRequestWatcher() {
  yield takeLatest(CREATE_SERVICE_REQUEST, createServiceRequestWorker);
}

export function* loadServiceRequestWatcher() {
  yield takeLatest(LOAD_SERVICE_REQUEST, loadServiceRequestWorker);
}

export function* loadServiceRequestsSuccessWatcher() {
  yield takeLatest(LOAD_SERVICE_REQUESTS_SUCCESS, loadServiceRequestsSuccessWorker);
}

// export all of the sagas.
export default [
  createServiceRequestWatcher(),
  loadServiceRequestWatcher(),
  loadServiceRequestsSuccessWatcher(),
];
