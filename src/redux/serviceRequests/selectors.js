import { Map } from 'immutable';
import { createSelector } from 'reselect';

import { assetsStoreSelector } from 'redux/assets/selectors';

// For convenience, extract the serviceRequests section of the store.
const serviceRequestsStoreSelector = state => state.get('serviceRequests');

const currentServiceRequestIdSelector = createSelector(
  serviceRequestsStoreSelector,
  serviceRequestStore => serviceRequestStore.get('currentServiceRequestId'),
);

export const currentServiceRequestSelector = createSelector(
  serviceRequestsStoreSelector,
  currentServiceRequestIdSelector,
  (
    serviceRequestsStore,
    currentServiceRequestId,
  ) => (
    (currentServiceRequestId && serviceRequestsStore.getIn(['byId', currentServiceRequestId])) ||
    Map()
  ),
);

export const currentServiceRequestAssetSelector = createSelector(
  currentServiceRequestSelector,
  assetsStoreSelector,
  (
    currentServiceRequest,
    assetsStore,
  ) => (
    (currentServiceRequest && assetsStore.getIn(['byId', currentServiceRequest.get('asset')])) ||
    Map()
  ),
);

export const currentServReqServProvSelector = createSelector(
  currentServiceRequestSelector,
  currentServiceRequest =>
    (currentServiceRequest && currentServiceRequest.get('serviceProvider')) || Map(),
);

export const serviceRequestsRequestingSelector = createSelector(
  serviceRequestsStoreSelector,
  serviceRequestsStore => serviceRequestsStore.get('requesting'),
);

// When all of the cases are required we convert serviceRequests.byId into an immutable list.
export const serviceRequestsSelector = createSelector(
  serviceRequestsStoreSelector,
  serviceRequestsStore => serviceRequestsStore.get('responseIds').map(id =>
    serviceRequestsStore.getIn(['byId', id])).toList(),
);

export const selectedServiceRequestSelector = createSelector(
  serviceRequestsStoreSelector,
  serviceRequestsStore => serviceRequestsStore.get('selectedServiceRequestId'),
);
