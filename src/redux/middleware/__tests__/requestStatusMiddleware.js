import {
  test,
  expect,
  createSpy,
} from '__tests__/helpers/test-setup';

import requestStatusMiddleware from '../requestStatusMiddleware';

const dispatchSpy = createSpy();
const nextSpy = createSpy();

function simulateStore(action, dispatch = dispatchSpy, next = nextSpy) {
  return requestStatusMiddleware({ dispatch })(next)(action);
}

test('requestStatusMiddleware calls next(action) if key `types` is absent', () => {
  const action = { type: 'test' };
  simulateStore(action);
  expect(nextSpy).toHaveBeenCalledWith(action);
});

test('requestStatusMiddleware throws an error if key `types` is not an array', () => {
  const action = { types: 'test' };
  expect(() => { simulateStore(action); })
    .toThrow('requestStatusMiddleware expected `types` to be an array of 3 strings');
});

test('requestStatusMiddleware throws an error if key `types` does not have 3 properties', () => {
  const action = { types: ['test', 'twoTest'] };
  expect(() => { simulateStore(action); })
    .toThrow('requestStatusMiddleware expected `types` to be an array of 3 strings');
});

test('requestStatusMiddleware throws an error if key `types` contains non-strings', () => {
  const action = { types: ['test', 123, true] };
  expect(() => { simulateStore(action); })
    .toThrow('requestStatusMiddleware expected `types` to be an array of 3 strings');
});

test('requestStatusMiddleware throws an error if key `apiCall` is absent', () => {
  const action = { types: ['request', 'success', 'failure'] };
  expect(() => { simulateStore(action); })
    .toThrow('requestStatusMiddleware expected `apiCall` to be a function');
});

test('requestStatusMiddleware throws an error if key `apiCall` is not a function', () => {
  const action = { types: ['request', 'success', 'failure'], apiCall: 'string' };
  expect(() => { simulateStore(action); })
    .toThrow('requestStatusMiddleware expected `apiCall` to be a function');
});

test('requestStatusMiddleware dispatches the requesting action with the payload', () => {
  const testDispatchSpy = createSpy();
  const action = {
    types: ['request', 'success', 'failure'],
    // apiCall: () => new Promise(() => ({})),
    apiCall: () => Promise.resolve({}),
    payload: { super: 'secret' },
  };
  simulateStore(action, testDispatchSpy);
  expect(testDispatchSpy).toHaveBeenCalledWith({
    type: action.types[0],
    payload: action.payload,
  });
});

test(
  'requestStatusMiddleware dispatches the failure action with the error payload if error returned',
  async () => {
    const testDispatchSpy = createSpy();
    const error = 'oops';
    const action = {
      types: ['request', 'success', 'failure'],
      apiCall: () => new Promise(() => { throw error; }).catch(err => ({ error: err })),
      payload: { super: 'secret' },
    };
    await simulateStore(action, testDispatchSpy);
    expect(testDispatchSpy).toHaveBeenCalledWith({
      type: action.types[2],
      error: true,
      payload: error,
    });
  },
);

test(
  'requestStatusMiddleware dispatches the success action with the response payload if successful',
  async () => {
    const testDispatchSpy = createSpy();
    const response = { success: true };
    const action = {
      types: ['request', 'success', 'failure'],
      apiCall: () => Promise.resolve({ response }),
      payload: { super: 'secret' },
    };
    await simulateStore(action, testDispatchSpy);
    expect(testDispatchSpy).toHaveBeenCalledWith({
      type: action.types[1],
      payload: response,
    });
  },
);
