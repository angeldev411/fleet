import { Map, fromJS } from 'immutable';
import { handleActions, combineActions } from 'redux-actions';

import {
  LOAD_ASSET,
  LOAD_CASE,
  LOAD_DASHBOARD_REQUEST,
  LOAD_DASHBOARD_SUCCESS,
  LOAD_DASHBOARD_FAILURE,
  SET_DASHBOARD_FAVORITES,
  SET_FAVORITE_PAGINATION,
  SET_LATEST_FAVORITE,
  SET_LOADING_STARTED,
  SET_SEARCH_PARAMS,
  SET_SEARCH_PAGINATION,
  CLEAR_LOCATION_WIDGET,
  ADD_PAGE_TO_BREADCRUMBS,
} from '../constants';

export const initialState = fromJS({
  dashboard: Map({
    error: null,
    favorites: Map(),
    requesting: false,
  }),
  favoritePagination: {},
  priorFavorite: {},
  latestFavorite: {},
  loadingStarted: false,
  searchParams: {},
  searchPagination: {
    cases: {},
    assets: {},
  },
  locationWidget: {
    assetInfo: {},
    serviceProviderInfo: {},
  },
  breadcrumbs: {
    current: {},
    previous: {},
  },
});

function loadDashboardRequestHandler(state) {
  return state
    .setIn(['dashboard', 'error'], null)
    .setIn(['dashboard', 'requesting'], true);
}

function loadDashboardSuccessHandler(state) {
  return state
    .setIn(['dashboard', 'error'], null)
    .setIn(['dashboard', 'requesting'], false);
}

function loadDashboardFailureHandler(state, action) {
  return state
    .setIn(['dashboard', 'error'], action.payload.response)
    .setIn(['dashboard', 'requesting'], false);
}

function setDashboardFavoritesHandler(state, action) {
  return state
    .setIn(['dashboard', 'favorites'], action.payload);
}

function setFavoritePagination(state, { payload }) {
  return state.set('favoritePagination', fromJS(payload));
}

function setLatestFavorite(state, { payload }) {
  return state
    .set('priorFavorite', state.get('latestFavorite'))
    .set('latestFavorite', fromJS(payload))
    .setIn(['favoritePagination', 'totalCount'], payload.get('count'))
    .setIn(['breadcrumbs', 'previous'], fromJS({}))
    .setIn(['breadcrumbs', 'current'], fromJS(payload));
}

function setLoadingStarted(state, { payload }) {
  return state.set('loadingStarted', payload);
}

function setSearchParams(state, { payload = {} }) {
  return state.set('searchParams', fromJS(payload));
}

function setSearchPagination(state, { payload }) {
  return state.setIn(['searchPagination', payload.type], fromJS(payload.pagination));
}

function setLocationWidget(state, action) {
  const body = action.payload.body;
  switch (action.type) {
    case LOAD_CASE:
      return state
        .setIn(['locationWidget', 'assetInfo'], fromJS(body.asset))
        .setIn(['locationWidget', 'serviceProviderInfo'], fromJS(body.serviceProvider));
    default:
      return state
        .setIn(['locationWidget', 'assetInfo'], fromJS(body))
        .setIn(['locationWidget', 'serviceProviderInfo'], fromJS({}));
  }
}

function clearLocationWidget(state) {
  return state
    .setIn(['locationWidget', 'assetInfo'], fromJS({}))
    .setIn(['locationWidget', 'serviceProviderInfo'], fromJS({}));
}

function addPageToBreadcrumbs(state, { payload }) {
  const storedCrumbs = state.get('breadcrumbs').toJS();
  if (storedCrumbs.previous.path === payload.path) {
    return state
      .setIn(['breadcrumbs', 'previous'], fromJS({}))
      .setIn(['breadcrumbs', 'current'], fromJS(payload));
  }
  return state
    .setIn(['breadcrumbs', 'previous'], state.getIn(['breadcrumbs', 'current']))
    .setIn(['breadcrumbs', 'current'], fromJS(payload));
}

export default handleActions({
  [combineActions(LOAD_CASE, LOAD_ASSET)]: setLocationWidget,
  [CLEAR_LOCATION_WIDGET]: clearLocationWidget,
  [LOAD_DASHBOARD_REQUEST]: loadDashboardRequestHandler,
  [LOAD_DASHBOARD_SUCCESS]: loadDashboardSuccessHandler,
  [LOAD_DASHBOARD_FAILURE]: loadDashboardFailureHandler,
  [SET_DASHBOARD_FAVORITES]: setDashboardFavoritesHandler,
  [SET_FAVORITE_PAGINATION]: setFavoritePagination,
  [SET_LATEST_FAVORITE]: setLatestFavorite,
  [SET_LOADING_STARTED]: setLoadingStarted,
  [SET_SEARCH_PARAMS]: setSearchParams,
  [SET_SEARCH_PAGINATION]: setSearchPagination,
  [ADD_PAGE_TO_BREADCRUMBS]: addPageToBreadcrumbs,
}, initialState);
