import { createAction } from 'redux-actions';
import { createApiAction } from 'utils/redux-api-actions';

import {
  getDashboard,
} from './api';

import {
  ADD_PAGE_TO_BREADCRUMBS,
  CLEAR_LOCATION_WIDGET,
  LOAD_ASSET,
  LOAD_CASE,
  LOAD_DASHBOARD_FAILURE,
  LOAD_DASHBOARD_REQUEST,
  LOAD_DASHBOARD_SUCCESS,
  PERFORM_SEARCH,
  SET_DASHBOARD_FAVORITES,
  SET_FAVORITE_PAGINATION,
  SET_LATEST_FAVORITE,
  SET_LOADING_STARTED,
  SET_SEARCH_PAGINATION,
  SET_SEARCH_PARAMS,
} from '../constants';

export const addPageToBreadcrumbs = createAction(ADD_PAGE_TO_BREADCRUMBS);

export const clearLocationWidget = createAction(CLEAR_LOCATION_WIDGET);

export const loadDashboard = createApiAction(
  [LOAD_DASHBOARD_REQUEST, LOAD_DASHBOARD_SUCCESS, LOAD_DASHBOARD_FAILURE],
  getDashboard,
);

export const performSearch = createAction(PERFORM_SEARCH);

export const setDashboardFavorites = createAction(SET_DASHBOARD_FAVORITES);

export const setFavoritePagination = createAction(SET_FAVORITE_PAGINATION);

export const setLatestFavorite = createAction(SET_LATEST_FAVORITE);

export const setLoadingStarted = createAction(SET_LOADING_STARTED);

export const setLocationWidget = createAction([LOAD_ASSET, LOAD_CASE]);

export const setSearchPagination = createAction(SET_SEARCH_PAGINATION);

export const setSearchParams = createAction(SET_SEARCH_PARAMS);
