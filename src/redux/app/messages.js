import { defineMessages } from 'react-intl';

const messages = defineMessages({
  //-------------------------------------------------------------------------------------
  // The following are mappings from the hard-coded "alpha" favorites that are provided
  // by the Fleet Mobile API to the display text.  These definitions are included in the favorites
  // data and used with <FormattedMessage /> when the favorites name should be displayed.
  // If any favorites returned from the API change or are added, this list of messages
  // must be modified.

  allRepairCases: {
    id: 'favorites.allRepairCases',
    defaultMessage: 'All Repair Cases',
  },
  downtime2Days: {
    id: 'favorites.downtimeGt2Days',
    defaultMessage: 'Downtime > 2 Days',
  },
  pastDueFollowUp: {
    id: 'favorites.pastDueFollowUp',
    defaultMessage: 'Past Due Follow Up',
  },
  pastDueEtr: {
    id: 'favorites.pastDueETR',
    defaultMessage: 'Past Due ETR',
  },
  current: {
    id: 'favorites.current',
    defaultMessage: 'Current',
  },
  uptimeCases: {
    id: 'favorites.uptimeCases',
    defaultMessage: 'Uptime Cases',
  },
  waitingApproval: {
    id: 'favorites.waitingApproval',
    defaultMessage: 'Waiting Approval',
  },
  openServiceRequest: {
    id: 'favorites.openServiceRequests',
    defaultMessage: 'Open Service Requests',
  },
  allAssets: {
    id: 'favorites.allAssets',
    defaultMessage: 'All Assets',
  },
  highSeverity: {
    id: 'favorites.highSeverity',
    defaultMessage: 'High Severity',
  },
  mediumSeverity: {
    id: 'favorites.mediumSeverity',
    defaultMessage: 'Medium Severity',
  },
  maintenanceOverdue: {
    id: 'favorites.maintenanceOverdue',
    defaultMessage: 'Maintenance Overdue',
  },
  maintenanceDue: {
    id: 'favorites.maintenanceDue',
    defaultMessage: 'Maintenance Due',
  },
  maintenanceCurrent: {
    id: 'favorites.maintenanceCurrent',
    defaultMessage: 'Maintenance Current',
  },
});

export default messages;
