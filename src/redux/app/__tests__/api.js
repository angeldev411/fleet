import nock from 'nock';
import { test, expect } from '__tests__/helpers/test-setup';

import * as api from '../api';

const testDomain = process.env.API_BASE_URL;

test('getDashboard retrieves the API /dashboard data', async () => {
  const response = [{ response: 'success' }];
  const httpMock = nock(testDomain);
  httpMock
    .get('/dashboard')
    .reply(200, response);

  const { response: httpResponse } = await api.getDashboard();

  expect(httpMock.isDone()).toBe(true);
  expect(httpResponse.body).toEqual(response);
});
