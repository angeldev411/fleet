import { test, expect } from '__tests__/helpers/test-setup';
import { fromJS, Map } from 'immutable';

import * as actions from '../actions';

import reducer, { initialState } from '../reducer';

import {
  LOAD_ASSET,
  LOAD_CASE,
  LOAD_DASHBOARD_REQUEST,
  LOAD_DASHBOARD_SUCCESS,
  LOAD_DASHBOARD_FAILURE,
  SET_DASHBOARD_FAVORITES,
} from '../../constants';

// --------------------- Dashboard ----------------------

test('action with type LOAD_DASHBOARD_REQUEST sets dashboard.requesting to true', () => {
  const newState = reducer(initialState, { type: LOAD_DASHBOARD_REQUEST });
  expect(newState.getIn(['dashboard', 'requesting'])).toEqual(true);
});

test('action with type LOAD_DASHBOARD_SUCCESS sets dashboard.requesting to false in state', () => {
  const testState = initialState.setIn(['dashboard', 'requesting'], true);
  const newState = reducer(testState, { type: LOAD_DASHBOARD_SUCCESS });
  expect(newState.getIn(['dashboard', 'requesting'])).toEqual(false);
});

test('action with type LOAD_DASHBOARD_FAILURE sets requesting to false and saves error', () => {
  const testState = initialState.setIn(['dashboard', 'requesting'], true);
  const action = {
    type: LOAD_DASHBOARD_FAILURE,
    error: true,
    payload: { response: 'load dashboard failure' },
  };
  const newState = reducer(testState, action);
  expect(newState.getIn(['dashboard', 'requesting'])).toEqual(false);
  expect(newState.getIn(['dashboard', 'error'])).toEqual(action.payload.response);
});

test('action with type SET_DASHBOARD_FAVORITES updates favorites.', () => {
  const testState = initialState.setIn(['dashboard', 'favorites'], Map());
  const payload = { result: 'testResults' };
  const action = {
    type: SET_DASHBOARD_FAVORITES,
    payload,
  };
  const newState = reducer(testState, action);
  expect(newState.getIn(['dashboard', 'favorites'])).toEqual(payload);
});

test('calling setSearchParams action updates app.searchParams in state', () => {
  const newSearchParams = { searchOptions: { searchParams: 'unit_no' }, searchValue: 'test' };
  const action = actions.setSearchParams(newSearchParams);
  const newState = reducer(initialState, action);

  expect(newState.get('searchParams')).toEqual(fromJS(newSearchParams));
});

test('calling setSearchParams action with no value clears app.searchParams in state', () => {
  const startState = initialState.set('searchParams', fromJS({ searchValue: 'test' }));
  const action = actions.setSearchParams();
  const newState = reducer(startState, action);

  expect(newState.get('searchParams')).toEqual(Map());
});

test('calling setLoadingStarted action updates the state', () => {
  const action = actions.setLoadingStarted(true);
  const newState = reducer(initialState, action);
  expect(newState.get('loadingStarted')).toEqual(true);
});

test('calling setSearchPagination action updates the state', () => {
  const pagination = { latestPage: 5 };
  const payload = {
    type: 'cases',
    pagination,
  };
  const action = actions.setSearchPagination(payload);
  const newState = reducer(initialState, action);

  expect(newState.getIn(['searchPagination', 'cases'])).toEqual(fromJS(pagination));
});

test('action with type LOAD_CASE calls setLocationWidget', () => {
  const asset = { location: { lat: 123, lng: 321 } };
  const serviceProvider = { address: { location: { lat: 123, lng: 321 } } };
  const payload = { body: { asset, serviceProvider } };
  const action = {
    type: LOAD_CASE,
    payload,
  };
  const newState = reducer(initialState, action);
  expect(newState.getIn(['locationWidget', 'assetInfo'])).toEqual(fromJS(asset));
  expect(newState.getIn(['locationWidget', 'serviceProviderInfo']))
    .toEqual(fromJS(serviceProvider));
});

test('action with type LOAD_ASSET calls setLocationWidget', () => {
  const asset = { location: { lat: 123, lng: 321 } };
  const payload = { body: asset };
  const action = {
    type: LOAD_ASSET,
    payload,
  };
  const newState = reducer(initialState, action);
  expect(newState.getIn(['locationWidget', 'assetInfo'])).toEqual(fromJS(asset));
  expect(newState.getIn(['locationWidget', 'serviceProviderInfo'])).toEqual(fromJS({}));
});

test('calling clearLocationWidget action clears locationWidget slice', () => {
  const payload = {
    assetInfo: { location: { lat: 123, lng: 321 } },
    serviceProviderInfo: { address: { location: { lat: 123, lng: 321 } } },
  };
  const startState = initialState.set('locatinWidget', fromJS(payload));
  const action = actions.clearLocationWidget();
  const newState = reducer(startState, action);
  expect(newState.getIn(['locationWidget', 'assetInfo'])).toEqual(fromJS({}));
  expect(newState.getIn(['locationWidget', 'serviceProviderInfo'])).toEqual(fromJS({}));
});

test('addPageToBreadcrumbs should move current to previous then set the current crumb if previous is empty', () => {
  const breadcrumbs = {
    message: {
      id: 'some-id',
    },
    path: 'some-path',
  };
  const startState = initialState.setIn(['breadcrumbs', 'current'], fromJS(breadcrumbs));
  expect(startState.getIn(['breadcrumbs', 'previous'])).toEqual(fromJS({}));
  const payload = {
    message: {
      id: 'new-id',
    },
    path: 'new-path',
  };
  const action = actions.addPageToBreadcrumbs(payload);
  const newState = reducer(startState, action);
  expect(newState.getIn(['breadcrumbs', 'previous'])).toEqual(fromJS(breadcrumbs));
  expect(newState.getIn(['breadcrumbs', 'current'])).toEqual(fromJS(payload));
});

test('addPageToBreadcrumbs should move remove previous then set the current crumb if previous is the same as current', () => {
  const breadcrumbs = {
    message: {
      id: 'same-id',
    },
    path: 'same-path',
  };
  const startState = initialState.setIn(['breadcrumbs', 'previous'], fromJS(breadcrumbs));
  expect(startState.getIn(['breadcrumbs', 'previous'])).toEqual(fromJS(breadcrumbs));
  const action = actions.addPageToBreadcrumbs(breadcrumbs);
  const newState = reducer(startState, action);
  expect(newState.getIn(['breadcrumbs', 'previous'])).toEqual(fromJS({}));
  expect(newState.getIn(['breadcrumbs', 'current'])).toEqual(fromJS(breadcrumbs));
});
