import { test, expect } from '__tests__/helpers/test-setup';
import { fromJS } from 'immutable';

import {
  appStoreSelector,
  favoritesSelector,
  searchParamsSelector,
  searchPaginationSelector,
  locationWidgetSelector,
  breadcrumbsSelector,
} from '../selectors';

// ------------------------- appStoreSelector -------------------------

test('appStoreSelector returns the app slice from the state', () => {
  const appState = 'important stuff';
  const state = fromJS({ app: appState });
  expect(appStoreSelector(state)).toEqual(appState);
});

// ------------------------- favoritesSelector -------------------------

test('favoritesSelector returns the favoritesSelector slice from the app state', () => {
  const favorites = 'FAVRITZ WUZ HERE';
  const state = fromJS({ app: { dashboard: { favorites } } });
  expect(favoritesSelector(state)).toEqual(favorites);
});

// ------------------------- searchParamsSelector -------------------------

test('searchParamsSelector returns the searchParams slice from the app state', () => {
  const searchParams = {
    test: 'FAVRITZ WUZ HERE',
  };
  const state = fromJS({ app: { searchParams } });
  expect(searchParamsSelector(state)).toEqual(searchParams);
});

// ------------------------- searchPaginationSelector -------------------------

test('searchPaginationSelector returns the searchPagination slice', () => {
  const searchPagination = {
    paginationKey: 'PAGINATION PROPERTIES',
  };
  const state = fromJS({ app: { searchPagination } });
  expect(searchPaginationSelector(state)).toEqual(fromJS(searchPagination));
});

// ------------------------- locationWidgetSelector -------------------------

test('locationWidgetSelector returns the locationWidget slice', () => {
  const locationWidget = {
    assetInfo: {},
    serviceProviderInfo: {},
  };
  const state = fromJS({ app: { locationWidget } });
  expect(locationWidgetSelector(state)).toEqual(locationWidget);
});

// ------------------------- breadcrumbsSelector -------------------------

test('breadcrumbsSelector returns the breadcrumbs slice from the app state', () => {
  const breadcrumbs = {
    previousPath: {
      path: '/some-path',
      message: {
        id: 'some-id',
      },
    },
    currentPath: {},
  };
  const state = fromJS({ app: { breadcrumbs } });
  expect(breadcrumbsSelector(state)).toEqual(breadcrumbs);
});
