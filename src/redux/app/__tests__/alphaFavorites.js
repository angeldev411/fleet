import { test, expect } from '__tests__/helpers/test-setup';

import processDashboard from '../alphaFavorites';
import messages from '../messages';

const apiResponse = [
  {
    category: 'assets',
    total: '360',
    filters: [
      {
        id: null,
        name: 'HIGH SEVERITY',
        total: '61',
        priority: 'red',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fassets?scope=high%20severity\u0026',
      },
      {
        id: null,
        name: 'MAINTENANCE OVERDUE',
        total: '1',
        priority: 'red',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fassets?scope=maintenance%20overdue\u0026',
      },
      {
        id: null,
        name: 'MAINTENANCE DUE',
        total: '1',
        priority: 'yellow',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fassets?scope=maintenance%20due\u0026',
      },
      {
        id: null,
        name: 'MAINTENANCE CURRENT',
        total: '199',
        priority: 'green',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fassets?scope=maintenance%20current\u0026',
      },
    ],
  },
  {
    category: 'cases',
    total: '642',
    filters: [
      {
        id: null,
        name: 'DOWNTIME \u003e 2 DAYS',
        total: '76',
        priority: 'red',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fcases?scope=downtime%20%3E%202%20days\u0026',
      },
      {
        id: null,
        name: 'PAST DUE FOLLOW UP',
        total: '48',
        priority: 'red',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fcases?scope=past%20due%20follow%20up\u0026',
      },
      {
        id: null,
        name: 'PAST DUE ETR',
        total: '8',
        priority: 'red',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fcases?scope=past%20due%20etr\u0026',
      },
      {
        id: null,
        name: 'OPEN SERVICE REQUEST',
        total: '575',
        priority: 'yellow',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fcases?scope=open%20service%20request\u0026',
      },
      {
        id: null,
        name: 'WAITING APPROVAL',
        total: '32',
        priority: 'yellow',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fcases?scope=waiting%20approval\u0026',
      },
      {
        id: null,
        name: 'CURRENT',
        total: '642',
        priority: 'green',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fcases?scope=current\u0026',
      },
      {
        id: null,
        name: 'UPTIME CASES',
        total: '56',
        priority: 'green',
        url: 'https%3A%2F%2Ffleet-mobile-api.test.decisivapps.com%2Fapi%2Fcases?scope=uptime%20cases\u0026',
      },
    ],
  },
];

const processedResult = {
  cases: [
    {
      count: '642',
      message: messages.allRepairCases,
      params: { scope: '' },
      path: '/cases/filter/all-repair-cases',
      slug: 'all-repair-cases',
      title: 'allRepairCases',
    },
    {
      count: '76',
      message: messages.downtime2Days,
      params: { scope: 'downtime > 2 days' },
      path: '/cases/filter/downtime-greater-than-2-days',
      slug: 'downtime-greater-than-2-days',
      title: 'downtime2Days',
    },
    {
      count: '48',
      message: messages.pastDueFollowUp,
      params: { scope: 'past due follow up' },
      path: '/cases/filter/past-due-follow-up',
      slug: 'past-due-follow-up',
      title: 'pastDueFollowUp',
    },
    {
      count: '8',
      message: messages.pastDueEtr,
      params: { scope: 'past due etr' },
      path: '/cases/filter/past-due-etr',
      slug: 'past-due-etr',
      title: 'pastDueEtr',
    },
    {
      count: '56',
      message: messages.uptimeCases,
      params: { scope: 'uptime cases' },
      path: '/cases/filter/uptime-cases',
      slug: 'uptime-cases',
      title: 'uptimeCases',
    },
  ],
  approvals: [
    {
      count: '32',
      message: messages.waitingApproval,
      params: { scope: 'waiting approval' },
      path: '/approvals/filter/waiting-approval',
      slug: 'waiting-approval',
      title: 'waitingApproval',
    },
  ],
  serviceRequests: [
    {
      count: '575',
      message: messages.openServiceRequest,
      params: { scope: 'open service request' },
      path: '/service-requests/filter/open-service-request',
      slug: 'open-service-request',
      title: 'openServiceRequest',
    },
  ],
  assets: [
    {
      count: '360',
      message: messages.allAssets,
      params: { scope: '' },
      path: '/assets/filter/all-assets',
      slug: 'all-assets',
      title: 'allAssets',
    },
    {
      count: '61',
      message: messages.highSeverity,
      params: { scope: 'high severity' },
      path: '/assets/filter/high-severity',
      slug: 'high-severity',
      title: 'highSeverity',
    },
    {
      count: '1',
      message: messages.maintenanceOverdue,
      params: { scope: 'maintenance overdue' },
      path: '/assets/filter/maintenance-overdue',
      slug: 'maintenance-overdue',
      title: 'maintenanceOverdue',
    },
    {
      count: '1',
      message: messages.maintenanceDue,
      params: { scope: 'maintenance due' },
      path: '/assets/filter/maintenance-due',
      slug: 'maintenance-due',
      title: 'maintenanceDue',
    },
    {
      count: '199',
      message: messages.maintenanceCurrent,
      params: { scope: 'maintenance current' },
      path: '/assets/filter/maintenance-current',
      slug: 'maintenance-current',
      title: 'maintenanceCurrent',
    },
  ],
};

test('processing the dashboard API response results in the correct left nav data', () => {
  expect(processDashboard({ body: apiResponse }).toJS()).toEqual(processedResult);
});
