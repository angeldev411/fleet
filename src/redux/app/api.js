import endpoints from 'apiEndpoints';
import { get } from 'utils/fetch';

// eslint-disable-next-line import/prefer-default-export
export function getDashboard() {
  return get(endpoints.dashboard());
}
