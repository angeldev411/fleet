import { all, put, call, cancel, takeLatest } from 'redux-saga/effects';

import {
  loadAssets,
} from 'redux/assets/actions';

import {
  loadCases,
} from 'redux/cases/actions';

import {
  LOAD_DASHBOARD_SUCCESS,
  PERFORM_SEARCH,
} from '../constants';

import {
  setDashboardFavorites,
  setLoadingStarted,
  setSearchParams,
  setSearchPagination,
} from './actions';

import processDashboard from './alphaFavorites';

//---------------------------------------------------------------------------------------
// Workers:

/**
 * Load the left-nav favorites from the dashboard API endpoint.
 */
function* loadDashboardSuccessWorker({ payload: response }) {
  // mutate the API response into the format needed for the left nav:
  const favorites = yield call(processDashboard, response);

  // setup the dashboard favorites:
  yield put(setDashboardFavorites(favorites));
}

function* performSearchWorker({
  payload: rawParams,
  payload: { searchOption, searchValue, ...rest },
}) {
  yield put(setSearchParams(rawParams));
  yield put(setSearchPagination({ type: 'assets', pagination: {} }));
  yield put(setSearchPagination({ type: 'cases', pagination: {} }));

  if (!searchValue || searchValue.length < 3) {
    yield cancel();
  }

  // start loading
  yield put(setLoadingStarted(true));

  const params = { ...rest };
  // TODO: The `all` function in redux-saga is so new at the time this was created
  // that the tools to test it do not yet exist. Revisit this generator function
  // after updating redux-saga-tester and add test coverage.
  /* istanbul ignore next */
  if (searchOption && searchValue) {
    params[searchOption.searchParam] = searchValue;
  }

  yield all([
    put(loadAssets(params)),
    put(loadCases(params)),
  ]);

  yield cancel();
}

//---------------------------------------------------------------------------------------
// Watchers:

function* loadDashboardSuccessWatcher() {
  yield takeLatest(LOAD_DASHBOARD_SUCCESS, loadDashboardSuccessWorker);
}

function* performSearchWatcher() {
  yield takeLatest(PERFORM_SEARCH, performSearchWorker);
}

//---------------------------------------------------------------------------------------
// Exported list of all sagas:

export default [
  loadDashboardSuccessWatcher(),
  performSearchWatcher(),
];
