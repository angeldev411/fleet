import { fromJS } from 'immutable';
import { camelCase, concat, curry, startCase, toLower } from 'lodash';
import { parse as parseQueryString } from 'qs';
import getSlug from 'speakingurl';

import messages from './messages';

function scopeFromUrl(url) {
  let scope = '';
  if (url) {
    const paramString = url.split('?').pop();
    const params = parseQueryString(paramString);
    scope = params.scope;
  }
  return scope;
}

const mapDashboardFilterToFavorite = curry((pathName, { name, url, total }) => {
  const title = camelCase(name);
  const slug = getSlug(name);
  return {
    title,
    params: { scope: scopeFromUrl(url) },
    slug,
    count: total,
    // To prevent the app from failing catastrophically in the event of an untranslated favorite,
    // define a default message based on the the params received.
    message: messages[title] || {
      id: `favorites.${title}`,
      defaultMessage: `${startCase(toLower(name))}`,
    },
    path: `/${pathName}/filter/${slug}`,
  };
});

/**
 * The Dashboard API response includes all case-related favorites in a single collection.
 * In this application we want to separate approvals and service requests into their own
 * categories.  This function is used to extract those filters.  It returns "approvals",
 * "serviceRequests", or "cases" for any given filter name based on which category the
 * filter should appear under.  `null` is returned for favorites which should be ignored
 * and not rendered into any category.
 */
function caseFavoriteSection({ name }) {
  // exclude 'Waiting Approval' and 'Open Service Requests' from the Cases
  // favorite section since they are shown under their own left-nav headings
  switch (name) {
    case 'WAITING APPROVAL':
      return 'approvals';
    case 'OPEN SERVICE REQUEST':
      return 'serviceRequests';
    case 'CURRENT':
      return null;
    default:
      return 'cases';
  }
}

function buildCaseFavorites({ total = 0, filters } = {}) {
  const allCasesFilter = { name: 'All Repair Cases', total, url: '' };
  const caseFilters = filters ?
    filters.filter(entry => caseFavoriteSection(entry) === 'cases') : [];
  const allFilters = concat([allCasesFilter], caseFilters);
  return allFilters.map(mapDashboardFilterToFavorite('cases'));
}

function buildApprovalsFavorites({ filters } = {}) {
  const approvalFilters = filters ?
    filters.filter(entry => caseFavoriteSection(entry) === 'approvals') : [];
  return approvalFilters.map(mapDashboardFilterToFavorite('approvals'));
}

function buildServiceRequestFavorites({ filters } = {}) {
  const serviceRequestFilters = filters ?
    filters.filter(entry => caseFavoriteSection(entry) === 'serviceRequests') : [];
  return serviceRequestFilters.map(mapDashboardFilterToFavorite('service-requests'));
}

function buildAssetFavorites({ total = 0, filters = [] } = {}) {
  const allAssetsFilter = { name: 'All Assets', total, url: '' };
  const allFilters = concat([allAssetsFilter], filters);
  return allFilters.map(mapDashboardFilterToFavorite('assets'));
}

function processDashboard(dashboardResponse) {
  const body = dashboardResponse.body;
  const caseDashboardInfo = body.find(entry => entry.category === 'cases');
  const assetDashboardInfo = body.find(entry => entry.category === 'assets');

  return fromJS({
    cases: buildCaseFavorites(caseDashboardInfo),
    approvals: buildApprovalsFavorites(caseDashboardInfo),
    serviceRequests: buildServiceRequestFavorites(caseDashboardInfo),
    assets: buildAssetFavorites(assetDashboardInfo),
    // reports: buildReportFavorites(),
  });
}

export default processDashboard;
