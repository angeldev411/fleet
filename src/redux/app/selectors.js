import { createSelector } from 'reselect';

// For convenience, extract the app section of the store.
const appStoreSelector = state => state.get('app');

const dashboardRequestingSelector = createSelector(
  appStoreSelector,
  appStore => appStore.getIn(['dashboard', 'requesting']),
);

// get the pagination data for the latest favorite.
const favoritePaginationSelector = createSelector(
  appStoreSelector,
  appStore => appStore.get('favoritePagination'),
);

// get the left nav favorites.
const favoritesSelector = createSelector(
  appStoreSelector,
  appStore => appStore.getIn(['dashboard', 'favorites']),
);

// get the latest favorite
const latestFavoriteSelector = createSelector(
  appStoreSelector,
  appStore => appStore.get('latestFavorite').toJS(),
);

// get the flag that indicates loading has started
const loadingStartedSelector = createSelector(
  appStoreSelector,
  appStore => appStore.get('loadingStarted'),
);

// get the prior favorite
const priorFavoriteSelector = createSelector(
  appStoreSelector,
  appStore => appStore.get('priorFavorite').toJS(),
);

// get the search params
const searchParamsSelector = createSelector(
  appStoreSelector,
  appStore => appStore.get('searchParams').toJS(),
);

const searchPaginationSelector = createSelector(
  appStoreSelector,
  appStore => appStore.get('searchPagination'),
);

const locationWidgetSelector = createSelector(
  appStoreSelector,
  appStore => appStore.get('locationWidget').toJS(),
);

const breadcrumbsSelector = createSelector(
  appStoreSelector,
  appStore => appStore.get('breadcrumbs').toJS(),
);

export {
  appStoreSelector,
  dashboardRequestingSelector,
  favoritePaginationSelector,
  favoritesSelector,
  latestFavoriteSelector,
  loadingStartedSelector,
  priorFavoriteSelector,
  searchParamsSelector,
  searchPaginationSelector,
  locationWidgetSelector,
  breadcrumbsSelector,
};
