import { schema } from 'normalizr';

/* ---------------------- Asset ------------------------ */
export const assetSchema = new schema.Entity('asset');
export const assetsSchema = [assetSchema];

/* ---------------------- Service Provider ------------------------ */
export const serviceProviderSchema = new schema.Entity('serviceProvider');

/* ---------------------- Case ------------------------ */
export const caseSchema = new schema.Entity('case', {
  serviceProvider: serviceProviderSchema,
  asset: assetSchema,
});
export const casesSchema = [caseSchema];

/* ---------------------- Service Request ------------------------ */
export const serviceRequestSchema = new schema.Entity('serviceRequest', {
  asset: assetSchema,
});
export const serviceRequestsSchema = [serviceRequestSchema];
