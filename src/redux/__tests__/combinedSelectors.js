import { fromJS, Map, OrderedSet } from 'immutable';

import { test, expect } from '__tests__/helpers/test-setup';

import {
  loadingSelector,
  noAssetsCasesSelector,
  noAssetsCasesFoundSelector,
  requestingSelector,
} from '../combinedSelectors';

const appStore = {
  dashboard: Map({
    requesting: false,
  }),
  loadingStarted: false,
};

const assetsStore = {
  responseIds: OrderedSet(['123', '456', '789']),
  byId: {
    123: { id: '123' },
    456: { id: '456' },
    789: { id: '789' },
  },
  requesting: true,
};

const casesStore = {
  responseIds: OrderedSet(['111', '222']),
  byId: {
    111: { id: '111' },
    222: { id: '222' },
  },
  requesting: false,
};

const serviceRequestsStore = {
  responseIds: OrderedSet(['321', '432']),
  byId: {
    321: { id: '321' },
    432: { id: '432' },
  },
  requesting: false,
};

const defaultState = fromJS({
  assets: assetsStore,
  cases: casesStore,
  app: appStore,
  serviceRequests: serviceRequestsStore,
});

// ------------------------- requestingSelector -------------------------

test('requestingSelector returns true if either assets, cases, or serviceRequests are being requested', () => {
  expect(requestingSelector(defaultState)).toEqual(true);
});

test('requestingSelector returns false if neither assets, cases, nor serviceRequests are being requested', () => {
  const testAppStore = {
    ...appStore,
    dashboard: Map({ requesting: false }),
  };
  const testAssetsStore = {
    ...assetsStore,
    requesting: false,
  };
  const testCasesStore = {
    ...casesStore,
    requesting: false,
  };
  const testServiceRequestsStore = {
    ...serviceRequestsStore,
    requesting: false,
  };
  const testState = fromJS({
    assets: testAssetsStore,
    cases: testCasesStore,
    app: testAppStore,
    serviceRequests: testServiceRequestsStore,
  });

  expect(requestingSelector(testState)).toEqual(false);
});

// ------------------------- noAssetsCasesSelector -------------------------

test('noAssetsCasesSelector returns false if there is either an asset or a case in the store', () => {
  expect(noAssetsCasesSelector(defaultState)).toEqual(false);
});

test('noAssetsCasesSelector returns true if there is no asset or case in the store', () => {
  const testAppStore = {
    ...appStore,
    dashboard: Map({ requesting: true }),
  };
  const testAssetsStore = {
    responseIds: [],
    byId: {},
    requesting: false,
  };
  const testCasesStore = {
    responseIds: [],
    byId: {},
    requesting: false,
  };
  const testState = fromJS({
    assets: testAssetsStore,
    cases: testCasesStore,
    app: testAppStore,
    serviceRequests: serviceRequestsStore,
  });
  expect(noAssetsCasesSelector(testState)).toEqual(true);
});

// ------------------------- noAssetsCasesFoundSelector -------------------------

test('noAssetsCasesFoundSelector returns false if requesting OR there are assets/cases', () => {
  expect(noAssetsCasesFoundSelector(defaultState)).toEqual(false);
});

test('noAssetsCasesFoundSelector returns true if not requesting AND there no assets/cases', () => {
  const testAppStore = {
    appStore: {
      ...appStore,
      dashboard: Map({ requesting: false }),
    },
  };
  const testAssetsStore = {
    responseIds: [],
    byId: {},
    requesting: false,
  };
  const testCasesStore = {
    responseIds: [],
    byId: {},
    requesting: false,
  };
  const testState = fromJS({
    assets: testAssetsStore,
    cases: testCasesStore,
    app: testAppStore,
    serviceRequests: serviceRequestsStore,
  });
  expect(noAssetsCasesFoundSelector(testState)).toEqual(true);
});

// ------------------------- loadingSelector -------------------------

test('loadingSelector returns true if any of the requesting selectors is true', () => {
  const testAppStore = {
    ...appStore,
    dashboard: Map({ requesting: true }),
  };
  const testAssetsStore = {
    ...assetsStore,
    requesting: false,
    assetCasesRequesting: false,
  };
  const testCasesStore = {
    ...casesStore,
    requesting: false,
    caseRequesting: false,
    caseFaultsRequesting: false,
    caseNotesRequesting: false,
  };
  const testRequestsStore = {
    pending: [],
  };
  const testServiceRequestsStore = {
    ...serviceRequestsStore,
    requesting: false,
  };
  const testState = fromJS({
    assets: testAssetsStore,
    cases: testCasesStore,
    app: testAppStore,
    requests: testRequestsStore,
    serviceRequests: testServiceRequestsStore,
  });

  expect(loadingSelector(testState)).toBe(true);
});

// for test coverage
test('loadingSelector returns true if the last item is true', () => {
  const testAppStore = {
    ...appStore,
    dashboard: Map({ requesting: true }),
  };
  const testAssetsStore = {
    ...assetsStore,
    requesting: false,
  };
  const testCasesStore = {
    ...casesStore,
    requesting: false,
    caseRequesting: false,
    caseFaultsRequesting: false,
    caseNotesRequesting: false,
  };
  const testRequestsStore = {
    pending: [],
  };
  const testServiceRequestsStore = {
    ...serviceRequestsStore,
    requesting: true,
  };
  const testState = fromJS({
    assets: testAssetsStore,
    cases: testCasesStore,
    app: testAppStore,
    requests: testRequestsStore,
    serviceRequests: testServiceRequestsStore,
  });
  expect(loadingSelector(testState)).toBe(true);
});

test('loadingSelector returns false if all of the requesting selectors are false', () => {
  const testAppStore = {
    ...appStore,
    dashboard: Map({ requesting: false }),
  };
  const testAssetsStore = {
    ...assetsStore,
    requesting: false,
    assetCasesRequesting: false,
  };
  const testCasesStore = {
    ...casesStore,
    requesting: false,
    caseRequesting: false,
    caseFaultsRequesting: false,
    caseNotesRequesting: false,
  };
  const testRequestsStore = {
    pending: [],
  };
  const testServiceRequestsStore = {
    ...serviceRequestsStore,
    requesting: false,
  };
  const testState = fromJS({
    assets: testAssetsStore,
    cases: testCasesStore,
    app: testAppStore,
    requests: testRequestsStore,
    serviceRequests: testServiceRequestsStore,
  });
  expect(loadingSelector(testState)).toBe(false);
});
