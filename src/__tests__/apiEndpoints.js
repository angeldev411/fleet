import { test, expect } from '__tests__/helpers/test-setup';

import endpoints, { apiBaseRoute } from 'apiEndpoints';

test('apiBaseRoute is extracted from process.env', () => {
  expect(apiBaseRoute).toEqual(process.env.API_BASE_URL);
});

//-----------------------------------------------------------------------------

const assetId = 'assetId456';

test('assets builds the correct route', () => {
  expect(endpoints.assets())
    .toEqual(`${apiBaseRoute}/assets`);
});

test('asset builds the correct route', () => {
  expect(endpoints.asset({ assetId }))
    .toEqual(`${apiBaseRoute}/assets/${assetId}`);
});

test('assetScheduledMaintenances builds the correct route', () => {
  expect(endpoints.assetScheduledMaintenances({ assetId }))
    .toEqual(`${apiBaseRoute}/assets/${assetId}/scheduled_maintenances`);
});

test('assetCases builds the correct route', () => {
  expect(endpoints.assetCases({ assetId }))
    .toEqual(`${apiBaseRoute}/assets/${assetId}/cases`);
});

test('assetWarranty builds the correct route', () => {
  expect(endpoints.assetWarranty({ assetId }))
    .toEqual(`${apiBaseRoute}/assets/${assetId}/warranty`);
});

test('assetFaults builds the correct route', () => {
  expect(endpoints.assetFaults({ assetId }))
    .toEqual(`${apiBaseRoute}/assets/${assetId}/faults`);
});

//-----------------------------------------------------------------------------

const caseId = 'caseId123';

test('cases builds the correct route', () => {
  expect(endpoints.cases())
    .toEqual(`${apiBaseRoute}/cases`);
});

test('case builds the correct route', () => {
  expect(endpoints.case({ caseId }))
    .toEqual(`${apiBaseRoute}/cases/${caseId}`);
});

test('caseRecipients builds the correct route', () => {
  expect(endpoints.caseRecipients({ caseId }))
    .toEqual(`${apiBaseRoute}/cases/${caseId}/recipients`);
});

test('caseNotes builds the correct route', () => {
  expect(endpoints.caseNotes({ caseId }))
    .toEqual(`${apiBaseRoute}/cases/${caseId}/notes`);
});

const noteId = 'noteId123';

test('caseNote builds the correct route', () => {
  expect(endpoints.caseNote({ caseId, noteId }))
    .toEqual(`${apiBaseRoute}/cases/${caseId}/notes/${noteId}`);
});

test('caseFaults builds the correct route', () => {
  expect(endpoints.caseFaults({ caseId }))
    .toEqual(`${apiBaseRoute}/cases/${caseId}/faults`);
});

//-----------------------------------------------------------------------------

const serviceRequestId = 'serviceRequestId123';

test('serviceRequests builds the correct route', () => {
  expect(endpoints.serviceRequests())
    .toEqual(`${apiBaseRoute}/service_requests`);
});

test('serviceRequest builds the correct route', () => {
  expect(endpoints.serviceRequest({ serviceRequestId }))
    .toEqual(`${apiBaseRoute}/service_requests/${serviceRequestId}`);
});

//-----------------------------------------------------------------------------

test('messagesStatus builds the correct route', () => {
  expect(endpoints.messagesStatus())
    .toEqual(`${apiBaseRoute}/messages/status`);
});

test('profile builds the correct route', () => {
  expect(endpoints.profile())
    .toEqual(`${apiBaseRoute}/profile`);
});

test('password builds the correct route', () => {
  expect(endpoints.password())
    .toEqual(`${apiBaseRoute}/password`);
});

test('passwordReset builds the correct route', () => {
  expect(endpoints.passwordReset())
    .toEqual(`${apiBaseRoute}/password/reset`);
});

test('dashboard builds the correct route', () => {
  expect(endpoints.dashboard())
    .toEqual(`${apiBaseRoute}/dashboard`);
});

//-----------------------------------------------------------------------------

const serviceProviderId = 'serviceProvierId999';

test('serviceProviders builds the correct route', () => {
  expect(endpoints.serviceProviders())
    .toEqual(`${apiBaseRoute}/service_providers`);
});

test('serviceProvider builds the correct route', () => {
  expect(endpoints.serviceProvider({ serviceProviderId }))
    .toEqual(`${apiBaseRoute}/service_providers/${serviceProviderId}`);
});

//-----------------------------------------------------------------------------

test('dataLists builds the correct route', () => {
  expect(endpoints.dataLists())
    .toEqual(`${apiBaseRoute}/data_lists`);
});

//-----------------------------------------------------------------------------

const oem = 'oh-ee-em';

test('login builds the correct route', () => {
  expect(endpoints.login(oem))
    .toEqual(`${apiBaseRoute}/login/${oem}`);
});

test('refreshToken builds the correct route', () => {
  expect(endpoints.refreshToken(oem))
    .toEqual(`${apiBaseRoute}/refresh_token/${oem}`);
});
