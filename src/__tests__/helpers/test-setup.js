/**
 * This file can (and should) be imported by any test files in the
 * project. It provides a simpler way to import various commonly-used
 * functions from various modules, and also performs some global
 * setup (e.g. linking in extensions to Enzyme).
 *
 * Most test files will then have imports that look something like this:
 *    import React from 'react';
 *    import { test, expect, shallow } from '__tests__/helpers/test-setup';
 *
 * Importing this file will automatically setup a restoreSpies() call to clear
 * all registered test spies after each test.
 */

import expectHelper, { createSpy } from 'expect';
import enzymify from 'expect-enzyme';
import expectJSX from 'expect-jsx';
import { test } from 'ava';

import * as appApi from 'redux/app/api';

expectHelper.extend(expectJSX);
expectHelper.extend(enzymify);

test.afterEach(() => {
  expectHelper.restoreSpies();
});

const dashboardApiResponse = [
  {
    category: 'assets',
    total: '0',
    filters: [
      {
        name: 'HIGH SEVERITY',
        total: '0',
        url: '/assets?scope=HIGH%20SEVERITY',
      },
    ],
  },
  {
    category: 'cases',
    total: '0',
    filters: [
      {
        name: 'DOWNTIME > 2 DAYS',
        total: '0',
        url: '/cases?scope=DOWNTIME%20%3C%202%20DAYS',
      },
    ],
  },
];

/**
 * This helper can be used to setup a mock response for the `getDashboard` API
 * call.  It will probably be needed in any place that mocks the entire layout,
 * including the left nav.
 */
export function mockDashboardApi(mockResponse = dashboardApiResponse) {
  expectHelper.spyOn(appApi, 'getDashboard').andReturn({ response: { body: mockResponse } });
}

/**
 * JSDOM does not handle path changes well at all. This method helps. Make sure you set
 * the URL back to 'http://example.com' when you are done.
 * Many thanks to https://github.com/facebook/jest/issues/890#issuecomment-298594389
 * for showing the way.
 * @param {String} url The FULL URL to set as the location in JSDom.
 */
function setJSDomURL(url) {
  const parser = document.createElement('a');
  parser.href = url;
  const windowLocationProps = [
    'href', 'protocol', 'host', 'hostname', 'origin', 'port', 'pathname', 'search', 'hash',
  ];
  windowLocationProps.forEach((prop) => {
    Object.defineProperty(window.location, prop, {
      value: parser[prop],
      writable: true,
    });
  });
}

/**
 * Execute a block of code (via the callback) with the current URL set to the given value.
 * This helper automatically reverts the current URL to its default value after the callback
 * completes.
 * @param {String} url The FULL URL to set as the location in JSDom.
 * @param {Function} callback A function to execute with the URL set to the given value.
 */
export function withJSDomURL(url, callback = null) {
  let result;
  setJSDomURL(url);
  try {
    if (callback) {
      result = callback();
    }
  } finally {
    setJSDomURL('http://example.com');
  }
  return result;
}

export test from 'ava';
export { shallow, mount } from 'enzyme';
export const expect = expectHelper;
export { spyOn, createSpy } from 'expect';
export MountableTestComponent from './mountable_test_component';

/**
 * This function is used to check if a specific action is called.
 * the problem with createSpy() or spyOn() is that those can not be used
 * as a parameter of dispatch because action.type causes error(action is undefined).
 * When original[methodName] is called while testing, it runs spy() so that it is counted
 * meanwhile returning the action object including 'type' field.
 */
export const actionAndSpy = (object, methodName, action = { type: '' }) => {
  const spy = createSpy();
  const original = object;
  original[methodName] = (...params) => {
    spy(...params);
    return action;
  };
  return spy;
};
