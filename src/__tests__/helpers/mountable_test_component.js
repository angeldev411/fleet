/**
 * The MountableTestComponent provides a single, uniform resource to be used with
 * `mount` from enzyme. Mounted components require certain items in context and may
 * throw errors if they are not present. Use this component in any test the requires
 * the use of `mount`. This component should mimic the Root component.
 *
 * Usage:
 *
 *   test('Sample test', () => {
 *     const wrapper = mount(
 *       <MountableTestComponent >
 *         <ComponentToTest />
 *       </MountableTestComponent>,
 *     );
 *     expect(wrapper.find('.something')).to...
 *   });
 */

import React from 'react';
import PropTypes from 'prop-types';

// Required for setting up Redux via the Provider
import { fromJS } from 'immutable';
import { Provider } from 'react-redux';
import configureStore from 'setup/store';

// Required for setting up React INTL
import { addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import enTranslations from 'translations/en.json';
import I18n from 'setup/I18n';

// Required for setting up React Router
import { MemoryRouter } from 'react-router-dom';

// Required for setting up Styled components
import AppThemeProvider from 'setup/AppThemeProvider';

// ----- setup state and locale -----
const initialState = fromJS({});
addLocaleData([...en]);

// ----------

function MountableTestComponent(props) {
  let store = props.store;
  if (props.authorized) {
    store = configureStore(fromJS({ user: { isAuthorized: true } }));
  }

  return (
    <Provider store={store} >
      <I18n locale={props.language} messages={props.messages} >
        <AppThemeProvider>
          <MemoryRouter initialEntries={props.initialEntries}>
            {props.children}
          </MemoryRouter>
        </AppThemeProvider>
      </I18n>
    </Provider>
  );
}

MountableTestComponent.propTypes = {
  authorized: PropTypes.bool,
  children: PropTypes.node,
  // children: PropTypes.oneOfType([
  //   PropTypes.arrayOf(PropTypes.node),
  //   PropTypes.node,
  // ]),
  initialEntries: PropTypes.arrayOf(PropTypes.string).isRequired,
  language: PropTypes.oneOf(['en']).isRequired,
  messages: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  store: PropTypes.object, // eslint-disable-line react/forbid-prop-types
};

MountableTestComponent.defaultProps = {
  authorized: false,
  children: null,
  initialEntries: ['/'],
  language: 'en',
  messages: { en: enTranslations },
  store: configureStore(initialState),
};

export default MountableTestComponent;
