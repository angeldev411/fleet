import Bugsnag from 'setup/Bugsnag'; // make sure Bugsnag is loaded first
import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { fromJS } from 'immutable';
import { Provider } from 'react-redux';
import keyboardEventKeyPolyfill from 'keyboardevent-key-polyfill';

import I18n from 'setup/I18n';
import AppThemeProvider from 'setup/AppThemeProvider';
import Router from 'setup/Router';

import configureStore from 'setup/store';
import initLocalStorage from 'setup/localStorage';
import { installPendo } from 'setup/Pendo';
import { installZendesk } from 'setup/Zendesk';

initLocalStorage();
installPendo();
installZendesk();

// This enables us to use `key` property of keyboardEvent object in all browsers.
keyboardEventKeyPolyfill.polyfill();

// ----- Setup the store -----

const initialState = fromJS({});
const store = configureStore(initialState);
Bugsnag.setStore(store);

// ----- Render the application root -----

// NOTE:  Any changes to the top-level application rendering -- i.e. the
// component structure built by `renderApp` -- need to be reflected in
// `MountableTestComponent` under src/__tests__/helpers.

function renderApp(Component) {
  render(
    <AppContainer>
      <Provider store={store}>
        <I18n>
          <AppThemeProvider>
            <Component />
          </AppThemeProvider>
        </I18n>
      </Provider>
    </AppContainer>,
    document.getElementById('root'),
  );
}

renderApp(Router);

// ----- Setup hot module reloading -----

if (module.hot) {
  module.hot.accept('./setup/Router/index.js', () => {
    /* eslint-disable global-require */
    const NewApp = require('./setup/Router/index.js').default;
    renderApp(NewApp);
  });
}
