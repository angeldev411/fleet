import { getAuthData } from './authData';

/**
 * Determine when the current authentication will expire.
 * @returns {number} the time of expiration, as milliseconds since 1 Jan 1970, like Date.now()
 */
export default function tokenExpiration({ expiresAt } = getAuthData()) {
  if (isNaN(expiresAt)) {
    return 0;
  }

  return expiresAt * 1000;
}
