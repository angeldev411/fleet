import { get, post, put } from 'utils/fetch';

const endpoints = {
  login: () =>
    `${process.env.PORTUNUS_BASE_URL}/api/v1/sessions`,

  renew: () =>
    `${process.env.PORTUNUS_BASE_URL}/api/v1/sessions/renew`,

  verify: () =>
    `${process.env.PORTUNUS_BASE_URL}/api/v1/sessions/verify`,
};

const CONTENT_HEADERS = {
  Accept: 'application/vnd.api+json',
  'Content-type': 'application/vnd.api+json',
};

export default {
  login: requestBody =>
    post(endpoints.login(), { requestBody, auth: false, headers: CONTENT_HEADERS }),
  renew: () =>
    put(endpoints.renew(), { headers: CONTENT_HEADERS }),
  verify: () =>
    get(endpoints.verify(), { headers: CONTENT_HEADERS }),
};
