import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import handleAuthResponse from '../handleAuthResponse';
import * as authData from '../authData';
import { successfulLoginResponse, decodeToken } from './helpers/responses';

test('clears auth data and returns unauthorized if response is an error', () => {
  const clearAuthDataSpy = spyOn(authData, 'clearAuthData');

  const result = handleAuthResponse({}, { response: { body: { foo: 'bar' } }, error: true });

  expect(clearAuthDataSpy).toHaveBeenCalled();
  expect(result.isAuthorized).toEqual(false);
});

test('clears auth data and returns unauthorized if response is invalid', () => {
  const response = {
    body: {
      data: {
        type: 'type',
        attributes: { access_token: 'thistoken.isnot.valid' },
      },
    },
  };

  const clearAuthDataSpy = spyOn(authData, 'clearAuthData');

  const result = handleAuthResponse({}, { response });

  expect(clearAuthDataSpy).toHaveBeenCalled();
  expect(result.isAuthorized).toEqual(false);
});

test('saves merged auth data on success', () => {
  const saveAuthDataSpy = spyOn(authData, 'saveAuthData');

  const response = successfulLoginResponse();
  const { data: { type, attributes: { access_token } } } = response.body;

  const {
    email,
    exp: expiresAt,
    external_id: externalID,
    iat: issuedAt,
    iss: issuedBy,
    name,
    username,
  } = decodeToken(access_token);

  const oem = 'Fiat';
  const currentAuthData = { username: 'ThisIsIgnored', oem };

  const result = handleAuthResponse(currentAuthData, { response });

  expect(saveAuthDataSpy).toHaveBeenCalled();

  const expectedResult = {
    username,
    oem,
    type,
    accessToken: access_token,
    email,
    expiresAt,
    externalID,
    issuedAt,
    issuedBy,
    name,
    isAuthorized: true,
  };
  const args = saveAuthDataSpy.calls[0].arguments;
  expect(args[0]).toEqual(expectedResult);
  expect(result).toEqual(expectedResult);
});
