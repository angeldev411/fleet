import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import tokenExpiration from '../tokenExpiration';
import * as authData from '../authData';

test('gets the expiration data from the current auth data and returns it in milliseconds', () => {
  spyOn(authData, 'getAuthData').andReturn({
    expiresAt: 9876,
  });

  expect(tokenExpiration()).toEqual(9876000);
});

test('returns 0 if expiration date is not a number', () => {
  spyOn(authData, 'getAuthData').andReturn({
    expiresAt: 'bogus',
  });

  expect(tokenExpiration()).toEqual(0);
});
