import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import refreshToken from '../refreshToken';
import api from '../api';
import { successfulLoginResponse } from './helpers/responses';

test('calls the api renew endpoint', async () => {
  const apiSpy = spyOn(api, 'renew').andReturn({ response: successfulLoginResponse() });

  const result = await refreshToken();

  expect(apiSpy).toHaveBeenCalled();

  expect(result.isAuthorized).toEqual(true);
});
