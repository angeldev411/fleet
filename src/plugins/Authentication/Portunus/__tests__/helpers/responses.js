import jwt from 'jwt-simple';

const JWT_SECRET = 'S3C433T';

function loginResponse({ expiresAt }) {
  const issuedAt = (Date.now() / 1000) - 60;
  const jwtParts = {
    email: 'jwt@decisiv.net',
    exp: expiresAt,
    external_id: '123',
    iat: issuedAt,
    name: 'Jane User',
    username: 'jane',
  };
  const encodedToken = jwt.encode(jwtParts, JWT_SECRET);

  return {
    body: {
      data: {
        type: 'type',
        attributes: {
          access_token: encodedToken,
        },
      },
    },
  };
}

export function decodeToken(token) {
  return jwt.decode(token, JWT_SECRET);
}

export function expiredLoginResponse() {
  const expiresAt = (Date.now() / 1000) - 10;

  return loginResponse({ expiresAt });
}

export function successfulLoginResponse() {
  // times are in *seconds*; Date.now() is *milliseconds*
  const expiresAt = (Date.now() / 1000) + 600;
  return loginResponse({ expiresAt });
}
