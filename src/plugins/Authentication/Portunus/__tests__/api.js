import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import * as fetchUtil from 'utils/fetch';

import api from '../api';

const env = { ...process.env };

const baseUrl = 'http://foo.com';

test.beforeEach(() => {
  process.env.PORTUNUS_BASE_URL = baseUrl;
});

test.afterEach(() => {
  process.env = env;
});

function validateUrl(fetchSpy, expectedUrl) {
  const actualUrl = fetchSpy.calls[0].arguments[0];
  expect(actualUrl).toEqual(expectedUrl);
}

function validateHeaders(fetchSpy) {
  const contentType = 'application/vnd.api+json';

  const headers = fetchSpy.calls[0].arguments[1].headers;
  expect(headers.Accept).toEqual(contentType);
  expect(headers['Content-type']).toEqual(contentType);
}

test('login POSTs an un-authed request to the login endpoint', () => {
  const theResponse = 'POST response';
  const requestBody = { foo: 'bar', test: 123 };

  const postSpy = spyOn(fetchUtil, 'post').andReturn(theResponse);
  const result = api.login(requestBody);
  expect(result).toEqual(theResponse);

  expect(postSpy).toHaveBeenCalled();
  validateUrl(postSpy, `${baseUrl}/api/v1/sessions`);
  validateHeaders(postSpy);
  const postOpts = postSpy.calls[0].arguments[1];
  expect(postOpts.requestBody).toEqual(requestBody);
  expect(postOpts.auth).toEqual(false);
});

test('renew PUTs to the renew endpoint', () => {
  const theResponse = 'PUT response';
  const putSpy = spyOn(fetchUtil, 'put').andReturn(theResponse);

  const result = api.renew();
  expect(result).toEqual(theResponse);

  expect(putSpy).toHaveBeenCalled();
  validateUrl(putSpy, `${baseUrl}/api/v1/sessions/renew`);
  validateHeaders(putSpy);
});

test('verify GETs from the verify endpoint', () => {
  const theResponse = 'GET response';
  const getSpy = spyOn(fetchUtil, 'get').andReturn(theResponse);

  const result = api.verify();
  expect(result).toEqual(theResponse);

  expect(getSpy).toHaveBeenCalled();
  validateUrl(getSpy, `${baseUrl}/api/v1/sessions/verify`);
  validateHeaders(getSpy);
});
