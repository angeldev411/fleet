import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import * as authData from '../authData';
import logout from '../logout';

test('logout clears the auth data', () => {
  const clearAuthDataSpy = spyOn(authData, 'clearAuthData');

  logout();

  expect(clearAuthDataSpy).toHaveBeenCalled();
});
