import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import api from '../api';

import login from '../login';
import { successfulLoginResponse } from './helpers/responses';

test('posts the expected data to the login endpoint', () => {
  const apiSpy = spyOn(api, 'login').andReturn({ response: successfulLoginResponse() });

  const username = 'CaptainKirk';
  const password = '3NT34P41S3';
  const params = { username, password };

  login(params);

  expect(apiSpy).toHaveBeenCalledWith(
    { data: { type: 'sessions', attributes: { username, password } } },
  );
});
