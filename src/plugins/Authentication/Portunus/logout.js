import { clearAuthData } from './authData';

/**
 * Clear the current auth data, which un-authenticates the current session.
 */
export default function logout() {
  // TODO: Portunus SSO *should* support single-sign-out as well, but current it does not
  clearAuthData();
}
