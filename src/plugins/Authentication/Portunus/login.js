import api from './api';
import handleAuthResponse from './handleAuthResponse';

function loginRequestBody({ username, password }) {
  return {
    data: {
      type: 'sessions',
      attributes: { username, password },
    },
  };
}

/**
 * Execute login against the Portunus API.
 * @param {string} username
 * @param {string} password
 * @returns {Promise<{isAuthorized: boolean}>}
 */
export default async function login({ username, password }) {
  const loginData = { username, password };

  return handleAuthResponse(
    loginData,
    await api.login(loginRequestBody(loginData)),
  );
}
