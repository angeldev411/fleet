import api from './api';
import { getAuthData } from './authData';
import handleAuthResponse from './handleAuthResponse';

/**
 * Request a new, fresh token to replace the current authorization token.
 * @returns {Promise<{isAuthorized: boolean}>}
 */
export default async function refreshToken() {
  return handleAuthResponse(
    getAuthData(),
    await api.renew(),
  );
}
