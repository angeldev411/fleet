import jwt from 'jwt-simple';

import { clearAuthData, decorateAuthData, saveAuthData } from './authData';

function extractAuthData(responseBody) {
  try {
    const { type, attributes } = responseBody.data;
    const { access_token: accessToken } = attributes;
    const {
      email,
      exp: expiresAt,
      external_id: externalID,
      iat: issuedAt,
      iss: issuedBy,
      name,
      username,
    } = jwt.decode(accessToken, null, true); // JWT decode without verification

    return {
      type,
      accessToken,
      // JWT parts:
      email,
      expiresAt,
      externalID,
      issuedAt,
      issuedBy,
      name,
      username,
    };
  } catch (err) {
    return null;
  }
}

/**
 * This is a helper function, private to the Portunus Authentication plugin,
 * for putting together the updated auth data in response to a login or token refresh.
 * @package
 * @param {Object} currentAuthData - any existing auth data to merge with the new auth data
 * @param {Object} response - the response object from the login or token refresh API call
 * @param {Object} error - any error result from the login or refresh
 * @returns {{isAuthorized: boolean}} - the updated auth data, including an isAuthorized flag
 */
export default function handleAuthResponse(currentAuthData, { response, error }) {
  if (!error) {
    const newAuthData = extractAuthData(response.body);
    if (newAuthData) {
      const result = decorateAuthData({ ...currentAuthData, ...newAuthData });
      if (result.isAuthorized) {
        // success 👍
        saveAuthData(result);
        return result;
      }
    }
  }

  // error during login or token refresh, or we got a bogus response 👎
  clearAuthData();

  return {
    isAuthorized: false,
  };
}
