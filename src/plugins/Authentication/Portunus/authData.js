import setupAuthData from './../shared/setupAuthData';
import tokenExpiration from './tokenExpiration';

function isTokenExpired(data) {
  return (tokenExpiration(data) - Date.now()) <= 1000;
}

export function decorateAuthData(authData) {
  return { ...authData, isAuthorized: !isTokenExpired(authData) };
}

/**
 * Known keys from the Portunus auth data that have string values (i.e. they can be stored
 * in localStorage with no further processing).
 * @private
 * @type {string[]}
 */
const STRING_KEYS = [
  'type',
  'accessToken',
  'email',
  'externalID',
  'issuedBy',
  'name',
  'username',
];

/**
 * Known keys from the Portunus auth data which have integer values (i.e. they can must be parsed
 * back into integers upon retrieval from localStorage).
 * @private
 * @type {string[]}
 */
const INT_KEYS = [
  'expiresAt',
  'issuedAt',
];

const {
  clear,
  get,
  save,
} = setupAuthData({ stringKeys: STRING_KEYS, integerKeys: INT_KEYS });

export const clearAuthData = clear;
export const getAuthData = () => decorateAuthData(get());
export const saveAuthData = save;
