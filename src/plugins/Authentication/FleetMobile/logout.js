import { clearAuthData } from './authData';

/**
 * Clear the current auth data, which un-authenticates the current session.
 */
export default function logout() {
  clearAuthData();
}
