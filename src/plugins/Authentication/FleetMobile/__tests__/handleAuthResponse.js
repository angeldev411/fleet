import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import handleAuthResponse from '../handleAuthResponse';
import * as authData from '../authData';
import { expiredLoginResponse, successfulLoginResponse } from './helpers/responses';

test('clears auth data and returns unauthorized if response is an error', () => {
  const clearAuthDataSpy = spyOn(authData, 'clearAuthData');

  const result = handleAuthResponse({}, { response: { body: { foo: 'bar' } }, error: true });

  expect(clearAuthDataSpy).toHaveBeenCalled();
  expect(result.isAuthorized).toEqual(false);
});

test('clears auth data and returns unauthorized if token is expired', () => {
  const clearAuthDataSpy = spyOn(authData, 'clearAuthData');
  const response = expiredLoginResponse();
  const result = handleAuthResponse({}, { response });

  expect(clearAuthDataSpy).toHaveBeenCalled();
  expect(result.isAuthorized).toEqual(false);
});

test('saves merged auth data on success', () => {
  const saveAuthDataSpy = spyOn(authData, 'saveAuthData');

  const response = successfulLoginResponse();
  const { created_at, expires_in } = response.body;

  const username = 'David';
  const oem = 'Yugo';
  const currentAuthData = { username, oem };

  const result = handleAuthResponse(currentAuthData, { response });

  expect(saveAuthDataSpy).toHaveBeenCalled();

  const expectedResult = {
    username,
    oem,
    created_at,
    expires_in,
    isAuthorized: true,
  };
  const args = saveAuthDataSpy.calls[0].arguments;
  expect(args[0]).toEqual(expectedResult);
  expect(result).toEqual(expectedResult);
});
