export function expiredLoginResponse() {
  return {
    body: {
      // created_at and expires_in are in *seconds*; Date.now() is *milliseconds*
      created_at: (Date.now() / 1000) - 50,
      expires_in: 10,
    },
  };
}

export function successfulLoginResponse() {
  // created_at and expires_in are in *seconds*; Date.now() is *milliseconds*
  return {
    body: {
      created_at: (Date.now() / 1000) - 50,
      expires_in: 120,
    },
  };
}

