import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import * as fetchUtil from 'utils/fetch';
import endpoints from 'apiEndpoints';

import login from '../login';
import { successfulLoginResponse } from './helpers/responses';

test('posts the expected data to the login endpoint', () => {
  spyOn(endpoints, 'login').andCall(oem => `/login/${oem}`);
  const postSpy = spyOn(fetchUtil, 'post').andReturn({ response: successfulLoginResponse() });

  const username = 'CaptainKirk';
  const password = '3NT34P41S3';
  const selectedOem = 'Starfleet';
  const params = { username, password, selectedOem };

  login(params);

  expect(postSpy).toHaveBeenCalledWith(
    '/login/Starfleet',
    { requestBody: { username, password }, auth: false },
  );
});
