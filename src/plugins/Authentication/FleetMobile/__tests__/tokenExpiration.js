import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import tokenExpiration from '../tokenExpiration';
import * as authData from '../authData';

test('returns the expiration time in milliseconds', () => {
  const expirationData = {
    created_at: 100,
    expires_in: 50,
  };

  expect(tokenExpiration(expirationData)).toEqual(150000);
});

test('by default gets the expiration data from the current auth data', () => {
  spyOn(authData, 'getAuthData').andReturn({
    created_at: 989,
    expires_in: 10,
  });

  expect(tokenExpiration()).toEqual(999000);
});

test('returns 0 if created_at is not a number', () => {
  const expirationData = {
    created_at: 'bogus',
    expires_in: 50,
  };

  expect(tokenExpiration(expirationData)).toEqual(0);
});

test('returns 0 if expires_in is not a number', () => {
  const expirationData = {
    created_at: 100,
    expires_in: 'junk',
  };

  expect(tokenExpiration(expirationData)).toEqual(0);
});
