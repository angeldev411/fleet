import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import * as fetchUtil from 'utils/fetch';
import endpoints from 'apiEndpoints';

import refreshToken from '../refreshToken';
import * as authData from '../authData';
import { successfulLoginResponse } from './helpers/responses';

test('posts the expected data to the login endpoint', async () => {
  spyOn(endpoints, 'refreshToken').andCall(oem => `/refresh/${oem}`);
  const postSpy = spyOn(fetchUtil, 'post').andReturn({ response: successfulLoginResponse() });

  const token = '123magicTokenString';
  spyOn(authData, 'getAuthData').andReturn({ oem: 'Tesla', refresh_token: token });

  const result = await refreshToken();

  expect(postSpy).toHaveBeenCalledWith(
    '/refresh/Tesla',
    { requestBody: { refresh_token: token } },
  );

  expect(result.isAuthorized).toEqual(true);
});
