import endpoints from 'apiEndpoints';
import { post } from 'utils/fetch';

import handleAuthResponse from './handleAuthResponse';

/**
 * @private
 * @param {string} oem - selected OEM, indicating which endpoint to use for FMAPI login
 * @param {Object} queryParams
 * @returns {Promise}
 */
function postLogin(oem, queryParams) {
  return post(endpoints.login(oem), { requestBody: queryParams, auth: false });
}

/**
 * Execute login against the appropriate FMAPI login endpoint.
 * @param {string} username
 * @param {string} password
 * @param {string} selectedOem
 * @returns {Promise<{isAuthorized: boolean}>}
 */
export default async function login({ username, password, selectedOem }) {
  const initialAuthData = { username, password, oem: selectedOem };
  return handleAuthResponse(
    initialAuthData,
    await postLogin(selectedOem, { username, password }),
  );
}
