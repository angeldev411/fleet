import setupAuthData from './../shared/setupAuthData';
import tokenExpiration from './tokenExpiration';

function isTokenExpired(data) {
  return (tokenExpiration(data) - Date.now()) <= 1000;
}

export function decorateAuthData(authData) {
  return { ...authData, isAuthorized: !isTokenExpired(authData) };
}

/**
 * Known keys from the FMAPI auth data + any extra stuff we store related to auth, which
 * have string values (i.e. they can be stored in localStorage with no further processing).
 * @private
 * @type {string[]}
 */
const STRING_KEYS = [
  // username and oem are not provided by FMAPI but passed through from the login process:
  'username',
  'oem',
  // the following string values come from FMAPI's auth process:
  'access_token',
  'refresh_token',
  'token_type',
];

/**
 * Known keys from the FMAPI auth data which have integer values (i.e. they must be parsed
 * back into integers upon retrieval from localStorage).
 * @private
 * @type {string[]}
 */
const INT_KEYS = [
  'created_at',
  'expires_in',
];

const {
  clear,
  get,
  save,
} = setupAuthData({ stringKeys: STRING_KEYS, integerKeys: INT_KEYS });

export const clearAuthData = clear;
export const getAuthData = () => decorateAuthData(get());
export const saveAuthData = save;
