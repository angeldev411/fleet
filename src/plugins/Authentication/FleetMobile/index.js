import makeAuthHeaders from './../shared/authHeaders';
import makeAuthQuery from './../shared/authQuery';
import { getAuthData } from './authData';
import login from './login';
import logout from './logout';
import refreshToken from './refreshToken';
import tokenExpiration from './tokenExpiration';

/**
 * Fleet Mobile API implementation of the Authentication plugin contract.
 * @module plugins/Authentication
 */
export default {
  // for enhancing API calls with authentication:
  authHeaders: makeAuthHeaders(getAuthData),
  authQuery: makeAuthQuery(getAuthData),
  // retrieve the current auth data:
  authData: getAuthData,
  tokenExpiration,
  // authentication process:
  login,
  refreshToken,
  logout,
};
