import { getAuthData } from './authData';

/**
 * Determine when the current authentication will expire.
 * @returns {number} the time of expiration, as milliseconds since 1 Jan 1970, like Date.now()
 */
export default function tokenExpiration({
  created_at: createdAt,
  expires_in: expiresIn,
} = getAuthData()) {
  if (isNaN(createdAt) || isNaN(expiresIn)) {
    return 0;
  }

  return (createdAt + expiresIn) * 1000;
}
