import { clearAuthData, decorateAuthData, saveAuthData } from './authData';

/**
 * This is a helper function, private to the FleetMobile Authentication plugin,
 * for putting together the updated auth data in response to a login or token refresh.
 * @package
 * @param {Object} currentAuthData - any existing auth data to merge with the new auth data
 * @param {Object} response - the response object from the login or token refresh API call
 * @param {Object} error - any error result from the login or refresh
 * @returns {{isAuthorized: boolean}} - the updated auth data, including an isAuthorized flag
 */
export default function handleAuthResponse(currentAuthData, { response, error }) {
  if (!error) {
    const newAuthData = decorateAuthData({ ...currentAuthData, ...response.body });

    if (newAuthData.isAuthorized) {
      // success 👍
      saveAuthData(newAuthData);
      return newAuthData;
    }
  }

  // error or expired token 👎

  clearAuthData();

  return {
    isAuthorized: false,
  };
}
