import endpoints from 'apiEndpoints';
import { post } from 'utils/fetch';

import { getAuthData } from './authData';
import handleAuthResponse from './handleAuthResponse';

function postRefreshToken(oem, requestBody) {
  return post(endpoints.refreshToken(oem), { requestBody });
}

function generateRefreshQueryParams({ refresh_token }) {
  return { refresh_token };
}

/**
 * Request a new, fresh token to replace the current authorization token.
 * @returns {Promise<{isAuthorized: boolean}>}
 */
export default async function refreshToken() {
  const currentAuthData = getAuthData();
  const queryParams = generateRefreshQueryParams(currentAuthData);

  return handleAuthResponse(
    currentAuthData,
    await postRefreshToken(currentAuthData.oem, queryParams),
  );
}
