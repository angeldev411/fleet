const unauthorized = { isAuthorized: false };

/**
 * The public interface of this plugin.  This is also the implementation used when
 * testing - see config/testing/ava_setup.js for details.
 *
 */
/* istanbul ignore next */
export default {
  // for enhancing API calls with authentication:
  authHeaders: () => ({}),
  authQuery: () => ({}),
  // retrieve the current auth data:
  authData: () => ({}),
  tokenExpiration: () => 0,
  // authentication process:
  login: () => unauthorized,
  refreshToken: () => unauthorized,
  logout: () => unauthorized,
};
