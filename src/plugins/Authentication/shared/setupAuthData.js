/**
 * Setup the auth-data handlers for a specific set of data keys.  These handlers are
 * responsible for storing auth data into / retrieving auth data from localStorage.
 *
 * @param {Array<string>} stringKeys - keys for data that should be handed as string values
 * @param {Array<string>} integerKeys - keys for data that should be interpreted as integers
 * @returns {{clear: function, save: function, get: function}}
 */
export default function setupAuthData({ stringKeys, integerKeys, storage }) {
  /**
   * Use either the injected `storage` or fall back to `memoryDB`.  But we can't just do
   * this via default arguments since at the time `setupAuthData` is called the `memoryDB`
   * might not yet have been initialized.
   *
   * @returns {MemoryStorage}
   */
  function authStorage() {
    /* istanbul ignore next */
    return storage || memoryDB;
  }

  /**
   * Take a basic key and decorate it for use as a localStorage key.
   * @private
   * @param {string} k
   * @returns {string}
   */
  function keyBuilder(k) {
    return `DECISIV__${k}`;
  }

  /**
   * A concatenation of ALL the auth data keys.
   * @private
   * @type {string[]}
   */
  const KEYS = [...stringKeys, ...integerKeys];

  /**
   * Clear all auth-data values from the memory DB / local storage.
   * @package
   */
  function clear() {
    KEYS.forEach(k => authStorage().removeItem(keyBuilder(k)));
  }

  /**
   * Extract all auth data information from the given argument and store the
   * data into localStorage.
   * @package
   * @param {Object} newAuthData - the auth data to be stored
   */
  function save(newAuthData) {
    KEYS.forEach(k => authStorage().setItem(keyBuilder(k), newAuthData[k]));
  }

  /**
   * Retrieve the auth data from localStorage.
   * @returns {Object}
   */
  function get() {
    return {
      ...stringKeys.reduce((acc, key) =>
        ({ ...acc, [key]: authStorage().getItem(keyBuilder(key)) }), {}),
      ...integerKeys.reduce((acc, key) =>
        ({ ...acc, [key]: parseInt(authStorage().getItem(keyBuilder(key)), 10) }), {}),
    };
  }

  return { clear, save, get };
}
