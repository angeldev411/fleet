/**
 * Build a function that can generate the headers that should be included in any
 * request requiring authorization.
 * @returns {function}
 */
export default function makeAuthHeaders(authDataGetter) {
  /**
   * Generate the required headers for an authorized API call.  If called while
   * unauthorized, an empty object will be returned.
   * @returns {Object}
   */
  return function authHeaders() {
    const { access_token: accessToken } = authDataGetter();
    if (accessToken) {
      return { Authorization: `Bearer ${accessToken}` };
    }
    return {};
  };
}
