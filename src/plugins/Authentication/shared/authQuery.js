/**
 * Build a function that can generate the query-string parameters to append to any request
 * requiring authorization.
 * @returns {function}
 */
export default function makeAuthQuery(authDataGetter) {
  /**
   * Get the query string parameters to append to any request requiring authorization. If
   * not currently authorized, an empty object will be returned.
   * @returns {Object}
   */
  return function authQuery() {
    const { access_token: accessToken } = authDataGetter();
    if (accessToken) {
      return { access_token: accessToken };
    }
    return {};
  };
}
