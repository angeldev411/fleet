import MemoryStorage from 'memorystorage';

import {
  expect,
  test,
} from '__tests__/helpers/test-setup';

import setupAuthData from '../setupAuthData';

let storage;

test.beforeEach(() => {
  storage = new MemoryStorage('AUTH-DATA-TEST');
});

test.afterEach(() => {
  storage.clear();
});

test('returns three functions', () => {
  const stringKeys = ['str'];
  const integerKeys = ['int'];
  const result = setupAuthData({ stringKeys, integerKeys });
  expect(result).toIncludeKeys(['clear', 'get', 'save']);
  expect(result.get).toBeA('function');
  expect(result.clear).toBeA('function');
  expect(result.save).toBeA('function');
});

test('authData.save adds auth data to storage', () => {
  const stringKeys = ['first', 'second'];
  const integerKeys = ['third'];

  const { save } = setupAuthData({ storage, stringKeys, integerKeys });
  expect(storage.length).toEqual(0);
  save({});
  expect(storage.length).toEqual(stringKeys.length + integerKeys.length);
});

test('authData.get gets string key values verbatim and turns integer values into numbers', () => {
  const stringKeys = ['key1', 'key2'];
  const integerKeys = ['intKey'];

  const { save, get } = setupAuthData({ storage, stringKeys, integerKeys });

  const updatedAuthData = {
    key1: 'some string value',
    key2: 'another',
    intKey: 42,
  };
  save(updatedAuthData);

  expect(get()).toEqual(updatedAuthData);
});

test('authData.clear removes only auth data from storage', () => {
  const stringKeys = ['str'];
  const integerKeys = ['int'];

  storage.setItem('special', 'should remain');

  const { clear, save } = setupAuthData({ storage, stringKeys, integerKeys });
  expect(storage.length).toEqual(1);
  save({ int: 999, str: 'a string value' });
  expect(storage.length).toEqual(3);
  clear();
  expect(storage.length).toEqual(1);
  expect(storage.getItem('special')).toEqual('should remain');
});
