import {
  expect,
  test,
} from '__tests__/helpers/test-setup';

import makeAuthHeaders from '../authHeaders';

test('retrieves the access_token from the auth data', () => {
  const accessToken = 'abc123secret';
  const authDataGetter = () => ({ access_token: accessToken });
  const fn = makeAuthHeaders(authDataGetter);
  const headers = fn();
  expect(headers.Authorization).toEqual('Bearer abc123secret');
});

test('returns an empty object if no access token is available', () => {
  const fn = makeAuthHeaders(() => ({}));
  const headers = fn();
  expect(headers).toEqual({});
});
