import {
  expect,
  test,
} from '__tests__/helpers/test-setup';

import makeAuthQuery from '../authQuery';

test('retrieves the access_token from the auth data', () => {
  const accessToken = 'abc123query4secret';
  const authDataGetter = () => ({ access_token: accessToken });
  const fn = makeAuthQuery(authDataGetter);
  const query = fn();
  expect(query.access_token).toEqual(accessToken);
});

test('returns an empty object if no access token is available', () => {
  const fn = makeAuthQuery(() => ({}));
  const headers = fn();
  expect(headers).toEqual({});
});
