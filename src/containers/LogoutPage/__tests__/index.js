import React from 'react';
import { noop } from 'lodash';
import {
  test,
  expect,
  shallow,
  createSpy,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';
import Routes from 'setup/routes';

import { LogoutPage } from '../index';

const testProps = {
  isAuthorized: true,
  logout: noop,
};

function renderPage(props = testProps) {
  return shallow(<LogoutPage {...props} />);
}

function mountRoutes(initialEntries, { authorized = true } = {}) {
  return mount(
    <MountableTestComponent authorized={authorized} initialEntries={initialEntries} >
      <Routes />
    </MountableTestComponent>,
  );
}

function mountPageWithProps(props = testProps) {
  return mount(
    <MountableTestComponent authorized>
      <LogoutPage {...props} />
    </MountableTestComponent>,
  );
}

test('visiting the logout route when authorized redirects to the login page', () => {
  const logoutRoute = '/logout';
  const page = mountRoutes([logoutRoute]);
  expect(page).toContain('LoginFormWrapper');
});

test('rendering the logout page when authorized calls the logout function', () => {
  const logout = createSpy();
  mountPageWithProps({ ...testProps, logout });
  expect(logout).toHaveBeenCalled();
});

test('rendering the logout page when unauthorized renders a redirect component', () => {
  const page = renderPage({ ...testProps, isAuthorized: false });
  expect(page).toBeA('Redirect');
  expect(page).toHaveProp('to', '/login');
});
