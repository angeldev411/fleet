import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { logout } from 'redux/user/actions';
import { isAuthorizedSelector } from 'redux/user/selectors';

export class LogoutPage extends Component {
  static propTypes = {
    isAuthorized: PropTypes.bool.isRequired,
    logout: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.logout();
  }

  render() {
    return (
      this.props.isAuthorized ? <section id="log-out" /> : <Redirect to="/login" />
    );
  }
}

function mapStateToProps(state) {
  return {
    isAuthorized: isAuthorizedSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LogoutPage);
