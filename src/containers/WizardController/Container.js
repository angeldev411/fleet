import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import { makeGetWizardConfigByKey } from 'redux/wizardConfigs/selectors';

import { wizardConfigKeys } from 'setup/WizardRegistry';

import immutableToJS from 'utils/immutableToJS';
import mapWizardConfigToRegistry from 'utils/mapWizardConfigToRegistry';

/* istanbul ignore next */
const withConnectedData = (GenericWizardController) => {
  function WithConnectedData(props) {
    const { configKey, wizardConfig } = props;
    const mappedWizardConfig = mapWizardConfigToRegistry(wizardConfig, configKey);
    const { SpecificWizardController } = mappedWizardConfig;

    return (
      <SpecificWizardController
        {...props}
        render={genericControllerProps => (
          <GenericWizardController
            {...genericControllerProps}
            mappedWizardConfig={mappedWizardConfig}
          />
        )}
      />
    );
  }

  WithConnectedData.propTypes = {
    configKey: PropTypes.oneOf(wizardConfigKeys).isRequired,
    modal: PropTypes.bool,
    wizardConfig: PropTypes.shape({
      steps: PropTypes.objectOf(
        PropTypes.arrayOf(
          PropTypes.string,
        ),
      ).isRequired,
      start: PropTypes.string.isRequired,
    }).isRequired,
  };

  WithConnectedData.defaultProps = {
    modal: false,
  };

  function mapStateToProps() {
    const getWizardConfigByKey = makeGetWizardConfigByKey();

    return (state, props) => ({
      wizardConfig: getWizardConfigByKey(state, props.configKey),
    });
  }

  return connect(mapStateToProps)(
    immutableToJS(WithConnectedData),
  );
};

export default withConnectedData;
