import { findIndex, merge } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import FontAwesome from 'react-fontawesome';
import {
  Column,
  Container,
  Row,
} from 'styled-components-reactive-grid';

import TextDiv from 'elements/TextDiv';

import { wizardConfigKeys } from 'setup/WizardRegistry';

import withConnectedData from './Container';
import messages from './messages';

export class WizardController extends Component {
  static propTypes = {
    buildStepsArray: PropTypes.func,
    configKey: PropTypes.oneOf(wizardConfigKeys).isRequired,
    handleWizardCancel: PropTypes.func.isRequired,
    handleWizardSubmit: PropTypes.func.isRequired,
    mappedWizardConfig: PropTypes.shape({
      start: PropTypes.string.isRequired,
      steps: PropTypes.objectOf(
        PropTypes.shape({
          component: PropTypes.func.isRequired,
          name: PropTypes.string.isRequired,
          nextSteps: PropTypes.arrayOf(PropTypes.string.isRequired),
        }).isRequired,
      ).isRequired,
    }).isRequired,
  };

  static defaultProps = {
    buildStepsArray: undefined,
  }

  state = {
    currentStepIndex: 0,
    inputData: {},
    stepsArray: [],
  };

  componentWillMount() {
    // At a later point, we may need to "hydrate" this data from saved in localstorage.
    const currentStepIndex = 0;
    const stepsArray = this.buildStepsArray(this.state, this.props.mappedWizardConfig);
    this.setState({
      currentStepIndex,
      stepsArray,
    });
  }

  /**
   * buildStepsArray builds a full list of steps for the wizard based on the data currently
   * held in state. The implementation held within the Wizard Controller handles a linear
   * progression of steps. More complicated progressions like branching must be handled with
   * a buildStepsArray prop.
   * @param  {Object} currentState the current state of the WizardController. Passed in for purity.
   * @property {Number} currentState.currentStepIndex the index of the current step
   * @property {Array} currentState.stepsArray the current steps array.
   * @param {Object} mappedWizardConfig wizard config from the runtime file, mapped to components
   * @property {String} mappedWizardConfig.start the starting step name
   * @property {Object} mappedWizardConfig.steps an object containing the available steps
   * @return {Array} a complete array of steps for the wizard, in the order they should be
   * presented.
   */
  buildStepsArray = (currentState, mappedWizardConfig) => {
    // if a `buildStepsArray` prop is available, prefer it.
    if (this.props.buildStepsArray) {
      return this.props.buildStepsArray(currentState, mappedWizardConfig);
    }

    // This recursively called function builds the stepsArray by evaluating the last step in the
    // array for a "nextSteps" property, then appending the first next step onto the stepsArray.
    // When no "nextSteps" property is found, the function returns the current array of steps and
    // the process terminates.
    const process = (allSteps, previousStepsArray) => {
      const lastStep = previousStepsArray[previousStepsArray.length - 1];
      if (lastStep.nextSteps) {
        const nextStepName = lastStep.nextSteps[0];
        const nextStep = allSteps[nextStepName];
        const withNextStep = previousStepsArray.concat(nextStep);
        return process(allSteps, withNextStep);
      }
      return previousStepsArray;
    };

    const { currentStepIndex, stepsArray } = currentState;
    const { steps, start } = mappedWizardConfig;

    // The current step index defaults to 0, which is falsey. When our currentStepIndex is 0, we
    // should build the steps array starting with the start step.
    if (!currentStepIndex) {
      // This calls the recursive builder function with all of the available steps and an array
      // containing the "start" step.
      return process(steps, [steps[start]]);
    }

    // If our currentStepIndex is higher then 0, build the remainder of the array without altering
    // the order of the steps already completed by providing the recursive builder function a
    // starting array containing the completed steps.
    return process(steps, stepsArray.slice(0, currentStepIndex));
  }

  /**
   * completeStep is passed to and called by the step currently being displayed.
   * This method orchestrates several actions:
   *   - merges in any new inputData
   *   - if a buildStepsArray prop has been provided, updates the stepsArray
   *   - advances the display to the next step
   * @param {Object} newInputData the data provided by the step being completed.
   */
  completeStep = (newInputData) => {
    // If a `buildStepsArray` prop has been passed in, this component expects
    // the user's flow through the steps to be non-linear and based on user inputs.
    // This block updates the stepsArray.
    if (this.props.buildStepsArray) {
      const stepsArray = this.props.buildStepsArray(this.state, this.props.mappedWizardConfig);
      this.setState({ stepsArray });
    }

    const inputData = merge(this.state.inputData, newInputData);
    const currentStepIndex = this.state.currentStepIndex + 1;

    this.setState({
      currentStepIndex,
      inputData,
    });
  }

  /**
   * jumpToStep locates a step by its name, then updates state so the user is viewing that step.
   * @param  {String} name the name property of the step the user should be viewing
   */
  jumpToStep = (name) => {
    const currentStepIndex = findIndex(this.state.stepsArray, s => s.name === name);
    this.setState({
      currentStepIndex,
      // `jumpToStep` is only called from the final (review) step.
      // Thus, it should be the highestViewedStep.
      highestViewedStep: this.state.stepsArray.length,
    });
  }

  /**
   * nextStep is passed to and called by the wizard navigation bar.
   * When a user has navigated to already completed steps, calling this will
   * navigate the user to the next step, in the order that they were completed.
   */
  nextStep = () => {
    if (this.state.highestViewedStep >= this.state.currentStepIndex) {
      this.setState({
        currentStepIndex: this.state.currentStepIndex + 1,
      });
    }
    if (this.state.currentStepIndex >= this.state.highestViewedStep) {
      this.setState({
        highestViewedStep: undefined,
      });
    }
  }

  /**
   * Evaluate current state to determine if the "nextStep" button should be disabled.
   * @return {Boolean} true if the next step button should be disabled.
   */
  nextStepDisabled = () => {
    const {
      currentStepIndex,
      highestViewedStep,
      stepsArray,
    } = this.state;
    return (
      // A highestViewedStep is present, indicating user has gone to a previous step.
      // The user is currently not on the highestViewedStep
      !(highestViewedStep && highestViewedStep > currentStepIndex) ||
      // The user is not currently on the last (review) step
      currentStepIndex === stepsArray.length - 1
    );
  }

  /**
   * previousStep is passed to and called by the wizard navigation bar.
   * When a user has completed steps, calling this will navigate the user
   * to the previous step, in the order that they were completed.
   */
  previousStep = () => {
    if (this.state.currentStepIndex === 0) {
      return;
    }
    const highestViewedStep = this.state.highestViewedStep || this.state.currentStepIndex;
    this.setState({
      currentStepIndex: this.state.currentStepIndex - 1,
      highestViewedStep,
    });
  }

  /**
   * Evaluate current state to determine if the "previousStep" button should be disabled.
   * @return {Boolean} true if the previous step button should be disabled.
   */
  previousStepDisabled = () => {
    const { currentStepIndex } = this.state;
    return currentStepIndex === 0;
  }

  render() {
    const { currentStepIndex, stepsArray } = this.state;
    const { component: CurrentStepComponent } = stepsArray[currentStepIndex] || {};

    return (
      <Container>
        <Row>
          <Column modifiers={['col_3']}>
            <TextDiv modifiers={['heavyText', 'mediumText', 'midGreyText', 'uppercase']}>
              <FormattedMessage {...messages[this.props.configKey]} />
            </TextDiv>
          </Column>
          <Column modifiers={['col_6', 'end']}>
            <button
              disabled={this.previousStepDisabled()}
              onClick={this.previousStep}
            >
              <FontAwesome name={'chevron-up'} />
            </button>
            <button
              disabled={this.nextStepDisabled()}
              onClick={this.nextStep}
            >
              <FontAwesome name={'chevron-down'} />
            </button>
          </Column>
        </Row>
        <Row>
          <Column modifiers={['col']}>
            {CurrentStepComponent &&
              <CurrentStepComponent
                completeStep={this.completeStep}
                configKey={this.props.configKey}
                handleWizardCancel={this.props.handleWizardCancel}
                handleWizardSubmit={this.props.handleWizardSubmit}
                inputData={this.state.inputData}
                jumpToStep={this.jumpToStep}
                stepsArray={stepsArray}
              />
            }
          </Column>
        </Row>
      </Container>
    );
  }
}

export default withConnectedData(WizardController);
