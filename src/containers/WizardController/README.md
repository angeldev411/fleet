# Wizards

## How it works

1. Configuring a wizard:
  Specific wizards will require a various number of generic steps in a specific order. The steps available, their order, and the starting step, are all established in the `wizard_config` section of runtime.yml configuration file.

  An example of what that config looks like:
  ```
    SERVICE_REQUEST_WIZARD:
      steps:
        SERVICE_REQUEST_TYPE:
          - ASSET_SELECTION
        ASSET_SELECTION:
          - REVIEW

      start: SERVICE_REQUEST_TYPE
  ```

  Each wizard configuration is required to have `steps` and `start` attributes.

  Top level keys within `steps` indicate the steps available to the wizard. These step keys are mapped to generic components found in the wizard registry. They each contain an array of `nextSteps`. Unless a more specific ordering of steps is declared in the specific wizard controller (more on that later), the `nextSteps` arrays should only have a single element.

  Note that the `REVIEW` step is not specified at the same level as `SERVICE_REQUEST_TYPE` and `ASSET_SELECTION`. This is because the review step is both required and the last step. There is no need to specify `nextSteps`. The `REVIEW` step will be added to the `mappedWizardConfig` by default.

  Also added to the `mappedWizardConfig` by default is a `SpecificWizardController`. This controller must be added to the wizard registry under the same `configKey` you specify in the `wizard_config`.

2. The `SpecificWizardController`:

  Each wizard available in the application must have a `SpecificWizardController`. This controller handles logic including:
    - Cancelling a wizard.
    - Manipulating raw `inputData` into a data object useful to the api endpoint.
    - Posting data to the api endpoint.
    - Handling what happens after a successful post to the api endpoint.

  Additionally, the `SpecificWizardController` can control a user's flow through the steps in a non-linear fashion by adding and passing down a `buildStepsArray` prop. For more details on non-linear flow, see the Non-Linear Flow section below.

3. Rendering a wizard:
  After creating a modal or container that you would like to render a wizard into, add this line of code:
  ```
    <WizardController configKey="YOUR_WIZARD_CONFIG_KEY" />
  ```

## Building Wizard Steps

  Wizard steps should be a single component with dual functionality. This functionality should be switched by providing a prop `review` with a value of `true`.

  When the `review` prop is `undefined` or `false`, the step should accept a user's input. To save the user's input, the prop `completeStep` should be called with an object. This object will be merged with the previously existing `inputData` currently held in the `WizardController`. Additionally, calling `completeStep` will advance the user to the next step.

  When the `review` prop is `true`, the step should display the relevant pieces of information in a simple format. This style is used in the `REVIEW` step at the end of the wizard.

## Non-Linear Flow

  This is currently a special kind of challenge, and one that has not been fully resolved. At this time, I'll simply refer you to the `buildStepsArray` method in `src/WizardController/index.js`. Essentially, you'll need to take the inputs provided to that method and build a new `stepsArray` based on that data. Ensure that you do not corrupt the already completed steps while rebuilding the next iteration of the `stepsArray`.

  In the future, a technique for generalizing non-linear flow may present itself, but at the time of this writing no clear abstraction is available.

## How do I...

  - **validate a user's input?**

    Raw input validation should be handled at the step level before calling `this.props.completeStep(newInputData)`. Because each step's validation requirements and handling are unique, no pattern was observed that enabled generic data validations.

  - **do some other specific thing?**

    I don't know, but share the answer when you figure it out!
