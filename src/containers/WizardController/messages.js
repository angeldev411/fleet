import { defineMessages } from 'react-intl';

const messages = defineMessages({
  SERVICE_REQUEST_WIZARD: {
    id: 'containers.WizardController.SERVICE_REQUEST_WIZARD.title',
    defaultMessage: 'Create Service Request',
  },
});

export default messages;
