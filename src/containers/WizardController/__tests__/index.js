import React from 'react';

import {
  expect,
  test,
  shallow,
  createSpy,
  // spyOn,
} from '__tests__/helpers/test-setup';

import { WizardController } from '../index';

const step1 = () => <div />;
const step2 = () => <div />;
const review = () => <div />;

const defaultProps = {
  configKey: 'SERVICE_REQUEST_WIZARD',
  handleWizardCancel: () => {},
  handleWizardSubmit: () => {},
  mappedWizardConfig: {
    start: 'STEP_1',
    steps: {
      STEP_1: {
        component: step1,
        name: 'STEP_1',
        nextSteps: ['STEP_2'],
      },
      STEP_2: {
        component: step2,
        name: 'STEP_2',
        nextSteps: ['REVIEW'],
      },
      REVIEW: {
        component: review,
        name: 'REVIEW',
      },
    },
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<WizardController {...props} />);
}

test('on mounting, builds steps array and saves to state', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.state.stepsArray)
    .toEqual(instance.buildStepsArray(instance.state, defaultProps.mappedWizardConfig));
  expect(instance.state.currentStepIndex).toEqual(0);
});

test('buildStepsArray returns the expected stepsArray based on the state and props', () => {
  const component = shallowRender();
  const instance = component.instance();
  const stepsArray = instance.buildStepsArray(instance.state, instance.props.mappedWizardConfig);
  expect(stepsArray).toEqual([
    { component: step1, name: 'STEP_1', nextSteps: ['STEP_2'] },
    { component: step2, name: 'STEP_2', nextSteps: ['REVIEW'] },
    { component: review, name: 'REVIEW' },
  ]);
});

test('buildStepsArray prefers the buildStepsArray prop to internal processing', () => {
  const dummyArr = [];
  const buildStepsArray = createSpy().andReturn(dummyArr);
  const component = shallowRender({ ...defaultProps, buildStepsArray });
  const instance = component.instance();
  const stepsArray = instance.buildStepsArray(instance.state, instance.props.mappedWizardConfig);
  expect(buildStepsArray).toHaveBeenCalledWith(instance.state, instance.props.mappedWizardConfig);
  expect(stepsArray).toEqual(dummyArr);
});

test('completeStep increments currentStepIndex', () => {
  const component = shallowRender();
  const instance = component.instance();
  const currentStepIndex = instance.state.currentStepIndex;
  instance.completeStep();
  expect(instance.state.currentStepIndex).toEqual(currentStepIndex + 1);
});

test('completeStep merges in new input data with existing input data', () => {
  const component = shallowRender();
  component.setState({ inputData: { oldValue: 'oldValue' } });
  const instance = component.instance();
  instance.completeStep({ newValue: 'newValue' });
  expect(instance.state.inputData).toEqual({
    oldValue: 'oldValue',
    newValue: 'newValue',
  });
});

test('completeStep calls buildStepsArray and updates the stepsArray if prop provided', () => {
  const dummyArr = [];
  const buildStepsArray = createSpy().andReturn(dummyArr);
  const component = shallowRender({ ...defaultProps, buildStepsArray });
  component.setState({ currentStepIndex: 1 });
  const instance = component.instance();
  const currentState = instance.state;
  const currentMappedWizardConfig = instance.props.mappedWizardConfig;
  instance.completeStep({ newValue: 'newValue' });
  expect(buildStepsArray).toHaveBeenCalledWith(currentState, currentMappedWizardConfig);
  expect(instance.state.stepsArray).toEqual(dummyArr);
});

test('jumpToStep sets the currentStepIndex to match the correct step', () => {
  const component = shallowRender();
  component.setState({ currentStepIndex: 2 });
  const instance = component.instance();
  const stepsArray = instance.state.stepsArray;
  instance.jumpToStep(stepsArray[0].name);
  expect(instance.state.currentStepIndex).toEqual(0);
  expect(instance.state.highestViewedStep).toEqual(stepsArray.length);
});

test('nextStep increments the currentStepIndex if highestViewedStep is greater', () => {
  const component = shallowRender();
  component.setState({ currentStepIndex: 1, highestViewedStep: 3 });
  const instance = component.instance();
  instance.nextStep();
  expect(instance.state.currentStepIndex).toEqual(2);
  expect(instance.state.highestViewedStep).toEqual(3);
});

test('nextStep does NOT increment the currentStepIndex if highestViewedStep undefined', () => {
  const component = shallowRender();
  component.setState({ currentStepIndex: 3, highestViewedStep: undefined });
  const instance = component.instance();
  instance.nextStep();
  expect(instance.state.currentStepIndex).toEqual(3);
  expect(instance.state.highestViewedStep).toEqual(undefined);
});

test('nextStep clears highestViewedStep if currentStepIndex is GT or ET', () => {
  const component = shallowRender();
  component.setState({ currentStepIndex: 2, highestViewedStep: 3 });
  const instance = component.instance();
  instance.nextStep();
  expect(instance.state.currentStepIndex).toEqual(3);
  expect(instance.state.highestViewedStep).toEqual(undefined);
});

test('previousStep does NOT update currentStepIndex if already zero', () => {
  const component = shallowRender();
  component.setState({ currentStepIndex: 0 });
  const instance = component.instance();
  instance.previousStep();
  expect(instance.state.currentStepIndex).toEqual(0);
});

test('previousStep decrements currentStepIndex and sets highestViewedStep', () => {
  const component = shallowRender();
  component.setState({ currentStepIndex: 3 });
  const instance = component.instance();
  instance.previousStep();
  expect(instance.state.currentStepIndex).toEqual(2);
  expect(instance.state.highestViewedStep).toEqual(3);
});
