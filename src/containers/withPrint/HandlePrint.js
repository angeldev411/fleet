import PropTypes from 'prop-types';
import React, { Component } from 'react';

class HandlePrint extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    printPageRequested: PropTypes.bool.isRequired,
    requestPrintPage: PropTypes.func.isRequired,
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.printPageRequested && nextProps.printPageRequested) {
      setTimeout(() => {
        window.print();
      }, 300);
      this.props.requestPrintPage(false);
    }
  }

  render() {
    return React.Children.only(this.props.children);
  }
}

export default HandlePrint;
