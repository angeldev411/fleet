import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import {
  requestPrintPage as requestPrintPageAction,
} from 'redux/ui/actions';

import {
  requestPrintPageSelector,
} from 'redux/ui/selectors';

import HandlePrint from './HandlePrint';

const withPrint = (WrappedComponent) => {
  function WithPrint(props) {
    const { printPageRequested, requestPrintPage, ...childProps } = props;

    return (
      <HandlePrint
        printPageRequested={printPageRequested}
        requestPrintPage={requestPrintPage}
      >
        <WrappedComponent {...childProps} />
      </HandlePrint>
    );
  }

  WithPrint.propTypes = {
    printPageRequested: PropTypes.bool,
    requestPrintPage: PropTypes.func.isRequired,
  };

  WithPrint.defaultProps = {
    printPageRequested: false,
  };

  function mapStateToProps(state) {
    return {
      printPageRequested: requestPrintPageSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      requestPrintPage: requested => dispatch(requestPrintPageAction(requested)),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(WithPrint);
};

export default withPrint;
