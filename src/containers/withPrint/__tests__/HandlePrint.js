import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  spyOn,
} from '__tests__/helpers/test-setup';

import HandlePrint from '../HandlePrint';

const ChildComponent = () => <div />;

const defaultProps = {
  printPageRequested: false,
  requestPrintPage: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(
    <HandlePrint {...props}>
      <ChildComponent />
    </HandlePrint>,
  );
}

test('renders the child component', () => {
  expect(shallowRender()).toBeA(ChildComponent);
});

test.cb('calls printPage when printPageRequested is changed to true', (t) => {
  t.plan(1);
  const printSpy = spyOn(window, 'print');
  const component = shallowRender();
  component.setProps({ printPageRequested: true });
  setTimeout(() => {
    expect(printSpy).toHaveBeenCalled();
    t.pass();
    t.end();
  }, 500);
});
