import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { handleActionBarItemClick } from 'redux/ui/actions';

import QuickActionBar from 'components/QuickActionBar';

import quickActionItems from './constants';

export function QuickActionBarContainer({ pageTitle, handleActionBarItem, disabled }) {
  return (
    <QuickActionBar
      items={quickActionItems[pageTitle]}
      onItemClicked={handleActionBarItem}
      disabled={disabled}
    />
  );
}

QuickActionBarContainer.propTypes = {
  pageTitle: PropTypes.string.isRequired,
  handleActionBarItem: PropTypes.func.isRequired,
  disabled: PropTypes.shape(),
};

QuickActionBarContainer.defaultProps = {
  disabled: {},
};

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    handleActionBarItem: payload => dispatch(handleActionBarItemClick(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  QuickActionBarContainer,
);
