import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  mount,
  actionAndSpy,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';
import * as uiActions from 'redux/ui/actions';

import QuickActionBarContainerMount, { QuickActionBarContainer } from '../index';

const pageTitle = 'CASE_PAGE';
const handleActionBarItem = noop;

const defaultProps = {
  pageTitle,
  handleActionBarItem,
};

const shallowRender = (props = defaultProps) => shallow(<QuickActionBarContainer {...props} />);
const mountContainer = (props = defaultProps) => mount(
  <MountableTestComponent authorized>
    <QuickActionBarContainerMount {...props} />
  </MountableTestComponent>,
);

test('renders QuickActionBar.', () => {
  const component = shallowRender();
  expect(component).toBeA('QuickActionBar');
});

test('handleActionBarItemClick is called when the prop function of QuickActionBarContainer is called.', () => {
  const handleActionBarItemClickSpy = actionAndSpy(uiActions, 'handleActionBarItemClick');
  const component = mountContainer();
  const payload = { test: 'testPayload' };
  component.find('QuickActionBar').props().onItemClicked(payload);
  expect(handleActionBarItemClickSpy).toHaveBeenCalledWith(payload);
});
