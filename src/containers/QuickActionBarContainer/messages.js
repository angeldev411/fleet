import { defineMessages } from 'react-intl';

const messages = defineMessages({
  addNote: {
    id: 'containers.QuickActionBar.button.addNote',
    defaultMessage: 'Add Note',
  },
  asset: {
    id: 'containers.QuickActionBar.dropdown.asset',
    defaultMessage: 'Asset',
  },
  caseStatus: {
    id: 'containers.QuickActionBar.dropdown.caseStatus',
    defaultMessage: 'Case Status',
  },
  complaint: {
    id: 'containers.QuickActionBar.dropdown.complaint',
    defaultMessage: 'Complaint',
  },
  diagnostics: {
    id: 'containers.QuickActionBar.dropdown.diagnostics',
    defaultMessage: 'Diagnostics',
  },
  estimateInvoiceDecided: {
    id: 'containers.QuickActionBar.dropdown.estimateInvoiceDecided',
    defaultMessage: 'Estimate/Invoice',
  },
  estimateInvoicePending: {
    id: 'containers.QuickActionBar.dropdown.estimateInvoicePending',
    defaultMessage: 'Estimate/Invoice',
  },
  jumpTo: {
    id: 'containers.QuickActionBar.button.jumpTo',
    defaultMessage: 'Jump To...',
  },
  location: {
    id: 'containers.QuickActionBar.dropdown.location',
    defaultMessage: 'Location',
  },
  notes: {
    id: 'containers.QuickActionBar.dropdown.notes',
    defaultMessage: 'Notes',
  },
  options: {
    id: 'containers.QuickActionBar.button.options',
    defaultMessage: 'Options',
  },
  printPage: {
    id: 'containers.QuickActionBar.dropdown.printPage',
    defaultMessage: 'Print Page',
  },
  requestService: {
    id: 'containers.QuickActionBar.button.requestService',
    defaultMessage: 'Request Service',
  },
  serviceProvider: {
    id: 'containers.QuickActionBar.dropdown.serviceProvider',
    defaultMessage: 'Service Provider',
  },
  serviceRequest: {
    id: 'containers.QuickActionBar.dropdown.serviceRequest',
    defaultMessage: 'Service Request',
  },
  viewInFullScreenMode: {
    id: 'containers.QuickActionBar.dropdown.viewInFullScreenMode',
    defaultMessage: 'View In Full-Screen Mode',
  },
  // In alpha version we don't have assignCase, addAttachment, and setFollowup.
  // addAttachment: {
  //   id: 'containers.QuickActionBar.button.addAttachment',
  //   defaultMessage: 'Add Attachment',
  // },
  // assignCase: {
  //   id: 'containers.QuickActionBar.button.assignCase',
  //   defaultMessage: 'Assign Case',
  // },
  // setFollowUp: {
  //   id: 'containers.QuickActionBar.button.setFollowUp',
  //   defaultMessage: 'Set Follow Up',
  // },
});

export default messages;
