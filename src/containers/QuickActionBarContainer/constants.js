import { compact } from 'lodash';

import oldIEVersion from 'utils/oldIEVersion';

import messages from './messages';

// actionbar item ids
export const ADD_NOTE = 'ADD_NOTE';
export const JUMP_TO = 'JUMP_TO';
export const OPTIONS = 'OPTIONS_BUTTON';

// react-scroll anchor names
export const ASSET = 'ASSET';
export const CASE_STATUS = 'CASE_STATUS';
export const COMPLAINT = 'COMPLAINT';
export const DIAGNOSTICS = 'DIAGNOSTICS';
export const ESTIMATE_INVOICE_DECIDED = 'ESTIMATE_INVOICE_DECIDED';
export const ESTIMATE_INVOICE_PENDING = 'ESTIMATE_INVOICE_PENDING';
export const LOCATION = 'LOCATION';
export const NOTES = 'NOTES';
export const SERVICE_PROVIDER = 'SERVICE_PROVIDER';
export const SERVICE_REQUEST = 'SERVICE_REQUEST';

const QUICK_ACTION_ITEMS = {
  ASSET_PAGE: {
    LEFT_ITEMS: [
      // In alpha version we don't have addContact.
      // {
      //   id: 'addContact',
      //   icon: 'user-plus',
      //   label: messages.addContact,
      // },
      {
        id: 'requestService',
        icon: 'wrench',
        label: messages.requestService,
      },
    ],
    RIGHT_ITEMS: [
      {
        id: OPTIONS,
        icon: 'caret-down',
        label: messages.options,
        options: compact([
          {
            id: 'printPage',
            label: messages.printPage,
          },
          !oldIEVersion() && {
            id: 'viewInFullScreenMode',
            label: messages.viewInFullScreenMode,
          },
        ]),
      },
    ],
  },

  CASE_PAGE: {
    LEFT_ITEMS: [
      {
        id: ADD_NOTE,
        icon: 'envelope-o',
        label: messages.addNote,
      },
      // In alpha version we don't have assignCase, addAttachment, and setFollowup.
      // {
      //   id: 'assignCase',
      //   icon: 'user-o',
      //   label: messages.assignCase,
      // },
      // {
      //   id: 'addAttachment',
      //   icon: 'paperclip',
      //   label: messages.addAttachment,
      // },
      // {
      //   id: 'setFollowUp',
      //   icon: 'flag-o',
      //   label: messages.setFollowUp,
      // },
    ],
    RIGHT_ITEMS: [
      {
        id: JUMP_TO,
        icon: 'caret-down',
        iconPressed: 'caret-up',
        label: messages.jumpTo,
        options: [
          {
            id: CASE_STATUS,
            label: messages.caseStatus,
          },
          {
            id: ASSET,
            label: messages.asset,
          },
          {
            id: NOTES,
            label: messages.notes,
          },
          {
            id: SERVICE_PROVIDER,
            label: messages.serviceProvider,
          },
          {
            id: COMPLAINT,
            label: messages.complaint,
          },
          {
            id: SERVICE_REQUEST,
            label: messages.serviceRequest,
          },
          {
            id: DIAGNOSTICS,
            label: messages.diagnostics,
          },
          {
            id: LOCATION,
            label: messages.location,
          },
          {
            id: ESTIMATE_INVOICE_DECIDED,
            label: messages.estimateInvoiceDecided,
          },
          {
            id: ESTIMATE_INVOICE_PENDING,
            label: messages.estimateInvoicePending,
          },
        ],
      },
      {
        id: OPTIONS,
        icon: 'caret-down',
        iconPressed: 'caret-up',
        label: messages.options,
        options: compact([
          {
            id: 'printPage',
            label: messages.printPage,
          },
          !oldIEVersion() && {
            id: 'viewInFullScreenMode',
            label: messages.viewInFullScreenMode,
          },
        ]),
      },
    ],
  },

  CASES_PAGE: {
    LEFT_ITEMS: [
      {
        id: ADD_NOTE,
        icon: 'envelope-o',
        label: messages.addNote,
      },
    ],
    RIGHT_ITEMS: [],
  },
};

export default QUICK_ACTION_ITEMS;
