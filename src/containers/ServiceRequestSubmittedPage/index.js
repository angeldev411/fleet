import { H1, P } from 'base-components';
import PropTypes from 'prop-types';
import React from 'react';
import {
  Container,
  Row,
  Column,
} from 'styled-components-reactive-grid';

import Link from 'elements/Link';

// TODO: build this page!
/* istanbul ignore next */
function ServiceRequestSubmittedPage({
  match: {
    params: {
      serviceRequestId,
    },
  },
}) {
  return (
    <Container id="service-request-submitted-page">
      <Row>
        <Column modifiers={['col_offset_1', 'col_8']}>
          <Row>
            <Column modifiers={['col_12']}>
              <H1>Service Request Submitted.</H1>
            </Column>
          </Row>
          <Row>
            <Column modifiers={['col_12']}>
              <P>This page is just mocked out. Extensive additional work is required</P>
            </Column>
          </Row>
          <Row>
            <Column modifiers={['col_offset_6', 'col_6']}>
              <Row>
                <Column modifiers={['col']}>
                  <Link to="/service-requests/new">Create Another Service Request</Link>
                </Column>
                <Column modifiers={['col']}>
                  <Link to={`/service-requests/${serviceRequestId}`}>View Service Request Details</Link>
                </Column>
              </Row>
            </Column>
          </Row>
        </Column>
      </Row>
    </Container>
  );
}

ServiceRequestSubmittedPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      serviceRequestId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default ServiceRequestSubmittedPage;
