import { noop } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import CaseDetailData from '../Component';

const TestDiv = () => <div id="test-div" />;

const caseId = '123';

const defaultProps = {
  addPageToBreadcrumbs: noop,
  caseError: {},
  caseInfo: {},
  ChildComponent: TestDiv,
  componentProps: {},
  intl: { formatMessage: message => message.defaultMessage },
  loadCase: noop,
  location: { pathname: 'some-path' },
  match: { params: { caseId } },
  setCurrentCase: noop,
  setLoadingStarted: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<CaseDetailData {...props} />);
}

test('mounting sets current case with id in route on mounting', () => {
  const setCurrentCase = createSpy();
  const testProps = {
    ...defaultProps,
    setCurrentCase,
  };
  const component = shallowRender(testProps);
  component.instance();
  expect(setCurrentCase).toHaveBeenCalledWith(caseId);
});

test('componentDidMount loads the case and sets loading started', () => {
  const loadCase = createSpy();
  const setLoadingStarted = createSpy();
  const testProps = {
    ...defaultProps,
    loadCase,
    setLoadingStarted,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.componentDidMount();
  expect(loadCase).toHaveBeenCalledWith(caseId);
  expect(setLoadingStarted).toHaveBeenCalled();
});

test('componentWillReceiveProps adds page to breadcrumbs when case info loads', () => {
  const addPageToBreadcrumbs = createSpy();
  const testProps = {
    ...defaultProps,
    addPageToBreadcrumbs,
  };
  const component = shallowRender(testProps);
  component.setProps({ caseInfo: { id: caseId } });
  expect(addPageToBreadcrumbs).toHaveBeenCalled();
});

test('componentWillReceiveProps does not add page to breadcrumbs when case already loaded', () => {
  const addPageToBreadcrumbs = createSpy();
  const caseInfo = { id: caseId };
  const testProps = {
    ...defaultProps,
    caseInfo,
    addPageToBreadcrumbs,
  };
  const component = shallowRender(testProps);
  component.setProps({ caseInfo });
  expect(addPageToBreadcrumbs).toNotHaveBeenCalled();
});

test('componentWillUnmount sets the current case to undefined', () => {
  const setCurrentCase = createSpy();
  const testProps = {
    ...defaultProps,
    setCurrentCase,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  setCurrentCase.reset();
  expect(setCurrentCase).toNotHaveBeenCalled();
  instance.componentWillUnmount();
  expect(setCurrentCase).toHaveBeenCalledWith();
});

test('renders the ChildComponent with the componentProps', () => {
  const component = shallowRender();
  expect(component.find(defaultProps.ChildComponent))
    .toHaveProps({ ...defaultProps.componentProps });
});

test('renders "NotFoundPage" if the status of response is 400', () => {
  const testProps = {
    ...defaultProps,
    caseError: {
      status: 400,
    },
  };
  const component = shallowRender(testProps);
  expect(component).toContain('NotFoundPage');
});
