import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Helmet from 'react-helmet';

import NotFoundPage from 'components/NotFoundPage';

import { dataNotFound } from 'utils/httpUtils';

import messages from './messages';

class CaseDetailData extends Component {
  static propTypes = {
    addPageToBreadcrumbs: PropTypes.func.isRequired,
    caseError: PropTypes.shape({
      error: PropTypes.oneOf([true]),
    }),
    caseInfo: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
    ChildComponent: PropTypes.func.isRequired,
    // This component doesn't care what is in componentProps, only that it is an object.
    // eslint-disable-next-line react/forbid-prop-types
    componentProps: PropTypes.object.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    loadCase: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        caseId: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    setCurrentCase: PropTypes.func.isRequired,
    setLoadingStarted: PropTypes.func.isRequired,
  };

  static defaultProps = {
    caseError: {},
  };

  componentWillMount() {
    const { match: { params: { caseId } } } = this.props;
    this.props.setCurrentCase(caseId);
  }

  componentDidMount() {
    const { match: { params: { caseId } } } = this.props;
    this.props.loadCase(caseId);
    this.props.setLoadingStarted();
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.caseInfo.id && nextProps.caseInfo.id) {
      this.props.addPageToBreadcrumbs({
        path: this.props.location.pathname,
        message: messages.breadcrumbs,
        number: nextProps.caseInfo.id,
      });
    }
  }

  componentWillUnmount() {
    this.props.setCurrentCase();
  }

  render() {
    const {
      caseError,
      ChildComponent,
      intl: { formatMessage },
    } = this.props;

    if (dataNotFound(caseError)) {
      return <NotFoundPage />;
    }

    return (
      <div>
        <Helmet
          title={formatMessage(messages.title)}
          meta={[{ name: 'description', content: formatMessage(messages.description) }]}
        />
        <ChildComponent {...this.props.componentProps} />
      </div>
    );
  }
}

export default CaseDetailData;
