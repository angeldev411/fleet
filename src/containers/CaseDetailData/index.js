import PropTypes from 'prop-types';
import React from 'react';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import {
  addPageToBreadcrumbs,
  setLoadingStarted,
} from 'redux/app/actions';
import {
  loadCase,
  setCurrentCase,
} from 'redux/cases/actions';
import {
  currentCaseSelector,
} from 'redux/cases/selectors';
import {
  loadCaseRequestErrorSelector,
} from 'redux/requests/selectors';

import immutableToJS from 'utils/immutableToJS';

import Component from './Component';

/* istanbul ignore next */
const withCaseDetailData = (DetailsPage) => {
  function WithCaseDetailData(props) {
    return (
      <Component {...props} ChildComponent={DetailsPage} />
    );
  }

  WithCaseDetailData.propTypes = {
    addPageToBreadcrumbs: PropTypes.func.isRequired,
    caseError: PropTypes.shape({
      error: PropTypes.oneOf([true]),
    }),
    caseInfo: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
    // This component doesn't care what is in componentProps, only that it is an object.
    // eslint-disable-next-line react/forbid-prop-types
    componentProps: PropTypes.object.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    loadCase: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        caseId: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    setCurrentCase: PropTypes.func.isRequired,
    setLoadingStarted: PropTypes.func.isRequired,
  };

  WithCaseDetailData.defaultProps = {
    caseError: {},
  };

  function mapStateToProps(state) {
    return {
      caseError: loadCaseRequestErrorSelector(state),
      caseInfo: currentCaseSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      addPageToBreadcrumbs: breadcrumbs => dispatch(addPageToBreadcrumbs(breadcrumbs)),
      loadCase: caseId => dispatch(loadCase({ caseId })),
      setCurrentCase: caseId => dispatch(setCurrentCase(caseId)),
      setLoadingStarted: () => dispatch(setLoadingStarted(true)),
    };
  }

  return compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    injectIntl,
    immutableToJS,
  )(WithCaseDetailData);
};

export default withCaseDetailData;
