import { defineMessages } from 'react-intl';

const messages = defineMessages({
  breadcrumbs: {
    id: 'containers.CaseDetailData.meta.breadcrumbs',
    defaultMessage: 'Case {number}',
  },
  description: {
    id: 'containers.CaseDetailData.meta.description',
    defaultMessage: 'The case page of the application',
  },
  title: {
    id: 'containers.CaseDetailData.meta.title',
    defaultMessage: 'The Case Page',
  },
});

export default messages;
