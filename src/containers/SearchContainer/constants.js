import messages from './messages';

// eslint-disable-next-line import/prefer-default-export
export const SEARCH_OPTIONS = [
  {
    default: true,
    id: 'unitNumber',
    label: messages.unitNumber,
    params: {
      searchParam: 'unit_no',
    },
  },
  {
    id: 'vin',
    label: messages.vin,
    params: {
      searchParam: 'vin',
    },
  },
];
