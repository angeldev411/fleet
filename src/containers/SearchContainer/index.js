import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { performSearch } from 'redux/app/actions';

import { clearCases } from 'redux/cases/actions';

import {
  requestingSelector,
  noAssetsCasesSelector,
} from 'redux/combinedSelectors';

import { parseQueryParams } from 'utils/url';

import Search from 'components/Search';

import { SEARCH_OPTIONS } from './constants';

export const SEARCH_RESULT_COUNT_PER_PAGE = 20;

/**
 * The SearchContainer is a smart component that controls the global search bar within the TopNav.
 * Responsibilities include:
 *   - Holding and manipulating search based state in a single location.
 *   - Displaying, hiding, and selecting a search options (unit number, vin, etc...)
 *   - Determining which search option to apply to the search (unit number, vin, etc...)
 *   - Parsing through search suggestion results to find matches.
 *   - Dispatching actions that trigger the actual the searches.
 */
export class SearchContainer extends Component {
  static propTypes = {
    // clearCases is used to remove all cases, assets, and other data
    // (including favorites pagination data) currently held in state.
    clearCases: PropTypes.func.isRequired,

    // history is provided by the HOC withRouter, and allows us to change routes
    // without rendering a redirect component.
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,

    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    noAssetsCases: PropTypes.bool,

    // dispatches an action triggering the search requests to be made.
    performSearch: PropTypes.func.isRequired,

    requesting: PropTypes.bool,

    // These are the filters that narrow the scope of where we are searching,
    // unit number, vin, etc...
    searchOptions: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        params: PropTypes.shape({
          searchParam: PropTypes.string.isRequired,
        }).isRequired,
        label: PropTypes.shape({
          id: PropTypes.string.isRequired,
          defaultMessage: PropTypes.string.isRequired,
        }).isRequired,
      }),
    ),

    // // TODO: This is an example of how to setup typeahead search suggestions.
    // // These are the results from the api request to get the type ahead search suggestions.
    // suggestionResults: PropTypes.arrayOf(
    //   PropTypes.shape({
    //     id: PropTypes.string.isRequired,
    //     type: PropTypes.string.isRequired,
    //   }),
    // ),
  };

  static defaultProps = {
    noAssetsCases: false,
    requesting: false,

    // Supply the "search options" via props. This increases our ability to expand on this later.
    searchOptions: SEARCH_OPTIONS,
  };

  state = {
    // Controls whether the "search options" dropdown menu is displayed.
    displaySearchOptions: false,

    // Controls whether the search input is displayed
    expanded: false,

    // The filtered list of suggestions. This is what will be displayed as suggestion options.
    searchSuggestions: [],

    // The selected search option, or the default search option.
    selectedSearchOption: this.props.searchOptions.find(option => option.default),

    searchValue: '',
  };

  componentDidMount() {
    const { location } = this.props;
    const params = parseQueryParams(location.search);
    this.performSearchFromQueryParams(params);
  }

  componentWillReceiveProps(nextProps) {
    const { location, noAssetsCases, requesting } = nextProps;

    if (location.pathname !== '/search') {
      this.setState({ searchValue: '' }); // Remove search value on route redirection
    } else if (!requesting) {
      if (noAssetsCases) {
        this.setState({ expanded: true });
      } else {
        this.setState({
          expanded: false,
          searchValue: '',
        });
      }
    }
  }

  componentWillUnmount() {
    this.props.clearCases();
  }

  /**
   * Handles click events outside of the search component.
   */
  onOutsideClick = () => {
    this.setState({ expanded: false });
  }

  /**
   * Handles calling the api, processing results, and saving the type ahead search suggestions.
   */
  getSearchSuggestions = (/* { value } */) => {
    // TODO: Implement type ahead search suggestions.
    // Call the api. Get the search suggestions based on the input value.
    // Process the results from the api into the results that will be suggested to the user.
    // Save the search suggestions to `this.state.searchSuggestions`
  }

  /**
   * Handles resetting the type ahead suggestions to an empty array.
   */
  clearSearchSuggestions = () => {
    // this.setState({ searchSuggestions: [] });
  }

  /**
   * Event handler for selecting a new search option.
   * @param  {Event} event         A DOM event, typically a click.
   * @param  {Object} searchOption The selected search option.
   */
  handleSearchOptionSelect = (event, searchOption) => {
    event.preventDefault();
    this.setState({
      displaySearchOptions: false,
      selectedSearchOption: searchOption,
    });
  }

  handleSearchValueChange = (event, { newValue }) => {
    this.setState({ searchValue: newValue });
  }

  /**
   * Handles performing the full search from the user's input.
   */
  performSearch = () => {
    const { searchValue, selectedSearchOption } = this.state;
    const { searchParam } = selectedSearchOption.params;
    if (searchValue && searchValue.length > 0) {
      this.toggleSearchExpanded({ expanded: false });
      this.props.clearCases();
      this.props.performSearch({
        searchOption: selectedSearchOption.params,
        searchValue,
        per_page: SEARCH_RESULT_COUNT_PER_PAGE,
      });

      // Active tab defaults to Cases
      this.props.history.push(`/search?by=${searchParam}&value=${searchValue}&active=cases`);
    }
  }

  /**
   * Handles performing the search from query params
   */
  performSearchFromQueryParams = (params) => {
    const searchValue = params.value;
    if (searchValue && searchValue.length > 0) {
      this.props.performSearch({
        searchOption: {
          searchParam: params.by,
        },
        searchValue: params.value,
        per_page: SEARCH_RESULT_COUNT_PER_PAGE,
      });
    }
  }

  /**
   * Handles toggling the display of the search options dropdown.
   */
  toggleDisplaySearchOptions = () => {
    this.setState({
      displaySearchOptions: !this.state.displaySearchOptions,
    });
  };

  /**
   * Handles toggling of the search bar's expanded state.
  */
  toggleSearchExpanded = () => {
    this.setState({
      expanded: !this.state.expanded,
    });
  }

  render() {
    const {
      searchOptions,
    } = this.props;
    const {
      displaySearchOptions,
      expanded,
      searchSuggestions,
      selectedSearchOption,
      searchValue,
    } = this.state;

    return (
      <Search
        displaySearchOptions={displaySearchOptions}
        expanded={expanded}
        getSearchSuggestions={this.getSearchSuggestions}
        clearSearchSuggestions={this.clearSearchSuggestions}
        handleSearchOptionSelect={this.handleSearchOptionSelect}
        handleSearchValueChange={this.handleSearchValueChange}
        performSearch={this.performSearch}
        searchOptions={searchOptions}
        searchSuggestions={searchSuggestions}
        searchValue={searchValue}
        selectedSearchOption={selectedSearchOption}
        toggleDisplaySearchOptions={this.toggleDisplaySearchOptions}
        toggleSearchExpanded={this.toggleSearchExpanded}
        onOutsideClick={this.onOutsideClick}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    requesting: requestingSelector(state),
    noAssetsCases: noAssetsCasesSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    clearCases: () => dispatch(clearCases()),
    performSearch: params => dispatch(performSearch(params)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SearchContainer));
