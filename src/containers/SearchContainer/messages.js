import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  unitNumber: {
    id: 'components.TopNav.Search.Option.unitNumber',
    defaultMessage: 'Unit Number',
  },
  vin: {
    id: 'components.TopNav.Search.Option.vin',
    defaultMessage: 'VIN',
  },
});

export default formattedMessages;
