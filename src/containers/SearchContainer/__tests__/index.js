import React from 'react';
import { noop } from 'lodash';
import {
  test,
  expect,
  shallow,
  createSpy,
  spyOn,
  actionAndSpy,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';
import * as casesActions from 'redux/cases/actions';
import * as appActions from 'redux/app/actions';

import SearchContainerMount, {
  SearchContainer,
  SEARCH_RESULT_COUNT_PER_PAGE,
} from '../index';
import { SEARCH_OPTIONS } from '../constants';

const defaultProps = {
  clearCases: noop,
  history: {
    push: noop,
  },
  intl: {
    formatMessage: noop,
  },
  location: {
    pathname: '/search',
    search: '',
  },
  noAssetsCases: false,
  performSearch: noop,
  requesting: false,
  searchOptions: SEARCH_OPTIONS,
};

const defaultState = {
  displaySearchOptions: false,
  expanded: false,
  searchSuggestions: [
    { item: 'suggestion 1' },
    { item: 'suggestion 2' },
  ],
  selectedSearchOption: defaultProps.searchOptions.find(option => option.default),
  searchValue: 'test value',
};

function shallowRender(props = defaultProps) {
  return shallow(<SearchContainer {...props} />);
}

const fullRender = (props = defaultProps) => mount(
  <MountableTestComponent authorized>
    <SearchContainerMount {...props} />
  </MountableTestComponent>,
);

test('has expected props', () => {
  const component = shallowRender();
  const instance = component.instance();
  instance.setState(defaultState);
  expect(component).toHaveProps({
    displaySearchOptions: defaultState.displaySearchOptions,
    expanded: defaultState.expanded,
    getSearchSuggestions: component.getSearchSuggestions,
    clearSearchSuggestions: component.clearSearchSuggestions,
    handleSearchOptionSelect: component.handleSearchOptionSelect,
    handleSearchValueChange: component.handleSearchValueChange,
    searchOptions: defaultProps.searchOptions,
    searchSuggestions: defaultState.searchSuggestions,
    searchValue: defaultState.searchValue,
    selectedSearchOption: defaultState.selectedSearchOption,
    toggleDisplaySearchOptions: component.toggleDisplaySearchOptions,
    toggleSearchExpanded: component.toggleSearchExpanded,
    onOutsideClick: component.onOutsideClick,
  });
});

/* --------------------- componentDidMount ------------------------- */

test('opening the page initiates a search from query params', () => {
  const location = {
    pathname: '/some_path',
    search: '?param1=abc&param2=def',
  };
  const testProps = {
    ...defaultProps,
    location,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  const performSearchSpy = spyOn(instance, 'performSearchFromQueryParams');
  instance.componentDidMount();
  expect(performSearchSpy).toHaveBeenCalledWith({
    param1: 'abc',
    param2: 'def',
  });
});

/* --------------------- componentWillReceiveProps ------------------------- */

test('on receiving props, removes searchValue state if route changes', () => {
  const location = {
    pathname: '/some_other_route',
    search: '',
  };
  const testProps = {
    ...defaultProps,
    location,
  };
  const component = shallowRender();
  const instance = component.instance();
  component.setState({ searchValue: 'some value' });
  instance.componentWillReceiveProps(testProps);
  expect(component.state('searchValue')).toBe('');
});

test('on receiving props, if requesting is true, does nothing', () => {
  const testProps = {
    ...defaultProps,
    requesting: true,
  };
  const component = shallowRender();
  const instance = component.instance();
  component.setState(defaultState);
  instance.componentWillReceiveProps(testProps);
  expect(component.state()).toEqual(defaultState);
});

test('on receiving props, if requesting is false AND noAssetsCases is true, expands search bar', () => {
  const testProps = {
    ...defaultProps,
    requesting: false,
    noAssetsCases: true,
  };
  const component = shallowRender();
  const instance = component.instance();
  instance.componentWillReceiveProps(testProps);
  expect(component.state('expanded')).toBe(true);
});

test('on receiving props, if requesting is false AND noAssetsCases is false, removes search value', () => {
  const testProps = {
    ...defaultProps,
    requesting: false,
    noAssetsCases: false,
  };
  const component = shallowRender();
  const instance = component.instance();
  instance.componentWillReceiveProps(testProps);
  expect(component.state()).toInclude({
    expanded: false,
    searchValue: '',
  });
});

/* --------------------- componentWillUnmount ------------------------- */

test('clears cases on componentWillUnmount', () => {
  const clearCasesSpy = createSpy();
  const testProps = {
    ...defaultProps,
    clearCases: clearCasesSpy,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.componentWillUnmount();
  expect(clearCasesSpy).toHaveBeenCalled();
});

/* --------------------- onOutsideClick ------------------------- */

test('onOutsideClick updates the state', () => {
  const component = shallowRender();
  const instance = component.instance();
  instance.state.expanded = true;
  instance.onOutsideClick();
  expect(instance.state.expanded).toBe(false);
});

/* --------------------- getSearchSuggestions ------------------------- */

/* --------------------- clearSearchSuggestions ------------------------- */

/* --------------------- handleSearchOptionSelect ------------------------- */

test('handleSearchOptionSelect prevents the default event', () => {
  const preventDefault = createSpy();
  const component = shallowRender();
  const instance = component.instance();
  instance.handleSearchOptionSelect({ preventDefault }, 'test');
  expect(preventDefault).toHaveBeenCalled();
});

test('handleSearchOptionSelect updates displaySearchOptions and selectedSearchOption', () => {
  const preventDefault = createSpy();
  const component = shallowRender();
  const instance = component.instance();
  instance.state.displaySearchOptions = true;
  const newOption = 'Kraft Macaroni';
  instance.handleSearchOptionSelect({ preventDefault }, newOption);
  expect(instance.state.displaySearchOptions).toBe(false);
  expect(instance.state.selectedSearchOption).toEqual(newOption);
});

/* --------------------- handleSearchValueChange ------------------------- */

test('handleSearchValueChange sets the searchValue state', () => {
  const newValue = 'new search value';
  const component = shallowRender();
  const instance = component.instance();
  instance.handleSearchValueChange(null, { newValue });
  expect(component.state('searchValue')).toEqual(newValue);
});

/* --------------------- performSearch ------------------------- */

test('performSearch does not call other functions if searchValue contains 0 characters', () => {
  const clearCasesSpy = createSpy();
  const performSearchSpy = createSpy();
  const historyPushSpy = createSpy();
  const component = shallowRender({
    ...defaultProps,
    clearCases: clearCasesSpy,
    performSearch: performSearchSpy,
    history: { push: historyPushSpy },
  });
  const instance = component.instance();
  const toggleSearchExpandedSpy = spyOn(instance, 'toggleSearchExpanded');
  component.setState({ searchValue: '' });
  instance.performSearch();
  expect(clearCasesSpy).toNotHaveBeenCalled();
  expect(performSearchSpy).toNotHaveBeenCalled();
  expect(historyPushSpy).toNotHaveBeenCalled();
  expect(toggleSearchExpandedSpy).toNotHaveBeenCalled();
});

test('performSearch calls toggleSearchExpanded with expanded = false', () => {
  const component = shallowRender();
  const instance = component.instance();
  const toggleSearchExpandedSpy = spyOn(instance, 'toggleSearchExpanded');
  component.setState({ searchValue: 'maggie' });
  instance.performSearch();
  expect(toggleSearchExpandedSpy).toHaveBeenCalledWith({ expanded: false });
});

test('performSearch calls clearCases', () => {
  const clearCasesSpy = createSpy();
  const component = shallowRender({
    ...defaultProps,
    clearCases: clearCasesSpy,
  });
  const instance = component.instance();
  component.setState({ searchValue: 'maggie' });
  instance.performSearch();
  expect(clearCasesSpy).toHaveBeenCalled();
});

test('performSearch calls props.performSearch with expected values', () => {
  const performSearchSpy = createSpy();
  const searchValue = 'magnolia';
  const component = shallowRender({
    ...defaultProps,
    performSearch: performSearchSpy,
  });
  const instance = component.instance();
  component.setState({ searchValue });
  instance.performSearch();
  expect(performSearchSpy).toHaveBeenCalledWith({
    per_page: SEARCH_RESULT_COUNT_PER_PAGE,
    searchOption: instance.state.selectedSearchOption.params,
    searchValue,
  });
});

test('performSearch pushes a new route to the history with correct query string', () => {
  const historyPushSpy = createSpy();
  const component = shallowRender({
    ...defaultProps,
    history: { push: historyPushSpy },
  });
  const expectedSearchParam = defaultState.selectedSearchOption.params.searchParam;
  const instance = component.instance();
  component.setState(defaultState);
  instance.performSearch();
  expect(historyPushSpy).toHaveBeenCalledWith(
    `/search?by=${expectedSearchParam}&value=${defaultState.searchValue}&active=cases`,
  );
});

/* --------------------- performSearchFromQueryParams ------------------------- */

test('performSearchFromQueryParams does not perform search if search value is empty', () => {
  const performSearchSpy = createSpy();
  const testParams = {
    by: 'vin',
    value: '',
  };
  const component = shallowRender({
    ...defaultProps,
    performSearch: performSearchSpy,
  });
  const instance = component.instance();
  instance.performSearchFromQueryParams(testParams);
  expect(performSearchSpy).toNotHaveBeenCalled();
});

test('performSearchFromQueryParams performs search with correct params', () => {
  const performSearchSpy = createSpy();
  const testParams = {
    by: 'vin',
    value: '3333',
  };
  const component = shallowRender({
    ...defaultProps,
    performSearch: performSearchSpy,
  });
  const instance = component.instance();
  instance.performSearchFromQueryParams(testParams);
  expect(performSearchSpy).toHaveBeenCalledWith({
    searchOption: {
      searchParam: testParams.by,
    },
    searchValue: testParams.value,
    per_page: SEARCH_RESULT_COUNT_PER_PAGE,
  });
});

/* --------------------- toggleDisplaySearchOptions ------------------------- */

test('toggleDisplaySearchOptions updates the state with an inverted value', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.state.displaySearchOptions).toBe(false);
  instance.toggleDisplaySearchOptions();
  expect(instance.state.displaySearchOptions).toBe(true);
  instance.toggleDisplaySearchOptions();
  expect(instance.state.displaySearchOptions).toBe(false);
});

test('toggleDisplaySearchOptions updates the state', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.state.displaySearchOptions).toBe(false);
  instance.toggleDisplaySearchOptions();
  expect(instance.state.displaySearchOptions).toBe(true);
});

/* --------------------- toggleSearchExpanded ------------------------- */

test('toggleSearchExpanded updates the state with an inverted value', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.state.expanded).toBe(false);
  instance.toggleSearchExpanded();
  expect(instance.state.expanded).toBe(true);
  instance.toggleSearchExpanded();
  expect(instance.state.expanded).toBe(false);
});

test('toggleSearchExpanded can be overridden with a provided expanded value', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.state.expanded).toBe(false);
  instance.toggleSearchExpanded();
  expect(instance.state.expanded).toBe(true);
});

test('performSearch action is called when prop function of Search is called.', () => {
  const performSearchSpy = actionAndSpy(appActions, 'performSearch');
  const component = fullRender();
  const componentProps = component.find('Search').props();
  componentProps.handleSearchValueChange({}, { newValue: 'searchValue' });
  componentProps.performSearch();
  expect(performSearchSpy).toHaveBeenCalled();
});

test('clearSearch action is called when prop function of Search is called.', () => {
  const clearCasesSpy = actionAndSpy(casesActions, 'clearCases');
  const component = fullRender();
  component.unmount();
  expect(clearCasesSpy).toHaveBeenCalled();
});
