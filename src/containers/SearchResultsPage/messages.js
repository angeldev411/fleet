import { defineMessages } from 'react-intl';

const messages = defineMessages({
  description: {
    id: 'containers.SearchResultsPage.meta.description',
    defaultMessage: 'The search results page of the application',
  },
  title: {
    id: 'containers.SearchResultsPage.meta.title',
    defaultMessage: 'The Search Results Page',
  },
  pageTitle: {
    id: 'containers.SearchResultsPage.pageTitle',
    defaultMessage: 'Search Results: {searchValue}',
  },
  pageTitleNoResult: {
    id: 'containers.SearchResultsPage.pageTitleNoResult',
    defaultMessage: 'Search Results',
  },
  tabTitleAssets: {
    id: 'containers.SearchResultsPage.tabTitle.assets',
    defaultMessage: 'Assets ({count})',
  },
  tabTitleCases: {
    id: 'containers.SearchResultsPage.tabTitle.cases',
    defaultMessage: 'Cases ({count})',
  },
});

export default messages;
