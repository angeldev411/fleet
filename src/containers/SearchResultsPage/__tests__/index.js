import { fromJS, List, Map } from 'immutable';
import { find, noop } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
  spyOn,
  actionAndSpy,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import * as appActions from 'redux/app/actions';
import * as appSelectors from 'redux/app/selectors';

import * as favorites from 'utils/favorites';

import SearchResultsPageMount, { SearchResultsPage } from '../index';
import messages from '../messages';

const defaultProps = {
  assets: List(),
  cases: List(),
  clearCases: noop,
  history: { replace: noop },
  intl: { formatMessage: ({ defaultMessage }) => defaultMessage },
  location: { search: '' },
  noSearchResult: false,
  performSearch: noop,
  requesting: false,
  searchParams: {
    searchOption: {
      searchParam: 'unit_no',
    },
    searchValue: 'my test value',
  },
  searchPagination: fromJS({
    cases: {
      latestPage: 10,
    },
    assets: {
      latestPage: 20,
    },
  }),
  loadCases: noop,
  loadAssets: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<SearchResultsPage {...props} />);
}

const fullRender = (props = defaultProps) => mount(
  <MountableTestComponent authorized>
    <SearchResultsPageMount {...props} />
  </MountableTestComponent>,
);

/* ----------------------------- componentDidMount ------------------------------------- */

test('starts search on mounting component, if there is no available cases or assets', () => {
  const searchSpy = createSpy();
  const component = shallowRender({
    ...defaultProps,
    performSearch: searchSpy,
  });
  const instance = component.instance();
  instance.componentDidMount();
  expect(searchSpy).toHaveBeenCalled(defaultProps.searchParams);
});

test('builds the content on mounting component, if there are available case or assets', () => {
  const testProps = {
    ...defaultProps,
    cases: fromJS([{ id: '3333' }]),
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  const buildContentSpy = spyOn(instance, 'buildContent');
  instance.componentDidMount();
  expect(buildContentSpy).toHaveBeenCalled(testProps);
});

/* ----------------------------- componentWillReceiveProps ------------------------------------- */

test('builds new tabs if nextProps.assets is different', () => {
  const component = shallowRender();
  const instance = component.instance();
  const buildTabsSpy = spyOn(instance, 'buildTabs').andCallThrough();
  component.setProps({ assets: fromJS([{ id: '123' }]) });
  expect(buildTabsSpy).toHaveBeenCalled();
});

test('builds new tabs if nextProps.cases is different', () => {
  const component = shallowRender();
  const instance = component.instance();
  const buildTabsSpy = spyOn(instance, 'buildTabs').andCallThrough();
  component.setProps({ cases: fromJS([{ id: '123' }]) });
  expect(buildTabsSpy).toHaveBeenCalled();
});

test('does not build new tabs if other prop is changed', () => {
  const component = shallowRender();
  const instance = component.instance();
  const buildTabsSpy = spyOn(instance, 'buildTabs').andCallThrough();
  component.setProps({ casesRequesting: true });
  expect(buildTabsSpy).toNotHaveBeenCalled();
});

/* ----------------------------- buildTabs ------------------------------------- */

test('buildTabs returns only a tab for cases if cases exist and assets do not', () => {
  const component = shallowRender();
  const instance = component.instance();
  const tabs = instance.buildTabs({
    ...instance.props,
    searchPagination: fromJS({
      cases: { totalCount: 1 },
      assets: { totalCount: 0 },
    }),
  });
  expect(tabs.length).toEqual(1);
  expect(tabs[0].name).toEqual('cases');
});

test('buildTabs returns tabs for cases and assets if both exist', () => {
  const component = shallowRender();
  const instance = component.instance();
  const tabs = instance.buildTabs({
    ...instance.props,
    searchPagination: fromJS({
      cases: { totalCount: 1 },
      assets: { totalCount: 1 },
    }),
  });
  expect(tabs.length).toEqual(2);
  expect(find(tabs, t => t.name === 'assets')).toBeTruthy();
  expect(find(tabs, t => t.name === 'cases')).toBeTruthy();
});

/* ----------------------------- handleTabChange ------------------------------------- */

test('handleTabChange replaces new url when `active` query param changed', () => {
  const newTab = 'assets';
  const queryString = '?by=unit_no&value=123&active=cases';
  const historyReplaceSpy = createSpy();
  const testProps = {
    ...defaultProps,
    history: { replace: historyReplaceSpy },
    location: { search: queryString },
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.handleTabChange(newTab);
  expect(historyReplaceSpy).toHaveBeenCalledWith(`/search?by=unit_no&value=123&active=${newTab}`);
});

/* ----------------------------- selectCurrentTab ------------------------------------- */

test('selectCurrentTab returns an empty string if no tabs are available', () => {
  const component = shallowRender();
  const instance = component.instance();
  const currentTab = instance.selectCurrentTab([], defaultProps);
  expect(currentTab).toEqual('');
});

test('selectCurrentTab sets the tab to the first available tab if no `active` query param', () => {
  const component = shallowRender();
  const instance = component.instance();
  const testProps = {
    ...instance.props,
    searchPagination: fromJS({
      assets: { totalCount: 1 },
    }),
  };
  const tabs = instance.buildTabs(testProps);
  const currentTab = instance.selectCurrentTab(tabs, testProps);
  expect(currentTab).toEqual('assets');
});

test('selectCurrentTab sets the tab to `active` query param if it is found among tabs', () => {
  const component = shallowRender();
  const instance = component.instance();
  const testProps = {
    ...instance.props,
    searchPagination: fromJS({
      assets: { totalCount: 1 },
    }),
    location: {
      search: '?active=assets',
    },
  };
  const tabs = instance.buildTabs(testProps);
  const currentTab = instance.selectCurrentTab(tabs, testProps);
  expect(currentTab).toEqual('assets');
});

test('selectCurrentTab sets the tab to the first available tab if `active` query param is not found among tabs', () => {
  const component = shallowRender();
  const instance = component.instance();
  const testProps = {
    ...instance.props,
    searchPagination: fromJS({
      assets: { totalCount: 1 },
    }),
    location: {
      search: '?active=users',
    },
  };
  const tabs = instance.buildTabs(testProps);
  const currentTab = instance.selectCurrentTab(tabs, testProps);
  expect(currentTab).toEqual('assets');
});

/* ----------------------------- refresh ------------------------------------- */

test('refresh clears the currentTab', () => {
  const component = shallowRender({
    ...defaultProps,
    searchParams: {
      searchValue: 'run',
    },
  });
  const instance = component.instance();
  instance.state.currentTab = 'cases';
  instance.refresh();
  expect(instance.state.currentTab).toEqual('');
});

test('refresh does not clear the currentTab if searchValue is empty', () => {
  const component = shallowRender({
    ...defaultProps,
    searchParams: {
      searchValue: '',
    },
  });
  const instance = component.instance();
  instance.state.currentTab = 'cases';
  instance.refresh();
  expect(instance.state.currentTab).toEqual('cases');
});

test('refresh calls `clearCases`', () => {
  const clearCases = createSpy();
  const component = shallowRender({
    ...defaultProps,
    clearCases,
    searchParams: {
      searchValue: 'run',
    },
  });
  const instance = component.instance();
  instance.refresh();
  expect(clearCases).toHaveBeenCalled();
});

test('refresh calls `performSearch` with the search params in props', () => {
  const performSearch = createSpy();
  const searchParams = {
    searchValue: 'hide',
  };
  const component = shallowRender({
    ...defaultProps,
    performSearch,
    searchParams,
  });
  const instance = component.instance();
  instance.refresh();
  expect(performSearch).toHaveBeenCalledWith(searchParams);
});

/* ----------------------------------------------------------------------------------- */

test('renders a correct PageHeadingPanel when no SearchResult is false', () => {
  const component = shallowRender();
  expect(component).toContain('PageHeadingPanel');
  const headingPanel = component.find('PageHeadingPanel');
  expect(headingPanel).toContain('PageHeadingTitle');
  const headingTitle = component.find('PageHeadingTitle');
  expect(headingTitle).toContain('FormattedMessage');
  expect(headingTitle.find('FormattedMessage')).toHaveProps({ ...messages.pageTitle });
});

test('renders a correct PageHeadingPanel when noSearchResult is true', () => {
  const testProps = {
    ...defaultProps,
    noSearchResult: true,
  };
  const component = shallowRender(testProps).find('PageHeadingTitle FormattedMessage');
  expect(component).toHaveProps({ ...messages.pageTitleNoResult });
});

test('renders a SearchResultsPanel with the expected props', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(component).toContain('SearchResultsPanel');
  const panel = component.find('SearchResultsPanel');
  expect(panel).toHaveProps({
    assets: defaultProps.assets,
    cases: defaultProps.cases,
    currentTab: instance.state.currentTab,
    handleTabChange: instance.handleTabChange,
    refresh: instance.refresh,
    requesting: false,
    searchValue: defaultProps.searchParams.searchValue,
    tabs: instance.props.tabs,
    requestNextCases: instance.requestNextCases,
    requestNextAssets: instance.requestNextAssets,
    searchPagination: defaultProps.searchPagination,
  });
});

test('performSearch is called the first render time.', () => {
  const performSearchSpy = actionAndSpy(appActions, 'performSearch');
  const searchParams = {
    searchValue: 'searchText',
  };
  appSelectors.searchParamsSelector = () => searchParams;
  fullRender();
  expect(performSearchSpy).toHaveBeenCalledWith(searchParams);
});

test('requestNextCases calls loadFavorite with proper arguments.', () => {
  const loadCases = () => {};
  const favoritePagination = Map({
    latestPage: 10,
  });
  const component = shallowRender({
    ...defaultProps,
    searchParams: {
      searchOption: {
        searchParam: 'search_param',
      },
      searchValue: 'search_value',
    },
    loadCases,
  });
  const spy = spyOn(favorites, 'loadFavorite');
  component.instance().requestNextCases(favoritePagination);
  expect(spy).toHaveBeenCalledWith({
    page: 11,
    favorite: {
      params: {
        search_param: 'search_value',
      },
    },
    loadAction: loadCases,
  });
});

test('requestNextAssets calls loadFavorite with proper arguments.', () => {
  const loadAssets = () => {};
  const favoritePagination = Map({
    latestPage: 20,
  });
  const component = shallowRender({
    ...defaultProps,
    searchParams: {
      searchOption: {
        searchParam: 'search_param',
      },
      searchValue: 'search_value',
    },
    loadAssets,
  });
  const spy = spyOn(favorites, 'loadFavorite');
  component.instance().requestNextAssets(favoritePagination);
  expect(spy).toHaveBeenCalledWith({
    page: 21,
    favorite: {
      params: {
        search_param: 'search_value',
      },
    },
    loadAction: loadAssets,
  });
});
