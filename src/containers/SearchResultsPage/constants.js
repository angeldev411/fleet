import messages from './messages';

// eslint-disable-next-line import/prefer-default-export
export const TAB_LABELS = [
  { name: 'cases', message: messages.tabTitleCases },
  { name: 'assets', message: messages.tabTitleAssets },
];
