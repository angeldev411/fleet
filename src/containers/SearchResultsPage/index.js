import { find } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Helmet from 'react-helmet';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { injectIntl, FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';

import { parseQueryParams } from 'utils/url';

import SearchResultsPanel from 'components/SearchResultsPanel';

import Divider from 'elements/Divider';
import Page from 'elements/Page';
import PageHeadingPanel from 'elements/PageHeadingPanel';
import PageHeadingTitle from 'elements/PageHeadingTitle';

import { performSearch } from 'redux/app/actions';
import {
  searchParamsSelector,
  searchPaginationSelector,
} from 'redux/app/selectors';

import { loadAssets } from 'redux/assets/actions';
import { assetsSelector } from 'redux/assets/selectors';

import { loadCases, clearCases } from 'redux/cases/actions';
import { casesSelector } from 'redux/cases/selectors';

import {
  requestingSelector,
  noAssetsCasesFoundSelector,
} from 'redux/combinedSelectors';

import {
  loadFavorite,
} from 'utils/favorites';

import { TAB_LABELS } from './constants';
import messages from './messages';

export class SearchResultsPage extends Component {
  static propTypes = {
    assets: ImmutablePropTypes.listOf(
      ImmutablePropTypes.mapContains({
        id: PropTypes.string,
      }),
    ).isRequired,
    cases: ImmutablePropTypes.listOf(
      ImmutablePropTypes.mapContains({
        id: PropTypes.string,
      }),
    ).isRequired,

    // clearCases is used to remove all cases, assets, and other data
    // (including favorites pagination data) currently held in state.
    clearCases: PropTypes.func.isRequired,

    history: PropTypes.shape({
      replace: PropTypes.func.isRequired,
    }).isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,
    noSearchResult: PropTypes.bool,
    performSearch: PropTypes.func.isRequired,
    requesting: PropTypes.bool,
    searchParams: PropTypes.shape({
      searchOption: PropTypes.shape({
        searchParam: PropTypes.string,
      }),
      searchValue: PropTypes.string,
    }).isRequired,
    tabLabels: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string.isRequired,
        message: PropTypes.shape({
          id: PropTypes.string.isRequired,
          defaultMessage: PropTypes.string.isRequired,
        }).isRequired,
      }).isRequired,
    ).isRequired,
    searchPagination: ImmutablePropTypes.mapContains({
      cases: ImmutablePropTypes.map.isRequired,
      assets: ImmutablePropTypes.map.isRequired,
    }).isRequired,
    loadCases: PropTypes.func.isRequired,
    loadAssets: PropTypes.func.isRequired,
  }

  static defaultProps = {
    noSearchResult: false,
    requesting: false,
    tabLabels: TAB_LABELS,
  }

  state = {
    currentTab: '',
    tabs: [],
  }

  componentDidMount() {
    if (this.props.cases.size || this.props.assets.size) {
      // The cases/assets are already loaded, so just show them
      this.buildContent(this.props);
    } else {
      this.props.performSearch(this.props.searchParams);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.assets !== this.props.assets || nextProps.cases !== this.props.cases) {
      this.buildContent(nextProps);
    }
  }

  buildContent = (props) => {
    const tabs = this.buildTabs(props);
    const currentTab = this.selectCurrentTab(tabs, props);
    this.setState({ currentTab, tabs });
  }

  buildTabs = (nextProps) => {
    const {
      tabLabels,
      searchPagination,
    } = nextProps;

    const filtered = searchPagination
      .filter(tabData => Number(tabData.get('totalCount')) > 0);

    const processed = filtered.map((tabData, tabName) => {
      const label = find(tabLabels, l => l.name === tabName);
      return ({
        name: tabName,
        label: (
          <FormattedMessage {...label.message} values={{ count: tabData.get('totalCount') }} />
        ),
      });
    });

    return processed.toList().toJS();
  }


  handleTabChange = (tabName) => {
    const { location: { search }, history } = this.props;
    const params = parseQueryParams(search);
    history.replace(`/search?by=${params.by}&value=${params.value}&active=${tabName}`);
  }

  selectCurrentTab = (tabs, props) => {
    const { location } = props;
    const params = location.search && parseQueryParams(location.search);
    if (tabs && tabs.length) {
      if (params && params.active) {
        if (tabs.some(tab => tab.name === params.active)) {
          return params.active;
        }
      }
      return tabs[0].name; // Tab does not include a tab named as "active" query param
    }
    return ''; // No tabs are avaialble
  }

  refresh = () => {
    if (this.props.searchParams.searchValue) {
      this.setState({ currentTab: '' });
      this.props.clearCases();
      this.props.performSearch(this.props.searchParams);
    }
  }

  requestNextData = (favoritePagination, loadAction) => {
    const {
      searchParams: {
        searchOption,
        searchValue,
      },
    } = this.props;
    const latestPage = parseInt(favoritePagination.get('latestPage'), 10);
    const nextPage = latestPage + 1;

    loadFavorite({
      page: nextPage,
      favorite: {
        params: {
          [searchOption.searchParam]: searchValue,
        },
      },
      loadAction,
    });
  }

  requestNextCases = (favoritePagination) => {
    this.requestNextData(favoritePagination, this.props.loadCases);
  }

  requestNextAssets = (favoritePagination) => {
    this.requestNextData(favoritePagination, this.props.loadAssets);
  }

  render() {
    const {
      assets,
      cases,
      intl: {
        formatMessage,
      },
      noSearchResult,
      requesting,
      searchParams: {
        searchValue,
      },
      searchPagination,
    } = this.props;

    return (
      <Page name="search-results-page" >
        <Helmet
          title={formatMessage(messages.title)}
          meta={[
            { name: 'description', content: formatMessage(messages.description) },
          ]}
        />
        <PageHeadingPanel>
          <PageHeadingTitle>
            {noSearchResult ?
              <FormattedMessage {...messages.pageTitleNoResult} />
              :
              <FormattedMessage {...messages.pageTitle} values={{ searchValue }} />
            }
          </PageHeadingTitle>
          <Divider />
        </PageHeadingPanel>
        <SearchResultsPanel
          assets={assets}
          cases={cases}
          currentTab={this.state.currentTab}
          handleTabChange={this.handleTabChange}
          refresh={this.refresh}
          requesting={requesting}
          searchValue={searchValue}
          tabs={this.state.tabs}
          requestNextCases={this.requestNextCases}
          requestNextAssets={this.requestNextAssets}
          searchPagination={searchPagination}
        />
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return {
    assets: assetsSelector(state),
    cases: casesSelector(state),
    requesting: requestingSelector(state),
    noSearchResult: noAssetsCasesFoundSelector(state),
    searchParams: searchParamsSelector(state),
    searchPagination: searchPaginationSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    performSearch: params => dispatch(performSearch(params)),
    loadCases: filter => dispatch(loadCases(filter)),
    clearCases: () => dispatch(clearCases()),
    loadAssets: filter => dispatch(loadAssets(filter)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(SearchResultsPage));
