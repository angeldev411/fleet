import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.chrome400};
  cursor: pointer;
  font-size: ${px2rem(12)};
  margin-right: ${px2rem(5)};
  text-align: center;
  width: ${px2rem(12)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome400: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'ExpandIconWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
