import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const modifierConfig = {
  clickable: () => ({ styles: 'cursor: pointer;' }),
};

/* istanbul ignore next */
const styles = () => `
  align-items: center;
  display: flex;
  .expand-icon {
    transition: transform 0.3s;
    &.expanded {
      transform: rotate(90deg);
      visibility: hidden;
    }
  }
  &:hover {
    .expanded {
      visibility: visible;
    }
  }
`;

export default buildStyledComponent(
  'HeaderWrapper',
  styled.div,
  styles,
  { modifierConfig },
);
