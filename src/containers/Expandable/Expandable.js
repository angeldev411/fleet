import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SmoothCollapse from 'react-smooth-collapse';
import FontAwesome from 'react-fontawesome';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Map } from 'immutable';
import { connect } from 'react-redux';
import { compact, noop } from 'lodash';
import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';

import { componentsExpandedSelector } from 'redux/ui/selectors';
import {
  setComponentExpanded as setComponentExpandedAction,
} from 'redux/ui/actions';

import Popover, { PopoverTarget, PopoverContent, MessagePopover } from 'elements/Popover';

import Wrapper from './Wrapper';
import HeaderWrapper from './HeaderWrapper';
import OverflowWrapper from './OverflowWrapper';
import ExpandIconWrapper from './ExpandIconWrapper';

function mapStateToProps(state) {
  return {
    componentsExpanded: componentsExpandedSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setComponentExpanded: (expandKey, expanded) =>
      dispatch(setComponentExpandedAction({ expandKey, expanded })),
  };
}

/*
  This is a general expand/collapse component to be applied to both
  entire widget and individual collapsible component like note (from Notes widget)
  and fault (from Diagnostics widget)
  Props:
    expandKey: If it is set, the status of component (expanded/collapsed)
      is stored in local storage and loaded when the page is refreshed.
      It is important not to use same name (you can pass this prop like sending id)
      because this name is directly used as a name to be stored in local storage
      If it is not set, expand/collapse status is managed locally in the component
      and page refresh doesn't keep the previous status (expanded originally).
    expandOnHeaderClick: If it is set to true, header is clickable and do expand/collapse
    onExpand: called when component is expanded or collapsed.
      It is used only when the component's status is controlled by outside.
*/
export class Expandable extends Component {
  static propTypes = {
    isExpanded: PropTypes.bool,
    expandKey: PropTypes.string,
    children: PropTypes.node,
    setComponentExpanded: PropTypes.func.isRequired,
    componentsExpanded: ImmutablePropTypes.map.isRequired,
    expandOnHeaderClick: PropTypes.bool,
    onExpand: PropTypes.func,
    hasPopover: PropTypes.bool,
    popoverMessages: PropTypes.objectOf(
      PropTypes.shape(messageDescriptorPropTypes),
    ),
    showHeaderWhenExpanded: PropTypes.bool,
    hasExpandIcon: PropTypes.bool,
  };

  static defaultProps = {
    isExpanded: undefined,
    expandKey: '',
    children: null,
    expandOnHeaderClick: false,
    setComponentExpanded: noop,
    componentsExpanded: Map(),
    onExpand: noop,
    hasPopover: false,
    popoverMessages: {
      expand: {
        id: '',
        defaultMessage: '',
      },
      collapse: {
        id: '',
        defaultMessage: '',
      },
    },
    showHeaderWhenExpanded: true,
    hasExpandIcon: true,
  };

  state = { isExpanded: true, contentExpanded: true };

  componentDidMount = () => {
    this.collapseStatusChanged();
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.isExpanded !== this.props.isExpanded) {
      this.expandFromProps(nextProps.isExpanded);
    }
  }

  getHeader = () => {
    const { children } = this.props;
    const targetComponent = React.Children
      .toArray(children)
      .filter(c => c.type.displayName === 'ExpandableHeader')[0];
    return targetComponent && targetComponent.props.children;
  };

  getContent = () => {
    const { children } = this.props;
    const contentComponent = React.Children
      .toArray(children)
      .filter(c => c.type.displayName === 'ExpandableContent')[0];
    return contentComponent && contentComponent.props.children;
  };

  getExpanded = () => {
    let { isExpanded } = this.state;
    const { expandKey, componentsExpanded } = this.props;
    if (expandKey) {
      if (componentsExpanded.get(expandKey) !== undefined) {
        isExpanded = componentsExpanded.get(expandKey);
      }
    }
    return isExpanded;
  }

  expand = () => {
    const { isExpanded } = this.state;
    const { expandKey, componentsExpanded, setComponentExpanded, onExpand } = this.props;
    if (expandKey) {
      if (componentsExpanded.get(expandKey) === undefined) {
        onExpand(false);
        return setComponentExpanded(expandKey, false);
      }
      onExpand(!componentsExpanded.get(expandKey));
      return setComponentExpanded(expandKey, !componentsExpanded.get(expandKey));
    }
    onExpand(!isExpanded);
    return this.setState({ isExpanded: !isExpanded });
  }

  expandFromProps = (isExpanded) => {
    const { expandKey, setComponentExpanded } = this.props;
    if (expandKey) {
      return setComponentExpanded(expandKey, isExpanded);
    }
    return this.setState({ isExpanded });
  }

  collapseStatusChanged = () => {
    this.setState({ contentExpanded: this.getExpanded() });
  }

  buildExpandIcon = () => {
    const { expandOnHeaderClick, hasPopover, popoverMessages } = this.props;
    const isExpanded = this.getExpanded();
    const target = (
      <ExpandIconWrapper
        className={`${isExpanded ? 'expanded' : ''} expand-icon`}
        onClick={expandOnHeaderClick ? noop : this.expand}
      >
        <FontAwesome name={'chevron-right'} />
      </ExpandIconWrapper>
    );
    if (hasPopover) {
      const content = (
        <MessagePopover>
          <FormattedMessage {...popoverMessages[isExpanded ? 'collapse' : 'expand']} />
        </MessagePopover>
      );
      return (
        <Popover
          showOnHover
          position="right"
          noOffset
        >
          <PopoverTarget>
            {target}
          </PopoverTarget>
          <PopoverContent>
            {content}
          </PopoverContent>
        </Popover>
      );
    }
    return target;
  }

  render() {
    const { expandOnHeaderClick, showHeaderWhenExpanded, hasExpandIcon } = this.props;
    const { contentExpanded } = this.state;
    const isExpanded = this.getExpanded();
    const clickable = expandOnHeaderClick && 'clickable';
    return (
      <Wrapper>
        {(!isExpanded || showHeaderWhenExpanded) && <HeaderWrapper
          modifiers={compact([clickable])}
          onClick={expandOnHeaderClick ? this.expand : noop}
        >
          {hasExpandIcon && this.buildExpandIcon()}
          {this.getHeader()}
        </HeaderWrapper>}
        <OverflowWrapper isExpanded={isExpanded ? contentExpanded : false}>
          <SmoothCollapse expanded={isExpanded} onChangeEnd={this.collapseStatusChanged}>
            {this.getContent()}
          </SmoothCollapse>
        </OverflowWrapper>
      </Wrapper>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Expandable);
