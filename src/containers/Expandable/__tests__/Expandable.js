import React from 'react';
import { Map } from 'immutable';
import { noop } from 'lodash';

import { test, expect, shallow, createSpy } from '__tests__/helpers/test-setup';
import { ExpandableHeader, ExpandableContent } from '../index';
import { Expandable } from '../Expandable';

const header = <span id="header">Header Thing</span>;
const content1 = <span id="content1">I am the first content</span>;
const content2 = <span id="content2">Second content is here</span>;
const componentsExpanded = Map();
const setComponentExpanded = noop;

const popoverTestProps = {
  hasPopover: true,
  popoverMessages: {
    expand: {
      id: 'test expand id',
      defaultMessage: 'test expand message',
    },
    collapse: {
      id: 'test collapse id',
      defaultMessage: 'test collapse message',
    },
  },
};

const renderComponent = (props = {}) => shallow(
  <Expandable
    {...props}
    componentsExpanded={componentsExpanded}
    setComponentExpanded={setComponentExpanded}
  >
    <ExpandableHeader>
      {header}
    </ExpandableHeader>
    <ExpandableContent>
      {content1}
      {content2}
    </ExpandableContent>
  </Expandable>,
);

// -------------------- ExpandableContent --------------------

test('Expandable renders as a Wrapper', () => {
  expect(renderComponent()).toBeA('Wrapper');
});

test('Expandable contains HeaderWrapper', () => {
  expect(renderComponent()).toContain('HeaderWrapper');
});

test('HeaderWrapper is not shown when expanded & showHeaderWhenExpanded is false', () => {
  const component = renderComponent({ showHeaderWhenExpanded: false, isExpanded: true });
  expect(component).toNotContain('HeaderWrapper');
});

test('Expandable contains ExpandIconWrapper', () => {
  expect(renderComponent()).toContain('ExpandIconWrapper');
});

test('Expandable does not contain ExpandIconWrapper when hasExpandIcon prop is set to false', () => {
  expect(renderComponent({ hasExpandIcon: false })).toNotContain('ExpandIconWrapper');
});

test('Expandable contains OverflowWrapper', () => {
  expect(renderComponent()).toContain('OverflowWrapper');
});

test('Expandable contains SmoothCollapse', () => {
  expect(renderComponent()).toContain('SmoothCollapse');
});

test('Expandable contains Header', () => {
  const component = renderComponent().find('HeaderWrapper');
  expect(component).toContain('span#header');
});

test('Expandable contains Contents', () => {
  const component = renderComponent().find('OverflowWrapper');
  expect(component).toContain('span#content1');
  expect(component).toContain('span#content2');
});

test('state isExpanded is true by default', () => {
  const component = renderComponent();
  expect(component.instance().getExpanded()).toBe(true);
});

test('Expandable contains FontAwesome with right props', () => {
  const component = renderComponent();
  expect(component.find('FontAwesome')).toHaveProps({ name: 'chevron-right' });
});

test('ExpandIconWrapper classname should include expand-icon and expanded when component is expanded', () => {
  const component = renderComponent();
  expect(component.find('ExpandIconWrapper.expand-icon.expanded').length).toEqual(1);
  component.instance().expand();
  expect(component.find('ExpandIconWrapper.expand-icon.expanded').length).toEqual(0);
});

test('Expandable contains OverflowWrapper with right props', () => {
  const component = renderComponent();
  expect(component.find('OverflowWrapper')).toHaveProps(
    { isExpanded: component.state().contentExpanded },
  );
  component.instance().expand();
  expect(component.find('OverflowWrapper')).toHaveProps({ isExpanded: false });
});

test('Expandable contains SmoothCollapse with right props', () => {
  const component = renderComponent();
  expect(component.find('SmoothCollapse')).toHaveProps({ expanded: true });
  component.instance().expand();
  expect(component.find('SmoothCollapse')).toHaveProps({ expanded: false });
});

test('Get Expandable status from the props if expandKey is not null', () => {
  const component = shallow(
    <Expandable
      componentsExpanded={Map({ test: true })}
      expandKey="test"
      setComponentExpanded={noop}
    />,
  );
  component.setState({ isExpanded: false });
  expect(component.instance().getExpanded()).toBe(true);
});

test('Render HeaderWrapper with right props when expandOnHeaderClick is set true', () => {
  const component = shallow(
    <Expandable expandOnHeaderClick />,
  );
  const headerWrapper = component.find('HeaderWrapper');
  expect(headerWrapper).toHaveProps({
    onClick: component.instance().expand,
  });
  expect(headerWrapper.props().modifiers).toContain('clickable');
});

test('Render ExpandIconWrapper with right props when expandOnHeaderClick is set false', () => {
  const component = shallow(
    <Expandable expandOnHeaderClick={false} />,
  );
  const expandIconWrapper = component.find('ExpandIconWrapper');
  expect(expandIconWrapper).toHaveProp('onClick', component.instance().expand);
});


// --------------------------- expand -------------------------
test('expand() changes state isExpanded with right param when expandKey is not set', () => {
  const setExpanded = createSpy();
  const component = shallow(
    <Expandable
      componentsExpanded={Map()}
      setComponentExpanded={setExpanded}
    />,
  );
  component.setState({ isExpanded: false });
  component.instance().expand();
  expect(component.state().isExpanded).toBe(true);
});

test('expand() runs setComponentExpanded with param false when expandKey is not null but componentsExpanded does not have that status', () => {
  const setExpanded = createSpy();
  const component = shallow(
    <Expandable
      componentsExpanded={Map()}
      expandKey="test"
      setComponentExpanded={setExpanded}
    />,
  );
  component.instance().expand();
  expect(setExpanded).toHaveBeenCalledWith('test', false);
});

test('expand() runs setComponentExpanded with proper param when expandKey is not null and componentsExpanded has that status', () => {
  const setExpanded = createSpy();
  const component = shallow(
    <Expandable
      componentsExpanded={Map({ test: false })}
      expandKey="test"
      setComponentExpanded={setExpanded}
    />,
  );
  component.instance().expand();
  expect(setExpanded).toHaveBeenCalledWith('test', true);
});

test('expand() changes runs onExpand with right param when expandKey is not set', () => {
  const onExpand = createSpy();
  const component = shallow(
    <Expandable
      componentsExpanded={Map()}
      onExpand={onExpand}
    />,
  );
  component.setState({ isExpanded: true });
  component.instance().expand();
  expect(onExpand).toHaveBeenCalledWith(false);
});

test('expand() runs onExpand with param false when expandKey is not null but componentsExpanded does not have that status', () => {
  const onExpand = createSpy();
  const component = shallow(
    <Expandable
      componentsExpanded={Map()}
      expandKey="test"
      onExpand={onExpand}
    />,
  );
  component.instance().expand();
  expect(onExpand).toHaveBeenCalledWith(false);
});

test('expand() runs onExpand with proper param when expandKey is not null and componentsExpanded has that status', () => {
  const onExpand = createSpy();
  const component = shallow(
    <Expandable
      componentsExpanded={Map({ test: false })}
      expandKey="test"
      onExpand={onExpand}
    />,
  );
  component.instance().expand();
  expect(onExpand).toHaveBeenCalledWith(true);
});

// -------------------------- expandFromProps -------------------------
test('expandFromProps() changes the state when expandKey is set', () => {
  const setExpanded = createSpy();
  const component = shallow(
    <Expandable
      componentsExpanded={Map()}
      setComponentExpanded={setExpanded}
    />,
  );
  component.setState({ isExpanded: false });
  component.instance().expandFromProps(true);
  expect(component.state().isExpanded).toBe(true);
});

test('expandFromProps() changes the state when expandKey is not set', () => {
  const setExpanded = createSpy();
  const component = shallow(
    <Expandable
      componentsExpanded={Map()}
      setComponentExpanded={setExpanded}
    />,
  );
  component.setState({ isExpanded: true });
  component.instance().expandFromProps(false);
  expect(component.state().isExpanded).toBe(false);
});

test('expandFromProps() is called and changes the state when new props is received.', () => {
  const setExpanded = createSpy();
  const component = shallow(
    <Expandable
      componentsExpanded={Map()}
      expandKey="test"
      setComponentExpanded={setExpanded}
      isExpanded
    />,
  );
  component.setProps({ isExpanded: false });
  expect(setExpanded).toHaveBeenCalledWith('test', false);
});

test('Renders Popover components if hasPopover is true', () => {
  const component = renderComponent(popoverTestProps);
  expect(component).toContain('PopoverTarget');
  expect(component).toContain('PopoverContent');
  expect(component).toContain('MessagePopover');
});
