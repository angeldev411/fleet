import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import ExpandableHeader from '../ExpandableHeader';

// -------------------- ExpandableHeader --------------------

test('ExpandableHeader renders null', () => {
  expect(shallow(<ExpandableHeader />).type()).toBe(null);
});
