import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import OverflowWrapper, { expandedStyle } from '../OverflowWrapper';

const renderComponent = (expanded = true) => shallow(<OverflowWrapper isExpanded={expanded} />);

test('Renders OverflowWrapper as a div', () => {
  expect(renderComponent()).toBeA('div');
});

// ------------------------- expandedStyle --------------------
test('expandedStyle returns overflow to be visible', () => {
  expect(expandedStyle({ isExpanded: true })).toContain('overflow: visible');
});

test('isExpanded returns overflow not to be visible', () => {
  expect(expandedStyle({ isExpanded: false })).toBe('');
});
