import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import Wrapper from '../Wrapper';

const renderComponent = () => shallow(<Wrapper />);

test('Renders Wrapper as a div', () => {
  expect(renderComponent()).toBeA('div');
});
