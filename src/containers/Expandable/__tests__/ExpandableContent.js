import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import ExpandableContent from '../ExpandableContent';

// -------------------- ExpandableContent --------------------

test('ExpandableContent renders null', () => {
  expect(shallow(<ExpandableContent />).type()).toBe(null);
});
