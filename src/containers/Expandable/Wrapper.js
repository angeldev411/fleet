import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  display: block;
  position: relative;
`;

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
);
