import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

export function expandedStyle({ isExpanded }) {
  if (isExpanded) {
    return 'overflow: visible !important;';
  }
  return '';
}

/* istanbul ignore next */
const styles = props => `
  padding-left: ${px2rem(4)};
  & > div,
  & > div > div {
    ${expandedStyle(props)}
    display: block;
  }
`;

const propTypes = {
  isExpanded: PropTypes.bool,
};

const defaultProps = {
  isExpanded: true,
};

export default buildStyledComponent(
  'OverflowWrapper',
  styled.div,
  styles,
  { defaultProps, propTypes },
);
