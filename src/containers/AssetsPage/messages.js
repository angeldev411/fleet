import { defineMessages } from 'react-intl';

const messages = defineMessages({
  defaultHeader: {
    id: 'containers.AssetsPage.defaultHeader',
    defaultMessage: 'Assets',
  },
  description: {
    id: 'containers.AssetsPage.meta.description',
    defaultMessage: 'The assets page of the application',
  },
  title: {
    id: 'containers.AssetsPage.meta.title',
    defaultMessage: 'The Assets Page',
  },
});

export default messages;
