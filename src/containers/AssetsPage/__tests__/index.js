import React from 'react';
import { fromJS, Map } from 'immutable';
import { noop } from 'lodash';

import configureStore from 'setup/store';
import {
  test,
  expect,
  createSpy,
  spyOn,
  mount,
  shallow,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import Routes from 'setup/routes';
import * as favoritesUtils from 'utils/favorites';

import { AssetsPage } from '../index';

const asset1 = {
  id: '12345',
  countActiveCases: '1',
  hasActiveCases: true,
};
const asset2 = {
  id: '67890',
  countActiveCases: '1',
  hasActiveCases: false,
};

const assets = [asset1, asset2];

const defaultProps = {
  assets: fromJS(assets),
  assetsView: '',
  favoritePagination: Map(),
  intl: {
    formatMessage: noop,
  },
  match: {
    params: {},
  },
  loadAssets: noop,
  clearAssets: noop,
  setAssets: noop,
  setAssetsView: noop,
  startLoading: noop,
  isValidSlug: true,
};

const favorite1 = {
  slug: 'high-severity',
  title: 'highSeverity',
  path: '/assets/filter/high-severity',
  message: {
    id: 'high-severity',
    defaultMessage: 'High Severity',
  },
};

const favorite2 = {
  slug: 'maintenance-current',
  title: 'maintenanceCurrent',
  path: '/assets/filter/maintenance-current',
  message: {
    id: 'maintenance-current',
    defaultMessage: 'Maintenance Current',
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetsPage {...props} />);
}

function mountPage(initialEntries, store = {}) {
  const authStore = configureStore(fromJS({ ...store, user: { isAuthorized: true } }));
  return mount(
    <MountableTestComponent initialEntries={initialEntries} store={authStore}>
      <Routes />
    </MountableTestComponent>,
  );
}

function mountPageWithProps(props = defaultProps) {
  return mount(
    <MountableTestComponent authorized>
      <AssetsPage {...props} />
    </MountableTestComponent>,
  );
}

test('Visiting the asset route renders the correct component', () => {
  const assetsRoute = '/assets/filter/slug1';
  const page = mountPage([assetsRoute]);
  expect(page.find('AssetsPage').exists()).toBe(true);
});

test('Visiting a filter route displays the correct page title', () => {
  const assetFavorites = [
    favorite1,
    favorite2,
  ];
  const store = {
    app: {
      dashboard: {
        favorites: {
          assets: assetFavorites,
        },
      },
      favoritePagination: Map(),
      priorFavorite: {},
      latestFavorite: {},
    },
  };

  assetFavorites.forEach(({ slug, message: { defaultMessage } }) => {
    const filteredRoute = `/assets/filter/${slug}`;
    const page = mountPage([filteredRoute], store);
    const pageHeading = page.find('PageHeadingPanel');
    expect(pageHeading.text()).toInclude(defaultMessage);
  });
});

test('Page renders SelectorBar with correct props', () => {
  const page = shallowRender();
  expect(page).toContain('SelectorBar');
  expect(page.find('SelectorBar')).toHaveProps({
    view: defaultProps.assetsView,
    setView: defaultProps.setAssetsView,
  });
});

test('Page renders AssetsPanel', () => {
  const page = shallowRender();
  expect(page).toContain('AssetsPanel');
});

/* ---------------------- clearing and loading favorites behavior --------------------------*/

test(
  'Visiting Assets page with hard coded favorite calls clearAndLoadFavorites with expected args',
  () => {
    const spy = spyOn(favoritesUtils, 'clearAndLoadFavorites');
    mountPageWithProps({ ...defaultProps, latestFavorite: favorite1 });
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: {},
      latestFavorite: favorite1,
      forceClear: false,
      clearAction: defaultProps.clearAssets,
      loadAction: defaultProps.loadAssets,
    });
  },
);

test(
  'Transitioning from one favorite to another calls clearAndLoadFavorites with expected args',
  () => {
    const spy = spyOn(favoritesUtils, 'clearAndLoadFavorites');
    mountPageWithProps({
      ...defaultProps,
      priorFavorite: favorite1,
      latestFavorite: favorite2,
    });
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: favorite1,
      latestFavorite: favorite2,
      forceClear: false,
      clearAction: defaultProps.clearAssets,
      loadAction: defaultProps.loadAssets,
    });
  },
);

test(
  'Visiting assets page triggers the global loading',
  () => {
    const startLoadingSpy = createSpy();
    const testProps = {
      ...defaultProps,
      latestFavorite: favorite1,
      startLoading: startLoadingSpy,
    };
    mountPageWithProps(testProps);
    expect(startLoadingSpy).toHaveBeenCalled();
  },
);

test(
  'Transitioning from one favorite to another triggers the global loading',
  () => {
    const startLoadingSpy = createSpy();
    const testProps = {
      ...defaultProps,
      priorFavorite: favorite1,
      latestFavorite: favorite2,
      startLoading: startLoadingSpy,
    };
    mountPageWithProps(testProps);
    expect(startLoadingSpy).toHaveBeenCalled();
  },
);

test(
  'Visiting page calls clearAndLoadFavorites with no favorites data if none in props',
  () => {
    const spy = spyOn(favoritesUtils, 'clearAndLoadFavorites');
    mountPageWithProps({
      ...defaultProps,
    });
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: {},
      latestFavorite: {},
      forceClear: false,
      clearAction: defaultProps.clearAssets,
      loadAction: defaultProps.loadAssets,
    });
  },
);

test(
  'Visiting page should call loadAssetFavorite after receiving new favorite prop.',
  () => {
    const component = shallowRender({
      ...defaultProps,
    });
    const instance = component.instance();
    const spy = createSpy();
    instance.loadAssetFavorite = spy;
    component.setProps({ latestFavorite: favorite1 });
    component.update();
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: {},
      latestFavorite: favorite1,
    });
  },
);

test(
  'Visiting page with same prior favorite and latestFavorite does not call clearAndLoadFavorites',
  () => {
    const component = shallowRender({
      ...defaultProps,
      priorFavorite: {},
      latestFavorite: favorite1,
    });
    const instance = component.instance();
    const spy = createSpy();
    instance.loadAssetFavorite = spy;
    component.setProps({ priorFavorite: favorite1, latestFavorite: favorite1 });
    component.update();
    expect(spy).toNotHaveBeenCalled();
  },
);

test(
  'Refresh calls loadAssetFavorite with forceClear = true',
  () => {
    const component = shallowRender({
      ...defaultProps,
      latestFavorite: favorite1,
    });
    const instance = component.instance();
    const spy = createSpy();
    instance.loadAssetFavorite = spy;
    instance.refreshAssets();
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: {},
      latestFavorite: favorite1,
      forceClear: true,
    });
  },
);

test(
  'Refresh does not trigger the global loading',
  () => {
    const startLoadingSpy = createSpy();
    const testProps = {
      ...defaultProps,
      latestFavorite: favorite1,
      startLoading: startLoadingSpy,
    };
    const component = shallowRender(testProps);
    const instance = component.instance();
    instance.refreshAssets();
    expect(startLoadingSpy).toNotHaveBeenCalled();
  },
);

/* -------------------------------------- requestNext ----------------------------------- */
test('requestNext triggers loadAssets', () => {
  const loadAssets = createSpy();
  const component = shallowRender({ ...defaultProps, loadAssets });
  const instance = component.instance();
  instance.requestNext(defaultProps.favoritePagination);
  expect(loadAssets).toHaveBeenCalled();
});

test('renders "NotFoundPage" if the slug is invalid', () => {
  const testProps = {
    ...defaultProps,
    isValidSlug: false,
  };
  const component = shallowRender(testProps);
  expect(component).toContain('NotFoundPage');
});
