import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage } from 'react-intl';

import AssetsPanel from 'components/AssetsPanel';
import SelectorBar from 'components/SelectorBar';
import NotFoundPage from 'components/NotFoundPage';

import Divider from 'elements/Divider';
import Page from 'elements/Page';
import PageHeadingPanel from 'elements/PageHeadingPanel';
import PageHeadingTitle from 'elements/PageHeadingTitle';

import {
  setLoadingStarted,
} from 'redux/app/actions';
import {
  favoritePaginationSelector,
  priorFavoriteSelector,
  latestFavoriteSelector,
} from 'redux/app/selectors';

import {
  loadAssets,
  clearAssets,
} from 'redux/assets/actions';
import {
  assetsSelector,
  assetsRequestingSelector,
} from 'redux/assets/selectors';

import { setAssetsView } from 'redux/ui/actions';
import { assetsViewSelector } from 'redux/ui/selectors';

import {
  shouldClearAndLoadFavorites,
  clearAndLoadFavorites,
  loadFavorite,
} from 'utils/favorites';

import messages from './messages';

export class AssetsPage extends Component {
  static propTypes = {
    // All application data kept in state is immutable. To properly validate these,
    // use ImmutablePropTypes.
    assets: ImmutablePropTypes.listOf(
      ImmutablePropTypes.mapContains({
        id: PropTypes.string,
      }),
    ).isRequired,
    assetsRequesting: PropTypes.bool,
    assetsView: PropTypes.string,
    clearAssets: PropTypes.func.isRequired,
    favoritePagination: ImmutablePropTypes.map.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    latestFavorite: PropTypes.shape({
      message: PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired,
      }),
      path: PropTypes.string,
      slug: PropTypes.string,
      title: PropTypes.string,
    }),
    loadAssets: PropTypes.func.isRequired,
    priorFavorite: PropTypes.shape({
      message: PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired,
      }),
      path: PropTypes.string,
      slug: PropTypes.string,
      title: PropTypes.string,
    }),
    setAssetsView: PropTypes.func.isRequired,
    startLoading: PropTypes.func.isRequired,
    isValidSlug: PropTypes.bool,
  };

  static defaultProps = {
    assetsRequesting: false,
    assetsView: '',
    latestFavorite: {},
    priorFavorite: {},
    isValidSlug: true,
  };

  componentDidMount() {
    // if the slug is invalid, we don't need to dispatch the following redux actions
    // to fetch the assets data from the server
    if (!this.props.isValidSlug) {
      return;
    }

    this.loadingOnMount = true;
    this.props.startLoading();

    this.loadAssetFavorite({
      priorFavorite: this.props.priorFavorite,
      latestFavorite: this.props.latestFavorite,
    });
  }

  componentWillReceiveProps(nextProps) {
    // if the slug is invalid, we don't need to dispatch the following redux actions
    // to fetch the assets data from the server
    if (!nextProps.isValidSlug) {
      return;
    }

    // if loading has finished, set loadingOnMount to false
    if (!nextProps.assetsRequesting && this.props.assetsRequesting) {
      this.loadingOnMount = false;
    }

    if (shouldClearAndLoadFavorites(this.props, nextProps)) {
      // if loading from componentDidMount is in progress, don't start loading again
      if (!this.loadingOnMount) this.props.startLoading();
      this.loadAssetFavorite({
        priorFavorite: nextProps.priorFavorite,
        latestFavorite: nextProps.latestFavorite,
      });
    }
  }

  loadAssetFavorite = ({ priorFavorite, latestFavorite, forceClear = false }) => {
    const opts = {
      priorFavorite,
      latestFavorite,
      forceClear,
      clearAction: this.props.clearAssets,
      loadAction: this.props.loadAssets,
    };
    clearAndLoadFavorites(opts);
  };

  refreshAssets = () => {
    this.loadAssetFavorite({
      priorFavorite: this.props.priorFavorite,
      latestFavorite: this.props.latestFavorite,
      forceClear: true,
    });
  };

  requestNext = (favoritePagination) => {
    const latestPage = parseInt(favoritePagination.get('latestPage'), 10);
    const nextPage = latestPage + 1;
    loadFavorite({
      page: nextPage,
      favorite: this.props.latestFavorite,
      loadAction: this.props.loadAssets,
    });
  };

  render() {
    const {
      assets,
      assetsView,
      assetsRequesting,
      favoritePagination,
      intl: {
        formatMessage,
      },
      latestFavorite,
      isValidSlug,
    } = this.props;

    if (!isValidSlug) {
      return <NotFoundPage />;
    }

    const headingTitle = latestFavorite.title ? latestFavorite.message : messages.defaultHeader;

    return (
      <Page id="assets-page">
        <Helmet
          title={formatMessage(headingTitle)}
          meta={[
            { name: 'description', content: formatMessage(messages.description) },
          ]}
        />
        <PageHeadingPanel>
          <PageHeadingTitle modifiers={['large', 'padLeft']}>
            <FormattedMessage {...headingTitle} />
          </PageHeadingTitle>
          <SelectorBar
            view={assetsView}
            setView={this.props.setAssetsView}
            type="asset"
          />
          <Divider />
        </PageHeadingPanel>
        {/* NOTE: Suppress QuickActionBar in Alpha */}
        {/* <QuickActionBar items={quickActionItems} onItemClicked={noop} /> */}
        <AssetsPanel
          assets={assets}
          assetsView={assetsView}
          refreshAssets={this.refreshAssets}
          requestNext={this.requestNext}
          assetsRequesting={assetsRequesting}
          favoritePagination={favoritePagination}
          latestFavorite={latestFavorite}
        />
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return {
    favoritePagination: favoritePaginationSelector(state),
    assets: assetsSelector(state),
    assetsRequesting: assetsRequestingSelector(state),
    assetsView: assetsViewSelector(state),
    latestFavorite: latestFavoriteSelector(state),
    priorFavorite: priorFavoriteSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadAssets: filter => dispatch(loadAssets(filter)),
    clearAssets: () => dispatch(clearAssets()),
    setAssetsView: id => dispatch(setAssetsView(id)),
    startLoading: () => dispatch(setLoadingStarted(true)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(AssetsPage));
