import { defineMessages } from 'react-intl';

const messages = defineMessages({
  breadcrumbs: {
    id: 'containers.ServiceRequestDetailPage.meta.breadcrumbs',
    defaultMessage: 'Request {number}',
  },
  description: {
    id: 'containers.ServiceRequestDetailPage.meta.description',
    defaultMessage: 'The Service Request Detail page of the application',
  },
  title: {
    id: 'containers.ServiceRequestDetailPage.meta.title',
    defaultMessage: 'The Service Request Detail Page',
  },
});

export default messages;
