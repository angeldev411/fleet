import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Helmet from 'react-helmet';

import NotFoundPage from 'components/NotFoundPage';

import { dataNotFound } from 'utils/httpUtils';

import messages from './messages';

class ServiceRequestDetailData extends Component {
  static propTypes = {
    addPageToBreadcrumbs: PropTypes.func.isRequired,
    ChildComponent: PropTypes.func.isRequired,
    // This component doesn't care what is in componentProps, only that it is an object.
    // eslint-disable-next-line react/forbid-prop-types
    componentProps: PropTypes.object.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    loadServiceRequest: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        serviceRequestId: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    serviceRequestError: PropTypes.shape({
      error: PropTypes.oneOf([true]),
    }),
    serviceRequestInfo: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
    setCurrentServiceRequest: PropTypes.func.isRequired,
    setLoadingStarted: PropTypes.func.isRequired,
  }

  static defaultProps = {
    serviceRequestError: {},
  }

  componentWillMount() {
    const { match: { params: { serviceRequestId } } } = this.props;
    this.props.setCurrentServiceRequest(serviceRequestId);
  }

  componentDidMount() {
    const { match: { params: { serviceRequestId } } } = this.props;
    this.props.loadServiceRequest(serviceRequestId);
    this.props.setLoadingStarted();
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.serviceRequestInfo.id && nextProps.serviceRequestInfo.id) {
      this.props.addPageToBreadcrumbs({
        path: this.props.location.pathname,
        message: messages.breadcrumbs,
        number: nextProps.serviceRequestInfo.id,
      });
    }
  }

  componentWillUnmount() {
    this.props.setCurrentServiceRequest();
  }

  render() {
    const {
      ChildComponent,
      intl: { formatMessage },
      serviceRequestError,
    } = this.props;

    if (dataNotFound(serviceRequestError)) {
      return <NotFoundPage />;
    }

    return (
      <div>
        <Helmet
          title={formatMessage(messages.title)}
          meta={[{ name: 'description', content: formatMessage(messages.description) }]}
        />
        <ChildComponent {...this.props.componentProps} />
      </div>
    );
  }
}

export default ServiceRequestDetailData;
