import { noop } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import ServiceRequestDetailData from '../Component';

const TestDiv = () => <div id="test-div" />;

const serviceRequestId = '123';

const defaultProps = {
  ChildComponent: TestDiv,
  componentProps: {},
  intl: { formatMessage: message => message.defaultMessage },
  loadServiceRequest: noop,
  match: { params: { serviceRequestId } },
  setCurrentServiceRequest: noop,
  setLoadingStarted: noop,
  serviceRequestError: {},
  addPageToBreadcrumbs: noop,
  location: { pathname: 'some-path' },
  serviceRequestInfo: {},
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestDetailData {...props} />);
}

test('mounting sets current service request with id in route on mounting', () => {
  const setCurrentServiceRequest = createSpy();
  const testProps = {
    ...defaultProps,
    setCurrentServiceRequest,
  };
  const component = shallowRender(testProps);
  component.instance();
  expect(setCurrentServiceRequest).toHaveBeenCalledWith(serviceRequestId);
});

test('componentDidMount loads the service request and sets loading started', () => {
  const loadServiceRequest = createSpy();
  const setLoadingStarted = createSpy();
  const testProps = {
    ...defaultProps,
    loadServiceRequest,
    setLoadingStarted,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.componentDidMount();
  expect(loadServiceRequest).toHaveBeenCalledWith(serviceRequestId);
  expect(setLoadingStarted).toHaveBeenCalled();
});

test('componentWillReceiveProps adds page to breadcrumbs when request info loads', () => {
  const addPageToBreadcrumbs = createSpy();
  const testProps = {
    ...defaultProps,
    addPageToBreadcrumbs,
  };
  const component = shallowRender(testProps);
  component.setProps({ serviceRequestInfo: { id: serviceRequestId } });
  expect(addPageToBreadcrumbs).toHaveBeenCalled();
});

test('componentWillReceiveProps does not add page to breadcrumbs when request already loaded', () => {
  const addPageToBreadcrumbs = createSpy();
  const serviceRequestInfo = { id: serviceRequestId };
  const testProps = {
    ...defaultProps,
    serviceRequestInfo,
    addPageToBreadcrumbs,
  };
  const component = shallowRender(testProps);
  component.setProps({ serviceRequestInfo });
  expect(addPageToBreadcrumbs).toNotHaveBeenCalled();
});


test('componentWillUnmount sets the current service request to undefined', () => {
  const setCurrentServiceRequest = createSpy();
  const testProps = {
    ...defaultProps,
    setCurrentServiceRequest,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  setCurrentServiceRequest.reset();
  expect(setCurrentServiceRequest).toNotHaveBeenCalled();
  instance.componentWillUnmount();
  expect(setCurrentServiceRequest).toHaveBeenCalledWith();
});

test('renders the ChildComponent with the componentProps', () => {
  const component = shallowRender();
  expect(component.find(defaultProps.ChildComponent))
    .toHaveProps({ ...defaultProps.componentProps });
});

test('renders "NotFoundPage" if the status of response is 400', () => {
  const testProps = {
    ...defaultProps,
    serviceRequestError: {
      status: 400,
    },
  };
  const component = shallowRender(testProps);
  expect(component).toContain('NotFoundPage');
});
