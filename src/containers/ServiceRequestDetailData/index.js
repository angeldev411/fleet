import PropTypes from 'prop-types';
import React from 'react';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  addPageToBreadcrumbs,
  setLoadingStarted,
} from 'redux/app/actions';

import { loadServiceRequestRequestErrorSelector } from 'redux/requests/selectors';
import {
  loadServiceRequest,
  setCurrentServiceRequest,
} from 'redux/serviceRequests/actions';
import {
  currentServiceRequestSelector,
} from 'redux/serviceRequests/selectors';

import immutableToJS from 'utils/immutableToJS';

import Component from './Component';

/**
 * `withServiceRequestDetailData` is an HOC used to map data from state down to the Component that
 * handles logic for setting page level details for service requests. The typical pattern for HOC's
 * is to include the logic component within the HOC and return it directly. Although there is
 * nothing wrong with this pattern, it makes testing the logic nearly impossible. Extracting the
 * logic from the data mapping allows simple testing of the logic.
 * @param  {Component} DetailsPage the details page component being rendered
 */
/* istanbul ignore next */
const withServiceRequestDetailData = (DetailsPage) => {
  function WithServiceRequestDetailData(props) {
    return (
      <Component {...props} ChildComponent={DetailsPage} />
    );
  }

  WithServiceRequestDetailData.propTypes = {
    addPageToBreadcrumbs: PropTypes.func.isRequired,
    // This component doesn't care what is in componentProps, only that it is an object.
    // eslint-disable-next-line react/forbid-prop-types
    componentProps: PropTypes.object.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    loadServiceRequest: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        serviceRequestId: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    serviceRequestError: PropTypes.shape({
      error: PropTypes.oneOf([true]),
    }),
    serviceRequestInfo: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
    setCurrentServiceRequest: PropTypes.func.isRequired,
    setLoadingStarted: PropTypes.func.isRequired,
  };

  WithServiceRequestDetailData.defaultProps = {
    serviceRequestError: {},
  };

  function mapStateToProps(state) {
    return {
      serviceRequestError: loadServiceRequestRequestErrorSelector(state),
      serviceRequestInfo: currentServiceRequestSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      addPageToBreadcrumbs: breadcrumbs => dispatch(addPageToBreadcrumbs(breadcrumbs)),
      loadServiceRequest: serviceRequestId =>
        dispatch(loadServiceRequest({ serviceRequestId })),
      setCurrentServiceRequest: serviceRequestId =>
        dispatch(setCurrentServiceRequest(serviceRequestId)),
      setLoadingStarted: () =>
        dispatch(setLoadingStarted(true)),
    };
  }

  return withRouter(
    connect(mapStateToProps, mapDispatchToProps)(
      injectIntl(
        immutableToJS(WithServiceRequestDetailData),
      ),
    ),
  );
};

export default withServiceRequestDetailData;
