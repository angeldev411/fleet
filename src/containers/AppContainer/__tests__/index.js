import React from 'react';
import {
  test,
  expect,
  shallow,
  spyOn,
} from '__tests__/helpers/test-setup';

import { AppContainer } from '../index';

const defaultChildren = <p>Children</p>;
const defaultLocation = {
  pathname: 'my_url',
};

const defaultProps = {
  children: defaultChildren,
  expanded: true,
  location: defaultLocation,
};

function shallowRender(props = defaultProps) {
  const { children, expanded, location } = props;
  return shallow(
    <AppContainer expanded={expanded} location={location}>
      {children}
    </AppContainer>);
}

test('AppContainer wraps an AppBody', () => {
  expect(shallowRender()).toContain('AppBody');
});

test('AppContainer passes expanded state to AppBody', () => {
  const testProps = {
    ...defaultProps,
    expanded: false,
  };
  const container = shallowRender(testProps);
  const appBody = container.find('AppBody');
  expect(appBody).toHaveProp('expanded', false);
});

test('AppContainer wraps children in the AppBody', () => {
  const children = <p id="children">Test</p>;
  const testProps = {
    ...defaultProps,
    children,
  };
  const container = shallowRender(testProps);
  const appBody = container.find('AppBody');
  expect(appBody).toContain('p#children');
});

test('resets scroll position on route change', () => {
  const scrollSpy = spyOn(window, 'scrollTo');
  const location = 'new_url';
  const prevProps = {
    ...defaultProps,
    location,
  };
  const container = shallowRender();
  const instance = container.instance();
  instance.componentDidUpdate(prevProps);
  expect(scrollSpy).toHaveBeenCalledWith(0, 0);
});

test('does not reset scroll position if route does not change', () => {
  const scrollSpy = spyOn(window, 'scrollTo');
  const container = shallowRender();
  const instance = container.instance();
  instance.componentDidUpdate(defaultProps);
  expect(scrollSpy).toNotHaveBeenCalled();
});
