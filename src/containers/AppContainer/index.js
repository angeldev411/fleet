import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { leftNavExpandedSelector } from 'redux/ui/selectors';

import AppBody from 'components/AppBody';

function mapStateToProps(state) {
  return {
    expanded: leftNavExpandedSelector(state),
  };
}

/**
 * AppContainer wraps the main body of the application (excluding the top and left nav)
 * and passes down the left-nav expanded state to the AppBody.
 */
export class AppContainer extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    expanded: PropTypes.bool,
    location: PropTypes.shape({
      pathname: PropTypes.string,
    }).isRequired,
  };

  static defaultProps = {
    expanded: true,
  };

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      // prevents Detail pages from NOT resetting the scroll state
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { expanded, children } = this.props;
    return (
      <AppBody expanded={expanded}>
        {children}
      </AppBody>
    );
  }
}

export default withRouter(connect(mapStateToProps)(AppContainer));
