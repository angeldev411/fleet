import React from 'react';
import { fromJS } from 'immutable';

import {
  test,
  expect,
  shallow,
  createSpy,
  noop,
} from '__tests__/helpers/test-setup';

import { AssetDisplayContainer } from '../index';

const assetInfo = fromJS({
  id: '456',
});

const type = 'card';
const select = createSpy();
const selectedAssetId = '456';

const defaultProps = {
  datum: assetInfo,
  context: {
    sizeStore: {
      size: 'large',
    },
  },
  assetInfo,
  type,
  select,
  selectedAssetId,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetDisplayContainer {...props} />);
}

test('Renders a AssetCard with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AssetCard');
  expect(component.find('AssetCard')).toHaveProps({
    assetInfo,
  });
});

test('Renders a AssetListRow with the expected props when the type is `list`', () => {
  const component = shallowRender({ ...defaultProps, type: 'list' });
  const onSelect = noop;
  const isSelected = assetInfo.get('id') === selectedAssetId;
  expect(component).toContain('AssetListRow');
  expect(component.find('AssetListRow')).toHaveProps({
    assetInfo,
    onSelect,
    isSelected,
  });
});
