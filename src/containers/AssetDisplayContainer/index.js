import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { sizes } from 'reactive-container';
import ImmutablePropTypes from 'react-immutable-proptypes';

import { selectAsset } from 'redux/assets/actions';
import { selectedAssetSelector } from 'redux/assets/selectors';

import { AssetCard, AssetListRow } from 'components/AssetDisplayElement';

import { CardWrapper } from 'elements/Card';

export class AssetDisplayContainer extends Component {
  componentWillUnmount() {
    this.props.select('');
  }

  onSelect = () => {
    this.props.select(this.props.datum.get('id'));
  }

  render() {
    const {
      datum,
      selectedAssetId,
      type,
    } = this.props;

    const isSelected = datum.get('id') === selectedAssetId;

    switch (type) {
      case 'list':
        return (
          <AssetListRow
            assetInfo={datum}
            onSelect={this.onSelect}
            isSelected={isSelected}
          />
        );
      case 'card':
      default:
        return (
          <CardWrapper
            responsiveModifiers={{
              [sizes.XS]: ['1_per_row'],
              [sizes.SM]: ['2_per_row'],
              [sizes.MD]: ['3_per_row'],
              [sizes.LG]: ['4_per_row'],
              [sizes.XL]: ['5_per_row'],
            }}
          >
            <AssetCard
              assetInfo={datum}
              onSelect={this.onSelect}
              isSelected={isSelected}
            />
          </CardWrapper>
        );
    }
  }
}

AssetDisplayContainer.propTypes = {
  datum: ImmutablePropTypes.contains({
    id: PropTypes.string,
  }).isRequired,
  select: PropTypes.func.isRequired,
  selectedAssetId: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['card', 'list', 'map']).isRequired,
};

function mapStateToProps() {
  return state => ({
    selectedAssetId: selectedAssetSelector(state),
  });
}

function mapDispatchToProps(dispatch) {
  return {
    select: id => dispatch(selectAsset(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AssetDisplayContainer);
