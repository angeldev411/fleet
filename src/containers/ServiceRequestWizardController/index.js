import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router';
import { compose } from 'recompose';

import withConnectedData from './Container';

export class ServiceRequestWizardController extends Component {
  static propTypes = {
    closeModal: PropTypes.func,
    createServiceRequest: PropTypes.func.isRequired,
    currentServiceRequest: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
    history: PropTypes.shape({
      goBack: PropTypes.func.isRequired,
    }).isRequired,
    render: PropTypes.func.isRequired,
    modal: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    closeModal: noop,
  };

  /**
   * This controller does not implement any type of specialized branching logic. If it did,
   * that logic would be implemented here and passed down to the rendered child.
   */
  // buildStepsArray = (wizardControllerState, mappedWizardConfig) => {}

  /**
   * handleWizardCancel handles cancelling the wizard and redirecting the
   * user to the appropriate location when cancelling a request.
   */
  handleWizardCancel = () => {
    // User is viewing the modal view. Close the modal.
    if (this.props.modal) {
      // handle closing modal action.
      this.props.closeModal();
      return;
    }
    // User is viewing the page view. Return to the previous page
    this.props.history.goBack();
  }

  /**
   * handleWizardSubmit orchestrates manipulating the raw input data into a proper request body
   * and dispatching a request action
   * @param  {Object} inputData the raw input data accumulated in the WizardController
   */
  handleWizardSubmit = (inputData) => {
    const processedData = this.processInputData(inputData);
    this.props.createServiceRequest(processedData);
  }

  /**
   * processInputData converts the raw input data into a useful request body for posting to the api.
   * @param  {Object} inputData the raw input data accumulated in the WizardController.
   * @return {Object} a properly formatted object ready to be sent over the api.
   */
  processInputData = (inputData) => {
    const {
      reasonForRepair: {
        complaintCode: complaint,
        complaintDescription,
        description: notes,
      },
    } = inputData;
    return {
      serviceProviderId: 'MVVOSE',
      assetId: '2034946',
      reasonForRepair: '10',
      complaint,
      complaintDescription,
      notes,
    };
  }

  render() {
    const {
      currentServiceRequest,
      render,
      ...rest
    } = this.props;

    if (currentServiceRequest.id) {
      // if the user is viewing the modal view, close it before redirecting.
      this.props.closeModal();
      return <Redirect to={`/service-requests/${currentServiceRequest.id}/submitted`} />;
    }

    return this.props.render({
      ...rest,
      handleWizardCancel: this.handleWizardCancel,
      handleWizardSubmit: this.handleWizardSubmit,
    });
  }
}

export default compose(
  withConnectedData,
  withRouter,
)(ServiceRequestWizardController);
