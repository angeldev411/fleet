import { noop } from 'lodash';
import React from 'react';

import {
  expect,
  test,
  shallow,
  createSpy,
  spyOn,
} from '__tests__/helpers/test-setup';

import { ServiceRequestWizardController } from '../index';

const Child = p => <div {...p} />;

const defaultProps = {
  closeModal: noop,
  createServiceRequest: noop,
  currentServiceRequest: {},
  history: { goBack: noop },
  render: p => <Child {...p} />,
  modal: false,
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestWizardController {...props} />);
}

test('it renders the Child with passed in and wizard handler props', () => {
  const testProps = { ...defaultProps, test: true };
  const component = shallowRender(testProps);
  const instance = component.instance();
  expect(component).toContain(Child);
  const child = component.find(Child);
  expect(child.props()).toContain({
    test: true,
    handleWizardCancel: instance.handleWizardCancel,
    handleWizardSubmit: instance.handleWizardSubmit,
  });
});

test('handleWizardCancel calls closeModal if modal: true', () => {
  const closeModal = createSpy();
  const testProps = {
    ...defaultProps,
    closeModal,
    modal: true,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.handleWizardCancel();
  expect(closeModal).toHaveBeenCalled();
});

test('handleWizardCancel does NOT call goBack if modal: true', () => {
  const goBack = createSpy();
  const testProps = {
    ...defaultProps,
    history: { goBack },
    modal: true,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.handleWizardCancel();
  expect(goBack).toNotHaveBeenCalled();
});

test('handleWizardCancel does NOT call closeModal if modal: false', () => {
  const closeModal = createSpy();
  const testProps = {
    ...defaultProps,
    closeModal,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.handleWizardCancel();
  expect(closeModal).toNotHaveBeenCalled();
});

test('handleWizardCancel calls goBack if modal: false', () => {
  const goBack = createSpy();
  const testProps = {
    ...defaultProps,
    history: { goBack },
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.handleWizardCancel();
  expect(goBack).toHaveBeenCalled();
});

test('handleWizardSubmit processes the input data and calls createServiceRequest', () => {
  const createServiceRequest = createSpy();
  const testProps = {
    ...defaultProps,
    createServiceRequest,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  const inputData = { raw: true };
  const processedInputData = { processed: true };
  const processInputData = spyOn(instance, 'processInputData').andReturn(processedInputData);
  instance.handleWizardSubmit(inputData);
  expect(processInputData).toHaveBeenCalledWith(inputData);
  expect(createServiceRequest).toHaveBeenCalledWith(processedInputData);
});

test('processInputData returns the expected value', () => {
  const complaintCode = '22';
  const complaintDescription = 'something is wrong';
  const description = 'It just feels funny';
  const inputData = {
    reasonForRepair: {
      complaintCode,
      complaintDescription,
      description,
    },
  };
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.processInputData(inputData)).toEqual({
    serviceProviderId: 'MVVOSE',
    assetId: '2034946',
    reasonForRepair: '10',
    complaint: complaintCode,
    complaintDescription,
    notes: description,
  });
});
