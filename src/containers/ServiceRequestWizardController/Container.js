import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import { createServiceRequest } from 'redux/serviceRequests/actions';
import { currentServiceRequestSelector } from 'redux/serviceRequests/selectors';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (SpecificWizardController) => {
  function WithConnectedData(props) {
    return (<SpecificWizardController {...props} />);
  }

  WithConnectedData.propTypes = {
    createServiceRequest: PropTypes.func.isRequired,
    currentServiceRequest: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
  };

  function mapStateToProps(state) {
    return {
      currentServiceRequest: currentServiceRequestSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      createServiceRequest: data => dispatch(createServiceRequest(data)),
    };
  }

  return compose(
    connect(mapStateToProps, mapDispatchToProps),
    immutableToJS,
  )(WithConnectedData);
};

export default withConnectedData;
