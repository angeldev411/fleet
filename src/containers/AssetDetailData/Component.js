import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Helmet from 'react-helmet';

import NotFoundPage from 'components/NotFoundPage';

import { dataNotFound } from 'utils/httpUtils';

import messages from './messages';

class AssetDetailData extends Component {
  static propTypes = {
    addPageToBreadcrumbs: PropTypes.func.isRequired,
    assetError: PropTypes.shape({
      error: PropTypes.oneOf([true]),
    }),
    assetInfo: PropTypes.shape({
      id: PropTypes.string,
      unitNumber: PropTypes.string,
    }).isRequired,
    ChildComponent: PropTypes.func.isRequired,
    // This component doesn't care what is in componentProps, only that it is an object.
    // eslint-disable-next-line react/forbid-prop-types
    componentProps: PropTypes.object.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    loadAsset: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        assetId: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    setCurrentAsset: PropTypes.func.isRequired,
    setCurrentAssetCases: PropTypes.func.isRequired,
    setLoadingStarted: PropTypes.func.isRequired,
  };

  static defaultProps = {
    assetError: {},
  };

  componentWillMount() {
    const { match: { params: { assetId } } } = this.props;
    this.props.setCurrentAsset(assetId);
  }

  componentDidMount() {
    const { match: { params: { assetId } } } = this.props;
    this.props.loadAsset(assetId);
    this.props.setLoadingStarted();
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.assetInfo.id && nextProps.assetInfo.id) {
      this.props.addPageToBreadcrumbs({
        path: this.props.location.pathname,
        message: messages.breadcrumbs,
        number: nextProps.assetInfo.unitNumber,
      });
    }
  }

  componentWillUnmount() {
    this.props.setCurrentAsset();
    this.props.setCurrentAssetCases({});
  }

  render() {
    const {
      assetError,
      ChildComponent,
      intl: { formatMessage },
    } = this.props;

    if (dataNotFound(assetError)) {
      return <NotFoundPage />;
    }

    return (
      <div>
        <Helmet
          title={formatMessage(messages.title)}
          meta={[{ name: 'description', content: formatMessage(messages.description) }]}
        />
        <ChildComponent {...this.props.componentProps} />
      </div>
    );
  }
}

export default AssetDetailData;
