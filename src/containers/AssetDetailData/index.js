import PropTypes from 'prop-types';
import React from 'react';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import {
  addPageToBreadcrumbs,
  setLoadingStarted,
} from 'redux/app/actions';
import {
  loadAsset,
  setCurrentAsset,
  setCurrentAssetCases,
} from 'redux/assets/actions';
import {
  currentAssetSelector,
} from 'redux/assets/selectors';
import {
  loadAssetRequestErrorSelector,
} from 'redux/requests/selectors';

import immutableToJS from 'utils/immutableToJS';

import Component from './Component';

/* istanbul ignore next */
const withAssetDetailData = (DetailsPage) => {
  function WithAssetDetailData(props) {
    return (
      <Component {...props} ChildComponent={DetailsPage} />
    );
  }

  WithAssetDetailData.propTypes = {
    addPageToBreadcrumbs: PropTypes.func.isRequired,
    assetError: PropTypes.shape({
      error: PropTypes.oneOf([true]),
    }),
    assetInfo: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
    // This component doesn't care what is in componentProps, only that it is an object.
    // eslint-disable-next-line react/forbid-prop-types
    componentProps: PropTypes.object.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    loadAsset: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        assetId: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    setCurrentAsset: PropTypes.func.isRequired,
    setCurrentAssetCases: PropTypes.func.isRequired,
    setLoadingStarted: PropTypes.func.isRequired,
  };

  WithAssetDetailData.defaultProps = {
    assetError: {},
  };

  function mapStateToProps(state) {
    return {
      assetError: loadAssetRequestErrorSelector(state),
      assetInfo: currentAssetSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      addPageToBreadcrumbs: breadcrumbs => dispatch(addPageToBreadcrumbs(breadcrumbs)),
      loadAsset: assetId => dispatch(loadAsset({ assetId })),
      setCurrentAsset: assetId => dispatch(setCurrentAsset(assetId)),
      setCurrentAssetCases: assetCases => dispatch(setCurrentAssetCases(assetCases)),
      setLoadingStarted: () => dispatch(setLoadingStarted(true)),
    };
  }

  return compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    injectIntl,
    immutableToJS,
  )(WithAssetDetailData);
};

export default withAssetDetailData;
