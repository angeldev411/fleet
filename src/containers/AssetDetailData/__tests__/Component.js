import { noop } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import AssetDetailData from '../Component';

const TestDiv = () => <div id="test-div" />;

const assetId = '123';

const defaultProps = {
  addPageToBreadcrumbs: noop,
  assetError: {},
  assetInfo: {},
  ChildComponent: TestDiv,
  componentProps: {},
  intl: { formatMessage: message => message.defaultMessage },
  loadAsset: noop,
  location: { pathname: 'some-path' },
  match: { params: { assetId } },
  setCurrentAsset: noop,
  setCurrentAssetCases: noop,
  setLoadingStarted: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetDetailData {...props} />);
}

test('mounting sets current asset with id in route on mounting', () => {
  const setCurrentAsset = createSpy();
  const testProps = {
    ...defaultProps,
    setCurrentAsset,
  };
  const component = shallowRender(testProps);
  component.instance();
  expect(setCurrentAsset).toHaveBeenCalledWith(assetId);
});

test('componentDidMount loads the asset and sets loading started', () => {
  const loadAsset = createSpy();
  const setLoadingStarted = createSpy();
  const testProps = {
    ...defaultProps,
    loadAsset,
    setLoadingStarted,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.componentDidMount();
  expect(loadAsset).toHaveBeenCalledWith(assetId);
  expect(setLoadingStarted).toHaveBeenCalled();
});

test('componentWillReceiveProps adds page to breadcrumbs when asset info loads', () => {
  const addPageToBreadcrumbs = createSpy();
  const testProps = {
    ...defaultProps,
    addPageToBreadcrumbs,
  };
  const component = shallowRender(testProps);
  component.setProps({ assetInfo: { id: assetId } });
  expect(addPageToBreadcrumbs).toHaveBeenCalled();
});

test('componentWillReceiveProps does not add page to breadcrumbs when asset already loaded', () => {
  const addPageToBreadcrumbs = createSpy();
  const assetInfo = { id: assetId };
  const testProps = {
    ...defaultProps,
    assetInfo,
    addPageToBreadcrumbs,
  };
  const component = shallowRender(testProps);
  component.setProps({ assetInfo });
  expect(addPageToBreadcrumbs).toNotHaveBeenCalled();
});

test('componentWillUnmount sets the current asset and asset cases to undefined', () => {
  const setCurrentAsset = createSpy();
  const setCurrentAssetCases = createSpy();
  const testProps = {
    ...defaultProps,
    setCurrentAsset,
    setCurrentAssetCases,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  setCurrentAsset.reset();
  expect(setCurrentAsset).toNotHaveBeenCalled();
  instance.componentWillUnmount();
  expect(setCurrentAsset).toHaveBeenCalledWith();
  expect(setCurrentAssetCases).toHaveBeenCalledWith({});
});

test('renders the ChildComponent with the componentProps', () => {
  const component = shallowRender();
  expect(component.find(defaultProps.ChildComponent))
    .toHaveProps({ ...defaultProps.componentProps });
});

test('renders "NotFoundPage" if the status of response is 400', () => {
  const testProps = {
    ...defaultProps,
    assetError: {
      status: 400,
    },
  };
  const component = shallowRender(testProps);
  expect(component).toContain('NotFoundPage');
});
