import { defineMessages } from 'react-intl';

const messages = defineMessages({
  breadcrumbs: {
    id: 'containers.AssetDetailPage.meta.breadcrumbs',
    defaultMessage: 'Unit {number}',
  },
  description: {
    id: 'containers.AssetDetailPage.meta.description',
    defaultMessage: 'The Asset Detail page of the application',
  },
  title: {
    id: 'containers.AssetDetailPage.meta.title',
    defaultMessage: 'The Asset Detail Page',
  },
});

export default messages;
