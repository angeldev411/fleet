import { defineMessages } from 'react-intl';

const messages = defineMessages({
  description: {
    id: 'containers.CasesPage.meta.description',
    defaultMessage: 'The cases page of the application',
  },
  title: {
    id: 'containers.CasesPage.meta.title',
    defaultMessage: 'The Cases Page',
  },
  defaultHeader: {
    id: 'containers.CasesPage.title.default',
    defaultMessage: 'Cases',
  },
  allRepairCases: {
    id: 'containers.CasesPage.title.allRepairCases',
    defaultMessage: 'All Repair Cases',
  },
  downtime2Days: {
    id: 'containers.CasesPage.title.downtimeGt2Days',
    defaultMessage: 'Downtime > 2 Days',
  },
  pastDueFollowUp: {
    id: 'containers.CasesPage.title.pastDueFollowUp',
    defaultMessage: 'Past Due Follow Up',
  },
  pastDueEtr: {
    id: 'containers.CasesPage.title.pastDueETR',
    defaultMessage: 'Past Due ETR',
  },
  current: {
    id: 'containers.CasesPage.title.current',
    defaultMessage: 'Current',
  },
  uptimeCases: {
    id: 'containers.CasesPage.title.uptimeCases',
    defaultMessage: 'Uptime Cases',
  },
});

export default messages;
