import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Map } from 'immutable';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { injectIntl, FormattedMessage } from 'react-intl';

import QuickActionBar from 'containers/QuickActionBarContainer';

import NotesWidget, { NotesWidgetHeader } from 'compositions/NotesWidget';

import CasesPanel from 'components/CasesPanel';
import SelectorBar from 'components/SelectorBar';
import NotFoundPage from 'components/NotFoundPage';
import ModalWindow from 'components/ModalWindow';

import Divider from 'elements/Divider';
import Page from 'elements/Page';
import PageHeadingPanel from 'elements/PageHeadingPanel';
import PageHeadingTitle from 'elements/PageHeadingTitle';

import {
  setLoadingStarted,
} from 'redux/app/actions';
import {
  favoritePaginationSelector,
  priorFavoriteSelector,
  latestFavoriteSelector,
} from 'redux/app/selectors';

import {
  loadCases,
  clearCases,
} from 'redux/cases/actions';
import {
  casesSelector,
  casesRequestingSelector,
  selectedCaseSelector,
} from 'redux/cases/selectors';

import {
  actionBarItemClickedSelector,
  casesViewSelector,
} from 'redux/ui/selectors';

import {
  ADD_NOTE,
} from 'containers/QuickActionBarContainer/constants';

import { setCasesView } from 'redux/ui/actions';

import {
  shouldClearAndLoadFavorites,
  clearAndLoadFavorites,
  loadFavorite,
} from 'utils/favorites';

import messages from './messages';

export class CasesPage extends Component {
  static propTypes = {
    // All application data kept in state is immutable. To properly validate these,
    // use ImmutablePropTypes.
    cases: ImmutablePropTypes.listOf(
      ImmutablePropTypes.mapContains({
        id: PropTypes.string,
      }),
    ).isRequired,
    casesRequesting: PropTypes.bool,
    casesView: PropTypes.string,
    clearCases: PropTypes.func.isRequired,
    favoritePagination: ImmutablePropTypes.map.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    latestFavorite: PropTypes.shape({
      message: PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired,
      }),
      path: PropTypes.string,
      slug: PropTypes.string,
      title: PropTypes.string,
    }),
    loadCases: PropTypes.func.isRequired,
    priorFavorite: PropTypes.shape({
      message: PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired,
      }),
      path: PropTypes.string,
      slug: PropTypes.string,
      title: PropTypes.string,
    }),
    setCasesView: PropTypes.func.isRequired,
    startLoading: PropTypes.func.isRequired,
    isValidSlug: PropTypes.bool,
    selectedCaseId: PropTypes.string.isRequired,
    actionBarItemClicked: ImmutablePropTypes.contains({
      buttonId: PropTypes.string,
      dropdownItemId: PropTypes.string,
    }),
  };

  static defaultProps = {
    casesRequesting: false,
    casesView: '',
    latestFavorite: {},
    priorFavorite: {},
    totalCount: '',
    isValidSlug: true,
    actionBarItemClicked: Map({
      buttonId: '',
      dropdownItemId: '',
    }),
  };

  state = { showCaseNotesModal: false, lastSelectedCaseId: '' };

  componentDidMount() {
    // if the slug is invalid, we don't need to dispatch the following redux actions
    // to fetch the data from the server

    if (!this.props.isValidSlug) {
      return;
    }

    this.loadingOnMount = true;
    this.props.startLoading();

    this.loadCaseFavorite({
      priorFavorite: this.props.priorFavorite,
      latestFavorite: this.props.latestFavorite,
    });
  }

  componentWillReceiveProps(nextProps) {
    const { actionBarItemClicked, selectedCaseId } = nextProps;
    const buttonId = actionBarItemClicked.get('buttonId');
    const actionTime = actionBarItemClicked.get('time');
    const addingNote = buttonId === ADD_NOTE;
    const buttonWasJustClicked = (
      buttonId !== this.props.actionBarItemClicked.get('buttonId') ||
      actionTime !== this.props.actionBarItemClicked.get('time')
    );
    if (addingNote && buttonWasJustClicked) {
      this.setState({ showCaseNotesModal: true });
    }

    if (selectedCaseId !== this.props.selectedCaseId && selectedCaseId) {
      this.setState({ lastSelectedCaseId: selectedCaseId });
    }

    // if the slug is invalid, we don't need to dispatch the following redux actions
    // to fetch the cases data from the server

    if (!nextProps.isValidSlug) {
      return;
    }

    // if loading has finished, set loadingOnMount to false

    if (!nextProps.casesRequesting && this.props.casesRequesting) {
      this.loadingOnMount = false;
    }

    if (shouldClearAndLoadFavorites(this.props, nextProps)) {
      // if loading from componentDidMount is in progress, don't start loading again
      if (!this.loadingOnMount) this.props.startLoading();
      this.loadCaseFavorite({
        priorFavorite: nextProps.priorFavorite,
        latestFavorite: nextProps.latestFavorite,
      });
    }
  }

  handleHideDetailsModal = () => {
    this.setState({ showCaseNotesModal: false });
  }

  loadCaseFavorite = ({ priorFavorite, latestFavorite, forceClear = false }) => {
    const opts = {
      priorFavorite,
      latestFavorite,
      forceClear,
      clearAction: this.props.clearCases,
      loadAction: this.props.loadCases,
    };
    clearAndLoadFavorites(opts);
  };

  refreshCases = () => {
    this.loadCaseFavorite({
      priorFavorite: this.props.priorFavorite,
      latestFavorite: this.props.latestFavorite,
      forceClear: true,
    });
  };

  requestNext = (favoritePagination) => {
    const latestPage = parseInt(favoritePagination.get('latestPage'), 10);
    const nextPage = latestPage + 1;
    loadFavorite({
      page: nextPage,
      favorite: this.props.latestFavorite,
      loadAction: this.props.loadCases,
    });
  };

  render() {
    const {
      cases,
      casesRequesting,
      casesView,
      favoritePagination,
      intl: {
        formatMessage,
      },
      latestFavorite,
      isValidSlug,
      selectedCaseId,
    } = this.props;

    const { showCaseNotesModal, lastSelectedCaseId } = this.state;

    if (!isValidSlug) {
      return <NotFoundPage />;
    }

    const headingTitle = latestFavorite.title ? latestFavorite.message : messages.defaultHeader;
    const selectedCase = cases.find(item => item.get('id') === lastSelectedCaseId);
    const caseInfo = (selectedCase || Map()).toJS();
    return (
      <Page id="cases-page">
        <Helmet
          title={formatMessage(headingTitle)}
          meta={[
            { name: 'description', content: formatMessage(messages.description) },
          ]}
        />
        <PageHeadingPanel>
          <PageHeadingTitle modifiers={['large', 'padLeft']}>
            <FormattedMessage {...headingTitle} />
          </PageHeadingTitle>
          <SelectorBar
            view={casesView}
            setView={this.props.setCasesView}
            type="case"
          />
          <Divider />
        </PageHeadingPanel>
        <QuickActionBar
          pageTitle="CASES_PAGE"
          disabled={{ ADD_NOTE: !selectedCaseId }}
        />
        <CasesPanel
          cases={cases}
          casesView={casesView}
          refreshCases={this.refreshCases}
          requestNext={this.requestNext}
          casesRequesting={casesRequesting}
          favoritePagination={favoritePagination}
          latestFavorite={latestFavorite}
        />
        {showCaseNotesModal && (
          <ModalWindow
            headerInfo={{ title: <NotesWidgetHeader caseInfo={caseInfo} /> }}
            hideModal={this.handleHideDetailsModal}
            hideByClickingBackground
          >
            <NotesWidget caseInfo={caseInfo} type="modal" action="add_note" />
          </ModalWindow>
        )}
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return {
    cases: casesSelector(state),
    casesRequesting: casesRequestingSelector(state),
    casesView: casesViewSelector(state),
    favoritePagination: favoritePaginationSelector(state),
    latestFavorite: latestFavoriteSelector(state),
    priorFavorite: priorFavoriteSelector(state),
    selectedCaseId: selectedCaseSelector(state),
    actionBarItemClicked: actionBarItemClickedSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    clearCases: () => dispatch(clearCases()),
    loadCases: filter => dispatch(loadCases(filter)),
    setCasesView: id => dispatch(setCasesView(id)),
    startLoading: () => dispatch(setLoadingStarted(true)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(CasesPage));
