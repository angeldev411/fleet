/**
 * This HOC wraps all routes, and is mounted anytime a user is viewing the application. It verifies
 * that a user is authorized to view the application and spawns a token refresh process.
 */

import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  verifyStoredAuthData,
} from 'redux/user/actions';

export class AuthorizationHandler extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    verifyStoredAuthData: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.verifyStoredAuthData();
  }

  render() {
    return this.props.children;
  }
}

function mapDispatchToProps(dispatch) {
  return {
    verifyStoredAuthData: () => dispatch(verifyStoredAuthData()),
  };
}

export default connect(undefined, mapDispatchToProps)(AuthorizationHandler);
