import fscreen from 'fscreen';
import { noop } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
  spyOn,
  createSpy,
} from '__tests__/helpers/test-setup';

import HandleFullScreenMode from '../HandleFullScreenMode';

const ChildComponent = () => <div />;

const defaultProps = {
  fullScreenModeRequested: false,
  requestFullScreenMode: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(
    <HandleFullScreenMode {...props}>
      <ChildComponent />
    </HandleFullScreenMode>,
  );
}

test('renders the child component', () => {
  expect(shallowRender()).toBeA(ChildComponent);
});

test('mounting component adds an event listener via fscreen', () => {
  const addEventListenerSpy = spyOn(fscreen, 'addEventListener');
  const component = shallowRender();
  component.instance();
  expect(addEventListenerSpy).toHaveBeenCalled();
});

test('new prop of fullScreenModeRequested = true starts full screen mode', () => {
  const component = shallowRender();
  const instance = component.instance();
  const startFullScreenModeSpy = createSpy();
  instance.startFullScreenMode = startFullScreenModeSpy;
  component.setProps({ fullScreenModeRequested: true });
  expect(startFullScreenModeSpy).toHaveBeenCalled();
});

test(
  'prop of fullScreenModeRequested = true does not start full screen mode if was already true',
  () => {
    const component = shallowRender({ ...defaultProps, fullScreenModeRequested: true });
    const instance = component.instance();
    const startFullScreenModeSpy = createSpy();
    instance.startFullScreenMode = startFullScreenModeSpy;
    component.setProps({ fullScreenModeRequested: true });
    expect(startFullScreenModeSpy).toNotHaveBeenCalled();
  },
);

test('unmounting component calls endFullScreenMode', () => {
  const component = shallowRender();
  const instance = component.instance();
  const endFullScreenModeSpy = createSpy();
  instance.endFullScreenMode = endFullScreenModeSpy;
  component.unmount();
  expect(endFullScreenModeSpy).toHaveBeenCalled();
});

test('start full screen requests full screen with fscreen', () => {
  const requestFullscreenSpy = spyOn(fscreen, 'requestFullscreen');
  const component = shallowRender();
  const instance = component.instance();
  instance.startFullScreenMode();
  expect(requestFullscreenSpy).toHaveBeenCalled();
});

test('end full screen removes the event listener via fscreen', () => {
  const removeEventListenerSpy = spyOn(fscreen, 'removeEventListener');
  const component = shallowRender();
  const instance = component.instance();
  instance.endFullScreenMode();
  expect(removeEventListenerSpy).toHaveBeenCalled();
});
