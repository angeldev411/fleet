import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import {
  requestFullScreenMode as requestFullScreenModeAction,
} from 'redux/ui/actions';
import {
  requestFullScreenModeSelector,
} from 'redux/ui/selectors';

import HandleFullScreenMode from './HandleFullScreenMode';

const withFullScreenMode = (WrappedComponent) => {
  function WithFullScreenMode(props) {
    const { fullScreenModeRequested, requestFullScreenMode, ...childProps } = props;
    return (
      <HandleFullScreenMode
        fullScreenModeRequested={fullScreenModeRequested}
        requestFullScreenMode={requestFullScreenMode}
      >
        <WrappedComponent {...childProps} />
      </HandleFullScreenMode>
    );
  }

  WithFullScreenMode.propTypes = {
    fullScreenModeRequested: PropTypes.bool,
    requestFullScreenMode: PropTypes.func.isRequired,
  };

  WithFullScreenMode.defaultProps = {
    fullScreenModeRequested: false,
  };

  function mapStateToProps(state) {
    return {
      fullScreenModeRequested: requestFullScreenModeSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      requestFullScreenMode: () => dispatch(requestFullScreenModeAction()),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(WithFullScreenMode);
};

export default withFullScreenMode;
