import fscreen from 'fscreen';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

class HandleFullScreenMode extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    fullScreenModeRequested: PropTypes.bool.isRequired,
    requestFullScreenMode: PropTypes.func.isRequired,
  }

  componentWillMount() {
    fscreen.addEventListener('fullscreenchange', this.handleFullScreenChangeEvent);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.fullScreenModeRequested && !this.props.fullScreenModeRequested) {
      this.startFullScreenMode();
    }
  }

  componentWillUnmount() {
    this.endFullScreenMode();
  }

  startFullScreenMode = () => {
    // You can supply any html element here, but do not supply `document.body`.
    // Numerous browsers translate that incorrectly, and will make elements render
    // in the smallest possible box. For any element with `position: absolute`,
    // that box is 0px X 0px, meaning it disappears.
    fscreen.requestFullscreen(document.documentElement);
  }

  endFullScreenMode = () => {
    fscreen.removeEventListener('fullscreenchange', this.handleFullScreenChangeEvent);
  }

  handleFullScreenChangeEvent = () => {
    if (fscreen.fullscreenEnabled && this.props.fullScreenModeRequested) {
      this.props.requestFullScreenMode(false);
    }
  }

  render() {
    return React.Children.only(this.props.children);
  }
}

export default HandleFullScreenMode;
