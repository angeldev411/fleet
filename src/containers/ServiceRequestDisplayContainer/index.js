import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { sizes } from 'reactive-container';
import ImmutablePropTypes from 'react-immutable-proptypes';

import { selectServiceRequest } from 'redux/serviceRequests/actions';
import { selectedServiceRequestSelector } from 'redux/serviceRequests/selectors';

import { ServiceRequestCard } from 'components/ServiceRequestDisplayElement';

import { CardWrapper } from 'elements/Card';

export class ServiceRequestDisplayContainer extends Component {
  componentWillUnmount() {
    this.props.select('');
  }

  onSelect = () => {
    this.props.select(this.props.datum.get('id'));
  }

  render() {
    const {
      datum,
      selectedServiceRequestId,
      type,
    } = this.props;

    const isSelected = datum.get('id') === selectedServiceRequestId;

    switch (type) {
      case 'list':
        return (
          null
        );
      case 'card':
      default:
        return (
          <CardWrapper
            responsiveModifiers={{
              [sizes.XS]: ['1_per_row'],
              [sizes.SM]: ['2_per_row'],
              [sizes.MD]: ['3_per_row'],
              [sizes.LG]: ['4_per_row'],
              [sizes.XL]: ['5_per_row'],
            }}
          >
            <ServiceRequestCard
              serviceRequestInfo={datum}
              onSelect={this.onSelect}
              isSelected={isSelected}
            />
          </CardWrapper>
        );
    }
  }
}

ServiceRequestDisplayContainer.propTypes = {
  datum: ImmutablePropTypes.contains({
    id: PropTypes.string,
  }).isRequired,
  select: PropTypes.func.isRequired,
  selectedServiceRequestId: PropTypes.string,
  type: PropTypes.oneOf(['card', 'list', 'map']).isRequired,
};

ServiceRequestDisplayContainer.defaultProps = {
  selectedServiceRequestId: '',
};

function mapStateToProps() {
  return state => ({
    selectedServiceRequestId: selectedServiceRequestSelector(state),
  });
}

function mapDispatchToProps(dispatch) {
  return {
    select: id => dispatch(selectServiceRequest(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceRequestDisplayContainer);
