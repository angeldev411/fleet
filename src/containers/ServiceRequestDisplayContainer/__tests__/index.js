import React from 'react';
import { fromJS } from 'immutable';

import {
  test,
  expect,
  shallow,
  createSpy,
  noop,
} from '__tests__/helpers/test-setup';

import { ServiceRequestDisplayContainer } from '../index';

const serviceRequestInfo = fromJS({
  id: '123',
});

const type = 'card';
const select = createSpy();
const selectedServiceRequestId = '123';

const defaultProps = {
  datum: serviceRequestInfo,
  type,
  select,
  selectedServiceRequestId,
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestDisplayContainer {...props} />);
}

test('Renders a ServiceRequestCard with the expected props', () => {
  const component = shallowRender();
  const onSelect = noop;
  const isSelected = serviceRequestInfo.get('id') === selectedServiceRequestId;
  expect(component).toContain('ServiceRequestCard');
  const serviceRequestCard = component.find('ServiceRequestCard');
  expect(serviceRequestCard).toHaveProps({
    serviceRequestInfo,
    onSelect,
    isSelected,
  });
});

test('Container displays the ServiceRequestListRow component when the type is `list`', () => {
  const component = shallowRender({ ...defaultProps, type: 'list' });
  expect(component.node).toEqual(null);
});
