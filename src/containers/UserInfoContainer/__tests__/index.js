import React from 'react';
import { Map } from 'immutable';
import { noop } from 'lodash';
import {
  test,
  expect,
  createSpy,
  mount,
  shallow,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import { UserInfoContainer } from '../index';

function Dummy() {
  return <div id="test-dummy">Inside</div>;
}

const testProfile = Map({ firstName: 'Ringo' });

const testProps = {
  userProfile: testProfile,
  loadUserProfile: noop,
  around: Dummy,
};

function renderContainer(props = testProps) {
  return shallow(<UserInfoContainer {...props} />);
}

function mountPageWithProps(props = testProps) {
  return mount(
    <MountableTestComponent authorized>
      <UserInfoContainer {...props} />
    </MountableTestComponent>,
  );
}

test('Mounting the UserInfoContainer retrieves the user profile data', () => {
  const loadUserProfile = createSpy();
  const userProfile = Map();
  mountPageWithProps({ ...testProps, userProfile, loadUserProfile });
  expect(loadUserProfile).toHaveBeenCalled();
});

test(
  'Mounting the UserInfoContainer does not call loadUserProfile if user profile has loaded',
  () => {
    const loadUserProfile = createSpy();
    mountPageWithProps({ ...testProps, loadUserProfile });
    expect(loadUserProfile).toNotHaveBeenCalled();
  },
);

test('Renders the component given as the "around" parameter', () => {
  const component = renderContainer();
  expect(component).toContain(Dummy);
});

test('Passes the user profile info the the nested component', () => {
  const component = renderContainer();
  expect(component.find('Dummy').props().userProfile).toEqual(testProfile);
});
