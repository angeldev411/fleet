import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';

import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';

import {
  loadUserProfile as loadUserProfileAction,
} from 'redux/user/actions';
import {
  userProfileSelector,
} from 'redux/user/selectors';

export class UserInfoContainer extends Component {
  static propTypes = {
    userProfile: ImmutablePropTypes.map,
    loadUserProfile: PropTypes.func.isRequired,
    around: PropTypes.func.isRequired,
  };

  static defaultProps = {
    userProfile: Map(),
  };

  componentWillMount() {
    if (this.props.userProfile.size === 0) {
      this.props.loadUserProfile();
    }
  }

  render() {
    const { userProfile, loadUserProfile, around: Child, ...rest } = this.props;

    return (
      <Child userProfile={userProfile} {...rest} />
    );
  }
}

function mapStateToProps(state) {
  return {
    userProfile: userProfileSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadUserProfile: () => dispatch(loadUserProfileAction()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserInfoContainer);
