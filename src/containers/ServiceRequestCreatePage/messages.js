import { defineMessages } from 'react-intl';

const messages = defineMessages({
  description: {
    id: 'containers.ServiceRequestCreatePage.meta.description',
    defaultMessage: 'Service Request Wizard',
  },
  title: {
    id: 'containers.ServiceRequestCreatePage.meta.title',
    defaultMessage: 'Service Request Wizard',
  },
});

export default messages;
