import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { injectIntl } from 'react-intl';

import Page from 'elements/Page';
import WizardController from 'containers/WizardController';

import messages from './messages';

function ServiceRequestCreatePage({
  intl: {
    formatMessage,
  },
}) {
  return (
    <Page name="service-request-wizard-page">
      <Helmet
        title={formatMessage(messages.title)}
        meta={[
          { name: 'description', content: formatMessage(messages.description) },
        ]}
      />
      <WizardController configKey="SERVICE_REQUEST_WIZARD" />
    </Page>
  );
}

ServiceRequestCreatePage.propTypes = {
  intl: PropTypes.shape({
    formatMessage: PropTypes.func.isRequired,
  }).isRequired,
};

export default injectIntl(ServiceRequestCreatePage);
