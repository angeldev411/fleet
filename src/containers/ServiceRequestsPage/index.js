import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { injectIntl, FormattedMessage } from 'react-intl';

import ServiceRequestsPanel from 'components/ServiceRequestsPanel';
import SelectorBar from 'components/SelectorBar';
import NotFoundPage from 'components/NotFoundPage';

import Divider from 'elements/Divider';
import Page from 'elements/Page';
import PageHeadingPanel from 'elements/PageHeadingPanel';
import PageHeadingTitle from 'elements/PageHeadingTitle';

// NOTE: Suppress QuickActionBar in Alpha
// import QuickActionBar from 'elements/QuickActionBar';

import {
  setLoadingStarted,
} from 'redux/app/actions';
import {
  favoritePaginationSelector,
  priorFavoriteSelector,
  latestFavoriteSelector,
} from 'redux/app/selectors';

import {
  loadServiceRequests,
  clearServiceRequests,
} from 'redux/serviceRequests/actions';
import {
  serviceRequestsSelector,
  serviceRequestsRequestingSelector,
} from 'redux/serviceRequests/selectors';

import { setServiceRequestsView } from 'redux/ui/actions';
import { serviceRequestsViewSelector } from 'redux/ui/selectors';

import {
  shouldClearAndLoadFavorites,
  clearAndLoadFavorites,
  loadFavorite,
} from 'utils/favorites';

import messages from './messages';

// NOTE: Suppress QuickActionBar in Alpha
// import quickActionItems from './constants';

export class ServiceRequestsPage extends Component {
  static propTypes = {
    // All application data kept in state is immutable. To properly validate these,
    // use ImmutablePropTypes.
    serviceRequests: ImmutablePropTypes.listOf(
      ImmutablePropTypes.mapContains({
        id: PropTypes.string,
      }),
    ).isRequired,
    serviceRequestsRequesting: PropTypes.bool,
    serviceRequestsView: PropTypes.string,
    clearServiceRequests: PropTypes.func.isRequired,
    favoritePagination: ImmutablePropTypes.map.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    latestFavorite: PropTypes.shape({
      message: PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired,
      }),
      path: PropTypes.string,
      slug: PropTypes.string,
      title: PropTypes.string,
    }),
    loadServiceRequests: PropTypes.func.isRequired,
    priorFavorite: PropTypes.shape({
      message: PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired,
      }),
      path: PropTypes.string,
      slug: PropTypes.string,
      title: PropTypes.string,
    }),
    setServiceRequestsView: PropTypes.func.isRequired,
    startLoading: PropTypes.func.isRequired,
    isValidSlug: PropTypes.bool,
  };

  static defaultProps = {
    serviceRequestsRequesting: false,
    serviceRequestsView: '',
    latestFavorite: {},
    priorFavorite: {},
    totalCount: '',
    isValidSlug: true,
  };

  componentDidMount() {
    // if the slug is invalid, we don't need to dispatch the following redux actions
    // to fetch the service requests data from the server
    if (!this.props.isValidSlug) {
      return;
    }

    this.loadingOnMount = true;
    this.props.startLoading();

    this.loadServiceRequestFavorite({
      priorFavorite: this.props.priorFavorite,
      latestFavorite: this.props.latestFavorite,
    });
  }

  componentWillReceiveProps(nextProps) {
    // if the slug is invalid, we don't need to dispatch the following redux actions
    // to fetch the service requests data from the server
    if (!nextProps.isValidSlug) {
      return;
    }

    // if loading has finished, set loadingOnMount to false
    if (!nextProps.serviceRequestsRequesting && this.props.serviceRequestsRequesting) {
      this.loadingOnMount = false;
    }

    if (shouldClearAndLoadFavorites(this.props, nextProps)) {
      // if loading from componentDidMount is in progress, don't start loading again
      if (!this.loadingOnMount) this.props.startLoading();
      this.loadServiceRequestFavorite({
        priorFavorite: nextProps.priorFavorite,
        latestFavorite: nextProps.latestFavorite,
      });
    }
  }

  loadServiceRequestFavorite = ({ priorFavorite, latestFavorite, forceClear = false }) => {
    const opts = {
      priorFavorite,
      latestFavorite,
      forceClear,
      clearAction: this.props.clearServiceRequests,
      loadAction: this.props.loadServiceRequests,
    };
    clearAndLoadFavorites(opts);
  };

  refreshServiceRequests = () => {
    this.loadServiceRequestFavorite({
      priorFavorite: this.props.priorFavorite,
      latestFavorite: this.props.latestFavorite,
      forceClear: true,
    });
  };

  requestNext = (favoritePagination) => {
    const latestPage = parseInt(favoritePagination.get('latestPage'), 10);
    const nextPage = latestPage + 1;
    loadFavorite({
      page: nextPage,
      favorite: this.props.latestFavorite,
      loadAction: this.props.loadServiceRequests,
    });
  };

  render() {
    const {
      serviceRequests,
      serviceRequestsRequesting,
      serviceRequestsView,
      favoritePagination,
      intl: {
        formatMessage,
      },
      latestFavorite,
      isValidSlug,
    } = this.props;

    if (!isValidSlug) {
      return <NotFoundPage />;
    }

    const headingTitle = latestFavorite.title ?
      <FormattedMessage {...latestFavorite.message} /> :
      <FormattedMessage {...messages.defaultHeader} />;

    return (
      <Page id="service-requests-page">
        <Helmet
          title={formatMessage(messages.title)}
          meta={[
            { name: 'description', content: formatMessage(messages.description) },
          ]}
        />
        <PageHeadingPanel>
          <PageHeadingTitle modifiers={['large', 'padLeft']}>
            {headingTitle}
          </PageHeadingTitle>
          <SelectorBar
            view={serviceRequestsView}
            setView={this.props.setServiceRequestsView}
            type="serviceRequests"
          />
          <Divider />
        </PageHeadingPanel>
        {/* NOTE: Suppress QuickActionBar in Alpha */}
        {/* <QuickActionBar items={quickActionItems} onItemClicked={noop} /> */}
        <ServiceRequestsPanel
          serviceRequests={serviceRequests}
          serviceRequestsView={serviceRequestsView}
          refreshServiceRequests={this.refreshServiceRequests}
          requestNext={this.requestNext}
          serviceRequestsRequesting={serviceRequestsRequesting}
          favoritePagination={favoritePagination}
          latestFavorite={latestFavorite}
        />
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return {
    serviceRequests: serviceRequestsSelector(state),
    serviceRequestsRequesting: serviceRequestsRequestingSelector(state),
    serviceRequestsView: serviceRequestsViewSelector(state),
    favoritePagination: favoritePaginationSelector(state),
    latestFavorite: latestFavoriteSelector(state),
    priorFavorite: priorFavoriteSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    clearServiceRequests: () => dispatch(clearServiceRequests()),
    loadServiceRequests: filter => dispatch(loadServiceRequests(filter)),
    setServiceRequestsView: id => dispatch(setServiceRequestsView(id)),
    startLoading: () => dispatch(setLoadingStarted(true)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(ServiceRequestsPage));
