import messages from './messages';

const QUICK_ACTION_ITEMS = [
  {
    id: 'addNote',
    icon: 'envelope-o',
    label: messages.addNote,
  },
  // In alpha version we don't have assignCase, addAttachment, and setFollowup.
  // {
  //   id: 'assignCase',
  //   icon: 'user-o',
  //   label: messages.assignCase,
  // },
  // {
  //   id: 'addAttachment',
  //   icon: 'paperclip',
  //   label: messages.addAttachment,
  // },
  // {
  //   id: 'setFollowUp',
  //   icon: 'flag-o',
  //   label: messages.setFollowUp,
  // },
];

export default QUICK_ACTION_ITEMS;
