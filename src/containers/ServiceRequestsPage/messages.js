import { defineMessages } from 'react-intl';

const messages = defineMessages({
  description: {
    id: 'containers.ServiceRequestsPage.meta.description',
    defaultMessage: 'Service Requests',
  },
  title: {
    id: 'containers.ServiceRequestsPage.meta.title',
    defaultMessage: 'Service Requests',
  },
  addNote: {
    id: 'containers.ServiceRequestsPage.quickActionItem.addNote',
    defaultMessage: 'Add Note',
  },
  defaultHeader: {
    id: 'containers.ServiceRequestsPage.title.default',
    defaultMessage: 'Service Requests',
  },
});

export default messages;
