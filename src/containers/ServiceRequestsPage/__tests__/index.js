import React from 'react';
import { fromJS, Map } from 'immutable';
import { noop } from 'lodash';

import configureStore from 'setup/store';
import {
  test,
  expect,
  createSpy,
  spyOn,
  mount,
  shallow,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import Routes from 'setup/routes';
import * as favoritesUtils from 'utils/favorites';

import { ServiceRequestsPage } from '../index';

const defaultProps = {
  serviceRequests: fromJS([]),
  serviceRequestsView: '',
  intl: {
    formatMessage: noop,
  },
  match: {
    params: {},
  },
  loadServiceRequests: noop,
  clearServiceRequests: noop,
  favoritePagination: Map(),
  setServiceRequestsView: noop,
  startLoading: noop,
  isValidSlug: true,
};

const favorite1 = {
  slug: 'downtime-greater-than-2-days',
  title: 'downtime2Days',
  path: '/cases/filter/downtime-greater-than-2-days',
  message: {
    id: 'downtime-greater-than-2-days',
    defaultMessage: 'Downtime > 2 Days',
  },
};

const favorite2 = {
  slug: 'past-due-follow-up',
  title: 'pastDueFollowUp',
  path: '/cases/filter/past-due-follow-up',
  message: {
    id: 'past-due-follow-up',
    defaultMessage: 'Past Due Follow Up',
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestsPage {...props} />);
}

function mountPage(initialEntries, store = {}) {
  const authStore = configureStore(fromJS({ ...store, user: { isAuthorized: true } }));
  return mount(
    <MountableTestComponent initialEntries={initialEntries} store={authStore}>
      <Routes />
    </MountableTestComponent>,
  );
}

function mountPageWithProps(props = defaultProps) {
  return mount(
    <MountableTestComponent authorized>
      <ServiceRequestsPage {...props} />
    </MountableTestComponent>,
  );
}

test('Visiting a filter route displays the correct page title', () => {
  const serviceRequestFavorites = [
    favorite1,
    favorite2,
  ];
  const store = {
    app: {
      dashboard: {
        favorites: {
          serviceRequests: serviceRequestFavorites,
        },
      },
      favoritePagination: {},
      priorFavorite: {},
      latestFavorite: {},
    },
  };

  serviceRequestFavorites.forEach(({ slug, message: { defaultMessage } }) => {
    const filteredRoute = `/service-requests/filter/${slug}`;
    const page = mountPage([filteredRoute], store);
    const pageHeadingPanel = page.find('PageHeadingPanel');
    expect(pageHeadingPanel.text()).toInclude(defaultMessage);
  });
});

test('Page renders SelectorBar with correct props', () => {
  const page = shallowRender();
  expect(page).toContain('SelectorBar');
  expect(page.find('SelectorBar')).toHaveProps({
    view: defaultProps.serviceRequestsView,
    setView: defaultProps.setServiceRequestsView,
  });
});

test('Page renders ServiceRequestsPanel', () => {
  const page = shallowRender();
  expect(page).toContain('ServiceRequestsPanel');
});

/* ---------------------- clearing and loading favorites behavior --------------------------*/

test(
  'Visiting service requests page with hard coded favorite calls clearAndLoadFavorites with expected args',
  () => {
    const spy = spyOn(favoritesUtils, 'clearAndLoadFavorites');
    mountPageWithProps({ ...defaultProps, latestFavorite: favorite1 });
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: {},
      latestFavorite: favorite1,
      forceClear: false,
      clearAction: defaultProps.clearServiceRequests,
      loadAction: defaultProps.loadServiceRequests,
    });
  },
);

test(
  'Transitioning from one favorite to another calls clearAndLoadFavorites with expected args',
  () => {
    const spy = spyOn(favoritesUtils, 'clearAndLoadFavorites');
    mountPageWithProps({
      ...defaultProps,
      priorFavorite: favorite1,
      latestFavorite: favorite2,
    });
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: favorite1,
      latestFavorite: favorite2,
      forceClear: false,
      clearAction: defaultProps.clearServiceRequests,
      loadAction: defaultProps.loadServiceRequests,
    });
  },
);

test(
  'Visiting service requests page triggers the global loading',
  () => {
    const startLoadingSpy = createSpy();
    const testProps = {
      ...defaultProps,
      latestFavorite: favorite1,
      startLoading: startLoadingSpy,
    };
    mountPageWithProps(testProps);
    expect(startLoadingSpy).toHaveBeenCalled();
  },
);

test(
  'Transitioning from one favorite to another triggers the global loading',
  () => {
    const startLoadingSpy = createSpy();
    const testProps = {
      ...defaultProps,
      priorFavorite: favorite1,
      latestFavorite: favorite2,
      startLoading: startLoadingSpy,
    };
    mountPageWithProps(testProps);
    expect(startLoadingSpy).toHaveBeenCalled();
  },
);

test(
  'Visiting page calls clearAndLoadFavorites with no favorites data if none in props',
  () => {
    const spy = spyOn(favoritesUtils, 'clearAndLoadFavorites');
    mountPageWithProps({
      ...defaultProps,
    });
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: {},
      latestFavorite: {},
      forceClear: false,
      clearAction: defaultProps.clearServiceRequests,
      loadAction: defaultProps.loadServiceRequests,
    });
  },
);

test(
  'Visiting page should call loadServiceRequestFavorite after receiving new favorite prop.',
  () => {
    const component = shallowRender({
      ...defaultProps,
    });
    const instance = component.instance();
    const spy = createSpy();
    instance.loadServiceRequestFavorite = spy;
    component.setProps({ latestFavorite: favorite1 });
    component.update();
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: {},
      latestFavorite: favorite1,
    });
  },
);

test(
  'Visiting page with same prior favorite and latestFavorite does not call clearAndLoadFavorites',
  () => {
    const component = shallowRender({
      ...defaultProps,
      priorFavorite: {},
      latestFavorite: favorite1,
    });
    const instance = component.instance();
    const spy = createSpy();
    instance.loadServiceRequestFavorite = spy;
    component.setProps({ priorFavorite: favorite1, latestFavorite: favorite1 });
    component.update();
    expect(spy).toNotHaveBeenCalled();
  },
);

test(
  'Refresh calls loadServiceRequestFavorite with forceClear = true',
  () => {
    const component = shallowRender({
      ...defaultProps,
      latestFavorite: favorite1,
    });
    const instance = component.instance();
    const spy = createSpy();
    instance.loadServiceRequestFavorite = spy;
    instance.refreshServiceRequests();
    expect(spy).toHaveBeenCalledWith({
      priorFavorite: {},
      latestFavorite: favorite1,
      forceClear: true,
    });
  },
);

test(
  'Refresh does not trigger the global loading',
  () => {
    const startLoadingSpy = createSpy();
    const testProps = {
      ...defaultProps,
      latestFavorite: favorite1,
      startLoading: startLoadingSpy,
    };
    const component = shallowRender(testProps);
    const instance = component.instance();
    instance.refreshServiceRequests();
    expect(startLoadingSpy).toNotHaveBeenCalled();
  },
);

test('requesting the next page includes the current favorite info', () => {
  const spy = spyOn(favoritesUtils, 'loadFavorite');
  const component = shallowRender({
    ...defaultProps,
    latestFavorite: favorite1,
  });
  const instance = component.instance();
  expect(spy).toNotHaveBeenCalled();
  instance.requestNext(fromJS({ latestPage: '24' }));
  expect(spy).toHaveBeenCalled();
  expect(spy.calls[0].arguments[0]).toInclude({ page: 25, favorite: favorite1 });
});

test('renders "NotFoundPage" if the slug is invalid', () => {
  const testProps = {
    ...defaultProps,
    isValidSlug: false,
  };
  const component = shallowRender(testProps);
  expect(component).toContain('NotFoundPage');
});
