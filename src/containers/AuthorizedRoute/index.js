/**
 * Provides an enhanced "Route" component that first verifies if the user is authorized to use the
 * application. If not, it saves the user's desired location and redirects the user to login page.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import isRequiredIf from 'react-proptype-conditional-require';
import { has } from 'lodash';

import {
  isAuthorizedSelector,
} from 'redux/user/selectors';

export function AuthorizedRoute({ component, isAuthorized, render, ...rest }) {
  return (
    <Route
      {...rest}
      render={(componentProps) => {
        if (isAuthorized) {
          if (component) {
            return React.createElement(component, { ...rest, ...componentProps });
          }
          if (render) {
            return render({ ...rest, ...componentProps });
          }
          return (
            <Redirect
              to={{
                pathname: '/404-not-found',
                state: { from: componentProps.location },
              }}
            />
          );
        }
        return (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: componentProps.location },
            }}
          />
        );
      }
      }
    />
  );
}

AuthorizedRoute.propTypes = {
  // A `component` prop or a `render` prop is required. Both should not be supplied.
  // If both are supplied, the `component` prop will take priority.
  // https://reacttraining.com/react-router/web/api/Route/render-func
  component: isRequiredIf(PropTypes.func, props => !has(props, 'render')),
  isAuthorized: PropTypes.bool.isRequired,
  render: isRequiredIf(PropTypes.func, props => !has(props, 'component')),
};

AuthorizedRoute.defaultProps = {
  component: undefined,
  render: undefined,
};

function mapStateToProps(state) {
  return {
    isAuthorized: isAuthorizedSelector(state),
  };
}

export default connect(mapStateToProps)(AuthorizedRoute);
