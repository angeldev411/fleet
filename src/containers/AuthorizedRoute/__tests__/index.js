import React from 'react';

import {
  test,
  expect,
  createSpy,
  spyOn,
  shallow,
} from '__tests__/helpers/test-setup';

import { AuthorizedRoute } from '../index';

const Component = () => <div>Test Component</div>;

const defaultProps = {
  isAuthorized: true,
  component: Component,
};

const componentProps = {
  location: 'testLocation',
};

function shallowRender(props = defaultProps) {
  return shallow(<AuthorizedRoute {...props} />);
}

test('renders Route', () => {
  const component = shallowRender(defaultProps);
  expect(component).toBeA('Route');
});

test('component is rendered if component prop exists.', () => {
  const component = shallowRender(defaultProps);
  const createElementSpy = spyOn(React, 'createElement');
  component.find('Route').props().render(componentProps);
  expect(createElementSpy).toHaveBeenCalledWith(defaultProps.component, componentProps);
});

test('renders function is called if component is not given but render prop function is given', () => {
  const renderSpy = createSpy();
  const testProps = {
    ...defaultProps,
    component: undefined,
    render: renderSpy,
  };
  const component = shallowRender(testProps);
  component.find('Route').props().render(componentProps);
  expect(renderSpy).toHaveBeenCalledWith(componentProps);
});

test('renders Redirect route to 404 when both render function and component are not given', () => {
  const testProps = {
    ...defaultProps,
    component: undefined,
    render: undefined,
  };
  const component = shallowRender(testProps);
  const renderedComponent = shallow(
    <div>
      {
        component.find('Route').props().render(componentProps)
      }
    </div>,
  );
  expect(renderedComponent).toContain('Redirect');
  expect(renderedComponent.find('Redirect').props().to).toEqual({
    pathname: '/404-not-found',
    state: { from: componentProps.location },
  });
});

test('renders Redirect route to login when isAuthorized is false.', () => {
  const testProps = {
    ...defaultProps,
    isAuthorized: false,
  };
  const component = shallowRender(testProps);
  const renderedComponent = shallow(
    <div>
      {
        component.find('Route').props().render(componentProps)
      }
    </div>,
  );
  expect(renderedComponent).toContain('Redirect');
  expect(renderedComponent.find('Redirect').props().to).toEqual({
    pathname: '/login',
    state: { from: componentProps.location },
  });
});
