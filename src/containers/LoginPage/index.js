import PropTypes from 'prop-types';
import React from 'react';
import Helmet from 'react-helmet';
import { injectIntl } from 'react-intl';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { isAuthorizedSelector } from 'redux/user/selectors';

import LoginForm from 'compositions/LoginForm';
import helmetAppConfig from 'setup/helmet';

import Wrapper from './Wrapper';
import messages from './messages';

/**
 * A simple SFC container is responsible for rendering the page containing the login form and
 * redirecting to the correct route when a user is authorized.
 * @param {Object} props The props passed to this component.
 * @param {Boolean} props.isAuthorized A boolean value indicating whether the use is authorized.
 * @param {Object} props.location Contains values relating to the current url.
 */
export function LoginPage({
  isAuthorized,
  location,
  intl: { formatMessage },
}) {
  if (isAuthorized) {
    const {
      state: {
        from: {
          pathname: redirectRoute = '/',
          search: queryString = '',
        } = {},
      } = {},
    } = location;
    const redirectPath = `${redirectRoute}${queryString}`;
    return (
      <Redirect to={redirectPath} />
    );
  }

  return (
    <Wrapper id="login-page">
      <Helmet
        {...helmetAppConfig}
        title={formatMessage(messages.title)}
      />
      <LoginForm />
    </Wrapper>
  );
}

LoginPage.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  location: PropTypes.shape({
    state: PropTypes.shape({
      from: PropTypes.shape({
        pathname: PropTypes.string,
        search: PropTypes.string,
      }),
    }),
  }).isRequired,
  intl: PropTypes.shape({
    formatMessage: PropTypes.func.isRequired,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    isAuthorized: isAuthorizedSelector(state),
  };
}

export default connect(mapStateToProps)(injectIntl(LoginPage));
