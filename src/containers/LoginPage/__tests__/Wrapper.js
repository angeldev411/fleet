import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import Wrapper from '../Wrapper';

test('renders a section', () => {
  expect(shallow(<Wrapper />)).toBeA('section');
});
