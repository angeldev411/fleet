import React from 'react';
import {
  test,
  expect,
  shallow,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';
import Routes from 'setup/routes';

import { LoginPage } from '../index';

const testRedirectRoute = '/test_redirect_path';
const testQueryString = '?query=abc&query2=def';
const intlMock = { formatMessage: message => message.defaultMessage };
const testProps = {
  isAuthorized: true,
  location: {
    state: {
      from: {
        pathname: testRedirectRoute,
        search: testQueryString,
      },
    },
  },
  intl: intlMock,
};

function renderPage(props = testProps) {
  return shallow(<LoginPage {...props} />);
}

function mountRoutes(initialEntries, { authorized = true } = {}) {
  return mount(
    <MountableTestComponent authorized={authorized} initialEntries={initialEntries}>
      <Routes />
    </MountableTestComponent>,
  );
}

test('visiting the login route when unauthorized mounts the login form', () => {
  const loginRoute = '/login';
  const page = mountRoutes([loginRoute], { authorized: false });
  expect(page.find('#login-form').exists()).toBe(true);
});

test('rendering the login page when authorized renders a redirect component', () => {
  const page = renderPage();
  expect(page).toBeA('Redirect');
  expect(page).toHaveProp('to', `${testRedirectRoute}${testQueryString}`);
});

test('rendering the login page when authorized defaults to root route redirect', () => {
  const page = renderPage({ isAuthorized: true, location: {}, intl: intlMock });
  expect(page).toBeA('Redirect');
  expect(page).toHaveProp('to', '/');
});
