import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const styles = props => `
  align-items: center;
  background: linear-gradient(179.89deg, ${props.theme.colors.brand.primary} 0%, ${props.theme.colors.brand.secondary} 72.71%, ${props.theme.colors.brand.primary} 100%);;
  display: flex;
  height: 100%;
  justify-content: center;
  left: 0;
  position: fixed;
  top: 0;
  width: 100%;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    brand: PropTypes.shape({
      primary: PropTypes.string.isRequired,
      secondary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'LoginPageWrapper',
  styled.section,
  styles,
  {
    themePropTypes,
    className: 'LoginPage',
  },
);
