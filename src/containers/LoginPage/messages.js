import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'containers.LoginPage.meta.title',
    defaultMessage: 'Log In',
  },
});

export default messages;
