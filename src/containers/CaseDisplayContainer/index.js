import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { sizes } from 'reactive-container';
import ImmutablePropTypes from 'react-immutable-proptypes';

import { selectCase } from 'redux/cases/actions';
import { selectedCaseSelector } from 'redux/cases/selectors';
import { makeGetAssetById } from 'redux/assets/selectors';
import { makeGetServiceProviderById } from 'redux/serviceProviders/selectors';

import { CaseCard, CaseListRow } from 'components/CaseDisplayElement';

import { CardWrapper } from 'elements/Card';

export class CaseDisplayContainer extends Component {
  componentWillUnmount() {
    this.props.select('');
  }

  onSelect = () => {
    this.props.select(this.props.datum.get('id'));
  }

  render() {
    const {
      assetInfo,
      datum,
      serviceProviderInfo,
      selectedCaseId,
      type,
    } = this.props;

    switch (type) {
      case 'list':
        return (
          <CaseListRow
            assetInfo={assetInfo}
            caseInfo={datum}
            onSelect={this.onSelect}
            isSelected={datum.get('id') === selectedCaseId}
          />
        );
      case 'card':
      default:
        return (
          <CardWrapper
            responsiveModifiers={{
              [sizes.XS]: ['1_per_row'],
              [sizes.SM]: ['2_per_row'],
              [sizes.MD]: ['3_per_row'],
              [sizes.LG]: ['4_per_row'],
              [sizes.XL]: ['5_per_row'],
            }}
          >
            <CaseCard
              caseInfo={datum}
              assetInfo={assetInfo}
              serviceProviderInfo={serviceProviderInfo}
              onSelect={this.onSelect}
              isSelected={datum.get('id') === selectedCaseId}
            />
          </CardWrapper>
        );
    }
  }
}

CaseDisplayContainer.propTypes = {
  assetInfo: ImmutablePropTypes.contains({
    id: PropTypes.string,
  }).isRequired,
  datum: ImmutablePropTypes.contains({
    id: PropTypes.string,
  }).isRequired,
  serviceProviderInfo: ImmutablePropTypes.contains({
    id: PropTypes.string,
  }).isRequired,
  select: PropTypes.func.isRequired,
  selectedCaseId: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['card', 'list', 'map']).isRequired,
};

CaseDisplayContainer.defaultProps = {
  selectedCaseId: '',
};

function mapStateToProps() {
  const getAsset = makeGetAssetById();
  const getServiceProvider = makeGetServiceProviderById();

  return (state, props) => ({
    assetInfo: getAsset(state, props.datum.get('asset')),
    serviceProviderInfo: getServiceProvider(state, props.datum.get('serviceProvider')),
    selectedCaseId: selectedCaseSelector(state),
  });
}

function mapDispatchToProps(dispatch) {
  return {
    select: id => dispatch(selectCase(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CaseDisplayContainer);
