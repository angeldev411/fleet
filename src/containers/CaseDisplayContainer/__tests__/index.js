import React from 'react';
import { fromJS } from 'immutable';

import {
  test,
  expect,
  shallow,
  mount,
  MountableTestComponent,
  createSpy,
  noop,
} from '__tests__/helpers/test-setup';
import configureStore from 'setup/store';

import ConnectedCaseDisplayContainer, { CaseDisplayContainer } from '../index';

const caseInfo = fromJS({
  id: '123',
  asset: '456',
  serviceProvider: '789',
  unreadNotesCount: 0,
});

const assetInfo = fromJS({
  id: '456',
});

const serviceProviderInfo = fromJS({
  id: '789',
});

const type = 'card';
const select = createSpy();
const selectedCaseId = '123';

const defaultProps = {
  datum: caseInfo,
  context: {
    sizeStore: {
      size: 'large',
    },
  },
  assetInfo,
  serviceProviderInfo,
  type,
  select,
  selectedCaseId,
};

const state = fromJS({
  cases: {
    responseIds: ['123'],
    byId: {
      123: caseInfo,
    },
  },
  assets: {
    responseIds: ['456'],
    byId: {
      456: assetInfo,
    },
  },
  serviceProviders: {
    responseIds: ['789'],
    byId: {
      789: serviceProviderInfo,
    },
  },
});

const store = configureStore(state);

function mountComponent(props = defaultProps, viewType = 'card') {
  return mount(
    <MountableTestComponent store={store}>
      <ConnectedCaseDisplayContainer {...props} type={viewType} />
    </MountableTestComponent>,
  );
}

function shallowRender(props = defaultProps) {
  return shallow(<CaseDisplayContainer {...props} />);
}

test('Renders a CaseCard with the expected props', () => {
  const component = shallowRender();
  const onSelect = noop;
  const isSelected = caseInfo.get('id') === selectedCaseId;
  expect(component).toContain('CaseCard');
  expect(component.find('CaseCard')).toHaveProps({
    caseInfo,
    assetInfo,
    serviceProviderInfo,
    onSelect,
    isSelected,
  });
});

test('Container retrieves expected asset and service provider info based on case', () => {
  const testProps = {
    datum: caseInfo,
    select,
    selectedCaseId,
  };
  const onSelect = noop;
  const isSelected = false;
  const component = mountComponent(testProps);
  const caseCard = component.find('CaseCard');
  expect(caseCard).toHaveProps({
    assetInfo,
    caseInfo,
    serviceProviderInfo,
    onSelect,
    isSelected,
  });
});

test('Renders a CaseListRow with the expected props when the type is `list`', () => {
  const component = shallowRender({ ...defaultProps, type: 'list' });
  const onSelect = noop;
  const isSelected = caseInfo.get('id') === selectedCaseId;
  expect(component).toContain('CaseListRow');
  expect(component.find('CaseListRow')).toHaveProps({
    caseInfo,
    assetInfo,
    onSelect,
    isSelected,
  });
});
