import { connect } from 'react-redux';
import { compose, setDisplayName } from 'recompose';

import {
  updateScrollToUnreadNote,
} from 'redux/ui/actions';

import immutableToJS from 'utils/immutableToJS';

import UnreadNoteLabel from 'components/UnreadNoteLabel';

function mapDispatchToProps(dispatch) {
  return {
    onClickAction: () => dispatch(updateScrollToUnreadNote(true)),
  };
}

export default compose(
  setDisplayName('UnreadNoteLabelContainer'),
  connect(null, mapDispatchToProps),
  immutableToJS,
)(UnreadNoteLabel);
