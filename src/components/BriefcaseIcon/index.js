import { omit } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

export function UnstyledFontAwesome(props) {
  const componentProps = omit(props, ['theme', 'modifiers']);
  return <FontAwesome {...componentProps} />;
}

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.brand.primary};
  font-size: ${px2rem(14)};
  padding-right: ${px2rem(6)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    brand: PropTypes.shape({
      primary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'BriefcaseIcon',
  styled(UnstyledFontAwesome),
  styles,
  { themePropTypes },
);
