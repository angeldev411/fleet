import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { UnstyledFontAwesome } from '../index';

test('returns an icon without styled component props', () => {
  const testProps = {
    name: 'briefcase',
    theme: {
      colors: {},
    },
    modifiers: ['test'],
  };
  const component = shallow(<UnstyledFontAwesome {...testProps} />);
  expect(component).toBeA('FontAwesome');
  expect(component.props()).toExcludeKeys(['theme', 'modifiers']);
  expect(component).toHaveProp('name', 'briefcase');
});
