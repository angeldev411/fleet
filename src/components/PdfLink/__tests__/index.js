import React from 'react';

import {
  test,
  expect,
  shallow,
  spyOn,
} from '__tests__/helpers/test-setup';

import * as fetch from 'utils/fetch';

import PdfLink from '../index';

function shallowRender(props) {
  return shallow(<PdfLink {...props} />);
}

const testMessage = {
  id: 'my.test.message',
  defaultMessage: 'This is the test message text',
};

test('PdfLink returns an empty span if no message is specified', () => {
  const component = shallowRender({ message: testMessage });
  expect(component).toBeA('span');
  expect(component.children().length).toEqual(0);
});

test('PdfLink links an authorized version of the given URL', () => {
  const authorizedUrl = 'http://authorized.url';
  spyOn(fetch, 'makeApiUrl').andReturn(authorizedUrl);
  const component = shallowRender({ url: 'http://www.pdf.url/path.pdf' });
  expect(component.find('A')).toHaveProp('href', authorizedUrl);
});

test('PdfLink renders the given message', () => {
  const component = shallowRender({ url: 'http://url.co', message: testMessage });
  expect(component.find('FormattedMessage')).toHaveProps(testMessage);
});

test('PdfLink renders no message if none is provided', () => {
  const component = shallowRender({ url: 'http://url.co' });
  expect(component).toNotContain('FormattedMessage');
});
