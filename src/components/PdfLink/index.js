import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage } from 'react-intl';
import URL from 'url-parse';

import { A } from 'base-components';
import IconWrapper from 'elements/IconWrapper';

import { makeApiUrl } from 'utils/fetch';

/**
 * The URLs for PDFs (for estimates, etc) provided by the API are potentially URLs that
 * will require an `access_token` to authorize access.  This component encapsulates the
 * logic for authorizing these URLs and also standardizes the formatting of a PDF link to
 * include the appropriate icon and formatting.
 *
 * @param url The URL for the PDF.  If not provided, an empty span will be returned.
 * @param message An optional message descriptor for a formatted message to accompany the link
 */
function PdfLink({ url, message }) {
  if (!url) {
    return <span />;
  }

  const parsedUrl = new URL(url, true);
  const authorizedUrl = makeApiUrl(`${parsedUrl.origin}${parsedUrl.pathname}`, parsedUrl.query);

  return (
    <A target="_blank" href={authorizedUrl} modifiers={['colorText']}>
      <IconWrapper>
        <FontAwesome name="file-pdf-o" />
      </IconWrapper>
      {message && <FormattedMessage {...message} />}
    </A>
  );
}

PdfLink.propTypes = {
  url: PropTypes.string,
  message: PropTypes.shape(),
};

PdfLink.defaultProps = {
  url: null,
  message: null,
};

export default PdfLink;
