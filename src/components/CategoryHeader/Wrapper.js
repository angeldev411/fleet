import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.chrome100};
  border: 1px solid ${props.theme.colors.base.chrome200};
  margin: 0 ${px2rem(8)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
  { themePropTypes },
);
