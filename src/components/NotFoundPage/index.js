import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
  Container,
  Column,
  Row,
} from 'styled-components-reactive-grid';
import { px2rem } from 'decisiv-ui-utils';

import Img from 'elements/Img';
import TextDiv from 'elements/TextDiv';

import messages from './messages';
import imgMap from './map-icon.svg';

function NotFoundPage() {
  return (
    <Container style={{ padding: `${px2rem(160)} 0` }}>
      <Row>
        <Column modifiers={['col', 'center']}>
          <Img alt="map" src={imgMap} />
        </Column>
      </Row>
      <Row>
        <Column
          modifiers={['col', 'center']}
        >
          <TextDiv modifiers={['bold', 'largeText']}>
            <FormattedMessage {...messages.title} />
          </TextDiv>
        </Column>
      </Row>
      <Row>
        <Column modifiers={['col', 'center']}>
          <TextDiv modifiers={['mediumLargeText']}>
            <FormattedMessage {...messages.line1} />
          </TextDiv>
        </Column>
      </Row>
      <Row>
        <Column modifiers={['col', 'center']}>
          <TextDiv modifiers={['mediumLargeText']}>
            <FormattedMessage {...messages.line2} />
          </TextDiv>
        </Column>
      </Row>
    </Container>
  );
}

export default NotFoundPage;
