import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'components.NotFoundPage.youLost',
    defaultMessage: 'It looks like you’re lost…',
  },
  line1: {
    id: 'components.NotFoundPage.line1',
    defaultMessage: 'This page either does not exist or you need permission to view it.',
  },
  line2: {
    id: 'components.NotFoundPage.line2',
    defaultMessage: 'Try using the search bar or contacting the person who gave you the link.',
  },
});

export default messages;
