import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import NotFoundPage from '../index';
import messages from '../messages';

function shallowRender() {
  return shallow(<NotFoundPage />);
}

test('renders image', () => {
  const component = shallowRender();

  expect(component).toContain('Img');
});

test('renders title, line1 and line2', () => {
  const component = shallowRender();
  const formattedMessages = component.find('FormattedMessage');

  expect(formattedMessages.first()).toHaveProps(messages.title);
  expect(formattedMessages.at(1)).toHaveProps(messages.line1);
  expect(formattedMessages.last()).toHaveProps(messages.line2);
});
