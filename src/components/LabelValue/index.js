import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './Wrapper';

function LabelValue({ label, value, marginRight }) {
  return (
    <Wrapper marginRight={marginRight}>
      <span className="label">
        { label }
      </span>
      <span>
        { value }
      </span>
    </Wrapper>
  );
}

LabelValue.propTypes = {
  label: PropTypes.node,
  value: PropTypes.node,
  marginRight: PropTypes.number,
};

LabelValue.defaultProps = {
  label: '',
  value: '',
  marginRight: 0,
};

export default LabelValue;
