import PropTypes from 'prop-types';
import styled from 'styled-components';
import { px2rem, buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  display: flex;
  align-items: center;
  vertical-align: middle;
  font-size: ${props.theme.dimensions.fontSizeNormal};
  margin-right: ${px2rem(props.marginRight)};

  .label {
    font-weight: 700;
    margin-right: 30px;
  }
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    fontSizeNormal: PropTypes.string.isRequired,
  }).isRequired,
};

const propTypes = {
  marginRight: PropTypes.number,
};

const defaultProps = {
  marginRight: 0,
};

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
