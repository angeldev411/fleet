import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import LabelValue from '../index';

test('LabelValue renders a widget', () => {
  const component = shallow(<LabelValue />);
  expect(component).toBeA('Wrapper');
});

test('LabelValue renders the label prop correctly', () => {
  const label = 'label';
  const component = shallow(<LabelValue label={label} />);
  const labelWrapper = component.find('span').first().render();
  const labelText = labelWrapper.text();

  expect(labelText).toInclude(label);
});

test('LabelValue renders the value prop correctly', () => {
  const value = 'value';
  const component = shallow(<LabelValue value={value} />);
  const valueWrapper = component.find('span').last().render();
  const valueText = valueWrapper.text();

  expect(valueText).toInclude(value);
});
