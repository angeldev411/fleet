import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';
import { fromJS, List, Map } from 'immutable';

import CasesListView from '../CasesListView';

const cases = fromJS([
  { id: '123' },
  { id: '456' },
  { id: '789' },
]);

const defaultProps = {
  cases: List(),
  favoritePagination: Map(),
  requesting: false,
};

function renderComponent(props = defaultProps) {
  return shallow(<CasesListView {...props} />);
}

test('CasesListView renders a ListTable', () => {
  const component = renderComponent();
  expect(component).toContain('ListTable');
});

test('CasesListView renders all headers for the table', () => {
  const component = renderComponent({ ...defaultProps });
  const tableHeaders = component.find('th');
  expect(tableHeaders.length).toEqual(12);
});

test('CasesListView renders 10 ghost cards if no cases provided', () => {
  const component = renderComponent();
  expect(component.find('ListRowLoading').length).toEqual(10);
});

test('CasesListView renders diff of cases if less than 10', () => {
  const favoritePagination = Map({ totalCount: 8 });
  const requesting = true;
  const component = renderComponent({ cases, favoritePagination, requesting });
  expect(component.find('ListRowLoading').length).toEqual(5);
});

test('Does not render ghosts when it is not requesting and currentCount is not zero', () => {
  const favoritePagination = Map({ totalCount: 8 });
  const requesting = false;
  const component = renderComponent({ cases, favoritePagination, requesting });
  expect(component).toNotContain('ListRowLoading');
});
