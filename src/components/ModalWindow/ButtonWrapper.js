import styled from 'styled-components';

const ButtonWrapper = styled.div`
  margin: 0 4px;
  display: inline-block;
`;

export default ButtonWrapper;
