import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

import Overlay from 'components/_common/Overlay';

import Divider from 'elements/Divider';

import Wrapper from './Wrapper';
import PopupWrapper from './PopupWrapper';
import HeaderWrapper from './HeaderWrapper';
import BodyWrapper from './BodyWrapper';
import FooterWrapper from './FooterWrapper';
import CloseButtonWrapper from './CloseButtonWrapper';

function ModalWindow({
  children,
  divider,
  headerInfo,
  hideByClickingBackground,
  hideModal,
  footerButtons,
}) {
  const handleBackgroundClick = (e) => {
    if (hideByClickingBackground && e.target === e.currentTarget) hideModal();
  };

  const showFooter = footerButtons && footerButtons.length > 0;

  return (
    <Wrapper>
      <Overlay onClick={handleBackgroundClick} />
      <PopupWrapper>
        {headerInfo &&
          <HeaderWrapper>
            {headerInfo.title}
            <CloseButtonWrapper onClick={hideModal}>
              <FontAwesome name="close" />
            </CloseButtonWrapper>
          </HeaderWrapper>}
        {divider && <Divider modifiers={['heavyMidGray']} />}
        <BodyWrapper id="modal-body">
          {children}
        </BodyWrapper>
        {showFooter && <Divider />}
        {showFooter &&
          <FooterWrapper>
            {footerButtons}
          </FooterWrapper>}
      </PopupWrapper>
    </Wrapper>
  );
}

ModalWindow.propTypes = {
  children: PropTypes.node,
  divider: PropTypes.bool,
  headerInfo: PropTypes.shape(),
  hideByClickingBackground: PropTypes.bool,
  hideModal: PropTypes.func.isRequired,
  footerButtons: PropTypes.arrayOf(PropTypes.node),
};

ModalWindow.defaultProps = {
  children: null,
  divider: false,
  headerInfo: null,
  footerButtons: [],
  hideByClickingBackground: false,
};

export default ModalWindow;
