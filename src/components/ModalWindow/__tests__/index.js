import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import ModalWindow from '../index';

const footerButton1 = <button key="1" />;
const defaultProps = {
  headerInfo: { title: 'Sample Modal' },
  hideModal: noop,
  footerButtons: [footerButton1],
};

function shallowRender(props = defaultProps) {
  return shallow(<ModalWindow {...props} />);
}

test('Renders an Overlay', () => {
  expect(shallowRender()).toContain('Overlay');
});

test('Renders the PopupWrapper', () => {
  expect(shallowRender()).toContain('PopupWrapper');
});

test('Renders the HeaderWrapper if headerInfo is set', () => {
  expect(shallowRender()).toContain('HeaderWrapper');
});

test('Does not render the HeaderWrapper if headerInfo is not set', () => {
  expect(shallowRender({ hideModal: noop })).toNotContain('HeaderWrapper');
});

test('Renders the close button', () => {
  expect(shallowRender()).toContain('CloseButtonWrapper');
});

test('Clicking the close button calls hideModal', () => {
  const hideModal = createSpy();
  const component = shallowRender({ ...defaultProps, hideModal });
  const closeButton = component.find('CloseButtonWrapper');
  closeButton.simulate('click');
  expect(hideModal).toHaveBeenCalled();
});

test('Renders the FooterWrapper', () => {
  expect(shallowRender()).toContain('FooterWrapper');
});

test('Does not render the FooterWrapper if no footerButtons are supplied', () => {
  expect(shallowRender({ hideModal: noop })).toNotContain('FooterWrapper');
});

test('Render Divider when prop divider is set true', () => {
  expect(shallowRender({ divider: true })).toContain('Divider');
});

test('Renders the Children', () => {
  const children = <div className="children">Children</div>;
  expect(shallowRender({ ...defaultProps, children })).toContain('div.children');
});

test('Clicking the background calls the hideModal handler if configured to do so', () => {
  const hideModal = createSpy();
  const hideByClickingBackground = true;
  const component = shallowRender({ ...defaultProps, hideByClickingBackground, hideModal });
  const overlay = component.find('Overlay');
  overlay.simulate('click', { target: overlay, currentTarget: overlay });
  expect(hideModal).toHaveBeenCalled();
});

test('Clicking the background does not call the hideModal handler by default', () => {
  const hideModal = createSpy();
  const component = shallowRender({ ...defaultProps, hideModal });
  const overlay = component.find('Overlay');
  overlay.simulate('click', { target: overlay, currentTarget: overlay });
  expect(hideModal).toNotHaveBeenCalled();
});
