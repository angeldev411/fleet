import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.background};
  border-radius: 2px;
  left: ${px2rem(26)};
  position: fixed;
  right: ${px2rem(26)};
  top: 50%;
  transform: translate(0, -50%);
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'PopupWrapper',
  styled.section,
  styles,
  { themePropTypes },
);
