import styled from 'styled-components';

const Wrapper = styled.section`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  z-index: 1000;
`;

export default Wrapper;
