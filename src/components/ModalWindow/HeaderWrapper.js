import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  box-sizing: border-box;
  color: ${props.theme.colors.base.textLight};
  font-size: ${px2rem(18)};
  font-weight: 500;
  line-height: ${px2rem(22)};
  padding: ${px2rem(16)};
  text-transform: uppercase;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'HeaderWrapper',
  styled.section,
  styles,
  { themePropTypes },
);
