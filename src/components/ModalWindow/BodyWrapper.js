import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const BodyWrapper = styled.div`
  max-height: calc(100vh - 100px);
  overflow: auto;
  padding: ${px2rem(8)};
  position: relative;
`;

export default BodyWrapper;
