import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const FooterWrapper = styled.section`
  font-size: ${px2rem(16)};
  text-align: right;
  padding: ${px2rem(20)};
`;

export default FooterWrapper;
