import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import Card from 'elements/Card';
import Divider from 'elements/Divider';

import AdditionalInfo from './AdditionalInfo';
import Complaint from './Complaint';
import ReasonForRepair from './ReasonForRepair';
import ServiceProvider from './ServiceProvider';
import ServiceRequestTitle from './ServiceRequestTitle';
import ServiceRequestVin from './ServiceRequestVin';

function ServiceRequestCard({ serviceRequestInfo, onSelect, isSelected }) {
  const cardModifier = isSelected ? 'selected' : 'selectable';

  return (
    <Card modifiers={[cardModifier]} onClick={onSelect}>
      <ServiceRequestTitle serviceRequestInfo={serviceRequestInfo} />
      <Divider modifiers={['gutter', 'heavy']} />
      <ServiceRequestVin serviceRequestInfo={serviceRequestInfo} />
      <Divider modifiers={['gutter', 'light', 'narrow']} />
      <ReasonForRepair serviceRequestInfo={serviceRequestInfo} />
      <Divider modifiers={['gutter', 'light', 'narrow']} />
      <ServiceProvider serviceRequestInfo={serviceRequestInfo} />
      <Divider modifiers={['gutter', 'light', 'narrow']} />
      <Complaint serviceRequestInfo={serviceRequestInfo} />
      <Divider modifiers={['gutter', 'light', 'narrow']} />
      <AdditionalInfo serviceRequestInfo={serviceRequestInfo} />
    </Card>
  );
}

ServiceRequestCard.propTypes = {
  serviceRequestInfo: ImmutablePropTypes.map.isRequired,
  onSelect: PropTypes.func.isRequired,
  isSelected: PropTypes.bool.isRequired,
};

export default ServiceRequestCard;
