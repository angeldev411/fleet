import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import { CardElement } from 'elements/Card';
import Link from 'elements/Link';
import { SplitBlock, SplitBlockElement, SplitBlockPart } from 'elements/SplitBlock';

import messages from './messages';

function ServiceRequestTitle({ serviceRequestInfo }) {
  const serviceRequestId = serviceRequestInfo.get('id');

  return (
    <CardElement>
      <SplitBlock pad="none">
        <SplitBlockPart modifiers={['left', 'wide']}>
          <SplitBlockElement modifiers={['padRight']}>
            <Link to={`/service-requests/${serviceRequestId}`} modifiers={['heavy', 'uppercase']}>
              <FormattedMessage {...messages.title} values={{ serviceRequestId }} />
            </Link>
          </SplitBlockElement>
        </SplitBlockPart>
      </SplitBlock>
    </CardElement>
  );
}

ServiceRequestTitle.propTypes = {
  serviceRequestInfo: ImmutablePropTypes.contains({
    serviceRequestId: PropTypes.string,
    severityColor: PropTypes.string,
    severityCount: PropTypes.string,
  }).isRequired,
};

export default ServiceRequestTitle;
