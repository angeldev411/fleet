import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import ServiceRequestTitle from '../ServiceRequestTitle';
import messages from '../messages';

const serviceRequestId = '123';
const serviceRequestInfo = fromJS({
  id: serviceRequestId,
  severityColor: 'green',
  severityCount: '10',
});

const defaultProps = {
  serviceRequestInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestTitle {...props} />);
}

test('renders a Link with the correct to prop', () => {
  const component = shallowRender();
  expect(component).toContain('Link');
  expect(component.find('Link')).toHaveProp('to', `/service-requests/${serviceRequestId}`);
});

test('renders a FormattedMessage with the expected props within the Link', () => {
  const component = shallowRender();
  expect(component).toContain('Link');
  const link = component.find('Link');
  expect(link.find('FormattedMessage').props()).toContain({
    ...messages.title,
    values: { serviceRequestId },
  });
});
