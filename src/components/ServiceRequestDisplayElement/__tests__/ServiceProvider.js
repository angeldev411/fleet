import React from 'react';
import { fromJS } from 'immutable';
import moment from 'moment';
import { camelCase } from 'lodash';

import { test, expect, shallow } from '__tests__/helpers/test-setup';
import { formatDate } from 'utils/timeUtils';

import ServiceProvider from '../ServiceProvider';

const serviceRequestInfo = fromJS({
  serviceProvider: {
    companyName: 'companyName',
  },
  requestedAt: moment().toISOString(),
  requestStatus: 'Approved',
});

const defaultProps = {
  serviceRequestInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceProvider {...props} />);
}

test('Renders a CardTable with 2 CardTableRows', () => {
  const component = shallowRender();
  expect(component).toContain('CardTable');
  expect(component.find('CardTable').find('CardTableRow').length).toEqual(2);
});

test('Renders companyName correctly', () => {
  const component = shallowRender();
  expect(component.find('TextDiv').render().text()).toContain(
    serviceRequestInfo.getIn(['serviceProvider', 'companyName']),
  );
});

test('Renders the StatusSpan with status modifier if requestStatus is supplied', () => {
  const requestStatus = serviceRequestInfo.get('requestStatus');
  const requestStatusModifier = requestStatus && camelCase(requestStatus);
  const component = shallowRender();

  expect(component).toContain('StatusSpan');
  expect(component.find('StatusSpan').props()).toContain({
    modifiers: [requestStatusModifier, 'bold', 'uppercase'],
  });
});

test('Renders the StatusSpan with no status modifier if no requestStatus is supplied', () => {
  const testServiceRequestInfo = serviceRequestInfo.set('requestStatus', undefined);
  const testProps = { serviceRequestInfo: testServiceRequestInfo };
  const component = shallowRender(testProps);

  expect(component).toContain('StatusSpan');
  expect(component.find('StatusSpan').props()).toContain({
    modifiers: ['bold', 'uppercase'],
  });
});

test('Renders requestedAt correctly', () => {
  const requestedAt = formatDate(serviceRequestInfo.get('requestedAt'));
  const component = shallowRender();
  const lastRow = component.find('CardTableRow').last();
  expect(lastRow.find('td').render().text()).toContain(requestedAt);
});
