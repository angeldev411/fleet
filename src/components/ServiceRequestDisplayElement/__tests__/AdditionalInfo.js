import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import AdditionalInfo from '../AdditionalInfo';

const followUpColor = 'red';
const followUpTime = '2017-01-11T16:46:22Z';
const unreadNotesCount = 10;
const serviceRequestInfo = fromJS({
  followUpColor,
  followUpTime,
  unreadNotesCount,
});

const defaultProps = {
  serviceRequestInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<AdditionalInfo {...props} />);
}

test('Renders a FollowUpLabel with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('FollowUpLabel');
  expect(component.find('FollowUpLabel')).toHaveProps({
    followUpColor,
    followUpTime,
  });
});
