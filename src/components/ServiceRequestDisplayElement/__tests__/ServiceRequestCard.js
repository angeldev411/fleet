/*
  This is just a placeholder until the service request cards are built.
  It should be replaced with a "real" test when the cards are implemented.
*/

import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import ServiceRequestCard from '../ServiceRequestCard';

const serviceRequestInfo = fromJS({
  id: '123',
});

const onSelect = expect.createSpy();

const isSelected = false;

const defaultProps = {
  serviceRequestInfo,
  onSelect,
  isSelected,
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestCard {...props} />);
}

test('Renders a CardView', () => {
  const component = shallowRender();
  expect(component).toBeA('Card');
});

test('Renders a Card with the expected props if the Card is not selected', () => {
  const component = shallowRender();
  const card = component.find('Card');
  expect(card.props().modifiers).toEqual(['selectable']);
});

test('Renders a Card with the expected props if the Card is selected', () => {
  const testProps = {
    ...defaultProps,
    isSelected: true,
  };
  const component = shallowRender(testProps);
  const card = component.find('Card');
  expect(card.props().modifiers).toEqual(['selected']);
});

test('Calls onSelect prop when the Card is clicked', () => {
  const component = shallowRender();
  component.simulate('click');
  expect(onSelect).toHaveBeenCalled();
});

test('Renders a ServiceRequestTitle with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('ServiceRequestTitle');
  expect(component.find('ServiceRequestTitle')).toHaveProps({
    serviceRequestInfo,
  });
});

test('Renders a ServiceRequestVin with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('ServiceRequestVin');
  expect(component.find('ServiceRequestVin')).toHaveProps({
    serviceRequestInfo,
  });
});

test('Renders a ReasonForRepair with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('ReasonForRepair');
  expect(component.find('ReasonForRepair')).toHaveProps({
    serviceRequestInfo,
  });
});

test('Renders a ServiceProvider with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('ServiceProvider');
  expect(component.find('ServiceProvider')).toHaveProps({
    serviceRequestInfo,
  });
});

test('Renders a Complaint with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('Complaint');
  expect(component.find('Complaint')).toHaveProps({
    serviceRequestInfo,
  });
});

test('Renders a AdditionalInfo with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AdditionalInfo');
  expect(component.find('AdditionalInfo')).toHaveProps({
    serviceRequestInfo,
  });
});
