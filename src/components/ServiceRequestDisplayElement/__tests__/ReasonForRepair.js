import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, mount, MountableTestComponent } from '__tests__/helpers/test-setup';

import messages from 'utils/messages';

import ReasonForRepair from '../ReasonForRepair';

const code = 'code';
const description = 'description';
const serviceRequestInfo = fromJS({
  reasonForRepair: {
    code,
    description,
  },
});

const defaultProps = {
  serviceRequestInfo,
};

function mountComponent(props = defaultProps) {
  return mount(
    <MountableTestComponent>
      <ReasonForRepair {...props} />
    </MountableTestComponent>,
  );
}

test('Renders correct code and description when given', () => {
  const component = mountComponent();
  const codeDescriptionText = component.find('TextDiv').last().render().text();
  expect(codeDescriptionText).toContain(`${code} - ${description}`);
});

test('Renders undefined code and description when undefined', () => {
  const testServiceRequestInfo = fromJS({
    reasonForRepair: {},
  });
  const component = mountComponent({ serviceRequestInfo: testServiceRequestInfo });
  const codeDescriptionText = component.find('TextDiv').last().render().text();
  expect(codeDescriptionText).toContain(messages.noValue.defaultMessage);
});
