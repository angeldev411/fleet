import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import ServiceRequestVin from '../ServiceRequestVin';

const vinNumber = '123';
const serviceRequestInfo = fromJS({
  vinNumber,
});

const defaultProps = {
  serviceRequestInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestVin {...props} />);
}

test('Renders vinNumber correctly', () => {
  const component = shallowRender();
  expect(component).toContain('CardTable');
  const td = component.find('td');
  expect(td.render().text()).toContain(vinNumber);
});
