import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import globalMessages from 'utils/messages';

import Complaint from '../Complaint';
import messages from '../messages';

const serviceRequestInfo = fromJS({
  complaint: {
    code: '101',
    description: 'This is a serious problem',
  },
  description: 'Engine makes funny noise',
});

const defaultProps = {
  serviceRequestInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<Complaint {...props} />);
}

test('Renders a Popover, PopoverTarget, and PopoverContent elements', () => {
  const component = shallowRender();
  expect(component).toContain('Popover'); // Because Popover is exported through another file
  expect(component).toContain('PopoverTarget');
  expect(component).toContain('PopoverContent');
});

test('Popover has prop showOnHover', () => {
  const component = shallowRender();
  const popover = component.find('Popover');
  expect(popover).toHaveProp('showOnHover', true);
});

test('PopoverTarget renders the complaint title, subtitle, and description', () => {
  const component = shallowRender();
  const popoverTarget = component.find('PopoverTarget');
  expect(popoverTarget.find('TextDiv').length).toEqual(3);

  const titleDiv = popoverTarget.find('TextDiv').first();
  expect(titleDiv).toContain('FormattedMessage');
  expect(titleDiv.find('FormattedMessage')).toHaveProps({
    ...messages.complaintTitle,
  });

  const subtitleDivText = popoverTarget.find('TextDiv').at(1).render().text();
  expect(subtitleDivText).toContain(serviceRequestInfo.getIn(['complaint', 'code']));
  expect(subtitleDivText).toContain(serviceRequestInfo.getIn(['complaint', 'description']));

  const descriptionDiv = popoverTarget.find('TextDiv').last();
  expect(descriptionDiv.render().text()).toContain(serviceRequestInfo.get('description'));
});

test('Complaint subtitle is default if complaint code is blank', () => {
  const testServiceRequestInfo = serviceRequestInfo.setIn(['complaint', 'code'], undefined);
  const testProps = { serviceRequestInfo: testServiceRequestInfo };
  const component = shallowRender(testProps);
  const subtitleTextDiv = component.find('PopoverTarget').find('TextDiv').at(1);
  expect(subtitleTextDiv.find('FormattedMessage')).toHaveProps(messages.undefinedComplaint);
});

test('Complaint subtitle is default if complaint code description is blank', () => {
  const testServiceRequestInfo = serviceRequestInfo.setIn(['description'], undefined);
  const testProps = { serviceRequestInfo: testServiceRequestInfo };
  const component = shallowRender(testProps);
  const serviceRequestDescriptionTextDiv = component.find('PopoverTarget').find('TextDiv').at(2);
  expect(serviceRequestDescriptionTextDiv.find('FormattedMessage')).toHaveProps(globalMessages.noValue);
});

test('PopoverContent renders the complaint subtitle and description', () => {
  const component = shallowRender();
  const popoverContent = component.find('PopoverContent');
  expect(popoverContent.find('TextDiv').length).toEqual(2);

  const subtitleDivText = popoverContent.find('TextDiv').first().render().text();
  expect(subtitleDivText).toContain(serviceRequestInfo.getIn(['complaint', 'code']));
  expect(subtitleDivText).toContain(serviceRequestInfo.getIn(['complaint', 'description']));

  const descriptionDiv = popoverContent.find('TextDiv').last();
  expect(descriptionDiv.render().text()).toContain(serviceRequestInfo.get('description'));
});
