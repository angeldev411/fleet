import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';

import FollowUpLabel from 'components/_common/FollowUpLabel';

import { CardElement } from 'elements/Card';
import { SplitBlock, SplitBlockElement, SplitBlockPart } from 'elements/SplitBlock';

function AdditionalInfo({ serviceRequestInfo }) {
  const followUpColor = serviceRequestInfo.get('followUpColor');
  const followUpTime = serviceRequestInfo.get('followUpTime');

  return (
    <CardElement>
      <SplitBlock pad="none">
        <SplitBlockPart modifiers={['left', 'wide']}>
          <SplitBlockElement>
            <FollowUpLabel
              followUpColor={followUpColor}
              followUpTime={followUpTime}
            />
          </SplitBlockElement>
        </SplitBlockPart>
      </SplitBlock>
    </CardElement>
  );
}

AdditionalInfo.propTypes = {
  serviceRequestInfo: ImmutablePropTypes.map.isRequired,
};

export default AdditionalInfo;
