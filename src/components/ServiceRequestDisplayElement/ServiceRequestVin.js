import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import { getOutputText } from 'utils/widget';

import {
  CardElement,
  CardTable,
  CardTableRow,
} from 'elements/Card';

import messages from './messages';

function ServiceRequestVin({ serviceRequestInfo }) {
  return (
    <CardElement>
      <CardTable modifiers={['uppercase']} tdModifiers={['active', 'leftAlign']}>
        <tbody>
          <CardTableRow type="topGap">
            <th>
              <FormattedMessage {...messages.vin} />
            </th>
            <td>
              {getOutputText(serviceRequestInfo.get('vinNumber'))}
            </td>
          </CardTableRow>
        </tbody>
      </CardTable>
    </CardElement>
  );
}

ServiceRequestVin.propTypes = {
  serviceRequestInfo: ImmutablePropTypes.contains({
    vinNumber: PropTypes.string,
  }).isRequired,
};

export default ServiceRequestVin;
