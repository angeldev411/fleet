import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import { getOutputText } from 'utils/widget';

import { CardElement } from 'elements/Card';
import TextDiv from 'elements/TextDiv';

import messages from './messages';

function ReasonForRepair({ serviceRequestInfo }) {
  const code = serviceRequestInfo.getIn(['reasonForRepair', 'code']);
  const description = serviceRequestInfo.getIn(['reasonForRepair', 'description']);
  const codeDescription = code && description ?
    `${code} - ${description}` : '';

  return (
    <CardElement>
      <TextDiv modifiers={['bold', 'bottomGap', 'smallText']}>
        <FormattedMessage {...messages.reasonForRepair} />
      </TextDiv>
      <TextDiv modifiers={['bottomGap', 'ellipsis', 'oneLine', 'smallText']}>
        {getOutputText(codeDescription)}
      </TextDiv>
    </CardElement>
  );
}

ReasonForRepair.propTypes = {
  serviceRequestInfo: ImmutablePropTypes.contains({
    reasonForRepair: ImmutablePropTypes.contains({
      code: PropTypes.string,
      description: PropTypes.string,
    }),
  }).isRequired,
};

export default ReasonForRepair;
