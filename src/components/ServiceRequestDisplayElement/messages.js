import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'components.ServiceRequestDisplayElement.title',
    defaultMessage: '{serviceRequestId}',
  },
  codeDescription: {
    id: 'components.ServiceRequestDisplayElement.ReasonForRepair.codeDescription',
    defaultMessage: 'Code - Description',
  },
  reasonForRepair: {
    id: 'components.ServiceRequestDisplayElement.ReasonForRepair.title',
    defaultMessage: 'Reason for Repair',
  },
  requested: {
    id: 'components.ServiceRequestDisplayElement.ServiceProvider.requested',
    defaultMessage: 'Requested',
  },
  serviceProvider: {
    id: 'components.ServiceRequestDisplayElement.ServiceProvider.title',
    defaultMessage: 'Service Provider',
  },
  status: {
    id: 'components.ServiceRequestDisplayElement.ServiceProvider.status',
    defaultMessage: 'Status',
  },
  complaint: {
    id: 'components.ServiceRequestDisplayElement.Complaint.complaint',
    defaultMessage: 'Complaint',
  },
  undefinedComplaint: {
    id: 'components.ServiceRequestDisplayElement.Complaint.undefined.complaint',
    defaultMessage: 'No Complaints Yet',
  },
  vin: {
    id: 'components.ServiceRequestDisplayElement.ServiceRequestVin.vin',
    defaultMessage: 'VIN',
  },
});

export default messages;
