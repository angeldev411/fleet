import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';
import { camelCase, compact } from 'lodash';

import { formatDate } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';

import {
  CardElement,
  CardTable,
  CardTableRow,
} from 'elements/Card';
import TextDiv from 'elements/TextDiv';
import StatusSpan from 'elements/StatusSpan';

import messages from './messages';

function ServiceProvider({ serviceRequestInfo }) {
  const companyName = serviceRequestInfo.getIn(['serviceProvider', 'companyName']);
  const requestedAt = formatDate(serviceRequestInfo.get('requestedAt'));
  const requestStatus = serviceRequestInfo.get('requestStatus');
  const requestStatusModifier = requestStatus && camelCase(requestStatus);
  const statusSpanModifiers = [requestStatusModifier, 'bold', 'uppercase'];

  return (
    <CardElement>
      <TextDiv modifiers={['active', 'bold', 'bottomGap', 'ellipsis', 'oneLine', 'smallText']}>
        {getOutputText(companyName)}
      </TextDiv>
      <CardTable thModifiers={['wide']} tdModifiers={['leftAlign', 'midGreyText']}>
        <tbody>
          <CardTableRow>
            <th>
              <FormattedMessage {...messages.status} />
            </th>
            <td colSpan="2">
              <StatusSpan modifiers={compact(statusSpanModifiers)}>
                {getOutputText(requestStatus)}
              </StatusSpan>
            </td>
          </CardTableRow>
          <CardTableRow type="topGap">
            <th><FormattedMessage {...messages.requested} /></th>
            <td colSpan="2">{getOutputText(requestedAt)}</td>
          </CardTableRow>
        </tbody>
      </CardTable>
    </CardElement>
  );
}

ServiceProvider.propTypes = {
  serviceRequestInfo: ImmutablePropTypes.contains({
    requestStatus: PropTypes.string,
    requestedAt: PropTypes.string,
    serviceProvider: ImmutablePropTypes.contains({
      companyName: PropTypes.string,
    }),
  }).isRequired,
};

export default ServiceProvider;
