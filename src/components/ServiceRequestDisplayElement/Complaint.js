import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import { getOutputText } from 'utils/widget';

import { CardElement } from 'elements/Card';
import Popover, {
  PopoverTarget,
  PopoverContent,
  ScrollingPopoverContentWrapper,
} from 'elements/Popover';

import TextDiv from 'elements/TextDiv';

import messages from './messages';

function Complaint({ serviceRequestInfo }) {
  // TODO: This logic could almost certainly be extracted to a utils function and shared between
  // here and the complaint widget.
  const complaintCode = serviceRequestInfo.getIn(['complaint', 'code']);
  const complaintDescription = serviceRequestInfo.getIn(['complaint', 'description']);
  const subtitle = complaintCode && complaintDescription
    ? `${complaintCode} - ${complaintDescription}`
    : '';
  const serviceRequestDescription = serviceRequestInfo.get('description');

  return (
    <CardElement>
      <Popover showOnHover>
        <PopoverTarget>
          <TextDiv modifiers={['bold', 'bottomGap', 'smallText']}>
            <FormattedMessage {...messages.complaint} />
          </TextDiv>
          <TextDiv modifiers={['bottomGap', 'heavyText', 'ellipsis', 'oneLine', 'smallText']}>
            {getOutputText(subtitle, messages.undefinedComplaint)}
          </TextDiv>
          <TextDiv modifiers={['ellipsis', 'midGreyText', 'short', 'smallText']}>
            {complaintCode && getOutputText(serviceRequestDescription)}
          </TextDiv>
        </PopoverTarget>
        <PopoverContent>
          <ScrollingPopoverContentWrapper modifiers={['smallText']}>
            <TextDiv modifiers={['bold', 'bright', 'bottomGap']}>
              {getOutputText(subtitle, messages.undefinedComplaint)}
            </TextDiv>
            <TextDiv modifiers={['bottomGap']}>
              {complaintCode && getOutputText(serviceRequestDescription)}
            </TextDiv>
          </ScrollingPopoverContentWrapper>
        </PopoverContent>
      </Popover>
    </CardElement>
  );
}

Complaint.propTypes = {
  serviceRequestInfo: ImmutablePropTypes.contains({
    complaint: PropTypes.shape({
      code: PropTypes.string,
      description: PropTypes.string,
    }),
    description: PropTypes.string,
  }).isRequired,
};

export default Complaint;
