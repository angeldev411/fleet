import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import SeverityLabel from 'components/SeverityLabel';

import { CardElement } from 'elements/Card';
import Link from 'elements/Link';
import { SplitBlock, SplitBlockElement, SplitBlockPart } from 'elements/SplitBlock';

import messages from './messages';

function AssetTitle({ assetInfo }) {
  const assetId = assetInfo.get('id');
  const unitNumber = assetInfo.get('unitNumber');

  return (
    <CardElement>
      <SplitBlock>
        <SplitBlockPart modifiers={['left', 'wide']}>
          <SplitBlockElement modifiers={['padRight']}>
            <Link to={`/assets/${assetId}`} modifiers={['heavy', 'uppercase']}>
              <FormattedMessage {...messages.title} values={{ unitNumber }} />
            </Link>
          </SplitBlockElement>
          <SplitBlockElement modifiers={['padLeft']}>
            <SeverityLabel
              color={assetInfo.get('severityColor')}
              value={Number(assetInfo.get('severityCount'))}
              small
            />
          </SplitBlockElement>
        </SplitBlockPart>
      </SplitBlock>
    </CardElement>
  );
}

AssetTitle.propTypes = {
  assetInfo: ImmutablePropTypes.contains({
    id: PropTypes.string,
    severityColor: PropTypes.string,
    severityCount: PropTypes.string,
  }).isRequired,
};

export default AssetTitle;
