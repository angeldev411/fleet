import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import { CardElement } from 'elements/Card';
import TextDiv from 'elements/TextDiv';

import BriefcaseIcon from 'components/BriefcaseIcon';

import messages from './messages';

function AssetOpenCases({ assetInfo }) {
  const hasOpenCases = assetInfo.get('hasActiveCases');
  const count = assetInfo.get('countActiveCases');

  if (hasOpenCases) {
    return (
      <CardElement modifiers={['flex']}>
        <BriefcaseIcon name="briefcase" />
        <TextDiv modifiers={['darkGreyText', 'smallText']}>
          <FormattedMessage {...messages.openCases} values={{ count }} />
        </TextDiv>
      </CardElement>
    );
  }
  return (
    <CardElement>
      <TextDiv modifiers={['smallText', 'lightText', 'italic', 'statusDefault']}>
        <FormattedMessage {...messages.noOpenCases} />
      </TextDiv>
    </CardElement>
  );
}

AssetOpenCases.propTypes = {
  assetInfo: ImmutablePropTypes.contains({
    countActiveCases: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]).isRequired,
    hasActiveCases: PropTypes.bool.isRequired,
  }).isRequired,
};

export default AssetOpenCases;
