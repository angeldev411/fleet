import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import AssetOpenCases from '../AssetOpenCases';
import messages from '../messages';

const assetInfo = fromJS({
  hasActiveCases: true,
  countActiveCases: '5',
});

const defaultProps = {
  assetInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetOpenCases {...props} />);
}

test('renders a CardElement with `flex` modifier if there are active cases', () => {
  const component = shallowRender();
  expect(component).toBeA('CardElement');
  expect(component.props().modifiers).toInclude('flex');
});

test('renders a briefcase icon if there are active cases', () => {
  const component = shallowRender();
  expect(component).toContain('BriefcaseIcon');
});

test('renders the correct text with correct count value if there are active cases', () => {
  const component = shallowRender();
  const formattedMessage = component.find('FormattedMessage');
  expect(formattedMessage).toHaveProps({ ...messages.openCases });
  expect(formattedMessage.props().values).toEqual({ count: '5' });
});

test('renders a CardElement without `flex` modifier if there are no active cases', () => {
  const testProps = {
    assetInfo: fromJS({
      hasActiveCases: false,
      countActiveCases: '0',
    }),
  };
  const component = shallowRender(testProps);
  expect(component).toBeA('CardElement');
  expect(component.props().modifiers).toNotExist();
});

test('renders the correct message if there are no active cases', () => {
  const testProps = {
    assetInfo: fromJS({
      hasActiveCases: false,
      countActiveCases: '0',
    }),
  };
  const component = shallowRender(testProps);
  expect(component.find('FormattedMessage')).toHaveProps({ ...messages.noOpenCases });
});
