import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import AssetStatus from '../AssetStatus';

const assetInfo = fromJS({
  campaignsStatus: 'red',
  pmStatus: 'green',
  warrantyStatus: 'yellow',
});

const defaultProps = {
  assetInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetStatus {...props} />);
}

test('AssetStatus should render a CardElement', () => {
  const component = shallowRender();
  expect(component).toBeA('CardElement');
});

test('AssetStatus should render a CardTable element with the expected props', () => {
  const component = shallowRender();
  const table = component.find('CardTable');
  expect(table.props().modifiers).toContain('capitalize');
  expect(table.props().tdModifiers).toEqual(['leftAlign']);
  expect(table.props().thModifiers).toContain('extraWide');
});

test('AssetStatus should render with three CardTableRows', () => {
  const component = shallowRender();
  const table = component.find('CardTable');
  expect(table.find('CardTableRow').length).toEqual(3);
});

test('AssetStatus should render the proper text if props are provided', () => {
  const component = shallowRender();
  const pmStatusText = component.find('StatusSpan').at(0).find('FormattedMessage');
  const warrantyStatusText = component.find('StatusSpan').at(1).find('FormattedMessage');
  const campaignStatusText = component.find('StatusSpan').at(2).find('FormattedMessage');
  expect(pmStatusText.props().defaultMessage).toEqual('current');
  expect(warrantyStatusText.props().defaultMessage).toEqual('partially covered');
  expect(campaignStatusText.props().defaultMessage).toEqual('campaign due');
});

test('AssetStatus should render placeholder text if asset info is dirty', () => {
  const badAssetInfo = fromJS({
    campaignsStatus: null,
    pmStatus: null,
    warrantyStatus: null,
  });

  const component = shallowRender({ ...defaultProps, assetInfo: badAssetInfo });
  const pmStatusText = component.find('td').at(0).find('FormattedMessage');
  const warrantyStatusText = component.find('td').at(1).find('FormattedMessage');
  const campaignStatusText = component.find('td').at(2).find('FormattedMessage');
  expect(pmStatusText.props().defaultMessage).toEqual('PM Status');
  expect(warrantyStatusText.props().defaultMessage).toEqual('Warranty');
  expect(campaignStatusText.props().defaultMessage).toEqual('Campaign');
});
