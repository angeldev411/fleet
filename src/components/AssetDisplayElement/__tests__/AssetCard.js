import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import AssetCard from '../AssetCard';

const assetInfo = fromJS({
  id: '123',
  hasActiveCases: true,
  countActiveCases: '5',
});

const onSelect = expect.createSpy();

const isSelected = false;

const defaultProps = {
  assetInfo,
  onSelect,
  isSelected,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetCard {...props} />);
}

test('AssetCard renders a CardView', () => {
  const component = shallowRender();
  expect(component).toBeA('Card');
});

test('AssetCard should render a Card with the expected props if the Card is not selected', () => {
  const component = shallowRender();
  const card = component.find('Card');
  expect(card.props().modifiers).toEqual(['selectable']);
});

test('AssetCard should render a Card with the expected props if the Card is selected', () => {
  const testProps = {
    ...defaultProps,
    isSelected: true,
  };
  const component = shallowRender(testProps);
  const card = component.find('Card');
  expect(card.props().modifiers).toEqual(['selected']);
});

test('AssetCard should call onSelect prop when a Card is clicked', () => {
  const component = shallowRender();
  const card = component.find('Card');
  card.simulate('click');
  expect(onSelect).toHaveBeenCalled();
});

test('AssetCard should render AssetTitle with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AssetTitle');
  expect(component.find('AssetTitle')).toHaveProp('assetInfo', assetInfo);
});

test('AssetCard should render AssetIds with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AssetIds');
  expect(component.find('AssetIds')).toHaveProp('assetInfo', assetInfo);
});

test('AssetCard should render AssetDetails with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AssetDetails');
  expect(component.find('AssetDetails')).toHaveProp('assetInfo', assetInfo);
});

test('AssetCard should render AssetStatus with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AssetStatus');
  expect(component.find('AssetStatus')).toHaveProp('assetInfo', assetInfo);
});

test('AssetCard should render AssetOpenCases with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AssetOpenCases');
  expect(component.find('AssetOpenCases')).toHaveProp('assetInfo', assetInfo);
});
