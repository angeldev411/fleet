import React from 'react';
import { fromJS } from 'immutable';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import AssetTitle from '../AssetTitle';
import messages from '../messages';

const assetInfo = fromJS({
  id: '123',
  severityColor: 'red',
  severityCount: '2',
});

const defaultProps = {
  assetInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetTitle {...props} />);
}

test('AssetTitle should render a CardElement', () => {
  const component = shallowRender();
  expect(component).toBeA('CardElement');
});

test('renders a Link with the correct to prop', () => {
  const component = shallowRender();
  expect(component).toContain('Link');
  expect(component.find('Link')).toHaveProp('to', `/assets/${assetInfo.get('id')}`);
});

test('renders a FormattedMessage with the expected props within the Link', () => {
  const component = shallowRender();
  const link = component.find('Link');
  expect(link).toContain('FormattedMessage');
  expect(link.find('FormattedMessage').props()).toContain({
    ...messages.title,
    values: {
      unitNumber: assetInfo.get('unitNumber'),
    },
  });
});

test('renders a SeverityLabel with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('SeverityLabel');
  expect(component.find('SeverityLabel').props()).toContain({
    color: assetInfo.get('severityColor'),
    value: Number(assetInfo.get('severityCount')),
    small: true,
  });
});
