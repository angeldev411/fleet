import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import messages from 'utils/messages';

import AssetDetails from '../AssetDetails';

const assetInfo = fromJS({
  equipmentStatus: '',
  make: 'Volvo',
  model: 'CH613',
  year: '2001',
  engine: 'E7',
  odometer: {
    reading: '1000000',
    unit: 'miles',
  },
});

const defaultProps = {
  assetInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetDetails {...props} />);
}

test('AssetDetails should render a CardElement', () => {
  const component = shallowRender();
  expect(component).toBeA('CardElement');
});

test('AssetDetails should render a CardTable element with the expected props', () => {
  const component = shallowRender();
  const table = component.find('CardTable');
  expect(table.props().modifiers).toContain('capitalize');
  expect(table.props().tdModifiers).toEqual(['leftAlign', 'midGreyText']);
  expect(table.props().thModifiers).toContain('extraWide');
});

test('AssetDetails should render with five CardTableRows', () => {
  const component = shallowRender();
  const table = component.find('CardTable');
  expect(table.find('CardTableRow').length).toEqual(5);
});

test('AssetDetails should render the proper text if props are provided', () => {
  const component = shallowRender();
  const makeText = component.find('td').at(0).text();
  const modelText = component.find('td').at(1).text();
  const yearText = component.find('td').at(2).text();
  const engineText = component.find('td').at(3).text();
  const odometerText = component.find('td').at(4).text();
  expect(makeText).toEqual('Volvo');
  expect(modelText).toEqual('CH613');
  expect(yearText).toEqual('2001');
  expect(engineText).toEqual('E7');
  expect(odometerText).toEqual('1,000,000 miles');
});

test('AssetDetails should render placeholder text if asset info is dirty', () => {
  const badAssetInfo = fromJS({
    make: null,
    model: null,
    year: null,
    engine: null,
    odometer: {},
  });

  const component = shallowRender({ ...defaultProps, assetInfo: badAssetInfo });
  const makeText = component.find('td').at(0).find('FormattedMessage');
  const modelText = component.find('td').at(1).find('FormattedMessage');
  const yearText = component.find('td').at(2).find('FormattedMessage');
  const engineText = component.find('td').at(3).find('FormattedMessage');
  const odometerText = component.find('td').at(4).find('FormattedMessage');
  expect(makeText.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
  expect(modelText.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
  expect(yearText.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
  expect(engineText.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
  expect(odometerText.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
});
