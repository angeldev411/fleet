import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import messages from 'utils/messages';

import AssetIds from '../AssetIds';

const assetInfo = fromJS({
  vinNumber: '1M1AA13Y42W146216',
});

const defaultProps = {
  assetInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetIds {...props} />);
}

test('AssetIds should render a CardElement', () => {
  const component = shallowRender();
  expect(component).toBeA('CardElement');
});

test('AssetIds should render a CardTable element with the expected props', () => {
  const component = shallowRender();
  const table = component.find('CardTable');
  expect(table.props().modifiers).toContain('uppercase');
  expect(table.props().tdModifiers).toEqual(['active', 'leftAlign']);
  expect(table.props().thModifiers).toContain('extraWide');
});

test('AssetIds should render with two CardTableRows', () => {
  const component = shallowRender();
  const table = component.find('CardTable');
  expect(table.find('CardTableRow').length).toEqual(2);
});

test('AssetIds should render the proper text if props are provided', () => {
  const component = shallowRender();
  const serialText = component.find('td').at(0).text();
  const vinNumberText = component.find('td').at(1).text();
  expect(serialText).toEqual('2W146216');
  expect(vinNumberText).toEqual('1M1AA13Y42W146216');
});

test('AssetIds should render placeholder text if asset info is dirty', () => {
  const vinNumber = null;
  const badAssetInfo = fromJS({ ...assetInfo, vinNumber });
  const component = shallowRender({ ...defaultProps, assetInfo: badAssetInfo });
  const serialText = component.find('td').at(0).find('FormattedMessage');
  const vinNumberText = component.find('td').at(1).find('FormattedMessage');
  expect(serialText.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
  expect(vinNumberText.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
});
