import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import messages from 'utils/messages';

import AssetListRow from '../AssetListRow';

const assetInfo = fromJS({
  id: '3071429',
  unitNumber: '1202',
  vinNumber: '1M1AA13Y42W146216',
  make: 'Mack',
  model: 'CH613',
  year: '2001',
  engine: 'E7',
  pmStatus: 'green',
  warrantyStatus: 'yellow',
  campaignsStatus: 'red',
  odometer: {
    reading: '887072',
    unit: 'miles',
  },
  severityColor: 'yellow',
  severityCount: '2',
});

const onSelect = expect.createSpy();

const isSelected = false;

const defaultProps = {
  assetInfo,
  onSelect,
  isSelected,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetListRow {...props} />);
}

test('AssetListRow passes the correct modifier to ListTable.Row if it is not selected', () => {
  const component = shallowRender();
  expect(component.props().modifiers).toContain('selectable');
});

test('AssetListRow passes the correct modifier to ListTable.Row if it is selected', () => {
  const testProps = {
    ...defaultProps,
    isSelected: true,
  };
  const component = shallowRender(testProps);
  expect(component.props().modifiers).toContain('selected');
});

test('call onSelect prop when the row is clicked', () => {
  const component = shallowRender();
  component.simulate('click');
  expect(onSelect).toHaveBeenCalled();
});

test('AssetListRow should render a Link with the correct props', () => {
  const component = shallowRender();
  const link = component.find('Link');
  expect(link).toHaveProp('to', `/assets/${assetInfo.get('id')}`);
});

test('AssetListRow should render a Link with the correct unitNumber', () => {
  const component = shallowRender();
  const linkText = component.find('Link').props().children;
  expect(linkText).toEqual(assetInfo.get('unitNumber'));
});

test('AssetListRow should render the default message if no unitNumber is given', () => {
  const badAssetInfo = fromJS({ unitNumber: '' });
  const component = shallowRender({ ...defaultProps, assetInfo: badAssetInfo });
  const link = component.find('Link').find('FormattedMessage');
  expect(link.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
});

test('AssetListRow should render the serial number properly', () => {
  const component = shallowRender();
  const serial = component.find('TextDiv').at(0);
  expect(serial.props().modifiers).toContain('bright');
  expect(serial.props().children).toEqual('2W146216');
});

test('AssetListRow should render the default message if no vin is given', () => {
  const badAssetInfo = fromJS({ vinNumber: '' });
  const component = shallowRender({ ...defaultProps, assetInfo: badAssetInfo });
  const vin = component.find('TextDiv').at(0).find('FormattedMessage');
  expect(vin.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
});

test('AssetListRow should render the vin number properly', () => {
  const component = shallowRender();
  const vin = component.find('TextDiv').at(1);
  expect(vin.props().modifiers).toContain('bright');
  expect(vin.props().children).toEqual('1M1AA13Y42W146216');
});

test('AssetListRow should render the default message if no vin is given', () => {
  const badAssetInfo = fromJS({ vinNumber: '' });
  const component = shallowRender({ ...defaultProps, assetInfo: badAssetInfo });
  const vin = component.find('TextDiv').at(1).find('FormattedMessage');
  expect(vin.props().defaultMessage).toEqual(messages.noValue.defaultMessage);
});

test('AssetListRow should render other detail information properly', () => {
  const component = shallowRender();
  const make = component.find('TextDiv').at(2);
  const model = component.find('TextDiv').at(3);
  const year = component.find('TextDiv').at(4);
  const engine = component.find('TextDiv').at(5);
  const odometer = component.find('TextDiv').at(6);
  const severity = component.find('SeverityLabel');
  expect(make.props().children).toEqual(assetInfo.get('make'));
  expect(model.props().children).toEqual(assetInfo.get('model'));
  expect(year.props().children).toEqual(assetInfo.get('year'));
  expect(engine.props().children).toEqual(assetInfo.get('engine'));
  expect(odometer.props().children).toEqual('887,072 miles');
  expect(severity.props()).toContain({
    color: assetInfo.get('severityColor'),
    value: assetInfo.get('severityCount'),
  });
});

test('AssetListRow should render default messages if props are not given', () => {
  const badAssetInfo = fromJS({});
  const component = shallowRender({ ...defaultProps, assetInfo: badAssetInfo });
  const make = component.find('TextDiv').at(2).find('FormattedMessage');
  const model = component.find('TextDiv').at(3).find('FormattedMessage');
  const year = component.find('TextDiv').at(4).find('FormattedMessage');
  const engine = component.find('TextDiv').at(5).find('FormattedMessage');
  const odometer = component.find('TextDiv').at(6).find('FormattedMessage');
  expect(make).toHaveProps({ ...messages.noValue });
  expect(model).toHaveProps({ ...messages.noValue });
  expect(year).toHaveProps({ ...messages.noValue });
  expect(engine).toHaveProps({ ...messages.noValue });
  expect(odometer).toHaveProps({ ...messages.noValue });
});

test('AssetListRow should render three status spans', () => {
  const component = shallowRender();
  expect(component.find('StatusSpan').length).toEqual(3);
});

test('AssetListRow should render pmStatus properly', () => {
  const component = shallowRender();
  const pmStatus = component.find('StatusSpan').at(0);
  expect(pmStatus.props().modifiers).toContain('bold', 'green', 'capitalize');
  const statusText = pmStatus.find('FormattedMessage').props().defaultMessage;
  expect(statusText).toEqual('current');
});

test('AssetListRow should render warrantyStatus properly', () => {
  const component = shallowRender();
  const warrantyStatus = component.find('StatusSpan').at(1);
  expect(warrantyStatus.props().modifiers).toContain(
    'bold',
    'yellow',
    'capitalize',
  );
  const statusText = warrantyStatus.find('FormattedMessage').props().defaultMessage;
  expect(statusText).toEqual('partially covered');
});

test('AssetListRow should render campaignStatus properly', () => {
  const component = shallowRender();
  const campaignStatus = component.find('StatusSpan').at(2);
  expect(campaignStatus.props().modifiers).toContain(
    'bold',
    'red',
    'capitalize',
  );
  const statusText = campaignStatus.find('FormattedMessage').props().defaultMessage;
  expect(statusText).toEqual('campaign due');
});

test('AssetListRow should render default status messages if props are not given', () => {
  const badAssetInfo = fromJS({
    ...assetInfo,
    pmStatus: '',
    warrantyStatus: '',
    campaignStatus: '',
  });
  const component = shallowRender({ ...defaultProps, assetInfo: badAssetInfo });
  const pmStatus = component.find('StatusSpan').at(0).find('FormattedMessage');
  const warrantyStatus = component.find('StatusSpan').at(1).find('FormattedMessage');
  const campaignStatus = component.find('StatusSpan').at(2).find('FormattedMessage');
  expect(pmStatus.props().defaultMessage).toEqual('PM Status');
  expect(warrantyStatus.props().defaultMessage).toEqual('Warranty');
  expect(campaignStatus.props().defaultMessage).toEqual('Campaign');
});
