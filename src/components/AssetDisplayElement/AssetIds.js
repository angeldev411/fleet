import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import { getSerialNumber } from 'utils/asset';
import { getOutputText } from 'utils/widget';

import {
  CardElement,
  CardTable,
  CardTableRow,
} from 'elements/Card';

import messages from './messages';

function AssetIds({ assetInfo }) {
  const rawVin = assetInfo.get('vinNumber');
  const vinNumber = getOutputText(rawVin);
  const serialNumber = getSerialNumber(rawVin);

  return (
    <CardElement>
      <CardTable
        modifiers={['uppercase']}
        tdModifiers={['active', 'leftAlign']}
        thModifiers={['extraWide']}
      >
        <tbody>
          <CardTableRow>
            <th><FormattedMessage {...messages.serial} /></th>
            <td>{serialNumber}</td>
          </CardTableRow>
          <CardTableRow type="topGap">
            <th><FormattedMessage {...messages.vin} /></th>
            <td>{vinNumber}</td>
          </CardTableRow>
        </tbody>
      </CardTable>
    </CardElement>
  );
}

AssetIds.propTypes = {
  assetInfo: ImmutablePropTypes.contains({
    vinNumber: PropTypes.string,
  }).isRequired,
};

export default AssetIds;
