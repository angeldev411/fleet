import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { compact } from 'lodash';

import SeverityLabel from 'components/SeverityLabel';

import Link from 'elements/Link';
import ListTable from 'elements/ListTable';
import StatusSpan from 'elements/StatusSpan';
import TextDiv from 'elements/TextDiv';

import {
  getAssetStatusText,
  getOdometerText,
  getSerialNumber,
} from 'utils/asset';
import { getOutputText } from 'utils/widget';

import messages from './messages';

function AssetListRow({
  assetInfo,
  onSelect,
  isSelected,
}) {
  // asset ids
  const assetId = assetInfo.get('id');
  const unitNumber = getOutputText(assetInfo.get('unitNumber'));
  const rawVin = assetInfo.get('vinNumber');
  const vinNumber = getOutputText(rawVin);
  const serialNumber = getSerialNumber(rawVin);
  // asset details
  const make = getOutputText(assetInfo.get('make'));
  const model = getOutputText(assetInfo.get('model'));
  const year = getOutputText(assetInfo.get('year'));
  const engine = getOutputText(assetInfo.get('engine'));
  const odometerReading = assetInfo.getIn(['odometer', 'reading']);
  const odometerUnit = assetInfo.getIn(['odometer', 'unit']);
  const odometerText = getOdometerText(odometerReading, odometerUnit);
  const odometer = getOutputText(odometerText);
  // asset status
  const campaignStatus = assetInfo.get('campaignsStatus');
  const pmStatus = assetInfo.get('pmStatus');
  const warrantyStatus = assetInfo.get('warrantyStatus');
  // asset severity info
  const severityColor = assetInfo.get('severityColor');
  const severityCount = Number(assetInfo.get('severityCount'));
  const rowModifier = isSelected ? 'selected' : 'selectable';

  return (
    <ListTable.Row
      modifiers={['hoverHighlight', 'lined', rowModifier]}
      onClick={onSelect}
    >
      <td>
        <Link
          to={`/assets/${assetId}`}
          modifiers={['heavy', 'uppercase']}
        >
          {unitNumber}
        </Link>
      </td>
      <td><TextDiv modifiers={['bright']}>{serialNumber}</TextDiv></td>
      <td><TextDiv modifiers={['bright']}>{vinNumber}</TextDiv></td>
      <td><TextDiv>{make}</TextDiv></td>
      <td><TextDiv>{model}</TextDiv></td>
      <td><TextDiv>{year}</TextDiv></td>
      <td><TextDiv>{engine}</TextDiv></td>
      <td><TextDiv>{odometer}</TextDiv></td>
      <td>
        <TextDiv>
          <StatusSpan modifiers={compact(['bold', pmStatus, 'capitalize'])}>
            {getAssetStatusText('pmStatus', pmStatus, messages.pmStatus)}
          </StatusSpan>
        </TextDiv>
      </td>
      <td>
        <TextDiv>
          <StatusSpan
            modifiers={compact(['bold', warrantyStatus, 'capitalize'])}
          >
            {getAssetStatusText(
              'warrantyStatus',
              warrantyStatus,
              messages.warrantyStatus,
            )}
          </StatusSpan>
        </TextDiv>
      </td>
      <td>
        <TextDiv>
          <StatusSpan
            modifiers={compact(['bold', campaignStatus, 'capitalize'])}
          >
            {getAssetStatusText(
              'campaignStatus',
              campaignStatus,
              messages.campaignStatus,
            )}
          </StatusSpan>
        </TextDiv>
      </td>
      <td>
        <SeverityLabel
          color={severityColor}
          value={severityCount}
          small
        />
      </td>
    </ListTable.Row>
  );
}

AssetListRow.propTypes = {
  assetInfo: ImmutablePropTypes.contains({
    id: PropTypes.string,
    unitNumber: PropTypes.string,
    vinNumber: PropTypes.string,
    make: PropTypes.string,
    model: PropTypes.string,
    year: PropTypes.string,
    engine: PropTypes.string,
    pmStatus: PropTypes.string,
    warrantyStatus: PropTypes.string,
    campaignsStatus: PropTypes.string,
    odometer: PropTypes.shape({
      reading: PropTypes.string,
      unit: PropTypes.string,
    }),
  }).isRequired,
  onSelect: PropTypes.func.isRequired,
  isSelected: PropTypes.bool.isRequired,
};

export default AssetListRow;
