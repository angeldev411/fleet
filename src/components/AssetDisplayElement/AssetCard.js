import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import Card from 'elements/Card';
import Divider from 'elements/Divider';

import AssetDetails from './AssetDetails';
import AssetIds from './AssetIds';
import AssetStatus from './AssetStatus';
import AssetTitle from './AssetTitle';
import AssetOpenCases from './AssetOpenCases';

function AssetCard({ assetInfo, onSelect, isSelected }) {
  const cardModifier = isSelected ? 'selected' : 'selectable';

  return (
    <Card modifiers={[cardModifier]} onClick={onSelect}>
      <AssetTitle assetInfo={assetInfo} />
      <Divider modifiers={['gutter', 'light']} />
      <AssetIds assetInfo={assetInfo} />
      <Divider modifiers={['gutter', 'light', 'narrow']} />
      <AssetDetails assetInfo={assetInfo} />
      <Divider modifiers={['gutter', 'light', 'narrow']} />
      <AssetStatus assetInfo={assetInfo} />
      <Divider modifiers={['gutter', 'light']} />
      <AssetOpenCases assetInfo={assetInfo} />
    </Card>
  );
}

AssetCard.propTypes = {
  assetInfo: ImmutablePropTypes.contains({
    id: PropTypes.string,
  }).isRequired,
  onSelect: PropTypes.func.isRequired,
  isSelected: PropTypes.bool.isRequired,
};

export default AssetCard;
