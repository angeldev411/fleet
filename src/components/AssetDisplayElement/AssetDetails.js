import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import { getOdometerText } from 'utils/asset';
import { getOutputText } from 'utils/widget';

import {
  CardElement,
  CardTable,
  CardTableRow,
} from 'elements/Card';

import messages from './messages';

function AssetDetails({ assetInfo }) {
  const make = getOutputText(assetInfo.get('make'));
  const model = getOutputText(assetInfo.get('model'));
  const year = getOutputText(assetInfo.get('year'));
  const engine = getOutputText(assetInfo.get('engine'));
  const odometerReading = assetInfo.getIn(['odometer', 'reading']);
  const odometerUnit = assetInfo.getIn(['odometer', 'unit']);
  const odometerText = getOdometerText(odometerReading, odometerUnit);
  const odometer = getOutputText(odometerText);

  return (
    <CardElement>
      <CardTable
        modifiers={['capitalize']}
        tdModifiers={['leftAlign', 'midGreyText']}
        thModifiers={['extraWide']}
      >
        <tbody>
          <CardTableRow>
            <th><FormattedMessage {...messages.make} /></th>
            <td>{make}</td>
          </CardTableRow>
          <CardTableRow type="topGap">
            <th><FormattedMessage {...messages.model} /></th>
            <td>{model}</td>
          </CardTableRow>
          <CardTableRow type="topGap">
            <th><FormattedMessage {...messages.year} /></th>
            <td>{year}</td>
          </CardTableRow>
          <CardTableRow type="topGap">
            <th><FormattedMessage {...messages.engine} /></th>
            <td>{engine}</td>
          </CardTableRow>
          <CardTableRow type="topGap">
            <th><FormattedMessage {...messages.odometer} /></th>
            <td>{odometer}</td>
          </CardTableRow>
        </tbody>
      </CardTable>
    </CardElement>
  );
}

AssetDetails.propTypes = {
  assetInfo: ImmutablePropTypes.contains({
    equipmentStatus: PropTypes.string,
    make: PropTypes.string,
    model: PropTypes.string,
    year: PropTypes.string,
    engine: PropTypes.string,
    odometer: PropTypes.shape({
      reading: PropTypes.string,
      unit: PropTypes.string,
    }),
  }).isRequired,
};

export default AssetDetails;
