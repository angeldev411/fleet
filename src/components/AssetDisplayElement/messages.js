import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  campaignStatus: {
    id: 'components.AssetDisplayElement.AssetStatus.campaignStatus',
    defaultMessage: 'Campaign',
  },
  engine: {
    id: 'components.AssetDisplayElement.AssetDetails.engine',
    defaultMessage: 'Engine',
  },
  make: {
    id: 'components.AssetDisplayElement.AssetDetails.make',
    defaultMessage: 'Make',
  },
  model: {
    id: 'components.AssetDisplayElement.AssetDetails.model',
    defaultMessage: 'Model',
  },
  noOpenCases: {
    id: 'components.AssetDisplayElement.AssetOpenCases.noOpenCases',
    defaultMessage: 'No Open Cases',
  },
  odometer: {
    id: 'components.AssetDisplayElement.AssetDetails.odometer',
    defaultMessage: 'Odometer',
  },
  openCases: {
    id: 'components.AssetDisplayElement.AssetOpenCases.openCases',
    defaultMessage: '{count} Open {count, plural, one {Case} other {Cases}}',
  },
  pmStatus: {
    id: 'components.AssetDisplayElement.AssetStatus.pmStatus',
    defaultMessage: 'PM Status',
  },
  serial: {
    id: 'components.AssetDisplayElement.AssetIds.serial',
    defaultMessage: 'Serial',
  },
  title: {
    id: 'components.AssetDisplayElement.AssetTitle.title',
    defaultMessage: 'Unit {unitNumber}',
  },
  unitNumber: {
    id: 'components.AssetDisplayElement.AssetListRow.unitNumber',
    defaultMessage: 'Unit',
  },
  vin: {
    id: 'components.AssetDisplayElement.AssetIds.vin',
    defaultMessage: 'VIN',
  },
  warrantyStatus: {
    id: 'components.AssetDisplayElement.AssetStatus.warrantyStatus',
    defaultMessage: 'Warranty',
  },
  year: {
    id: 'components.AssetDisplayElement.AssetDetails.year',
    defaultMessage: 'Year',
  },
});

export default formattedMessages;
