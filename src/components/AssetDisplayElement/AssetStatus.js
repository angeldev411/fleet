import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';
import { compact } from 'lodash';

import StatusSpan from 'elements/StatusSpan';

import { getAssetStatusText } from 'utils/asset';

import {
  CardElement,
  CardTable,
  CardTableRow,
} from 'elements/Card';

import messages from './messages';


function AssetStatus({ assetInfo }) {
  const campaignStatus = assetInfo.get('campaignsStatus');
  const pmStatus = assetInfo.get('pmStatus');
  const warrantyStatus = assetInfo.get('warrantyStatus');

  return (
    <CardElement>
      <CardTable
        modifiers={['capitalize']}
        tdModifiers={['leftAlign']}
        thModifiers={['extraWide']}
      >
        <tbody>
          <CardTableRow>
            <th><FormattedMessage {...messages.pmStatus} /></th>
            <td>
              <StatusSpan modifiers={compact(['bold', pmStatus, 'capitalize'])}>
                {getAssetStatusText('pmStatus', pmStatus, messages.pmStatus)}
              </StatusSpan>
            </td>
          </CardTableRow>

          <CardTableRow type="topGap">
            <th><FormattedMessage {...messages.warrantyStatus} /></th>
            <td>
              <StatusSpan modifiers={compact(['bold', warrantyStatus, 'capitalize'])}>
                {getAssetStatusText('warrantyStatus', warrantyStatus, messages.warrantyStatus)}
              </StatusSpan>
            </td>
          </CardTableRow>

          <CardTableRow type="topGap">
            <th><FormattedMessage {...messages.campaignStatus} /></th>
            <td>
              <StatusSpan modifiers={compact(['bold', campaignStatus, 'capitalize'])}>
                {getAssetStatusText('campaignStatus', campaignStatus, messages.campaignStatus)}
              </StatusSpan>
            </td>
          </CardTableRow>
        </tbody>
      </CardTable>
    </CardElement>
  );
}

AssetStatus.propTypes = {
  assetInfo: ImmutablePropTypes.contains({
    campaignsStatus: PropTypes.string,
    pmStatus: PropTypes.string,
    warrantyStatus: PropTypes.string,
  }).isRequired,
};

export default AssetStatus;
