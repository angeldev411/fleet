import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  hovered: ({ theme }) => ({
    styles: `
      border-color: ${theme.colors.base.chrome600};
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  align-items: center;
  background-color: ${props.theme.colors.base.background};
  border: 1px solid ${props.theme.colors.base.chrome400};
  border-radius: ${px2rem(25)};
  display: flex;
  height: ${px2rem(25)};
  margin: ${px2rem(0)} ${px2rem(10)};
  width: ${px2rem(25)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
      chrome400: PropTypes.string.isRequired,
      chrome600: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Circle',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
