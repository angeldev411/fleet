import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  align-items: center;
  display: flex;
  position: absolute;
  top: ${px2rem(4)};
  right: ${px2rem(4)};

  span {
    font-size: ${px2rem(10)};
    color: ${props.theme.colors.base.textLight};
  }

  .fa-keyboard-o {
    color: ${props.theme.colors.base.chrome500};
    font-size: ${px2rem(12)};
    margin-right: ${px2rem(4)};
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome500: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'InputIcon',
  styled.div,
  styles,
  { themePropTypes },
);
