import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.status.info};
  border-radius: ${px2rem(11)};
  height: ${px2rem(11)};
  margin: auto;
  width: ${px2rem(11)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    status: PropTypes.shape({
      info: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'SmallCircle',
  styled.div,
  styles,
  { themePropTypes },
);
