import React from 'react';
import {
  test,
  expect,
  shallow,
  noop,
  createSpy,
} from '__tests__/helpers/test-setup';

import TypeSelect from '../index';

const onSelect = noop;
const isSelected = true;
const typeName = 'maintenance';

const defaultProps = {
  onSelect,
  isSelected,
  selectKey: '1',
  typeName,
};

function shallowRender(props = defaultProps) {
  return shallow(<TypeSelect {...props} />);
}

test('renders Wrapper with the expected props', () => {
  const component = shallowRender();
  const wrapper = component.find('Wrapper');
  expect(wrapper.props().modifiers).toContain(['selected']);
});

test('renders Circle with the expected props', () => {
  const testProps = {
    ...defaultProps,
    isSelected: false,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.setState({ isHovered: true });
  const circle = component.find('Circle');
  expect(circle.props().modifiers).toContain(['hovered']);
});

test('renders SmallCircle if the input is selected', () => {
  const component = shallowRender();
  expect(component).toContain('SmallCircle');
});

test('renders TypeName with the expected text', () => {
  const component = shallowRender();
  const typeNameComponent = component.find('TypeName');
  expect(typeNameComponent.props().children).toContain(typeName);
});

test('renders keyboard icon', () => {
  const component = shallowRender();
  expect(component).toContain('FontAwesome');
  const fontAwesome = component.find('FontAwesome');
  expect(fontAwesome).toHaveProp('name', 'keyboard-o');
});

test('onKeyDown with number performs selection', () => {
  const handleSelect = createSpy();
  const testProps = {
    ...defaultProps,
    onSelect: handleSelect,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.keydownEventHandler({ key: '1' });
  expect(handleSelect).toHaveBeenCalled();
});
