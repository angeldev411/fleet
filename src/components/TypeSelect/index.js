import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import { compact } from 'lodash';

import Circle from './Circle';
import InputIcon from './InputIcon';
import SmallCircle from './SmallCircle';
import TypeName from './TypeName';
import Wrapper from './Wrapper';

class TypeSelect extends Component {
  state = {
    isHovered: false,
  }

  componentDidMount() {
    window.addEventListener('keydown', this.keydownEventHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.keydownEventHandler);
  }

  onMouseEnter = () => {
    this.setState({ isHovered: true });
  }

  onMouseLeave = () => {
    this.setState({ isHovered: false });
  }

  keydownEventHandler = ({ key }) => {
    const { selectKey, onSelect } = this.props;

    if (key === selectKey) {
      onSelect();
    }
  }

  render() {
    const { onSelect, isSelected, selectKey, typeName } = this.props;
    const { isHovered } = this.state;

    return (
      <Wrapper
        modifiers={compact([isSelected && 'selected'])}
        onClick={onSelect}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Circle modifiers={compact([!isSelected && isHovered && 'hovered'])}>
          {isSelected && <SmallCircle />}
        </Circle>
        <TypeName>{typeName}</TypeName>
        <InputIcon><FontAwesome name="keyboard-o" /><span>{selectKey}</span></InputIcon>
      </Wrapper>
    );
  }
}

TypeSelect.propTypes = {
  selectKey: PropTypes.string.isRequired,
  isSelected: PropTypes.bool,
  onSelect: PropTypes.func.isRequired,
  typeName: PropTypes.node.isRequired,
};

TypeSelect.defaultProps = {
  isSelected: false,
};

export default TypeSelect;

