import React from 'react';
import PropTypes from 'prop-types';

import { SEVERITY_COLORS } from './constants';
import Wrapper from './Wrapper';
import SeverityIcon from './SeverityIcon';

/**
 * Presents a severity indicator based on a numeric severity
 * level (typically 0-3) and a color (red, orange, yellow, etc)
 */
function SeverityLabel({
  color,
  value,
  fontSize,
  name,
  small,
}) {
  // per UI-215, a green/zero severity is "no" severity.
  if (color === 'green' && !value) {
    return <div />;
  }

  return (
    <Wrapper fontSize={fontSize} small={small}>
      <SeverityIcon
        color={color}
        value={value}
        small={small}
      />
      {name && <span>{name}</span>}
    </Wrapper>
  );
}

SeverityLabel.propTypes = {
  color: PropTypes.oneOf(SEVERITY_COLORS).isRequired,
  value: PropTypes.number,
  fontSize: PropTypes.number,
  name: PropTypes.string,
  small: PropTypes.bool,
};

SeverityLabel.defaultProps = {
  color: 'grey',
  fontSize: 14,
  name: '',
  small: false,
  value: null,
};

export default SeverityLabel;
