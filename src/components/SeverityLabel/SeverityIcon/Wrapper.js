import styled from 'styled-components';

const SeverityIconWrapper = styled.div`
  position: relative;
  display: inline-block;
  vertical-align: middle;
`;

export default SeverityIconWrapper;
