import React from 'react';
import { theme } from 'decisiv-ui-utils';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import { withTestTheme } from 'utils/styles';

import { SeverityIcon, getStatusColor } from '../index';

const severityValue = 2;
const severityColor = 'yellow';
const severitySmall = true;

const defaultProps = {
  color: severityColor,
  value: severityValue,
  small: severitySmall,
};

const SeverityIconWithTheme = withTestTheme(SeverityIcon);

function shallowRender(props = defaultProps) {
  return shallow(<SeverityIconWithTheme {...props} />);
}

test('Passes `small` prop to SeverityIconValue', () => {
  const wrapper = shallowRender({ ...defaultProps, small: true });
  const value = wrapper.find('SeverityIconValue');
  expect(value).toHaveProp('small', true);
});

test('Includes the warning icon with the status color', () => {
  const wrapper = shallowRender({ ...defaultProps, color: 'orange' });
  expect(wrapper).toContain(`IconWarning[color="${theme.colors.status.warning}"]`);
});

test('Includes the icon value with the given color', () => {
  const wrapper = shallowRender({ ...defaultProps, theme, color: 'yellow' });
  expect(wrapper).toContain('SeverityIconValue[color="yellow"]');
});

test('Renders the given value text', () => {
  const wrapper = shallowRender({ ...defaultProps, value: 3 });
  expect(wrapper.find('SeverityIconValue').render().text()).toEqual('3');
});

// ---------------------------- getStatusColor -----------------------------------

test('getStatusColor returns the info status color', () => {
  expect(getStatusColor({ theme, color: 'deepskyblue' })).toEqual(theme.colors.status.info);
});

test('getStatusColor returns the red status color', () => {
  expect(getStatusColor({ theme, color: 'red' })).toEqual(theme.colors.status.danger);
});

test('getStatusColor returns the yellow status color', () => {
  expect(getStatusColor({ theme, color: 'yellow' })).toEqual(theme.colors.status.warning);
});

test('getStatusColor returns the orange status color', () => {
  expect(getStatusColor({ theme, color: 'orange' })).toEqual(theme.colors.status.warning);
});

test('getStatusColor returns the green status color', () => {
  expect(getStatusColor({ theme, color: 'green' })).toEqual(theme.colors.status.success);
});

test('getStatusColor returns status grey by default', () => {
  expect(getStatusColor({ theme, color: 'grey' })).toEqual(theme.colors.status.default);
});

test('getStatusColor returns status grey by default', () => {
  expect(getStatusColor({ theme, color: 'nonsense' })).toEqual(theme.colors.status.default);
});
