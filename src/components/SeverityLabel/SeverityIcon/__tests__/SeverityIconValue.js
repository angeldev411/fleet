import { test, expect } from '__tests__/helpers/test-setup';

import { valueTextColor } from '../SeverityIconValue';

const theme = {
  colors: {
    base: {
      text: 'text',
      background: 'white',
    },
  },
};

test('valueTextColor returns the expected default color', () => {
  const themeValues = valueTextColor({ theme });
  expect(themeValues).toEqual(theme.colors.base.text);
});

const map = {
  deepskyblue: 'background',
  orange: 'background',
  red: 'background',
  yellow: 'text',
  grey: 'background',
  green: 'background',
};

Object.entries(map).forEach(([key, val]) => {
  test(`valueTextColor returns the expected text color for background color '${key}'`, () => {
    const textColor = valueTextColor({ theme, color: key });
    expect(textColor).toEqual(theme.colors.base[val]);
  });
});
