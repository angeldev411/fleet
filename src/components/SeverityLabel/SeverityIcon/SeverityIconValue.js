import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import { SEVERITY_COLORS } from '../constants';

export function valueTextColor({ theme, color: backgroundColor }) {
  switch (backgroundColor) {
    case 'deepskyblue':
    case 'orange':
    case 'red':
    case 'grey':
    case 'green':
      return theme.colors.base.background;
    default:
      return theme.colors.base.text;
  }
}

/* istanbul ignore next */
const styles = props => `
  color: ${valueTextColor(props)};
  font-size: ${props.small ? px2rem(10) : px2rem(12)};
  left: 50%;
  position: absolute;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  color: PropTypes.oneOf(SEVERITY_COLORS).isRequired,
  small: PropTypes.bool,
};

const defaultProps = {
  small: false,
};

export default buildStyledComponent(
  'SeverityIconValue',
  styled.div,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
