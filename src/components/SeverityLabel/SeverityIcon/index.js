import { isNaN } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { withTheme } from 'styled-components';

import IconWarning from 'elements/IconWarning';

import { SEVERITY_COLORS } from '../constants';
import Wrapper from './Wrapper';
import SeverityIconValue from './SeverityIconValue';

export function getStatusColor({ color, theme }) {
  switch (color) {
    case 'deepskyblue':
      return theme.colors.status.info;
    case 'red':
      return theme.colors.status.danger;
    case 'orange':
    case 'yellow':
      return theme.colors.status.warning;
    case 'green':
      return theme.colors.status.success;
    case 'grey':
    default:
      return theme.colors.status.default;
  }
}

export function SeverityIcon({ color, value, theme, small }) {
  return (
    <Wrapper>
      <IconWarning color={getStatusColor({ color, theme })} />
      <SeverityIconValue color={color} small={small}>
        {isNaN(value) ? 0 : value}
      </SeverityIconValue>
    </Wrapper>
  );
}

SeverityIcon.propTypes = {
  color: PropTypes.oneOf(SEVERITY_COLORS).isRequired,
  value: PropTypes.number,
  theme: PropTypes.shape({
    colors: PropTypes.shape({
      status: PropTypes.shape({
        danger: PropTypes.string.isRequired,
        default: PropTypes.string.isRequired,
        info: PropTypes.string.isRequired,
        success: PropTypes.string.isRequired,
        warning: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
  small: PropTypes.bool,
};

SeverityIcon.defaultProps = {
  color: 'grey',
  small: false,
  value: null,
};

export default withTheme(SeverityIcon);
