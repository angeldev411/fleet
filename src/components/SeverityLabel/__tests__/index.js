import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import SeverityLabel from '../index';
import SeverityIcon from '../SeverityIcon';

const testColor = 'grey';
const testValue = 0;
const testName = '';
const testSmall = true;

function renderComponent({
  color = testColor,
  value = testValue,
  name = testName,
  small = testSmall,
} = {}) {
  return shallow(<SeverityLabel color={color} value={value} name={name} small={small} />);
}

test('includes a SeverityIcon', () => {
  const wrapper = renderComponent();
  expect(wrapper).toContain(SeverityIcon);
});

test('passes color, value and small props to SeverityIcon', () => {
  const wrapper = renderComponent({ color: 'orange', value: 3, small: true });
  const icon = wrapper.find(SeverityIcon);
  expect(icon).toHaveProp('color', 'orange');
  expect(icon).toHaveProp('value', 3);
  expect(icon).toHaveProp('small', true);
});

test('Includes the name in the label string', () => {
  const wrapper = renderComponent({ name: 'Severe' });
  expect(wrapper.find('span').render().text()).toInclude('Severe');
});

test('Only show the icon when the name prop is empty', () => {
  const wrapper = renderComponent({ name: '' });
  expect(wrapper.children().last()).toBeA(SeverityIcon);
});

test('SeverityLabel returns an empty div if given green/zero', () => {
  const severityLabel = renderComponent({ color: 'green', value: 0 });
  expect(severityLabel).toBeA('div');
  expect(severityLabel.children().length).toEqual(0);
});

test('SeverityLabel returns an empty div if given green/null', () => {
  const severityLabel = renderComponent({ color: 'green', value: null });
  expect(severityLabel).toBeA('div');
  expect(severityLabel.children().length).toEqual(0);
});
