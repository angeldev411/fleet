/* eslint-disable import/prefer-default-export */
export const SEVERITY_COLORS = [
  'grey',
  'yellow',
  'red',
  'green',
  'orange',
  'deepskyblue', // TODO: determine if this color name should be shown in the UI?
  'unknown', // TODO: evaluate why this color is being rendered, as in `/cases/6811575`
];
