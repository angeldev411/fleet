import PropTypes from 'prop-types';
import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const Wrapper = styled.div`
  position: relative;
  text-transform: uppercase;
  display: inline-block;
  align-items: center;
  font-size: ${props => (props.small ? px2rem(13) : px2rem(props.fontSize))};
  height: ${props => (props.small ? px2rem(19) : 'auto')};
  span {
    padding-left: 0.5rem;
  }
`;

Wrapper.propTypes = {
  fontSize: PropTypes.number,
  small: PropTypes.bool,
};

Wrapper.defaultProps = {
  fontSize: 14,
  small: false,
};

export default Wrapper;
