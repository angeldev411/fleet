import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

export function setAnimation({ requesting, theme }) {
  return requesting ? `${theme.animations.spin} 0.8s linear infinite` : 'none';
}

export function setColor({ requesting, theme }) {
  return requesting ? theme.colors.base.linkHover : theme.colors.base.chrome500;
}

/* istanbul ignore next */
const styles = props => `
  align-items: center;
  color: ${setColor(props)};
  display: flex;
  font-size: 14px;
  text-align: center;

  &:hover {
    color: ${props.theme.colors.base.linkHover};
    cursor: pointer;
  }

  .fa {
    animation: ${setAnimation(props)};
  }
`;

const themePropTypes = {
  animations: PropTypes.shape({
    spin: PropTypes.string.isRequired,
  }).isRequired,
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome500: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  requesting: PropTypes.bool,
};

export default buildStyledComponent(
  'RefreshIcon',
  styled.div,
  styles,
  { propTypes, themePropTypes },
);
