import { noop, omit } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import RefreshBlock from '../index';
import messages from '../messages';

const defaultProps = {
  type: 'search',
  count: '3',
  refresh: noop,
  requesting: false,
};

function shallowRender(props = defaultProps) {
  return shallow(<RefreshBlock {...props} />);
}

test('renders Count with expected FormattedMessage', () => {
  const component = shallowRender();
  expect(component).toContain('Count');
  const count = component.find('Count');
  expect(count).toContain('FormattedMessage');
  expect(count.find('FormattedMessage').props()).toContain({
    ...messages.matches,
    values: {
      count: defaultProps.count,
    },
  });
});

test('renders Count message with "-" if no value provided', () => {
  const component = shallowRender(omit(defaultProps, 'count'));
  const count = component.find('Count');
  expect(count).toContain('FormattedMessage');
  expect(count.find('FormattedMessage').props()).toContain({
    ...messages.matches,
    values: {
      count: '-',
    },
  });
});

test('does not render Count message if requesting = true', () => {
  const component = shallowRender({
    ...defaultProps,
    requesting: true,
  });
  const count = component.find('Count');
  expect(count).toNotContain('FormattedMessage');
});

test('renders RefreshIcon with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('RefreshIcon');
  const icon = component.find('RefreshIcon');
  expect(icon).toHaveProps({
    requesting: defaultProps.requesting,
    onClick: component.handleRefreshClick,
  });
  expect(icon).toContain('FontAwesome');
  expect(icon.find('FontAwesome')).toHaveProp('name', 'refresh');
});

test('clicking RefreshIcon calls refresh prop', () => {
  const preventDefault = createSpy();
  const refresh = createSpy();
  const component = shallowRender({
    ...defaultProps,
    refresh,
  });
  component.find('RefreshIcon').simulate('click', { preventDefault });
  expect(preventDefault).toHaveBeenCalled();
  expect(refresh).toHaveBeenCalled();
});

test('clicking RefreshIcon does not call refresh prop if requesting = true', () => {
  const preventDefault = createSpy();
  const refresh = createSpy();
  const component = shallowRender({
    ...defaultProps,
    refresh,
    requesting: true,
  });
  component.find('RefreshIcon').simulate('click', { preventDefault });
  expect(preventDefault).toHaveBeenCalled();
  expect(refresh).toNotHaveBeenCalled();
});
