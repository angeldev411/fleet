import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import { setAnimation, setColor } from '../RefreshIcon';

const linkHover = 'linkHover';
const chrome500 = 'chrome500';

const theme = {
  animations: {
    spin: 'weeeeee!!!!',
  },
  colors: {
    base: {
      chrome500,
      linkHover,
    },
  },
};


// -------------------- setAnimation --------------------

test('setAnimation returns no animation if not requesting data', () => {
  expect(setAnimation({ requesting: false, theme })).toEqual('none');
});

test('setAnimation returns the expected animation if requesting data', () => {
  expect(setAnimation({ requesting: true, theme })).toInclude('linear infinite');
});

// -------------------- setColor --------------------

test('setColor returns linkHover when requesting data', () => {
  expect(setColor({ requesting: true, theme })).toEqual(linkHover);
});

test('setColor returns grey when not requesting data', () => {
  expect(setColor({ requesting: false, theme })).toEqual(chrome500);
});
