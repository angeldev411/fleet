import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  assets: {
    id: 'components.CategoryHeader.data.assets',
    defaultMessage: 'Assets: {count}',
  },
  cases: {
    id: 'components.CategoryHeader.data.cases',
    defaultMessage: 'Cases: {count}',
  },
  matches: {
    id: 'components.CategoryHeader.data.matches',
    defaultMessage: 'Matches: {count}',
  },
  serviceRequests: {
    id: 'components.CategoryHeader.data.serviceRequests',
    defaultMessage: 'Service Requests',
  },
});

export default formattedMessages;
