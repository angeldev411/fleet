import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage } from 'react-intl';

import Count from './Count';
import messages from './messages';
import RefreshBlockWrapper from './RefreshBlockWrapper';
import RefreshIcon from './RefreshIcon';

function getMessage(type, count) {
  switch (type) {
    case 'asset':
      return <FormattedMessage {...messages.assets} values={{ count }} />;
    case 'case':
      return <FormattedMessage {...messages.cases} values={{ count }} />;
    case 'search':
      return <FormattedMessage {...messages.matches} values={{ count }} />;
    default:
      return '';
  }
}

function RefreshBlock({
  count,
  refresh,
  requesting,
  type,
}) {
  const message = getMessage(type, count);

  function renderCount() {
    if (requesting) {
      return null;
    }
    return <Count>{message}</Count>;
  }

  function handleRefreshClick(event) {
    event.preventDefault();
    if (requesting) {
      return null;
    }
    return refresh();
  }

  return (
    <RefreshBlockWrapper id="refresh-block">
      {renderCount()}
      <RefreshIcon
        requesting={requesting}
        onClick={handleRefreshClick}
      >
        <FontAwesome name="refresh" />
      </RefreshIcon>
    </RefreshBlockWrapper>
  );
}

RefreshBlock.propTypes = {
  count: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  refresh: PropTypes.func.isRequired,
  requesting: PropTypes.bool.isRequired,
  type: PropTypes.string,
};

RefreshBlock.defaultProps = {
  count: '-',
  type: undefined,
};

export default RefreshBlock;
