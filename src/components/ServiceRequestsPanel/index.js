import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { messageDescriptorPropTypes } from 'react-intl';

import ServiceRequestDisplayContainer from 'containers/ServiceRequestDisplayContainer';

import CategoryHeader from 'components/CategoryHeader';
import paginateFavorites from 'components/_common/paginateFavorites';
import CardView from 'components/_common/CardView';

import PageContentPanel from 'elements/PageContentPanel';

import ServiceRequestsListView from './ServiceRequestsListView';
import ServiceRequestsMapView from './ServiceRequestsMapView';

function getServiceRequestsView(serviceRequestsView) {
  switch (serviceRequestsView) {
    case 'list':
      return ServiceRequestsListView;
    case 'map':
      return ServiceRequestsMapView;
    case 'card':
    default:
      return CardView;
  }
}

class ServiceRequestsPanel extends Component {
  static propTypes = {
    serviceRequests: ImmutablePropTypes.list.isRequired,
    serviceRequestsRequesting: PropTypes.bool,
    serviceRequestsView: PropTypes.string.isRequired,
    favoritePagination: ImmutablePropTypes.map.isRequired,
    latestFavorite: PropTypes.shape({
      message: PropTypes.shape(messageDescriptorPropTypes),
    }),
    refreshServiceRequests: PropTypes.func.isRequired,
    requestNext: PropTypes.func.isRequired,
  };

  static defaultProps = {
    serviceRequestsRequesting: false,
    latestFavorite: {},
  };

  componentWillMount() {
    this.PaginatedServiceRequestsView =
      this.buildPaginatedServiceRequestsView(this.props.serviceRequestsView);
  }

  componentWillReceiveProps(nextProps) {
    // If the users has changed view types, a new HOC wrapped component must be made.
    if (this.props.serviceRequestsView !== nextProps.serviceRequestsView) {
      this.PaginatedServiceRequestsView =
        this.buildPaginatedServiceRequestsView(nextProps.serviceRequestsView);
    }
  }

  buildPaginatedServiceRequestsView = (viewType) => {
    // define the component that will be displayed with the super pagination powers.
    const ServiceRequestsView = getServiceRequestsView(viewType);

    // add methods concerning pagination
    const paginationMethods = {
      requestNext: this.props.requestNext,
    };

    return paginateFavorites(paginationMethods)(ServiceRequestsView);
  }

  render() {
    const {
      serviceRequests,
      serviceRequestsRequesting,
      refreshServiceRequests,
      favoritePagination,
      latestFavorite,
    } = this.props;

    const { PaginatedServiceRequestsView } = this;

    const componentProps = {
      serviceRequests,
      dataList: serviceRequests,
      requesting: serviceRequestsRequesting,
      favoritePagination,
      CardComp: ServiceRequestDisplayContainer,
    };

    return (
      <PageContentPanel id="service-requests-panel">
        <CategoryHeader
          refresh={refreshServiceRequests}
          requesting={serviceRequestsRequesting}
          favoritePagination={favoritePagination}
          type="serviceRequest"
        />
        <PaginatedServiceRequestsView
          componentProps={componentProps}
          favoritePagination={favoritePagination}
          noFavoritesMessage={latestFavorite.message}
          requestInProgress={serviceRequestsRequesting}
        />
      </PageContentPanel>
    );
  }
}

export default ServiceRequestsPanel;
