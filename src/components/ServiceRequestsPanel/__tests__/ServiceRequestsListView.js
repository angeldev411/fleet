import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';
import { fromJS, List } from 'immutable';

import ServiceRequestsListView from '../ServiceRequestsListView';

const defaultProps = {
  serviceRequests: List(),
};

function renderComponent(props = defaultProps) {
  return shallow(<ServiceRequestsListView {...props} />);
}

test('ServiceRequestsListView renders a "TODO" title', () => {
  const component = renderComponent();
  expect(component.find('h2').text()).toInclude('TODO:');
});

test('ServiceRequestsListView renders one link per case', () => {
  const serviceRequests = fromJS([
    { id: '1' },
    { id: '2' },
    { id: '3' },
  ]);
  const component = renderComponent({ ...defaultProps, serviceRequests });
  expect(component.find('ul').length).toEqual(1);
  expect(component.find('li').length).toEqual(3);
  expect(component.find('Link').length).toEqual(3);
});
