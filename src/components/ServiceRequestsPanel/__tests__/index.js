import React from 'react';
import { noop } from 'lodash';
import {
  test,
  expect,
  shallow,
  spyOn,
} from '__tests__/helpers/test-setup';
import { List, Map } from 'immutable';

import ServiceRequestsPanel from '../index';

const defaultProps = {
  serviceRequests: List(),
  serviceRequestsView: '',
  favoritePagination: Map(),
  refreshServiceRequests: noop,
  requestNext: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestsPanel {...props} />);
}

test('ServiceRequestsPanel contains CategoryHeader', () => {
  const component = shallowRender();
  expect(component).toContain('CategoryHeader');
});

test('ServiceRequestsPanel renders a FavoritesPaginator by default', () => {
  const component = shallowRender();
  expect(component).toContain('FavoritesPaginator');
});

test('ServiceRequestsPanel renders a card view with serviceRequestsView === card', () => {
  const component = shallowRender({ ...defaultProps, serviceRequestsView: 'card' });
  expect(component.find('FavoritesPaginator').dive()).toContain('CardView');
});

test('ServiceRequestsPanel renders a list view with serviceRequestsView === list', () => {
  const component = shallowRender({ ...defaultProps, serviceRequestsView: 'list' });
  expect(component.find('FavoritesPaginator').dive()).toContain('ServiceRequestsListView');
});

test('ServiceRequestsPanel renders a map view with serviceRequestsView === map', () => {
  const component = shallowRender({ ...defaultProps, serviceRequestsView: 'map' });
  expect(component.find('FavoritesPaginator').dive()).toContain('ServiceRequestsMapView');
});

test('ServiceRequestsPanel builds a new serviceRequest view if the serviceRequestsView prop changes', () => {
  const component = shallowRender({ ...defaultProps, serviceRequestsView: 'card' });
  const instance = component.instance();
  const viewBuilderSpy = spyOn(instance, 'buildPaginatedServiceRequestsView');
  expect(viewBuilderSpy).toNotHaveBeenCalled();
  instance.componentWillReceiveProps({ serviceRequestsView: 'list' });
  expect(viewBuilderSpy).toHaveBeenCalledWith('list');
});
