import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Link } from 'react-router-dom';

function buildServiceRequestList(serviceRequests) {
  return serviceRequests.map((s) => {
    const serviceRequestId = s.get('id');
    return (
      <li key={serviceRequestId}>
        <Link to={`/serviceRequests/${serviceRequestId}`}>
          {`Service Request ID: ${serviceRequestId}`}
        </Link>
      </li>
    );
  });
}

function ServiceRequestsMapView({
  serviceRequests,
}) {
  return (
    <div style={{ padding: '0.5rem' }}>
      <h2>TODO: Build MAP view</h2>
      <ul>
        {buildServiceRequestList(serviceRequests)}
      </ul>
    </div>
  );
}

ServiceRequestsMapView.propTypes = {
  serviceRequests: ImmutablePropTypes.listOf(
    ImmutablePropTypes.mapContains({
      id: PropTypes.string,
    }),
  ).isRequired,
};

export default ServiceRequestsMapView;
