import React from 'react';
import PropTypes from 'prop-types';
import SmoothCollapse from 'react-smooth-collapse';

import Wrapper from './Wrapper';

// TODO: Refactor AbsoluteSmoothCollapse to use modifiers
function AbsoluteSmoothCollapse(props) {
  return (
    <Wrapper {...props}>
      <SmoothCollapse {...props}>
        { props.children }
      </SmoothCollapse>
    </Wrapper>
  );
}

AbsoluteSmoothCollapse.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AbsoluteSmoothCollapse;
