import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

// TODO: Refactor to use modifiers instead of passing props directly through component to styles.
/* istanbul ignore next */
const styles = props => `
  color: ${props.color || props.theme.colors.base.textLight};
  position: absolute;
  left: ${props.left};
  right: ${props.right};
  top: ${props.top};
  bottom: ${props.bottom};
  z-index: ${props.zIndex};
  padding-top: ${props.paddingTop};
  line-height: ${props.lineHeight};
  text-align: left;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  left: PropTypes.string,
  right: PropTypes.string,
  top: PropTypes.string,
  bottom: PropTypes.string,
  paddingTop: PropTypes.string,
  zIndex: PropTypes.number,
  color: PropTypes.string,
  lineHeight: PropTypes.string,
};

const defaultProps = {
  left: 'auto',
  right: 'auto',
  top: 'auto',
  bottom: 'auto',
  paddingTop: '0',
  zIndex: 1,
  color: undefined,
  lineHeight: '14px',
};

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
