import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import AbsoluteSmoothCollapse from '../index';

const testChildren = <p id="test-child">Hi</p>;

const testProps = {
  expanded: true,
  heightTransition: '.15s cubic-bezier(0, 1, 0.5, 1)',
  left: '1.5rem',
  bottom: '2rem',
  children: testChildren,
};

const setupComponent = () => shallow(<AbsoluteSmoothCollapse {...testProps} />);

test('Renders the wrapper', () => {
  const component = setupComponent();
  expect(component).toBeA('Wrapper');
});

test('Renders the smooth collapse component', () => {
  const component = setupComponent();
  expect(component).toContain('SmoothCollapse');
});

test('Renders the children', () => {
  const component = setupComponent();
  expect(component.find('SmoothCollapse')).toContain('p#test-child');
});
