import React from 'react';
import { noop } from 'lodash';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import QuickActionBar from '../index';

const menuMessage = {
  id: 'message-id',
  defaultMessage: 'the Menu Label',
};

const menuItems = {
  LEFT_ITEMS: [
    {
      id: 'the-item-id1',
      icon: 'icon-name1',
      label: menuMessage,
    },
    {
      id: 'the-item-id2',
      icon: 'icon-name2',
      label: menuMessage,
    },
  ],
  RIGHT_ITEMS: [
    {
      id: 'options',
      icon: 'icon-name1',
      label: menuMessage,
    },
  ],
};

const onItemClicked = noop;

const defaultProps = {
  items: menuItems,
  onItemClicked,
};

const renderComponent = (props = defaultProps) =>
  shallow(<QuickActionBar {...props} />);

test('Renders GroupItem components', () => {
  const itemGroups = renderComponent().find('ItemGroup');
  expect(itemGroups.length).toEqual(2);
});

test('Renders QuickActionButton components', () => {
  const actionBar = renderComponent().find('QuickActionButton');
  expect(actionBar.length).toEqual(menuItems.LEFT_ITEMS.length);
});

test('Renders ActionSelectorItem components', () => {
  const actionSelectorItems = renderComponent().find('QuickActionSelector');
  expect(actionSelectorItems.length).toEqual(menuItems.RIGHT_ITEMS.length);
});
