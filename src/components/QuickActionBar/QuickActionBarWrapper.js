import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.background};
  display: flex;
  justify-content: space-between;
  padding: ${px2rem(8)} ${px2rem(17)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'QuickActionBarWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
