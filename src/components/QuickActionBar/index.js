import { QuickActionButton } from 'base-components';
import React from 'react';
import { compact } from 'lodash';
import PropTypes from 'prop-types';
import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';

import QuickActionSelector from 'components/QuickActionSelector';

import QuickActionBarWrapper from './QuickActionBarWrapper';
import ItemGroup from './ItemGroup';

// TODO: Remove this component and all associate files after all detail pages are being rendered
// from the config/DetailPage composition.
function QuickActionBar({
  items,
  onItemClicked,
  disabled,
}) {
  return (
    <QuickActionBarWrapper>
      <ItemGroup>
        {items.LEFT_ITEMS.map(item => (
          <QuickActionButton
            key={item.id}
            modifiers={compact([!!disabled[item.id] && 'disabled'])}
            onClick={() => onItemClicked({ buttonId: item.id })}
          >
            {item.icon &&
              <QuickActionButton.Icon
                modifiers={['left']}
                name={item.icon}
              />
            }
            <QuickActionButton.Text>
              <FormattedMessage {...item.label} />
            </QuickActionButton.Text>
          </QuickActionButton>
        ))}
      </ItemGroup>
      <ItemGroup>
        {items.RIGHT_ITEMS.map(item => (
          <QuickActionSelector
            key={item.id}
            item={item}
            onClick={onItemClicked}
            disabled={!!disabled[item.id]}
          />
        ))}
      </ItemGroup>
    </QuickActionBarWrapper>
  );
}

QuickActionBar.propTypes = {
  items: PropTypes.shape({
    LEFT_ITEMS: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired,
      label: PropTypes.shape(messageDescriptorPropTypes).isRequired,
    })).isRequired,
    RIGHT_ITEMS: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired,
      label: PropTypes.shape(messageDescriptorPropTypes).isRequired,
      options: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        label: PropTypes.shape(messageDescriptorPropTypes).isRequired,
      })),
    })).isRequired,
  }).isRequired,
  onItemClicked: PropTypes.func.isRequired,
  disabled: PropTypes.shape(),
};

QuickActionBar.defaultProps = {
  disabled: {},
};

export default QuickActionBar;
