import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import WarningLabel from '../index';

const defaultProps = {
  label: {
    id: 'test id',
    defaultMessage: 'test warning message',
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<WarningLabel {...props} />);
}

test('renders a WarningIcon', () => {
  const component = shallowRender();
  expect(component).toContain('WarningIcon');
});

test('renders a WarningText with correct formatted label', () => {
  const component = shallowRender();
  expect(component).toContain('WarningText');

  const formattedLabel = component.find('WarningText FormattedMessage');
  expect(formattedLabel).toHaveProps({ ...defaultProps.label });
});
