import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';
import PropTypes from 'prop-types';
import React from 'react';

import WarningIcon from './WarningIcon';
import WarningText from './WarningText';

function WarningLabel({ label }) {
  return (
    <div>
      <WarningIcon />
      <WarningText>
        <FormattedMessage {...label} />
      </WarningText>
    </div>
  );
}

WarningLabel.propTypes = {
  label: PropTypes.shape(messageDescriptorPropTypes).isRequired,
};

export default WarningLabel;
