import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = ({ theme }) => `
  color: ${theme.colors.base.textLight};
  font-size: ${px2rem(12)};
  font-weight: 300;
  line-height: ${px2rem(21)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'WarningText',
  styled.span,
  styles,
  { themePropTypes },
);
