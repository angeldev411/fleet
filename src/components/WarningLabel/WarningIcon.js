import { omit } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

export function UnstyledFontAwesome(props) {
  const componentProps = omit(props, ['theme', 'modifiers']);
  return <FontAwesome {...componentProps} name="minus-circle" />;
}

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.status.warning};
  font-size: ${px2rem(14)};
  padding-right: ${px2rem(6)};
  vertical-align: middle;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    status: PropTypes.shape({
      warning: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'WarningIcon',
  styled(UnstyledFontAwesome),
  styles,
  { themePropTypes },
);
