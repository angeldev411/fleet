import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

import CircleButton from 'elements/CircleButton';

// A simple magnifying glass in a circle. Clicking it expands the search bar.
function SearchButton({ onClick }) {
  return (
    <CircleButton id="search-button" onClick={onClick}>
      <FontAwesome name="search" />
    </CircleButton>
  );
}

SearchButton.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default SearchButton;
