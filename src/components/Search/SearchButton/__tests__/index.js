import React from 'react';
import { noop } from 'lodash';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import SearchButton from '../index';

const defaultProps = {
  onClick: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<SearchButton {...props} />);
}

test('Includes a CircleButton', () => {
  const component = shallowRender();
  expect(component).toContain('CircleButton');
});
