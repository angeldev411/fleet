import React, { Component } from 'react';
import PropTypes from 'prop-types';
import enhanceWithClickOutside from 'react-click-outside';

import Wrapper from './Wrapper';
import SearchBar from './SearchBar';
import SearchButton from './SearchButton';

// The "...rest" of the props is lengthy, and there is no need to individually specify them here.
export class Search extends Component {
  handleClickOutside = (e) => {
    this.props.onOutsideClick(e);
  };

  render() {
    const { toggleSearchExpanded, ...rest } = this.props;

    return (
      <Wrapper>
        <SearchBar {...rest} />
        <SearchButton onClick={toggleSearchExpanded} />
      </Wrapper>
    );
  }
}

Search.propTypes = {
  toggleSearchExpanded: PropTypes.func.isRequired,
  onOutsideClick: PropTypes.func.isRequired,
};

export default enhanceWithClickOutside(Search);
