import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import { Search } from '../index';

const displaySearchOptions = true;
const expanded = true;
const getSearchSuggestions = noop;
const clearSearchSuggestions = noop;
const handleSearchOptionSelect = noop;
const handleSearchValueChange = noop;
const performSearch = noop;
const searchOptions = [];
const searchSuggestions = [];
const selectedSearchOption = {};
const toggleDisplaySearchOptions = noop;
const toggleSearchExpanded = noop;
const onOutsideClick = noop;

const defaultProps = {
  displaySearchOptions,
  expanded,
  getSearchSuggestions,
  clearSearchSuggestions,
  handleSearchOptionSelect,
  handleSearchValueChange,
  performSearch,
  searchOptions,
  searchSuggestions,
  selectedSearchOption,
  toggleDisplaySearchOptions,
  toggleSearchExpanded,
  onOutsideClick,
};

function shallowRender(props = defaultProps) {
  return shallow(<Search {...props} />);
}

test('renders SearchBar with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('SearchBar');
  expect(component.find('SearchBar')).toHaveProps({
    displaySearchOptions: defaultProps.displaySearchOptions,
    expanded: defaultProps.expanded,
    getSearchSuggestions: defaultProps.getSearchSuggestions,
    clearSearchSuggestions: defaultProps.clearSearchSuggestions,
    handleSearchOptionSelect: defaultProps.handleSearchOptionSelect,
    handleSearchValueChange: defaultProps.handleSearchValueChange,
    performSearch: defaultProps.performSearch,
    searchOptions: defaultProps.searchOptions,
    searchSuggestions: defaultProps.searchSuggestions,
    selectedSearchOption: defaultProps.selectedSearchOption,
    toggleDisplaySearchOptions: defaultProps.toggleDisplaySearchOptions,
    onOutsideClick: defaultProps.onOutsideClick,
  });
});

test('renders a SearchButton with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('SearchButton');
  expect(component.find('SearchButton')).toHaveProps({
    onClick: defaultProps.toggleSearchExpanded,
  });
});

test('calls onOutsideClick when handleClickOutside is called', () => {
  const outsideClick = createSpy();
  const component = shallowRender({ ...defaultProps, onOutsideClick: outsideClick });
  const e = 'test';
  component.instance().handleClickOutside(e);
  expect(outsideClick).toHaveBeenCalledWith(e);
});
