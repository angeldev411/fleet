import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const styles = `
  height: ${px2rem(34)};
  display: inline-block;
  overflow: visible;
  position: relative;
  text-align: right;
`;

export default buildStyledComponent(
  'SearchWrapper',
  styled.div,
  styles,
);
