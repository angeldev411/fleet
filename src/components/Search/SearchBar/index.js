import React from 'react';
import PropTypes from 'prop-types';
import { compact } from 'lodash';

import VerticalDivider from 'elements/VerticalDivider';

import Wrapper from './Wrapper';
import SearchOption from './SearchOption';
import Typeahead from './Typeahead';

function SearchBar({
  displaySearchOptions,
  expanded,
  getSearchSuggestions,
  clearSearchSuggestions,
  handleSearchOptionSelect,
  handleSearchValueChange,
  performSearch,
  searchOptions,
  searchSuggestions,
  searchValue,
  selectedSearchOption,
  toggleDisplaySearchOptions,
}) {
  const showExpanded = expanded && 'expanded';
  return (
    <Wrapper modifiers={compact([showExpanded])}>
      <SearchOption
        displaySearchOptions={displaySearchOptions}
        expanded={expanded}
        handleSearchOptionSelect={handleSearchOptionSelect}
        searchOptions={searchOptions}
        selectedSearchOption={selectedSearchOption}
        toggleDisplaySearchOptions={toggleDisplaySearchOptions}
      />
      <VerticalDivider modifiers={['dark']} />
      <Typeahead
        clearSearchSuggestions={clearSearchSuggestions}
        displaySearchOptions={displaySearchOptions}
        focused={expanded && !!selectedSearchOption.id}
        getSearchSuggestions={getSearchSuggestions}
        handleSearchValueChange={handleSearchValueChange}
        performSearch={performSearch}
        searchSuggestions={searchSuggestions}
        searchValue={searchValue}
      />
    </Wrapper>
  );
}

SearchBar.propTypes = {
  displaySearchOptions: PropTypes.bool.isRequired,
  expanded: PropTypes.bool.isRequired,
  getSearchSuggestions: PropTypes.func.isRequired,
  clearSearchSuggestions: PropTypes.func.isRequired,
  handleSearchOptionSelect: PropTypes.func.isRequired,
  handleSearchValueChange: PropTypes.func.isRequired,
  performSearch: PropTypes.func.isRequired,
  searchOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
  searchSuggestions: PropTypes.arrayOf(PropTypes.object).isRequired,
  searchValue: PropTypes.string,
  selectedSearchOption: PropTypes.shape().isRequired,
  toggleDisplaySearchOptions: PropTypes.func.isRequired,
};

SearchBar.defaultProps = {
  searchValue: '',
};

export default SearchBar;
