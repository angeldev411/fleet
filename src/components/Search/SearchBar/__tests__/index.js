import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import SearchBar from '../index';

const displaySearchOptions = true;
const expanded = true;
const getSearchSuggestions = noop;
const clearSearchSuggestions = noop;
const handleSearchOptionSelect = noop;
const handleSearchValueChange = noop;
const performSearch = noop;
const searchOptions = [];
const searchSuggestions = [];
const searchValue = 'test value';
const selectedSearchOption = {
  id: 'all',
  label: {
    id: 'components.TopNav.Search.Option.all',
    defaultMessage: 'All',
  },
};
const toggleDisplaySearchOptions = noop;

const defaultProps = {
  displaySearchOptions,
  expanded,
  getSearchSuggestions,
  clearSearchSuggestions,
  handleSearchOptionSelect,
  handleSearchValueChange,
  performSearch,
  searchOptions,
  searchSuggestions,
  searchValue,
  selectedSearchOption,
  toggleDisplaySearchOptions,
};

function shallowRender(props = defaultProps) {
  return shallow(<SearchBar {...props} />);
}

test('renders a SearchBarWrapper with the expected props', () => {
  const component = shallowRender();
  expect(component).toBeA('SearchBarWrapper');
  expect(component.find('SearchBarWrapper').props()).toContain({ modifiers: ['expanded'] });
});

test('renders a VerticalDivider', () => {
  const component = shallowRender();
  expect(component).toContain('VerticalDivider');
});

test('renders a SearchOption with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('SearchOption');
  expect(component.find('SearchOption')).toHaveProps({
    displaySearchOptions: defaultProps.displaySearchOptions,
    expanded: defaultProps.expanded,
    handleSearchOptionSelect: defaultProps.handleSearchOptionSelect,
    searchOptions: defaultProps.searchOptions,
    selectedSearchOption: defaultProps.selectedSearchOption,
    toggleDisplaySearchOptions: defaultProps.toggleDisplaySearchOptions,
  });
});

test('renders a Typeahead with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('Typeahead');
  expect(component.find('Typeahead')).toHaveProps({
    focused: defaultProps.expanded && !!defaultProps.selectedSearchOption.id,
    displaySearchOptions: defaultProps.displaySearchOptions,
    getSearchSuggestions: defaultProps.getSearchSuggestions,
    clearSearchSuggestions: defaultProps.clearSearchSuggestions,
    searchSuggestions: defaultProps.searchSuggestions,
    performSearch: defaultProps.performSearch,
    handleSearchValueChange: defaultProps.handleSearchValueChange,
    searchValue: defaultProps.searchValue,
  });
});
