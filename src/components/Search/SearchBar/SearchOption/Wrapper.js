import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  collapsed: () => ({
    styles: `width: ${px2rem(17)};`,
  }),
  expanded: () => ({
    styles: `width: ${px2rem(145)};`,
  }),
  isDisplayed: ({ theme }) => ({
    styles: `
      background: ${theme.colors.brand.primary};
      color: ${theme.colors.base.chrome000};
    `,
  }),
  isHidden: ({ theme }) => ({
    styles: `
      background: ${theme.colors.base.chrome200};
      color: ${theme.colors.base.textLight};
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  border-bottom-left-radius: ${px2rem(17)};
  border-top-left-radius: ${px2rem(17)};
  color: ${props.theme.colors.base.textLight};
  cursor: pointer;
  font-size: ${px2rem(12)};
  line-height: ${px2rem(34)};
  overflow: hidden;
  text-align: center;
  transition: width 0.2s;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome000: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
    brand: PropTypes.shape({
      primary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'SearchOptionWrapper',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
