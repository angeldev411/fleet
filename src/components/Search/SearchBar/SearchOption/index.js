import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import FontAwesome from 'react-fontawesome';
import { compact } from 'lodash';
import { px2rem } from 'decisiv-ui-utils';

import AbsoluteSmoothCollapse from 'components/AbsoluteSmoothCollapse';
import PopupMenu from 'components/PopupMenu';

import {
  SelectorOptionsPopup,
  SelectorOptionsList,
  SelectorOptionListElement,
} from 'elements/SelectorButton';

import Wrapper from './Wrapper';
import OptionButton from './OptionButton';

// Displays the selectedSearchOption. Clicking the link will open a dropdown selected and allow you
// to choose a different option.
function SearchOption({
  displaySearchOptions,
  expanded,
  handleSearchOptionSelect,
  searchOptions,
  selectedSearchOption,
  toggleDisplaySearchOptions,
}) {
  const stateModifier = displaySearchOptions ? 'isDisplayed' : 'isHidden';
  const expandedModifier = expanded ? 'expanded' : 'collapsed';

  /* istanbul ignore next */
  function onClickOutsidePopupMenu(e) {
    if (displaySearchOptions) {
      e.stopPropagation();
      toggleDisplaySearchOptions();
    }
  }

  return (
    <Wrapper modifiers={[stateModifier, expandedModifier]} >
      <OptionButton onClick={toggleDisplaySearchOptions} >
        <FormattedMessage {...selectedSearchOption.label} />
        <FontAwesome name="caret-down" />
      </OptionButton>
      <AbsoluteSmoothCollapse
        expanded={displaySearchOptions}
        heightTransition=".15s cubic-bezier(0, 1, 0.5, 1)"
        top={px2rem(32)}
        left={px2rem(-3)}
        zIndex={1}
        width={px2rem(150)}
      >
        <PopupMenu onOutsideClick={onClickOutsidePopupMenu}>
          <SelectorOptionsPopup minWidth="0">
            <SelectorOptionsList>
              {
                searchOptions.map((option) => {
                  const selected = selectedSearchOption.id === option.id && 'selected';
                  return (
                    <SelectorOptionListElement
                      key={option.id}
                      id={`search-option-${option.id}`}
                      onClick={(event) => { handleSearchOptionSelect(event, option); }}
                      modifiers={compact([selected])}
                    >
                      <FormattedMessage {...option.label} />
                    </SelectorOptionListElement>
                  );
                })
              }
            </SelectorOptionsList>
          </SelectorOptionsPopup>
        </PopupMenu>
      </AbsoluteSmoothCollapse>
    </Wrapper>
  );
}

SearchOption.propTypes = {
  displaySearchOptions: PropTypes.bool.isRequired,
  expanded: PropTypes.bool.isRequired,
  handleSearchOptionSelect: PropTypes.func.isRequired,
  searchOptions: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedSearchOption: PropTypes.shape({
    id: PropTypes.string.isRequired,
    label: PropTypes.shape({
      id: PropTypes.string.isRequired,
      defaultMessage: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  toggleDisplaySearchOptions: PropTypes.func.isRequired,
};

export default SearchOption;
