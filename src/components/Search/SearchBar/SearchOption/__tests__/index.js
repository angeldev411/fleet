import React from 'react';
import { noop } from 'lodash';
import { defineMessages } from 'react-intl';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import SearchOption from '../index';

const displaySearchOptions = true;
const expanded = true;
const searchOptions = [];
const selectedSearchOption = {
  id: 'all',
  label: {
    id: 'components.TopNav.Search.Option.all',
    defaultMessage: 'All',
  },
};

const messages = defineMessages({
  all: {
    id: 'components.TopNav.Search.Option.all',
    defaultMessage: 'All',
  },
});

const defaultProps = {
  displaySearchOptions,
  expanded,
  handleSearchOptionSelect: noop,
  searchOptions,
  selectedSearchOption,
  toggleDisplaySearchOptions: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<SearchOption {...props} />);
}

test('renders a SearchOptionWrapper with `expanded` modifier when expanded prop is true', () => {
  const component = shallowRender();
  expect(component).toBeA('SearchOptionWrapper');
  expect(component.props().modifiers).toInclude('expanded');
});

test('renders a SearchOptionWrapper with `collapsed` modifier when expanded prop is false', () => {
  const component = shallowRender({ ...defaultProps, expanded: false });
  expect(component).toBeA('SearchOptionWrapper');
  expect(component.props().modifiers).toInclude('collapsed');
});

test('renders an OptionButton with the expected props and message for selected option', () => {
  const component = shallowRender();
  expect(component).toContain('OptionButton');
  expect(component.find('OptionButton'))
    .toHaveProp('onClick', defaultProps.toggleDisplaySearchOptions);
  expect(component.find('FormattedMessage')).toHaveProps({ ...messages.all });
});

test('renders a AbsoluteSmoothCollapse with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AbsoluteSmoothCollapse');
  expect(component.find('AbsoluteSmoothCollapse'))
    .toHaveProp('expanded', defaultProps.displaySearchOptions);
});

test('renders SelectorOptionListElement when search options provided', () => {
  const testProps = {
    ...defaultProps,
    searchOptions: [{ id: '123', label: { id: 'test', defaultMessage: 'TEST' } }],
  };
  const component = shallowRender(testProps);
  expect(component.find('SelectorOptionListElement').length).toEqual(1);
});

test('clicking SelectorOptionListElement calls handleSearchOptionSelect', () => {
  const handleSearchOptionSelect = createSpy();
  const option = { id: '123', label: { id: 'test', defaultMessage: 'TEST' } };
  const testProps = {
    ...defaultProps,
    handleSearchOptionSelect,
    searchOptions: [option],
  };
  const component = shallowRender(testProps);
  const element = component.find('SelectorOptionListElement').first();
  const event = {};
  element.simulate('click', event);
  expect(handleSearchOptionSelect).toHaveBeenCalledWith(event, option);
});
