import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const OptionButton = styled.a`
  padding: 0 ${px2rem(20)};
  span {
    margin-left: ${px2rem(10)};
  }
`;

export default OptionButton;
