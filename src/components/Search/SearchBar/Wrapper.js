import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  expanded: () => ({
    styles: `width: ${px2rem(300)};`,
  }),
};

/* istanbul ignore next */
const styles = () => `
  display: inline-flex;
  height: 100%;
  transition-duration: 0.2s;
  transition-timing-function: cubic-bezier(0, 0, 1, 1);
  vertical-align: top;
  width: 0;
`;

export default buildStyledComponent(
  'SearchBarWrapper',
  styled.div,
  styles,
  { modifierConfig },
);
