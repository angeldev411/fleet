import React from 'react';
import { noop } from 'lodash';
import {
  test,
  expect,
  shallow,
  createSpy,
  mount,
} from '__tests__/helpers/test-setup';

import Typeahead from '../index';

const defaultProps = {
  clearSearchSuggestions: () => true,
  displaySearchOptions: true,
  focused: true,
  getSearchSuggestions: () => true,
  handleSearchValueChange: noop,
  performSearch: noop,
  searchSuggestions: [
    { field: 'value 1' },
    { field: 'value 2' },
  ],
  searchValue: 'test search value',
};

function shallowRender(props = defaultProps) {
  return shallow(<Typeahead {...props} />);
}

function fullRender(props = defaultProps) {
  return mount(<Typeahead {...props} />);
}

/* ---------------------------- componentDidUpdate -------------------------------------- */

test('focuses on input if focused changes from false to true', () => {
  const component = fullRender({ ...defaultProps, focused: false });
  const instance = component.instance();
  const focus = createSpy();
  instance.self.input.focus = focus;
  component.setProps({ focused: true });
  expect(focus).toHaveBeenCalled();
});

test('focuses on input if displaySearchOptions changes from true to false', () => {
  const component = fullRender({ ...defaultProps });
  const instance = component.instance();
  const focus = createSpy();
  instance.self.input.focus = focus;
  component.setProps({ displaySearchOptions: false });
  expect(focus).toHaveBeenCalled();
});

test('does not focus on input if focused changes from true to false', () => {
  const component = fullRender({ ...defaultProps, focused: true });
  const instance = component.instance();
  const focus = createSpy();
  instance.self.input.focus = focus;
  component.setProps({ focused: false });
  expect(focus).toNotHaveBeenCalled();
});

/* ---------------------------- modifiers -------------------------------------- */

test('passes "focused" modifier down when focused is true', () => {
  const component = shallowRender({ ...defaultProps, focused: true });
  const autoSuggest = component.find('Autosuggest');
  expect(autoSuggest.props().modifiers).toInclude('focused');
});

test('passes no modifiers down when focused is false', () => {
  const component = shallowRender({ ...defaultProps, focused: false });
  const autoSuggest = component.find('Autosuggest');
  expect(autoSuggest.props().modifiers).toNotInclude('focused');
});

/* ---------------------------- onKeyDown -------------------------------------- */

test(
  'onKeyDown with enter keypress performs search ',
  () => {
    const event = { key: 'Enter' };
    const performSearch = createSpy();
    const component = shallowRender({ ...defaultProps, performSearch });
    const instance = component.instance();
    instance.onKeyDown(event);
    expect(performSearch).toHaveBeenCalled();
  },
);

test('onKeyDown with non-enter keypress does not perform search', () => {
  const event = { key: 'o' };
  const performSearch = createSpy();
  const component = shallowRender({ ...defaultProps, performSearch });
  const instance = component.instance();
  instance.onKeyDown(event);
  expect(performSearch).toNotHaveBeenCalled();
});

/* ---------------------------- render -------------------------------------- */

test('component has expected props', () => {
  const testProps = {
    ...defaultProps,
  };
  const component = shallowRender(testProps);
  expect(component).toHaveProps({
    onSuggestionsClearRequested: defaultProps.clearSearchSuggestions,
    onSuggestionsFetchRequested: defaultProps.getSearchSuggestions,
    suggestions: defaultProps.searchSuggestions,
  });
});

// ----------------------- getSuggestionValue -----------------------------------
test('component has expected prop getSuggetionValue function', () => {
  const component = shallowRender();
  const suggestion = { id: 'testID' };
  const id = component.find('Autosuggest').props().getSuggestionValue(suggestion);
  expect(id).toEqual('testID');
});

// ----------------------- renderSuggestion -----------------------------------
test('component has expected prop renderSuggestion function', () => {
  const component = shallowRender();
  const suggestion = { id: 'testID', type: 'whatever' };
  const suggestionComponent = component.find('Autosuggest').props().renderSuggestion(suggestion);
  expect(shallow(suggestionComponent)).toBeA('div');
});
