import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compact } from 'lodash';

// Autosuggest is an external dependency that is styled with classnames. elements/Autosuggest is
// the export from react-autosuggest that has been wrapped in a style wrapper.
import Autosuggest, { Suggestion } from 'elements/Autosuggest';

class Typeahead extends Component {
  // Autosuggest is a controlled component.
  // This means that you need to provide an input value
  // and an onChange handler that updates this value (see below).
  // Suggestions also need to be provided to the Autosuggest,
  // and they are initially empty because the Autosuggest is closed.
  static propTypes = {
    clearSearchSuggestions: PropTypes.func.isRequired,
    displaySearchOptions: PropTypes.bool.isRequired,
    focused: PropTypes.bool.isRequired,
    getSearchSuggestions: PropTypes.func.isRequired,
    handleSearchValueChange: PropTypes.func.isRequired,
    performSearch: PropTypes.func.isRequired,
    searchSuggestions: PropTypes.arrayOf(PropTypes.object),
    searchValue: PropTypes.string,
  };

  static defaultProps = {
    searchSuggestions: [],
    searchValue: '',
  };

  // This logic also helps maintain focus on the user input.
  componentDidUpdate(prevProps) {
    const { focused: wasFocused, displaySearchOptions: wasSearchOptionDisplayed } = prevProps;
    const { focused, displaySearchOptions: searchOptionDisplayed } = this.props;
    if (
      (focused !== wasFocused && focused) ||
      (searchOptionDisplayed !== wasSearchOptionDisplayed && !searchOptionDisplayed)
    ) {
      this.self.input.focus();
    }
  }

  // If the user presses the enter key, we should clear the search input and perform the search.
  // This will fire if the user has keyed to their desired suggestion or if the user is simply
  // typing and hits enter.
  onKeyDown = (event) => {
    if (event.key === 'Enter') {
      this.props.performSearch();
    }
  }

  render() {
    const {
      getSearchSuggestions,
      clearSearchSuggestions,
      searchSuggestions,
      focused,
    } = this.props;

    const {
      handleSearchValueChange,
      searchValue,
    } = this.props;

    // Autosuggest will pass through all these props to the input element.
    const inputProps = {
      placeholder: 'Search',
      onChange: handleSearchValueChange,
      onKeyDown: this.onKeyDown,
      value: searchValue,
    };

    return (
      <Autosuggest
        autosuggestRef={(self) => { this.self = self; }}
        getSuggestionValue={suggestion => suggestion.id}
        inputProps={inputProps}
        onSuggestionsClearRequested={clearSearchSuggestions}
        onSuggestionsFetchRequested={getSearchSuggestions}
        renderSuggestion={suggestion => <Suggestion suggestion={suggestion} />}
        suggestions={searchSuggestions}
        modifiers={compact([focused && 'focused'])}
      />
    );
  }
}

export default Typeahead;
