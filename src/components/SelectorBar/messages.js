import { defineMessages } from 'react-intl';

const messages = defineMessages({
  cards: {
    id: 'components.CasesSelectorBar.option.cards',
    defaultMessage: 'Cards',
  },
  list: {
    id: 'components.CasesSelectorBar.option.list',
    defaultMessage: 'List',
  },
  map: {
    id: 'components.CasesSelectorBar.option.map',
    defaultMessage: 'Map',
  },
});

export default messages;
