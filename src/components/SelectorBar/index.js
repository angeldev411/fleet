import PropTypes from 'prop-types';
import React from 'react';

import ViewSelector from 'components/ViewSelector';

import {
  SplitBlock,
  SplitBlockPart,
  SplitBlockElement,
} from 'elements/SplitBlock';

import { CASES_VIEW_OPTIONS, ASSETS_VIEW_OPTIONS, SERVICE_REQUESTS_VIEW_OPTIONS } from './constants';

function getOptions(type) {
  switch (type) {
    case 'asset':
      return ASSETS_VIEW_OPTIONS;
    case 'serviceRequests':
      return SERVICE_REQUESTS_VIEW_OPTIONS;
    case 'case':
    default:
      return CASES_VIEW_OPTIONS;
  }
}

function SelectorBar({
  view,
  setView,
  type,
}) {
  const options = getOptions(type);
  return (
    <SplitBlock modifiers={['pad']}>
      <SplitBlockPart modifiers={['left', 'wide']}>
        <SplitBlockElement modifiers={['padRight']}>
          <ViewSelector
            id="cases-view-selector"
            changeView={setView}
            options={options}
            selected={view}
          />
        </SplitBlockElement>
      </SplitBlockPart>
    </SplitBlock>
  );
}

SelectorBar.propTypes = {
  view: PropTypes.string.isRequired,
  setView: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};

export default SelectorBar;
