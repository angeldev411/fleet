import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import SelectorBar from '../index';
import { CASES_VIEW_OPTIONS, ASSETS_VIEW_OPTIONS, SERVICE_REQUESTS_VIEW_OPTIONS } from '../constants';

const defaultProps = {
  view: 'card',
  setView: noop,
  type: 'case',
};

function renderComponent(props = defaultProps) {
  return shallow(<SelectorBar {...props} />);
}

test('SelectorBar renders a ViewSelector with the expected props(case)', () => {
  const component = renderComponent();
  expect(component.find('#cases-view-selector').length).toEqual(1);
  expect(component.find('#cases-view-selector')).toHaveProps({
    options: CASES_VIEW_OPTIONS,
    selected: defaultProps.casesView,
    changeView: defaultProps.setCasesView,
  });
});

test('SelectorBar renders a ViewSelector with the expected props(asset)', () => {
  const component = renderComponent({ ...defaultProps, type: 'asset' });
  expect(component.find('#cases-view-selector').length).toEqual(1);
  expect(component.find('#cases-view-selector')).toHaveProps({
    options: ASSETS_VIEW_OPTIONS,
    selected: defaultProps.casesView,
    changeView: defaultProps.setCasesView,
  });
});

test('SelectorBar renders a ViewSelector with the expected props(serviceRequest)', () => {
  const component = renderComponent({ ...defaultProps, type: 'serviceRequests' });
  expect(component.find('#cases-view-selector').length).toEqual(1);
  expect(component.find('#cases-view-selector')).toHaveProps({
    options: SERVICE_REQUESTS_VIEW_OPTIONS,
    selected: defaultProps.casesView,
    changeView: defaultProps.setCasesView,
  });
});
