import messages from './messages';

// TODO: Re-introduce map views by un-commenting commented options

export const CASES_VIEW_OPTIONS = [
  {
    id: 'cards',
    label: messages.cards,
  },
  {
    id: 'list',
    label: messages.list,
  },
  // {
  //   id: 'map',
  //   label: messages.map,
  // },
];

export const ASSETS_VIEW_OPTIONS = [
  {
    id: 'cards',
    label: messages.cards,
  },
  {
    id: 'list',
    label: messages.list,
  },
  // {
  //   id: 'map',
  //   label: messages.map,
  // },
];

export const SERVICE_REQUESTS_VIEW_OPTIONS = [
  {
    id: 'cards',
    label: messages.cards,
  },
  {
    id: 'list',
    label: messages.list,
  },
  // {
  //   id: 'map',
  //   label: messages.map,
  // },
];
