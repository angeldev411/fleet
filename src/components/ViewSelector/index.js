import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import FontAwesome from 'react-fontawesome';
import { px2rem } from 'decisiv-ui-utils';

import AbsoluteSmoothCollapse from 'components/AbsoluteSmoothCollapse';
import PopupMenu from 'components/PopupMenu';

import {
  SelectorButton,
  SelectorButtonAction,
  SelectorOptionsPopup,
  SelectorOptionsList,
  SelectorOptionListElement,
} from 'elements/SelectorButton';

import ButtonWrapper from './ButtonWrapper';
import IconWrapper from './IconWrapper';
import messages from './messages';

class ViewSelector extends Component {
  static propTypes = {
    changeView: PropTypes.func.isRequired,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        label: PropTypes.shape({
          id: PropTypes.string.isRequired,
          defaultMessage: PropTypes.string.isRequired,
        }).isRequired,
      }),
    ).isRequired,
    selected: PropTypes.string.isRequired,
  }

  state = {
    expanded: false,
  };

  collapse = () => {
    this.setState({ expanded: false });
  }

  handleSelect = (event, optionId) => {
    event.preventDefault();
    this.props.changeView(optionId);
    this.collapse();
  }

  toggle = () => {
    this.setState({ expanded: !this.state.expanded });
  }

  render() {
    const {
      options,
      selected,
    } = this.props;
    const { expanded } = this.state;

    const selectedOption = options.find(option => option.id === selected) || {};
    const availableOptions = options.filter(option => option.id !== selected);

    return (
      <SelectorButton>
        <FormattedMessage {...messages.viewAs} />:
        <SelectorButtonAction
          expanded={expanded}
          onClick={this.toggle}
        >
          <PopupMenu onOutsideClick={this.collapse}>
            {!!selectedOption.id &&
              <ButtonWrapper>
                <FormattedMessage {...selectedOption.label} />
                <IconWrapper>
                  <FontAwesome name={expanded ? 'caret-up' : 'caret-down'} />
                </IconWrapper>
              </ButtonWrapper>
            }
            <AbsoluteSmoothCollapse
              expanded={expanded}
              heightTransition=".15s cubic-bezier(0, 1, 0.5, 1)"
              top={px2rem(24)}
              left={px2rem(-4)}
              right={px2rem(-4)}
            >
              <SelectorOptionsPopup>
                <SelectorOptionsList>
                  {
                    availableOptions.map(option => (
                      <SelectorOptionListElement
                        key={option.id}
                        id={`case-view-option-${option.id}`}
                        onClick={(event) => { this.handleSelect(event, option.id); }}
                      >
                        <FormattedMessage {...option.label} />
                      </SelectorOptionListElement>),
                    )
                  }
                </SelectorOptionsList>
              </SelectorOptionsPopup>
            </AbsoluteSmoothCollapse>
          </PopupMenu>
        </SelectorButtonAction>
      </SelectorButton>
    );
  }
}

export default ViewSelector;
