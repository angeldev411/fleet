import { defineMessages } from 'react-intl';

const messages = defineMessages({
  viewAs: {
    id: 'components.ViewSelector.message.viewAs',
    defaultMessage: 'View As',
  },
});

export default messages;
