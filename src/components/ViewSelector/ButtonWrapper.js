import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = `
  display: flex;
  justify-content: space-between;
  flex: 1;
`;

export default buildStyledComponent(
  'ButtonWrapper',
  styled.div,
  styles,
);
