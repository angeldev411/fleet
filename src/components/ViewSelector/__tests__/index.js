import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  createSpy,
  shallow,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import ViewSelector from '../index';

const options = [
  { id: 'card', label: { id: 'card', defaultMessage: 'Card' } },
  { id: 'list', label: { id: 'list', defaultMessage: 'List' } },
  { id: 'map', label: { id: 'map', defaultMessage: 'Map' } },
];

const defaultProps = {
  changeView: noop,
  options,
  selected: options[2].id,
};

function renderComponent(props = defaultProps) {
  return shallow(<ViewSelector {...props} />);
}

function mountComponent(props = defaultProps) {
  return mount(
    <MountableTestComponent authorized>
      <ViewSelector {...props} />
    </MountableTestComponent>,
  );
}

test('Renders the message associated with the selected prop', () => {
  const component = mountComponent();
  expect(component.text()).toEqual(`View As:${options[2].label.defaultMessage}`);
});

test('Has state expanded = false by default', () => {
  const component = renderComponent();
  expect(component).toHaveState({ expanded: false });
});

test('Clicking the displayed view type sets the state expended = true', () => {
  const component = renderComponent();
  component.find('SelectorButtonAction').simulate('click');
  expect(component).toHaveState({ expanded: true });
});

test(
  'Selecting an option from the SelectorOptionsList calls the changeView prop and sets the state expanded = false',
  () => {
    const changeViewSpy = createSpy();
    const preventDefaultSpy = createSpy();
    const props = {
      ...defaultProps,
      changeView: changeViewSpy,
    };
    const component = renderComponent(props);
    component.find(`#case-view-option-${options[1].id}`)
      .simulate('click', { preventDefault: preventDefaultSpy });
    expect(changeViewSpy).toHaveBeenCalledWith(options[1].id);
    expect(component).toHaveState({ expanded: false });
  },
);

test('Renders `FontAwesome` with `caret-down` as `name` props when collapsed', () => {
  const component = renderComponent();
  expect(component).toContain('FontAwesome');
  const FontAwesome = component.find('FontAwesome');
  expect(FontAwesome).toHaveProp('name', 'caret-down');
});

test('Renders `FontAwesome` with `caret-up` as `name` props when expanded', () => {
  const component = renderComponent();
  component.setState({ expanded: true });
  expect(component).toContain('FontAwesome');
  const FontAwesome = component.find('FontAwesome');
  expect(FontAwesome).toHaveProp('name', 'caret-up');
});
