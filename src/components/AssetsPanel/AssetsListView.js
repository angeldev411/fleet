import { Map } from 'immutable';
import PropTypes from 'prop-types';
import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';

import AssetDisplayContainer from 'containers/AssetDisplayContainer';

import ResponsiveCardGrid, { CARD_GRID_BREAKPOINTS } from 'elements/Card/ResponsiveCardGrid';
import ListTable from 'elements/ListTable';
import TableWrapper from 'elements/TableWrapper';

import { shouldBuildLoadingGhosts } from 'utils/favorites';

import ListRowLoading from 'components/_common/ListRowLoading';

import messages from './messages';

const MAX_LOADING_ROWS = 10;

function renderAssetList(assets) {
  return assets.map((assetInfo) => {
    const assetId = assetInfo.get('id');
    return (
      <AssetDisplayContainer
        datum={assetInfo}
        key={assetId}
        type="list"
      />
    );
  });
}

function renderGhostRows(assets, favoritePagination, requesting) {
  if (!shouldBuildLoadingGhosts({ currentCount: assets.size, favoritePagination, requesting })) {
    return null;
  }
  const totalCount = favoritePagination.get('totalCount');
  const count = totalCount ?
    Math.min(MAX_LOADING_ROWS, totalCount - assets.size) :
    MAX_LOADING_ROWS;
  return Array.from(
    { length: count },
    (_, i) => (
      <ListRowLoading
        key={i}
        columnSpans={[3, 2, 4, 3]}
      />
    ),
  );
}


function AssetsListView({ assets, favoritePagination, requesting }) {
  return (
    <ResponsiveCardGrid breakpoints={CARD_GRID_BREAKPOINTS}>
      <TableWrapper>
        <ListTable
          modifiers={['borderCollapse', 'fullWidth']}
          tdModifiers={['tall']}
          thModifiers={['chrome500', 'leftAlign', 'regular']}
        >
          <thead>
            <ListTable.Row modifiers={['lined']}>
              <th><FormattedMessage {...messages.unitNumber} /></th>
              <th><FormattedMessage {...messages.serial} /></th>
              <th><FormattedMessage {...messages.vin} /></th>
              <th><FormattedMessage {...messages.make} /></th>
              <th><FormattedMessage {...messages.model} /></th>
              <th><FormattedMessage {...messages.year} /></th>
              <th><FormattedMessage {...messages.engine} /></th>
              <th><FormattedMessage {...messages.odometer} /></th>
              <th><FormattedMessage {...messages.pmStatus} /></th>
              <th><FormattedMessage {...messages.warrantyStatus} /></th>
              <th><FormattedMessage {...messages.campaignStatus} /></th>
              <th><FormattedMessage {...messages.other} /></th>
            </ListTable.Row>
          </thead>
          <tbody>
            {renderAssetList(assets)}
            {renderGhostRows(assets, favoritePagination, requesting)}
          </tbody>
        </ListTable>
      </TableWrapper>
    </ResponsiveCardGrid>
  );
}

AssetsListView.propTypes = {
  assets: ImmutablePropTypes.listOf(
    ImmutablePropTypes.mapContains({
      id: PropTypes.string,
    }),
  ).isRequired,
  favoritePagination: ImmutablePropTypes.map.isRequired,
  requesting: PropTypes.bool.isRequired,
};

AssetsListView.defaultProps = {
  favoritePagination: Map(),
  requesting: false,
};

export default AssetsListView;
