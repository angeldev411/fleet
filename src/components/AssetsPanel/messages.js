import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  campaignStatus: {
    id: 'components.AssetsPanel.AssetsListView.campaignStatus',
    defaultMessage: 'Campaigns',
  },
  engine: {
    id: 'components.AssetsPanel.AssetsListView.engine',
    defaultMessage: 'Engine',
  },
  make: {
    id: 'components.AssetsPanel.AssetsListView.make',
    defaultMessage: 'Make',
  },
  model: {
    id: 'components.AssetsPanel.AssetsListView.model',
    defaultMessage: 'Model',
  },
  odometer: {
    id: 'components.AssetsPanel.AssetsListView.odometer',
    defaultMessage: 'Odometer',
  },
  other: {
    id: 'components.AssetsPanel.AssetsListView.other',
    defaultMessage: 'Other',
  },
  pmStatus: {
    id: 'components.AssetsPanel.AssetsListView.pmStatus',
    defaultMessage: 'PM Status',
  },
  serial: {
    id: 'components.AssetsPanel.AssetsListView.serial',
    defaultMessage: 'Serial No.',
  },
  unitNumber: {
    id: 'components.AssetsPanel.AssetsListView.unitNumber',
    defaultMessage: 'Unit',
  },
  vin: {
    id: 'components.AssetsPanel.AssetsListView.vin',
    defaultMessage: 'VIN',
  },
  warrantyStatus: {
    id: 'components.AssetsPanel.AssetsListView.warrantyStatus',
    defaultMessage: 'Warranty',
  },
  year: {
    id: 'components.AssetsPanel.AssetsListView.year',
    defaultMessage: 'Year',
  },
});

export default formattedMessages;
