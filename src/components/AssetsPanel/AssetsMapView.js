import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Link } from 'react-router-dom';

function buildAssetList(assets) {
  return assets.map((c) => {
    const assetId = c.get('id');
    return (
      <li key={assetId}>
        <Link to={`/assets/${assetId}`}>
          {`Asset ID: ${assetId}`}
        </Link>
      </li>
    );
  });
}

function AssetsMapView({
  assets,
}) {
  return (
    <div style={{ padding: '0.5rem' }}>
      <h2>TODO: Build MAP view</h2>
      <ul>
        {buildAssetList(assets)}
      </ul>
    </div>
  );
}

AssetsMapView.propTypes = {
  assets: ImmutablePropTypes.listOf(
    ImmutablePropTypes.mapContains({
      id: PropTypes.string,
    }),
  ).isRequired,
};

export default AssetsMapView;
