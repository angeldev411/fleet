import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { messageDescriptorPropTypes } from 'react-intl';

import AssetDisplayContainer from 'containers/AssetDisplayContainer';

import CategoryHeader from 'components/CategoryHeader';
import paginateFavorites from 'components/_common/paginateFavorites';
import CardView from 'components/_common/CardView';

import PageContentPanel from 'elements/PageContentPanel';

import AssetsListView from './AssetsListView';
import AssetsMapView from './AssetsMapView';

function getAssetsView(assetsView) {
  switch (assetsView) {
    case 'list':
      return AssetsListView;
    case 'map':
      return AssetsMapView;
    case 'card':
    default:
      return CardView;
  }
}

class AssetsPanel extends Component {
  static propTypes = {
    assets: ImmutablePropTypes.list.isRequired,
    assetsRequesting: PropTypes.bool,
    assetsView: PropTypes.string.isRequired,
    favoritePagination: ImmutablePropTypes.map.isRequired,
    latestFavorite: PropTypes.shape({
      message: PropTypes.shape(messageDescriptorPropTypes),
    }),
    refreshAssets: PropTypes.func.isRequired,
    requestNext: PropTypes.func.isRequired,
  };

  static defaultProps = {
    assetsRequesting: false,
    latestFavorite: {},
  };

  componentWillMount() {
    this.PaginatedAssetsView = this.buildPaginatedAssetsView(this.props.assetsView);
  }

  componentWillReceiveProps(nextProps) {
    // If the users has changed view types, a new HOC wrapped component must be made.
    if (this.props.assetsView !== nextProps.assetsView) {
      this.PaginatedAssetsView = this.buildPaginatedAssetsView(nextProps.assetsView);
    }
  }

  buildPaginatedAssetsView = (viewType) => {
    // define the component that will be displayed with the super pagination powers.
    const AssetsView = getAssetsView(viewType);

    // add methods concerning pagination
    const paginationMethods = {
      requestNext: this.props.requestNext,
    };

    return paginateFavorites(paginationMethods)(AssetsView);
  }

  render() {
    const {
      assets,
      assetsRequesting,
      refreshAssets,
      favoritePagination,
      latestFavorite,
    } = this.props;

    const { PaginatedAssetsView } = this;

    const componentProps = {
      assets,
      dataList: assets,
      requesting: assetsRequesting,
      favoritePagination,
      CardComp: AssetDisplayContainer,
    };

    return (
      <PageContentPanel id="assets-panel">
        <CategoryHeader
          favoritePagination={favoritePagination}
          refresh={refreshAssets}
          requesting={assetsRequesting}
          type="asset"
        />
        <PaginatedAssetsView
          componentProps={componentProps}
          favoritePagination={favoritePagination}
          noFavoritesMessage={latestFavorite.message}
          requestInProgress={assetsRequesting}
        />
      </PageContentPanel>
    );
  }
}

export default AssetsPanel;
export AssetsListView from './AssetsListView';
