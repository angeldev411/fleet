import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';
import { fromJS, List, Map } from 'immutable';

import AssetsListView from '../AssetsListView';

const assets = fromJS([
  { id: '123' },
  { id: '456' },
  { id: '789' },
]);

const defaultProps = {
  assets: List(),
  favoritePagination: Map(),
  requesting: false,
};

function renderComponent(props = defaultProps) {
  return shallow(<AssetsListView {...props} />);
}

test('AssetsListView renders a ListTable', () => {
  const component = renderComponent();
  expect(component).toContain('ListTable');
});

test('AssetsListView renders all headers for the table', () => {
  const component = renderComponent();
  const tableHeaders = component.find('th');
  expect(tableHeaders.length).toEqual(12);
});

test('AssetsListView renders 10 ghost cards if no assets provided', () => {
  const component = renderComponent();
  expect(component.find('ListRowLoading').length).toEqual(10);
});

test('AssetsListView renders diff of assets if less than 10', () => {
  const favoritePagination = Map({ totalCount: 8 });
  const requesting = true;
  const component = renderComponent({ assets, favoritePagination, requesting });
  expect(component.find('ListRowLoading').length).toEqual(5);
});

test('Does not render ghosts when it is not requesting and currentCount is not zero', () => {
  const favoritePagination = Map({ totalCount: 8 });
  const requesting = false;
  const component = renderComponent({ assets, favoritePagination, requesting });
  expect(component).toNotContain('ListRowLoading');
});
