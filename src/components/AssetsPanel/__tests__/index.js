import React from 'react';
import { noop } from 'lodash';
import { List, Map } from 'immutable';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import AssetsPanel from '../index';

const defaultProps = {
  assets: List(),
  assetsView: '',
  favoritePagination: Map(),
  refreshAssets: noop,
  requestNext: noop,
};

function renderComponent(props = defaultProps) {
  return shallow(<AssetsPanel {...props} />);
}

test('AssetsPanel contains CategoryHeader', () => {
  const component = renderComponent();
  expect(component).toContain('CategoryHeader');
});

test('AssetsPanel renders a FavoritesPaginator by default', () => {
  const component = renderComponent();
  expect(component).toContain('FavoritesPaginator');
});

test('AssetsPanel renders card view by default', () => {
  const component = renderComponent({ ...defaultProps });
  expect(component.find('FavoritesPaginator').dive()).toContain('CardView');
});

test('AssetsPanel renders card view with assetsView === card', () => {
  const component = renderComponent({ ...defaultProps, assetsView: 'card' });
  expect(component.find('FavoritesPaginator').dive()).toContain('CardView');
});

test('AssetsPanel renders list view with assetsView === list', () => {
  const component = renderComponent({ ...defaultProps, assetsView: 'list' });
  expect(component.find('FavoritesPaginator').dive()).toContain('AssetsListView');
});

test('AssetsPanel renders map view with assetsView === map', () => {
  const component = renderComponent({ ...defaultProps, assetsView: 'map' });
  expect(component.find('FavoritesPaginator').dive()).toContain('AssetsMapView');
});

test('AssetsPanel renders the correct view after assetsView is changed', () => {
  const component = renderComponent({ ...defaultProps, assetsView: 'list' });
  component.setProps({ assetsView: 'map' });
  expect(component.find('FavoritesPaginator').dive()).toContain('AssetsMapView');
});

test('AssetsPanel renders the correct view after assetsView is not changed', () => {
  const component = renderComponent({ ...defaultProps, assetsView: 'list' });
  component.setProps({ assetsView: 'list' });
  expect(component.find('FavoritesPaginator').dive()).toContain('AssetsListView');
});
