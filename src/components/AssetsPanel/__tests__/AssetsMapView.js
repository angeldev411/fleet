import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';
import { fromJS, List } from 'immutable';

import AssetsMapView from '../AssetsMapView';

const defaultProps = {
  assets: List(),
};

function renderComponent(props = defaultProps) {
  return shallow(<AssetsMapView {...props} />);
}

test('AssetsMapView renders a "TODO" title', () => {
  const component = renderComponent();
  expect(component.find('h2').text()).toInclude('TODO:');
});

test('AssetsMapView renders one link per asset', () => {
  const assets = fromJS([
    { id: '1' },
    { id: '2' },
    { id: '3' },
  ]);
  const component = renderComponent({ ...defaultProps, assets });
  expect(component.find('ul').length).toEqual(1);
  expect(component.find('li').length).toEqual(3);
  expect(component.find('Link').length).toEqual(3);
});
