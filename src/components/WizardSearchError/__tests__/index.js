import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import WizardSearchError from '../index';

const defaultProps = {
  searchValue: 'test search value',
  titlePrefix: {
    id: 'prefix id',
    defaultMessage: 'title prefix',
  },
  titleSuffix: {
    id: 'suffix id',
    defaultMessage: 'title suffix',
  },
  subtitle: {
    id: 'subtitle id',
    defaultMessage: 'subtitle',
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<WizardSearchError {...props} />);
}

test('renders a search icon', () => {
  const component = shallowRender();
  expect(component).toContain('FontAwesome[name="search"]');
});

test('renders the title prefix & suffix under the search icon', () => {
  const prefixMessage = shallowRender().find('FormattedMessage').at(0);
  expect(prefixMessage).toHaveProps({ ...defaultProps.titlePrefix });
  const suffixMessage = shallowRender().find('FormattedMessage').at(1);
  expect(suffixMessage).toHaveProps({ ...defaultProps.titleSuffix });
});

test('renders the search query', () => {
  const component = shallowRender().find('TextDiv');
  expect(component.html()).toInclude(defaultProps.searchValue);
});

test('renders the subtitle with correct error message when there is no match', () => {
  const component = shallowRender().find('FormattedMessage').last();
  expect(component).toHaveProps({ ...defaultProps.subtitle });
});
