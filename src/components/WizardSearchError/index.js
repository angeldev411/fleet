import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';
import { H4, P } from 'base-components';
import { px2rem } from 'decisiv-ui-utils';

import TextDiv from 'elements/TextDiv';

import Wrapper from './Wrapper';

function WizardSearchError({ searchValue, titlePrefix, titleSuffix, subtitle }) {
  return (
    <Wrapper>
      <FontAwesome name="search" />
      <H4 modifiers={['fontWeightRegular']} style={{ marginTop: px2rem(20) }}>
        <FormattedMessage {...titlePrefix} />
        <TextDiv modifiers={['bold', 'inline']}>
          {`${searchValue}`}
        </TextDiv>
        <FormattedMessage {...titleSuffix} />
      </H4>
      <P modifiers={['textLight']} style={{ marginTop: px2rem(6) }}>
        <FormattedMessage {...subtitle} />
      </P>
    </Wrapper>
  );
}

WizardSearchError.propTypes = {
  searchValue: PropTypes.string.isRequired,
  titlePrefix: PropTypes.shape(messageDescriptorPropTypes).isRequired,
  titleSuffix: PropTypes.shape(messageDescriptorPropTypes).isRequired,
  subtitle: PropTypes.shape(messageDescriptorPropTypes).isRequired,
};

export default WizardSearchError;
