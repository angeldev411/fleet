import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import Tooltip from 'elements/Tooltip';

import StatusCircle from './StatusCircle';
import StatusIcon from './StatusIcon';
import StatusValue from './StatusValue';

const DEFAULT_COLOR = 'green';

export function StatusLabel({ color: rawColor, label, showIcon, tooltip }) {
  const color = rawColor || DEFAULT_COLOR;
  const target = (
    <div>
      {
        showIcon
          ? <StatusIcon color={color} />
          : <StatusCircle color={color} />
      }
      { label &&
        <StatusValue>
          <FormattedMessage {...label} />
        </StatusValue>
      }
    </div>
  );
  if (!tooltip) return target;
  return (
    <Tooltip
      position="top"
      message={tooltip}
    >
      {target}
    </Tooltip>
  );
}

StatusLabel.propTypes = {
  color: PropTypes.string,
  label: PropTypes.shape({
    id: PropTypes.string.isRequired,
    defaultMessage: PropTypes.string.isRequired,
  }),
  showIcon: PropTypes.bool,
  tooltip: PropTypes.shape({
    id: PropTypes.string.isRequired,
    defaultMessage: PropTypes.string.isRequired,
  }),
};

StatusLabel.defaultProps = {
  color: DEFAULT_COLOR,
  showIcon: false,
  tooltip: null,
  label: null,
};

export default StatusLabel;
