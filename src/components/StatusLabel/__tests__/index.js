import React from 'react';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import { withTestTheme } from 'utils/styles';

import { StatusLabel } from '../index';

const StatusLabelWithTestTheme = withTestTheme(StatusLabel);

const labelId = 'Label ID';
const labelDefaultMessage = 'Label default message';
const defaultStatusInfo = {
  color: 'green',
  label: {
    id: labelId,
    defaultMessage: labelDefaultMessage,
  },
  showIcon: false,
  tooltip: { id: 'tooltip', defaultMessage: 'ToolTip' },
};

function renderComponent(statusInfo = defaultStatusInfo) {
  return shallow(
    <StatusLabelWithTestTheme
      {...statusInfo}
    />,
  );
}

test('Renders the Tooltip when tooltip is there', () => {
  const component = renderComponent();
  expect(component).toBeA('Tooltip');
});

test('Renders the div when tooltip is null', () => {
  const component = renderComponent({ ...defaultStatusInfo, tooltip: null });
  expect(component).toBeA('div');
});

test('Renders the status circle with correct color', () => {
  const color = 'yellow';
  const component = renderComponent({ ...defaultStatusInfo, color });
  expect(component.find(`StatusCircle[color="${color}"]`).length).toEqual(1);
});

test('Renders the status icon with correct color when showIcon is true', () => {
  const showIcon = true;
  const color = 'red';
  const component = renderComponent({ ...defaultStatusInfo, showIcon, color });
  expect(component.find(`StatusIcon[color="${color}"]`).length).toEqual(1);
});

test('Renders the status text with correct value', () => {
  const component = renderComponent();
  const statusValue = component.find('StatusValue');
  const statusValueMessage = statusValue.find('FormattedMessage');
  expect(statusValue.length).toEqual(1);
  expect(statusValueMessage.length).toEqual(1);
  expect(statusValueMessage).toHaveProp('id', labelId);
  expect(statusValueMessage).toHaveProp('defaultMessage', labelDefaultMessage);
});

test('Renders the status icon with correct color when showIcon is true and color is null', () => {
  const showIcon = true;
  const color = null;
  const component = renderComponent({ ...defaultStatusInfo, showIcon, color }).find('StatusIcon');
  expect(component).toHaveProp('color', 'green');
});
