import { theme } from 'decisiv-ui-utils';

import { test, expect } from '__tests__/helpers/test-setup';

import { statusColor } from '../StatusIconWrapper';

// -------------------- statusColor --------------------

test('statusColor returns the red status color', () => {
  expect(statusColor({ theme, color: 'red' })).toEqual(theme.colors.status.danger);
});

test('statusColor returns the yellow status color', () => {
  expect(statusColor({ theme, color: 'yellow' })).toEqual(theme.colors.status.warning);
});

test('statusColor returns the grey status color', () => {
  expect(statusColor({ theme, color: 'grey' })).toEqual(theme.colors.status.default);
});

test('statusColor returns the green status color', () => {
  expect(statusColor({ theme, color: 'green' })).toEqual(theme.colors.status.success);
});

test('statusColor returns status green color by default', () => {
  expect(statusColor({ theme, color: 'nonsense' })).toEqual(theme.colors.status.success);
});
