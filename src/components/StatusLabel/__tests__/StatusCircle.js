import { theme } from 'decisiv-ui-utils';

import { test, expect } from '__tests__/helpers/test-setup';

import { backgroundColor } from '../StatusCircle';

// -------------------- backgroundColor --------------------

test('backgroundColor returns the red status color', () => {
  expect(backgroundColor({ theme, color: 'red' })).toEqual(theme.colors.status.danger);
});

test('backgroundColor returns the yellow status color', () => {
  expect(backgroundColor({ theme, color: 'yellow' })).toEqual(theme.colors.status.warning);
});

test('backgroundColor returns the green status color', () => {
  expect(backgroundColor({ theme, color: 'green' })).toEqual(theme.colors.status.success);
});

test('backgroundColor returns status green by default', () => {
  expect(backgroundColor({ theme, color: 'nonsense' })).toEqual(theme.colors.status.success);
});
