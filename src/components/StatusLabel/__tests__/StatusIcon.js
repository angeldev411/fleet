import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';


import StatusIcon, { iconName } from '../StatusIcon';

const defaultProps = {
  color: 'yellow',
};

// -------------------- statusIcon --------------------
function renderComponent(props = defaultProps) {
  return shallow(<StatusIcon {...props} />);
}

test('Renders StatusIconWrapper', () => {
  expect(renderComponent()).toContain('StatusIconWrapper');
});

test('Renders FontAwesome', () => {
  const name = iconName(defaultProps.color);
  expect(renderComponent()).toContain(`FontAwesome[name='${name}']`);
});

// -------------------- iconName --------------------

test('iconName returns the red status icon', () => {
  expect(iconName('red')).toEqual('times');
});

test('iconName returns the yellow status icon', () => {
  expect(iconName('yellow')).toEqual('exclamation-circle');
});

test('iconName returns the green status icon', () => {
  expect(iconName('green')).toEqual('check');
});

test('iconName returns the grey status icon', () => {
  expect(iconName('grey')).toEqual('minus');
});

test('iconName returns status icon check by default', () => {
  expect(iconName('nonsense')).toEqual('check');
});
