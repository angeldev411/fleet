import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';

import StatusIconWrapper from './StatusIconWrapper';

export function iconName(color) {
  switch (color) {
    case 'red':
      return 'times';
    case 'yellow':
      return 'exclamation-circle';
    case 'grey':
      return 'minus';
    case 'green':
    default:
      return 'check';
  }
}

function StatusIcon({ color }) {
  return (
    <StatusIconWrapper color={color}>
      <FontAwesome name={iconName(color)} />
    </StatusIconWrapper>
  );
}

StatusIcon.propTypes = {
  color: PropTypes.oneOf([
    'red',
    'yellow',
    'green',
    'grey',
  ]).isRequired,
};

StatusIcon.defaultProps = {
  color: 'green',
};

export default StatusIcon;
