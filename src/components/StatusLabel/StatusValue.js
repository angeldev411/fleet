import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const styles = props => `
  vertical-align: middle;
  text-transform: uppercase;
  margin-left: ${px2rem(11)};
  font-size: ${props.theme.dimensions.fontSizeNormal};
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    fontSizeNormal: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'StatusValue',
  styled.span,
  styles,
  { themePropTypes },
);
