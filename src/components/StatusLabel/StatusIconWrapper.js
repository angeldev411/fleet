import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

export function statusColor({ color, theme }) {
  switch (color) {
    case 'red':
      return theme.colors.status.danger;
    case 'yellow':
      return theme.colors.status.warning;
    case 'grey':
      return theme.colors.status.default;
    case 'green':
    default:
      return theme.colors.status.success;
  }
}

/* istanbul ignore next */
const styles = props => `
  color: ${statusColor(props)};
  display: inline-block;
  height: ${px2rem(props.theme.dimensions.statusCircleRadius * 2)};
  vertical-align: middle;
  width: ${px2rem(props.theme.dimensions.statusCircleRadius * 2)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
      default: PropTypes.string.isRequired,
      success: PropTypes.string.isRequired,
      warning: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    statusCircleRadius: PropTypes.number.isRequired,
  }).isRequired,
};

const propTypes = {
  color: PropTypes.oneOf([
    'red',
    'yellow',
    'green',
    'grey',
  ]).isRequired,
};

const defaultProps = {
  color: 'green',
};

export default buildStyledComponent(
  'StatusIconWrapper',
  styled.div,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
