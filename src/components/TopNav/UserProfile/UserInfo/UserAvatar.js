import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import Gravatar from 'elements/Gravatar';

const AVATAR_SIZE_PX = 34;

/* istanbul ignore next */
const styles = props => `
  img {
    border-radius: ${px2rem(AVATAR_SIZE_PX / 2)};
    width: ${px2rem(AVATAR_SIZE_PX)};
    height: ${px2rem(AVATAR_SIZE_PX)};
    margin-right: ${props.theme.dimensions.spacingNormal};
    vertical-align: middle;
  }
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    spacingNormal: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'UserAvatar',
  styled(Gravatar),
  styles,
  { themePropTypes },
);
