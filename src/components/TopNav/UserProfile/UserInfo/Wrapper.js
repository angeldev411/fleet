import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

export function wrapperColor({ theme, expanded }) {
  if (expanded) {
    return theme.colors.base.linkHover;
  }

  return theme.colors.base.chrome500;
}

/* istanbul ignore next */
const styles = props => `
  color: ${wrapperColor(props)};

  .fa-caret-down {
    font-size: ${props.theme.dimensions.fontSizeMedium};
  }

  &:hover {
    color: ${props.theme.colors.base.linkHover};

    span:not(.fa) {
      text-decoration: underline;
    }
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome500: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    fontSizeMedium: PropTypes.string.isRequired,
  }).isRequired,
};

const propTypes = {
  expanded: PropTypes.bool,
};

const defaultProps = {
  expanded: false,
};

export default buildStyledComponent(
  'UserInfoWrapper',
  styled.div,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
