import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  display: block;
  font-size: ${props.theme.dimensions.fontSizeNormal};
  font-weight: ${props.bold ? '500' : '300'};
  line-height: ${px2rem(17)};
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    fontSizeNormal: PropTypes.string.isRequired,
  }).isRequired,
};

const propTypes = {
  bold: PropTypes.bool,
};

const defaultProps = {
  bold: false,
};

export default buildStyledComponent(
  'UserInfoLabel',
  styled.span,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
