import { test, expect } from '__tests__/helpers/test-setup';

import { wrapperColor } from '../Wrapper';

const linkHover = 'linkHover';
const chrome500 = 'chrome500';
const theme = {
  colors: {
    base: {
      linkHover,
      chrome500,
    },
  },
};

test('Uses linkHover color when expanded', () => {
  expect(wrapperColor({ theme, expanded: true })).toEqual(linkHover);
});

test('Uses chrome500 color by default', () => {
  expect(wrapperColor({ theme, expanded: false })).toEqual(chrome500);
});
