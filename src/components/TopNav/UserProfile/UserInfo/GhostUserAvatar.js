import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import GhostIndicator from 'components/_common/GhostIndicator';

const AVATAR_SIZE_PX = 34;

const styles = props => `
  border-radius: ${px2rem(AVATAR_SIZE_PX / 2)};
  width: ${px2rem(AVATAR_SIZE_PX)};
  height: ${px2rem(AVATAR_SIZE_PX)};
  margin: 0 ${props.theme.dimensions.spacingNormal} 0 0;
  vertical-align: middle;
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    spacingNormal: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'GhostUserAvatar',
  styled(GhostIndicator),
  styles,
  { themePropTypes },
);
