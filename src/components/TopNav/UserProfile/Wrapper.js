import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const styles = `
  cursor: pointer;
  div {
    display: inline-block;
    vertical-align: middle;
  }
`;

export default buildStyledComponent(
  'UserProfileWrapper',
  styled.div,
  styles,
);
