import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  background: #fff;
  box-shadow: 0 ${px2rem(3)} ${px2rem(7)} 0 ${props.theme.colors.base.shadow};
  box-sizing: border-box;
  cursor: auto;
  height: auto;
  margin: ${px2rem(10)};
  overflow: hidden;
  padding: ${px2rem(10)};
  text-align: right;
  width: ${px2rem(215)};

  .menu-item {
    a {
      text-decoration: none;
      &:hover {
        text-decoration: underline;
      }
    }
    span {
      color: ${props.theme.colors.base.textLight};
      font-size: ${props.theme.dimensions.fontSizeNormal};
    }
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      shadow: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    fontSizeNormal: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'UserPopupWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
