import React from 'react';

import LoadingBar from 'compositions/LoadingBar';

import Controls from './Controls';
import Logo from './Logo';
import TopNavWrapper from './Wrapper';

function TopNav() {
  return (
    <TopNavWrapper id="top-nav">
      <Logo />
      <Controls />
      <LoadingBar />
    </TopNavWrapper>
  );
}

export default TopNav;
