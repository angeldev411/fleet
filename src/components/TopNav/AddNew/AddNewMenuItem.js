import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const AddNewMenuItem = styled.li`
  font-size: ${px2rem(12)};
  list-style-type: none;
  padding: ${px2rem(8)};
`;

export default AddNewMenuItem;
