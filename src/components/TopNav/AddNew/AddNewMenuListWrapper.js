import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const AddNewMenuList = styled.ul`
  margin: 0;
  min-width: ${px2rem(198)};
  padding: 0;
`;

export default AddNewMenuList;
