import messages from './messages';

// eslint-disable-next-line import/prefer-default-export
export const ADD_NEW_MENU_ITEMS = [
  {
    id: 'createNewServiceRequest',
    label: messages.createNewServiceRequest,
    href: '/service-requests/new',
  },
];
