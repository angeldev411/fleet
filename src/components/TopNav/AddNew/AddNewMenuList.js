import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Link from 'elements/Link';
import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';
import { noop } from 'lodash';

import AddNewMenuListWrapper from './AddNewMenuListWrapper';
import AddNewMenuItem from './AddNewMenuItem';

export function AddNewMenuList({ location, menuItems, hidePopover }) {
  return (
    <AddNewMenuListWrapper>
      {
        menuItems.map(menuItem => (
          <AddNewMenuItem
            key={menuItem.id}
            onClick={hidePopover}
          >
            <Link
              to={{
                pathname: menuItem.href,
                state: { previousPage: location.pathname },
              }}
              modifiers={['textColor']}
            >
              <FormattedMessage {...menuItem.label} />
            </Link>
          </AddNewMenuItem>
        ))
      }
    </AddNewMenuListWrapper>
  );
}

AddNewMenuList.propTypes = {
  // This prop is added by Popover component's getContent() method
  hidePopover: PropTypes.func,

  menuItems: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    label: PropTypes.shape(messageDescriptorPropTypes).isRequired,
  })).isRequired,

  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

AddNewMenuList.defaultProps = {
  hidePopover: noop,
};

export default withRouter(AddNewMenuList);
