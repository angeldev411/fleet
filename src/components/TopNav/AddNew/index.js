import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';
import { px2rem } from 'decisiv-ui-utils';

import CircleButton from 'elements/CircleButton';
import Popover, { PopoverTarget, PopoverContent } from 'elements/Popover';

import AddNewMenuList from './AddNewMenuList';

import { ADD_NEW_MENU_ITEMS } from './constants';

class AddNew extends Component {
  state = {
    popoverVisible: false,
  }

  handlePopoverHide = () => {
    this.setState({ popoverVisible: false });
  }

  togglePopoverVisibility = () => {
    this.setState(prevState => ({ popoverVisible: !prevState.popoverVisible }));
  }

  render() {
    const buttonStyles = this.state.popoverVisible ? 'brandPrimary' : 'brandSecondary';

    return (
      <Popover
        position="bottom"
        onHide={this.handlePopoverHide}
      >
        <PopoverTarget>
          <CircleButton
            modifiers={[buttonStyles, 'hoverBrand', 'hoverShadow']}
            onClick={this.togglePopoverVisibility}
          >
            <FontAwesome name="plus" style={{ lineHeight: px2rem(31) }} />
          </CircleButton>
        </PopoverTarget>
        <PopoverContent>
          <AddNewMenuList menuItems={ADD_NEW_MENU_ITEMS} />
        </PopoverContent>
      </Popover>
    );
  }
}

export default AddNew;
