import { defineMessages } from 'react-intl';

const messages = defineMessages({
  createNewServiceRequest: {
    id: 'components.TopNav.AddNew.MenuItem.createNewServiceRequest',
    defaultMessage: 'Create New Service Request',
  },
});

export default messages;
