import React from 'react';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import AddNew from '../index';
import { ADD_NEW_MENU_ITEMS } from '../constants';

const defaultState = {
  popoverVisible: false,
};

function shallowRender() {
  return shallow(<AddNew />);
}

test('renders a Popover and sets the correct onHide callback handler', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(component).toContain('PopoverTarget');
  expect(component).toContain('PopoverContent');
  expect(component.find('Popover')).toHaveProp('onHide', instance.handlePopoverHide);
});

test('renders a CircleButton with plus sign', () => {
  const component = shallowRender();
  expect(component).toContain('CircleButton');

  const addButton = component.find('CircleButton');
  expect(addButton.find('FontAwesome')).toHaveProp('name', 'plus');
});

test('renders Add New menu list with default menu items', () => {
  const component = shallowRender();
  expect(component).toContain('withRouter(AddNewMenuList)');
  expect(component.find('withRouter(AddNewMenuList)')).toHaveProp('menuItems', ADD_NEW_MENU_ITEMS);
});

/* --------------------- CircleButton active state --------------------- */

test('AddNew button includes the brandSecondary modifier if popover is hidden', () => {
  const component = shallowRender();
  const plusButton = component.find('CircleButton');
  expect(plusButton.props().modifiers).toContain('brandSecondary');
});

test('AddNew button includes the brandPrimary modifier if popover is visible', () => {
  const component = shallowRender();
  component.setState({ popoverVisible: true });
  const plusButton = component.find('CircleButton');
  expect(plusButton.props().modifiers).toContain('brandPrimary');
});

test('sets the popoverVisible state to false when the popover is hidden', () => {
  const component = shallowRender();
  const instance = component.instance();
  component.setState({ popoverVisible: true });
  instance.handlePopoverHide();
  expect(component.state()).toEqual({ popoverVisible: false });
});

test('toggles the popoverVisible state when the plus button is clicked', () => {
  const component = shallowRender();
  const plusButton = component.find('CircleButton');

  component.setState(defaultState);
  plusButton.simulate('click');
  expect(component.state()).toEqual({ popoverVisible: true });
  component.setState({ popoverVisible: true });
  plusButton.simulate('click');
  expect(component.state()).toEqual(defaultState);
});
