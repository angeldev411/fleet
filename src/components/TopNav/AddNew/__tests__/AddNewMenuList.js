import React from 'react';
import { noop } from 'lodash';

import { test, expect, shallow, createSpy } from '__tests__/helpers/test-setup';

import { AddNewMenuList } from '../AddNewMenuList';
import messages from '../messages';

const menuItems = [
  {
    id: '123',
    label: messages.createNewServiceRequest,
    href: '/some-url',
  },
];
const defaultProps = {
  menuItems,
  hidePopover: noop,
  location: {
    pathname: '/cases',
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<AddNewMenuList {...props} />);
}

test('renders all menu items', () => {
  const component = shallowRender();
  expect(component.find('AddNewMenuItem').length).toEqual(menuItems.length);
});

test('menu item renders a Link that is linked to its href', () => {
  const menuItem = shallowRender().find('AddNewMenuItem').first();
  expect(menuItem).toContain('Link');
  expect(menuItem.find('Link').props().to).toEqual({
    pathname: menuItems[0].href,
    state: { previousPage: defaultProps.location.pathname },
  });
});

test('menu item renders the FormattedMessage with its label', () => {
  const menuItem = shallowRender().find('AddNewMenuItem').first();
  expect(menuItem).toContain('FormattedMessage');
  expect(menuItem.find('FormattedMessage')).toHaveProps({ ...menuItems[0].label });
});

test('clicking a menu item triggers hidePopover prop function call', () => {
  const hidePopoverSpy = createSpy();
  const testProps = {
    ...defaultProps,
    hidePopover: hidePopoverSpy,
  };
  const menuItem = shallowRender(testProps).find('AddNewMenuItem').first();
  menuItem.simulate('click');
  expect(hidePopoverSpy).toHaveBeenCalled();
});
