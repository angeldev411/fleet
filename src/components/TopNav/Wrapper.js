import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.background};
  box-sizing: border-box;
  display: flex;
  height: ${props.theme.dimensions.topNavHeight};
  justify-content: space-between;
  overflow: visible;
  padding: ${px2rem(22)} ${px2rem(20)};
  position: relative;
  z-index: 10;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      background: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    topNavHeight: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'TopNavWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
