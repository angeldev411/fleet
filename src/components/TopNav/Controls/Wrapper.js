import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

// Controls section (right side)
/* istanbul ignore next */
const styles = props => `
  display: block;
  height: 100%;
  margin-left: auto;
  > div {
    display: inline-block;
    vertical-align: middle;
    margin-left: ${props.theme.dimensions.spacingNormal};
  }
  > button {
    margin-left: ${props.theme.dimensions.spacingNormal};
  }
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    spacingNormal: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'ControlsWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
