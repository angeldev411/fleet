import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { Layout } from 'components/Layout';

test('Renders the children', () => {
  const TestComponent = () => (<div id="test-component" />);
  const component = shallow(
    <Layout>
      <TestComponent />
    </Layout>,
  );
  expect(component).toContain(TestComponent);
});
