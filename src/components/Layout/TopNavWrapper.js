import styled from 'styled-components';

export default styled.section`
  flex: 1;
  order: 1;
  width: 100%;
  max-height: 5rem;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 2;
`;
