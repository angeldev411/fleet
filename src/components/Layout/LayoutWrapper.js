import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  font-family: ${props.theme.fonts.primary};
`;

const themePropTypes = {
  fonts: PropTypes.shape({
    primary: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'LayoutWrapper',
  styled.section,
  styles,
  { themePropTypes },
);

