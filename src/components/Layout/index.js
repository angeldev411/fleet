import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { compose, setDisplayName } from 'recompose';

import TopNav from 'components/TopNav';

import LeftNav from 'compositions/LeftNav';

import AppContainer from 'containers/AppContainer';
import withFullScreenMode from 'containers/withFullScreenMode';
import withPrint from 'containers/withPrint';

import helmetAppConfig from 'setup/helmet';

import LayoutWrapper from './LayoutWrapper';
import TopNavWrapper from './TopNavWrapper';
import PageWrapper from './PageWrapper';

export function Layout({ children }) {
  return (
    <LayoutWrapper id="layout-wrapper">
      <Helmet {...helmetAppConfig} />
      <TopNavWrapper>
        <TopNav />
      </TopNavWrapper>
      <PageWrapper id="page-wrapper">
        <LeftNav />
        <AppContainer>{children}</AppContainer>
      </PageWrapper>
    </LayoutWrapper>
  );
}

Layout.propTypes = {
  children: PropTypes.node,
};

Layout.defaultProps = {
  children: null,
};

export default compose(
  setDisplayName('Layout'),
  withFullScreenMode,
  withPrint,
)(Layout);
