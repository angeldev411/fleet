import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

export default styled.section`
  flex: 1;
  order: 2;
  display: flex;
  flex-direction: row;
  position: relative;
  height: 100%;
  top: ${px2rem(72)};
`;
