import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  border-top: ${px2rem(2)} solid ${props.theme.colors.base.chrome300};
  display: flex;
  margin: ${px2rem(30)} ${px2rem(16)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
  { themePropTypes },
);
