import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './Wrapper';
import Logo from './Logo';
import InfoWrapper from './InfoWrapper';

import footerLogo from './footer_logo.png';

function GlobalFooter({ isBranded, version }) {
  return (
    <Wrapper>
      {isBranded && <Logo alt="Decisiv Logo" src={footerLogo} />}
      <InfoWrapper version={version} />
    </Wrapper>
  );
}

GlobalFooter.propTypes = {
  isBranded: PropTypes.bool,
  version: PropTypes.string.isRequired,
};

GlobalFooter.defaultProps = {
  isBranded: false,
};

export default GlobalFooter;
