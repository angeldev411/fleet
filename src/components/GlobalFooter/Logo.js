import styled from 'styled-components';
import Img from 'elements/Img';

const Logo = styled(Img)`
  width: auto;
  height: 120px;

  /* FIXME: Remove below when the app is branded for a customer */
  display: none;
`;

export default Logo;
