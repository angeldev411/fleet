import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import GlobalFooter from '../index';

const renderComponent = ({ version = '0', ...rest } = {}) =>
  shallow(<GlobalFooter version={version} {...rest} />);

test('Renders the logo when isBranded is true', () => {
  const renderedComponent = renderComponent({ isBranded: true });
  expect(renderedComponent).toContain('Logo');
});

test('Does not render the logo when isBranded is false', () => {
  const renderedComponent = renderComponent({ isBranded: false });
  expect(renderedComponent).toNotContain('Logo');
});

test('Renders an InfoWrapper with the given version', () => {
  const version = '123.567';
  const renderedComponent = renderComponent({ version });
  expect(renderedComponent).toContain('InfoWrapper');
  expect(renderedComponent.find('InfoWrapper')).toHaveProp('version', version);
});
