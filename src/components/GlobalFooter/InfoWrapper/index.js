import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Link from 'elements/Link';

import { configEnabled } from 'utils/config';

// eslint-disable-next-line import/no-unresolved
import runtimeConfig from 'runtimeConfig.json';

import Wrapper from './Wrapper';
import TextWrapper from './TextWrapper';

import {
  // BLOG_URL,
  TERMS_OF_USE_URL,
  PRIVACY_POLICY_URL,
} from './constants';
import messages from './messages';

function renderVersion(version) {
  const gitVersion = process.env.GIT_COMMIT;
  const msgValues = {
    app: runtimeConfig.title,
    version,
    gitVersion,
  };
  const showGitRevision = gitVersion && configEnabled('SHOW_GIT_REVISION');
  const msg = showGitRevision ? messages.versionWithGit : messages.version;
  return <FormattedMessage {...msg} values={msgValues} />;
}

function InfoWrapper({ version }) {
  function getCurrentYear() {
    return new Date().getFullYear();
  }

  return (
    <Wrapper>
      <TextWrapper>
        <FormattedMessage {...messages.copyrightText} values={{ year: getCurrentYear() }} /> |
        <Link
          modifiers={['padded', 'textColor']}
          target="fleet_footer_link"
          to={TERMS_OF_USE_URL}
        >
          <FormattedMessage {...messages.termsOfUse} />
        </Link>|
        <Link
          modifiers={['padded', 'textColor']}
          target="fleet_footer_link"
          to={PRIVACY_POLICY_URL}
        >
          <FormattedMessage {...messages.privacyPolicy} />
        </Link>
        {/* FIXME: Show the blog for a customer if needed by uncommenting below */}
        {/* |<Link target="_blank" to={BLOG_URL}>Decisiv Blog</Link> */}
      </TextWrapper>
      <TextWrapper>
        {renderVersion(version)}
      </TextWrapper>
    </Wrapper>
  );
}

InfoWrapper.propTypes = {
  version: PropTypes.string.isRequired,
};

export default InfoWrapper;
