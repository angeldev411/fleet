import { defineMessages } from 'react-intl';

const messages = defineMessages({
  copyrightText: {
    id: 'components.GlobalFooter.message.copyrightText',
    defaultMessage: 'Copyright © {year} Decisiv, Inc.',
  },
  termsOfUse: {
    id: 'components.GlobalFooter.message.termsOfUse',
    defaultMessage: 'Terms of Use',
  },
  privacyPolicy: {
    id: 'components.GlobalFooter.message.privacyPolicy',
    defaultMessage: 'Privacy Policy',
  },
  version: {
    id: 'components.GlobalFooter.message.version',
    defaultMessage: '{app} version {version}',
  },
  versionWithGit: {
    id: 'components.GlobalFooter.message.versionWithGit',
    defaultMessage: '{app} version {version} (GIT SHA {gitVersion})',
  },
});

export default messages;
