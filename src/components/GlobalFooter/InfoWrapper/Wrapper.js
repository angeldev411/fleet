import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const Wrapper = styled.div`
  display: inline;
  margin: auto 0;
  padding: ${px2rem(24)} 0 0 ${px2rem(14)};
`;

export default Wrapper;
