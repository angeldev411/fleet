import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.text};
  font-family: ${props.theme.fonts.primary};
  font-size: ${px2rem(14)};
  font-weight: 300;
  line-height: 30px;
  margin: 0;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  fonts: PropTypes.shape({
    primary: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'TextWrapper',
  styled.p,
  styles,
  { themePropTypes },
);
