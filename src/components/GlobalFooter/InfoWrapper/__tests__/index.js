import React from 'react';
import {
  test,
  expect,
  MountableTestComponent,
  mount,
} from '__tests__/helpers/test-setup';

import InfoWrapper from '../index';

const renderComponent = ({ version = '123.567', ...rest } = {}) => mount(
  <MountableTestComponent>
    <InfoWrapper version={version} {...rest} />
  </MountableTestComponent>,
);

// the "Test Runtime" app name should match src/__tests__/helpers/testRuntimeConfig.json
const APP_NAME = 'Test Runtime';

test('Renders the given version number', () => {
  const version = '55.44.33.22.11';
  const lastChild = renderComponent({ version }).children().last();
  expect(lastChild).toExist();
  expect(lastChild.text()).toEqual(`${APP_NAME} version ${version}`);
});

test('Renders a copyright with the current year', () => {
  const firstChild = renderComponent().children().first();
  const thisYear = new Date().getFullYear();
  expect(firstChild.text()).toContain(`Copyright © ${thisYear} Decisiv, Inc.`);
});

test('Includes the git SHA if SHOW_GIT_REVISION is set', () => {
  // store global values before we muck with them
  const oldFlag = process.env.SHOW_GIT_REVISION;
  const oldCommit = process.env.GIT_COMMIT;

  process.env.SHOW_GIT_REVISION = 'true';
  process.env.GIT_COMMIT = 'abc123';
  const version = '0.0.1';
  const lastChild = renderComponent({ version }).children().last();
  expect(lastChild).toExist();
  expect(lastChild.text()).toEqual(`${APP_NAME} version ${version} (GIT SHA abc123)`);

  // cleanup
  process.env.GIT_COMMIT = oldCommit;
  process.env.SHOW_GIT_REVISION = oldFlag;
});
