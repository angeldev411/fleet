export const BLOG_URL = 'https://blog.decisiv.com';
export const TERMS_OF_USE_URL = 'https://www.decisiv.com/resources/product-agreements';
export const PRIVACY_POLICY_URL = '/';
