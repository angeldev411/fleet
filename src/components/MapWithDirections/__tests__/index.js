import React from 'react';
import {
  createSpy,
  expect,
  shallow,
  test,
} from '__tests__/helpers/test-setup';

import WithDirections from 'utils/mapping/WithDirections';
import { mockDirectionResult } from 'utils/mapping/__tests__/helpers';

import MapWithDirections from '../index';

const markerOne = {
  label: 'Marker One',
  position: { lat: 123.45, lng: 88.76 },
  showInfo: false,
};
const markerTwo = {
  label: 'Marker Two',
  position: { lat: 55.432, lng: 66.789 },
  showInfo: false,
};

const onDirectionsUpdate = () => 'onDirectionsUpdate';
const onMarkerClick = () => 'onMarkerClick';
const onMarkerClose = () => 'onMarkerClose';

const defaultProps = {
  markers: [markerOne, markerTwo],
  onDirectionsUpdate,
  onMarkerClick,
  onMarkerClose,
};

function shallowRender(props = defaultProps) {
  return shallow(<MapWithDirections {...props} />);
}

test('renders a WithDirections wrapper', () => {
  const component = shallowRender();
  expect(component).toContain(WithDirections);
  const wrapper = component.find(WithDirections);
  expect(wrapper.props().markers).toEqual(defaultProps.markers);
  expect(wrapper.props().onDirectionsUpdate).toEqual(defaultProps.onDirectionsUpdate);
});

/**
 * Helper to render the component and then call the embedded render function
 * to also render the map. The map component is returned.
 */
function shallowRenderMap(props = defaultProps) {
  const { directions, ...componentProps } = props;
  const component = shallowRender({ ...defaultProps, ...componentProps });
  const wrapper = component.find(WithDirections);
  const renderFn = wrapper.props().render;
  return shallow(renderFn({ directions }));
}

test('the render function renders markers', () => {
  const markerClickSpy = createSpy();
  const mapComponent = shallowRenderMap({
    onMarkerClick: markerClickSpy,
  });

  // check 'defaultCenter' prop to GoogleMap component
  expect(mapComponent.props().defaultCenter).toEqual(defaultProps.markers[0].position);

  // check that all the Marker components are rendered
  expect(mapComponent).toContain('Marker');
  const markerComponents = mapComponent.find('Marker');
  expect(markerComponents.length).toEqual(defaultProps.markers.length);

  // check that each Marker has the correct props
  markerComponents.forEach((node, index) => {
    const nodeProps = node.props();
    const marker = defaultProps.markers[index];

    expect(node.key()).toEqual(marker.label);
    expect(nodeProps.position).toEqual(marker.position);

    nodeProps.onClick();
    expect(markerClickSpy).toHaveBeenCalledWith(marker);
    markerClickSpy.reset();
  });
});

test('the render function does not render directions if not given directions', () => {
  const directions = null;
  const mapComponent = shallowRenderMap({ directions });
  expect(mapComponent).toNotContain('DirectionsRenderer');
});

test('the render function renders directions if given directions', () => {
  const directions = mockDirectionResult();
  const mapComponent = shallowRenderMap({ directions });
  expect(mapComponent).toContain('DirectionsRenderer');
  expect(mapComponent.find('DirectionsRenderer').props().directions).toEqual(directions);
});

test('InfoWindows are shown for each visible marker', () => {
  // three markers, but only two are visible:
  const markers = [
    { ...markerOne, label: 'visible', showInfo: true },
    { ...markerOne, label: 'hidden', showInfo: false },
    { ...markerOne, label: 'also visible', showInfo: true },
  ];
  const visibleMarkers = [markers[0], markers[2]];

  const markerCloseSpy = createSpy();
  const mapComponent = shallowRenderMap({
    markers,
    onMarkerClose: markerCloseSpy,
  });

  // check that the correct number of InfoWindows are rendered
  expect(mapComponent).toContain('InfoWindow');
  const infoWindows = mapComponent.find('InfoWindow');
  expect(infoWindows.length).toEqual(visibleMarkers.length);

  // check that each InfoWindow has the correct props
  infoWindows.forEach((node, index) => {
    const marker = visibleMarkers[index];
    expect(node.find('div').text()).toEqual(marker.label);

    node.props().onCloseClick();
    expect(markerCloseSpy).toHaveBeenCalledWith(marker);
    markerCloseSpy.reset();
  });
});
