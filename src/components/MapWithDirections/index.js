import React from 'react';
import {
  GoogleMap,
  DirectionsRenderer,
  Marker,
  InfoWindow,
} from 'react-google-maps';

import PropTypes from 'utils/prop-types';
import WithDirections from 'utils/mapping/WithDirections';

/**
 * Map is the (private) component that is rendered by the WithDirections component
 * in MapWithDirections.
 */
function Map({ directions, markers, onMarkerClick, onMarkerClose }) {
  // default to the first marker as the map center-point
  const firstMarker = markers && markers[0];
  const center = firstMarker && firstMarker.position;

  return (
    <GoogleMap
      defaultZoom={12}
      defaultCenter={center}
    >
      {
        // render the directions, but don't include markers/info windows
        // since we'll render our custom info windows below
        directions &&
        <DirectionsRenderer
          directions={directions}
          options={{ suppressInfoWindows: true, suppressMarkers: true }}
        />
      }
      {
        // render custom markers & info windows for each received marker
        // TODO:  We might eventually want to pass in the component to
        //        be used for rendering the info window contents, to allow
        //        for customization.
        markers && markers.map(m => (
          <Marker
            key={m.label}
            position={m.position}
            onClick={() => onMarkerClick(m)}
          >
            {m.showInfo && (
              <InfoWindow
                onCloseClick={() => onMarkerClose(m)}
              >
                <div>{m.label}</div>
              </InfoWindow>
            )}
          </Marker>
        ))
      }
    </GoogleMap>
  );
}

/**
 * MapWithDirections wraps a Map (see above) with the WithDirections component, which
 * provides the calculated directions to the Map.
 */
function MapWithDirections({
  markers,
  onDirectionsUpdate,
  onMarkerClick,
  onMarkerClose,
}) {
  return (
    <WithDirections
      markers={markers}
      onDirectionsUpdate={onDirectionsUpdate}
      render={({ directions }) => (
        <Map
          directions={directions}
          markers={markers}
          onMarkerClick={onMarkerClick}
          onMarkerClose={onMarkerClose}
        />
      )}
    />
  );
}

// ------------ prop types for both components above ------------

const markerPropType = PropTypes.shape({
  label: PropTypes.string.isRequired,
  position: PropTypes.latLng.isRequired,
  showInfo: PropTypes.bool.isRequired,
});

const sharedPropTypes = {
  markers: PropTypes.arrayOf(markerPropType),
  onMarkerClick: PropTypes.func.isRequired,
  onMarkerClose: PropTypes.func.isRequired,
};

Map.propTypes = {
  ...sharedPropTypes,
  directions: PropTypes.googleDirections,
};

Map.defaultProps = {
  markers: undefined,
  directions: undefined,
};

MapWithDirections.propTypes = {
  ...sharedPropTypes,
  onDirectionsUpdate: PropTypes.func.isRequired,
};

MapWithDirections.defaultProps = {
  markers: undefined,
  onDirectionsUpdate: undefined,
};

export default MapWithDirections;
