import PropTypes from 'prop-types';
import React from 'react';

// TODO: Make this a real step.
/* istanbul ignore next */
function Step(props) {
  console.log('WizardSteps - AssetAvailability - props:', props);

  const submitInputData = (value) => {
    props.completeStep({ assetAvailability: value });
  };

  return (
    <div style={{ backgroundColor: 'papayawhip', padding: '1rem' }}>
      <h3>{'AssetAvailability'}</h3>
      <button onClick={() => { submitInputData('Today'); }}>
        {'Today'}
      </button>
      <button onClick={() => { submitInputData('Next Week'); }}>
        {'Next Week'}
      </button>
      <h4>Input Data</h4>
      <pre>{JSON.stringify(props.inputData, null, 2)}</pre>
    </div>
  );
}

Step.propTypes = {
  completeStep: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  inputData: PropTypes.object.isRequired,
};

export default Step;
