import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Container,
  Row,
  Column,
} from 'styled-components-reactive-grid';
import { FormattedMessage } from 'react-intl';
import { P } from 'base-components';

import AssetSelectionStep from 'compositions/AssetSelectionStep';

import messages from './messages';

class AssetSelection extends Component {
  static propTypes = {
    inputData: PropTypes.shape({
      asset: PropTypes.shape({
        unitNumber: PropTypes.string,
        make: PropTypes.string,
        model: PropTypes.string,
        engine: PropTypes.string,
        year: PropTypes.string,
      }),
    }),
    review: PropTypes.bool,
    completeStep: PropTypes.func.isRequired,
  };

  static defaultProps = {
    inputData: {},
    review: false,
  }

  submitInputData = (value) => {
    this.props.completeStep({ asset: value });
  };

  // TODO: make AssetSelector a composition, not the entire AssetSelectionStep,
  // so that AssetSelector can more easily be re-used elsewhere.
  render() {
    const {
      review,
      inputData: {
        asset = {},
      },
    } = this.props;

    return (
      review ?
        <Container>
          <Row>
            <Column modifiers={['col_3']}>
              <P modifiers={['fontWeightMedium']}>
                <FormattedMessage {...messages.asset} />
              </P>
            </Column>
            <Column modifiers={['col_9']}>
              <P modifiers={['fontWeightRegular']}>
                {`${asset.unitNumber} ${asset.make} ${asset.model} ${asset.engine} - ${asset.year}`}
              </P>
            </Column>
          </Row>
        </Container>
        :
        <AssetSelectionStep submitInputData={this.submitInputData} />
    );
  }
}

export default AssetSelection;
