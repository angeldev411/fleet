import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import AssetSelection from '../index';
import messages from '../messages';

const defaultProps = {
  inputData: {
    asset: {
      unitNumber: '1214',
      make: 'Mack',
      model: 'CH613',
      engine: 'E7',
      year: '2003',
    },
  },
  completeStep: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetSelection {...props} />);
}

function mountComponent(props = defaultProps) {
  return mount(
    <MountableTestComponent>
      <AssetSelection {...props} />
    </MountableTestComponent>,
  );
}

test('as an input component, renders AssetSelection composition and passes submitInputData prop', () => {
  const component = shallowRender();
  expect(component).toBeA('AssetSelectionStep');
  const instance = component.instance();
  expect(component).toHaveProps({
    submitInputData: instance.submitInputData,
  });
});

test('calling AssetSelection method triggers completeStep prop call with correct key', () => {
  const completeStep = createSpy();
  const testValue = 'Congrats, David!';
  const component = shallowRender({ ...defaultProps, completeStep });
  const instance = component.instance();
  instance.submitInputData(testValue);
  expect(completeStep).toHaveBeenCalledWith({ asset: testValue });
});

test('as a review component, renders 2 columns in asset row', () => {
  const component = shallowRender({
    ...defaultProps,
    review: true,
  });
  const row = component.find('withSize(Row)').at(0); // Using `at` considering index can be changed later.
  expect(row.find('withSize(Column)').length).toEqual(2);
});

test('as a review component, renders content in asset row', () => {
  const component = mountComponent({
    ...defaultProps,
    review: true,
  });
  const row = component.find('withSize(Row)').at(0);
  let column;
  column = row.find('withSize(Column)').first();
  expect(column).toContain('FormattedMessage');
  const FormattedMessage = column.find('FormattedMessage');
  expect(FormattedMessage).toHaveProps({
    ...messages.asset,
  });

  column = row.find('withSize(Column)').last();
  expect(column.render().text()).toContain('1214 Mack CH613 E7 - 2003');
});
