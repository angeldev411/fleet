import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  asset: {
    id: 'components.WizardSteps.AssetSelection.asset',
    defaultMessage: 'Asset',
  },
});

export default formattedMessages;
