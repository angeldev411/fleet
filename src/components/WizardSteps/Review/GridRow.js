import { buildStyledComponent } from 'decisiv-ui-utils';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  Row,
} from 'styled-components-reactive-grid';


/* istanbul ignore next */
const styles = ({ theme }) => `
  &:hover {
    background-color: ${theme.colors.base.chrome100};
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
    }),
  }),
};

export default buildStyledComponent(
  'GridRow',
  styled(Row),
  styles,
  { themePropTypes },
);
