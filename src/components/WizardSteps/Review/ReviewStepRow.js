import { QuickActionButton } from 'base-components';
import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Column } from 'styled-components-reactive-grid';

import GridRow from './GridRow';
import messages from './messages';

class ReviewStepRow extends Component {
  static propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    inputData: PropTypes.object.isRequired,
    jumpToStep: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    ReviewComponent: PropTypes.func.isRequired,
  };

  state = {
    hovering: false,
  };

  setHoveringTrue = () => {
    this.setState({ hovering: true });
  }

  setHoveringFalse = () => {
    this.setState({ hovering: false });
  }

  handleClick = () => {
    const { jumpToStep, name } = this.props;
    jumpToStep(name);
  }

  render() {
    const {
      inputData,
      ReviewComponent,
    } = this.props;

    return (
      <GridRow
        modifiers={['fluid']}
        onMouseEnter={this.setHoveringTrue}
        onMouseLeave={this.setHoveringFalse}
      >
        <Column modifiers={['col_9']}>
          <ReviewComponent completeStep={noop} inputData={inputData} review />
        </Column>
        <Column modifiers={['col_3']}>
          {this.state.hovering &&
            <QuickActionButton onClick={this.handleClick}>
              <QuickActionButton.Icon modifiers={['left']} name="pencil" />
              <QuickActionButton.Text>
                <FormattedMessage {...messages.editButton} />
              </QuickActionButton.Text>
            </QuickActionButton>
          }
        </Column>
      </GridRow>
    );
  }
}

export default ReviewStepRow;
