import React from 'react';

import {
  expect,
  test,
  shallow,
  createSpy,
  spyOn,
} from '__tests__/helpers/test-setup';

import ReviewStep from '../index';

const defaultProps = {
  handleWizardCancel: () => {},
  handleWizardSubmit: () => {},
  inputData: {},
  jumpToStep: () => {},
  stepsArray: [
    { component: () => <div />, name: 'STEP_1' },
    { component: () => <div />, name: 'STEP_2' },
    { component: ReviewStep, name: 'REVIEW' },
  ],
};

function shallowRender(props = defaultProps) {
  return shallow(<ReviewStep {...props} />);
}

test('componentDidMount adds a keydown event listener', () => {
  const addEventListener = spyOn(window, 'addEventListener');
  const component = shallowRender();
  const instance = component.instance();
  instance.componentDidMount();
  expect(addEventListener).toHaveBeenCalledWith('keydown', instance.keydownEventHandler);
});

test('componentWillUnmount removes a keydown event listener', () => {
  const removeEventListener = spyOn(window, 'removeEventListener');
  const component = shallowRender();
  const instance = component.instance();
  instance.componentWillUnmount();
  expect(removeEventListener).toHaveBeenCalledWith('keydown', instance.keydownEventHandler);
});

test('Enter keydown event submits the wizard', () => {
  const handleWizardSubmit = createSpy();
  const component = shallowRender({ ...defaultProps, handleWizardSubmit });
  const instance = component.instance();
  instance.keydownEventHandler({ key: 'Enter' });
  expect(handleWizardSubmit).toHaveBeenCalledWith(defaultProps.inputData);
});

test('Escape keydown event cancels the wizard', () => {
  const handleWizardCancel = createSpy();
  const component = shallowRender({ ...defaultProps, handleWizardCancel });
  const instance = component.instance();
  instance.keydownEventHandler({ key: 'Escape' });
  expect(handleWizardCancel).toHaveBeenCalled();
});

test('non ESC or Enter keydown events do not cancel or submit the wizard', () => {
  const handleWizardSubmit = createSpy();
  const handleWizardCancel = createSpy();
  const component = shallowRender({
    ...defaultProps,
    handleWizardCancel,
    handleWizardSubmit,
  });
  const instance = component.instance();
  instance.keydownEventHandler({ key: 'Backsapce' });
  expect(handleWizardCancel).toNotHaveBeenCalled();
  expect(handleWizardSubmit).toNotHaveBeenCalled();
});

test('renders 1 fewer ReviewStepRows then steps in the steps array', () => {
  const component = shallowRender();
  const reviewStepRows = component.find('ReviewStepRow');
  expect(reviewStepRows.length).toEqual(defaultProps.stepsArray.length - 1);
});

test('clicking submit button submits wizard with inputData', () => {
  const handleWizardSubmit = createSpy();
  const component = shallowRender({ ...defaultProps, handleWizardSubmit });
  const submitButton = component.find('QuickActionButton').at(1);
  submitButton.simulate('click');
  expect(handleWizardSubmit).toHaveBeenCalledWith(defaultProps.inputData);
});
