import React from 'react';

import {
  expect,
  test,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import ReviewStepRow from '../ReviewStepRow';

const defaultProps = {
  inputData: {},
  jumpToStep: () => {},
  name: 'STEP_1',
  ReviewComponent: () => <div />,
};

function shallowRender(props = defaultProps) {
  return shallow(<ReviewStepRow {...props} />);
}

test('mousing over the GridRow sets the hovering state to true', () => {
  const component = shallowRender();
  component.simulate('mouseEnter');
  expect(component.state().hovering).toEqual(true);
});

test('mousing out of the GridRow sets the hovering state to false', () => {
  const component = shallowRender();
  component.setState({ hovering: true });
  component.simulate('mouseLeave');
  expect(component.state().hovering).toEqual(false);
});

test('clicking the edit button calls jumpToStep with the name of the step', () => {
  const jumpToStep = createSpy();
  const component = shallowRender({ ...defaultProps, jumpToStep });
  component.setState({ hovering: true });
  const editButton = component.find('QuickActionButton');
  editButton.simulate('click');
  expect(jumpToStep).toHaveBeenCalledWith(defaultProps.name);
});
