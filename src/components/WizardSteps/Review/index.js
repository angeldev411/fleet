import { QuickActionButton } from 'base-components';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage } from 'react-intl';
import {
  Column,
  Container,
  Row,
} from 'styled-components-reactive-grid';

import TextDiv from 'elements/TextDiv';

import ReviewStepRow from './ReviewStepRow';
import messages from './messages';

class ReviewStep extends Component {
  static propTypes = {
    handleWizardCancel: PropTypes.func.isRequired,
    handleWizardSubmit: PropTypes.func.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    inputData: PropTypes.object.isRequired,
    jumpToStep: PropTypes.func.isRequired,
    stepsArray: PropTypes.arrayOf(
      PropTypes.shape({
        component: PropTypes.func.isRequired,
        name: PropTypes.string.isRequired,
      }).isRequired,
    ).isRequired,
  };

  componentDidMount() {
    window.addEventListener('keydown', this.keydownEventHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.keydownEventHandler);
  }

  keydownEventHandler = ({ key }) => {
    // handle the Enter key being pressed
    if (key === 'Enter') {
      this.props.handleWizardSubmit(this.props.inputData);
    }

    // handle the ESC key being pressed
    if (key === 'Escape') {
      this.props.handleWizardCancel();
    }
  }

  handleClick = () => {
    const { handleWizardSubmit, inputData } = this.props;
    handleWizardSubmit(inputData);
  }

  render() {
    const {
      handleWizardCancel,
      inputData,
      jumpToStep,
      stepsArray,
    } = this.props;

    // The last step in the stepsArray is the ReviewStep (this step).
    // It is not included in the review steps display.
    const stepsToDisplay = stepsArray.slice(0, stepsArray.length - 1);

    return (
      <Container>
        {stepsToDisplay.map(({ name, component: ReviewComponent }) => (
          <ReviewStepRow
            key={name}
            inputData={inputData}
            jumpToStep={jumpToStep}
            name={name}
            ReviewComponent={ReviewComponent}
          />
        ))}
        <Row>
          <Column modifiers={['col_12']}>
            <Container>
              <Row modifiers={['end', 'middle']}>
                <Column>
                  <TextDiv modifiers={['midGreyText']}>
                    <FontAwesome name="keyboard-o" />
                  </TextDiv>
                </Column>
                <Column>
                  <TextDiv modifiers={['midGreyText', 'xSmallText']}>
                    <FormattedMessage {...messages.keypressGuide} />
                  </TextDiv>
                </Column>
                <Column>
                  <QuickActionButton
                    modifiers={['hoverDanger']}
                    onClick={handleWizardCancel}
                  >
                    <QuickActionButton.Text>
                      <FormattedMessage {...messages.cancelButton} />
                    </QuickActionButton.Text>
                  </QuickActionButton>
                </Column>
                <Column>
                  <QuickActionButton
                    modifiers={['hoverSuccess', 'secondary']}
                    onClick={this.handleClick}
                  >
                    <QuickActionButton.Text>
                      <FormattedMessage {...messages.submitButton} />
                    </QuickActionButton.Text>
                  </QuickActionButton>
                </Column>
              </Row>
            </Container>
          </Column>
        </Row>
      </Container>
    );
  }
}

export default ReviewStep;
