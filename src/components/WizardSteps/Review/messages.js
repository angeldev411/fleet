import { defineMessages } from 'react-intl';

const messages = defineMessages({
  cancelButton: {
    id: 'components.WizardSteps.Review.cancelButton',
    defaultMessage: 'Cancel',
  },
  editButton: {
    id: 'components.WizardSteps.Review.editButton',
    defaultMessage: 'Edit',
  },
  keypressGuide: {
    id: 'components.WizardSteps.Review.keypressGuide',
    defaultMessage: 'Press ESC to cancel or ENTER to submit',
  },
  submitButton: {
    id: 'components.WizardSteps.Review.submitButton',
    defaultMessage: 'Submit Request',
  },
});

export default messages;
