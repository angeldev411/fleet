import PropTypes from 'prop-types';
import React from 'react';

// TODO: Make this a real step.
/* istanbul ignore next */
function Step(props) {
  console.log('WizardSteps - ContactPerson - props:', props);

  const submitInputData = (value) => {
    props.completeStep({ contact: value });
  };

  return (
    <div style={{ backgroundColor: 'papayawhip', padding: '1rem' }}>
      <h3>{'ContactPerson'}</h3>
      <button onClick={() => { submitInputData('David'); }}>
        {'David'}
      </button>
      <button onClick={() => { submitInputData('Kim'); }}>
        {'Kim'}
      </button>
      <h4>Input Data</h4>
      <pre>{JSON.stringify(props.inputData, null, 2)}</pre>
    </div>
  );
}

Step.propTypes = {
  completeStep: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  inputData: PropTypes.object.isRequired,
};

export default Step;
