import { defineMessages } from 'react-intl';

const messages = defineMessages({
  titlePrefix: {
    id: 'components.WizardSteps.error.noAssetLocation.titlePrefix',
    defaultMessage: 'No matches for "',
  },
  titleSuffix: {
    id: 'components.WizardSteps.error.noAssetLocation.titleSuffix',
    defaultMessage: '".',
  },
  subtitle: {
    id: 'components.WizardSteps.error.noAssetLocation.subtitle',
    defaultMessage: 'Try removing or editing some of your criteria to expand the search or search by city and state or zip code.',
  },
});

export default messages;
