import PropTypes from 'prop-types';
import React from 'react';

import WizardSearchError from 'components/WizardSearchError';

import messages from './messages';

// TODO: Make this a real step.
/* istanbul ignore next */
function Step(props) {
  console.log('WizardSteps - ServiceProviderSelection - props:', props);

  const submitInputData = (value) => {
    props.completeStep({ serviceProvider: value });
  };

  return (
    <div style={{ backgroundColor: 'papayawhip', padding: '1rem' }}>
      <h3>{'ServiceProviderSelection'}</h3>
      <button onClick={() => { submitInputData('Service Provider 1'); }}>
        {'Service Provider 1'}
      </button>
      <button onClick={() => { submitInputData('Service Provider 2'); }}>
        {'Service Provider 2'}
      </button>
      <h4>Input Data</h4>
      <pre>{JSON.stringify(props.inputData, null, 2)}</pre>
      {/* TODO: The below is just for the demo */}
      <WizardSearchError
        searchValue="dfghdk"
        titlePrefix={messages.titlePrefix}
        titleSuffix={messages.titleSuffix}
        subtitle={messages.subtitle}
      />
    </div>
  );
}

Step.propTypes = {
  completeStep: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  inputData: PropTypes.object.isRequired,
};

export default Step;
