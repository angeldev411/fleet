import { defineMessages } from 'react-intl';

const messages = defineMessages({
  complaint: {
    id: 'components.WizardSteps.ReasonForRepair.complaint',
    defaultMessage: 'Complaint',
  },
});

export default messages;
