import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import ReasonForRepair from '../index';
import messages from '../messages';

const defaultProps = {
  inputData: {
    reasonForRepair: {
      complaintCode: '01',
      complaintDescription: 'Testing complaint',
      description: 'Unit testing is cumbersome',
    },
  },
  completeStep: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<ReasonForRepair {...props} />);
}

function mountComponent(props = defaultProps) {
  return mount(
    <MountableTestComponent>
      <ReasonForRepair {...props} />
    </MountableTestComponent>,
  );
}

test('as an input component, renders ReasonForRepairStep composition and passes submitInputData prop', () => {
  const component = shallowRender();
  expect(component).toBeA('ReasonForRepairStepContainer'); // because of the HOC connection
  const instance = component.instance();
  expect(component).toHaveProps({
    submitInputData: instance.submitInputData,
  });
});

test('calling submitInputData method triggers completeStep prop call with correct key', () => {
  const completeStep = createSpy();
  const testValue = 'Happy birthday, David!';
  const component = shallowRender({ ...defaultProps, completeStep });
  const instance = component.instance();
  instance.submitInputData(testValue);
  expect(completeStep).toHaveBeenCalledWith({ reasonForRepair: testValue });
});

test('as a review component, renders 2 columns', () => {
  const component = shallowRender({
    ...defaultProps,
    review: true,
  });
  const row = component.find('withSize(Row)');
  expect(row.find('withSize(Column)').length).toEqual(2);
});

test('as a review component, renders title', () => {
  const component = shallowRender({
    ...defaultProps,
    review: true,
  });
  const row = component.find('withSize(Row)');
  const column = row.find('withSize(Column)').first();
  expect(column).toContain('FormattedMessage');
  const FormattedMessage = column.find('FormattedMessage');
  expect(FormattedMessage).toHaveProps({
    ...messages.complaint,
  });
});

test('as a review component, renders complaintCode/complaintDescription', () => {
  const component = mountComponent({
    ...defaultProps,
    review: true,
  });
  const row = component.find('withSize(Row)');
  const column = row.find('withSize(Column)').last();
  const p = column.find('P').first();
  expect(p.render().text()).toContain('01 - Testing complaint');
});

test('as a review component, renders description', () => {
  const component = mountComponent({
    ...defaultProps,
    review: true,
  });
  const row = component.find('withSize(Row)');
  const column = row.find('withSize(Column)').last();
  const p = column.find('P').last();
  expect(p.render().text()).toContain('Unit testing is cumbersome');
});
