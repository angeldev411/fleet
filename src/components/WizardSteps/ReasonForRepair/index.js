import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Container,
  Row,
  Column,
} from 'styled-components-reactive-grid';
import { P } from 'base-components';
import { FormattedMessage } from 'react-intl';

import ReasonForRepairStep from 'compositions/ReasonForRepairStep';

import messages from './messages';

class ReasonForRepair extends Component {
  static propTypes = {
    inputData: PropTypes.shape({
      reasonForRepair: PropTypes.shape({
        complaintCode: PropTypes.string,
        complaintDescription: PropTypes.string,
        description: PropTypes.string,
      }),
    }),
    review: PropTypes.bool,
    completeStep: PropTypes.func.isRequired,
  };

  static defaultProps = {
    inputData: {},
    review: false,
  }

  submitInputData = (value) => {
    this.props.completeStep({ reasonForRepair: value });
  };

  render() {
    const {
      review,
      inputData: {
        reasonForRepair: {
          complaintCode,
          complaintDescription,
          description,
        } = {},
      },
    } = this.props;

    return (
      review ?
        <Container>
          <Row>
            <Column modifiers={['col_3']}>
              <P modifiers={['fontWeightMedium']}>
                <FormattedMessage {...messages.complaint} />
              </P>
            </Column>
            <Column modifiers={['col_9']}>
              <P modifiers={['fontWeightMedium']}>
                {`${complaintCode} - ${complaintDescription}`}
              </P>
              <P modifiers={['fontWeightRegular']}>
                {description}
              </P>
            </Column>
          </Row>
        </Container>
        :
        <ReasonForRepairStep submitInputData={this.submitInputData} />
    );
  }
}

export default ReasonForRepair;
