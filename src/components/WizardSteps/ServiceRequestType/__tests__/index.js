import React from 'react';
import {
  test,
  expect,
  shallow,
  createSpy,
  noop,
} from '__tests__/helpers/test-setup';

import ServiceRequestType from '../index';
import messages from '../messages';

const inputData = {
  typeOfService: 'breakdown',
};

const defaultProps = {
  completeStep: noop,
  inputData,
  review: false,
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestType {...props} />);
}

test('submitInputData calls completeStep with the provided value', () => {
  const completeStep = createSpy();
  const component = shallowRender({ ...defaultProps, completeStep });
  const instance = component.instance();
  const value = 'breakdown';
  instance.submitInputData(value);
  expect(completeStep).toHaveBeenCalledWith({ typeOfService: value });
});

test('renders ServiceRequestType with the correct title', () => {
  const component = shallowRender();
  expect(component).toContain('FormattedMessage');
  const formattedMessage = component.find('FormattedMessage');
  expect(formattedMessage).toHaveProps({ ...messages.title });
});

test('renders 2 TypeSelect components in input step', () => {
  const component = shallowRender();
  const typeSelect = component.find('TypeSelect');
  expect(typeSelect.length).toEqual(2);
});

test('submit correct service type if maintenance option is selected', () => {
  const completeStep = createSpy();
  const testProps = {
    ...defaultProps,
    completeStep,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.submitInputData('maintenance');
  expect(completeStep).toHaveBeenCalledWith({ typeOfService: 'maintenance' });
});


test('renders correct title in review step', () => {
  const testProps = {
    ...defaultProps,
    review: true,
  };
  const component = shallowRender(testProps);
  expect(component).toContain('FormattedMessage');
  const formattedMessage = component.find('FormattedMessage').first();
  expect(formattedMessage).toHaveProps({ ...messages.requestType });
});

test('renders correct request type in review step', () => {
  const testProps = {
    ...defaultProps,
    review: true,
  };
  const component = shallowRender(testProps);
  expect(component).toContain('FormattedMessage');
  const formattedMessage = component.find('FormattedMessage').last();
  expect(formattedMessage).toHaveProps({ ...messages[inputData.typeOfService] });
});
