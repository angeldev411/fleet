import { defineMessages } from 'react-intl';

const messages = defineMessages({
  breakdown: {
    id: 'components.WizardSteps.ServiceRequestType.breakdown',
    defaultMessage: 'Breakdown',
  },
  maintenance: {
    id: 'components.WizardSteps.ServiceRequestType.maintenance',
    defaultMessage: 'Maintenance',
  },
  title: {
    id: 'components.WizardSteps.ServiceRequestType.title',
    defaultMessage: 'What type of Service?',
  },
  requestType: {
    id: 'components.WizardSteps.ServiceRequestType.requestType',
    defaultMessage: 'Request Type',
  },
});

export default messages;
