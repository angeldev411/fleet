import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { H3, P } from 'base-components';
import {
  Container,
  Row,
  Column,
} from 'styled-components-reactive-grid';

import TypeSelect from 'components/TypeSelect';

import InputWrapper from './InputWrapper';
import { SERVICE_TYPES } from './constants';
import messages from './messages';

class ServiceRequestType extends Component {
  submitInputData = (value) => {
    this.props.completeStep({ typeOfService: value });
  };

  renderInput = inputData =>
    (<div>
      <H3 modifiers={['fontWeightRegular']}>
        <FormattedMessage {...messages.title} />
      </H3>
      <InputWrapper>
        {SERVICE_TYPES.map((serviceType, index) =>
          (<TypeSelect
            key={serviceType}
            onSelect={() => { this.submitInputData(serviceType); }}
            isSelected={inputData.typeOfService === serviceType}
            typeName={<FormattedMessage {...messages[serviceType]} />}
            selectKey={`${index + 1}`}
          />),
        )}
      </InputWrapper>
    </div>)

  renderReview = inputData =>
    (<Container>
      <Row>
        <Column modifiers={['col_3']}>
          <P modifiers={['fontWeightMedium']}>
            <FormattedMessage {...messages.requestType} />
          </P>
        </Column>
        <Column modifiers={['col_9']}>
          <P><FormattedMessage {...messages[inputData.typeOfService]} /></P>
        </Column>
      </Row>
    </Container>)

  render() {
    const { inputData, review } = this.props;
    return (
      !review ? this.renderInput(inputData) : this.renderReview(inputData)
    );
  }
}

ServiceRequestType.propTypes = {
  completeStep: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  inputData: PropTypes.shape({
    typeOfService: PropTypes.string,
  }),
  review: PropTypes.bool,
};

ServiceRequestType.defaultProps = {
  inputData: {},
  review: false,
};

export default ServiceRequestType;
