import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = `
  display: flex;
  margin-top: ${px2rem(30)};
`;

export default buildStyledComponent(
  'InputWrapper',
  styled.div,
  styles,
);
