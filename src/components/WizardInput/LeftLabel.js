import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  left: ${px2rem(10)};
  position: absolute;
  top: ${px2rem(10)};
`;

export default buildStyledComponent(
  'LeftLabel',
  styled.span,
  styles,
);
