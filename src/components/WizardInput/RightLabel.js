import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  rightLabelVisible: () => ({
    styles: `
      visibility: visible;
    `,
  }),
};

/* istanbul ignore next */
const styles = () => `
  right: ${px2rem(10)};
  position: absolute;
  top: ${px2rem(10)};
  visibility: hidden;
`;

export default buildStyledComponent(
  'RightLabel',
  styled.span,
  styles,
  { modifierConfig },
);
