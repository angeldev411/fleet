import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const defaultHeight = px2rem(25);

/* istanbul ignore next */
const modifierConfig = {
  flexibleHeight: () => ({
    styles: `
      box-sizing: border-box;
      font-size: 0;
      height: auto;
      min-height: ${defaultHeight};
    `,
  }),
  topOverlap: () => ({
    styles: `
      margin-top: -1px;
    `,
  }),
  warning: ({ theme }) => ({
    styles: `
      border-color: ${theme.colors.status.warning} !important;
    `,
  }),
};

/* istanbul ignore next */
const styles = ({ theme }) => `
  border: 1px solid ${theme.colors.base.chrome400};
  height: ${defaultHeight};
  padding-left: ${px2rem(10)};
  padding-top: ${px2rem(20)};
  position: relative;

  &:focus-within {
    border-color: ${theme.colors.base.chrome600};
    z-index: 1;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome400: PropTypes.string.isRequired,
      chrome600: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      warning: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'WizardInputWrapper',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
