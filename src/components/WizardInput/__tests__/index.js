import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import WizardInput from '../index';

const defaultProps = {
  children: <input id="test-child" type="text" value="test input field" />,
  label: {
    id: 'test id',
    defaultMessage: 'test message',
  },
  rightLabel: {
    id: 'test id for right label',
    defaultMessage: 'test right label message',
  },
  rightLabelValue: 100,
  modifiers: ['flexibleHeight', 'rightLabelVisible'],
};

function shallowRender(props = defaultProps) {
  return shallow(<WizardInput {...props} />);
}

test('renders the children', () => {
  const component = shallowRender();
  expect(component).toContain('input#test-child');
});

test('passes down component modifiers to the wrapper component', () => {
  const component = shallowRender();
  expect(component.props().modifiers).toEqual(['flexibleHeight']);
});

test('renders the default (left) label with correct message', () => {
  const component = shallowRender();
  expect(component).toContain('LeftLabel');
  expect(component.find('LeftLabel FormattedMessage')).toHaveProps({ ...defaultProps.label });
});

test('does not render the right label if rightLabel prop is falsy', () => {
  const component = shallowRender({ ...defaultProps, rightLabel: undefined });
  expect(component).toNotContain('RightLabel');
});

test('renders the right label with correct message and values', () => {
  const component = shallowRender();
  expect(component).toContain('RightLabel');

  const rightLabelMessage = component.find('RightLabel FormattedMessage');
  expect(rightLabelMessage).toHaveProps({
    ...defaultProps.rightLabel,
  });
  expect(rightLabelMessage.props().values).toEqual({ value: defaultProps.rightLabelValue });
});

test('passes down right label modifiers to the RightLabel component', () => {
  const component = shallowRender();
  const rightLabel = component.find('RightLabel');
  expect(rightLabel.props().modifiers).toEqual(['rightLabelVisible']);
});
