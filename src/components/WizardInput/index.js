/**
 * A UI wrapper for input fields in Wizard steps (e.g. Complaint Select, Asset Select, etc)
 * Basically, it contains the border, a label and an icon (TBD).
 * @param {array} modifiers includes component-level modifiers and rightLabel modifiers
 * that start with `rightLabel-` prefix. (e.g. rightLabelVisible)
 */
import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';
import { difference } from 'lodash';

import TextDiv from 'elements/TextDiv';

import WizardInputWrapper from './WizardInputWrapper';
import LeftLabel from './LeftLabel';
import RightLabel from './RightLabel';

function WizardInput({ children, label, rightLabel, rightLabelValue, modifiers }) {
  const rightLabelModifiers = modifiers.filter(modifier => modifier.startsWith('rightLabel'));
  const componentModifiers = difference(modifiers, rightLabelModifiers);
  return (
    <WizardInputWrapper modifiers={componentModifiers}>
      {children}
      <LeftLabel>
        <TextDiv modifiers={['midGreyText', 'xSmallText']}>
          <FormattedMessage {...label} />
        </TextDiv>
      </LeftLabel>
      {rightLabel &&
        <RightLabel modifiers={rightLabelModifiers}>
          <TextDiv modifiers={['midGreyText', 'xSmallText']}>
            <FormattedMessage {...rightLabel} values={{ value: rightLabelValue }} />
          </TextDiv>
        </RightLabel>
      }
    </WizardInputWrapper>
  );
}

WizardInput.propTypes = {
  children: PropTypes.node.isRequired,
  label: PropTypes.shape(messageDescriptorPropTypes).isRequired,
  rightLabel: PropTypes.shape(messageDescriptorPropTypes),
  rightLabelValue: PropTypes.number,
  modifiers: PropTypes.arrayOf(PropTypes.string),
};

WizardInput.defaultProps = {
  modifiers: [],
  rightLabel: undefined,
  rightLabelValue: 0,
};

export default WizardInput;
