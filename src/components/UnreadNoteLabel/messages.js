import { defineMessages } from 'react-intl';

const messages = defineMessages({
  unreadCount: {
    id: 'components.UnreadNoteLabel.label.unreadCount',
    defaultMessage: '{count} Unread {count, plural, one {Note} other {Notes}}',
  },
});

export default messages;
