import React from 'react';
import {
  test,
  expect,
  shallow,
  createSpy,
  spyOn,
} from '__tests__/helpers/test-setup';
import { noop } from 'lodash';

import UnreadNoteLabel from '../index';
import messages from '../messages';

const id = '123';
const unreadNotesCount = 3;
const defaultProps = {
  caseInfo: {
    id,
    unreadNotesCount,
  },
  onClickAction: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<UnreadNoteLabel {...props} />);
}

test('renders UnreadNotesIcon with correct name when unreadNotesCount is greater than 0', () => {
  const component = shallowRender();
  expect(component).toContain('UnreadNotesIcon');
  expect(component.find('UnreadNotesIcon')).toHaveProp('name', 'envelope');
});

test('renders UnreadNotesIcon with correct name when unreadNotesCount is 0', () => {
  const testProps = {
    ...defaultProps,
    caseInfo: { id: '123', unreadNotesCount: 0 },
  };
  const component = shallowRender(testProps);
  expect(component).toContain('UnreadNotesIcon');
  expect(component.find('UnreadNotesIcon')).toHaveProp('name', 'envelope-o');
});

test('renders a FormattedMessage with correct props', () => {
  const component = shallowRender();
  expect(component).toContain('FormattedMessage');
  expect(component.find('FormattedMessage').props()).toContain({
    ...messages.unreadCount,
    values: {
      count: unreadNotesCount,
    },
  });
});

test('handleToggleNotesModal updates the state', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.state.showCaseNotesModal).toEqual(false);
  instance.handleToggleNotesModal();
  expect(instance.state.showCaseNotesModal).toEqual(true);
});

test('handleClickLabel toggles a modal if hasModal prop is true', () => {
  const component = shallowRender({
    ...defaultProps,
    hasModal: true,
  });
  const instance = component.instance();
  const preventDefault = createSpy();
  const stopPropagation = createSpy();
  const toggleSpy = spyOn(instance, 'handleToggleNotesModal');
  instance.handleClickLabel({ preventDefault, stopPropagation });
  expect(preventDefault).toHaveBeenCalled();
  expect(stopPropagation).toHaveBeenCalled();
  expect(toggleSpy).toHaveBeenCalled();
});

test('handleClickLabel does appropriate action if hasModal prop is false', () => {
  const onClickAction = createSpy();
  const component = shallowRender({
    ...defaultProps,
    hasModal: false,
    onClickAction,
  });
  const instance = component.instance();
  const preventDefault = createSpy();
  const stopPropagation = createSpy();
  instance.handleClickLabel({ preventDefault, stopPropagation });
  expect(preventDefault).toHaveBeenCalled();
  expect(stopPropagation).toHaveBeenCalled();
  expect(onClickAction).toHaveBeenCalled();
});

test('hideIcon does not show the icon', () => {
  const component = shallowRender({ ...defaultProps, hideIcon: true });
  expect(component).toNotContain('UnreadNotesIcon');
});

test('hideIcon only renders one column with fluid modifier', () => {
  const component = shallowRender({ ...defaultProps, hideIcon: true });
  expect(component.find('withSize(Column)').length).toEqual(1);
  const modifiers = component.find('withSize(Column)').props().modifiers;
  expect(modifiers).toInclude('fluid');
});
