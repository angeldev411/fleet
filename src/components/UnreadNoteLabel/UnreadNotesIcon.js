import { omit } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

export function UnstyledFontAwesome(props) {
  const componentProps = omit(props, ['theme', 'modifiers']);
  return <FontAwesome {...componentProps} />;
}

/* istanbul ignore next */
const modifierConfig = {
  loud: ({ theme }) => ({
    styles: `
      color: ${theme.colors.status.info} !important;
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.textLight};
  font-size: ${px2rem(16)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      textLight: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      info: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'UnreadNotesIcon',
  styled(UnstyledFontAwesome),
  styles,
  { modifierConfig, themePropTypes },
);
