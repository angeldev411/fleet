import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  Column,
  Row,
} from 'styled-components-reactive-grid';
import { compact } from 'lodash';

import ModalWindow from 'components/ModalWindow';

import NotesWidget, { NotesWidgetHeader } from 'compositions/NotesWidget';

import { A } from 'base-components';
import TextDiv from 'elements/TextDiv';

import messages from './messages';
import UnreadNotesIcon from './UnreadNotesIcon';

function getIcon(count) {
  return count ? 'envelope' : 'envelope-o';
}

class UnreadNotesLabel extends Component {
  state = {
    showCaseNotesModal: false,
  }

  handleClickLabel = (event) => {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    const {
      hasModal,
      onClickAction,
    } = this.props;

    return hasModal ? this.handleToggleNotesModal() : onClickAction();
  }

  handleToggleNotesModal = () => {
    this.setState({
      showCaseNotesModal: !this.state.showCaseNotesModal,
    });
  }

  render() {
    const {
      caseInfo,
      hideIcon,
    } = this.props;
    const {
      showCaseNotesModal,
    } = this.state;

    const { unreadNotesCount } = caseInfo;
    const iconModifier = unreadNotesCount > 0 && 'loud';

    return (
      <div>
        <A
          href=""
          onClick={this.handleClickLabel}
        >
          <Row modifiers={['middle']}>
            { !hideIcon &&
              <Column>
                <UnreadNotesIcon
                  modifiers={compact([iconModifier])}
                  name={getIcon(unreadNotesCount)}
                />
              </Column>
            }
            <Column modifiers={compact([hideIcon && 'fluid'])}>
              <TextDiv modifiers={['smallText', 'statusInfo']}>
                <FormattedMessage
                  {...messages.unreadCount}
                  values={{ count: String(unreadNotesCount) }}
                />
              </TextDiv>
            </Column>
          </Row>
        </A>
        {showCaseNotesModal &&
          <ModalWindow
            headerInfo={{ title: <NotesWidgetHeader caseInfo={caseInfo} /> }}
            hideModal={this.handleToggleNotesModal}
            hideByClickingBackground
          >
            <NotesWidget caseInfo={caseInfo} type="modal" />
          </ModalWindow>
        }
      </div>
    );
  }
}

UnreadNotesLabel.propTypes = {
  hasModal: PropTypes.bool,
  caseInfo: PropTypes.shape({
    id: PropTypes.string,
    unreadNotesCount: PropTypes.number.isRequired,
  }).isRequired,
  onClickAction: PropTypes.func.isRequired,
  hideIcon: PropTypes.bool,
};

UnreadNotesLabel.defaultProps = {
  hasModal: false,
  hideIcon: false,
};

export default UnreadNotesLabel;
