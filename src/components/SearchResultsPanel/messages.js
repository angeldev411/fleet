import { defineMessages } from 'react-intl';

const messages = defineMessages({
  noResultsTitle: {
    id: 'components.SearchResultsPanel.message.noResultsTitle',
    defaultMessage: 'No matches for',
  },
  noResultsNoMatch: {
    id: 'components.SearchResultsPanel.message.noResultsSubtitle.noMatch',
    defaultMessage: 'Try broadening your search criteria.',
  },
  noResultsWrongQuery: {
    id: 'components.SearchResultsPanel.message.noResultsSubtitle.wrongQuery',
    defaultMessage: 'Enter at least 3 characters for your search.',
  },
});

export default messages;
