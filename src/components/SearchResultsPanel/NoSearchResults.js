import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage } from 'react-intl';

import messages from './messages';
import Wrapper from './NoSearchResultsWrapper';
import NoSearchResultsTitle from './NoSearchResultsTitle';
import NoSearchResultsTitleValue from './NoSearchResultsTitleValue';
import NoSearchResultsSubtitle from './NoSearchResultsSubtitle';

function NoSearchResults({ searchValue }) {
  return (
    <Wrapper>
      <FontAwesome name="search" />
      <NoSearchResultsTitle>
        <FormattedMessage {...messages.noResultsTitle} />
        <NoSearchResultsTitleValue>
          {` ${searchValue}`}
        </NoSearchResultsTitleValue>
      </NoSearchResultsTitle>
      <NoSearchResultsSubtitle>
        {searchValue && searchValue.length > 2 ?
          <FormattedMessage {...messages.noResultsNoMatch} />
          :
          <FormattedMessage {...messages.noResultsWrongQuery} />
        }
      </NoSearchResultsSubtitle>
    </Wrapper>
  );
}

NoSearchResults.propTypes = {
  searchValue: PropTypes.string.isRequired,
};

export default NoSearchResults;
