import styled from 'styled-components';

const NoSearchResultsTitleValue = styled.span`
  font-weight: bold;
`;

export default NoSearchResultsTitleValue;
