import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import messages from '../messages';
import NoSearchResults from '../NoSearchResults';

const defaultProps = {
  searchValue: 'test search value',
};

function shallowRender(props = defaultProps) {
  return shallow(<NoSearchResults {...props} />);
}

test('renders a search icon', () => {
  const component = shallowRender();
  expect(component).toContain('FontAwesome[name="search"]');
});

test('renders the title under the search icon', () => {
  const component = shallowRender().find('FormattedMessage').first();
  expect(component).toHaveProps({ ...messages.noResultsTitle });
});

test('renders the search query', () => {
  const component = shallowRender().find('NoSearchResultsTitleValue');
  expect(component.html()).toInclude(defaultProps.searchValue);
});

test('renders the subtitle with correct error message when there is no match', () => {
  const component = shallowRender().find('FormattedMessage').last();
  expect(component).toHaveProps({ ...messages.noResultsNoMatch });
});

test('renders the subtitle with correct error message when search value is less than 3 characters', () => {
  const searchValue = '12';
  const component = shallowRender({ searchValue }).find('FormattedMessage').last();
  expect(component).toHaveProps({ ...messages.noResultsWrongQuery });
});
