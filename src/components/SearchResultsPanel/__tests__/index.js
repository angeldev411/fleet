import { fromJS, List } from 'immutable';
import { noop } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import SearchResultsPanel from '../index';

const tabs = [
  { name: 'cases', label: { id: 'Cases' } },
  { name: 'assets', label: { id: 'Assets' } },
];

const defaultProps = {
  assets: List(),
  cases: List(),
  currentTab: tabs[0].name,
  handleTabChange: noop,
  refresh: noop,
  requesting: false,
  searchValue: 'test value',
  tabs,
  searchPagination: fromJS({
    cases: {
      latestPage: 1,
      perPage: 20,
      totalCount: 12,
      totalPages: 1,
    },
    assets: {
      latestPage: 1,
      perPage: 20,
      totalCount: 12,
      totalPages: 1,
    },
  }),
  requestNextCases: noop,
  requestNextAssets: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<SearchResultsPanel {...props} />);
}

test('renders a PageContentPanel', () => {
  const component = shallowRender();
  expect(component).toBeA('PageContentPanel');
});

test('renders a TabBar with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('TabBar');
  const tabBar = component.find('TabBar');
  expect(tabBar).toHaveProps({
    currentTab: defaultProps.currentTab,
    onChangeTab: defaultProps.handleTabChange,
    tabs: defaultProps.tabs,
  });
});

// ---------------------- AssetsListView ----------------------

test('renders an Asset FavoritesPaginator with the expected props when assets tab is selected', () => {
  const assets = fromJS([
    { id: '123' },
    { id: '456' },
    { id: '789' },
  ]);
  const cases = fromJS([
    { id: '321' },
  ]);
  const component = shallowRender({ ...defaultProps, currentTab: tabs[1].name, assets, cases });
  expect(component.find('FavoritesPaginator').dive()).toContain('AssetsListView');
});

test('does not render an Asset FavoritesPaginator when assets tab is not selected', () => {
  const assets = fromJS([
    { id: '123' },
    { id: '456' },
    { id: '789' },
  ]);
  const cases = fromJS([
    { id: '321' },
  ]);
  const component = shallowRender({ ...defaultProps, currentTab: 'wrong', assets, cases });
  expect(component.instance().buildAssetsList()).toEqual(null);
});

// ---------------------- CasesListView ----------------------

test('renders a Case FavoritesPaginator with the expected props when cases tab is selected', () => {
  const assets = fromJS([
    { id: '321' },
  ]);
  const cases = fromJS([
    { id: '123' },
    { id: '456' },
    { id: '789' },
  ]);
  const component = shallowRender({ ...defaultProps, currentTab: tabs[0].name, assets, cases });
  expect(component.find('FavoritesPaginator').dive()).toContain('CasesListView');
});

test('does not render a Case FavoritesPaginator when cases tab is not selected', () => {
  const assets = fromJS([
    { id: '321' },
  ]);
  const cases = fromJS([
    { id: '123' },
    { id: '456' },
    { id: '789' },
  ]);
  const component = shallowRender({ ...defaultProps, currentTab: 'wrong', assets, cases });
  expect(component).toNotContain('CasesListView');
});

// ---------------------- NoSearchResults ----------------------

test('renders NoSearchResults component with correct prop if requesting is false and there are no assets or cases', () => {
  const testProps = {
    ...defaultProps,
    requesting: false,
    searchPagination: fromJS({
      cases: {
        totalCount: 0,
      },
      assets: {
        totalCount: 0,
      },
    }),
  };
  const component = shallowRender(testProps);
  expect(component).toContain('NoSearchResults');
  expect(component.find('NoSearchResults')).toHaveProp('searchValue', defaultProps.searchValue);
});

test('does not render NoSearchResults component if requesting is true', () => {
  const testProps = {
    ...defaultProps,
    requesting: true,
  };
  const component = shallowRender(testProps);
  expect(component).toNotContain('NoSearchResults');
});

test('does not render NoSearchResults component if there is an asset or a case', () => {
  const assets = fromJS([
    { id: '321' },
  ]);
  const cases = fromJS([
    { id: '123' },
    { id: '456' },
    { id: '789' },
  ]);
  const testProps = {
    ...defaultProps,
    assets,
    cases,
  };
  const component = shallowRender(testProps);
  expect(component).toNotContain('NoSearchResults');
});
