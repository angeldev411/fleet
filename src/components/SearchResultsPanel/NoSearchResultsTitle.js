import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.text};
  font-size: ${px2rem(25)};
  font-weight: normal;
  line-height: ${px2rem(25)};
  margin-bottom: ${px2rem(16)};
  margin-top: ${px2rem(30)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'NoSearchResultsTitle',
  styled.h2,
  styles,
  { themePropTypes },
);
