import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.chrome300};
  font-size: ${px2rem(72)};
  margin-top: ${px2rem(90)};
  line-height: ${px2rem(72)};
  text-align: center;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'NoSearchResultsWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
