import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';

import CaseDisplayContainer from 'containers/CaseDisplayContainer';
import AssetDisplayElement from 'components/AssetDisplayElement';

import paginateFavorites from 'components/_common/paginateFavorites';
import { AssetsListView } from 'components/AssetsPanel';
import { CasesListView } from 'components/CasesPanel';
import RefreshBlock from 'components/RefreshBlock';
import TabBar, { TabBarOptionsWrapper } from 'components/TabBar';

import PageContentPanel from 'elements/PageContentPanel';

import NoSearchResults from './NoSearchResults';
import messages from './messages';

class SearchResultsPanel extends Component {
  componentWillMount() {
    const casesPaginationMethods = {
      requestNext: this.props.requestNextCases,
    };
    const assetsPaginationMethods = {
      requestNext: this.props.requestNextAssets,
    };
    this.PaginatedCasesView = paginateFavorites(casesPaginationMethods)(CasesListView);
    this.PaginatedAssetsView = paginateFavorites(assetsPaginationMethods)(AssetsListView);
  }

  buildCasesList = () => {
    const {
      currentTab,
      cases,
      requesting,
      searchPagination,
    } = this.props;

    if (currentTab !== 'cases') {
      return null;
    }

    const favoritePagination = searchPagination.get('cases');
    const componentProps = {
      cases,
      dataList: cases,
      requesting,
      favoritePagination,
      CardComp: CaseDisplayContainer,
    };
    const { PaginatedCasesView } = this;

    return (
      <PaginatedCasesView
        componentProps={componentProps}
        favoritePagination={favoritePagination}
        noFavoritesMessage={messages.noResultsNoMatch}
        requestInProgress={requesting}
      />
    );
  }

  buildAssetsList = () => {
    const {
      currentTab,
      assets,
      requesting,
      searchPagination,
    } = this.props;

    if (currentTab !== 'assets') {
      return null;
    }

    const favoritePagination = searchPagination.get('assets');
    const componentProps = {
      assets,
      dataList: assets,
      requesting,
      favoritePagination,
      CardComp: AssetDisplayElement,
    };
    const { PaginatedAssetsView } = this;

    return (
      <PaginatedAssetsView
        componentProps={componentProps}
        favoritePagination={favoritePagination}
        noFavoritesMessage={messages.noResultsNoMatch}
        requestInProgress={requesting}
      />
    );
  }

  /**
   * sumSearchPaginaton returns sum of totalCount values from each group
   * ("tab" in the UI) of the searchResults
   */
  sumSearchPagination = (searchPagination) => {
    const tabCounts = searchPagination.map(tabData => Number(tabData.get('totalCount') || 0));
    return tabCounts.reduce((a, b) => a + b, 0);
  }

  render() {
    const {
      currentTab,
      handleTabChange,
      refresh,
      requesting,
      searchValue,
      tabs,
      searchPagination,
    } = this.props;

    const matchesCount = this.sumSearchPagination(searchPagination);
    const noSearchResult = !requesting && matchesCount === 0;

    return (
      <PageContentPanel id="search-results-panel" >
        <TabBar
          currentTab={currentTab}
          onChangeTab={handleTabChange}
          tabs={tabs}
        >
          <TabBarOptionsWrapper>
            <RefreshBlock
              count={matchesCount}
              refresh={refresh}
              requesting={requesting}
              type="search"
            />
          </TabBarOptionsWrapper>
        </TabBar>
        {this.buildCasesList()}
        {this.buildAssetsList()}
        {noSearchResult && <NoSearchResults searchValue={searchValue} />}
      </PageContentPanel>
    );
  }
}

SearchResultsPanel.propTypes = {
  assets: ImmutablePropTypes.listOf(
    ImmutablePropTypes.mapContains({
      id: PropTypes.string,
    }),
  ).isRequired,
  cases: ImmutablePropTypes.listOf(
    ImmutablePropTypes.mapContains({
      id: PropTypes.string,
    }),
  ).isRequired,
  currentTab: PropTypes.string.isRequired,
  handleTabChange: PropTypes.func.isRequired,
  refresh: PropTypes.func.isRequired,
  requesting: PropTypes.bool.isRequired,
  searchValue: PropTypes.string.isRequired,
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.object.isRequired,
    }).isRequired,
  ).isRequired,
  searchPagination: ImmutablePropTypes.map.isRequired,
  requestNextCases: PropTypes.func.isRequired,
  requestNextAssets: PropTypes.func.isRequired,
};

export default SearchResultsPanel;
