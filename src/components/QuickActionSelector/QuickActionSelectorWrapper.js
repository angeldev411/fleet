import styled from 'styled-components';

const QuickActionSelectorWrapper = styled.div`
  position: relative;
  display: inline-block;
`;

export default QuickActionSelectorWrapper;
