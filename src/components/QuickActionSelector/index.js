import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';
import { noop, compact } from 'lodash';
import PropTypes from 'prop-types';
import { withTheme } from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';
import { QuickActionButton } from 'base-components';
import React, { Component } from 'react';
import { compose, setDisplayName } from 'recompose';

import AbsoluteSmoothCollapse from 'components/AbsoluteSmoothCollapse';
import PopupMenu from 'components/PopupMenu';

import {
  SelectorOptionsPopup,
  SelectorOptionsList,
  SelectorOptionListElement,
} from 'elements/SelectorButton';

import QuickActionSelectorWrapper from './QuickActionSelectorWrapper';

function renderOptionsList(options, onClick) {
  return options.map(option => (
    <SelectorOptionListElement
      key={option.id}
      id={`action-option-${option.id}`}
      onClick={() => onClick(option.id)}
      modifiers={['small']}
    >
      <FormattedMessage {...option.label} />
    </SelectorOptionListElement>
  ));
}

export class QuickActionSelector extends Component {
  state = {
    expanded: false,
  }

  toggleOptions = (value = false) => {
    this.setState({
      expanded: value,
    });
  }

  handleClickButton = () => {
    const {
      item: { id },
      onClick,
    } = this.props;

    this.toggleOptions(true);
    onClick({ buttonId: id });
  }

  handleClickItem = (dropdownItemId) => {
    const {
      item: { id },
      onClick,
    } = this.props;

    this.toggleOptions();
    onClick({ buttonId: id, dropdownItemId });
  }

  handleOutsideClick = (e) => {
    if (this.state.expanded) {
      e.stopPropagation();
      this.toggleOptions();
    }
  }

  render() {
    const {
      item,
      disabled,
      theme,
    } = this.props;
    const {
      expanded,
    } = this.state;
    const { label, icon, iconPressed = icon, options } = item;

    const wrapperClass = `action-selector-${item.id}`;
    const buttonClass = `action-button-${item.id}`;

    return (
      <QuickActionSelectorWrapper className={wrapperClass}>
        <QuickActionButton
          className={buttonClass}
          modifiers={compact([
            disabled && 'disabled',
            expanded && 'active',
          ])}
          onClick={this.handleClickButton}
        >
          <QuickActionButton.Text>
            <FormattedMessage {...label} />
          </QuickActionButton.Text>
          <QuickActionButton.Icon
            modifiers={['right']}
            name={expanded ? iconPressed : icon}
          />
        </QuickActionButton>
        <AbsoluteSmoothCollapse
          expanded={expanded}
          heightTransition={theme.transitions.heightExpand}
          top={px2rem(25)}
          left={px2rem(-50)}
          right={px2rem(-3)}
          zIndex={1}
        >
          <PopupMenu onOutsideClick={this.handleOutsideClick}>
            <SelectorOptionsPopup modifiers={['noPad']} minWidth="0">
              <SelectorOptionsList>
                {renderOptionsList(options, this.handleClickItem)}
              </SelectorOptionsList>
            </SelectorOptionsPopup>
          </PopupMenu>
        </AbsoluteSmoothCollapse>
      </QuickActionSelectorWrapper>
    );
  }
}

QuickActionSelector.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    iconPressed: PropTypes.string,
    label: PropTypes.shape(messageDescriptorPropTypes).isRequired,
  }).isRequired,
  theme: PropTypes.shape({
    transitions: PropTypes.shape({
      heightExpand: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

QuickActionSelector.defaultProps = {
  onClick: noop,
  disabled: false,
};

export default compose(
  setDisplayName('QuickActionSelector'),
  withTheme,
)(QuickActionSelector);
