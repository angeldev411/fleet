import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  spyOn,
} from '__tests__/helpers/test-setup';

import { withTestTheme } from 'utils/styles';

import { QuickActionSelector } from '../index';

const labelId = 'label ID';
const labelText = 'Text of the label';

const item = {
  id: 'the-item-id',
  icon: 'icon-name',
  iconPressed: 'icon-pressed-name',
  label: {
    id: labelId,
    defaultMessage: labelText,
  },
  options: [
    {
      id: 'printPage',
      label: {
        id: labelId,
        defaultMessage: labelText,
      },
    },
    {
      id: 'viewInFullScreenMode',
      label: {
        id: labelId,
        defaultMessage: labelText,
      },
    },
  ],
};

const onClick = expect.createSpy();

const defaultProps = {
  item,
  onClick,
};

const QuickActionSelectorWithTheme = withTestTheme(QuickActionSelector);

function shallowRender(props = defaultProps) {
  return shallow(<QuickActionSelectorWithTheme {...props} />);
}

test('renders a QuickActionButton from base-components', () => {
  const component = shallowRender();
  expect(component).toContain('QuickActionButton');
});

test('clicking QuickActionButton calls onClick with item.id', () => {
  const button = shallowRender().find('QuickActionButton');
  button.simulate('click');
  expect(onClick).toHaveBeenCalledWith({ buttonId: item.id });
});

test('includes `disabled` modifier to the button if disabled prop is set to true', () => {
  const component = shallowRender({ ...defaultProps, disabled: true });
  const button = component.find('QuickActionButton');
  expect(button.props().modifiers).toInclude('disabled');
});

test('includes `active` modifier to the button if item is expanded', () => {
  const component = shallowRender();
  component.setState({ expanded: true });
  const button = component.find('QuickActionButton');
  expect(button.props().modifiers).toInclude('active');
});

test('does not include `active` modifier to the button if the given item is not selected', () => {
  const component = shallowRender({ ...defaultProps, selectedId: 'something-else' });
  const button = component.find('QuickActionButton');
  expect(button.props().modifiers).toNotInclude('active');
});

test('renders FormattedMessage with the expected message props', () => {
  const component = shallowRender();
  expect(component.find('FormattedMessage').at(0)).toHaveProps({
    ...item.label,
  });
});

test('renders QuickActionButton.Icon with `right` modifier and correct icon name', () => {
  const component = shallowRender();
  expect(component).toContain('Icon');
  const icon = component.find('Icon');
  expect(icon.props().modifiers).toEqual(['right']);
  expect(icon).toHaveProp('name', item.icon);
});

test('renders the pressed icon if item is expanded', () => {
  const component = shallowRender();
  component.setState({ expanded: true });
  expect(component).toContain('Icon');
  const icon = component.find('Icon');
  expect(icon).toHaveProp('name', item.iconPressed);
});

test('renders AbsoluteSmoothCollapse component with the expected expanded props', () => {
  const component = shallowRender();
  expect(component).toContain('AbsoluteSmoothCollapse');
  const smoothCollapse = component.find('AbsoluteSmoothCollapse');
  expect(smoothCollapse).toHaveProp('expanded', component.state().expanded);
});

test('renders SelectorOptionListElement when item options provided', () => {
  const component = shallowRender();
  expect(component.find('SelectorOptionListElement').length).toEqual(defaultProps.item.options.length);
});

test('clicking SelectorOptionListElement calls onClick props', () => {
  const component = shallowRender();
  const element = component.find('SelectorOptionListElement').first();
  const event = {};
  element.simulate('click', event);
  expect(onClick).toHaveBeenCalledWith({
    buttonId: defaultProps.item.id,
    dropdownItemId: defaultProps.item.options[0].id,
  });
});

test('calls toggleOptions when handleOutsideClick is called with state expanded', () => {
  const component = shallowRender();
  component.setState({ expanded: true });
  component.instance().handleOutsideClick({ stopPropagation: noop });
  expect(component).toHaveState({ expanded: false });
});

test('does not call toggleOptions when handleOutsideClick is called with state collapsed', () => {
  const component = shallowRender();
  const instance = component.instance();
  const toggleOptionsSpy = spyOn(instance, 'toggleOptions');
  component.setState({ expanded: false });
  instance.handleOutsideClick();
  expect(toggleOptionsSpy).toNotHaveBeenCalled();
});
