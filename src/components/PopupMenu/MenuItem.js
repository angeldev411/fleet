import React from 'react';
import PropTypes from 'prop-types';

function MenuItem({ children }) {
  return (
    <div className="menu-item">
      {children}
    </div>
  );
}

MenuItem.propTypes = {
  children: PropTypes.node,
};

MenuItem.defaultProps = {
  children: null,
};

export default MenuItem;
