import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import MenuItem from '../MenuItem';

const renderComponent = () => shallow(<MenuItem />);

test('Renders a div', () => {
  const component = renderComponent();
  expect(component).toBeA('div');
});

test('Renders children', () => {
  const children = (<p id="testChild">Test child</p>);
  const component = shallow(
    <MenuItem >
      {children}
    </MenuItem>,
  );
  expect(component).toContain('p#testChild');
});

test('Has a correct className', () => {
  const component = renderComponent();
  expect(component).toHaveClass('menu-item');
});
