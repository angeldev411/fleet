import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import { PopupMenu } from '../index';

const defaultProps = { onOutsideClick: noop };

function shallowRender(props = defaultProps) {
  return shallow(<PopupMenu onOutsideClick={props.onOutsideClick}>{props.children}</PopupMenu>);
}

test('Renders children', () => {
  const children = (<p id="testChild">Test child</p>);
  const component = shallowRender({ ...defaultProps, children });
  expect(component).toContain('p#testChild');
});

test('PopupMenu handleClickOutside calls the provided handler', () => {
  const onOutsideClick = createSpy();
  const component = shallowRender({ ...defaultProps, onOutsideClick });
  component.instance().handleClickOutside();
  expect(onOutsideClick).toHaveBeenCalled();
});
