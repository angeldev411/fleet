import React, { Component } from 'react';
import PropTypes from 'prop-types';
import enhanceWithClickOutside from 'react-click-outside';

import Wrapper from './Wrapper';

export class PopupMenu extends Component {
  handleClickOutside = (e) => {
    this.props.onOutsideClick(e);
  }

  render() {
    const { children } = this.props;
    return (
      <Wrapper>
        { children }
      </Wrapper>
    );
  }
}

PopupMenu.propTypes = {
  children: PropTypes.node,
  onOutsideClick: PropTypes.func.isRequired,
};

PopupMenu.defaultProps = {
  children: null,
};

export default enhanceWithClickOutside(PopupMenu);
