import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = `
  width: 100%;
`;

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
);
