import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import Table from 'elements/Table';

/* istanbul ignore next */
const styles = props => `
  margin-top: ${px2rem(6)};
  th {
    color: ${props.theme.colors.base.text};
    font-weight: bold !important;
    min-width: ${px2rem(130)};
    padding: 0 0 ${px2rem(4)};
  }
  td {
    color: ${props.theme.colors.base.text};
    font-size: ${px2rem(10)};
    font-weight: normal !important;
    min-width: 0;
    padding: 0;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'StatusPopoverTable',
  styled(Table),
  styles,
  { themePropTypes },
);
