import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage } from 'react-intl';
import { camelCase, compact } from 'lodash';

import UnreadNoteLabel from 'containers/UnreadNoteLabelContainer';

import ETR from 'components/_common/ETR';
import FollowUpLabel from 'components/_common/FollowUpLabel';
import SeverityLabel from 'components/SeverityLabel';

import BorderedStatus from 'elements/BorderedStatus';
import Link from 'elements/Link';
import ListTable from 'elements/ListTable';
import Popover, {
  PopoverTarget,
  PopoverContent,
  ScrollingPopoverContentWrapper,
} from 'elements/Popover';
import StatusSpan from 'elements/StatusSpan';
import Span from 'elements/Span';
import TextDiv from 'elements/TextDiv';

import { getOutputText } from 'utils/widget';

import StatusPopoverContent from './StatusPopoverContent';
import StatusPopoverTable from './StatusPopoverTable';
import messages from './messages';

function CaseListRow({
  caseInfo,
  assetInfo,
  onSelect,
  isSelected,
}) {
  const caseId = caseInfo.get('id');
  const assetUnitNumber = assetInfo.get('unitNumber');
  const assetVINNumber = assetInfo.get('vinNumber');
  const providerName = caseInfo.getIn(['defaultRecipient', 'group', 'companyName']);
  const complaintCode = caseInfo.getIn(['complaint', 'code']);
  const complaintDescription = caseInfo.getIn(['complaint', 'description']);
  const complaintTitle = complaintCode && complaintDescription ? `${complaintCode} - ${complaintDescription}` : '';
  const caseDescription = caseInfo.get('description');
  const closedAt = caseInfo.get('closedAt');
  const openOrClosed = closedAt ? 'closed' : 'open';
  const repairStatus = caseInfo.get('repairStatus');
  const estimateTotal = caseInfo.get('estimateTotal');
  const approvalStatus = caseInfo.get('approvalStatus');
  const approvalStatusModifier = approvalStatus && camelCase(approvalStatus);
  const downtime = caseInfo.get('downtime');
  const severityColor = caseInfo.get('severityColor');
  const severityCount = Number(caseInfo.get('severityCount'));
  const followUpColor = caseInfo.get('followUpColor');
  const followUpTime = caseInfo.get('followUpTime');
  const rowModifier = isSelected ? 'selected' : 'selectable';
  return (
    <ListTable.Row
      modifiers={['hoverHighlight', 'lined', rowModifier]}
      onClick={onSelect}
    >
      <td>
        <Link
          to={`/cases/${caseId}`}
          modifiers={['heavy', 'uppercase']}
        >
          <FormattedMessage {...messages.title} values={{ caseId }} />
        </Link>
      </td>
      <td>
        <Popover
          showOnHover
          position="right"
          positionOffset={35}
        >
          <PopoverTarget>
            <BorderedStatus modifiers={[openOrClosed]}>{openOrClosed}</BorderedStatus>
          </PopoverTarget>
          <PopoverContent>
            <StatusPopoverContent>
              <TextDiv modifiers={['uppercase', 'xSmallText']}>
                <FormattedMessage {...messages.statusPopoverTitle} />
              </TextDiv>
              <StatusPopoverTable modifiers={['smallText']}>
                <tbody>
                  <tr>
                    <th><FormattedMessage {...messages.statusPopoverCaseStatus} /></th>
                    <td>
                      <BorderedStatus modifiers={[openOrClosed]}>{openOrClosed}</BorderedStatus>
                    </td>
                  </tr>
                  <tr>
                    <th><FormattedMessage {...messages.statusPopoverRepairStatus} /></th>
                    <td>{getOutputText(repairStatus, messages.status)}</td>
                  </tr>
                </tbody>
              </StatusPopoverTable>
            </StatusPopoverContent>
          </PopoverContent>
        </Popover>
      </td>
      <td>
        <Link to={`/assets/${assetInfo.get('id')}`}>
          {getOutputText(assetUnitNumber, messages.unit)}
        </Link>
      </td>
      <td>
        {getOutputText(assetVINNumber)}
      </td>
      <td>
        <TextDiv modifiers={['ellipsis', 'short', 'smallWidth']}>
          {getOutputText(providerName)}
        </TextDiv>
      </td>
      <td>
        <Popover showOnHover>
          <PopoverTarget>
            <TextDiv
              modifiers={[
                'bottomGap',
                'heavyText',
                'ellipsis',
                'oneLine',
                'smallText',
                'smallWidth',
              ]}
            >
              {getOutputText(complaintTitle, messages.undefinedComplaintSubTitle)}
            </TextDiv>
            <TextDiv modifiers={['ellipsis', 'short', 'smallWidth']}>
              {getOutputText(caseDescription, messages.undefinedComplaint)}
            </TextDiv>
          </PopoverTarget>
          <PopoverContent>
            <ScrollingPopoverContentWrapper modifiers={['smallText']}>
              <TextDiv modifiers={['bold', 'bright', 'bottomGap']}>
                {getOutputText(complaintTitle, messages.undefinedComplaintSubTitle)}
              </TextDiv>
              <TextDiv modifiers={['bottomGap']}>
                {getOutputText(caseDescription, messages.undefinedComplaint)}
              </TextDiv>
            </ScrollingPopoverContentWrapper>
          </PopoverContent>
        </Popover>
      </td>
      <td>
        <TextDiv>
          {getOutputText(estimateTotal, messages.estimate)}
        </TextDiv>
        <StatusSpan modifiers={compact([approvalStatusModifier, 'bold', 'tall', 'uppercase'])}>
          {approvalStatus}
        </StatusSpan>
      </td>
      <td className="etr">
        <ETR caseInfo={caseInfo} maxWidth="6rem" />
      </td>
      <td>
        <Span modifiers={['capitalize']}>
          {getOutputText(downtime, messages.downtime)}
        </Span>
      </td>
      <td>
        <SeverityLabel
          color={severityColor}
          value={severityCount}
          small
        />
      </td>
      <td>
        <UnreadNoteLabel hasModal caseInfo={caseInfo} />
      </td>
      <td>
        <FollowUpLabel
          followUpColor={followUpColor}
          followUpTime={followUpTime}
        />
      </td>
    </ListTable.Row>
  );
}

CaseListRow.propTypes = {
  assetInfo: ImmutablePropTypes.contains({
    id: PropTypes.string,
  }).isRequired,
  caseInfo: ImmutablePropTypes.contains({
    id: PropTypes.string,
  }).isRequired,
  onSelect: PropTypes.func.isRequired,
  isSelected: PropTypes.bool.isRequired,
};

export default CaseListRow;
