import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import CaseListRow from '../CaseListRow';
import messages from '../messages';

const caseInfo = fromJS({
  approvalStatus: 'Pending',
  id: '123',
  unreadNotesCount: 10,
  closedAt: '2017-01-13T16:46:22Z',
  repairStatus: 'Approved',
});

const assetInfo = fromJS({
  id: '456',
  unitNumber: '2343',
  vinNumber: '3GBKC34F41M111906',
});

const serviceProviderInfo = fromJS({
  id: '789',
});

const onSelect = expect.createSpy();

const isSelected = false;

const defaultProps = {
  caseInfo,
  assetInfo,
  serviceProviderInfo,
  onSelect,
  isSelected,
};

function shallowRender(props = defaultProps) {
  return shallow(<CaseListRow {...props} />);
}

test('passes the correct modifier to ListTable.Row if it is not selected', () => {
  const component = shallowRender();
  expect(component.props().modifiers).toContain('selectable');
});

test('passes the correct modifier to ListTable.Row if it is selected', () => {
  const testProps = {
    ...defaultProps,
    isSelected: true,
  };
  const component = shallowRender(testProps);
  expect(component.props().modifiers).toContain('selected');
});

test('call onSelect prop when the row is clicked', () => {
  const component = shallowRender();
  component.simulate('click');
  expect(onSelect).toHaveBeenCalled();
});

test('renders a Link for caseId with the correct props', () => {
  const component = shallowRender();
  const link = component.find('Link').first();
  expect(link).toHaveProp('to', `/cases/${caseInfo.get('id')}`);
});

test('renders a Link for caseId  with the correct caseId', () => {
  const component = shallowRender();
  const linkText = component.find('Link').first();
  expect(linkText.find('FormattedMessage').props()).toContain({
    ...messages.title,
    values: {
      caseId: caseInfo.get('id'),
    },
  });
});

test('renders a Link for unitNumber with the correct props', () => {
  const component = shallowRender();
  const link = component.find('Link').last();
  expect(link).toHaveProp('to', `/assets/${assetInfo.get('id')}`);
});

test('renders a Link for unitNumber  with the correct unitNumber', () => {
  const component = shallowRender();
  const linkText = component.find('Link').last();
  expect(linkText.props().children).toEqual(assetInfo.get('unitNumber'));
});

test('passes the correct modifier to the status component', () => {
  const component = shallowRender();
  const approvalSpan = component.find('StatusSpan');
  expect(approvalSpan.props().modifiers).toContain('pending');
});

test('removes invalid modifiers from props', () => {
  const newCaseInfo = fromJS({ ...caseInfo.toJS(), approvalStatus: null });
  const component = shallowRender({ ...defaultProps, caseInfo: newCaseInfo });
  const approvalSpan = component.find('StatusSpan');
  // this status span has three modifiers by default
  expect(approvalSpan.props().modifiers.length).toEqual(3);
});

test('renders case status (open/closed) in a Popover correctly when closedAt is not null', () => {
  const component = shallowRender();
  const openOrClosed = 'closed'; // because defaultProps.caseInfo.get('closedAt') !== null
  const borderedStatus = component.find('PopoverTarget').at(0).find('BorderedStatus');
  expect(borderedStatus.props().modifiers).toInclude(openOrClosed);
  expect(borderedStatus.render().text()).toInclude(openOrClosed);
});

test('renders case status (open/closed) in a Popover correctly when closedAt is null', () => {
  const testCaseInfo = caseInfo.set('closedAt', null);
  const component = shallowRender({ ...defaultProps, caseInfo: testCaseInfo });
  const openOrClosed = 'open'; // because closedAt is null
  const borderedStatus = component.find('PopoverTarget').at(0).find('BorderedStatus');
  expect(borderedStatus.props().modifiers).toInclude(openOrClosed);
  expect(borderedStatus.render().text()).toInclude(openOrClosed);
});

test('renders correct case status on hovering the Case Status row', () => {
  const component = shallowRender();
  const popoverTable = component.find('PopoverContent').at(0).find('tbody');
  expect(popoverTable.find('BorderedStatus').render().text()).toEqual('closed');
  expect(popoverTable.find('td').at(1).text()).toEqual('Approved');
});

test('renders correct VIN number if provided', () => {
  const component = shallowRender();
  const vinTd = component.find('td').at(5);
  expect(vinTd.text()).toEqual(assetInfo.get('vinNumber'));
});

test('renders the right complaint title if there are both complaint code and description', () => {
  const newCaseInfo = fromJS({
    ...caseInfo.toJS(),
    complaint: { code: 'CODE', description: 'DESCRIPTION' },
  });
  const component = shallowRender({ ...defaultProps, caseInfo: newCaseInfo });
  const complaintTitle = 'CODE - DESCRIPTION';
  const titleComponent = component.find('PopoverTarget').at(1).find('TextDiv').first();
  expect(titleComponent.render().text()).toInclude(complaintTitle);
});
