import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import UnreadNoteLabel from 'containers/UnreadNoteLabelContainer';

import AdditionalInfo from '../AdditionalInfo';

const followUpColor = 'red';
const followUpTime = '2017-01-11T16:46:22Z';
const unreadNotesCount = 10;
const caseInfo = fromJS({
  followUpColor,
  followUpTime,
  unreadNotesCount,
});

const defaultProps = {
  caseInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<AdditionalInfo {...props} />);
}

test('Renders a CardElement', () => {
  const component = shallowRender();
  expect(component).toBeA('CardElement');
});

test('Renders an UnreadNotesLabel with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain(UnreadNoteLabel);
  expect(component.find(UnreadNoteLabel)).toHaveProp('caseInfo', caseInfo);
});

test('Renders a FollowUpLabel with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('FollowUpLabel');
  expect(component.find('FollowUpLabel')).toHaveProps({
    followUpColor,
    followUpTime,
  });
});
