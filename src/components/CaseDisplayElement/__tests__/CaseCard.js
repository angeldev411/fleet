import React from 'react';
import { fromJS } from 'immutable';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import CaseCard from '../CaseCard';

const caseInfo = fromJS({
  id: '123',
});

const assetInfo = fromJS({
  id: '456',
});

const serviceProviderInfo = fromJS({
  id: '789',
});

const onSelect = expect.createSpy();

const isSelected = false;

const defaultProps = {
  caseInfo,
  assetInfo,
  serviceProviderInfo,
  onSelect,
  isSelected,
};

function shallowRender(props = defaultProps) {
  return shallow(<CaseCard {...props} />);
}

test('Renders a CardView', () => {
  const component = shallowRender();
  expect(component).toBeA('Card');
});

test('Renders a Card with the expected props if the Card is not selected', () => {
  const component = shallowRender();
  const card = component.find('Card');
  expect(card.props().modifiers).toEqual(['selectable']);
});

test('Renders a Card with the expected props if the Card is selected', () => {
  const testProps = {
    ...defaultProps,
    isSelected: true,
  };
  const component = shallowRender(testProps);
  const card = component.find('Card');
  expect(card.props().modifiers).toEqual(['selected']);
});

test('Calls onSelect prop when the Card is clicked', () => {
  const component = shallowRender();
  component.simulate('click');
  expect(onSelect).toHaveBeenCalled();
});

test('Renders a CaseTitle with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('CaseTitle');
  expect(component.find('CaseTitle')).toHaveProp('caseInfo', caseInfo);
});

test('Renders an AssetInfo with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AssetInfo');
  expect(component.find('AssetInfo')).toHaveProp('assetInfo', assetInfo);
});

test('Renders a ServiceProvider with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('ServiceProvider');
  expect(component.find('ServiceProvider')).toHaveProp('serviceProviderInfo', serviceProviderInfo);
});

test('Renders a Complaint with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('Complaint');
  expect(component.find('Complaint')).toHaveProp('caseInfo', caseInfo);
});

test('Renders a CaseInfo with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('CaseInfo');
  expect(component.find('CaseInfo')).toHaveProp('caseInfo', caseInfo);
});

test('Renders a AdditionalInfo with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('AdditionalInfo');
  expect(component.find('AdditionalInfo')).toHaveProp('caseInfo', caseInfo);
});
