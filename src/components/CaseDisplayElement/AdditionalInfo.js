import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';

import FollowUpLabel from 'components/_common/FollowUpLabel';
import UnreadNoteLabel from 'containers/UnreadNoteLabelContainer';

import { CardElement } from 'elements/Card';
import { SplitBlock, SplitBlockElement, SplitBlockPart } from 'elements/SplitBlock';

function AdditionalInfo({ caseInfo }) {
  const followUpColor = caseInfo.get('followUpColor');
  const followUpTime = caseInfo.get('followUpTime');

  return (
    <CardElement>
      <SplitBlock>
        <SplitBlockPart modifiers={['left', 'wide']}>
          <SplitBlockElement modifiers={['padRight']}>
            <UnreadNoteLabel hasModal caseInfo={caseInfo} />
          </SplitBlockElement>
          <SplitBlockElement modifiers={['padLeft']}>
            <FollowUpLabel
              followUpColor={followUpColor}
              followUpTime={followUpTime}
            />
          </SplitBlockElement>
        </SplitBlockPart>
      </SplitBlock>
    </CardElement>
  );
}

AdditionalInfo.propTypes = {
  caseInfo: ImmutablePropTypes.map.isRequired,
};

export default AdditionalInfo;
