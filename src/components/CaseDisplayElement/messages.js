import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  title: {
    id: 'components.CaseDisplayElement.title',
    defaultMessage: '#{caseId}',
  },
  complaintTitle: {
    id: 'components.CaseDisplayElement.Complaint.title',
    defaultMessage: 'Complaint',
  },
  downtime: {
    id: 'components.CaseDisplayElement.CaseInfo.downtime',
    defaultMessage: 'Downtime',
  },
  estimate: {
    id: 'components.CaseDisplayElement.CaseInfo.estimate',
    defaultMessage: 'Estimate',
  },
  etr: {
    id: 'components.CaseDisplayElement.CaseInfo.etr',
    defaultMessage: 'ETR',
  },
  status: {
    id: 'components.CaseDisplayElement.CaseInfo.status',
    defaultMessage: 'Status',
  },
  statusPopoverCaseStatus: {
    id: 'components.CaseDisplayElement.CaseListRow.statusPopover.caseStatus',
    defaultMessage: 'Case Status',
  },
  statusPopoverRepairStatus: {
    id: 'components.CaseDisplayElement.CaseListRow.statusPopover.repairStatus',
    defaultMessage: 'Repair Status',
  },
  statusPopoverTitle: {
    id: 'components.CaseDisplayElement.CaseListRow.statusPopover.title',
    defaultMessage: 'Status',
  },
  undefinedComplaint: {
    id: 'components.CaseDisplayElement.Complaint.undefined.complaint',
    defaultMessage: 'No Complaints Yet',
  },
  undefinedComplaintSubTitle: {
    id: 'components.CaseDisplayElement.Complaint.undefined.complaintSubTitle',
    defaultMessage: 'Code - Description',
  },
  undefinedServiceProvider: {
    id: 'components.CaseDisplayElement.undefined.serviceProvider',
    defaultMessage: 'Service Provider',
  },
  unit: {
    id: 'components.CaseDisplayElement.AssetInfo.unit',
    defaultMessage: 'Unit',
  },
  vin: {
    id: 'components.CaseDisplayElement.AssetInfo.vin',
    defaultMessage: 'VIN',
  },
});

export default formattedMessages;
