import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import DangerousHtmlMessage from '../index';

function shallowRender(props) {
  return shallow(<DangerousHtmlMessage {...props} />);
}

test('renders the given message in a TextWithLinks tag', () => {
  const message = 'message';
  const component = shallowRender({ message });
  const paragraphTag = component.find('TextWithLinks');
  expect(paragraphTag.length).toEqual(1);
  const text = paragraphTag.first().html();
  expect(text).toInclude(message);
});

test('properly renders links within the message', () => {
  const url = 'http://www.decisiv.com';
  const linkText = 'contains a link';
  const messageWithHtml = `This message <a href="${url}">${linkText}</a>`;
  const component = shallowRender({ message: messageWithHtml });
  const rendered = component.render();
  expect(rendered.find('p').html()).toEqual(messageWithHtml);
  const anchorTag = rendered.find('a');
  expect(anchorTag.attr('href')).toEqual(url);
  const anchorTagContent = anchorTag.html();
  expect(anchorTagContent).toEqual(linkText);
});

test('unsafe HTML is removed', () => {
  const unsafeMessage = '<span>abc<iframe/\\/src=jAva&Tab;script:alert(3)>def';
  const sanitizedMessage = '<span>abcdef</span>';
  const component = shallowRender({ message: unsafeMessage });
  const rendered = component.render();
  expect(rendered.find('p').html()).toEqual(sanitizedMessage);
});

test('strips any tags other than A, BR, and SPAN', () => {
  const message = `<p>
abc<img src="foo.png"> d 
   ef<style>ghi</style><b><a href="test.html">bold</a></b><br>
<i>italic</i><span>spanned</span>
</p><script src="javascript.js"></script>
<h1>Heading</h1>`;

  const sanitizedMessage = `
abc d 
   ef<a href="test.html">bold</a><br>
italic<span>spanned</span>

Heading`;

  const component = shallowRender({ message });
  const rendered = component.render();
  expect(rendered.find('p').html()).toEqual(sanitizedMessage);
});
