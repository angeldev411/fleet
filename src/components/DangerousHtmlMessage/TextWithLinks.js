import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  a {
    color: ${props.theme.colors.base.link};
    text-decoration: none;
    &:hover {
      color: ${props.theme.colors.base.linkHover};
      text-decoration: underline;
    }
    cursor: pointer;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      link: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'TextWithLinks',
  styled.p,
  styles,
  { themePropTypes },
);
