import DOMPurify from 'dompurify';
import PropTypes from 'prop-types';
import React from 'react';

import TextWithLinks from './TextWithLinks';

const SANITIZE_OPTS = {
  ALLOWED_TAGS: ['a', 'br', 'span'],
};

function sanitizedHtml(message) {
  return { __html: DOMPurify.sanitize(message, SANITIZE_OPTS) };
}

function DangerousHtmlMessage({ message }) {
  return (
    <TextWithLinks dangerouslySetInnerHTML={sanitizedHtml(message)} />
  );
}

DangerousHtmlMessage.propTypes = {
  message: PropTypes.string,
};

DangerousHtmlMessage.defaultProps = {
  message: '',
};

export default DangerousHtmlMessage;
