import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  createSpy,
  shallow,
} from '__tests__/helpers/test-setup';

import WindowQuery from '../index';

const defaultProps = {
  DEBOUNCE_TIME_MS: 250,
  children: <div>Children</div>,
  onResize: noop,
};

function renderComponent(props = defaultProps) {
  return shallow(<WindowQuery {...props} />);
}

test('call onResize when resizeEventHandler is triggerred', () => {
  const onResize = createSpy();
  const component = renderComponent({ ...defaultProps, onResize });
  component.instance().resizeEventHandler();
  expect(onResize).toHaveBeenCalledWith({ windowWidth: window.innerWidth });
});
