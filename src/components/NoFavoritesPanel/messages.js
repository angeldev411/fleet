import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'components.NoFavoritesPanel.title',
    defaultMessage: 'No matches for',
  },
  instruction: {
    id: 'components.NoFavoritesPanel.instruction',
    defaultMessage: 'Try removing or editing some of your criteria to expand the search or review another favorite.',
  },
});

export default messages;
