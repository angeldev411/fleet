import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  width: 90%;
  max-width: ${px2rem(542)};
  color: ${props.theme.colors.base.text};
  font-size: ${px2rem(20)};
  line-height: ${px2rem(25)};
  text-align: center;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Instruction',
  styled.span,
  styles,
  { themePropTypes },
);
