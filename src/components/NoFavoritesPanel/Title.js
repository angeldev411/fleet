import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  margin: ${px2rem(30)} 0 ${px2rem(16)};
  color: ${props.theme.colors.base.text};
  font-size: ${px2rem(25)};
  line-height: ${px2rem(25)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Title',
  styled.span,
  styles,
  { themePropTypes },
);
