import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';

import TextDiv from 'elements/TextDiv';

import messages from './messages';
import Wrapper from './Wrapper';
import Title from './Title';
import Instruction from './Instruction';

function NoFavoritesPanel({ filter }) {
  return (
    <Wrapper>
      <FontAwesome name="star-o" />
      <Title>
        <FormattedMessage {...messages.title} />&nbsp;
        <TextDiv modifiers={['heavyText', 'inline']}><FormattedMessage {...filter} /></TextDiv>
      </Title>
      <Instruction>
        <FormattedMessage {...messages.instruction} />
      </Instruction>
    </Wrapper>
  );
}

NoFavoritesPanel.propTypes = {
  filter: PropTypes.shape(messageDescriptorPropTypes).isRequired,
};

export default NoFavoritesPanel;
