import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  padding: ${px2rem(70)} 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: ${px2rem(68)};
  font-weight: 300;
  color: ${props.theme.colors.base.chrome300};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
  { themePropTypes },
);
