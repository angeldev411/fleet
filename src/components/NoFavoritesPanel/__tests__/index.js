import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import NoFavoritesPanel from '../index';
import messages from '../messages';

const defaultProps = {
  filter: {
    id: 'favorites.uptimeCases',
    defaultMessage: 'Uptime Cases',
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<NoFavoritesPanel {...props} />);
}

test('renders star outline fontawesome icon', () => {
  const component = shallowRender();

  expect(component).toContain('FontAwesome');
  expect(component.find('FontAwesome')).toHaveProp('name', 'star-o');
});

test('renders title and filter', () => {
  const component = shallowRender();
  const formattedMessages = component.find('FormattedMessage');

  expect(formattedMessages.first()).toHaveProps(messages.title);
  expect(formattedMessages.at(1)).toHaveProps(defaultProps.filter);
});

test('renders instruction', () => {
  const component = shallowRender();
  const formattedMessages = component.find('FormattedMessage');

  expect(formattedMessages.last()).toHaveProps(messages.instruction);
});
