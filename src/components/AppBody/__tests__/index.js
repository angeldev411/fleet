import React from 'react';
import { animateScroll } from 'react-scroll';
import {
  test,
  expect,
  shallow,
  spyOn,
} from '__tests__/helpers/test-setup';

import AppBody from '../index';

const defaultExpanded = true;
const defaultChildren = <p>child</p>;
function renderComponent({ expanded = defaultExpanded, children = defaultChildren } = {}) {
  return shallow(<AppBody expanded={expanded}>{children}</AppBody>);
}

test('AppBody passes `expanded` to the AppWrapper', () => {
  const appBody = renderComponent({ expanded: false });
  const appWrapper = appBody.find('AppWrapper');
  expect(appWrapper).toHaveProp('expanded', false);
});

test('AppBody contains the GlobalFooter in a ContentWrapper', () => {
  const appBody = renderComponent();
  expect(appBody).toContain('AppWrapper ContentWrapper GlobalFooter');
});

test('AppBody renders children into the ContentWrapper', () => {
  const childNode = <p id="testChild">This is the child</p>;
  const appBody = renderComponent({ children: childNode });
  expect(appBody.find('ContentWrapper')).toContain('p#testChild');
});

test('AppBody sets scrollHeight in localState when the component mounts', () => {
  const appBody = renderComponent();
  expect(appBody.state().scrollHeight).toEqual(0);
});

test('The scroll button is hidden when scrollHeight is less than 1000px', () => {
  const appBody = renderComponent();
  const floatingActionWrapper = appBody.find('FloatingActionWrapper');
  expect(floatingActionWrapper.props().modifiers).toContain('hide');
});

test('The scroll button is visible when scrollHeight is >= than 1000px', () => {
  const appBody = renderComponent();
  appBody.setState({ scrollHeight: 1000 });
  const floatingActionWrapper = appBody.find('FloatingActionWrapper');
  expect(floatingActionWrapper.props().modifiers).toNotContain('hide');
});

test('The state scrollHeight is set when scrollListener is called.', () => {
  const scrollTop = 10000;
  const appBody = renderComponent();
  const instance = appBody.instance();
  appBody.setState({ scrollHeight: 100 });
  document.body.scrollTop = scrollTop;
  instance.scrollListener();
  expect(appBody.state().scrollHeight).toEqual(scrollTop);
});

test('Call scrollToTop when reactangle button is clicked.', () => {
  const appBody = renderComponent();
  const rectangleButton = appBody.find('RectangleButton');
  const scrollToTopSpy = spyOn(animateScroll, 'scrollToTop');
  rectangleButton.simulate('click');
  expect(scrollToTopSpy).toHaveBeenCalled();
});
