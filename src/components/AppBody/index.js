import { compact } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import { animateScroll } from 'react-scroll';
import { FormattedMessage } from 'react-intl';

import { getScrollTop } from 'utils/document';

import FloatingActionWrapper from 'elements/FloatingActionWrapper';
import RectangleButton, {
  RectangleButtonIcon,
  RectangleButtonText,
} from 'elements/RectangleButton';

import GlobalFooter from 'components/GlobalFooter';

import AppWrapper from './AppWrapper';
import ContentWrapper from './ContentWrapper';
import messages from './messages';

class AppBody extends React.Component {
  state = {
    scrollHeight: 0,
  };

  componentDidMount() {
    window.addEventListener('scroll', this.scrollListener);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollListener);
  }

  scrollListener = () => {
    this.setState({ scrollHeight: getScrollTop(document) });
  }

  render() {
    const { expanded, children } = this.props;
    const { scrollHeight } = this.state;
    const hide = scrollHeight < 1000 && 'hide';
    const bounceIn = scrollHeight < 1000 && 'bounceIn';

    return (
      <AppWrapper
        expanded={expanded}
        id="app-wrapper"
      >
        <ContentWrapper>
          {children}
          <GlobalFooter
            isBranded
            version="0.1.0-alpha"
          />
        </ContentWrapper>
        <FloatingActionWrapper
          modifiers={compact(['bottomRight', bounceIn, 'boxShadow', hide])}
        >
          <RectangleButton
            className="back-to-top-button"
            modifiers={[
              'hoverBrandBright',
              'hoverUnderline',
              'offWhite',
              'small',
            ]}
            onClick={() => animateScroll.scrollToTop()}
          >
            <RectangleButtonText modifiers={['small']}>
              <FormattedMessage {...messages.backToTopButton} />
            </RectangleButtonText>
            <RectangleButtonIcon modifiers={['padLeft']}>
              <FontAwesome name="long-arrow-up" />
            </RectangleButtonIcon>
          </RectangleButton>
        </FloatingActionWrapper>
      </AppWrapper>
    );
  }
}

AppBody.propTypes = {
  children: PropTypes.node.isRequired,
  expanded: PropTypes.bool,
};

AppBody.defaultProps = {
  children: null,
  expanded: true,
};

export default AppBody;
