import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  backToTopButton: {
    id: 'components.AppBody.backToTopButton',
    defaultMessage: 'Back to Top',
  },
});

export default formattedMessages;
