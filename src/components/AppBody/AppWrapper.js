import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

function expandedStyle({ expanded, theme }) {
  if (expanded) {
    return theme.dimensions.leftNavWidth;
  }
  return theme.dimensions.leftNavWidthCollapsed;
}

/* istanbul ignore next */
const styles = props => `
  height: 100%;
  position: absolute;
  left: ${expandedStyle(props)};
  right: 0;
  transition: left ${props.theme.dimensions.leftNavExpandTime};
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    leftNavExpandTime: PropTypes.string.isRequired,
    leftNavWidth: PropTypes.string.isRequired,
    leftNavWidthCollapsed: PropTypes.string.isRequired,
  }).isRequired,
};

const propTypes = {
  expanded: PropTypes.bool,
};

const defaultProps = {
  expanded: false,
};

export default buildStyledComponent(
  'AppWrapper',
  styled.section,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
