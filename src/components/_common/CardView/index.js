import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import ResponsiveCardGrid, { CARD_GRID_BREAKPOINTS } from 'elements/Card/ResponsiveCardGrid';

import CardsWithSize from './CardsWithSize';

function CardView({ dataList, favoritePagination, requesting, CardComp }) {
  return (
    <ResponsiveCardGrid breakpoints={CARD_GRID_BREAKPOINTS}>
      <CardsWithSize
        dataList={dataList}
        favoritePagination={favoritePagination}
        requesting={requesting}
        CardComp={CardComp}
      />
    </ResponsiveCardGrid>
  );
}

CardView.propTypes = {
  dataList: ImmutablePropTypes.listOf(
    ImmutablePropTypes.mapContains({
      id: PropTypes.string,
    }),
  ).isRequired,
  favoritePagination: ImmutablePropTypes.map.isRequired,
  requesting: PropTypes.bool.isRequired,
  CardComp: PropTypes.func.isRequired,
};

export default CardView;
