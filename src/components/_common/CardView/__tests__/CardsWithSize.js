import React from 'react';
import { fromJS, List, Map } from 'immutable';
import { sizes } from 'reactive-container';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import { CardsWithSize } from '../CardsWithSize';

const DummyCard = () => <div />;

const defaultProps = {
  dataList: List(),
  favoritePagination: Map(),
  requesting: false,
  size: sizes.LG,
  CardComp: DummyCard,
};

function shallowRender(props = defaultProps) {
  return shallow(<CardsWithSize {...props} />);
}

test('CardsWithSize renders one Card per datum', () => {
  const dataList = fromJS([{ id: '1' }, { id: '2' }, { id: '3' }]);
  const component = shallowRender({ ...defaultProps, dataList });
  expect(component.children().length).toEqual(3);
});

test('CardsWithSize renders 8 ghost cards if no dataList is provided because default size is large', () => {
  const component = shallowRender();
  expect(component.find('CardLoading').length).toEqual(8);
});

test('CardsWithSize renders 2 ghost cards if no dataList provided and size is xsmall', () => {
  const component = shallowRender({
    ...defaultProps,
    size: sizes.XS,
  });
  expect(component.find('CardLoading').length).toEqual(2);
});

test('CardsWithSize renders diff of dataList if less than 8', () => {
  const dataList = fromJS([{ id: '1' }, { id: '2' }, { id: '3' }]);
  const favoritePagination = Map({
    totalCount: 8,
  });
  const component = shallowRender({
    dataList,
    favoritePagination,
    requesting: true,
    size: sizes.LG,
    CardComp: DummyCard,
  });
  expect(component.find('CardLoading').length).toEqual(5);
});
