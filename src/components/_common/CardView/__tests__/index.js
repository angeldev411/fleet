import React from 'react';
import { List, Map } from 'immutable';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import CardView from '../index';

const DummyCard = () => <div />;

const defaultProps = {
  dataList: List(),
  favoritePagination: Map(),
  requesting: false,
  CardComp: DummyCard,
};

function shallowRender(props = defaultProps) {
  return shallow(<CardView {...props} />);
}

test('CardView renders CardsWithSize with correct props', () => {
  const component = shallowRender();
  expect(component).toContain('withSize(CardsWithSize)');
  const cardsWrapper = component.find('withSize(CardsWithSize)');
  expect(cardsWrapper).toHaveProps(defaultProps);
});
