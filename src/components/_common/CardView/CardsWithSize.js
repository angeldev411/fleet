import PropTypes from 'prop-types';
import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { sizes, withSize } from 'reactive-container';

import CardLoading from 'components/_common/CardLoading';

import { shouldBuildLoadingGhosts } from 'utils/favorites';

const MAX_LOADING_CARDS_PER_SIZE = {
  [sizes.SM]: 4,
  [sizes.MD]: 6,
  [sizes.LG]: 8,
  [sizes.XL]: 10,
};

function renderDataCards(dataList, CardComp) {
  return dataList.map(datum => (
    <CardComp
      key={datum.get('id')}
      datum={datum}
      type="card"
    />
  ));
}

function renderGhostCards(dataList, favoritePagination, requesting, size) {
  if (!shouldBuildLoadingGhosts({ currentCount: dataList.size, favoritePagination, requesting })) {
    return null;
  }

  const MAX_LOADING_CARDS = MAX_LOADING_CARDS_PER_SIZE[size] || 2;
  const totalCount = favoritePagination.get('totalCount');
  const count = totalCount ?
    Math.min(MAX_LOADING_CARDS, totalCount - dataList.size) :
    MAX_LOADING_CARDS;
  return Array.from(
    { length: count },
    (_, i) => <CardLoading key={i} />,
  );
}

export function CardsWithSize({ dataList, favoritePagination, requesting, CardComp, size }) {
  return (
    <div>
      {renderDataCards(dataList, CardComp)}
      {renderGhostCards(dataList, favoritePagination, requesting, size)}
    </div>
  );
}

CardsWithSize.propTypes = {
  dataList: ImmutablePropTypes.listOf(
    ImmutablePropTypes.mapContains({
      id: PropTypes.string,
    }),
  ).isRequired,
  favoritePagination: ImmutablePropTypes.map.isRequired,
  requesting: PropTypes.bool.isRequired,
  size: PropTypes.string.isRequired,
  CardComp: PropTypes.func.isRequired,
};

export default withSize(CardsWithSize);
