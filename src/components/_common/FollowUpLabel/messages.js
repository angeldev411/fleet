import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  followUpPlaceholder: {
    id: 'components.FollowUpLabel.placeholder',
    defaultMessage: 'Follow Up Time',
  },
});

export default formattedMessages;
