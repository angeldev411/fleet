import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import FollowUpValue from '../FollowUpValue';

function shallowRender(props = {}) {
  return shallow(<FollowUpValue {...props} />);
}

test('renders a span', () => {
  const component = shallowRender();
  expect(component).toBeA('span');
});
