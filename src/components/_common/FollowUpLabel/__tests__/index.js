import React from 'react';
import moment from 'moment';

import { test, expect, shallow, mount, MountableTestComponent } from '__tests__/helpers/test-setup';

import messages from 'utils/messages';

import FollowUpLabel from '../index';

const followUpColor = 'green';
const followUpTime = moment().add(2, 'days').toISOString();
const defaultProps = {
  followUpColor,
  followUpTime,
};

function shallowRender(props = defaultProps) {
  return shallow(<FollowUpLabel {...props} />);
}

function mountComponent(props = defaultProps) {
  return mount(
    <MountableTestComponent>
      <FollowUpLabel {...props} />
    </MountableTestComponent>,
  );
}

test('renders the wrapper with the correct color prop', () => {
  const component = shallowRender();
  expect(component).toBeA('Wrapper');
  expect(component).toHaveProp('color', followUpColor);
});

test('renders FontAwesome icon with correct name', () => {
  const component = shallowRender();
  expect(component).toContain('FontAwesome');
  expect(component.find('FontAwesome')).toHaveProp('name', 'flag');
});

test('renders FollowUpValue component', () => {
  const component = shallowRender();
  expect(component).toContain('FollowUpValue');
});

test('renders FormattedRelative component if time is valid', () => {
  const component = mountComponent();
  expect(component).toContain('FormattedRelative');
});

test('renders placeholder if time is empty', () => {
  const component = mountComponent({
    ...defaultProps,
    followUpTime: '',
  });
  expect(component.find('FollowUpValue').render().text()).toInclude(
    messages.noValue.defaultMessage,
  );
});
