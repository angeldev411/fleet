import React from 'react';
import { theme } from 'decisiv-ui-utils';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import { withTestTheme } from 'utils/styles';

import Wrapper, { getColor } from '../Wrapper';

const defaultProps = {
  color: 'green',
};

const WrapperWithTheme = withTestTheme(Wrapper);

function shallowRender(props = defaultProps) {
  return shallow(<WrapperWithTheme {...props} />);
}

/* --------------------------------- getColor() ----------------------------------- */

test('getColor returns status.success if no color is supplied', () => {
  expect(getColor({ color: undefined, theme })).toEqual(theme.colors.status.success);
});

test('getColor() returns status.success if color is `green`', () => {
  expect(getColor({ color: 'green', theme })).toEqual(theme.colors.status.success);
});

test('getColor() returns status.warning if color is `yellow`', () => {
  expect(getColor({ color: 'yellow', theme })).toEqual(theme.colors.status.warning);
});

test('getColor() returns status.danger if color is `red`', () => {
  expect(getColor({ color: 'red', theme })).toEqual(theme.colors.status.danger);
});

/* --------------------------------- Wrapper -------------------------------------- */

test('Renders a <div> tag', () => {
  const wrapper = shallowRender();
  expect(wrapper).toBeA('div');
});
