import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import { FormattedRelative } from 'react-intl';
import moment from 'moment';

import { userTimeZone } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';

import Wrapper from './Wrapper';
import FollowUpValue from './FollowUpValue';

function FollowUpLabel({ followUpColor, followUpTime }) {
  const timeLabel = followUpTime &&
    <FormattedRelative value={moment.tz(followUpTime, userTimeZone())} />;

  return (
    <Wrapper color={followUpColor}>
      <FontAwesome name="flag" />
      <FollowUpValue>
        {getOutputText(timeLabel)}
      </FollowUpValue>
    </Wrapper>
  );
}

FollowUpLabel.propTypes = {
  followUpColor: PropTypes.string,
  followUpTime: PropTypes.string,
};

FollowUpLabel.defaultProps = {
  followUpColor: '',
  followUpTime: '',
};

export default FollowUpLabel;
