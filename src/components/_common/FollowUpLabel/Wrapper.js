import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

export function getColor({ color, theme }) {
  switch (color) {
    case 'yellow':
      return theme.colors.status.warning;
    case 'red':
      return theme.colors.status.danger;
    case 'green':
    default:
      return theme.colors.status.success;
  }
}

/* istanbul ignore next */
const styles = props => `
  align-items: center;
  color: ${getColor(props)};
  display: flex;
  font-size: ${px2rem(16)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
      success: PropTypes.string.isRequired,
      warning: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  color: PropTypes.string,
};

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
  { propTypes, themePropTypes },
);
