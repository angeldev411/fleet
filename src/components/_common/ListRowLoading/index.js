import React from 'react';
import PropTypes from 'prop-types';

import GhostIndicator from 'components/_common/GhostIndicator';

function ghostColumns({ columnSpans }) {
  const colCount = columnSpans.length;

  return columnSpans.map((columnSpan, index) => {
    const modifiers = [];
    if (index < (colCount - 1)) modifiers.push('rightGap');
    return (
      // Since loading indicator td's are relatively static:
      // eslint-disable-next-line react/no-array-index-key
      <td key={index} colSpan={columnSpan}>
        <GhostIndicator modifiers={modifiers} />
      </td>
    );
  });
}

function ListRowLoading({ columnSpans }) {
  return (
    <tr className="list-row-loading">
      {ghostColumns({ columnSpans })}
    </tr>
  );
}

ListRowLoading.propTypes = {
  columnSpans: PropTypes.arrayOf(PropTypes.number).isRequired,
};

export default ListRowLoading;
