import React from 'react';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import ListRowLoading from '../index';

const testColumnSpans = [1, 2, 3, 4, 5];
const testProps = { columnSpans: testColumnSpans };

function shallowRender(props = testProps) {
  return shallow(<ListRowLoading {...props} />);
}

test('Renders a tr with correct class name', () => {
  const component = shallowRender();
  expect(component).toBeA('tr.list-row-loading');
});

test('Renders `columnSpans` number of GhostIndicator components', () => {
  const component = shallowRender();
  expect(component.find('GhostIndicator').length).toEqual(testColumnSpans.length);
});

test('ListRowLoading renders the intermediate ghost indicators with modifier rightGap', () => {
  const component = shallowRender();
  for (let i = 1; i < testColumnSpans.length - 1; i += 1) {
    const ghostCol = component.find('GhostIndicator').at(i);
    const modifiers = ghostCol.props().modifiers;
    expect(modifiers).toInclude('rightGap');
  }
});

test('ListRowLoading renders the last ghost indicator without modifier rightGap', () => {
  const component = shallowRender();
  const lastGhost = component.find('GhostIndicator').last();
  const modifiers = lastGhost.props().modifiers;
  expect(modifiers).toNotInclude('rightGap');
});
