import React from 'react';
import { List, Map } from 'immutable';

import {
  test,
  expect,
  shallow,
  mount,
  MountableTestComponent,
  createSpy,
} from '__tests__/helpers/test-setup';

import paginateFavorites, { showLoadMoreButton, shouldRequestNext } from '../index';
import messages from '../messages';

/* ------------------------------ showLoadMoreButton ------------------------------------- */

test('showLoadMoreButton returns true if latest page is >= 3', () => {
  const testData = Map({ totalPages: 12 });
  expect(showLoadMoreButton(testData.set('latestPage', 2))).toBe(false);
  expect(showLoadMoreButton(testData.set('latestPage', 3))).toBe(true);
  expect(showLoadMoreButton(testData.set('latestPage', 4))).toBe(true);
});

test('showLoadMoreButton returns false if latestPage is the same as totalPages', () => {
  const testData = Map({ latestPage: 5, totalPages: 5 });
  expect(showLoadMoreButton(testData)).toBe(false);
});

/* ------------------------------ showLoadMoreButton ------------------------------------- */

const document = {
  documentElement: { scrollTop: 0 },
  body: { scrollHeight: 1000, scrollTop: 0 },
};

test('shouldRequestNext returns true if latest page is one', () => {
  const componentProps = {
    favoritePagination: Map({
      latestPage: 1,
      totalPages: 12,
      totalCount: '10',
    }),
    requestInProgress: false,
  };
  expect(shouldRequestNext(componentProps, document)).toBe(true);
});

test(
  'shouldRequestNext returns true if latest page is two and user is viewing second half of page',
  () => {
    const componentProps = {
      favoritePagination: Map({
        latestPage: 2,
        totalPages: 12,
      }),
      requestInProgress: false,
    };
    const documentElement = { scrollTop: 504 };
    expect(shouldRequestNext(componentProps, { ...document, documentElement })).toBe(true);
  },
);

test('shouldRequestNext returns false if request is in progress', () => {
  const componentProps = {
    favoritePagination: Map({
      latestPage: 1,
      totalPages: 12,
    }),
    requestInProgress: true,
  };
  expect(shouldRequestNext(componentProps, document)).toBe(false);
});

test(
  'shouldRequestNext returns false if total pages is less then or equal to latest page',
  () => {
    const componentProps = {
      favoritePagination: Map({
        latestPage: 1,
        totalPages: 1,
      }),
      requestInProgress: false,
    };
    expect(shouldRequestNext(componentProps, document)).toBe(false);
  },
);

test(
  'shouldRequestNext returns false if latest page is 2 but user is not viewing second half',
  () => {
    const componentProps = {
      favoritePagination: Map({
        latestPage: 2,
        totalPages: 12,
      }),
      requestInProgress: false,
    };
    const documentElement = { scrollTop: 496 };
    expect(shouldRequestNext(componentProps, { ...document, documentElement })).toBe(false);
  },
);

test('shouldRequestNext returns false if latest page is 3 or higher', () => {
  const componentProps = {
    favoritePagination: Map({
      latestPage: 3,
      totalPages: 12,
    }),
    requestInProgress: false,
  };
  const documentElement = { scrollTop: 504 };
  expect(shouldRequestNext(componentProps, { ...document, documentElement })).toBe(false);
});

/* ------------------------------ paginateFavorites ------------------------------------- */

const TestView = () => <div style={{ minHeight: '1000px' }} />;
const requestNextSpy = createSpy();
const defaultComponentProps = {
  componentProps: {
    cases: List(),
  },
  favoritePagination: Map({
    latestPage: 1,
    totalPages: 12,
    totalCount: '10',
  }),
  noFavoritesMessage: {
    id: 'favorites.uptimeCases',
    defaultMessage: 'Uptime Cases',
  },
  requestInProgress: false,
};

function buildWrappedComponent(requestNext = requestNextSpy, View = TestView) {
  return paginateFavorites({ requestNext })(View);
}

function fullRenderHOC(WrappedComponent, componentProps = defaultComponentProps) {
  return mount(
    <MountableTestComponent authorized>
      <WrappedComponent {...componentProps} />
    </MountableTestComponent>,
  );
}

function shallowRenderHOC(WrappedComponent, componentProps = defaultComponentProps) {
  return shallow(<WrappedComponent {...componentProps} />);
}

test('Wrapped component contains the favorite view with the component props when favorites are NOT empty', () => {
  const WrappedComponent = buildWrappedComponent();
  const component = shallowRenderHOC(WrappedComponent);
  expect(component).toContain(TestView);
  const view = component.find(TestView);
  expect(view).toHaveProps(defaultComponentProps.componentProps);
});

test(
  'with correct props, wrapped component contains a RectangleButton with the correct message when favorites are NOT empty',
  () => {
    document.body.scrollTop = 600;

    const WrappedComponent = buildWrappedComponent();
    const component = shallowRenderHOC(
      WrappedComponent,
      {
        ...defaultComponentProps,
        favoritePagination: Map({ latestPage: 4, totalPages: 12, totalCount: '10' }),
      },
    );

    expect(component).toContain('RectangleButton');
    const button = component.find('RectangleButton');
    expect(button).toContain('FormattedMessage');
    expect(button.find('FormattedMessage')).toHaveProps({ ...messages.loadMore });
  },
);

test('clicking the load more button calls requestNext with the favoritePagination when favorites are NOT empty', () => {
  document.body.scrollTop = 600;

  const testFavoritePagination = Map({ latestPage: 4, totalPages: 12, totalCount: '10' });
  const WrappedComponent = buildWrappedComponent();
  const component = fullRenderHOC(
    WrappedComponent,
    { ...defaultComponentProps, favoritePagination: testFavoritePagination },
  );
  const button = component.find('RectangleButton');

  button.simulate('click');
  expect(requestNextSpy).toHaveBeenCalledWith(testFavoritePagination);
});

test('on initial scroll, requestNext is called with the favoritePagination when favorites are NOT empty', () => {
  const WrappedComponent = buildWrappedComponent();
  fullRenderHOC(WrappedComponent);

  window.dispatchEvent(new window.UIEvent('scroll'));

  expect(requestNextSpy).toHaveBeenCalledWith(defaultComponentProps.favoritePagination);
});

test('after componentWillUnmount, requestNext is not called', () => {
  const requestNext = createSpy();
  const component = fullRenderHOC(buildWrappedComponent(requestNext));
  component.unmount();

  window.dispatchEvent(new window.UIEvent('scroll'));

  expect(requestNext).toNotHaveBeenCalled();
});

test('renders NoFavoritesPanel with correct props when favorites are empty.', () => {
  const WrappedComponent = buildWrappedComponent();
  const component = shallowRenderHOC(
    WrappedComponent,
    {
      ...defaultComponentProps,
      favoritePagination: Map({ latestPage: 4, totalPages: 12, totalCount: '0' }),
    },
  );

  expect(component).toContain('NoFavoritesPanel');
  expect(component.find('NoFavoritesPanel')).toHaveProp(
    'filter',
    defaultComponentProps.noFavoritesMessage,
  );
});

test('renders wrapped component with correct props even if totalCount is 0 when loading.', () => {
  const WrappedComponent = buildWrappedComponent();
  const component = shallowRenderHOC(
    WrappedComponent,
    {
      ...defaultComponentProps,
      favoritePagination: Map({ latestPage: 4, totalPages: 12, totalCount: '0' }),
      requestInProgress: true,
    },
  );

  expect(component).toContain(TestView);
  const view = component.find(TestView);
  expect(view).toHaveProps(defaultComponentProps.componentProps);
});
