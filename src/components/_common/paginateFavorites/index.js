import { compact, throttle } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';

import NoFavoritesPanel from 'components/NoFavoritesPanel';

import RectangleButton, { RectangleButtonText } from 'elements/RectangleButton';

import { getScrollTop } from 'utils/document';

import ButtonContainer from './ButtonContainer';
import messages from './messages';

/**
 * Evaluates pagination data to determine if the "Load More" button should be displayed.
 * @param  {Map} favoritePagination An Immutable Map with pagination data.
 * @return {Boolean}
 */
export function showLoadMoreButton(favoritePagination) {
  const latestPage = parseInt(favoritePagination.get('latestPage'), 10);
  const totalPages = parseInt(favoritePagination.get('totalPages'), 10);
  return latestPage >= 3 && totalPages > latestPage;
}

/**
 * Evaluates pagination and scroll information from the document body to determine if
 * a scroll event should make the next request for the favorites
 * @param  {Object} componentProps The props of the calling component. Required keys:
 * favoritePagination, requestInProgress
 * @param  {Object} documentBody The data held at document.body.
 * @return {Boolean}
 */
export function shouldRequestNext(
  { favoritePagination, requestInProgress },
  document,
) {
  const latestPage = parseInt(favoritePagination.get('latestPage'), 10);
  const totalPages = parseInt(favoritePagination.get('totalPages'), 10);
  const viewingSecondHalf = getScrollTop(document) > document.body.scrollHeight / 2;

  return (
    (!requestInProgress && totalPages > latestPage) &&
    (latestPage === 1 || (latestPage === 2 && viewingSecondHalf))
  );
}

/**
 * An HOC that wraps a favorite view component and provides pagination per the specs outlined here:
 * https://decisiv.atlassian.net/browse/UI-192
 * @param  {Object} paginationParams Contains pagination methods. Required keys:
 * requestNext,
 * @param  {Component} FavoritesView A react component that renders the favorites.
 * @return {Component} The favorites view with pagination helpers.
 */
const paginateFavorites = ({ requestNext }) => FavoritesView =>
  class FavoritesPaginator extends Component {
    static propTypes = {
      // This component doesn't care what is in componentProps, only that it is an object.
      // eslint-disable-next-line react/forbid-prop-types
      componentProps: PropTypes.object.isRequired,

      DEBOUNCE_TIME_MS: PropTypes.number.isRequired,

      favoritePagination: ImmutablePropTypes.map.isRequired,
      noFavoritesMessage: PropTypes.shape(messageDescriptorPropTypes).isRequired,

      // requesting is passed through to shouldRequestNext and should not set a lint error
      requestInProgress: PropTypes.bool.isRequired,

      requestNext: PropTypes.func.isRequired,
    };

    static defaultProps = {
      DEBOUNCE_TIME_MS: 250,
      requestNext,
      latestFavorite: {},
      noFavoritesMessage: {},
    };

    componentWillMount() {
      this.scrollEventListener = throttle(this.scrollEventHandler, this.props.DEBOUNCE_TIME_MS);
    }

    componentDidMount() {
      window.addEventListener('scroll', this.scrollEventListener);
    }

    componentWillUnmount() {
      window.removeEventListener('scroll', this.scrollEventListener);
    }

    scrollEventHandler = () => {
      if (shouldRequestNext(this.props, document)) {
        this.props.requestNext(this.props.favoritePagination);
      }
    }

    render() {
      const { noFavoritesMessage, requestInProgress } = this.props;
      const disabled = requestInProgress && 'disabled';
      const totalCount = this.props.favoritePagination.get('totalCount');
      const isEmpty = !requestInProgress && totalCount === '0';

      if (isEmpty) {
        return (
          <NoFavoritesPanel filter={noFavoritesMessage} />
        );
      }

      return (
        <div>
          <FavoritesView {...this.props.componentProps} />
          {showLoadMoreButton(this.props.favoritePagination) &&
            <ButtonContainer>
              <RectangleButton
                disabled={disabled}
                modifiers={compact([
                  disabled,
                  'fullWidth',
                  'hoverBrandBright',
                  'hoverUnderline',
                  'offWhite',
                  'small',
                ])}
                onClick={() => { this.props.requestNext(this.props.favoritePagination); }}
              >
                <RectangleButtonText modifiers={['uppercase']}>
                  <FormattedMessage {...messages.loadMore} />
                </RectangleButtonText>
              </RectangleButton>
            </ButtonContainer>
          }
        </div>
      );
    }
  };

export default paginateFavorites;
