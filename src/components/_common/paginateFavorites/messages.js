import { defineMessages } from 'react-intl';

const messages = defineMessages({
  loadMore: {
    id: 'common.FavoritesPaginator.loadMore',
    defaultMessage: 'Load More',
  },
});

export default messages;
