import styled from 'styled-components';

/* istanbul ignore next */
const ButtonContainer = styled.div`
  padding: 0.5rem;
`;

export default ButtonContainer;
