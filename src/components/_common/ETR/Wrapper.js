import moment from 'moment';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

export function getColor({ etr, theme }) {
  if (!etr) return '';

  const etrTime = moment(etr);
  const nowTime = moment(new Date());
  const secondsInDay = 86400;
  const diffSeconds = etrTime.diff(nowTime, 'seconds');

  switch (true) {
    case diffSeconds <= 0:
      return `color: ${theme.colors.status.danger};`;
    case diffSeconds > 0 && diffSeconds <= secondsInDay:
      return `color: ${theme.colors.status.warning};`;
    case diffSeconds > secondsInDay:
    default:
      return `color: ${theme.colors.status.success};`;
  }
}

/* istanbul ignore next */
const styles = props => `
  ${getColor(props)}
  display: inline-block;
  font-weight: 500;
  max-width: ${props.maxWidth};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
      success: PropTypes.string.isRequired,
      warning: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  etr: PropTypes.string,
  maxWidth: PropTypes.string,
};

const defaultProps = {
  etr: '',
  maxWidth: '0',
};

export default buildStyledComponent(
  'Wrapper',
  styled.span,
  styles,
  { defaultProps, propTypes, themePropTypes },
);
