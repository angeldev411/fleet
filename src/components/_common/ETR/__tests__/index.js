import React from 'react';
import { fromJS } from 'immutable';
import moment from 'moment';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import { formatDate } from 'utils/timeUtils';

import ETR from '../index';

const etr = moment().toISOString();

const defaultCaseInfo = fromJS({
  etr,
});

const defaultProps = {
  caseInfo: defaultCaseInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<ETR {...props} />);
}

test('Renders the Wrapper with correct time prop', () => {
  const component = shallowRender();
  expect(component).toBeA('Wrapper');
  expect(component).toHaveProp('etr', defaultCaseInfo.get('etr'));
});

test('Renders the formatted time text', () => {
  const component = shallowRender();
  expect(component.render().text()).toEqual(formatDate(etr));
});
