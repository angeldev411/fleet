import moment from 'moment';
import { theme } from 'decisiv-ui-utils';

import { test, expect } from '__tests__/helpers/test-setup';

import { getColor } from '../Wrapper';

/* ------------------------ getColor() --------------------------- */

test('getColor() returns empty string if no ETR is provided', () => {
  expect(getColor({})).toEqual('');
});

test('getColor() returns status.danger if the ETR time is in the past', () => {
  const etr = moment().subtract(1, 'hours').toISOString();
  expect(getColor({ etr, theme })).toContain(theme.colors.status.danger);
});

test('getColor() returns status.warning if the ETR time is within the next 24 hours', () => {
  const firstEtr = moment().add(1, 'minute').toISOString();
  expect(getColor({ etr: firstEtr, theme })).toContain(theme.colors.status.warning);

  const secondEtr = moment().add(23, 'hours').toISOString();
  expect(getColor({ etr: secondEtr, theme })).toContain(theme.colors.status.warning);
});

test('getColor() returns status.success if the ETR time is 1 day or more in the future', () => {
  const etr = moment().add(25, 'hours').toISOString();
  expect(getColor({ etr, theme })).toContain(theme.colors.status.success);
});
