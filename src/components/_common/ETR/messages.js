import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  etr: {
    id: 'components._common.ETR.undefined',
    defaultMessage: 'ETR',
  },
});

export default formattedMessages;
