import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';

import { formatDate } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';

import Wrapper from './Wrapper';

function ETR({ caseInfo, maxWidth }) {
  const etr = caseInfo.get('etr');
  const formattedTime = etr ? formatDate(etr) : '';

  return (
    <Wrapper etr={etr} maxWidth={maxWidth}>
      {getOutputText(formattedTime)}
    </Wrapper>
  );
}

ETR.defaultProps = {
  maxWidth: 'none',
};

ETR.propTypes = {
  caseInfo: ImmutablePropTypes.contains({
    etr: PropTypes.string,
  }).isRequired,
  maxWidth: PropTypes.string,
};

export default ETR;
