import React from 'react';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import CardLoading from '../index';

function shallowRender() {
  return shallow(<CardLoading />);
}

test('Renders a Card with `loading` modifier', () => {
  const component = shallowRender();
  expect(component).toContain('Card');
  const cardProps = component.find('Card').props();
  expect(cardProps.modifiers).toInclude('loading');
});

test('Renders four GhostIndicator components', () => {
  const component = shallowRender();
  expect(component.find('GhostIndicator').length).toEqual(4);
});

test('First GhostIndicator has `bottomGap` modifier', () => {
  const component = shallowRender();
  expect(component.find('GhostIndicator').first().props().modifiers).toInclude('bottomGap');
});
