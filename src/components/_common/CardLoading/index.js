import React from 'react';
import { sizes } from 'reactive-container';

import Card, { CardWrapper } from 'elements/Card';
import GhostIndicator from 'components/_common/GhostIndicator';

function CardLoading() {
  return (
    <CardWrapper
      responsiveModifiers={{
        [sizes.XS]: ['1_per_row'],
        [sizes.SM]: ['2_per_row'],
        [sizes.MD]: ['3_per_row'],
        [sizes.LG]: ['4_per_row'],
        [sizes.XL]: ['5_per_row'],
      }}
    >
      <Card modifiers={['loading']}>
        <GhostIndicator modifiers={['bottomGap']} />
        <GhostIndicator />
        <GhostIndicator />
        <GhostIndicator />
      </Card>
    </CardWrapper>
  );
}

export default CardLoading;
