import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import GhostAnimation from './GhostAnimation';

const modifierConfig = {
  bottomGap: () => ({
    styles: `margin-bottom: ${px2rem(30)};`,
  }),

  dark: ({ theme }) => ({
    styles: `
      background: linear-gradient(90deg, ${theme.colors.base.chrome200} 0%, ${theme.colors.base.chrome300} 16.42%, ${theme.colors.base.chrome200} 100%);
    `,
  }),

  expanded: () => ({
    styles: `
      height: ${px2rem(23)};
      margin: ${px2rem(12)} ${px2rem(5)};
    `,
  }),

  rightGap: () => ({
    styles: `margin-right: ${px2rem(13)};`,
  }),

  smallWidth: () => ({
    styles: `
      flex: none;
      width: ${px2rem(20)};
    `,
  }),
  wide: () => ({
    styles: `
      width: 80%;
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  animation: ${GhostAnimation} 2s ease infinite;
  background-size: 600% 600% !important;
  background: linear-gradient(90deg, ${props.theme.colors.base.chrome100} 0%, ${props.theme.colors.base.chrome200} 24.85%, ${props.theme.colors.base.chrome100} 100%);
  border-radius: ${px2rem(2)};
  height: ${px2rem(20)};
  margin: ${px2rem(7)} ${px2rem(2)};
  max-width: 100%;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'GhostIndicator',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
