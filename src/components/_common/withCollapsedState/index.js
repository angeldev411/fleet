import { omit } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { withTheme } from 'styled-components';

import WindowQuery from 'components/WindowQuery';

const withCollapsedState = (WrappedComponent) => {
  class AddCollapsedStateToProps extends Component {
    static propTypes = {
      theme: PropTypes.shape({
        dimensions: PropTypes.shape({
          autoCollapseLeftNavPx: PropTypes.number.isRequired,
        }).isRequired,
      }).isRequired,
    };

    state = {
      collapsed: true,
    };

    /**
     * On initial mount, update collapsed state based on window size.
     * @return {undefined} This method is expected to trigger a state mutation, not return a value.
     */
    componentDidMount() {
      this.updateCollapsedState(window.innerWidth);
    }

    /**
     * event handler for resizing the window
     * @param  {number} windowWidth  The width of the window after resizing.
     * @return {undefined} This method is expected to mutate state, not return a value.
     */
    onResizeWindow = ({ windowWidth }) => {
      this.updateCollapsedState(windowWidth);
    }

    /**
     * Sets a new value for this.state.collapsed based on the current window width
     * @param  {number} windowWidth  The width of the window to be used in determining the collapsed
     * state
     * @return {undefined} This method is expected to mutate state, not return a value.
     */
    updateCollapsedState = (windowWidth) => {
      const {
        theme: {
          dimensions: {
            autoCollapseLeftNavPx,
          },
        },
      } = this.props;

      this.setState({ collapsed: windowWidth < autoCollapseLeftNavPx });
    }

    render() {
      const componentProps = omit(this.props, 'theme');

      return (
        <WindowQuery onResize={this.onResizeWindow}>
          <WrappedComponent collapsed={this.state.collapsed} {...componentProps} />
        </WindowQuery>
      );
    }
  }

  return withTheme(AddCollapsedStateToProps);
};

export default withCollapsedState;
