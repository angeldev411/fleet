import React from 'react';
import { ThemeProvider } from 'styled-components';
import { theme as defaultTheme } from 'decisiv-ui-utils';

import {
  test,
  expect,
  mount,
} from '__tests__/helpers/test-setup';

import withCollapsedState from '../index';

function fullRenderWithHOC(Component) {
  const ComponentWithHOC = withCollapsedState(Component);
  // a full mount with the ThemeProvider is required as there is no means to pass through
  // a test theme to the component returned within the HOC.
  return mount(
    <ThemeProvider theme={defaultTheme}>
      <ComponentWithHOC />
    </ThemeProvider>,
  );
}

const autoCollapseLeftNavPx = defaultTheme.dimensions.autoCollapseLeftNavPx;

test('collapse when windowWidth is less than autoCollapseLeftNavPx', () => {
  window.innerWidth = autoCollapseLeftNavPx - 1;
  const WrappedComponent = () => <div />;
  const component = fullRenderWithHOC(WrappedComponent);
  const child = component.find(WrappedComponent);
  expect(child).toHaveProp('collapsed', true);
});

test('expand when windowWidth is less than autoCollapseLeftNavPx', () => {
  window.innerWidth = autoCollapseLeftNavPx + 1;
  const WrappedComponent = () => <div />;
  const component = fullRenderWithHOC(WrappedComponent);
  const child = component.find(WrappedComponent);
  expect(child).toHaveProp('collapsed', false);
});
