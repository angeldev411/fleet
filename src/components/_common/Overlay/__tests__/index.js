import { test, expect, shallow } from '__tests__/helpers/test-setup';
import React from 'react';
import Overlay from '../index';

function shallowRender() {
  return shallow(
    <Overlay />,
  );
}

test('Renders a Background', () => {
  expect(shallowRender()).toBeA('Background');
});

test('Set overflow of body to "hidden" when it is mounted', () => {
  document.body.style = { overflow: 'whatever' };
  shallowRender();
  expect(document.body.style.overflow).toEqual('hidden');
});

test('Set overflow of body to "auto" when it is unmounted', () => {
  document.body.style = { overflow: 'whatever' };
  const overlay = shallowRender();
  document.body.style = { overflow: 'whatever' };
  overlay.instance().componentWillUnmount();
  expect(document.body.style.overflow).toEqual('auto');
});
