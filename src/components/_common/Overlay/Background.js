import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.overlay};
  height: 100%;
  position: absolute;
  width: 100%;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      overlay: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Background',
  styled.section,
  styles,
  { themePropTypes },
);
