import React, { Component } from 'react';
import Background from './Background';

class Overlay extends Component {
  componentWillMount() {
    document.body.style.overflow = 'hidden';
  }

  componentWillUnmount() {
    document.body.style.overflow = 'auto';
  }

  render() {
    return <Background {...this.props} />;
  }
}

export default Overlay;
