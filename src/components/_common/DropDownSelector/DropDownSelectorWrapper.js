import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const DropDownSelectorWrapper = styled.div`
  display: flex;
  align-items: center;
  text-align: left;

  label {
    margin-right: ${px2rem(8)};
    display: ${props => (props.hideLabel ? 'none' : 'inherit')};
  }
`;

export default DropDownSelectorWrapper;
