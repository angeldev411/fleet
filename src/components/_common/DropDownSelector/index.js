import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import DropDown from 'elements/DropDown';

import DropDownSelectorWrapper from './DropDownSelectorWrapper';
import messages from './messages';

function DropDownSelector({
  label,
  onChange,
  options,
  placeholder,
  selected,
}) {
  return (
    <DropDownSelectorWrapper>
      <label htmlFor="options-selector">
        <FormattedMessage {...label} />
      </label>
      <DropDown
        name="options-selector"
        onChange={onChange}
        options={options}
        placeholder={placeholder}
        value={selected}
      />
    </DropDownSelectorWrapper>
  );
}

DropDownSelector.propTypes = {
  label: PropTypes.shape({
    id: PropTypes.string.isRequired,
    defaultMessage: PropTypes.string.isRequired,
  }),
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  })).isRequired,
  placeholder: PropTypes.string.isRequired,
  selected: PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.string,
  }),
  onChange: PropTypes.func.isRequired,
};

DropDownSelector.defaultProps = {
  label: messages.label,
  placeholder: 'Select...',
  selected: undefined,
};

export default DropDownSelector;
