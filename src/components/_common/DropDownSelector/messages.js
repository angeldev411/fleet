import { defineMessages } from 'react-intl';

const messages = defineMessages({
  label: {
    id: 'common.DropDownSelector.defaultLabel',
    defaultMessage: 'OPTIONS:',
  },
});

export default messages;
