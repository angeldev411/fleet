import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import DropDownSelector from '../index';

const label = {
  id: 'test.label',
  defaultMessage: 'label',
};

const onChange = createSpy();

const options = [
  { value: 'first', label: 'First' },
  { value: 'second', label: 'Second' },
];

const placeholder = 'placeholder';

const value = options[0];

const defaultProps = {
  label,
  onChange,
  options,
  placeholder,
  selected: value,
};

function shallowRender(props = defaultProps) {
  return shallow(<DropDownSelector {...props} />);
}

test('renders a DropDownSelectorWrapper', () => {
  expect(shallowRender()).toBeA('DropDownSelectorWrapper');
});

test('renders the label as a FormattedMessage', () => {
  const component = shallowRender();
  expect(component).toContain('FormattedMessage');
  expect(component.find('FormattedMessage')).toHaveProps(label);
});

test('renders a DropDown with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('DropDown');
  expect(component.find('DropDown')).toHaveProps({
    onChange,
    options,
    placeholder,
    value,
  });
});
