import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import DropDownSelectorWrapper from '../DropDownSelectorWrapper';

const defaultProps = {
  hideLabel: true,
};

function buildStyleWrapper(props = defaultProps) {
  return shallow(<DropDownSelectorWrapper {...props} />);
}

test('Renders style wrapper', () => {
  expect(buildStyleWrapper()).toBeA('div');
});
