import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  isHovered: ({ theme }) => ({
    styles: `
      border: 1px solid;
      border-color: ${theme.colors.base.chrome300};
      padding-left: ${px2rem(5)};
    `,
  }),
  isFocused: ({ theme }) => ({
    styles: `
      border: 1px solid;
      border-color: ${theme.colors.base.chrome300};
      padding-left: ${px2rem(5)};
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  border: 0px;
  border-radius: 2px;
  height: ${px2rem(26)};
  padding-right: ${px2rem(5)};
  padding-left: ${px2rem(0)};
  width: ${px2rem(150)};
  &:hover {
    cursor: text;
  }
  &:focus {
    border-color: ${props.theme.colors.base.chrome300};
  }
  &::placeholder {
    font-weight: 200;
    font-style: italic;
    font-color: ${props.theme.colors.base.chrome500};
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
      chrome500: PropTypes.string.isRequired,
    }).isRequired,
    brand: PropTypes.shape({
      secondary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Input',
  styled.input,
  styles,
  { modifierConfig, themePropTypes },
);
