import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import { InlineEditableField } from '../index';

const onUpdate = createSpy();
const getInitialValue = createSpy();

const defaultProps = {
  value: 'text',
  onUpdate,
  getInitialValue,
};

function shallowRender(props = defaultProps) {
  return shallow(<InlineEditableField {...props} />);
}

test('InlineEditableField renders a `Input` with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('Input');
  expect(component.find('Input')).toHaveProp('value', defaultProps.value);
});

test('InlineEditableField renders `Button` for loading', () => {
  const component = shallowRender();
  component.setState({
    isSubmitting: true,
    isHovered: false,
    isFocused: true,
  });
  expect(component).toContain('Button');
  expect(component.find('Button')).toHaveProp('type', 'loading');
});

test('InlineEditableField renders `Button` for editing', () => {
  const component = shallowRender();
  component.setState({
    isSubmitting: false,
    isHovered: true,
    isFocused: false,
  });
  expect(component).toContain('Button');
  expect(component.find('Button')).toHaveProp('type', 'edit');
});

test('InlineEditableField renders `Button` for submitting', () => {
  const component = shallowRender();
  component.setState({
    isSubmitting: false,
    isHovered: true,
    isFocused: true,
  });
  const cancelButton = component.find('Button').first();
  expect(cancelButton).toHaveProp('type', 'cancel');
  const submitButton = component.find('Button').last();
  expect(submitButton).toHaveProp('type', 'submit');
});

test('`onCancel` sets the component state', () => {
  const props = {
    ...defaultProps,
    onUpdate: createSpy(),
  };
  const component = shallowRender(props);
  component.instance().onCancel();
  expect(component).toHaveState({
    isFocused: false,
    isHovered: false,
  });
  expect(props.onUpdate).toHaveBeenCalledWith(false);
});

test('`submitValue` sets the component state', () => {
  const props = {
    ...defaultProps,
    onUpdate: createSpy(),
  };
  const component = shallowRender(props);
  component.instance().submitValue();
  expect(component).toHaveState({
    isFocused: false,
    isHovered: false,
  });
  expect(props.onUpdate).toHaveBeenCalledWith(true);
});

test('`onEnter` sets the component state', () => {
  const component = shallowRender();
  component.instance().onEnter();
  expect(component).toHaveState({
    isHovered: true,
  });
});

test('`onLeave` sets the component state', () => {
  const component = shallowRender();
  component.instance().onLeave();
  expect(component).toHaveState({
    isHovered: false,
  });
});

test('`onFocus` sets the component state', () => {
  const props = {
    ...defaultProps,
    getInitialValue: createSpy(),
  };

  const component = shallowRender(props);
  component.instance().onFocus();

  expect(component).toHaveState({
    isFocused: true,
  });
  expect(props.getInitialValue).toHaveBeenCalled();
});

test('`handleClickOutside` sets the component state', () => {
  const component = shallowRender();
  const submitValueSpy = createSpy();
  const instance = component.instance();
  instance.submitValue = submitValueSpy;
  instance.setState({ isFocused: true });
  instance.handleClickOutside();
  expect(component).toHaveState({ isFocused: false });
  expect(submitValueSpy).toHaveBeenCalled();
});

test('`handleClickOutside` does not call submitValue when isFocused is false', () => {
  const component = shallowRender();
  const submitValueSpy = createSpy();
  const instance = component.instance();
  instance.submitValue = submitValueSpy;
  instance.setState({ isFocused: false });
  instance.handleClickOutside();
  expect(submitValueSpy).toNotHaveBeenCalled();
});

test('sets the state whenever `value` props is changed', () => {
  const component = shallowRender();
  const instance = component.instance();

  const newProps = {
    ...defaultProps,
    value: 'text1',
  };

  instance.componentWillReceiveProps(newProps);
  expect(component).toHaveState({ isSubmitting: false });
});

test('does not change the state when `value` props is not changed', () => {
  const component = shallowRender();
  const instance = component.instance();
  instance.setState({ isSubmitting: true });

  instance.componentWillReceiveProps({ ...defaultProps });
  expect(component).toHaveState({ isSubmitting: true });
});

test('textInput.focus is called when edit button is clicked', () => {
  const component = shallowRender();
  const instance = component.instance();
  const focusSpy = createSpy();
  instance.textInput = {
    focus: focusSpy,
  };
  instance.setState({ isHovered: true });
  const editButton = component.find('Button[type="edit"]');
  editButton.simulate('click');
  expect(focusSpy).toHaveBeenCalled();
});

test('submitValue is called when submit button is clicked', () => {
  const component = shallowRender();
  const instance = component.instance();
  const submitValue = createSpy();
  instance.submitValue = submitValue;
  instance.setState({ isFocused: true });
  const editButton = component.find('Button[type="submit"]');
  editButton.simulate('click');
  expect(submitValue).toHaveBeenCalled();
});

test('follow the exact workflow when Enter key is pressed', () => {
  const component = shallowRender();
  const instance = component.instance();
  const preventDefaultSpy = createSpy();
  const blurSpy = createSpy();
  const submitValueSpy = createSpy();
  instance.submitValue = submitValueSpy;
  instance.onKeyPress({
    key: 'Enter',
    preventDefault: preventDefaultSpy,
    target: {
      blur: blurSpy,
    },
  });
  expect(preventDefaultSpy).toHaveBeenCalled();
  expect(blurSpy).toHaveBeenCalled();
  expect(submitValueSpy).toHaveBeenCalled();
});

test('nothing is called when another key is pressed', () => {
  const component = shallowRender();
  const instance = component.instance();
  const preventDefaultSpy = createSpy();
  const blurSpy = createSpy();
  const submitValueSpy = createSpy();
  instance.submitValue = submitValueSpy;
  instance.onKeyPress({
    key: 'Not-Enter',
    preventDefault: preventDefaultSpy,
    target: {
      blur: blurSpy,
    },
  });
  expect(preventDefaultSpy).toNotHaveBeenCalled();
  expect(blurSpy).toNotHaveBeenCalled();
  expect(submitValueSpy).toNotHaveBeenCalled();
});
