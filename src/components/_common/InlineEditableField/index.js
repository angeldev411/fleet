import React, { Component } from 'react';
import PropTypes from 'prop-types';
import enhanceWithClickOutside from 'react-click-outside';
import { compact } from 'lodash';

import Wrapper from './Wrapper';
import Input from './Input';
import Button from './Button';
import ButtonWrapper from './ButtonWrapper';

export class InlineEditableField extends Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    onUpdate: PropTypes.func.isRequired,
    getInitialValue: PropTypes.func.isRequired,
  };

  state = {
    isHovered: false,
    isFocused: false,
    isSubmitting: false,
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.setState({
        isSubmitting: false,
      });
    }
  }

  onEnter = () => {
    this.setState({ isHovered: true });
  }

  onEdit = () => {
    this.textInput.focus();
  }

  onLeave = () => {
    this.setState({ isHovered: false });
  }

  onFocus = () => {
    this.setState({ isFocused: true });
    this.props.getInitialValue(this.props.value);
  }

  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      e.target.blur();

      this.submitValue();
    }
  }

  onCancel = () => {
    this.setState({
      isFocused: false,
      isHovered: false,
    });

    this.props.onUpdate(false);
  }

  onSubmit = () => {
    this.submitValue();
  }

  handleClickOutside = () => {
    if (this.state.isFocused) {
      this.setState({ isFocused: false });
      this.submitValue();
    }
  }

  submitValue = () => {
    this.setState({
      isFocused: false,
      isHovered: false,
    });

    this.props.onUpdate(true);
  }

  render() {
    const {
      isHovered,
      isFocused,
      isSubmitting,
    } = this.state;

    return (
      <Wrapper
        onMouseEnter={this.onEnter}
        onMouseLeave={this.onLeave}
      >
        <Input
          {...this.props}
          modifiers={compact([isHovered && 'isHovered', isFocused && 'isFocused'])}
          placeholder="Edit"
          value={this.props.value}
          innerRef={(input) => { this.textInput = input; }}
          disabled={isSubmitting}
          onFocus={this.onFocus}
          onKeyPress={this.onKeyPress}
        />
        {isSubmitting &&
          <Button type="loading" onClick={this.onCancel} />
        }
        {!isSubmitting && isHovered && !isFocused &&
          <Button type="edit" onClick={this.onEdit} />}
        {!isSubmitting && isFocused &&
          <ButtonWrapper>
            <Button type="cancel" onClick={this.onCancel} />
            <Button type="submit" onClick={this.onSubmit} />
          </ButtonWrapper>
        }
      </Wrapper>
    );
  }
}

export default enhanceWithClickOutside(InlineEditableField);
