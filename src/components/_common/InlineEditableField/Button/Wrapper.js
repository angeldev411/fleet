import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  cancel: ({ theme }) => ({
    styles: `
      &:hover {
        background-color: ${theme.colors.status.danger};
        border: 1px solid ${theme.colors.status.danger};
        span {
          color: ${theme.colors.base.chrome100};
        }
      }
    `,
  }),
  edit: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.base.chrome100};
      color: ${theme.colors.brand.secondary};
      span {
        color: ${theme.colors.base.chrome500};
      }
    `,
  }),
  loading: ({ theme }) => ({
    styles: `
      .fa {
        animation: ${theme.animations.spin} 0.8s linear infinite;
      }
    `,
  }),
  submit: ({ theme }) => ({
    styles: `
      &:hover {
        background-color: ${theme.colors.status.success};
        border: 1px solid ${theme.colors.status.success};
        span {
          color: ${theme.colors.base.chrome100};
        }
      }
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  align-items: center;
  background-color: ${props.theme.colors.base.chrome100};
  border-radius: 2px;
  border: 1px solid ${props.theme.colors.base.chrome300};
  cursor: pointer;
  display: flex;
  justify-content: center;
  margin: 0;
  width: ${px2rem(31)};

  span {
    color: ${props.theme.colors.base.chrome500};
    font-size: ${px2rem(17)};
    margin: auto;
  }
`;

const themePropTypes = {
  animations: PropTypes.shape({
    spin: PropTypes.string.isRequired,
  }).isRequired,
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome300: PropTypes.string.isRequired,
      chrome500: PropTypes.string.isRequired,
    }).isRequired,
    brand: PropTypes.shape({
      secondary: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
      success: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Wrapper',
  styled.button,
  styles,
  { modifierConfig, themePropTypes },
);
