import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import OptionButton, { ICON_MAP } from '../index';

const onClick = createSpy();

const defaultProps = {
  type: 'edit',
  onClick,
};

function shallowRender(props = defaultProps) {
  return shallow(<OptionButton {...props} />);
}

test('OptionButton renders `FontAwesome` with the expected name', () => {
  const component = shallowRender();
  expect(component).toContain('FontAwesome');
  expect(component.find('FontAwesome')).toHaveProp('name', ICON_MAP[defaultProps.type]);
});
