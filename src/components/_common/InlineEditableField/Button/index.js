import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

import Wrapper from './Wrapper';

export const ICON_MAP = {
  edit: 'pencil',
  submit: 'check',
  cancel: 'close',
  loading: 'refresh',
};

function Button({ type, onClick }) {
  return (
    <Wrapper modifiers={[type]} onClick={onClick}>
      <FontAwesome name={ICON_MAP[type]} />
    </Wrapper>
  );
}

Button.propTypes = {
  type: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Button;
