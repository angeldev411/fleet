import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import Routes from 'setup/routes';

function Router() {
  return (
    <BrowserRouter >
      <Routes />
    </BrowserRouter>
  );
}

export default Router;
