// eslint-disable-next-line import/no-unresolved
import config from 'runtimeConfig.json';

const helmetAppConfig = {
  htmlAttributes: { lang: 'en' },
  defaultTitle: config.title,
  titleTemplate: `%s - ${config.title}`,
  meta: [
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1',
    },
  ],
  // Uncomment this if you want to watch Helmet's updates in the JS console:
  // onChangeClientState: newState => console.log('helmet update: ', newState),
};

export default helmetAppConfig;
