import appSagas from 'redux/app/sagas';
import uiSagas from 'redux/ui/sagas';
import userSagas from 'redux/user/sagas';
import casesSagas from 'redux/cases/sagas';
import assetsSagas from 'redux/assets/sagas';
import serviceRequestsSagas from 'redux/serviceRequests/sagas';

export default function* rootSaga() {
  yield [
    ...appSagas,
    ...uiSagas,
    ...userSagas,
    ...casesSagas,
    ...assetsSagas,
    ...serviceRequestsSagas,
  ];
}
