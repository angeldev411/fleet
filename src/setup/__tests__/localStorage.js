import { test, expect, spyOn } from '__tests__/helpers/test-setup';
import MemoryStorage from 'memorystorage';

import initLocalStorage from '../localStorage';

let oldLocalStorage = null;
let oldMemoryDB = null;

test.beforeEach(() => {
  oldLocalStorage = window.localStorage;
  oldMemoryDB = window.memoryDB;
});

test.afterEach(() => {
  window.localStorage = oldLocalStorage;
  window.memoryDB = oldMemoryDB;
  oldLocalStorage = null;
  oldMemoryDB = null;
});

test('initLocalStorage falls back to MemoryStorage if localStorage fails', () => {
  spyOn(localStorage, 'setItem').andReturn('nope');
  const result = initLocalStorage();
  expect(memoryDB).toBeA(MemoryStorage);
  expect(result).toEqual(memoryDB);
});

test('initLocalStorage uses localStorage is available', () => {
  const result = initLocalStorage();
  expect(memoryDB).toEqual(localStorage);
  expect(result).toEqual(memoryDB);
});
