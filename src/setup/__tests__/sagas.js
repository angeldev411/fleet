import { test, expect } from '__tests__/helpers/test-setup';

import appSagas from 'redux/app/sagas';
import uiSagas from 'redux/ui/sagas';
import casesSagas from 'redux/cases/sagas';
import userSagas from 'redux/user/sagas';
import assetsSagas from 'redux/assets/sagas';
import serviceRequestsSagas from 'redux/serviceRequests/sagas';

import sagas from '../sagas';

test('root saga is correctly constructed', () => {
  const generator = sagas();
  expect(generator.next().value).toEqual([
    ...appSagas,
    ...uiSagas,
    ...userSagas,
    ...casesSagas,
    ...assetsSagas,
    ...serviceRequestsSagas,
  ]);
});
