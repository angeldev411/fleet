import React from 'react';
import {
  test,
  expect,
  spyOn,
  mount,
  MountableTestComponent,
  mockDashboardApi,
} from '__tests__/helpers/test-setup';

import Routes from 'setup/routes';

import * as casesApi from 'redux/cases/api';

function router(initialEntries = ['/'], { authorized = false } = {}) {
  return mount(
    <MountableTestComponent authorized={authorized} initialEntries={initialEntries}>
      <Routes />
    </MountableTestComponent>,
  );
}

test.beforeEach(() => {
  mockDashboardApi();
  spyOn(casesApi, 'getCases').andReturn([]);
});

test('the router renders the login-page when unauthorized', () => {
  const wrapper = router();
  const cheerio = wrapper.render();
  expect(cheerio.find('#login-page')[0]).toExist();
});

test('the router renders the correct component for the route when authorized', () => {
  // Testing routes is tricky. All routes are children of the router, and will exist. We are
  // are only interested in testing which components are rendered though. Enzyme defers this
  // functionality to cheerio.
  const wrapper = router(undefined, { authorized: true });
  const cheerio = wrapper.render();
  expect(cheerio.find('NotFoundPage')).toExist();
});

test('the router does not render the incorrect component for the route', () => {
  const wrapper = router(undefined, { authorized: true });
  const cheerio = wrapper.render();
  expect(cheerio.find('#jokes-page')[0]).toNotExist();
});

// This test demonstrates how to change the URL being referenced for use in a test.
//
// test('the router renders the correct component for /jokes', () => {
//   const wrapper = router(['/jokes']);
//   const cheerio = wrapper.render();
//   expect(cheerio.find('#home-page')[0]).toNotExist();
//   expect(cheerio.find('#jokes-page')[0]).toExist();
// });
