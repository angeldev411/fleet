import { merge } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { ThemeProvider } from 'styled-components';
import { theme as defaultTheme } from 'decisiv-ui-utils';

import configureFonts from './configureFonts';
import configureGlobalStyles from './configureGlobalStyles';

function AppThemeProvider({ baseTheme, children, customTheme }) {
  const appTheme = merge(baseTheme, customTheme);

  configureFonts(appTheme);

  configureGlobalStyles(appTheme);

  return (
    <ThemeProvider theme={appTheme}>
      {children}
    </ThemeProvider>
  );
}

AppThemeProvider.propTypes = {
  baseTheme: PropTypes.shape({
    animations: PropTypes.object.isRequired,
    colors: PropTypes.shape({
      base: PropTypes.object.isRequired,
      brand: PropTypes.object.isRequired,
      status: PropTypes.object.isRequired,
    }).isRequired,
    dimensions: PropTypes.object.isRequired,
    globalStyles: PropTypes.shape({
      buildGlobalStyleString: PropTypes.func.isRequired,
    }).isRequired,
    transitions: PropTypes.object.isRequired,
  }).isRequired,
  children: PropTypes.node.isRequired,
  customTheme: PropTypes.shape({
    animations: PropTypes.object,
    colors: PropTypes.shape({
      base: PropTypes.object,
      brand: PropTypes.object,
      status: PropTypes.object,
    }),
    dimensions: PropTypes.object,
    globalStyles: PropTypes.shape({
      buildGlobalStyleString: PropTypes.func,
    }),
    transitions: PropTypes.object,
  }).isRequired,
};

AppThemeProvider.defaultProps = {
  baseTheme: defaultTheme,
  customTheme: {},
};

export default AppThemeProvider;
