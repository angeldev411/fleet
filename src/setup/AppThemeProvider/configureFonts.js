import FontFaceObserver from 'fontfaceobserver';

/* istanbul ignore next */
function configureFonts(appTheme) {
  if (process.env.NODE_ENV === 'test') { return null; }

  const primaryFonts = appTheme.fonts.primary.split(',');
  const fontObservers = primaryFonts.map(f => new FontFaceObserver(f, {}));

  function fontLoadSuccess() {
    console.log('All fonts have loaded'); // TODO: better logging
    document.body.classList.add('fontsLoaded');
  }

  function fontLoadFailure() {
    console.error('Fonts have failed to load'); // TODO: better logging
    document.body.classList.remove('fontsLoaded');
  }

  Promise.all(fontObservers.map(o => o.load())).then(fontLoadSuccess, fontLoadFailure);

  return true;
}

export default configureFonts;
