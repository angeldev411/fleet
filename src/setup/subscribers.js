import addRequestsSubscriber from 'redux/requests/subscriber';
import addAppStateSubscriber from 'redux/ui/subscriber';

/**
 * List of all the various Redux store subscribers to be registered.
 */
const storeSubscribers = [
  addRequestsSubscriber,
  addAppStateSubscriber,
];

/**
 * Register all the Redux store subscribers.
 * @param store The Redux store
 */
export default function setupSubscriptions(store) {
  storeSubscribers.forEach(subscribe => subscribe(store));
}
