import AddNoteButton from 'compositions/AddNoteButton';
import AssetOverview from 'compositions/AssetOverview';
import AssetSummaryWidget from 'compositions/AssetSummaryWidget';
import AssetsWidget from 'compositions/AssetsWidget';
import CancelServiceRequestButton from 'compositions/CancelServiceRequestButton';
import CaseOverview from 'compositions/CaseOverview';
import CaseStatusWidget from 'compositions/CaseStatusWidget';
import ComplaintWidget from 'compositions/ComplaintWidget';
import {
  AssetFaultDiagnosticsWidget,
  CaseFaultDiagnosticsWidget,
} from 'compositions/DiagnosticsWidget';
import EstimateInvoiceDecidedWidget from 'compositions/EstimateInvoiceDecidedWidget';
import EstimateInvoicePendingWidget from 'compositions/EstimateInvoicePendingWidget';
import JumpToButton from 'compositions/JumpToButton';
import LocationWidget from 'compositions/LocationWidget';
import NotesWidget from 'compositions/NotesWidget';
import OpenEventsWidget from 'compositions/OpenEventsWidget';
import OptionsButton from 'compositions/OptionsButton';
import QRCodeWidget from 'compositions/QRCodeWidget';
import RequestServiceButton from 'compositions/RequestServiceButton';
import {
  CaseServiceProviderWidget,
  ServiceRequestServiceProviderWidget,
} from 'compositions/ServiceProviderWidget';
import ServiceRequestOverview from 'compositions/ServiceRequestOverview';
import ServiceRequestWidget from 'compositions/ServiceRequestWidget';

import AssetDetailData from 'containers/AssetDetailData';
import CaseDetailData from 'containers/CaseDetailData';
import ServiceRequestDetailData from 'containers/ServiceRequestDetailData';

import {
  ADD_NOTE_BUTTON,
  ASSET_DETAIL_DATA,
  ASSET_FAULT_DIAGNOSTIC_WIDGET,
  ASSET_OVERVIEW,
  ASSET_SUMMARY_WIDGET,
  ASSETS_WIDGET,
  CANCEL_SERVICE_REQUEST_BUTTON,
  CASE_DETAIL_DATA,
  CASE_FAULT_DIAGNOSTIC_WIDGET,
  CASE_OVERVIEW,
  CASE_SERVICE_PROVIDER_WIDGET,
  CASE_STATUS_WIDGET,
  COMPLAINT_WIDGET,
  ESTIMATE_INVOICE_DECIDED_WIDGET,
  ESTIMATE_INVOICE_PENDING_WIDGET,
  JUMP_TO_BUTTON,
  LOCATION_WIDGET,
  NOTES_WIDGET,
  OPEN_EVENTS_WIDGET,
  OPTIONS_BUTTON,
  QR_CODE_WIDGET,
  REQUEST_SERVICE_BUTTON,
  SERVICE_REQUEST_DETAIL_DATA,
  SERVICE_REQUEST_OVERVIEW,
  SERVICE_REQUEST_SERVICE_PROVIDER_WIDGET,
  SERVICE_REQUEST_WIDGET,
} from './constants';

export default {
  [ADD_NOTE_BUTTON]: AddNoteButton,
  [ASSET_DETAIL_DATA]: AssetDetailData,
  [ASSET_FAULT_DIAGNOSTIC_WIDGET]: AssetFaultDiagnosticsWidget,
  [ASSET_OVERVIEW]: AssetOverview,
  [ASSET_SUMMARY_WIDGET]: AssetSummaryWidget,
  [ASSETS_WIDGET]: AssetsWidget,
  [CANCEL_SERVICE_REQUEST_BUTTON]: CancelServiceRequestButton,
  [CASE_DETAIL_DATA]: CaseDetailData,
  [CASE_FAULT_DIAGNOSTIC_WIDGET]: CaseFaultDiagnosticsWidget,
  [CASE_OVERVIEW]: CaseOverview,
  [CASE_SERVICE_PROVIDER_WIDGET]: CaseServiceProviderWidget,
  [CASE_STATUS_WIDGET]: CaseStatusWidget,
  [COMPLAINT_WIDGET]: ComplaintWidget,
  [ESTIMATE_INVOICE_DECIDED_WIDGET]: EstimateInvoiceDecidedWidget,
  [ESTIMATE_INVOICE_PENDING_WIDGET]: EstimateInvoicePendingWidget,
  [JUMP_TO_BUTTON]: JumpToButton,
  [LOCATION_WIDGET]: LocationWidget,
  [NOTES_WIDGET]: NotesWidget,
  [OPEN_EVENTS_WIDGET]: OpenEventsWidget,
  [OPTIONS_BUTTON]: OptionsButton,
  [QR_CODE_WIDGET]: QRCodeWidget,
  [REQUEST_SERVICE_BUTTON]: RequestServiceButton,
  [SERVICE_REQUEST_DETAIL_DATA]: ServiceRequestDetailData,
  [SERVICE_REQUEST_OVERVIEW]: ServiceRequestOverview,
  [SERVICE_REQUEST_SERVICE_PROVIDER_WIDGET]: ServiceRequestServiceProviderWidget,
  [SERVICE_REQUEST_WIDGET]: ServiceRequestWidget,
};
