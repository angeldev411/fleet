import PropTypes from 'prop-types';

import REGISTRY_KEYS from './constants';

const compositionConfig = PropTypes.arrayOf(PropTypes.oneOf(REGISTRY_KEYS));

const registeredCompositions = PropTypes.arrayOf(
  PropTypes.shape({
    name: PropTypes.string.isRequired,
    composition: PropTypes.func.isRequired,
  }),
).isRequired;

export default {
  compositionConfig,
  registeredCompositions,
};
