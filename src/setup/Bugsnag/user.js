import { userProfileSelector, usernameSelector } from 'redux/user/selectors';
import addBeforeNotify from './addBeforeNotify';

function beforeNotify(payload, getStore) {
  // Report current user to Bugsnag, if available
  const store = getStore();
  const currentState = store.getState();

  if (store && currentState) {
    const username = usernameSelector(currentState);
    const userInfo = userProfileSelector(currentState);

    if (userInfo) {
      // eslint-disable-next-line no-param-reassign
      payload.user = { username, ...userInfo.toJS() };
    }
  } else {
    console.log('Bugsnag user beforeNotify - no Redux store available yet');
  }
}

function setup(Bugsnag) {
  addBeforeNotify(Bugsnag, beforeNotify);
}

export default { setup };
