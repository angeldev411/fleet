import Bugsnag from 'bugsnag-js';

/**
 * Redux middleware that catches exceptions during the dispatching of actions and reports
 * them to Bugsnag.
 * @param store
 */
const bugsnagReporter = store => next => (action) => {
  try {
    return next(action);
  } catch (err) {
    const metadata = {
      action,
      state: store.getState(),
    };

    Bugsnag.notifyException(err, 'Exception in Redux store', metadata);
    throw err;
  }
};

export default bugsnagReporter;
