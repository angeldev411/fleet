import { includes } from 'lodash';

const callbacks = [];

let reduxStore;

function getStore() {
  return reduxStore;
}

export function setReduxStore(store) {
  reduxStore = store;
}

export function clearBeforeNotifyCallbacks() {
  // this function really only exists for test purposes,
  // to clear the beforeNotify callbacks between tests.
  callbacks.length = 0;
}

/**
 * Allows us to add Bugsnag `beforeNotify` steps without interfering with existing
 * `beforeNotify` callbacks.
 *
 * @example
 *  addBeforeNotify(Bugsnag, (payload, getStore, metadata) =>
 *    payload.user = selectUser(getStore().getState());
 *
 * If any beforeNotify returns false, no notification will happen.
 */
export default function addBeforeNotify(Bugsnag, callback) {
  // don't lose a beforeNotify callback that was created by other means
  if (Bugsnag.beforeNotify && callbacks.length === 0) {
    callbacks.push(Bugsnag.beforeNotify);
  }

  callbacks.push(callback);

  // eslint-disable-next-line no-param-reassign
  Bugsnag.beforeNotify = (payload, metaData) => {
    // call all the callbacks and get their return values
    const results = callbacks.map(cb => cb(payload, getStore, metaData));

    // if any callback is false, return false so the notification doesn't happen
    if (includes(results, false)) {
      return false;
    }

    return payload;
  };
}
