import fetchIntercept from 'fetch-intercept';
import URL from 'url-parse';

import { parseQueryParams } from 'utils/url';

function makeBreadcrumb(fetchUrl) {
  const parsedUrl = new URL(fetchUrl);
  const query = parseQueryParams(parsedUrl.query);
  return {
    type: 'request',
    name: 'fetch',
    metaData: {
      host: parsedUrl.host,
      pathname: parsedUrl.pathname,
      query: parsedUrl.query,
      ...query,
    },
  };
}

/**
 * Sets up logging of any `fetch` requests to the back-end API as part of the breadcrumbs
 * that come over with an error report to Bugsnag.
 */
function setup(Bugsnag) {
  fetchIntercept.register({
    request(url, config) {
      Bugsnag.leaveBreadcrumb(makeBreadcrumb(url));
      return [url, config];
    },
  });
}

export default { setup };
