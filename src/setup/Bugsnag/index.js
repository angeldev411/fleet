import Bugsnag from 'bugsnag-js';

import addBeforeNotify, { setReduxStore } from './addBeforeNotify';
import user from './user';
import fetchBreadcrumbs from './fetchBreadcrumbs';

Bugsnag.apiKey = process.env.BUGSNAG_API_KEY;
Bugsnag.releaseStage = process.env.RELEASE_STAGE;
Bugsnag.notifyReleaseStages = [
  'preview',
  'production',
];

Bugsnag.appVersion = '0.1.0-alpha'; // TODO: set actual app version

/**
 * Helper to set the current Redux store so that Bugsnag can have
 * access to it.
 *
 * @param store
 */
function setStore(store) {
  setReduxStore(store);
}

user.setup(Bugsnag);
fetchBreadcrumbs.setup(Bugsnag);

Bugsnag.metaData = {
  request: {
    referrer: document.referrer, // TODO: get last route from redux-router
    errorCount: 0,
  },
};

addBeforeNotify(Bugsnag, (payload) => {
  if (payload.severity === 'error') {
    Bugsnag.metaData.request.errorCount += 1;
  }
});

// TODO: add other monitoring (e.g. performance), enhanced breadcrumb tracking, etc

export default {
  setStore,
};
