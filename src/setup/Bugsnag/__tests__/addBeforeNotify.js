import { noop } from 'lodash';

import {
  test,
  expect,
  createSpy,
} from '__tests__/helpers/test-setup';

import addBeforeNotify, { setReduxStore, clearBeforeNotifyCallbacks } from '../addBeforeNotify';

test.beforeEach(clearBeforeNotifyCallbacks);

test('addBeforeNotify does not discard beforeNotify callbacks elsewise registered', () => {
  const oldskoolBeforeNotify = createSpy();
  const bugsnagMock = { beforeNotify: oldskoolBeforeNotify };
  const store = 'fake store';
  setReduxStore(store);
  addBeforeNotify(bugsnagMock, noop);
  const payload = 123;
  const metaData = 456;
  bugsnagMock.beforeNotify(payload, metaData);
  expect(oldskoolBeforeNotify).toHaveBeenCalled();
  const firstCall = oldskoolBeforeNotify.calls[0];
  expect(firstCall.arguments[0]).toEqual(payload);
  expect(firstCall.arguments[1]()).toEqual(store); // arg 2 is a function to get the store
  expect(firstCall.arguments[2]).toEqual(metaData);
});

test('addBeforeNotify aborts if any notifier returns false', () => {
  const falseNotifier = () => false;
  const bugsnagMock = { };
  addBeforeNotify(bugsnagMock, falseNotifier);
  expect(bugsnagMock.beforeNotify()).toEqual(false);
});
