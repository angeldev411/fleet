import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Redirect, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

// User Authorization containers
import AuthorizationHandler from 'containers/AuthorizationHandler';
import AuthorizedRoute from 'containers/AuthorizedRoute';

import Layout from 'components/Layout';

// Page Containers
import AssetsPage from 'containers/AssetsPage';

// Case Containers
import CasesPage from 'containers/CasesPage';

import LoginPage from 'containers/LoginPage';
import LogoutPage from 'containers/LogoutPage';

import SearchResultsPage from 'containers/SearchResultsPage';
import ServiceRequestCreatePage from 'containers/ServiceRequestCreatePage';
import ServiceRequestSubmittedPage from 'containers/ServiceRequestSubmittedPage';

// 404 not found
import NotFoundPage from 'components/NotFoundPage';
import ServiceRequestsPage from 'containers/ServiceRequestsPage';

import DetailsPage from 'compositions/DetailsPage';

import { setLatestFavorite as setLatestFavoriteAction } from 'redux/app/actions';
import { favoritesSelector } from 'redux/app/selectors';

import {
  renderPageFromConfig,
  setFavoriteAndRender,
  setSearchFavoriteAndRender,
} from 'utils/routes';

function mapStateToProps(state) {
  return {
    favorites: favoritesSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setLatestFavorite: favoriteWithLabel => dispatch(setLatestFavoriteAction(favoriteWithLabel)),
  };
}

function AuthorizedRoutes({
  favorites,
  setLatestFavorite,
}) {
  // Preload setFavoriteAndRender with the favorites and setLatestFavorite.
  // This allows us to simply pass the curried function to the render prop.
  const renderFavoriteComponent = setFavoriteAndRender(favorites, setLatestFavorite);
  const renderSearchFavoriteComponent = setSearchFavoriteAndRender(setLatestFavorite);

  // Preload `renderPageFromConfig` with the DetailsPage.
  // Set the configKey when rendering in the route.
  const renderDetailsPage = renderPageFromConfig(DetailsPage);

  return (
    <Layout>
      <Switch>
        <AuthorizedRoute
          exact
          path="/approvals/filter/:slug"
          render={renderFavoriteComponent(CasesPage)}
          submenu="approvals"
        />
        <AuthorizedRoute exact path="/appointments" />
        <AuthorizedRoute
          exact
          path="/assets/filter/:slug"
          render={renderFavoriteComponent(AssetsPage)}
          submenu="assets"
        />
        <AuthorizedRoute
          exact
          path="/assets/:assetId"
          render={renderDetailsPage('ASSET_DETAIL')}
        />
        <AuthorizedRoute
          exact
          path="/cases/filter/:slug"
          render={renderFavoriteComponent(CasesPage)}
          submenu="cases"
        />
        <AuthorizedRoute
          exact
          path="/cases/:caseId"
          render={renderDetailsPage('CASE_DETAIL')}
        />
        <AuthorizedRoute
          path="/search"
          component={renderSearchFavoriteComponent(SearchResultsPage)}
        />
        <AuthorizedRoute
          exact
          path="/service-requests/filter/:slug"
          render={renderFavoriteComponent(ServiceRequestsPage)}
          submenu="serviceRequests"
        />
        <AuthorizedRoute
          exact
          path="/service-requests/new"
          component={ServiceRequestCreatePage}
        />
        <AuthorizedRoute
          exact
          path="/service-requests/:serviceRequestId/submitted"
          component={ServiceRequestSubmittedPage}
        />
        <AuthorizedRoute
          exact
          path="/service-requests/:serviceRequestId"
          render={renderDetailsPage('SERVICE_REQUEST_DETAIL')}
        />
        <AuthorizedRoute exact path="/reports" />
        <AuthorizedRoute exact path="/administration" />
        <AuthorizedRoute path="/404-not-found" component={NotFoundPage} />
        <Redirect exact from="/" to="/cases/filter/all-repair-cases" />
        <Redirect exact from="/assets" to="/assets/filter/all-assets" />
        <Redirect exact from="/cases" to="/cases/filter/all-repair-cases" />
        <NotFoundPage />
      </Switch>
    </Layout>
  );
}

AuthorizedRoutes.propTypes = {
  // `favorites` is an immutable map of the submenu item definitions
  favorites: ImmutablePropTypes.map.isRequired,

  // `setLatestFavorite` dispatches an action the updates the app state with the most recent
  // favorite
  setLatestFavorite: PropTypes.func.isRequired,
};

const ConnectedAuthorizedRoutes = connect(mapStateToProps, mapDispatchToProps)(AuthorizedRoutes);

function Routes() {
  return (
    <AuthorizationHandler>
      <Switch>
        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/logout" component={LogoutPage} />
        <Route path="/" component={ConnectedAuthorizedRoutes} />
      </Switch>
    </AuthorizationHandler>
  );
}

export default Routes;
