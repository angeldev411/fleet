import { combineReducers } from 'redux-immutable';

import { LOGOUT } from 'redux/constants';
import uiReducer from 'redux/ui/reducer';
import appReducer from 'redux/app/reducer';
import assetsReducer from 'redux/assets/reducer';
import casesReducer from 'redux/cases/reducer';
import dataListsReducer from 'redux/dataLists/reducer';
import pageConfigsReducer from 'redux/pageConfigs/reducer';
import requestsReducer from 'redux/requests/reducer';
import serviceProvidersReducer from 'redux/serviceProviders/reducer';
import serviceRequestsReducer from 'redux/serviceRequests/reducer';
import userReducer from 'redux/user/reducer';
import wizardConfigsReducer from 'redux/wizardConfigs/reducer';

const rootReducer = combineReducers({
  ui: uiReducer,
  app: appReducer,
  assets: assetsReducer,
  cases: casesReducer,
  dataLists: dataListsReducer,
  pageConfigs: pageConfigsReducer,
  requests: requestsReducer,
  serviceProviders: serviceProvidersReducer,
  serviceRequests: serviceRequestsReducer,
  user: userReducer,
  wizardConfigs: wizardConfigsReducer,
});

export default function createReducer() {
  return function applicationReducer(state, action) {
    const newState = action.type === LOGOUT ? undefined : state;

    return rootReducer(newState, action);
  };
}
