import React from 'react';
import { test } from 'ava';
import expect from 'expect';
import { mount, shallow } from 'enzyme';

import I18n from '../index';

const TestComponent = () => (<div id="test-component" />);

test('renders its children', () => {
  const wrapper = mount(
    <I18n>
      <TestComponent />
    </I18n>,
  );
  expect(wrapper.find('#test-component').length).toEqual(1);
});

test('renders an IntlProvider component with the given locale and messages', () => {
  const locale = 'the locale';
  const messages = { my: 'message' };
  const wrapper = shallow(
    <I18n locale={locale} messages={messages}>
      <TestComponent />
    </I18n>,
  );

  const intlProvider = wrapper.find('IntlProvider');
  expect(intlProvider.length).toEqual(1);
  expect(intlProvider.props().locale).toEqual(locale);
  expect(intlProvider.props().messages).toEqual(messages);
});
