import AssetAvailability from 'components/WizardSteps/AssetAvailability';
import AssetSelection from 'components/WizardSteps/AssetSelection';
import ContactPerson from 'components/WizardSteps/ContactPerson';
import ReasonForRepair from 'components/WizardSteps/ReasonForRepair';
import Review from 'components/WizardSteps/Review';
import ServiceProviderSelection from 'components/WizardSteps/ServiceProviderSelection';
import ServiceRequestType from 'components/WizardSteps/ServiceRequestType';

import ServiceRequestWizardController from 'containers/ServiceRequestWizardController';

import {
  ASSET_AVAILABILITY,
  ASSET_SELECTION,
  CONTACT_PERSON,
  REASON_FOR_REPAIR,
  REVIEW,
  SERVICE_PROVIDER_SELECTION,
  SERVICE_REQUEST_TYPE,
  SERVICE_REQUEST_WIZARD,
} from './constants';

// The wizard configs are used to add the specfic wizard controllers to the mappedWizardConfigs,
// but they get lost when simply added to the default export.
const specificWizardControllers = {
  [SERVICE_REQUEST_WIZARD]: ServiceRequestWizardController,
};

// Export the wizardConfigKeys for use in propTypes validations.
export const wizardConfigKeys = Object.keys(specificWizardControllers);

export default {
  [ASSET_AVAILABILITY]: AssetAvailability,
  [ASSET_SELECTION]: AssetSelection,
  [CONTACT_PERSON]: ContactPerson,
  [REASON_FOR_REPAIR]: ReasonForRepair,
  [REVIEW]: Review,
  [SERVICE_PROVIDER_SELECTION]: ServiceProviderSelection,
  [SERVICE_REQUEST_TYPE]: ServiceRequestType,
  ...specificWizardControllers,
};
