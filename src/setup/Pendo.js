import { configEnabled } from 'utils/config';

/* istanbul ignore next */
function enablePendo() {
  return configEnabled('PENDO_ENABLED');
}

/* istanbul ignore next */
export function installPendo() {
  if (enablePendo()) {
    // This snippet is the install script provided by Pendo for SPA's
    /* eslint-disable */
    (function(apiKey){
      (function(p,e,n,d,o){var v,w,x,y,z;o=p[d]=p[d]||{};o._q=[];
        v=['initialize','identify','updateOptions','pageLoad'];for(w=0,x=v.length;w<x;++w)(function(m){
        o[m]=o[m]||function(){o._q[m===v[0]?'unshift':'push']([m].concat([].slice.call(arguments,0)));};})(v[w]);
        y=e.createElement(n);y.async=!0;y.src='https://cdn.pendo.io/agent/static/'+apiKey+'/pendo.js';
        z=e.getElementsByTagName(n)[0];z.parentNode.insertBefore(y,z);})(window,document,'script','pendo');
    })(process.env.PENDO_API_KEY);
    /* eslint-enable */
  }
}

/* istanbul ignore next */
export function initializePendo(user) {
  if (enablePendo()) {
    pendo.initialize({
      visitor: {
        id: user.email,
        oem: memoryDB.DECISIV__oem,
        ...user,
      },
      account: {
        id: memoryDB.DECISIV__oem,
      },
    });
  }
}
