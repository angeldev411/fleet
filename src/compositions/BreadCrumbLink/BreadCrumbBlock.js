import styled from 'styled-components';

/**
 * Contains all of the BreadCrumb link and icon display components
 * @type {StyledComponent}
 */
const BreadCrumbBlock = styled.div`
  display: flex;
`;

export default BreadCrumbBlock;
