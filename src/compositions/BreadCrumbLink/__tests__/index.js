import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { BreadCrumbLink } from '../index';

const defaultProps = {
  collapsed: false,
  breadcrumbs: {
    previous: {
      message: { id: 'some-id' },
      path: 'path/to/previous/',
      number: '123',
    },
    current: {
      message: { id: 'some-id' },
      path: 'path/to/case',
      number: '321',
    },
  },
  setCurrentBreadcrumbs: () => {},
  setPreviousBreadcrumbs: () => {},
  clearBreadcrumbs: () => {},
};

function shallowRender(props = defaultProps) {
  return shallow(<BreadCrumbLink {...props} />);
}

test('with number = undefined, renders null', () => {
  const breadcrumbs = { current: {}, previous: {} };
  const component = shallowRender({ ...defaultProps, breadcrumbs });
  expect(component.type()).toBe(null);
});

test('with collapsed = false, renders title as link', () => {
  const component = shallowRender();
  expect(component).toContain('BreadCrumbLinkWrapper');
  const link = component.find('BreadCrumbLinkWrapper');
  expect(link).toContain('FormattedMessage');
  expect(link.find('FormattedMessage')).toHaveProps({ ...defaultProps.breadcrumbs.previous.message });
});

test('with collapsed = false, renders TextDiv containing icon', () => {
  const component = shallowRender();
  expect(component).toContain('TextDiv');
  const div = component.find('TextDiv');
  expect(div).toContain('FontAwesome');
  expect(div.find('FontAwesome')).toHaveProp('name', 'angle-right');
});

test('with collapsed = true, renders icon as link', () => {
  const component = shallowRender({ ...defaultProps, collapsed: true });
  expect(component).toContain('BreadCrumbLinkWrapper');
  const link = component.find('BreadCrumbLinkWrapper');
  expect(link).toContain('FontAwesome');
  expect(link.find('FontAwesome')).toHaveProp('name', 'angle-left');
});
