import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { LinkWithoutStyleProps, modifierConfig } from '../BreadCrumbLinkWrapper';

const defaultProps = {
  theme: 'THEME',
  modifiers: 'MODIFIERS',
  others: 'OTHERS',
  to: '/',
};

const buildLink = (props = defaultProps) =>
  shallow(
    <MemoryRouter>
      { LinkWithoutStyleProps(props) }
    </MemoryRouter>,
  );

// --------------------------- LinkWithoutStyleProps ---------------------------
test('LinkWithoutStyleProps renders without props `theme` and `modifiers`', () => {
  const link = buildLink().props().children;
  expect(link.props.theme).toEqual(undefined);
  expect(link.props.modifiers).toEqual(undefined);
  expect(link.props.others).toEqual('OTHERS');
});

// --------------------------- modifierConfig ---------------------------

test('modifierConfig/icon has color textLight', () => {
  const testColor = 'testColor';
  const theme = { colors: { base: { link: testColor } } };
  const iconStyle = modifierConfig.icon({ theme }).styles;
  expect(iconStyle).toInclude(`color: ${testColor}`);
});
