import { omit } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { Link as UnstyledLink } from 'react-router-dom';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/**
 * LinkWithoutStyle removes the theme props from react-router-dom's `Link` component,
 * allowing us to use styled-components without prop errors in the console.
 */
export const LinkWithoutStyleProps = props =>
  <UnstyledLink {...omit(props, ['theme', 'modifiers'])} />;

/* istanbul ignore next */
export const modifierConfig = {
  icon: ({ theme }) => ({
    styles: `
      color: ${theme.colors.base.link};
      font-size: ${px2rem(15)};
      &:hover {
        text-decoration: none;
      }
      .fa {
        font-size: ${px2rem(15)};
        line-height: ${px2rem(19)};
      }
    `,
  }),
  padRight: () => ({
    styles: `
      padding-right: ${px2rem(8)};
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.link};
  font-size: ${px2rem(15)};
  font-weight: 400;
  line-height: ${px2rem(19)};
  margin: auto;
  text-decoration: none;
  text-transform: uppercase;
  &:hover {
    color: ${props.theme.colors.base.linkHover};
    text-decoration: underline;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      link: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'BreadCrumbLinkWrapper',
  styled(LinkWithoutStyleProps),
  styles,
  { modifierConfig, themePropTypes },
);
