import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import FontAwesome from 'react-fontawesome';
import { isEmpty } from 'lodash';
import {
  Column,
  Row,
} from 'styled-components-reactive-grid';

import PageHeadingTitle from 'elements/PageHeadingTitle';
import TextDiv from 'elements/TextDiv';

import BreadCrumbBlock from './BreadCrumbBlock';
import BreadCrumbLinkWrapper from './BreadCrumbLinkWrapper';
import withConnected from './Container';

function buildPreviousBreadcrumb(breadcrumbs, collapsed) {
  if (isEmpty(breadcrumbs.previous)) {
    return null;
  }

  if (collapsed) {
    return (
      <BreadCrumbBlock>
        <BreadCrumbLinkWrapper modifiers={['icon']} to={breadcrumbs.previous.path} >
          <FontAwesome name="angle-left" />
        </BreadCrumbLinkWrapper>
      </BreadCrumbBlock>
    );
  }
  return (
    <BreadCrumbBlock>
      <BreadCrumbLinkWrapper
        modifiers={['padRight']}
        to={breadcrumbs.previous.path}
      >
        <FormattedMessage
          {...breadcrumbs.previous.message}
          values={{ number: breadcrumbs.previous.number }}
        />
      </BreadCrumbLinkWrapper>
      <TextDiv modifiers={['bold', 'inline', 'largeText', 'lightGreyText']}>
        <FontAwesome name="angle-right" />
      </TextDiv>
    </BreadCrumbBlock>
  );
}

export class BreadCrumbLink extends Component {
  static propTypes = {
    collapsed: PropTypes.bool,
    breadcrumbs: PropTypes.shape({
      current: PropTypes.shape({
        message: PropTypes.object,
        number: PropTypes.string,
        path: PropTypes.string,
      }).isRequired,
      previous: PropTypes.shape({
        message: PropTypes.object,
        number: PropTypes.string,
        path: PropTypes.string,
      }).isRequired,
    }).isRequired,
  };

  static defaultProps = {
    collapsed: false,
  };

  render() {
    const {
      breadcrumbs,
      collapsed,
    } = this.props;

    if (!breadcrumbs.current.message) {
      return null;
    }

    const previousBreadcrumb = buildPreviousBreadcrumb(breadcrumbs, collapsed);

    return (
      <Row modifiers={['middle']}>
        { !isEmpty(previousBreadcrumb) &&
          <Column>
            {previousBreadcrumb}
          </Column>
        }
        <Column>
          <PageHeadingTitle>
            <FormattedMessage
              {...breadcrumbs.current.message}
              values={{ number: breadcrumbs.current.number }}
            />
          </PageHeadingTitle>
        </Column>
      </Row>
    );
  }
}

const wrappedComponent = withConnected(BreadCrumbLink);
wrappedComponent.displayName = 'BreadCrumbLink';
export default wrappedComponent;
