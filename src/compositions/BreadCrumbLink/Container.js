import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import withCollapsedState from 'components/_common/withCollapsedState';

import { breadcrumbsSelector } from 'redux/app/selectors';

/* istanbul ignore next */
const BreadcrumbLinkContainer = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    breadcrumbs: PropTypes.shape({
      current: PropTypes.shape({
        path: PropTypes.string,
        message: PropTypes.object,
      }).isRequired,
      previous: PropTypes.shape({
        path: PropTypes.string,
        message: PropTypes.object,
      }).isRequired,
    }).isRequired,
    collapsed: PropTypes.bool.isRequired,
  };

  function mapStateToProps(state) {
    return {
      breadcrumbs: breadcrumbsSelector(state),
    };
  }

  return connect(mapStateToProps)(
    withCollapsedState(Container),
  );
};

export default BreadcrumbLinkContainer;
