import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { compact } from 'lodash';
import { compose, setDisplayName } from 'recompose';

import { SplitBlock } from 'elements/SplitBlock';
import StatusSpan from 'elements/StatusSpan';
import Table from 'elements/Table';
import TextDiv from 'elements/TextDiv';

import Widget from 'elements/Widget';

import { getAssetStatusText, getOdometerText } from 'utils/asset';
import { getOutputText } from 'utils/widget';

import InfoBox from './InfoBox';
import messages from './messages';
import withConnectedData from './Container';

export function AssetSummaryWidget({
  assetInfo: {
    odometer,
    campaignsStatus,
    engine,
    make,
    model,
    pmStatus,
    unitNumber,
    vinNumber,
    warrantyStatus,
    year,
  },
}) {
  const odometerUnit = (odometer) ? odometer.unit : '';
  const odometerReading = (odometer) ? odometer.reading : '';
  const odometerReadAt = (odometer) ? odometer.readAt : '';

  return (
    <Widget expandKey="asset-summary">
      <Widget.Header>
        <FormattedMessage {...messages.title} />
      </Widget.Header>
      <Widget.Item>
        <SplitBlock modifiers={['flexRow']}>
          <Table modifiers={['smallText']} trModifiers={['flex']}>
            <tbody>
              <tr>
                <td>
                  <TextDiv modifiers={['bold', 'capitalize', 'inline', 'padRight']}>
                    <FormattedMessage {...messages.unit} />
                  </TextDiv>
                  {getOutputText(unitNumber)}
                </td>
                <td>
                  <TextDiv modifiers={['bold', 'inline', 'padRight']}>
                    <FormattedMessage {...messages.vin} />
                  </TextDiv>
                  {getOutputText(vinNumber)}
                </td>
                <td>
                  <TextDiv modifiers={['bold', 'capitalize', 'inline', 'padRight']}>
                    <FormattedMessage {...messages.make} />
                  </TextDiv>
                  {getOutputText(make)}
                </td>
                <td>
                  <TextDiv modifiers={['bold', 'capitalize', 'inline', 'padRight']}>
                    <FormattedMessage {...messages.model} />
                  </TextDiv>
                  {getOutputText(model)}
                </td>
                <td>
                  <TextDiv modifiers={['bold', 'capitalize', 'inline', 'padRight']}>
                    <FormattedMessage {...messages.year} />
                  </TextDiv>
                  {getOutputText(year)}
                </td>
                <td>
                  <TextDiv modifiers={['bold', 'capitalize', 'inline', 'padRight']}>
                    <FormattedMessage {...messages.engine} />
                  </TextDiv>
                  {getOutputText(engine)}
                </td>
              </tr>
            </tbody>
          </Table>
        </SplitBlock>
        <SplitBlock modifiers={['flexRow']}>
          <InfoBox title={messages.odometer}>
            {getOdometerText(odometerReading, `${odometerUnit}\n`, odometerReadAt)}
          </InfoBox>
          <InfoBox title={messages.maintenance}>
            <StatusSpan modifiers={compact(['bold', 'capitalize', pmStatus])}>
              {getAssetStatusText('pmStatus', pmStatus)}
            </StatusSpan>
          </InfoBox>
          <InfoBox title={messages.campaigns}>
            <StatusSpan modifiers={compact(['bold', 'capitalize', campaignsStatus])}>
              {getAssetStatusText('campaignsStatus', campaignsStatus)}
            </StatusSpan>
          </InfoBox>
          <InfoBox title={messages.warranty}>
            <StatusSpan modifiers={compact(['bold', 'capitalize', warrantyStatus])}>
              {getAssetStatusText('warrantyStatus', warrantyStatus)}
            </StatusSpan>
          </InfoBox>
        </SplitBlock>
      </Widget.Item>
    </Widget>
  );
}

AssetSummaryWidget.propTypes = {
  assetInfo: PropTypes.shape({
    campaignsStatus: PropTypes.string,
    engine: PropTypes.string,
    make: PropTypes.string,
    model: PropTypes.string,
    odometer: PropTypes.shape({
      readAt: PropTypes.string,
      reading: PropTypes.string,
      unit: PropTypes.string,
    }),
    pmStatus: PropTypes.string,
    readAt: PropTypes.string,
    reading: PropTypes.string,
    unit: PropTypes.string,
    unitNumber: PropTypes.string,
    vinNumber: PropTypes.string,
    warrantyStatus: PropTypes.string,
    year: PropTypes.string,
  }).isRequired,
};

export default compose(
  setDisplayName('AssetSummaryWidget'),
  withConnectedData,
)(AssetSummaryWidget);
