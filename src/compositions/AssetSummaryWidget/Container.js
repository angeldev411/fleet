import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import {
  currentAssetSelector,
} from 'redux/assets/selectors';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    assetInfo: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
  };

  Container.defaultProps = {
    assetInfo: {},
  };

  function mapStateToProps(state) {
    return {
      assetInfo: currentAssetSelector(state),
    };
  }

  return connect(mapStateToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
