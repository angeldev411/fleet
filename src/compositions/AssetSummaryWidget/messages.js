import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'compositions.AssetSummaryWidget.title',
    defaultMessage: 'Asset Summary',
  },
  maintenance: {
    id: 'compositions.AssetSummaryWidget.maintenance',
    defaultMessage: 'Maintenance',
  },
  campaigns: {
    id: 'compositions.AssetSummaryWidget.campaigns',
    defaultMessage: 'Campaigns',
  },
  warranty: {
    id: 'compositions.AssetSummaryWidget.warranty',
    defaultMessage: 'Warranty',
  },
  unit: {
    id: 'compositions.AssetSummaryWidget.data.unit',
    defaultMessage: 'UNIT',
  },
  vin: {
    id: 'compositions.AssetSummaryWidget.data.vin',
    defaultMessage: 'VIN',
  },
  make: {
    id: 'compositions.AssetSummaryWidget.data.make',
    defaultMessage: 'Make',
  },
  model: {
    id: 'compositions.AssetSummaryWidget.data.model',
    defaultMessage: 'Model',
  },
  year: {
    id: 'compositions.AssetSummaryWidget.data.year',
    defaultMessage: 'Year',
  },
  engine: {
    id: 'compositions.AssetSummaryWidget.data.engine',
    defaultMessage: 'Engine',
  },
  odometer: {
    id: 'compositions.AssetSummaryWidget.data.odometer',
    defaultMessage: 'Odometer',
  },
  current: {
    id: 'compositions.AssetSummaryWidget.status.current',
    defaultMessage: 'Current',
  },
  due: {
    id: 'compositions.AssetSummaryWidget.status.due',
    defaultMessage: 'Due',
  },
  overdue: {
    id: 'compositions.AssetSummaryWidget.status.overdue',
    defaultMessage: 'Overdue',
  },
  allCurrent: {
    id: 'compositions.AssetSummaryWidget.status.allCurrent',
    defaultMessage: 'All Current',
  },
  someCurrent: {
    id: 'compositions.AssetSummaryWidget.status.someCurrent',
    defaultMessage: 'Some Current',
  },
  noCoverage: {
    id: 'compositions.AssetSummaryWidget.status.noCoverage',
    defaultMessage: 'No Coverage',
  },
  campaignDue: {
    id: 'compositions.AssetSummaryWidget.status.campaignDue',
    defaultMessage: 'Campaign Due',
  },
});

export default messages;
