import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  border: 1px solid ${props.theme.colors.base.chrome500};
  display: flex;
  flex-direction: column;
  margin: ${px2rem(2.5)} ${px2rem(5)} ${px2rem(2.5)} 0;
  min-height: ${px2rem(60)};
  padding: ${px2rem(10)};
  width: ${px2rem(120)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome500: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
  { themePropTypes },
);
