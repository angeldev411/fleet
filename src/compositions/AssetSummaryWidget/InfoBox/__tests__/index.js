import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import InfoBox from '../index';

const defaultTitle = {
  id: 'title_id',
  defaultMessage: 'title_message',
};

function renderComponent(title = defaultTitle) {
  return shallow(<InfoBox title={title} />);
}

test('InfoBox renders FormattedMessage in the Title', () => {
  const component = renderComponent();
  const title = component.find('Title');
  expect(title).toContain('FormattedMessage');
});
