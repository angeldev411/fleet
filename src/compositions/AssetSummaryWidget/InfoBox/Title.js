import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  align-items: center;
  color: ${props.theme.colors.base.text};
  display: flex;
  flex-direction: column;
  font-size: ${px2rem(10)};
  margin: ${px2rem(5)} 0;
  text-transform: uppercase;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Title',
  styled.div,
  styles,
  { themePropTypes },
);
