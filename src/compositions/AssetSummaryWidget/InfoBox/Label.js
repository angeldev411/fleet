import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const Label = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  font-size: ${px2rem(12)};
  font-weight: 700;
  margin: ${px2rem(5)} 0;
  text-align: center;
  white-space: pre-line;
`;

export default Label;
