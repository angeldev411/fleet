import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, messageDescriptorPropTypes } from 'react-intl';

import Wrapper from './Wrapper';
import Title from './Title';
import Label from './Label';

function InfoBox({ title, children }) {
  return (
    <Wrapper>
      <Title>
        <FormattedMessage {...title} />
      </Title>
      <Label>
        {children}
      </Label>
    </Wrapper>
  );
}

InfoBox.propTypes = {
  title: PropTypes.shape(messageDescriptorPropTypes).isRequired,
  children: PropTypes.node,
};

InfoBox.defaultProps = {
  children: null,
};

export default InfoBox;
