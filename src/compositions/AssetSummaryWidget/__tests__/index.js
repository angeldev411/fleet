import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { AssetSummaryWidget } from '../index';
import messages from '../messages';

const defaultProps = {
  assetInfo: {
    unitNumber: '1234567',
    vinNumber: '3GBKC34F41M111912',
    year: '2015',
    make: 'Volvo',
    model: 'Truck',
    engine: 'MP10',
    pmStatus: 'green',
    warrantyStatus: 'green',
    campaignsStatus: 'red',
    odometer: {
      unit: 'kilometers',
      reading: '3242',
      readAt: '2017-01-10T16:46:22Z',
    },
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetSummaryWidget {...props} />);
}

test('AssetSummaryWidget renders a WidgetHeader with title', () => {
  const component = shallowRender();
  const Header = component.find('Header');
  expect(Header).toContain('FormattedMessage');
  expect(Header.find('FormattedMessage')).toHaveProps({ ...messages.title });
});
