import { noop } from 'lodash';
import React from 'react';

import { test, expect, shallow, createSpy } from '__tests__/helpers/test-setup';

import { ParentMenuContainer } from '../ParentMenuContainer';

const extraProps = {
  border: false,
  path: '/super-fly',
};

const defaultProps = {
  ...extraProps,
  expandedParents: [],
  expandParent: noop,
  icon: 'icon',
  label: 'label',
  navExpanded: true,
  isActiveFunc: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<ParentMenuContainer {...props} />);
}

test('ParentMenuContainer wraps a ParentMenu', () => {
  expect(shallowRender()).toContain('ParentMenu');
});

test('ParentMenuContainer passes correct expanded state to ParentMenu', () => {
  const expandedParents = ['label-icon', 'super-fly'];
  const container = shallowRender({ ...defaultProps, expandedParents });
  const parentMenu = container.find('ParentMenu');
  expect(parentMenu).toHaveProp('navExpanded', true);
  expect(parentMenu).toHaveProp('expanded', true);
});

test('ParentMenuContainer does not pass expanded state when not navExpanded', () => {
  const expandedParents = ['label-icon', 'super-fly'];
  const container = shallowRender({ ...defaultProps, expandedParents, navExpanded: false });
  const parentMenu = container.find('ParentMenu');
  expect(parentMenu).toHaveProp('navExpanded', false);
  expect(parentMenu).toHaveProp('expanded', false);
});

test('ParentMenuContainer can call expandLeftNavParent from ParentMenu when navExpanded', () => {
  const action = createSpy();
  const container = shallowRender({ ...defaultProps, navExpanded: true, expandParent: action });
  const parentMenu = container.find('ParentMenu');
  const onExpandParent = parentMenu.prop('onExpandParent');
  onExpandParent('whoosh');
  expect(action).toHaveBeenCalledWith('whoosh');
});

test('ParentMenuContainer can not call expandLeftNavParent when not navExpanded', () => {
  const action = createSpy();
  const container = shallowRender({ ...defaultProps, navExpanded: false, expandParent: action });
  const parentMenu = container.find('ParentMenu');
  const onExpandParent = parentMenu.prop('onExpandParent');
  onExpandParent('whoosh');
  expect(action).toNotHaveBeenCalled();
});
