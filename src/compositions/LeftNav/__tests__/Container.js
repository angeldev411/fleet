import { noop } from 'lodash';
import React from 'react';

import { test, expect, shallow, createSpy } from '__tests__/helpers/test-setup';

import { withTestTheme } from 'utils/styles';

import { LeftNavContainer, composeMenus } from '../Container';
import menuDefault from '../menuDefault';

const LeftNavContainerWithTestTheme = withTestTheme(LeftNavContainer);

// -------------------- Container --------------------

const defaultFavorites = {};

const defaultProps = {
  favorites: defaultFavorites,
  initFavorites: noop,
  loadDashboard: noop,
  navExpanded: true,
  setLeftNavExpanded: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<LeftNavContainerWithTestTheme {...props} />);
}

test('Container wraps a LeftNav', () => {
  expect(shallowRender()).toContain('WithTheme(LeftNav)');
});

test('Container loads the dashboard before mounting', () => {
  const loadDashboard = createSpy();
  const container = shallowRender({ ...defaultProps, loadDashboard });
  loadDashboard.reset(); // make sure no false triggers
  container.instance().componentWillMount();
  expect(loadDashboard).toHaveBeenCalled();
  expect(loadDashboard.calls.length).toEqual(1);
});

test('Container composes the menus with the given favorites', () => {
  const favorites = {
    cases: [
      { title: 'First Favorite', slug: 'first-favorite', path: '/first-favorite' },
    ],
  };
  const container = shallowRender({ ...defaultProps, favorites });
  const composedMenus = composeMenus(menuDefault, favorites);
  const leftNav = container.find('WithTheme(LeftNav)');
  expect(leftNav.prop('menus')).toEqual(composedMenus);
});

test('Container passes navExpanded state to LeftNav', () => {
  const container = shallowRender({ ...defaultProps, navExpanded: false });
  const leftNav = container.find('WithTheme(LeftNav)');
  expect(leftNav).toHaveProp('navExpanded', false);
});

test('Container passes a function to LeftNav for expanding the left nav', () => {
  const action = createSpy();
  const container = shallowRender({ ...defaultProps, setLeftNavExpanded: action });
  const leftNav = container.find('WithTheme(LeftNav)');
  const onExpandLeftNav = leftNav.prop('onExpandLeftNav');
  expect(action).toNotHaveBeenCalled();
  onExpandLeftNav('some fancy argument');
  expect(action).toHaveBeenCalledWith('some fancy argument');
});

test('setLeftNavExpanded is called when window.innerWidth is small enough.', () => {
  const action = createSpy();
  const container = shallowRender({ ...defaultProps, setLeftNavExpanded: action });
  window.innerWidth = 1;
  container.instance().componentDidMount();
  expect(action).toHaveBeenCalledWith(false);
});

test('setLeftNavExpanded is not called when window.innerWidth is big enough.', () => {
  const action = createSpy();
  const container = shallowRender({ ...defaultProps, setLeftNavExpanded: action });
  window.innerWidth = 1000000;
  container.instance().componentDidMount();
  expect(action).toNotHaveBeenCalled();
});

test('Renders the WindowQuery', () => {
  expect(shallowRender()).toContain('WindowQuery');
});

// -------------------- onResizeWindow --------------------

test('collapse leftnav when it\'s expanded and windowWidth is less than 1366', () => {
  const setLeftNavExpanded = createSpy();
  const component = shallowRender({ ...defaultProps, expanded: true, setLeftNavExpanded });
  component.instance().onResizeWindow({ windowWidth: 1200 });
  expect(setLeftNavExpanded).toHaveBeenCalledWith(false);
});

test(`expand leftnav when state requestedNavExpanded is true,
  it's collapsed and windowWidth is more than 1366`, () => {
    const setLeftNavExpanded = createSpy();
    const component = shallowRender({ ...defaultProps, navExpanded: false, setLeftNavExpanded });
    component.setState({ requestedNavExpanded: true });
    component.instance().onResizeWindow({ windowWidth: 1400 });
    expect(setLeftNavExpanded).toHaveBeenCalledWith(true);
  });

test(`no action when state requestedNavExpanded is false,
  it's collapsed and windowWidth is more than 1366`, () => {
    const setLeftNavExpanded = createSpy();
    const component = shallowRender({ ...defaultProps, navExpanded: false, setLeftNavExpanded });
    component.setState({ requestedNavExpanded: false });
    component.instance().onResizeWindow({ windowWidth: 1400 });
    expect(setLeftNavExpanded).toNotHaveBeenCalled();
  });

// -------------------- handleExpandLeftNavClick --------------------

test('call setLeftNavExpanded with the value from expanded param', () => {
  const setLeftNavExpanded = createSpy();
  const component = shallowRender({ ...defaultProps, setLeftNavExpanded });
  const expanded = true;
  component.instance().handleExpandLeftNavClick(expanded);
  expect(setLeftNavExpanded).toHaveBeenCalledWith(expanded);
});

test('set state requestedNavExpanded as the value from expanded param ', () => {
  const component = shallowRender();
  component.setState({ ...defaultProps, requestedNavExpanded: true });
  const expanded = false;
  component.instance().handleExpandLeftNavClick(expanded);
  expect(component.state().requestedNavExpanded).toBe(expanded);
});

// -------------------- composeMenus --------------------

test('composeMenus adds the favorites to each matching menu item', () => {
  const menuGroups = [
    [{ label: 'first_parent' }, { label: 'second_nonparent' }],
    [{ label: 'third_nonparent' }],
    [{ label: 'fourth_parent' }],
  ];

  const favorites = {
    first_parent: ['first-one', 'first-two'],
    fourth_parent: ['fourth-one'],
  };

  const expectedResult = [
    [{ label: 'first_parent', favorites: favorites.first_parent }, { label: 'second_nonparent' }],
    [{ label: 'third_nonparent' }],
    [{ label: 'fourth_parent', favorites: favorites.fourth_parent }],
  ];

  expect(composeMenus(menuGroups, favorites)).toEqual(expectedResult);
});
