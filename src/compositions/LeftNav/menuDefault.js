/**
 * The menu info passed into LeftNav is an array of arrays; each top-level array is a "group",
 * and within each group is a top-level menu item definition.
 *
 * The top-level menu item definitions each have the following properties:
 *  * label: the message key for the displayed text of the menu item
 *  * icon: the FontAwesome icon for the menu item
 *  * and either...
 *    * path: if the menu item has no "favorites", clicking it will go to this path
 *    ...or...
 *    * path: prefix of pathname for all favorites under this parent menu item
 *    * favorites: an array of sub-menu items, each having:
 *      * label: the message key for the displayed text of the menu item
 *      * filterId: the ID of the individual favorite
 *
 * If a top-level menu item has favorites, then clicking the menu item will simply expand or
 * contract the list of favorites; clicking the menu item will not change the current route.
 */
const menuDefault = [
  [
    {
      icon: 'briefcase',
      label: 'cases',
      path: '/cases',
    },
    {
      icon: 'check',
      label: 'approvals',
      path: '/approvals',
    },
    {
      icon: 'wrench',
      label: 'serviceRequests',
      path: '/service-requests',
    },
    // TODO: parts requests
    {
      icon: 'truck',
      label: 'assets',
      path: '/assets',
    },
    // TODO: service providers
    // {
    //   icon: 'calendar',
    //   label: 'appointments',
    //   path: '/appointments',
    // },
  ],
  // TODO: reports
  // [
  //   {
  //     icon: 'area-chart',
  //     label: 'reports',
  //     path: '/reports',
  //   },
  // ],
];

export default menuDefault;
