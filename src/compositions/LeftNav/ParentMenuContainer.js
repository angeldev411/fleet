import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { expandLeftNavParent } from 'redux/ui/actions';
import {
  leftNavExpandedSelector,
  leftNavExpandedParentsSelector,
} from 'redux/ui/selectors';

import immutableToJS from 'utils/immutableToJS';

import ParentMenu from './LeftNavComponent/ParentMenu';

function parentMenuKey({ label, icon }) {
  return `${label}-${icon}`;
}

/**
 * This component hooks up a top-level left nav menu item (containing a favorites
 * submenu) with the Redux store so that the expanded/collapsed state of the menu
 * item can be persisted to the store.
 */
export function ParentMenuContainer({
  expandedParents,
  expandParent,
  icon,
  label,
  navExpanded,
  isActiveFunc,
  ...rest
}) {
  const onExpandParent = (payload) => {
    // only expand/contract submenus when the left nav is expanded
    if (navExpanded) {
      return expandParent(payload);
    }

    return false;
  };

  const navKey = parentMenuKey({ label, icon });
  const isExpanded = !!(
    navExpanded &&
    Array.isArray(expandedParents) &&
    expandedParents.includes(navKey)
  );

  return (
    <ParentMenu
      onExpandParent={onExpandParent}
      navExpanded={navExpanded}
      expanded={isExpanded}
      label={label}
      icon={icon}
      navKey={navKey}
      isActiveFunc={isActiveFunc}
      {...rest}
    />
  );
}

ParentMenuContainer.propTypes = {
  // `expandedParents` is the array of navKeys of the currently-expanded menu items
  expandedParents: PropTypes.arrayOf(
    PropTypes.string,
  ).isRequired,

  // `expandParent` should be called when this menu item is expanded or collapsed
  expandParent: PropTypes.func.isRequired,

  // `icon` is the name of the FontAwesome icon to render for the menu item
  icon: PropTypes.string.isRequired,

  // `label` is a property name in the message definitions (messages.js) for the label text
  label: PropTypes.string.isRequired,

  // `navExpanded` is true of the overall left nav menu is expanded
  navExpanded: PropTypes.bool,

  // `isActiveFunc` is a passthrough function
  isActiveFunc: PropTypes.func.isRequired,
};

ParentMenuContainer.defaultProps = {
  navExpanded: true,
  expandedParents: [],
};

function mapStateToProps(state) {
  return {
    navExpanded: leftNavExpandedSelector(state),
    expandedParents: leftNavExpandedParentsSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    expandParent: expanded => dispatch(expandLeftNavParent(expanded)),
  };
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(
    immutableToJS(ParentMenuContainer),
  ),
);
