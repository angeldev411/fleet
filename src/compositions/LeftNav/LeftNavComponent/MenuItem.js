import { omit } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { NavLink as UnstyledNavLink } from 'react-router-dom';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/**
 * LinkWithoutStyle removes the modifier and theme props from react-router-dom's `Link` component,
 * allowing us to use styled-components without prop errors in the console.
 */
function NavLinkWithoutStyleProps(props) {
  return <UnstyledNavLink {...omit(props, ['modifiers', 'theme'])} />;
}

const modifierConfig = {
  popover: ({ theme }) => ({
    styles: `
      line-height: ${theme.dimensions.leftNavChildItemHeight};
      padding: 0 ${px2rem(10)};
      border-radius: 2px;

      &:hover:not(.active) {
        text-decoration: underline;
        color: ${theme.colors.base.linkHover};
        background-color: ${theme.colors.base.chrome100};
      }
    `,
  }),
  singleLine: () => ({
    styles: `
      max-height: ${px2rem(42)};
    `,
  }),
  center: () => ({
    styles: `
      text-align: center;
    `,
  }),
  chrome200: () => ({
    styles: `
      background-color: #e6e7e8;
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.text};
  display: block;
  line-height: ${props.theme.dimensions.leftNavItemHeight};
  overflow: hidden;
  text-decoration: none;
  white-space: nowrap;

  &.active {
    color: ${props.theme.colors.base.chrome100};
    background-color: ${props.theme.colors.brand.primary};

    .menuIcon {
      color: ${props.theme.colors.base.chrome100};
    }
  }

  .menuIcon {
    font-size: ${px2rem(18)};
    text-align: center;
    width: ${px2rem(33)};
    color: ${props.theme.colors.base.chrome500};
  }

  .chevron {
    font-size: ${px2rem(10)};
    margin-right: ${px2rem(8)}
  }

  &:hover:not(.active) {
    background-color: ${props.theme.colors.base.chrome200};
    .menuIcon {
      color: ${props.theme.colors.base.text};
    }
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      chrome500: PropTypes.string.isRequired,
      chrome600: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
    brand: PropTypes.shape({
      primary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    leftNavChildItemHeight: PropTypes.string.isRequired,
    leftNavItemHeight: PropTypes.string.isRequired,
  }).isRequired,
};

const propTypes = {
  to: PropTypes.string.isRequired,
};

const defaultProps = {
  to: '#',
};

export default buildStyledComponent(
  'MenuItem',
  styled(NavLinkWithoutStyleProps),
  styles,
  { defaultProps, modifierConfig, propTypes, themePropTypes },
);
