import styled from 'styled-components';

const GhostIndicatorLeftNavWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export default GhostIndicatorLeftNavWrapper;
