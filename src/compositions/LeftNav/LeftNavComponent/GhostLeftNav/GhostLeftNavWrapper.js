import PropTypes from 'prop-types';
import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const GhostLeftNavWrapper = styled.div`
  margin: 0 ${props => (props.navExpanded ? px2rem(10) : px2rem(4))};
`;

GhostLeftNavWrapper.propTypes = {
  navExpanded: PropTypes.bool.isRequired,
};

export default GhostLeftNavWrapper;
