import PropTypes from 'prop-types';
import React from 'react';

import GhostIndicator from 'components/_common/GhostIndicator';

import GhostIndicatorLeftNavWrapper from './GhostIndicatorLeftNavWrapper';

function GhostIndicatorLeftNav({ navExpanded }) {
  return navExpanded ?
    <GhostIndicatorLeftNavWrapper>
      <GhostIndicator modifiers={['dark', 'expanded', 'smallWidth']} />
      <GhostIndicator modifiers={['dark', 'expanded', 'wide']} />
    </GhostIndicatorLeftNavWrapper>
    :
    <GhostIndicatorLeftNavWrapper>
      <GhostIndicator modifiers={['dark', 'wide']} />
    </GhostIndicatorLeftNavWrapper>;
}

GhostIndicatorLeftNav.propTypes = {
  navExpanded: PropTypes.bool.isRequired,
};

export default GhostIndicatorLeftNav;
