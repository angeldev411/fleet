import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import GhostIndicatorLeftNav from '../GhostIndicatorLeftNav';

const defaultProps = {
  navExpanded: false,
};

function renderComponent(props = defaultProps) {
  return shallow(<GhostIndicatorLeftNav {...props} />);
}

test('renders 2 GhostIndicator components if navExpanded', () => {
  const component = renderComponent({ navExpanded: true });
  expect(component.find('GhostIndicator').length).toEqual(2);
});

test('when navExpanded, the first GhostIndicator has smallWidth modifier', () => {
  const component = renderComponent({ navExpanded: true });
  const firstGhostIndicator = component.find('GhostIndicator').first();
  expect(firstGhostIndicator.props().modifiers).toInclude('smallWidth');
});

test('renders 1 GhostIndicator component if collapsed', () => {
  const component = renderComponent();
  expect(component.find('GhostIndicator').length).toEqual(1);
});
