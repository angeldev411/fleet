import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import GhostLeftNav from '../index';

const defaultProps = {
  navExpanded: false,
};

function renderComponent(props = defaultProps) {
  return shallow(<GhostLeftNav {...props} />);
}

test('renders 8 GhostIndicatorLeftNav components', () => {
  const component = renderComponent();
  expect(component.find('GhostIndicatorLeftNav').length).toEqual(8);
});
