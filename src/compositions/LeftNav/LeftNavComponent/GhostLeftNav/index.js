import React from 'react';
import PropTypes from 'prop-types';

import Divider from 'elements/Divider';

import GhostLeftNavWrapper from './GhostLeftNavWrapper';
import GhostIndicatorLeftNav from './GhostIndicatorLeftNav';

function buildGhostIndicators(num, navExpanded) {
  return Array.from(new Array(num), (x, i) => (
    <GhostIndicatorLeftNav key={i} navExpanded={navExpanded} />
  ));
}

function GhostLeftNav({ navExpanded }) {
  return (
    <GhostLeftNavWrapper navExpanded={navExpanded}>
      {buildGhostIndicators(1, navExpanded)}
      <Divider modifiers={['extraLightGray']} />
      {buildGhostIndicators(6, navExpanded)}
      <Divider modifiers={['extraLightGray']} />
      {buildGhostIndicators(1, navExpanded)}
      <Divider modifiers={['extraLightGray']} />
    </GhostLeftNavWrapper>
  );
}

GhostLeftNav.propTypes = {
  navExpanded: PropTypes.bool.isRequired,
};

export default GhostLeftNav;
