import { compact, noop, curry } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { withTheme } from 'styled-components';

import Popover, { PopoverTarget, PopoverContent, MessagePopover } from 'elements/Popover';

import messages from './messages';

import FavoritesPopover from './FavoritesPopover';
import GhostLeftNav from './GhostLeftNav';
import LeftNavExpander from './LeftNavExpander';
import LeftNavList from './LeftNavList';
import LeftNavWrapper from './LeftNavWrapper';
import NonParentMenu from './NonParentMenu';

import ParentMenuContainer from '../ParentMenuContainer';

function parentMenuGroup(menuGroup, isFinalGroup, leftNavIsExpanded, theme) {
  return menuGroup.map(({ label, icon, path, favorites }, index) => {
    // Overrides the default logic used by React Router's `NavLink` to determine if a
    // parent menu item is active.
    const parentMenuIsActive = curry((currentPath, match, { pathname }) =>
      pathname.startsWith(currentPath),
    );

    // include a border for last menu item in each group, except
    // for the final group in the menu
    const hasBorder = (index + 1 === menuGroup.length) && !isFinalGroup;

    const hasFavorites = favorites && favorites.length;

    const target = hasFavorites ?
      (<ParentMenuContainer
        key={path}
        label={label}
        icon={icon}
        path={path}
        favorites={favorites}
        border={hasBorder}
        isActiveFunc={parentMenuIsActive(path)}
      />) :
      (<NonParentMenu
        key={path}
        navExpanded={leftNavIsExpanded}
        label={label}
        icon={icon}
        path={path}
        border={hasBorder}
      />);

    if (leftNavIsExpanded) { return target; }

    const content = hasFavorites ?
      (<FavoritesPopover
        favorites={favorites}
        label={label}
        path={path}
        isActiveFunc={parentMenuIsActive(path)}
      />) :
      <MessagePopover><FormattedMessage {...messages[label]} /></MessagePopover>;

    return (
      <Popover
        key={path}
        showOnHover
        position="right"
        noOffset
        arrowVisible={false}
        bgColor={theme.colors.base.chrome100}
      >
        <PopoverTarget>
          {target}
        </PopoverTarget>
        <PopoverContent>
          {content}
        </PopoverContent>
      </Popover>
    );
  });
}

function parentMenus(menus, leftNavIsExpanded, theme) {
  if (!menus.length) { return <GhostLeftNav navExpanded={leftNavIsExpanded} />; }
  return menus.map((menuGroup, groupIndex) => {
    const isFinalGroup = groupIndex + 1 === menus.length;
    return parentMenuGroup(menuGroup, isFinalGroup, leftNavIsExpanded, theme);
  });
}

/**
 * The top-level component wrapper for the application left navigation menu.
 * `LeftNav` renders the top-level menu items and submenus for any favorites
 * specified in `menus`.
 */
export function LeftNav({
  menus,
  navExpanded,
  onExpandLeftNav,
  theme,
}) {
  return (
    <LeftNavWrapper id="left-nav" modifiers={compact([navExpanded && 'navExpanded'])}>
      <LeftNavList>
        {parentMenus(menus, navExpanded, theme)}
      </LeftNavList>
      <LeftNavExpander
        navExpanded={navExpanded}
        onExpandLeftNav={onExpandLeftNav}
      />
    </LeftNavWrapper>
  );
}

LeftNav.propTypes = {
  // `menus` defines the menu structure - see `LeftNavContainer/menuDefault.js` for details
  menus: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape())),

  // `navExpanded` is true if the left nav is navExpanded, false if it is collapsed
  navExpanded: PropTypes.bool,

  // `onExpandLeftNav` is called by the expander when expanding or collapsing the left nav
  onExpandLeftNav: PropTypes.func,

  theme: PropTypes.shape({
    colors: PropTypes.shape({
      base: PropTypes.shape({
        chrome100: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};

LeftNav.defaultProps = {
  menus: [],
  navExpanded: true,
  onExpandLeftNav: noop,
};

export default withTheme(LeftNav);
