import { compact } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage } from 'react-intl';
import SmoothCollapse from 'react-smooth-collapse';
import {
  Container,
  Column,
  Row,
} from 'styled-components-reactive-grid';

import TextDiv from 'elements/TextDiv';

import messages from './messages';

import MenuItem from './MenuItem';
import MenuItemWrapper from './MenuItemWrapper';
import SubMenu from './SubMenu';
import SubMenuItem from './SubMenuItem';


/**
 * Builds the sub menu items for the favorites supplied.
 * @param  {Collection} favorites An array of favorites objects
 * @return {Collection}           An array of sub menu item components
 */
function subMenuItems(favorites) {
  return favorites.map(({ message, path, slug }) => (
    <MenuItemWrapper key={slug}>
      <SubMenuItem id={`menu-item-${message.id.split('.').pop()}`} to={path}>
        <FormattedMessage {...message} />
      </SubMenuItem>
    </MenuItemWrapper>
  ));
}

/**
 * A ParentMenu is a top level menu item that has a submenu (for the list of favorites).
 */
function ParentMenu({
  border,
  expanded,
  favorites,
  icon,
  label,
  navExpanded,
  navKey,
  onExpandParent,
  isActiveFunc,
}) {
  // Handles click events on parent menu items.
  function parentMenuItemClickHandler() {
    // toggle the expanded state of this menu item & its children
    return onExpandParent({ key: navKey, expanded: !expanded });
  }

  return (
    <MenuItemWrapper modifiers={compact([border && 'border'])}>
      <MenuItem
        isActive={isActiveFunc}
        onClick={parentMenuItemClickHandler}
        modifiers={['singleLine']}
      >
        <Container>
          <Row modifiers={['middle']}>
            <Column modifiers={[navExpanded ? 'col_2' : 'col', 'start']}>
              {icon && <FontAwesome name={icon} className="menuIcon" />}
            </Column>
            {navExpanded &&
              <Column modifiers={['col']}>
                <TextDiv modifiers={['smallText', 'uppercase']}>
                  <FormattedMessage {...messages[label]} />
                </TextDiv>
              </Column>
            }
            {navExpanded &&
              <Column modifiers={['end', 'col']}>
                <TextDiv modifiers={['textAlignRight']}>
                  <FontAwesome name={expanded ? 'chevron-up' : 'chevron-down'} className="chevron" />
                </TextDiv>
              </Column>
            }
          </Row>
        </Container>
      </MenuItem>
      <SmoothCollapse expanded={expanded}>
        <SubMenu navExpanded={navExpanded}>
          {subMenuItems(favorites)}
        </SubMenu>
      </SmoothCollapse>
    </MenuItemWrapper>
  );
}

ParentMenu.propTypes = {
  // `border` is true if this menu item should include a bottom border
  border: PropTypes.bool.isRequired,

  // `expanded` is a boolean indicating if the submenu should be displayed.
  expanded: PropTypes.bool,

  // `favorites` is an array of the submenu item definitions
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      message: PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired,
      }).isRequired,
      path: PropTypes.string.isRequired,
      slug: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    }),
  ).isRequired,

  // `icon` is the name of the FontAwesome icon to render for the menu item
  icon: PropTypes.string.isRequired,

  // `label` is a property name in the message definitions (messages.js) for the label text
  label: PropTypes.string.isRequired,

  // `navExpanded` is true if the left nav is expanded, false if not
  navExpanded: PropTypes.bool.isRequired,

  // `navKey` is the in-app ID for the parent menu
  navKey: PropTypes.string.isRequired,

  // `onExpandParent` is called when the parent menu item is clicked
  onExpandParent: PropTypes.func.isRequired,

  // `isActiveFunc` is now being passed down, for shareability
  isActiveFunc: PropTypes.func.isRequired,

};

ParentMenu.defaultProps = {
  expanded: false,
  favorites: [],
};


export default ParentMenu;
