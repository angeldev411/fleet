import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/**
 * A SubMenu is the unordered list of favorites links under a parent menu item.
 */
/* istanbul ignore next */
const styles = props => `
  background: ${props.theme.colors.base.chrome100};
  border-bottom: 0.8px solid ${props.theme.colors.base.chrome200};
  font-size: ${px2rem(11)};
  list-style: none;
  margin: 0;
  padding: 0;

  /* add a little extra padding above & below the submenu items: */
  li:first-child a {
    margin-top: ${px2rem(6)};
  }
  li:last-child a {
    margin-bottom: ${px2rem(6)};
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'SubMenu',
  styled.ul,
  styles,
  { themePropTypes },
);
