import { defineMessages } from 'react-intl';

const messages = defineMessages({
  administration: {
    id: 'compositions.LeftNav.LeftNavComponent.parentMenu.administration',
    defaultMessage: 'Administration',
  },
  approvals: {
    id: 'compositions.LeftNav.LeftNavComponent.parentMenu.approvals',
    defaultMessage: 'Approvals',
  },
  assets: {
    id: 'compositions.LeftNav.LeftNavComponent.parentMenu.assets',
    defaultMessage: 'Assets',
  },
  cases: {
    id: 'compositions.LeftNav.LeftNavComponent.parentMenu.cases',
    defaultMessage: 'Cases',
  },
  collapsed: {
    id: 'compositions.LeftNav.LeftNavComponent.parentMenu.collapsed',
    defaultMessage: 'Collapse Sidebar',
  },
  expanded: {
    id: 'compositions.LeftNav.LeftNavComponent.parentMenu.expanded',
    defaultMessage: 'Expand Sidebar',
  },
  reports: {
    id: 'compositions.LeftNav.LeftNavComponent.parentMenu.reports',
    defaultMessage: 'Reports',
  },
  serviceRequests: {
    id: 'compositions.LeftNav.LeftNavComponent.parentMenu.serviceRequests',
    defaultMessage: 'Service Requests',
  },
});

export default messages;
