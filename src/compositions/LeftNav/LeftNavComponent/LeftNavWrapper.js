import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

const modifierConfig = {
  navExpanded: ({ theme }) => ({
    styles: `
      overflow: visible;
      width: ${theme.dimensions.leftNavWidth};
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  background-color: ${props.theme.colors.base.chrome100};
  border-right: 1px solid ${props.theme.colors.base.chrome300};
  height: 100%;
  left: 0;
  width: ${props.theme.dimensions.leftNavWidthCollapsed};
  padding-top: ${props.theme.dimensions.gridPad};
  position: fixed;
  top: 80px;
  transition: width ${props.theme.dimensions.leftNavExpandTime};
  z-index: 2;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    gridPad: PropTypes.string.isRequired,
    leftNavWidth: PropTypes.string.isRequired,
    leftNavWidthCollapsed: PropTypes.string.isRequired,
    leftNavExpandTime: PropTypes.string.isRequired,
  }).isRequired,
};

const propTypes = {
  navExpanded: PropTypes.bool.isRequired,
};

const defaultProps = {
  navExpanded: true,
};

export default buildStyledComponent(
  'LeftNavWrapper',
  styled.section,
  styles,
  { defaultProps, modifierConfig, propTypes, themePropTypes },
);
