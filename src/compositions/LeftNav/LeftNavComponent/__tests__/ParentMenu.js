import { noop } from 'lodash';
import React from 'react';

import { test, expect, shallow, createSpy } from '__tests__/helpers/test-setup';

import ParentMenu from '../ParentMenu';
import messages from '../messages';

const defaultLabel = Object.keys(messages)[0];

const icon = 'icon';

const defaultProps = {
  border: false,
  expanded: false,
  favorites: [],
  icon,
  label: defaultLabel,
  navExpanded: true,
  navKey: `${defaultLabel}-${icon}`,
  onExpandParent: noop,
  path: 'path',
  isActiveFunc: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<ParentMenu {...props} />);
}

test('ParentMenu renders the given icon', () => {
  const iconName = 'fancy-icon';
  const component = shallowRender({ ...defaultProps, icon: iconName });
  const fontAwesome = component.find('MenuItem FontAwesome.menuIcon');
  expect(fontAwesome).toHaveProp('name', iconName);
});

test('NonParentMenu renders a down-pointing chevron icon when collapsed', () => {
  const component = shallowRender();
  const chevron = component.find('FontAwesome.chevron');
  expect(chevron).toHaveProp('name', 'chevron-down');
});

test('NonParentMenu renders an up-pointing chevron icon when expanded', () => {
  const component = shallowRender({ ...defaultProps, expanded: true });
  const chevron = component.find('FontAwesome.chevron');
  expect(chevron).toHaveProp('name', 'chevron-up');
});

test('ParentMenu renders the menu item text when the nav menu is expanded', () => {
  const component = shallowRender({ ...defaultProps, expanded: true });
  const message = component.find('MenuItem FormattedMessage');
  expect(message).toHaveProps({
    id: messages[defaultLabel].id,
    defaultMessage: messages[defaultLabel].defaultMessage,
  });
});

test('ParentMenu does not render the menu item text when the nav menu is collapsed', () => {
  const component = shallowRender({ ...defaultProps, navExpanded: false });
  expect(component).toNotContain('MenuItem FormattedMessage');
});

test('ParentMenu renders a smooth-collapse SubMenu', () => {
  const component = shallowRender();
  expect(component).toContain('SmoothCollapse SubMenu');
});

test('ParentMenu passes parent expanded state to SmoothCollapse', () => {
  const expanded = shallowRender({ ...defaultProps, expanded: true });
  expect(expanded.find('SmoothCollapse')).toHaveProp('expanded', true);

  const collapsed = shallowRender();
  expect(collapsed.find('SmoothCollapse')).toHaveProp('expanded', false);
});

test('ParentMenu passes nav menu expanded state to SubMenu', () => {
  const expanded = shallowRender({ ...defaultProps, navExpanded: true });
  expect(expanded.find('SubMenu')).toHaveProp('navExpanded', true);

  const collapsed = shallowRender({ ...defaultProps, navExpanded: false });
  expect(collapsed.find('SubMenu')).toHaveProp('navExpanded', false);
});

const testFavorites = [
  {
    title: 'allRepairCases',
    path: '/allRepairCases',
    message: {
      id: 'allRepairCases',
      defaultMessage: 'All Repair Cases',
    },
    slug: 'fav1',
  },
  {
    title: 'downtime2Days',
    path: '/downtime2Days',
    message: {
      id: 'downtime2Days',
      defaultMessage: 'Downtime > 2 Days',
    },
    slug: 'fav2',
  },
  {
    title: 'pastDueFollowUp',
    path: '/pastDueFollowUp',
    message: {
      id: 'pastDueFollowUp',
      defaultMessage: 'Past Due Follow Up',
    },
    slug: 'fav3',
  },
];

test('ParentMenu renders one SubMenuItem for each favorite', () => {
  const component = shallowRender({ ...defaultProps, favorites: testFavorites });
  const submenuItems = component.find('SubMenuItem');
  expect(submenuItems.length).toEqual(testFavorites.length);
});

test('ParentMenu passes the rendered favorite labels to the submenus', () => {
  const component = shallowRender({ ...defaultProps, favorites: testFavorites });
  const submenu = component.find('SubMenu');
  testFavorites.forEach((favorite, i) => {
    const submenuLabel = submenu.childAt(i).find('MenuItemWrapper SubMenuItem');
    expect(submenuLabel).toContain('FormattedMessage');
    expect(submenuLabel.find('FormattedMessage')).toHaveProps({ ...messages[favorite.title] });
  });
});

test('ParentMenu expand/collapse parent when MenuItem is clicked', () => {
  const expandSpy = createSpy();
  const rendered = shallowRender({
    ...defaultProps,
    favorites: testFavorites,
    onExpandParent: expandSpy,
  });
  const menuItem = rendered.find('MenuItem').first();
  menuItem.simulate('click');
  expect(expandSpy).toHaveBeenCalled();
});
