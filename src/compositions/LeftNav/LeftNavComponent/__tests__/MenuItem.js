import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import MenuItem from '../MenuItem';

const testChild = <span>TEST</span>;

const defaultProps = {
  to: '/test_path',
  children: testChild,
};

function shallowRender(props = defaultProps) {
  return shallow(<MenuItem {...props} />);
}

test('MenuItem renders a NavLinkWithoutStyleProps with expected props if to is provided', () => {
  const component = shallowRender();
  expect(component).toContain('NavLinkWithoutStyleProps');
  expect(component.find('NavLinkWithoutStyleProps')).toHaveProps(defaultProps);
});

test('MenuItem provides default prop to=#" to MenuItemLink', () => {
  const testProps = {
    children: testChild,
  };
  const component = shallowRender(testProps);
  expect(component).toContain('NavLinkWithoutStyleProps');
  expect(component.find('NavLinkWithoutStyleProps')).toHaveProp('to', '#');
});
