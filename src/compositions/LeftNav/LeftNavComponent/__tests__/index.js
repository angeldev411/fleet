import React from 'react';
import { noop } from 'lodash';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import { withTestTheme } from 'utils/styles';

import { LeftNav as LeftNavWithoutTheme } from '../index';

import ParentMenuContainer from '../../ParentMenuContainer';

const LeftNav = withTestTheme(LeftNavWithoutTheme);
const menus = [
  [
    {
      label: 'service request',
      icon: 'icon-one',
      path: '/one',
      favorites: [
        {
          message: { id: '1', defaultMessage: '1' },
          path: '/fav2',
          slug: 'fav2',
          title: 'fav2',
        },
      ],
    },
    {
      label: 'cases',
      icon: 'icon-two',
      path: '/two',
    },
  ],
  [
    {
      label: 'approvals',
      icon: 'icon-three',
      path: '/three',
      favorites: [
        {
          slug: 'fav3',
          title: 'fav3',
        },
      ],
    },
  ],
  [
    {
      label: 'assets',
      icon: 'icon-four',
      path: '/four',
    },
  ],
];

const defaultProps = {
  navExpanded: true,
  menus,
  onExpandLeftNav: noop,
  isActiveFunc: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<LeftNav {...props} />);
}

test('LeftNav renders GhostLeftNav if there is no menu items', () => {
  const navExpanded = false;
  const leftNav = shallowRender({ ...defaultProps, navExpanded, menus: [] });
  expect(leftNav).toContain('GhostLeftNav');

  // also passes down the navExpanded prop
  expect(leftNav.find('GhostLeftNav')).toHaveProps({ navExpanded });
});

test('LeftNav renders one list item per menu item plus the expander', () => {
  const leftNav = shallowRender();
  const list = leftNav.find('LeftNavList');
  expect(list.children().length).toEqual(menus.length + 1);
});

test('LeftNav renders the expander at the end', () => {
  const leftNav = shallowRender();
  expect(leftNav.children().last()).toBeA('WithTheme(LeftNavExpander)');
});

test('LeftNav renders ParentMenuContainer for items with favorites', () => {
  const leftNav = shallowRender();
  const list = leftNav.find('LeftNavList');
  // have to use the actual ParentMenuContainer component here and not just the
  // name since the container is wrapped in HOCs:
  expect(list.childAt(0)).toContain(ParentMenuContainer);
  expect(list.childAt(2)).toContain(ParentMenuContainer);
});

test('LeftNav renders NonParentMenu for items without favorites', () => {
  const leftNav = shallowRender();
  const list = leftNav.find('LeftNavList');
  expect(list.childAt(1)).toContain('NonParentMenu');
  expect(list.childAt(3)).toContain('NonParentMenu');
});

test('LeftNav sets border=true for the last item in each group (except the last group)', () => {
  const leftNav = shallowRender();
  const list = leftNav.find('LeftNavList');
  expect(list.childAt(0).find(ParentMenuContainer)).toHaveProp('border', false);
  expect(list.childAt(1).find('NonParentMenu')).toHaveProp('border', true);
  expect(list.childAt(2).find(ParentMenuContainer)).toHaveProp('border', true);
  expect(list.childAt(3).find('NonParentMenu')).toHaveProp('border', false);
});
