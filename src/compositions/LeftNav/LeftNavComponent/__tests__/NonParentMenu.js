import React from 'react';
import { noop } from 'lodash';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import NonParentMenu from '../NonParentMenu';
import messages from '../messages';

const defaultLabel = Object.keys(messages)[0];


const defaultProps = {
  border: false,
  icon: 'icon',
  label: defaultLabel,
  navExpanded: true,
  path: 'path',
  isActiveFunc: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<NonParentMenu {...props} />);
}

test('NonParentMenu renders a MenuItemWrapper', () => {
  const component = shallowRender();
  expect(component).toContain('MenuItemWrapper');
});

// test('NonParentMenu passes the border to the MenuItemWrapper', () => {
//   const component = shallowRender({ border: true });
//   const wrapper = component.find('MenuItemWrapper');
//   expect(wrapper).toHaveProp('border', true);
// });
//
// test('NonParentMenu renders the given icon', () => {
//   const iconName = 'fancy-icon';
//   const component = shallowRender({ icon: iconName });
//   const icon = component.find('MenuItemWrapper FontAwesome.menuIcon');
//   expect(icon).toHaveProp('name', iconName);
// });
//
// test('NonParentMenu does not render the chevron icon', () => {
//   const component = shallowRender();
//   expect(component).toNotContain('FontAwesome.chevron');
// });
//
// test('NonParentMenu renders the menu item text when the nav menu is expanded', () => {
//   const component = shallowRender({ navExpanded: true, label: defaultLabel });
//   const message = component.find('FormattedMessage');
//   expect(message).toHaveProps({
//     id: messages[defaultLabel].id,
//     defaultMessage: messages[defaultLabel].defaultMessage,
//   });
// });
//
// test('NonParentMenu does not render the menu item text when the nav menu is collapsed', () => {
//   const component = shallowRender({ navExpanded: false, label: defaultLabel });
//   expect(component).toNotContain('FormattedMessage');
// });
//
// test('NonParentMenu does not render a SubMenu', () => {
//   const component = shallowRender();
//   expect(component).toNotContain('SubMenu');
// });
