import { compact, noop } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import {
  Column,
  Container,
  Row,
} from 'styled-components-reactive-grid';
import { withTheme } from 'styled-components';
import { px2rem, theme as defaultTheme } from 'decisiv-ui-utils';

import Tooltip from 'elements/Tooltip';
import TextDiv from 'elements/TextDiv';

import messages from '../messages';

import ExpanderMenuWrapper from './ExpanderMenuWrapper';

import MenuItem from '../MenuItem';

export function LeftNavExpander({
  navExpanded,
  onExpandLeftNav,
  theme,
}) {
  const icon = navExpanded ? 'angle-double-left' : 'angle-double-right';
  const { base } = theme.colors;

  const onExpand = (e) => {
    e.preventDefault();
    onExpandLeftNav(!navExpanded);
  };

  const styleIconBorder = () => {
    if (navExpanded) { return { borderLeft: `${px2rem(1)} ${base.chrome300} solid` }; }
    return { borderTop: `${px2rem(1)} ${base.chrome300} solid` };
  };

  return (
    <ExpanderMenuWrapper id="expander-menu-wrapper">
      <Container modifiers={['fluid']}>
        <Row modifiers={compact([!navExpanded && 'center'])}>
          <Column modifiers={[navExpanded ? 'col_2' : 'col_12']}>
            <Tooltip
              message={messages.administration}
              position="right"
            >
              <MenuItem
                to="/admin"
                modifiers={['center', 'chrome200']}
                style={{ borderRadius: '2px' }}
              >
                <FontAwesome name="cog" />
              </MenuItem>
            </Tooltip>
          </Column>
          <Column modifiers={[navExpanded ? 'col' : 'noDisplay', 'fluid']} />
          <Column
            modifiers={[navExpanded ? 'col_2' : 'col_12', 'fluid']}
            style={styleIconBorder()}
          >
            <Tooltip
              message={messages[`${navExpanded ? 'collapsed' : 'expanded'}`]}
              position="right"
            >
              <MenuItem to="#" onClick={onExpand}>
                <TextDiv modifiers={['textAlignCenter']}>
                  <FontAwesome name={icon} />
                </TextDiv>
              </MenuItem>
            </Tooltip>
          </Column>
        </Row>
      </Container>
    </ExpanderMenuWrapper>
  );
}

LeftNavExpander.propTypes = {
  // `navExpanded` is true if left nav is navExpanded, false if it is collapsed
  navExpanded: PropTypes.bool.isRequired,

  // `onExpandLeftNav` is called when the expander is clicked
  onExpandLeftNav: PropTypes.func.isRequired,

  theme: PropTypes.shape({
    colors: PropTypes.shape({
      base: PropTypes.shape({
        chrome000: PropTypes.string,
        chrome200: PropTypes.string,
        chrome300: PropTypes.string,
        chrome700: PropTypes.string,
      }),
    }).isRequired,
  }).isRequired,
};

LeftNavExpander.defaultProps = {
  navExpanded: true,
  onExpandLeftNav: noop,
  theme: defaultTheme,
};

export default withTheme(LeftNavExpander);
