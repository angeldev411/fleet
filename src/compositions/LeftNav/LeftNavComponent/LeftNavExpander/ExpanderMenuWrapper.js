import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

// `ExpanderMenuWrapper` sticks the expander menu item to the bottom of the left nav and
// adds a border to the top of it.
/* istanbul ignore next */
const styles = props => `
  background: ${props.theme.colors.base.chrome100};
  border-top: 1px solid ${props.theme.colors.base.chrome300};
  bottom: 84px;
  left: 0;
  list-style: none;
  position: absolute;
  right: 0;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'ExpanderMenuWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
