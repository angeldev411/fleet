import React from 'react';
import { noop } from 'lodash';

import { test, expect, shallow, createSpy } from '__tests__/helpers/test-setup';

import { withTestTheme } from 'utils/styles';

import { LeftNavExpander } from '../index';
import messages from '../../messages';

const LeftNavExpanderWithTheme = withTestTheme(LeftNavExpander);

const defaultProps = {
  navExpanded: true,
  onExpandLeftNav: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<LeftNavExpanderWithTheme {...props} />);
}

test('LeftNavExpander renders a MenuItemLink with onClick=onExpandLeftNav', () => {
  const handler = createSpy();
  const preventDefault = createSpy();
  const clickEvent = {
    preventDefault,
  };
  // clicking when collapsed calls handler with 'true' to toggle expanded state...
  const collapsed = shallowRender({ navExpanded: false, onExpandLeftNav: handler });
  const collapsedIcon = collapsed.find('MenuItem').at(1);
  collapsedIcon.simulate('click', clickEvent);
  expect(preventDefault).toHaveBeenCalled();
  expect(handler).toHaveBeenCalledWith(true);

  // clicking when expanded calls handler with 'false' to toggle expanded state...
  const expanded = shallowRender({ navExpanded: true, onExpandLeftNav: handler });
  const expandedIcon = expanded.find('MenuItem').at(1);
  expandedIcon.simulate('click', clickEvent);
  expect(preventDefault).toHaveBeenCalled();
  expect(handler).toHaveBeenCalledWith(false);
});

test('LeftNavExpander renders a left-pointing icon when expanded', () => {
  const expanded = shallowRender({ ...defaultProps, navExpanded: true });
  expect(expanded.find('FontAwesome').at(1)).toHaveProp('name', 'angle-double-left');
});

test('LeftNavExpander passes correct message prop when expanded', () => {
  const expanded = shallowRender({ ...defaultProps, navExpanded: true });
  const tooltip = expanded.find('Tooltip').at(1);
  expect(tooltip).toHaveProp('message', messages.collapsed);
});

test('LeftNavExpander renders a right-pointing icon when collapsed', () => {
  const expanded = shallowRender({ ...defaultProps, navExpanded: false });
  expect(expanded.find('FontAwesome').at(1)).toHaveProp('name', 'angle-double-right');
});

test('LeftNavExpander passes correct message prop when collapsed', () => {
  const expanded = shallowRender({ ...defaultProps, navExpanded: false });
  const tooltip = expanded.find('Tooltip').at(1);
  expect(tooltip).toHaveProp('message', messages.expanded);
});
