import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const styles = props => `
  font-size: ${px2rem(12)};
  font-weight: 400;
  list-style: none;
  margin-bottom: ${props.theme.dimensions.leftNavItemHeight};
  margin: 0;
  padding: 0;
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    leftNavItemHeight: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'LeftNavList',
  styled.ul,
  styles,
  { themePropTypes },
);
