import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { compact } from 'lodash';
import { withRouter } from 'react-router-dom';

import TextDiv from 'elements/TextDiv';

import Wrapper from './Wrapper';
import Header from './Header';
import Body from './Body';

import MenuItem from '../MenuItem';
import messages from '../messages';

function favoriteLinks(favorites, hidePopover) {
  return favorites.map(({ message, path, slug }) => (
    <MenuItem key={slug} modifiers={['popover']} to={path} onClick={hidePopover}>
      <FormattedMessage {...message} />
    </MenuItem>
  ));
}

export function FavoritesPopover({
  favorites,
  hidePopover,
  label,
  location,
  match,
  isActiveFunc,
}) {
  const isActive = isActiveFunc(match, location);
  const headerModifiers = compact([isActive && 'active']);
  const textModifiers = compact([!isActive && 'bold', 'uppercase']);

  return (
    <Wrapper>
      <Header modifiers={headerModifiers}>
        <TextDiv modifiers={textModifiers}>
          <FormattedMessage {...messages[label]} />
        </TextDiv>
      </Header>
      <Body>
        {favoriteLinks(favorites, hidePopover)}
      </Body>
    </Wrapper>
  );
}

FavoritesPopover.propTypes = {
  // `favorites` is an array of the submenu item definitions
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      message: PropTypes.shape({
        id: PropTypes.string.isRequired,
        defaultMessage: PropTypes.string.isRequired,
      }).isRequired,
      path: PropTypes.string.isRequired,
      slug: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    }),
  ),

  // `hidePopover` is a function called when a submenu item clicked
  hidePopover: PropTypes.func,

  // `label` is a property name in the message definitions (messages.js) for the label text
  label: PropTypes.string.isRequired,

  // `isActiveFunc` is a function passed down to determine if the
  // FavoritesPopoverHeader should reflect an actively selected status
  isActiveFunc: PropTypes.func.isRequired,

  match: PropTypes.shape({
    params: PropTypes.shape({
      eventId: PropTypes.number,
    }).isRequired,
  }).isRequired,

  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};

FavoritesPopover.defaultProps = {
  favorites: [],
  hidePopover: () => {},
};

export default withRouter(FavoritesPopover);
