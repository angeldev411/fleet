import styled from 'styled-components';
import { Container } from 'styled-components-reactive-grid';
import { buildStyledComponent } from 'decisiv-ui-utils';

export default buildStyledComponent(
  'FavoritesPopoverBody',
  styled(Container),
);
