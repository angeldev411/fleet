import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const styles = `
  line-height: ${px2rem(22)};
`;

export default buildStyledComponent(
  'FavoritesPopoverWrapper',
  styled.div,
  styles,
);
