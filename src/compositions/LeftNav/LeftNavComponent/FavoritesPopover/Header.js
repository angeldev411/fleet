import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  active: ({ theme }) => ({
    styles: `
      background-color: ${theme.colors.brand.primary};
      color: ${theme.colors.base.chrome100};
    `,
  }),
};

const styles = ({ theme }) => `
  background-color: ${theme.colors.base.chrome200};
  padding: ${px2rem(12)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
    }).isRequired,
    brand: PropTypes.shape({
      primary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'FavoritesPopoverHeader',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
