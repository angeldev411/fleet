import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';
import { noop } from 'lodash';

import { FavoritesPopover } from '../index';

const defaultProps = {
  favorites: [],
  hidePopover: noop,
  label: 'cases',
  path: '',
  location: {
    pathname: 'test',
  },
  isActiveFunc: noop,
  match: {
    params: {
      eventId: 1,
    },
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<FavoritesPopover {...props} />);
}

const simpleFavorites = [
  {
    message: {
      id: 'allRepairCases',
      defaultMessage: 'All Repair Cases',
    },
    path: '/allRepairCases',
    slug: 'fav1',
    title: 'allRepairCases',
  },
  {
    message: {
      id: 'downtime2Days',
      defaultMessage: 'Downtime > 2 Days',
    },
    path: '/downtime2Days',
    slug: 'fav2',
    title: 'downtime2Days',
  },
  {
    message: {
      id: 'pastDueFollowUp',
      defaultMessage: 'Past Due Follow Up',
    },
    path: '/pastDueFollowUp',
    slug: 'fav3',
    title: 'pastDueFollowUp',
  },
];

test('FavoritesPopover renders the correct number of favorites', () => {
  const wrapper = shallowRender({ ...defaultProps, favorites: simpleFavorites });
  expect(wrapper.find('MenuItem').length).toEqual(simpleFavorites.length);
});

test('FavoritesPopover passes down hidePopover to FavoriteLinkWrapper', () => {
  const onHide = () => {};
  const wrapper = shallowRender({
    ...defaultProps,
    favorites: simpleFavorites,
    hidePopover: onHide,
  });
  const component = wrapper.find('MenuItem').first();
  expect(component).toHaveProp('onClick', onHide);
});
