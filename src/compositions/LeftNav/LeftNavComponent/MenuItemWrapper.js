/**
 * MenuItemWrapper is the list item for an individual LeftNav menu item.  It is
 * used for both top level (parent or non-parent) menu items and also for sub-menu
 * items (favorites). It removes the default list item padding and margins and
 * applies a bottom border if the modifier `border` is applied.
 */

import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  /**
   * This emits CSS for a partial-width bottom border.  It
   * is actually a "fake" border using :after and absolute positioning.
   */
  border: ({ theme }) => ({
    styles: `
      &:after {
        background: ${theme.colors.base.chrome200};
        bottom: 0;
        content: "";
        height: 0.8px;
        left: ${px2rem(16)};
        right: ${px2rem(16)};
        position: absolute;
      }
    `,
  }),
};

/* istanbul ignore next */
const styles = () => `
  margin: 0;
  padding: 0;
  position: relative;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome200: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'MenuItemWrapper',
  styled.li,
  styles,
  { modifierConfig, themePropTypes },
);
