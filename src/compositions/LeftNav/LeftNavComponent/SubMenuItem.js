import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import MenuItem from './MenuItem';

/**
 * SubMenuItem applies padding and line height to the submenu items (favorites).
 */
/* istanbul ignore next */
const styles = props => `
  line-height: ${props.theme.dimensions.leftNavChildItemHeight};
  padding-left: ${px2rem(42)};

  &:hover:not(.active) {
    text-decoration: underline;
    color: ${props.theme.colors.base.linkHover};
    background: ${props.theme.colors.base.chrome100};
  }

  &.active {
    margin-left: ${px2rem(6)};
    padding-left: ${px2rem(36)};
    border-radius: 2px;
    width: 77%;
  }
`;

const themePropTypes = {
  dimensions: PropTypes.shape({
    leftNavChildItemHeight: PropTypes.string.isRequired,
  }),
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'SubMenuItem',
  styled(MenuItem),
  styles,
  { themePropTypes },
);
