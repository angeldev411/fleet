import { compact } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage } from 'react-intl';
import {
  Container,
  Column,
  Row,
} from 'styled-components-reactive-grid';

import TextDiv from 'elements/TextDiv';

import messages from './messages';

import MenuItemWrapper from './MenuItemWrapper';
import MenuItem from './MenuItem';

/**
 * A NonParentMenu is a top level menu item that does not have favorites (so, it does
 * not have a submenu).
 */
function NonParentMenu({
  border,
  icon,
  label,
  navExpanded,
  path,
}) {
  return (
    <MenuItemWrapper modifiers={compact([border && 'border'])}>
      <MenuItem
        to={path}
        modifiers={['singleLine']}
      >
        <Container>
          <Row modifiers={['middle']}>
            <Column modifiers={[navExpanded ? 'col_2' : 'col', 'start']}>
              {icon && <FontAwesome name={icon} className="menuIcon" />}
            </Column>
            {navExpanded &&
              <Column modifiers={['col']}>
                <TextDiv modifiers={['smallText', 'uppercase']}>
                  <FormattedMessage {...messages[label]} />
                </TextDiv>
              </Column>
            }
          </Row>
        </Container>

      </MenuItem>
    </MenuItemWrapper>
  );
}

NonParentMenu.propTypes = {
  // `border` is true if this menu item should include a bottom border
  border: PropTypes.bool.isRequired,

  // `icon` is the name of the FontAwesome icon to render for the menu item
  icon: PropTypes.string.isRequired,

  // `label` is a property name in the message definitions (messages.js) for the label text
  label: PropTypes.string.isRequired,

  // `navExpanded` is true if the left nav is expanded, false if not
  navExpanded: PropTypes.bool.isRequired,

  // `path` is the route path for this menu item's link
  path: PropTypes.string.isRequired,

};

export default NonParentMenu;
