import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withTheme } from 'styled-components';

import WindowQuery from 'components/WindowQuery';

import { loadDashboard as loadDashboardAction } from 'redux/app/actions';
import {
  dashboardRequestingSelector,
  favoritesSelector,
} from 'redux/app/selectors';

import { setLeftNavExpanded as setLeftNavExpandedAction } from 'redux/ui/actions';
import { leftNavExpandedSelector } from 'redux/ui/selectors';

import immutableToJS from 'utils/immutableToJS';

import LeftNav from './LeftNavComponent';
import menuDefault from './menuDefault';

/**
 * Processes the user's favorites from the dashboard api response into menu groups for display.
 * @param  {Array} menuGroups The default menu groups arrays
 * @param  {Object} favorites The user's favorites, keyed under menu group labels
 * @return {Array}            The processed array, with the user's favorites in the correct groups.
 */
export function composeMenus(menuGroups, favorites) {
  if (isEmpty(favorites)) { return []; }
  return (
    menuGroups.map(menuGroup => menuGroup.map((menu) => {
      const menuFavorites = favorites[menu.label];
      if (menuFavorites) {
        return { ...menu, favorites: menuFavorites };
      }
      return menu;
    }))
  );
}

/**
 * LeftNavContainer wraps the entire left-hand navigation panel for the application.
 * It is rendered by the Layout.
 */
export class LeftNavContainer extends Component {
  static propTypes = {
    // `dashboardRequesting` is true if a request is currently ongoing.
    dashboardRequesting: PropTypes.bool.isRequired,

    // `favorites` is the collection of favorites for all the menu items
    favorites: PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          path: PropTypes.string.isRequired,
          slug: PropTypes.string.isRequired,
          title: PropTypes.string.isRequired,
        }),
      ),
    ).isRequired,

    // `loadDashboard` triggers loading the user's favorites from the api
    loadDashboard: PropTypes.func.isRequired,

    // `navExpanded` is true if the left nav is currently navExpanded, false otherwise
    navExpanded: PropTypes.bool,

    // `setLeftNavExpanded` allows saving the user's left nav navExpanded preference
    setLeftNavExpanded: PropTypes.func.isRequired,

    theme: PropTypes.shape({
      dimensions: PropTypes.shape({
        autoCollapseLeftNavPx: PropTypes.number.isRequired,
      }).isRequired,
    }).isRequired,
  };

  static defaultProps = {
    dashboardRequesting: false,
    navExpanded: true,
  };

  state = {
    // `requestedNavExpanded` tracks whether the user has *manually* expanded a collapsed left-nav
    // (contrasted with an auto-expand due to a window resize)
    requestedNavExpanded: true,
  };

  componentWillMount() {
    if (isEmpty(this.props.favorites) && !this.props.dashboardRequesting) {
      this.props.loadDashboard();
    }
  }

  componentDidMount() {
    if (window.innerWidth < this.props.theme.dimensions.autoCollapseLeftNavPx) {
      this.props.setLeftNavExpanded(false);
    }
  }

  onResizeWindow = ({ windowWidth }) => {
    const { navExpanded, setLeftNavExpanded, theme } = this.props;
    const collapseAt = theme.dimensions.autoCollapseLeftNavPx;
    if (navExpanded && (windowWidth < collapseAt)) {
      // collapse the left nav when shrinking the window below the configured width
      setLeftNavExpanded(false);
    } else if (this.state.requestedNavExpanded && !navExpanded && (windowWidth > collapseAt)) {
      // only auto-expand the left nav is resizing to make the window larger *and*
      // the left nav had previously been manually navExpanded
      setLeftNavExpanded(true);
    }
  }

  handleExpandLeftNavClick = (navExpanded) => {
    this.props.setLeftNavExpanded(navExpanded);
    this.setState({ requestedNavExpanded: navExpanded });
  }

  render() {
    const { navExpanded } = this.props;

    return (
      <WindowQuery onResize={this.onResizeWindow} >
        <LeftNav
          menus={composeMenus(menuDefault, this.props.favorites)}
          navExpanded={navExpanded}
          onExpandLeftNav={this.handleExpandLeftNavClick}
        />
      </WindowQuery>
    );
  }
}

function mapStateToProps(state) {
  return {
    dashboardRequesting: dashboardRequestingSelector(state),
    favorites: favoritesSelector(state),
    navExpanded: leftNavExpandedSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadDashboard: () => dispatch(loadDashboardAction()),
    setLeftNavExpanded: navExpanded => dispatch(setLeftNavExpandedAction(navExpanded)),
  };
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(
    withTheme(
      immutableToJS(LeftNavContainer),
    ),
  ),
);
