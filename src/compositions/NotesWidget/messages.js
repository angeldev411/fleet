import { defineMessages } from 'react-intl';

const messages = defineMessages({
  collapseAll: {
    id: 'compositions.NotesWidget.notesOptions.collapseAll',
    defaultMessage: 'Collapse All',
  },
  expandAll: {
    id: 'compositions.NotesWidget.notesOptions.expandAll',
    defaultMessage: 'Expand All',
  },
  loadingNotes: {
    id: 'compositions.NotesWidget.notesSortOptions.loadingNotes',
    defaultMessage: '...Loading',
  },
  newToOld: {
    id: 'compositions.NotesWidget.notesSortOptions.newToOld',
    defaultMessage: 'Newest to Oldest',
  },
  noNotesYet: {
    id: 'compositions.NotesWidget.notesSortOptions.noNotesYet',
    defaultMessage: 'No notes yet.',
  },
  oldToNew: {
    id: 'compositions.NotesWidget.notesSortOptions.oldToNew',
    defaultMessage: 'Oldest to Newest',
  },
  oldNotesCollapsedHeader: {
    id: 'compositions.NotesWidget.notesMore.oldNotesCollapsedHeader',
    defaultMessage: '({count}) Older {count, plural, one {Comment} other {Comments}}',
  },
  sortLabel: {
    id: 'compositions.NotesWidget.sortLabel',
    defaultMessage: 'SORT:',
  },
  title: {
    id: 'compositions.NotesWidget.title',
    defaultMessage: 'Notes {count}',
  },
});

export default messages;
