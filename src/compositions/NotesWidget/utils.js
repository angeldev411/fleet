import { find } from 'lodash';

/**
 * Turn the given recipient (as received from the Redux store) into an option value for use by the
 * recipients multiselect component. In addition to the required `label` and
 * `value` fields, extra fields will be included to help with rendering and processing of
 * the selected items.
 * @param {Object} recipient
 * @returns {{label: string, companyName: string, userId: string, groupId: string, value: string}}
 */
export function recipientToOption({ user, group }) {
  return ({
    label: `${user.firstName} ${user.lastName}`,
    companyName: group.companyName,
    userId: user.id,
    groupId: group.id,
    value: user.id,
  });
}

/**
 * Builds an array of recipient options that are used in the `react-select-plus` component.
 * The given `recipients` are collected together based on shared `group` values so that they
 * appear as `optgroups` in the rendered select.
 *
 * @param {Array} recipients: an array of `group` and `user` pairs received from the API.
 *  Example:
 *    recipients = [
 *      {
 *        group: {
 *          companyName: ‘Volvo’,
 *          id: ’12’,
 *        },
 *        user: {
 *          email: ‘abc@abc.com’,
 *          id: ’333’,
 *          firstName: ‘Bill’,
 *          lastName: ‘Lentz’,
 *        },
 *      },
 *      {
 *        group: {...},
 *        user: {...},
 *      },
 *    ];
 * @return {Array} grouped options based on the user's groups
 *  Example:
 *    returnArray = [
 *      {
 *        label: <group_company_name>,
 *        options: [ ...results from `recipientToOption` for each recipient ]
 *      },
 *      {
 *        label: <group_company_name>,
 *        options: [ ...results from `recipientToOption` for each recipient ]
 *      }
 *    ];
 */
export function buildRecipientOptions(recipients) {
  return recipients.reduce((acc, recipient) => {
    const { group: { companyName } } = recipient;
    const newOption = recipientToOption(recipient);
    const existingGroup = find(acc, g => g.label === companyName);
    if (existingGroup) {
      existingGroup.options = existingGroup.options.concat(newOption);
      return acc;
    }
    return acc.concat({
      label: companyName,
      options: [newOption],
    });
  }, []);
}

/**
 * Given an array of selected note recipients from the multiselect, return an
 * array of objects representing these users for use by the API.  Each item
 * in the array will be an object with the `userId` and `groupId` of the
 * recipient.
 * @param {{userId: string, groupId: string}[]} selections
 * @returns {{userId: string, groupId: string}[]}
 */
export function selectedRecipientsforApi(selections) {
  return selections.map(selection => ({
    userId: selection.userId,
    groupId: selection.groupId,
  }));
}
