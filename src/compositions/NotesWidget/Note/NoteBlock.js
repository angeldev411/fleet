import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  unread: ({ theme }) => ({ styles: `
    background-color: ${theme.colors.base.selectableActive};
    border-left: ${px2rem(4)} solid ${theme.colors.base.linkHover};
  ` }),
  borderBottom: ({ theme }) => ({ styles: `
    border-bottom: 2px solid ${theme.colors.base.chrome200};
  ` }),
};

const styles = `
  font-size: ${px2rem(12)};
  margin-bottom: ${px2rem(5)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome200: PropTypes.string.isRequired,
      selectableActive: PropTypes.string.isRequired,
      linkHover: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  isRead: PropTypes.bool,
};

export default buildStyledComponent(
  'NoteBlock',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
