import React from 'react';
import { omit } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import Note, {
  buildFirstTwoRecipientText,
  buildFullRecipientText,
  buildSentAt,
  buildUserLabel,
} from '../index';
import messages from '../messages';

const user = {
  firstName: 'George',
  lastName: 'Thomas',
  email: 'test_user_email@fake.com',
};

const group = {
  companyName: 'Trucker Central Inc.',
};

const recip1 = {
  firstName: 'Bobby',
  lastName: 'Flay',
  email: 'test_recip1_email@fake.com',
};

const group1 = {
  companyName: 'Mr. Fixer Inc.',
};

const recip2 = {
  firstName: 'Tim',
  lastName: 'McGraw',
  email: 'test_recip2_email@fake.com',
};

const recip3 = {
  firstName: 'Chris',
  lastName: 'Bates',
  email: 'test_recip3_email@fake.com',
};

const group2 = {};

const group3 = {
  companyName: 'WorldBank Inc.',
};

const recipients = [
  { user: recip1, group: group1 },
  { user: recip2, group: group2 },
  { user: recip3, group: group3 },
];

const sentAt = new Date(2000, 0, 1);

const note = {
  id: 12345,
  recipients,
  sender: {
    user,
    group,
  },
  sentAt: sentAt.toISOString(),
  status: 'unread',
};

const defaultProps = {
  expandKey: 'noteExpanded-12345',
  handleReply: () => {},
  handleStatusToggle: () => {},
  isExpanded: undefined,
  note,
  onExpand: () => {},
  updateNoteStatus: () => {},
};

function shallowRender(props = defaultProps) {
  return shallow(<Note {...props} />);
}

/* ----------------------- buildUserLabel --------------------------- */

test('returns the expected string with first name, last name, and company name', () => {
  const label = buildUserLabel(user, group);
  expect(label).toEqual(
    `${user.firstName} ${user.lastName} (${group.companyName})`,
  );
});

test('returns the expected string with no user name provided', () => {
  const label = buildUserLabel(null, group);
  expect(label).toEqual(`(${group.companyName})`);
});

test('returns the expected string with no companyName provided', () => {
  const label = buildUserLabel(user, {});
  expect(label).toEqual(`${user.firstName} ${user.lastName} `);
});

/* ----------------------- buildSentAt --------------------------- */

test('generates the expected moment object', () => {
  const time = buildSentAt(note);
  expect(time.toISOString()).toEqual(sentAt.toISOString());
});

/* ----------------------- buildRecipientText --------------------------- */

test('generates all expected recipients text', () => {
  const text = buildFullRecipientText(note);
  const label1 = buildUserLabel(recip1, group1);
  const label2 = buildUserLabel(recip2, group2);
  const label3 = buildUserLabel(recip3, group3);
  expect(text).toContain(`${label1}\n ${label2}\n ${label3}\n`);
});

test('generates only two recipients text if there are over 2 recipients', () => {
  const text = buildFirstTwoRecipientText(note);
  const label1 = buildUserLabel(recip1, group1);
  const label2 = buildUserLabel(recip2, group2);
  expect(text).toContain(`${label1}\n ${label2}\n`);
});

/* ----------------------- Note --------------------------- */

test('does not start timeout on mouse enter, if note status is `read`', () => {
  const testNote = {
    ...note,
    status: 'read',
  };
  const testProps = { ...defaultProps, note: testNote };
  const component = shallowRender(testProps);
  const instance = component.instance();
  component.simulate('mouseenter');
  expect(instance.hoverTimeout).toNotExist();
});

test('stops hover timer on mouse leave', () => {
  const component = shallowRender();
  const instance = component.instance();
  component.simulate('mouseleave');
  expect(instance.hoverTimeout).toNotExist();
});

test('starts timeout on mouse enter, if note status is `unread`; clears it on mouse leave', () => {
  const component = shallowRender();
  const instance = component.instance();
  component.simulate('mouseenter');
  expect(instance.hoverTimeout).toExist();
  component.simulate('mouseleave');
  expect(instance.hoverTimeout).toNotExist();
});

test.cb('calls `updateNoteStatus` with `read` param ~3 seconds after mouse enter', (t) => {
  t.plan(1);
  const updateNoteStatus = createSpy();
  const component = shallowRender({ ...defaultProps, updateNoteStatus });
  component.simulate('mouseenter');
  setTimeout(() => {
    expect(updateNoteStatus).toHaveBeenCalledWith(note.id, 'read');
    t.pass();
    t.end();
  }, 3500);
});

test('renders Expandable components', () => {
  const component = shallowRender();
  expect(component).toContain('ExpandableHeader');
  expect(component).toContain('ExpandableContent');
});

test('renders a Gravatar with the users email', () => {
  const component = shallowRender();
  expect(component).toContain('Gravatar');
  expect(component.find('Gravatar')).toHaveProp('email', user.email);
});

test('renders a NoteSender with empty string if sender is not set', () => {
  const testNote = omit(note, 'sender');
  const component = shallowRender({
    ...defaultProps,
    note: testNote,
  });
  const noteSender = component.find('TextDiv').at(0);
  expect(noteSender.render().text()).toEqual('  ');
});

test('renders a NoteBlock with modifier `borderBottom` if a prop borderBottom is set to true', () => {
  const component = shallowRender({
    ...defaultProps,
    borderBottom: true,
  });
  const noteBlock = component.find('NoteBlock');
  expect(noteBlock.props().modifiers).toInclude('borderBottom');
});

test('renders reply link', () => {
  const component = shallowRender();
  const replyLink = component.find('ButtonLink').at(0);
  expect(replyLink).toContain('FormattedMessage');
  expect(replyLink.find('FormattedMessage')).toHaveProps({ ...messages.reply });
});

test('clicking reply link calls `handleReply` from props', () => {
  const preventDefault = createSpy();
  const handleReply = createSpy();
  const component = shallowRender({
    ...defaultProps,
    handleReply,
  });
  const replyLink = component.find('ButtonLink').at(0);
  replyLink.simulate('click', { preventDefault });
  expect(preventDefault).toHaveBeenCalled();
  expect(handleReply).toHaveBeenCalledWith(defaultProps.note);
});

test('does not render a mark as read ButtonLink if note status is read_only', () => {
  const testNote = {
    ...note,
    status: 'read_only',
  };
  const component = shallowRender({
    ...defaultProps,
    note: testNote,
  });

  expect(component.find('ButtonLink').length).toEqual(1);
  const link = component.find('ButtonLink').at(0);
  expect(link.find('FormattedMessage').props().id).toNotEqual(messages.markAsRead.id);
});

test('renders a mark as read ButtonLink with note status unread', () => {
  const component = shallowRender();
  const readLink = component.find('ButtonLink').at(1);
  expect(readLink).toContain('FormattedMessage');
  expect(readLink.find('FormattedMessage')).toHaveProps({ ...messages.markAsRead });
});

test('clicking mark as read link calls `handleStatusToggle` from props', () => {
  const preventDefault = createSpy();
  const handleStatusToggle = createSpy();
  const component = shallowRender({
    ...defaultProps,
    handleStatusToggle,
  });
  const readLink = component.find('ButtonLink').at(1);
  readLink.simulate('click', { preventDefault });
  expect(preventDefault).toHaveBeenCalled();
  expect(handleStatusToggle).toHaveBeenCalledWith(defaultProps.note);
});

test('renders a mark as unread ButtonLink with note status read', () => {
  const testNote = {
    ...note,
    status: 'read',
  };
  const component = shallowRender({
    ...defaultProps,
    note: testNote,
  });
  const unreadLink = component.find('ButtonLink').at(1);
  expect(unreadLink).toContain('FormattedMessage');
  expect(unreadLink.find('FormattedMessage')).toHaveProps({ ...messages.markAsUnread });
});

test('handleExpandNote calls `onExpand` from props with not id and expanded value', () => {
  const onExpand = createSpy();
  const component = shallowRender({
    ...defaultProps,
    onExpand,
  });
  const instance = component.instance();
  instance.handleExpandNote(true);
  expect(onExpand).toHaveBeenCalledWith(defaultProps.note.id, true);
});

test('Note renders the message text using DangerousHtmlMessage', () => {
  const message = 'This is my note message';
  const noteWithMessage = {
    ...note,
    message,
  };
  const component = shallowRender({
    ...defaultProps,
    note: noteWithMessage,
  });
  expect(component.find('ExpandableContent')).toContain('DangerousHtmlMessage');
  expect(component.find('ExpandableContent DangerousHtmlMessage')).toHaveProp('message', message);
});
