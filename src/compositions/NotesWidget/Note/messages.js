import { defineMessages } from 'react-intl';

const messages = defineMessages({
  collapse: {
    id: 'compositions.NotesWidget.Note.popoverMessages.collapse',
    defaultMessage: 'Collapse Note',
  },
  expand: {
    id: 'compositions.NotesWidget.Note.popoverMessages.expand',
    defaultMessage: 'Expand Note',
  },
  markAsRead: {
    id: 'compositions.NotesWidget.Note.message.markAsRead',
    defaultMessage: 'Mark as Read',
  },
  markAsUnread: {
    id: 'compositions.NotesWidget.Note.message.markAsUnread',
    defaultMessage: 'Mark as Unread',
  },
  readAt: {
    id: 'compositions.NotesWidget.Note.readAt',
    defaultMessage: '{date}',
  },
  reply: {
    id: 'compositions.NotesWidget.Note.message.reply',
    defaultMessage: 'Reply',
  },
  more: {
    id: 'compositions.NotesWidget.Note.recipient.more',
    defaultMessage: '... {count} more',
  },
});

export default messages;
