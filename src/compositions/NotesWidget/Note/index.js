import { noop, compact } from 'lodash';
import moment from 'moment-timezone';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { FormattedMessage, FormattedRelative } from 'react-intl';
import {
  Column,
  Container,
  Row,
} from 'styled-components-reactive-grid';

import Expandable, { ExpandableContent, ExpandableHeader } from 'containers/Expandable';

import MessageBody from 'components/DangerousHtmlMessage';

import { ButtonLink } from 'base-components';
import Gravatar from 'elements/Gravatar';
import TextDiv from 'elements/TextDiv';
import Popover, {
  PopoverTarget,
  PopoverContent,
  ScrollingPopoverContentWrapper,
} from 'elements/Popover';
import Span from 'elements/Span';
import Tooltip from 'elements/Tooltip';

import { userTimeZone, formatDate } from 'utils/timeUtils';

import NoteBlock from './NoteBlock';
import messages from './messages';


export function buildRecipientFullName(user) {
  return user && `${user.firstName || ''} ${user.lastName || ''} `;
}

export function buildRecipientCompanyName(group) {
  return group.companyName;
}

export function buildUserLabel(user, group) {
  const fullName = buildRecipientFullName(user);
  const companyName = buildRecipientCompanyName(group);
  return `${fullName || ''}${companyName ? `(${companyName})` : ''}`;
}

export function buildFullRecipientText({ recipients }) {
  return recipients.reduce((acc, recipient) =>
    `${acc} ${buildUserLabel(recipient.user, recipient.group)}\n`,
  '',
  );
}

export function buildFirstTwoRecipientText({ recipients }) {
  return buildFullRecipientText({ recipients: recipients.slice(0, 2) });
}

export function buildSentAt(note) {
  return moment.tz(note.sentAt, userTimeZone());
}

class Note extends Component {
  static propTypes = {
    expandKey: PropTypes.string.isRequired,
    handleStatusToggle: PropTypes.func.isRequired,
    handleReply: PropTypes.func.isRequired,
    isExpanded: PropTypes.bool,
    note: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
    onExpand: PropTypes.func,
    updateNoteStatus: PropTypes.func.isRequired,
    borderBottom: PropTypes.bool,
  };

  static defaultProps = {
    isExpanded: undefined,
    onExpand: noop,
    borderBottom: false,
  };

  handleMouseEnter = () => {
    const { note } = this.props;
    if (note.status === 'read') return;
    // It's better to save the timeout handler as a variable on this than
    // storing it in state because. In the latter case the component will
    // unnecessarily re-render whenever this happens.
    // Refer to: http://stackoverflow.com/a/25208809/3930247
    this.hoverTimeout = setTimeout(() => {
      this.props.updateNoteStatus(note.id, 'read');
    }, 3000);
  }

  handleMouseLeave = () => {
    if (this.hoverTimeout) {
      clearTimeout(this.hoverTimeout);
      this.hoverTimeout = null;
    }
  }

  handleReplyClick = (event) => {
    event.preventDefault();
    this.props.handleReply(this.props.note);
  }

  handleExpandNote = (expanded) => {
    this.props.onExpand(this.props.note.id, expanded);
  }

  handleStatusLinkClick = (event) => {
    event.preventDefault();
    this.props.handleStatusToggle(this.props.note);
  }

  renderRecipientItem = ({ user, group }) => (
    <Row key={`${user.id}:${group.id}`}>
      <Column>
        <TextDiv modifiers={['bold', 'midGreyText', 'padRight']}>
          {buildRecipientFullName(user)}
        </TextDiv>
      </Column>
      <Column>
        <TextDiv modifiers={['darkGreyText', 'lightText']}>
          {buildRecipientCompanyName(group)}
        </TextDiv>
      </Column>
    </Row>
  )

  render() {
    const {
      expandKey,
      isExpanded,
      note: {
        sender = {},
      },
      note,
      borderBottom,
    } = this.props;
    const senderGroup = sender.group || {};
    const senderUser = sender.user || {};

    const noteReadUnreadMessage = note.status === 'read' ?
      messages.markAsUnread :
      messages.markAsRead;

    const readStatus = note.status === 'unread' ? 'unread' : null;
    const borderBottomStatus = borderBottom && 'borderBottom';
    const hasMoreRecipients = note.recipients.length > 2;

    return (
      <NoteBlock
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        modifiers={compact([readStatus, borderBottomStatus])}
        id={this.props.note.id}
      >
        <Expandable
          expandKey={expandKey}
          hasPopover
          isExpanded={isExpanded}
          onExpand={this.handleExpandNote}
          popoverMessages={messages}
        >
          <ExpandableHeader>
            <Container>
              <Row modifiers={['middle']}>
                <Column>
                  <Gravatar email={senderUser.email} round />
                </Column>
                <Column>
                  <TextDiv modifiers={['bold', 'mediumText']}>
                    {buildUserLabel(senderUser, senderGroup)}
                  </TextDiv>
                </Column>
                <Column modifiers={['col']} />
                <Column modifiers={['end']}>
                  <Tooltip
                    position="top"
                    message={{ ...messages.readAt,
                      values: {
                        date: formatDate(buildSentAt(note)),
                      },
                    }}
                  >
                    <TextDiv modifiers={['capitalize', 'midGreyText', 'textAlignRight']}>
                      <FormattedRelative value={buildSentAt(note)} />
                    </TextDiv>
                  </Tooltip>
                </Column>
              </Row>
            </Container>
          </ExpandableHeader>
          <ExpandableContent>
            <Container>
              <Row>
                <Column style={{ width: '44px' }} />
                <Column modifiers={['col']}>
                  <TextDiv modifiers={['heavyText', 'inline']}>
                    {hasMoreRecipients ?
                      buildFirstTwoRecipientText(note) : buildFullRecipientText(note)}
                    {hasMoreRecipients &&
                      <Popover
                        showOnHover
                        position="right"
                        positionOffset={50}
                        delay={500}
                        inlineBlock
                      >
                        <PopoverTarget>
                          <Span modifiers={['textLight', 'weightNormal']}>
                            <FormattedMessage
                              {...messages.more}
                              values={{ count: note.recipients.length - 2 }}
                            />
                          </Span>
                        </PopoverTarget>
                        <PopoverContent>
                          <ScrollingPopoverContentWrapper modifiers={['smallText']}>
                            <Container>
                              {note.recipients.map(
                                recipient => this.renderRecipientItem(recipient))}
                            </Container>
                          </ScrollingPopoverContentWrapper>
                        </PopoverContent>
                      </Popover>
                    }
                  </TextDiv>
                  <MessageBody message={note.message} />
                  <Row>
                    <Column modifiers={['col', 'fluid']}>
                      <ButtonLink onClick={this.handleReplyClick}>
                        <FormattedMessage {...messages.reply} />
                      </ButtonLink>
                    </Column>
                    {note.status !== 'read_only' &&
                      <Column modifiers={['col', 'end', 'fluid']}>
                        <ButtonLink onClick={this.handleStatusLinkClick}>
                          <FormattedMessage {...noteReadUnreadMessage} />
                        </ButtonLink>
                      </Column>
                    }
                  </Row>
                </Column>
              </Row>
            </Container>
          </ExpandableContent>
        </Expandable>
      </NoteBlock>
    );
  }
}

export default Note;
