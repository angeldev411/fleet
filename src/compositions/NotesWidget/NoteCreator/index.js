import { QuickActionButton } from 'base-components';
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  Column,
  Container,
  Row,
} from 'styled-components-reactive-grid';

import NoteComposer from './NoteComposer';
import NoteCreatorError from './NoteCreatorError';

import messages from './messages';

import {
  COMPOSER_ERROR,
  COMPOSER_HIDDEN,
  COMPOSER_INVISIBLE,
  COMPOSER_LOADING,
  COMPOSER_NO_RECIPIENTS,
  COMPOSER_VISIBLE,
} from '../constants';

function NoteCreator({
  composerStatus,
  handleNewNoteClick,
  ...rest
}) {
  return (
    <Container>
      {composerStatus === COMPOSER_VISIBLE ?
        <NoteComposer {...rest} />
        :
        <Row modifiers={['fluid']}>
          <Column>
            <QuickActionButton onClick={handleNewNoteClick}>
              <QuickActionButton.Icon modifiers={['left']} name="plus" />
              <QuickActionButton.Text>
                <FormattedMessage {...messages.newNote} />
              </QuickActionButton.Text>
            </QuickActionButton>
          </Column>
          <Column>
            {composerStatus === COMPOSER_ERROR &&
              <NoteCreatorError>
                <FormattedMessage {...messages.loadingRecipientsError} />
              </NoteCreatorError>
            }
            {composerStatus === COMPOSER_NO_RECIPIENTS &&
              <NoteCreatorError>
                <FormattedMessage {...messages.noRecipientsError} />
              </NoteCreatorError>
            }
          </Column>
        </Row>
      }
    </Container>
  );
}

NoteCreator.propTypes = {
  composerStatus: PropTypes.oneOf([
    COMPOSER_ERROR,
    COMPOSER_HIDDEN,
    COMPOSER_INVISIBLE,
    COMPOSER_LOADING,
    COMPOSER_NO_RECIPIENTS,
    COMPOSER_VISIBLE,
  ]).isRequired,
  handleNewNoteClick: PropTypes.func.isRequired,
};

export default NoteCreator;
