import { compact, noop } from 'lodash';
import PropTypes from 'prop-types';
import { QuickActionButton } from 'base-components';
import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import {
  Column,
  Container,
  Row,
} from 'styled-components-reactive-grid';
import { px2rem } from 'decisiv-ui-utils';

import Select from 'elements/Select';
import Textarea from 'elements/Textarea';
import TextDiv from 'elements/TextDiv';

import messages from './messages';

import { buildRecipientOptions } from '../utils';

class NoteComposer extends Component {
  static propTypes = {
    caseRecipients: PropTypes.shape({
      requesting: PropTypes.bool.isRequired,
      error: PropTypes.object,
      data: PropTypes.arrayOf(
        PropTypes.shape({
          group: PropTypes.shape({
            id: PropTypes.string.isRequired,
          }),
          user: PropTypes.shape({
            id: PropTypes.string.isRequired,
          }),
        }),
      ).isRequired,
    }).isRequired,
    handleRecipientChange: PropTypes.func.isRequired,
    handleNewNoteCancel: PropTypes.func.isRequired,
    handleNoteCreate: PropTypes.func.isRequired,
    focusTextArea: PropTypes.func,
    selectedRecipients: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        companyName: PropTypes.string.isRequired,
        userId: PropTypes.string.isRequired,
        groupId: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
      }),
    ).isRequired,
  };

  static defaultProps = {
    focusTextArea: noop,
  };

  state = {
    message: '',
  };

  handleMessageChange = ({ target }) => {
    this.setState({ message: target.value });
  }

  handleCreateClick = () => {
    const { message } = this.state;
    this.props.handleNoteCreate(message);
    this.setState({ message: '' });
  }

  valueRenderer = option => `${option.label} (${option.companyName})`

  render() {
    const {
      caseRecipients,
      focusTextArea,
      handleRecipientChange,
      handleNewNoteCancel,
      selectedRecipients,
    } = this.props;
    const { message } = this.state;
    const postButtonDisabled = !message || selectedRecipients.length === 0;
    const recipientOptions = buildRecipientOptions(caseRecipients.data);

    return (
      <Container>
        <Row modifiers={['fluid']}>
          <Column>
            <TextDiv modifiers={['midGreyText', 'smallText', 'uppercase']}>
              <FormattedMessage {...messages.title} />
            </TextDiv>
          </Column>
        </Row>
        <Row modifiers={['fluid', 'middle']}>
          <Column style={{ width: px2rem(32) }}>
            <TextDiv modifiers={['midGreyText', 'smallText', 'uppercase']}>
              <FormattedMessage {...messages.to} />
            </TextDiv>
          </Column>
          <Column modifiers={['col', 'fluid']}>
            <Select
              autofocus
              multi
              onChange={handleRecipientChange}
              openOnFocus
              options={recipientOptions}
              value={selectedRecipients}
              valueRenderer={this.valueRenderer}
            />
          </Column>
        </Row>
        <Row modifiers={['fluid']}>
          <Column style={{ width: px2rem(32) }} />
          <Column modifiers={['col']}>
            <Row modifiers={['fluid']}>
              <Column modifiers={['col']} style={{ display: 'flex' }}>
                <Textarea
                  innerRef={(textarea) => { focusTextArea(textarea); }}
                  onChange={this.handleMessageChange}
                  value={message}
                />
              </Column>
            </Row>
            <Row modifiers={['end', 'fluid']}>
              <Column modifiers={['end']}>
                <QuickActionButton onClick={handleNewNoteCancel}>
                  <QuickActionButton.Text>
                    <FormattedMessage {...messages.cancelButton} />
                  </QuickActionButton.Text>
                </QuickActionButton>
              </Column>
              <Column modifiers={['end']}>
                <QuickActionButton
                  modifiers={compact([
                    postButtonDisabled && 'disabled',
                    'secondary',
                    'hoverSuccess',
                  ])}
                  onClick={this.handleCreateClick}
                >
                  <QuickActionButton.Text>
                    <FormattedMessage {...messages.postButton} />
                  </QuickActionButton.Text>
                </QuickActionButton>
              </Column>
            </Row>
          </Column>
        </Row>
      </Container>
    );
  }
}

export default NoteComposer;
