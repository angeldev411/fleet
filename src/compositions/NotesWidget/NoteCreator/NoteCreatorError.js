import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.status.danger};
  font-size: ${px2rem(12)};
  font-weight: bold;
  line-height: ${px2rem(18)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'NoteCreatorError',
  styled.span,
  styles,
  { themePropTypes },
);
