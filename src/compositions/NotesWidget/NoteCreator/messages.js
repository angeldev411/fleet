import { defineMessages } from 'react-intl';

const messages = defineMessages({
  cancelButton: {
    id: 'compositions.NotesWidget.NoteCreator.NoteComposer.cancelButton',
    defaultMessage: 'Cancel',
  },
  loadingRecipientsError: {
    id: 'compositions.NotesWidget.NoteCreator.error.loadingRecipientsErrorDescription',
    defaultMessage: 'Sorry, we had a connection issue while loading recipients. Please try again.',
  },
  newNote: {
    id: 'compositions.NotesWidget.NoteCreator.noteCreatorButtonText',
    defaultMessage: 'New Note',
  },
  noRecipientsError: {
    id: 'compositions.NotesWidget.NoteCreator.error.noRecipientsErrorDescription',
    defaultMessage: 'We didn\'t find any possible recipients. Please contact our support services team.',
  },
  postButton: {
    id: 'compositions.NotesWidget.NoteCreator.NoteComposer.postButton',
    defaultMessage: 'Post',
  },
  title: {
    id: 'compositions.NotesWidget.NoteCreator.NoteComposer.title',
    defaultMessage: 'New Note',
  },
  to: {
    id: 'compositions.NotesWidget.NoteCreator.NoteComposer.to',
    defaultMessage: 'To:',
  },
});

export default messages;
