import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import {
  COMPOSER_ERROR,
  COMPOSER_HIDDEN,
  COMPOSER_INVISIBLE,
  COMPOSER_LOADING,
  COMPOSER_NO_RECIPIENTS,
  COMPOSER_VISIBLE,
} from '../../constants';
import NoteCreator from '../index';
import messages from '../messages';

const handleNewNoteClick = () => {};

const defaultProps = {
  caseRecipients: { data: [], error: null, requesting: false },
  composerStatus: COMPOSER_INVISIBLE,
  handleNewNoteCancel: () => {},
  handleNewNoteClick,
  handleNoteCreate: () => {},
  handleRecipientChange: () => {},
  selectedRecipients: [],
};

function shallowRender(props = defaultProps) {
  return (shallow(<NoteCreator {...props} />));
}

test('renders a NoteComposer with "rest" of props if composerStatus is COMPOSER_VISIBLE', () => {
  const props = {
    ...defaultProps,
    composerStatus: COMPOSER_VISIBLE,
    otherProp: 'test',
  };
  const component = shallowRender(props);
  expect(component).toContain('NoteComposer');
  expect(component.find('NoteComposer')).toHaveProp('otherProp', props.otherProp);
});

[
  COMPOSER_ERROR,
  COMPOSER_HIDDEN,
  COMPOSER_INVISIBLE,
  COMPOSER_LOADING,
  COMPOSER_NO_RECIPIENTS,
].forEach((composerStatus) => {
  test(`does not render a NoteComposer if composerStatus is ${composerStatus}`, () => {
    const component = shallowRender({
      ...defaultProps,
      composerStatus,
    });
    expect(component).toNotContain('NoteComposer');
  });

  test(`renders a NoteCreatorButton if composerStatus is ${composerStatus}`, () => {
    const component = shallowRender({
      ...defaultProps,
      composerStatus,
    });
    expect(component).toContain('QuickActionButton');
    expect(component.find('QuickActionButton')).toHaveProps({
      onClick: handleNewNoteClick,
    });
  });
});

test(
  'renders a NoteCreatorError block with correct error message if composerStatus is COMPOSER_ERROR',
  () => {
    const props = {
      ...defaultProps,
      composerStatus: COMPOSER_ERROR,
    };
    const component = shallowRender(props);
    expect(component).toContain('NoteCreatorError');
    expect(component.find('NoteCreatorError FormattedMessage')).toHaveProps({
      ...messages.loadingRecipientsError,
    });
  },
);

test(
  'renders a NoteCreatorError block with correct error message if composerStatus is COMPOSER_NO_RECIPIENTS',
  () => {
    const props = {
      ...defaultProps,
      composerStatus: COMPOSER_NO_RECIPIENTS,
    };
    const component = shallowRender(props);
    expect(component).toContain('NoteCreatorError');
    expect(component.find('NoteCreatorError FormattedMessage')).toHaveProps({
      ...messages.noRecipientsError,
    });
  },
);
