import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import NoteComposer from '../NoteComposer';
import messages from '../messages';

const caseRecipients = {
  data: [],
  error: null,
  requesting: false,
};

const selectedRecipients = [];

const defaultProps = {
  caseRecipients,
  handleCreateClick: () => true,
  handleNewNoteCancel: () => true,
  handleNoteCreate: () => true,
  handleRecipientChange: () => true,
  selectedRecipients,
};

function shallowRender(props = defaultProps) {
  return (shallow(<NoteComposer {...props} />));
}

test('renders a title message', () => {
  const component = shallowRender();
  const formattedMessage = component.find('FormattedMessage').at(0);
  expect(formattedMessage).toHaveProps({ ...messages.title });
});

test('renders a to message', () => {
  const component = shallowRender();
  const formattedMessage = component.find('FormattedMessage').at(1);
  expect(formattedMessage).toHaveProps({ ...messages.to });
});

test('renders a Select component with correct props', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(component).toContain('Select');
  expect(component.find('Select')).toHaveProps({
    multi: true,
    onChange: defaultProps.handleRecipientChange,
    value: defaultProps.selectedRecipients,
    valueRenderer: instance.valueRenderer,
  });
});

test('autofocus is triggered when add note is clicked', () => {
  const component = shallowRender();
  expect(component.find('Select')).toHaveProps({
    autofocus: true,
    openOnFocus: true,
  });
});

test('renders a Textarea component with correct props', () => {
  const testMessage = 'test note value';
  const component = shallowRender();
  const instance = component.instance();
  component.setState({ message: testMessage });
  expect(component).toContain('Textarea');
  expect(component.find('Textarea')).toHaveProps({
    onChange: instance.handleMessageChange,
    value: testMessage,
  });
});

test('renders a `Cancel` QuickActionButton', () => {
  const component = shallowRender();
  const cancelButton = component.find('QuickActionButton').at(0);
  expect(cancelButton).toHaveProps({
    onClick: defaultProps.handleNewNoteCancel,
  });
  expect(cancelButton.find('FormattedMessage')).toHaveProps({
    ...messages.cancelButton,
  });
});

test('renders a `Post` QuickActionButton', () => {
  const component = shallowRender();
  const postButton = component.find('QuickActionButton').at(1);
  const instance = component.instance();
  expect(postButton).toHaveProps({
    onClick: instance.handleCreateClick,
  });
  expect(postButton.find('FormattedMessage')).toHaveProps({
    ...messages.postButton,
  });
});

test('Post button is disabled if the note message is empty', () => {
  const testSelectedRecipients = [{
    label: 'test',
    companyName: 'SuperFastTrucking',
    userId: '123',
    groupId: '456',
    value: 'idk',
  }];
  const component = shallowRender({
    ...defaultProps,
    selectedRecipients: testSelectedRecipients,
  });
  const postButton = component.find('QuickActionButton').at(1);
  expect(postButton.props().modifiers).toInclude('disabled');
});

test('Post button is disabled if selectedRecipients is empty', () => {
  const component = shallowRender();
  component.setState({ message: 'Not blank' });
  const postButton = component.find('QuickActionButton').at(1);
  expect(postButton.props().modifiers).toInclude('disabled');
});

test('Post button is enabled if the note message AND selectedRecipients are not empty', () => {
  const testSelectedRecipients = [{
    label: 'test',
    companyName: 'SuperFastTrucking',
    userId: '123',
    groupId: '456',
    value: 'idk',
  }];
  const component = shallowRender({
    ...defaultProps,
    selectedRecipients: testSelectedRecipients,
  });
  component.setState({ message: 'Not blank' });
  const postButton = component.find('QuickActionButton').at(1);
  expect(postButton.props().modifiers).toNotInclude('disabled');
});

test('call focusTextArea by innerRef', () => {
  const focusTextArea = createSpy();
  const component = shallowRender({ ...defaultProps, focusTextArea });
  const textArea = component.find('Textarea');
  const input = 'TestInput';
  textArea.props().innerRef(input);
  expect(focusTextArea).toHaveBeenCalledWith(input);
});

// ------------------------------ handleMessageChange ------------------------------

test('handleMessageChange updates `message` state', () => {
  const testValue = 'test target message';
  const syntheticEvent = {
    target: {
      value: testValue,
    },
  };
  const component = shallowRender();
  const instance = component.instance();
  instance.handleMessageChange(syntheticEvent);
  expect(component.state('message')).toBe(testValue);
});

// ------------------------------ handleCreateClick ------------------------------

test('handleCreateClick calls props.handleCreateClick with correct message param', () => {
  const testHandleNoteCreate = createSpy();
  const testProps = {
    ...defaultProps,
    handleNoteCreate: testHandleNoteCreate,
  };
  const testMessage = 'test message for handleNoteCreate';
  const component = shallowRender(testProps);
  component.setState({ message: testMessage });
  const instance = component.instance();
  instance.handleCreateClick();
  expect(testHandleNoteCreate).toHaveBeenCalledWith(testMessage);

  // also resets `message` state
  expect(component.state('message')).toBe('');
});
