import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import messages from './messages';

function generateWidgetHeaderNote({ unreadNotesCount }) {
  return unreadNotesCount ? ` (${unreadNotesCount} unread)` : '';
}

function NotesWidgetHeader({ caseInfo }) {
  return (
    <FormattedMessage
      {...messages.title}
      values={{ count: generateWidgetHeaderNote(caseInfo) }}
    />
  );
}

NotesWidgetHeader.propTypes = {
  caseInfo: PropTypes.shape({
    unreadNotesCount: PropTypes.number,
  }).isRequired,
};

export default NotesWidgetHeader;
