import { QuickActionButton } from 'base-components';
import React from 'react';
import PropTypes from 'prop-types';
import {
  Column,
  Row,
} from 'styled-components-reactive-grid';

import DropDownSelector from 'components/_common/DropDownSelector';

import Widget from 'elements/Widget';

import messages from './messages';

function NotesOptionsBar({
  handleNewNoteClick,
  handleNoteOptionChange,
  handleNoteSortOptionChange,
  notesOptions,
  notesSortOptions,
  selectedNoteOption,
  selectedNoteSortOption,
}) {
  return (
    <Widget.OptionsBar>
      <Row modifiers={['middle']}>
        <Column>
          <DropDownSelector
            onChange={handleNoteOptionChange}
            options={notesOptions}
            selected={selectedNoteOption}
          />
        </Column>
        <Column />
        <Column>
          <DropDownSelector
            label={messages.sortLabel}
            onChange={handleNoteSortOptionChange}
            options={notesSortOptions}
            selected={selectedNoteSortOption}
          />
        </Column>
        <Column modifiers={['col', 'end']}>
          <QuickActionButton onClick={handleNewNoteClick}>
            <QuickActionButton.Icon name="plus" />
          </QuickActionButton>
        </Column>
      </Row>
    </Widget.OptionsBar>
  );
}

NotesOptionsBar.propTypes = {
  handleNewNoteClick: PropTypes.func.isRequired,
  handleNoteOptionChange: PropTypes.func.isRequired,
  handleNoteSortOptionChange: PropTypes.func.isRequired,
  notesOptions: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }),
  ).isRequired,
  notesSortOptions: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }),
  ).isRequired,
  selectedNoteOption: PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.string,
  }),
  selectedNoteSortOption: PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.string,
  }),
};

NotesOptionsBar.defaultProps = {
  selectedNoteOption: undefined,
  selectedNoteSortOption: undefined,
};

export default NotesOptionsBar;
