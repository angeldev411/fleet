import React from 'react';
import { mapKeys } from 'lodash';
import { scroller } from 'react-scroll';

import {
  test,
  expect,
  shallow,
  createSpy,
  spyOn,
} from '__tests__/helpers/test-setup';

// TODO: Find a way to handle this that removes cross component dependency!
// Perhaps a app wide component registry, as has been suggested for
// generalized favorites & overview pages.
import {
  ADD_NOTE,
} from 'containers/QuickActionBarContainer/constants';
import { withTestTheme } from 'utils/styles';

import Note from '../Note';

import {
  NotesWidget,
} from '../index';
import {
  COMPOSER_ERROR,
  COMPOSER_HIDDEN,
  COMPOSER_INVISIBLE,
  COMPOSER_NO_RECIPIENTS,
  COMPOSER_VISIBLE,
  SCROLLABLE_COMPOSER_NAME,
} from '../constants';
import { selectedRecipientsforApi } from '../utils';
import messages from '../messages';

const NotesWidgetWithTheme = withTestTheme(NotesWidget);

const caseInfo = {
  id: '123',
  unreadNotesCount: 1,
};

const user = {
  firstName: 'George',
  lastName: 'Thomas',
  email: 'test_user_email@fake.com',
};

const group = {
  companyName: 'Trucker Central Inc.',
};

const recip1 = {
  firstName: 'Bobby',
  lastName: 'Flay',
  email: 'test_recip1_email@fake.com',
};

const group1 = {
  companyName: 'Mr. Fixer Inc.',
};

const recip2 = {
  firstName: 'Tim',
  lastName: 'McGraw',
  email: 'test_recip2_email@fake.com',
};

const group2 = {};

const recipients = [
  { user: recip1, group: group1 },
  { user: recip2, group: group2 },
];

const sentAt = new Date(2000, 0, 1);

const caseNotes = {
  data: [
    {
      id: 32123,
      recipients,
      sender: {
        user,
        group,
      },
      sentAt: sentAt.toISOString(),
      status: 'unread',
    },
    {
      id: 45624,
      recipients,
      sender: {
        user,
        group,
      },
      sentAt: sentAt.toISOString(),
      status: 'unread',
    },
    {
      id: 45334,
      recipients,
      sender: {
        user,
        group,
      },
      sentAt: sentAt.toISOString(),
      status: 'read',
    },
    {
      id: 45724,
      recipients,
      sender: {
        user,
        group,
      },
      sentAt: sentAt.toISOString(),
      status: 'read',
    },
  ],
  error: null,
  requesting: false,
};

const manyNotes = [
  ...caseNotes.data,
  {
    id: 32124,
    recipients,
    sender: {
      user,
      group,
    },
    sentAt: sentAt.toISOString(),
    status: 'unread',
  },
  {
    id: 45625,
    recipients,
    sender: {
      user,
      group,
    },
    sentAt: sentAt.toISOString(),
    status: 'unread',
  },
  {
    id: 45335,
    recipients,
    sender: {
      user,
      group,
    },
    sentAt: sentAt.toISOString(),
    status: 'read',
  },
  {
    id: 45725,
    recipients,
    sender: {
      user,
      group,
    },
    sentAt: sentAt.toISOString(),
    status: 'read',
  },
  {
    id: 55725,
    recipients,
    sender: {
      user,
      group,
    },
    sentAt: sentAt.toISOString(),
    status: 'read',
  },
];

const user1 = {
  email: 'abc@abc.com',
  id: '333',
  firstName: 'Bill',
  lastName: 'Lentz',
};
const user2 = {
  email: 'hey@gmail.com',
  id: '353',
  firstName: 'Sara',
  lastName: 'Howes',
};
const user3 = {
  email: 'hey@gmail.com',
  id: '453',
  firstName: 'New',
  lastName: 'User',
};
const caseRecipients = {
  data: [
    {
      group: {
        companyName: 'Volvo',
        id: '12',
      },
      user: user1,
    },
    {
      group: {
        companyName: 'Mack',
        id: '23',
      },
      user: user2,
    },
    {
      group: {
        companyName: 'Volvo',
        id: '12',
      },
      user: user3,
    },
  ],
  error: null,
  requesting: false,
};

const intl = {
  formatMessage: m => m.defaultMessage,
};

const defaultProps = {
  caseInfo,
  caseNotes,
  caseRecipients,
  clearCaseRecipients: () => {},
  intl,
  loadCaseNotes: () => {},
  loadCaseRecipients: () => {},
  postCaseNote: () => {},
  setCaseNotes: () => {},
  updateCaseNote: () => {},
  componentsExpanded: {},
  setComponentExpanded: () => {},
  updateScrollToUnreadNote: () => {},
};

function shallowRender(props = defaultProps) {
  return shallow(<NotesWidgetWithTheme {...props} />);
}

const selectedRecipients = [
  {
    companyName: 'Volvo',
    groupId: '12',
    isInTree: true,
    label: 'Bill Lentz',
    userId: '333',
    value: '333',
  },
  {
    companyName: 'Mack',
    groupId: '23',
    isInTree: true,
    label: 'Sara Howes',
    userId: '353',
    value: '353',
  },
  {
    companyName: 'Volvo',
    groupId: '12',
    isInTree: true,
    label: 'New User',
    userId: '453',
    value: '453',
  },
];

test('renders a Header with the note value', () => {
  const component = shallowRender();
  expect(component).toContain('Header');
  const widgetHeader = component.find('Header');
  expect(widgetHeader).toContain('NotesWidgetHeader');
  expect(widgetHeader.find('NotesWidgetHeader')).toHaveProp('caseInfo', defaultProps.caseInfo);
});

test('renders a NotesOptionsBar with the expected props', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(component).toContain('NotesOptionsBar');
  expect(component.find('NotesOptionsBar')).toHaveProps({
    handleNewNoteClick: instance.handleNewNoteClick,
    handleNoteOptionChange: instance.handleNoteOptionChange,
    handleNoteSortOptionChange: instance.handleNoteSortOptionChange,
  });
});

test(
  'renders a react-scroll component that has correct `name` prop and wraps the NoteCreator',
  () => {
    const component = shallowRender().find('.react-scroll-element');
    expect(component.length).toEqual(1);
    expect(component).toHaveProp('name', SCROLLABLE_COMPOSER_NAME);
    expect(component).toContain('NoteCreator');
  },
);

test('renders a NoteCreator with correct props', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(component).toContain('NoteCreator');
  const composerStatus = COMPOSER_VISIBLE;
  component.setState({ composerStatus });
  expect(component.find('NoteCreator')).toHaveProps({
    caseRecipients,
    composerStatus,
    focusTextArea: instance.focusTextArea,
    handleNewNoteCancel: instance.handleNewNoteCancel,
    handleNewNoteClick: instance.handleNewNoteClick,
    handleNoteCreate: instance.handleNoteCreate,
    handleRecipientChange: instance.handleRecipientChange,
    selectedRecipients: instance.state.selectedRecipients,
  });
});

// ------------------------------ componentWillReceiveProps ------------------------------

test('successful recipients load sets the composer status to visible', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(component).toHaveState({ composerStatus: COMPOSER_INVISIBLE });

  component.setState({ hasRequestedRecipients: true });
  const newProps = {
    ...defaultProps,
    caseRecipients: { requesting: false, error: false, data: recipients },
  };
  instance.componentWillReceiveProps(newProps);
  expect(component).toHaveState({ composerStatus: COMPOSER_VISIBLE });
});

test('error during recipients load sets the composer status to error', () => {
  const component = shallowRender();
  const instance = component.instance();
  component.setState({ hasRequestedRecipients: true });
  const newProps = {
    ...defaultProps,
    caseRecipients: {
      error: true,
      requesting: false,
    },
  };
  instance.componentWillReceiveProps(newProps);
  expect(component).toHaveState({ composerStatus: COMPOSER_ERROR });
});

test('if no recipients are found the composer status is set to no-recipients', () => {
  const component = shallowRender();
  const instance = component.instance();
  component.setState({ hasRequestedRecipients: true });
  const newProps = {
    ...defaultProps,
    caseRecipients: {
      data: [],
      error: false,
      requesting: false,
    },
  };
  instance.componentWillReceiveProps(newProps);
  expect(component).toHaveState({ composerStatus: COMPOSER_NO_RECIPIENTS });
});

test('if it is requesting is not changed', () => {
  const component = shallowRender();
  const instance = component.instance();
  const newProps = {
    caseInfo: {},
    caseRecipients: {
      data: [],
      error: false,
      requesting: true,
    },
    caseNotes: {
      requesting: false,
    },
  };
  const previousState = component.state().composerStatus;
  instance.componentWillReceiveProps(newProps);
  expect(component).toHaveState({ composerStatus: previousState });
});

test('calls handleNewNoteClick if new actionBarItemClicked is `ADD_NOTE`', () => {
  const component = shallowRender();
  const instance = component.instance();
  const handleNewNoteClick = createSpy();
  instance.handleNewNoteClick = handleNewNoteClick;
  const newProps = {
    ...defaultProps,
    actionBarItemClicked: {
      buttonId: ADD_NOTE,
    },
    componentsExpanded: {
      'widget-notes': true,
    },
  };
  instance.componentWillReceiveProps(newProps);
  expect(handleNewNoteClick).toHaveBeenCalled();
});

test('does not call handleNewNoteClick if new actionBarItemClicked is same as old', () => {
  const component = shallowRender({
    ...defaultProps,
    actionBarItemClicked: {
      buttonId: ADD_NOTE,
    },
  });
  const instance = component.instance();
  const handleNewNoteClick = createSpy();
  instance.handleNewNoteClick = handleNewNoteClick;
  const newProps = {
    ...defaultProps,
    actionBarItemClicked: {
      buttonId: ADD_NOTE,
    },
  };
  instance.componentWillReceiveProps(newProps);
  expect(handleNewNoteClick).toNotHaveBeenCalled();
});

// ------------------------------ componentWillUnmount ------------------------------

test('componentWillUnmount calls clearCaseRecipients', () => {
  const clearCaseRecipients = createSpy();
  const component = shallowRender({ ...defaultProps, clearCaseRecipients });
  const instance = component.instance();
  instance.componentWillUnmount();
  expect(clearCaseRecipients).toHaveBeenCalled();
});

// ------------------------------ focusTextArea ------------------------------

test(
  'focusTextArea calls focus method of the param element if shouldFocusTextArea is true',
  () => {
    const focus = createSpy();
    const textarea = { focus };
    const component = shallowRender();
    const instance = component.instance();
    component.setState({ shouldFocusTextArea: true });
    instance.focusTextArea(textarea);
    expect(focus).toHaveBeenCalled();
  },
);

test(
  'focusTextArea does not call focus method of the param element if shouldFocusTextArea is false',
  () => {
    const focus = createSpy();
    const textarea = { focus };
    const component = shallowRender();
    const instance = component.instance();
    component.setState({ shouldFocusTextArea: false });
    instance.focusTextArea(textarea);
    expect(focus).toNotHaveBeenCalled();
  },
);

// ------------------------------ handleClickCancel ------------------------------

test('handleNewNoteCancel updates sets the component status to COMPOSER_HIDDEN', () => {
  const component = shallowRender();
  const instance = component.instance();
  component.setState({ composerStatus: COMPOSER_VISIBLE, selectedRecipients: [{ id: '123' }] });
  const preventDefault = createSpy();
  instance.handleNewNoteCancel({ preventDefault });
  expect(preventDefault).toHaveBeenCalled();
  // hides composer
  expect(component.state('composerStatus')).toEqual(COMPOSER_HIDDEN);
  // also clears the selected recipients
  expect(component.state('selectedRecipients')).toEqual([]);
  // clear should focus
  expect(component.state('shouldFocusTextArea')).toEqual(false);
});

// ------------------------------ handleNewNoteClick ------------------------------

test('handleNewNoteClick scrolls to the note editor and loads the recipients', () => {
  const loadCaseRecipients = createSpy();
  const props = {
    ...defaultProps,
    loadCaseRecipients,
  };
  const component = shallowRender(props);
  const instance = component.instance();
  const scrollToNoteCreator = spyOn(instance, 'scrollToNoteCreator');
  instance.handleNewNoteClick();
  expect(scrollToNoteCreator).toHaveBeenCalled();
  expect(loadCaseRecipients).toHaveBeenCalled(caseInfo.id);

  // also works when we're in the error state
  component.setState({ composerStatus: COMPOSER_ERROR });
  instance.handleNewNoteClick();
  expect(scrollToNoteCreator).toHaveBeenCalled();
  expect(loadCaseRecipients).toHaveBeenCalled(caseInfo.id);
});

test('handleNewNoteClick sets composerStatus to COMPOSER_VISIBLE if it is COMPOSER_HIDDEN', () => {
  const component = shallowRender();
  const instance = component.instance();
  component.setState({ composerStatus: COMPOSER_HIDDEN });
  instance.handleNewNoteClick();
  expect(component.state('composerStatus')).toEqual(COMPOSER_VISIBLE);
});

test('handleNewNoteClick sets composerStatus to COMPOSER_VISIBLE', () => {
  const component = shallowRender();
  const instance = component.instance();
  const testStatus = 'COMPOSER_INVISIBLE';
  component.setState({ composerStatus: testStatus });
  instance.handleNewNoteClick();
  expect(component.state('composerStatus')).toEqual(COMPOSER_VISIBLE);
});

test('handleNewNoteClick focuses the note message field if there is a selected recipient', () => {
  const component = shallowRender();
  const instance = component.instance();
  component.setState({
    selectedRecipients,
    shouldFocusTextArea: false,
  });
  instance.handleNewNoteClick();
  expect(component.state('shouldFocusTextArea')).toBe(true);
});

test('handleNewNoteClick does not focus the note message field if there is no recipient', () => {
  const component = shallowRender();
  const instance = component.instance();
  component.setState({
    selectedRecipients: [],
    shouldFocusTextArea: false,
  });
  instance.handleNewNoteClick();
  expect(component.state('shouldFocusTextArea')).toBe(false);
});

// ------------------------------ handleNoteCreate ------------------------------

test('handleNoteCreate calls postCaseNote prop function with correct payload', () => {
  const postCaseNote = createSpy();
  const testProps = {
    ...defaultProps,
    postCaseNote,
  };
  const message = 'some note';
  const component = shallowRender(testProps);
  const instance = component.instance();
  component.setState({ selectedRecipients });
  instance.handleNoteCreate(message);
  expect(postCaseNote).toHaveBeenCalledWith({
    caseId: defaultProps.caseInfo.id,
    params: {
      recipients: selectedRecipientsforApi(selectedRecipients),
      message,
    },
  });

  // also resets the recipients field & composerStatus
  expect(component.state('composerStatus')).toBe(COMPOSER_HIDDEN);
  expect(component.state('selectedRecipients')).toEqual([]);
  expect(component.state('shouldFocusTextArea')).toEqual(false);
});

// ------------------------------ handleExpandNote ------------------------------

test('handleExpandNote sets a new note expanded value in state', () => {
  const component = shallowRender();
  const instance = component.instance();
  instance.handleExpandNote(123, false);
  expect(component.state('notesExpanded')).toContain({
    'noteExpanded-123': false,
  });
});

test('handleExpandNote updates an existing note expanded value in state', () => {
  const component = shallowRender();
  const instance = component.instance();
  instance.handleExpandNote(123, false);
  expect(component.state('notesExpanded')).toContain({
    'noteExpanded-123': false,
  });
  instance.handleExpandNote(123, true);
  expect(component.state('notesExpanded')).toContain({
    'noteExpanded-123': true,
  });
  expect(Object.keys(component.state('notesExpanded')).length).toEqual(1);
});

// ------------------------------ handleNoteOptionChange ------------------------------

test('handleNoteOptionChange updates all noteExpanded states', () => {
  const component = shallowRender({
    ...defaultProps,
    caseNotes: {
      data: [
        { id: 123 },
        { id: 456 },
        { id: 789 },
      ],
      error: null,
      requesting: false,
    },
  });
  const instance = component.instance();
  instance.handleExpandNote(123, false);
  instance.handleExpandNote(456, true);
  instance.handleExpandNote(789, false);
  instance.handleNoteOptionChange({ value: 'expand' });
  const notesExpanded = component.state('notesExpanded');
  expect(Object.keys(notesExpanded).length).toEqual(3);
  mapKeys(notesExpanded, (expanded) => {
    expect(expanded).toEqual(true);
  });
  expect(component.state('selectedNoteValue')).toEqual(null);
});

// ------------------------------ handleNoteSortOptionChange ------------------------------

test('handleNoteSortOptionChange updates state', () => {
  const component = shallowRender();
  const instance = component.instance();
  instance.handleNoteSortOptionChange({ value: 'test' });
  expect(component.state('selectedNoteSortValue')).toEqual('test');
});

// ------------------------------ handleChangeRecipient ------------------------------

test('handleRecipientChange updates state with the new value', () => {
  const value = [{
    someRecipient: 'some value',
  }];
  const component = shallowRender();
  const instance = component.instance();
  instance.handleRecipientChange(value);
  expect(component.state('selectedRecipients')).toEqual(value);
});

// ------------------------------ handleReply ------------------------------

test('handleReply appends new recipients to selectedRecipients and de-duplicates them', () => {
  const component = shallowRender();
  const instance = component.instance();
  const handleNewNoteClickSpy = spyOn(instance, 'handleNewNoteClick');
  const previousRecipients = [
    {
      companyName: 'Volvo',
      groupId: '12',
      isInTree: true,
      label: 'Bill Lentz',
      userId: '333',
      value: '333', // THIS IS A DUPLICATE RECIPIENT
    },
    {
      companyName: 'Volvo',
      groupId: '12',
      isInTree: true,
      label: 'Tom Cruise',
      userId: '999',
      value: '999',
    },
  ];
  component.setState({ selectedRecipients: previousRecipients });
  instance.handleReply({ recipients: caseRecipients.data });
  previousRecipients.forEach(recipient =>
    expect(component.state('selectedRecipients')).toInclude(recipient));
  selectedRecipients.forEach(recipient =>
    expect(component.state('selectedRecipients')).toInclude(recipient));
  expect(component.state('selectedRecipients').length).toEqual(4); // de-duplication check

  // also focuses the text area
  expect(component.state('shouldFocusTextArea')).toEqual(true);
  expect(handleNewNoteClickSpy).toHaveBeenCalled();
});

// ------------------------------ handleStatusToggle ------------------------------

test('handleStatusToggle calls handleUpdateNoteStatus with inverted status', () => {
  const component = shallowRender();
  const instance = component.instance();
  const handleUpdateNoteStatusSpy = spyOn(instance, 'handleUpdateNoteStatus');
  instance.handleStatusToggle({ id: '123', status: 'unread' });
  expect(handleUpdateNoteStatusSpy).toHaveBeenCalledWith('123', 'read');

  instance.handleStatusToggle({ id: '456', status: 'read' });
  expect(handleUpdateNoteStatusSpy).toHaveBeenCalledWith('456', 'unread');
});

// ------------------------------ handleUpdateNoteStatus ------------------------------

test('handleUpdateNoteStatus calls `updateCaseNote` with expected data', () => {
  const updateCaseNote = createSpy();
  const component = shallowRender({ ...defaultProps, updateCaseNote });
  const instance = component.instance();
  instance.handleUpdateNoteStatus('123', 'read');
  expect(updateCaseNote).toHaveBeenCalledWith({
    caseId: defaultProps.caseInfo.id,
    noteId: '123',
    status: 'read',
  });
});

test('renders NoNotes when there is no case note.', () => {
  const component = shallowRender({
    ...defaultProps,
    caseInfo: {
      ...caseInfo,
      notesCount: 0,
    },
    caseNotes: {
      data: [],
      error: null,
      requesting: false,
    },
  });

  expect(component).toContain('NoNotes');
});

test('renders number of GhostNotes same to notesCount when it is less than 9 while it is being requested', () => {
  const notesCount = 8;
  const component = shallowRender({
    ...defaultProps,
    caseInfo: {
      ...caseInfo,
      notesCount,
    },
    caseNotes: {
      data: [],
      error: null,
      requesting: false,
    },
  });
  expect(component.find('GhostNote').length).toEqual(notesCount);
});

test('renders 5 GhostNotes when it is greater than 8 while it is being requested', () => {
  const notesCount = 9;
  const component = shallowRender({
    ...defaultProps,
    caseInfo: {
      ...caseInfo,
      notesCount,
    },
    caseNotes: {
      data: [],
      error: null,
      requesting: false,
    },
  });

  expect(component.find('GhostNote').length).toEqual(5);
});

test('renders a Note with border for read, read-only notes', () => {
  const component = shallowRender();
  const borderIndex = caseNotes.data.findIndex((note, index) => {
    const nextNote = caseNotes.data[index + 1];
    if (nextNote && note.status === nextNote.status
      && note.status !== 'unread') {
      return true;
    }
    return false;
  });
  expect(component.find(Note).at(borderIndex)).toHaveProp('borderBottom', true);
});

test('modal should scroll to unread note if present and scrollToUnreadNote is true', () => {
  const scrollToSpy = spyOn(scroller, 'scrollTo');
  const component = shallowRender({
    ...defaultProps,
    type: 'modal',
    scrollToUnreadNote: true,
  });
  component.instance().componentDidUpdate(defaultProps);
  expect(scrollToSpy).toHaveBeenCalled();
});

test('modal should not scroll to unread note if present and scrollToUnreadNote is false', () => {
  const scrollToSpy = spyOn(scroller, 'scrollTo');
  const component = shallowRender({
    ...defaultProps,
    type: 'modal',
  });
  component.instance().componentDidUpdate(defaultProps);
  expect(scrollToSpy).toNotHaveBeenCalled();
});

test('modal should not scroll if unread note is not present and scrollToUnreadNote is true', () => {
  const scrollToSpy = spyOn(scroller, 'scrollTo');
  const component = shallowRender({
    ...defaultProps,
    type: 'modal',
    caseNotes: {
      data: [],
      error: null,
      requesting: false,
    },
    scrollToUnreadNote: true,
  });
  component.instance().componentDidUpdate(defaultProps);
  expect(scrollToSpy).toNotHaveBeenCalled();
});

test('add_note modal should show composer', () => {
  const component = shallowRender({
    ...defaultProps,
    type: 'modal',
    action: 'add_note',
  });
  expect(component).toHaveState({ composerStatus: COMPOSER_VISIBLE });
});

// ------------------------------ oldNotesCollapsed ------------------------------
test('oldNotesCollapsed should be false by default if notes length is not greater than 8.', () => {
  const component = shallowRender();
  expect(component).toHaveState({ oldNotesCollapsed: false });
});

test('oldNotesCollapsed should be true by default if notes length becomes greater than 8.', () => {
  const component = shallowRender({
    ...defaultProps,
    caseNotes: {
      ...defaultProps.caseNotes,
      data: [],
    },
  });
  component.setProps({
    caseNotes: {
      ...defaultProps.caseNotes,
      data: manyNotes,
    },
  });
  expect(component).toHaveState({ oldNotesCollapsed: true });
});

// ------------------------------ ExpandableHeader ------------------------------
test('ExpandableHeader includes proper text when notes length becomes greater than 8.', () => {
  const component = shallowRender({
    ...defaultProps,
    caseNotes: {
      ...defaultProps.caseNotes,
      data: [],
    },
  });
  component.setProps({
    caseNotes: {
      ...defaultProps.caseNotes,
      data: manyNotes,
    },
  });
  const Row = component.find('withSize(Row)');
  const formattedMessages = Row.find('FormattedMessage');
  const props = formattedMessages.first().props();
  expect(formattedMessages.first()).toHaveProps({ ...messages.oldNotesCollapsedHeader });
  expect(props.values).toEqual({ count: manyNotes.length - 5 });
});

// ------------------------------ Expandable ------------------------------
test('Expandable has proper props.', () => {
  const component = shallowRender();
  component.setState({ oldNotesCollapsed: true });
  expect(component.find('Connect(Expandable)')).toHaveProps({
    showHeaderWhenExpanded: false,
    isExpanded: false,
    hasExpandIcon: false,
  });
  component.setState({ oldNotesCollapsed: false });
  expect(component.find('Connect(Expandable)')).toHaveProps({
    showHeaderWhenExpanded: false,
    isExpanded: true,
    hasExpandIcon: false,
  });
});
