import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import NotesWidgetHeader from '../NotesWidgetHeader';

const defaultProps = {
  caseInfo: {
    unreadNotesCount: 0,
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<NotesWidgetHeader {...props} />);
}

test('formatted message gets the correct value when case has no unread notes', () => {
  const component = shallowRender();
  const {
    values: {
      count,
    },
  } = component.find('FormattedMessage').props();
  expect(count).toEqual('');
});

test('formatted message gets the correct value when case has no read notes', () => {
  const testProps = {
    caseInfo: {
      unreadNotesCount: 4,
    },
  };
  const component = shallowRender(testProps);
  const {
    values: {
      count,
    },
  } = component.find('FormattedMessage').props();
  expect(count).toEqual(' (4 unread)');
});
