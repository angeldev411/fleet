import {
  find,
  isEmpty,
  set,
  sortBy,
  uniqBy,
} from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { scroller, Element } from 'react-scroll';
import { withTheme } from 'styled-components';
import { Column, Row, Container } from 'styled-components-reactive-grid';
import { QuickActionButton } from 'base-components';

// TODO: Find a way to handle this that removes cross component dependency!
// Perhaps a app wide component registry, as has been suggested for
// generalized favorites & overview pages.
import {
  ADD_NOTE,
} from 'containers/QuickActionBarContainer/constants';

import Expandable, { ExpandableHeader, ExpandableContent } from 'containers/Expandable';

import Divider from 'elements/Divider';
import Widget from 'elements/Widget';

import {
  COMPOSER_ERROR,
  COMPOSER_HIDDEN,
  COMPOSER_INVISIBLE,
  COMPOSER_NO_RECIPIENTS,
  COMPOSER_VISIBLE,
  SCROLLABLE_COMPOSER_NAME,
} from './constants';

import withConnectedData from './Container';

import NotesWidgetHeader from './NotesWidgetHeader';
import Note from './Note';
import NoteCreator from './NoteCreator';
import NotesOptionsBar from './NotesOptionsBar';
import NoNotes from './NoNotes';
import GhostNote from './GhostNote';

import messages from './messages';

import {
  buildRecipientOptions,
  selectedRecipientsforApi,
} from './utils';

function buildSelectedRecipients(selectedRecipients, noteRecipients) {
  const selectedRecipientsFromNote = noteRecipients.reduce((acc, group) =>
    acc.concat(
      group.options.map(recipient => ({ ...recipient, isInTree: true })),
    ), []);
  const allSelectedRecipients = selectedRecipients.concat(selectedRecipientsFromNote);
  return uniqBy(allSelectedRecipients, 'userId');
}

function generateNoteExpandKey(noteId) {
  return `noteExpanded-${noteId}`;
}

function getCurrentOption(options, value) {
  return find(options, opt => opt.value === value);
}

function sortNotes(caseNotes, sortValue) {
  const ascendingCaseNotes = sortBy(caseNotes, note => note.sentAt);
  if (sortValue === 'desc') {
    return ascendingCaseNotes.reverse();
  }
  return ascendingCaseNotes;
}

function filterUnreadNotes(caseNotes) {
  return caseNotes.filter(note => note.status === 'unread');
}

export class NotesWidget extends Component {
  static propTypes = {
    actionBarItemClicked: PropTypes.shape({
      buttonId: PropTypes.string,
      dropdownItemId: PropTypes.string,
      time: PropTypes.number,
    }),
    componentsExpanded: PropTypes.shape().isRequired,
    caseInfo: PropTypes.shape({
      id: PropTypes.string,
      unreadNotesCount: PropTypes.number,
    }).isRequired,
    caseNotes: PropTypes.shape({
      requesting: PropTypes.bool.isRequired,
      error: PropTypes.object,
      data: PropTypes.arrayOf(
        PropTypes.object,
      ).isRequired,
    }).isRequired,
    caseRecipients: PropTypes.shape({
      requesting: PropTypes.bool.isRequired,
      error: PropTypes.object,
      data: PropTypes.arrayOf(
        PropTypes.shape({
          group: PropTypes.shape({
            id: PropTypes.string.isRequired,
          }),
          user: PropTypes.shape({
            id: PropTypes.string.isRequired,
          }),
        }),
      ).isRequired,
    }).isRequired,
    clearCaseRecipients: PropTypes.func.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    loadCaseNotes: PropTypes.func.isRequired,
    loadCaseRecipients: PropTypes.func.isRequired,
    postCaseNote: PropTypes.func.isRequired,
    setCaseNotes: PropTypes.func.isRequired,
    updateCaseNote: PropTypes.func.isRequired,
    setComponentExpanded: PropTypes.func.isRequired,
    theme: PropTypes.shape({
      dimensions: PropTypes.shape({
        topNavHeightInPixel: PropTypes.number.isRequired,
      }).isRequired,
    }).isRequired,
    type: PropTypes.oneOf([
      undefined,
      'modal',
    ]),
    action: PropTypes.oneOf([
      undefined,
      'add_note',
    ]),
    scrollToUnreadNote: PropTypes.bool,
    updateScrollToUnreadNote: PropTypes.func.isRequired,
  };

  static defaultProps = {
    actionBarItemClicked: {},
    type: undefined,
    scrollToUnreadNote: false,
    action: undefined,
  };

  state = {
    composerStatus: COMPOSER_INVISIBLE,
    notesExpanded: {},
    selectedRecipients: [],
    shouldFocusTextArea: false,
    selectedNoteValue: '',
    selectedNoteSortValue: 'asc',
    oldNotesCollapsed: false,
  };

  componentWillMount() {
    const {
      caseInfo: {
        id,
        notesCount,
      },
      type,
      action,
      updateScrollToUnreadNote,
      loadCaseNotes,
    } = this.props;

    if (type === 'modal') {
      if (action === 'add_note') {
        this.setState({ composerStatus: COMPOSER_VISIBLE });
      } else {
        updateScrollToUnreadNote(true);
      }
    }

    if (id && notesCount) {
      loadCaseNotes(id);
    }
  }

  componentWillReceiveProps({
    actionBarItemClicked,
    caseInfo: {
      id,
      notesCount,
    },
    caseNotes,
    caseRecipients,
    setComponentExpanded,
    componentsExpanded,
  }) {
    if (!this.props.caseInfo.id && id && notesCount) {
      this.props.loadCaseNotes(id);
    }

    if (!this.props.caseNotes.data.length && caseNotes.data.length > 8) {
      this.setState({ oldNotesCollapsed: true });
    }

    const recipientsChanging = caseRecipients !== this.props.caseRecipients;
    const requestInProgress = caseRecipients.requesting;
    if (this.state.hasRequestedRecipients && recipientsChanging && !requestInProgress) {
      const error = caseRecipients.error;
      const recipientData = caseRecipients.data;
      if (error || !recipientData) {
        this.setState({ composerStatus: COMPOSER_ERROR });
      } else if (isEmpty(recipientData)) {
        this.setState({ composerStatus: COMPOSER_NO_RECIPIENTS });
      } else {
        this.setState({ composerStatus: COMPOSER_VISIBLE });
      }
    }

    /* The logic here is complicated but required
      We should expand the component only when
      notesWidget was not expanded and button 'ADD NOTE' is just clicked.
      If notesWidget is expanded just before and 'ADD NOTE' is clicked,
      we call handleNoteClick which scrolls you to new note area which is ready to be written.
    */
    const buttonId = actionBarItemClicked && actionBarItemClicked.buttonId;
    const actionTime = actionBarItemClicked && actionBarItemClicked.time;
    const addingNote = buttonId === ADD_NOTE;
    const buttonWasJustClicked = (
      buttonId !== this.props.actionBarItemClicked.buttonId ||
      actionTime !== this.props.actionBarItemClicked.time
    );
    const notesWidgetExpanded = componentsExpanded && componentsExpanded['widget-notes'];
    const notesWidgetWasJustExpanded = (
      notesWidgetExpanded &&
      notesWidgetExpanded !== this.props.componentsExpanded['widget-notes']
    );
    const shouldExpandNotesWidget = !notesWidgetExpanded && buttonWasJustClicked;
    const shouldHandleNewNoteClick = notesWidgetExpanded && (
      buttonWasJustClicked || notesWidgetWasJustExpanded
    );
    if (addingNote) {
      if (shouldExpandNotesWidget) {
        setComponentExpanded('widget-notes', true);
      } else if (shouldHandleNewNoteClick) {
        this.handleNewNoteClick();
      }
    }
  }

  componentDidUpdate(prevProps) {
    const { type, action, caseNotes: { requesting, data } } = this.props;
    /* When it's a modal to add a note from cases page */
    if (!requesting && prevProps.caseNotes.requesting && data.length) {
      if (type === 'modal' && action === 'add_note') {
        /* right after the notes are loaded there are not yet rendered so we need wait a bit
          to scroll to note creator.
        */
        this.handleNewNoteClick();
        return;
      }
    }
    this.scrollToUnreadNote();
  }

  componentWillUnmount() {
    this.props.clearCaseRecipients();
    this.props.setCaseNotes();
    this.props.updateScrollToUnreadNote(false);
  }

  focusTextArea = (textarea) => {
    if (textarea && this.state.shouldFocusTextArea) {
      textarea.focus();
      this.setState({ shouldFocusTextArea: false });
    }
  }

  handleExpandNote = (id, expanded) => {
    const noteExpandKey = generateNoteExpandKey(id);
    this.setState({
      notesExpanded: {
        ...this.state.notesExpanded,
        [noteExpandKey]: expanded,
      },
    });
  }

  handleExpandOldNotes = () => {
    this.setState({ oldNotesCollapsed: false });
  }

  handleNewNoteCancel = (event) => {
    event.preventDefault();
    this.setState({
      composerStatus: COMPOSER_HIDDEN,
      selectedRecipients: [],
      shouldFocusTextArea: false,
    });
  }

  handleNewNoteClick = () => {
    const {
      caseInfo,
      loadCaseRecipients,
    } = this.props;
    const {
      composerStatus,
      selectedRecipients,
    } = this.state;

    this.scrollToNoteCreator();

    if (composerStatus === COMPOSER_INVISIBLE || composerStatus === COMPOSER_ERROR) {
      loadCaseRecipients(caseInfo.id);
      this.setState({ hasRequestedRecipients: true });
    }

    this.setState({ composerStatus: COMPOSER_VISIBLE }); // Batch state update makes this fine

    if (selectedRecipients.length) {
      this.setState({
        shouldFocusTextArea: true,
      });
    }
  }

  handleNoteCreate = (message) => {
    const { caseInfo, postCaseNote } = this.props;
    const { selectedRecipients } = this.state;
    const recipients = selectedRecipientsforApi(selectedRecipients);
    const payload = {
      caseId: caseInfo.id,
      params: {
        recipients,
        message,
      },
    };
    this.setState({
      composerStatus: COMPOSER_HIDDEN,
      selectedRecipients: [],
      shouldFocusTextArea: false,
    });
    postCaseNote(payload);
  }

  handleNoteOptionChange = ({ value }) => {
    const { caseNotes: { data: notes } } = this.props;
    const notesExpanded = notes.reduce((acc, note) => {
      const noteExpandKey = generateNoteExpandKey(note.id);
      set(acc, noteExpandKey, value === 'expand');
      return acc;
    }, {});
    this.setState({
      notesExpanded,
      selectedNoteValue: null,
    });
  }

  handleNoteSortOptionChange = ({ value }) => {
    this.setState({ selectedNoteSortValue: value });
  }

  handleRecipientChange = (value) => {
    this.setState({
      selectedRecipients: value,
    });
  }

  handleReply = (note) => {
    const { selectedRecipients: oldSelectedRecipients } = this.state;
    const noteRecipients = buildRecipientOptions(note.recipients);
    const selectedRecipients = buildSelectedRecipients(oldSelectedRecipients, noteRecipients);
    this.setState({
      selectedRecipients,
      shouldFocusTextArea: true,
    });
    this.handleNewNoteClick();
  }

  handleStatusToggle = ({ id: noteId, status: oldStatus }) => {
    const status = oldStatus === 'read' ? 'unread' : 'read';
    this.handleUpdateNoteStatus(noteId, status);
  }

  handleUpdateNoteStatus = (noteId, status) => {
    this.props.updateCaseNote({
      caseId: this.props.caseInfo.id,
      noteId,
      status,
    });
  }

  notesOptions = () => {
    const {
      intl: {
        formatMessage,
      },
    } = this.props;
    return [
      {
        value: 'expand',
        label: formatMessage(messages.expandAll),
      },
      {
        value: 'collapse',
        label: formatMessage(messages.collapseAll),
      },
    ];
  }

  notesSortOptions = () => {
    const {
      intl: {
        formatMessage,
      },
    } = this.props;
    return [
      {
        value: 'asc',
        label: formatMessage(messages.oldToNew),
      },
      {
        value: 'desc',
        label: formatMessage(messages.newToOld),
      },
    ];
  }

  scrollToNoteCreator = () => {
    const options = {
      duration: 1000,
      smooth: true,
      offset: -this.props.theme.dimensions.topNavHeightInPixel - 10, // adding some more space
    };

    if (this.props.type === 'modal') {
      options.containerId = 'modal-body';
    }

    scroller.scrollTo(SCROLLABLE_COMPOSER_NAME, options);
  }

  scrollToUnreadNote = () => {
    if (this.props.scrollToUnreadNote && this.props.caseNotes.data.length) {
      const sortedNotes = sortNotes(
        this.props.caseNotes.data,
        this.state.selectedNoteSortValue,
      );
      const firstUnreadNote = filterUnreadNotes(sortedNotes)[0];

      if (firstUnreadNote) {
        const options = {
          duration: 1500,
          smooth: true,
          delay: 200,
        };

        if (this.props.type === 'modal') {
          options.containerId = 'modal-body';
        } else {
          options.offset = -this.props.theme.dimensions.topNavHeightInPixel;
        }

        scroller.scrollTo(firstUnreadNote.id, options);
      }
      this.props.updateScrollToUnreadNote(false);
    }
  }

  render() {
    const {
      caseInfo,
      caseNotes,
      caseRecipients,
    } = this.props;

    const {
      composerStatus,
      notesExpanded,
      selectedNoteSortValue,
      selectedNoteValue,
      selectedRecipients,
      oldNotesCollapsed,
    } = this.state;

    const notesOptions = this.notesOptions();
    const notesSortOptions = this.notesSortOptions();

    const selectedNoteOption = getCurrentOption(notesOptions, selectedNoteValue);
    const selectedNoteSortOption = getCurrentOption(notesSortOptions, selectedNoteSortValue);

    const sortedCaseNotes = sortNotes(caseNotes.data, selectedNoteSortValue);
    const shouldCollapseNotes = sortedCaseNotes.length > 8;
    const ascendingOrder = selectedNoteSortValue === 'asc';
    let shownNotes;
    let collapsedNotes;
    if (shouldCollapseNotes) {
      if (ascendingOrder) {
        shownNotes = sortedCaseNotes.slice(-5);
        collapsedNotes = sortedCaseNotes.slice(0, -5);
      } else {
        shownNotes = sortedCaseNotes.slice(0, 5);
        collapsedNotes = sortedCaseNotes.slice(5);
      }
    } else {
      shownNotes = sortedCaseNotes;
      collapsedNotes = [];
    }
    const collapsedTitle = (
      <Container>
        <Row modifiers={['center']}>
          <Column modifiers={['col']}>
            <QuickActionButton
              onClick={this.handleExpandOldNotes}
            >
              <FormattedMessage
                {...messages.oldNotesCollapsedHeader}
                values={{ count: collapsedNotes.length }}
              />
            </QuickActionButton>
          </Column>
        </Row>
      </Container>
    );
    const collapsedNotesContent = (
      <Expandable
        isExpanded={!oldNotesCollapsed}
        showHeaderWhenExpanded={false}
        hasExpandIcon={false}
      >
        <ExpandableHeader>{collapsedTitle}</ExpandableHeader>
        <ExpandableContent>
          {
            collapsedNotes.map((note, index) => {
              const noteExpandKey = generateNoteExpandKey(note.id);
              const nextNoteIndex = index + 1 + (ascendingOrder ? 0 : shownNotes.length);
              const nextNote = sortedCaseNotes[nextNoteIndex];
              let borderBottom = false;
              if (nextNote && note.status === nextNote.status
                && note.status !== 'unread') {
                borderBottom = true;
              }
              return (
                <Note
                  key={note.id}
                  expandKey={noteExpandKey}
                  handleReply={this.handleReply}
                  handleStatusToggle={this.handleStatusToggle}
                  isExpanded={notesExpanded[noteExpandKey]}
                  note={note}
                  onExpand={this.handleExpandNote}
                  updateNoteStatus={this.handleUpdateNoteStatus}
                  borderBottom={borderBottom}
                />
              );
            })
          }
        </ExpandableContent>
      </Expandable>
    );

    // If notesCount is less than 9, show number of GhostNotes equal to notesCount
    // Otherwise, show 5
    const ghostNotesCount = caseInfo.notesCount < 9 ? caseInfo.notesCount : 5;
    const noNotesContent = (
      caseInfo.notesCount > 0
        ? (
          <div>
            {
              Array.from(new Array(ghostNotesCount), (x, i) => (
                <GhostNote key={i} />
              ))
            }
          </div>
        ) : (
          <div>
            <NoNotes requesting={caseNotes.requesting} />
            <Divider modifiers={['light']} />
          </div>
        )
    );
    const shownNotesContent = (
      caseNotes.data.length > 0
        ? (
          shownNotes.map((note, index) => {
            const noteExpandKey = generateNoteExpandKey(note.id);
            const nextNoteIndex = index + 1 + (ascendingOrder ? collapsedNotes.length : 0);
            const nextNote = sortedCaseNotes[nextNoteIndex];
            let borderBottom = false;
            if (nextNote && note.status === nextNote.status
              && note.status !== 'unread') {
              borderBottom = true;
            }
            return (
              <Note
                key={note.id}
                expandKey={noteExpandKey}
                handleReply={this.handleReply}
                handleStatusToggle={this.handleStatusToggle}
                isExpanded={notesExpanded[noteExpandKey]}
                note={note}
                onExpand={this.handleExpandNote}
                updateNoteStatus={this.handleUpdateNoteStatus}
                borderBottom={borderBottom}
              />
            );
          })
        ) : noNotesContent
    );

    const NotesWidgetBody = (
      <div>
        <NotesOptionsBar
          handleNewNoteClick={this.handleNewNoteClick}
          handleNoteOptionChange={this.handleNoteOptionChange}
          handleNoteSortOptionChange={this.handleNoteSortOptionChange}
          notesOptions={notesOptions}
          notesSortOptions={notesSortOptions}
          selectedNoteOption={selectedNoteOption}
          selectedNoteSortOption={selectedNoteSortOption}
        />
        {!ascendingOrder && shownNotesContent}
        {collapsedNotesContent}
        {ascendingOrder && shownNotesContent}
        <Container />
        <Element className="react-scroll-element NoteCreator" name={SCROLLABLE_COMPOSER_NAME}>
          <NoteCreator
            caseRecipients={caseRecipients}
            composerStatus={composerStatus}
            focusTextArea={this.focusTextArea}
            handleNewNoteCancel={this.handleNewNoteCancel}
            handleNewNoteClick={this.handleNewNoteClick}
            handleNoteCreate={this.handleNoteCreate}
            handleRecipientChange={this.handleRecipientChange}
            selectedRecipients={selectedRecipients}
          />
        </Element>
      </div>
    );

    if (this.props.type === 'modal') {
      return NotesWidgetBody;
    }

    return (
      <Widget className="NotesWidget" id="notes" expandKey="notes">
        <Widget.Header>
          <NotesWidgetHeader caseInfo={caseInfo} />
        </Widget.Header>
        <Widget.Item>
          {NotesWidgetBody}
        </Widget.Item>
      </Widget>
    );
  }
}

export NotesWidgetHeader from './NotesWidgetHeader';
export default withConnectedData(
  withTheme(
    injectIntl(NotesWidget),
  ),
);
