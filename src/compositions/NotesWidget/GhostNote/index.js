import { GhostIndicator } from 'base-components';
import React from 'react';
import {
  Container,
  Column,
  Row,
} from 'styled-components-reactive-grid';

import Gravatar from 'elements/Gravatar';

function GhostNote() {
  return (
    <Container>
      <Row>
        <Column modifiers={['center']} style={{ maxWidth: '44px' }}>
          <Gravatar ghost />
        </Column>
        <Column modifiers={['col']}>
          <Row>
            <Column modifiers={['col_5']}>
              <GhostIndicator />
            </Column>
          </Row>
          <Row>
            <Column modifiers={['col_12']}>
              <GhostIndicator />
              <GhostIndicator />
            </Column>
          </Row>
        </Column>
      </Row>
    </Container>
  );
}

export default GhostNote;
