import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import GhostNote from '../index';

function renderComponent() {
  return shallow(<GhostNote />);
}

test('renders a Gravatar with a ghost prop', () => {
  const component = renderComponent();
  expect(component).toContain('Gravatar');
  expect(component.find('Gravatar')).toHaveProp('ghost', true);
});

test('renders 3 GhostIndicator components.', () => {
  const component = renderComponent();
  expect(component.find('GhostIndicator').length).toEqual(3);
});
