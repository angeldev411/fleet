import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';

import {
  clearCaseRecipients,
  loadCaseNotes,
  loadCaseRecipients,
  postCaseNote,
  setCaseNotes,
  updateCaseNote,
} from 'redux/cases/actions';
import {
  setComponentExpanded as setComponentExpandedAction,
  updateScrollToUnreadNote,
} from 'redux/ui/actions';
import {
  caseNotesSelector,
  caseRecipientsSelector,
  currentCaseSelector,
} from 'redux/cases/selectors';
import {
  actionBarItemClickedSelector,
  componentsExpandedSelector,
  scrollToUnreadNoteSelector,
} from 'redux/ui/selectors';

const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    actionBarItemClicked: PropTypes.shape({
      buttonId: PropTypes.string,
      dropdownItemId: PropTypes.string,
    }),
    caseInfo: PropTypes.shape({
      id: PropTypes.string,
      unreadNotesCount: PropTypes.number,
    }).isRequired,
    caseNotes: PropTypes.shape({
      requesting: PropTypes.bool.isRequired,
      error: PropTypes.object,
      data: PropTypes.arrayOf(
        PropTypes.object,
      ).isRequired,
    }).isRequired,
    caseRecipients: PropTypes.shape({
      requesting: PropTypes.bool.isRequired,
      error: PropTypes.object,
      data: PropTypes.arrayOf(
        PropTypes.shape({
          group: PropTypes.shape({
            id: PropTypes.string.isRequired,
          }),
          user: PropTypes.shape({
            id: PropTypes.string.isRequired,
          }),
        }),
      ).isRequired,
    }).isRequired,
    clearCaseRecipients: PropTypes.func.isRequired,
    loadCaseNotes: PropTypes.func.isRequired,
    loadCaseRecipients: PropTypes.func.isRequired,
    postCaseNote: PropTypes.func.isRequired,
    setCaseNotes: PropTypes.func.isRequired,
    updateCaseNote: PropTypes.func.isRequired,
    scrollToUnreadNote: PropTypes.bool,
    updateScrollToUnreadNote: PropTypes.func.isRequired,
  };

  Container.defaultProps = {
    actionBarItemClicked: {},
    caseInfo: {},
    caseNotes: {
      requesting: false,
      error: null,
      data: [],
    },
    caseRecipients: {
      requesting: false,
      error: null,
      data: [],
    },
    scrollToUnreadNote: false,
  };

  function mapStateToProps(state, ownProps) {
    return {
      actionBarItemClicked: actionBarItemClickedSelector(state),
      caseInfo: ownProps.caseInfo || currentCaseSelector(state),
      caseNotes: caseNotesSelector(state),
      caseRecipients: caseRecipientsSelector(state),
      componentsExpanded: componentsExpandedSelector(state),
      scrollToUnreadNote: scrollToUnreadNoteSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      clearCaseRecipients: () => dispatch(clearCaseRecipients()),
      loadCaseNotes: caseId => dispatch(loadCaseNotes({ caseId })),
      loadCaseRecipients: caseId => dispatch(loadCaseRecipients({ caseId })),
      postCaseNote: payload => dispatch(postCaseNote(payload)),
      setCaseNotes: caseNotes => dispatch(setCaseNotes(caseNotes)),
      updateCaseNote: params => dispatch(updateCaseNote(params)),
      setComponentExpanded: (expandKey, expanded) =>
        dispatch(setComponentExpandedAction({ expandKey, expanded })),
      updateScrollToUnreadNote: bool => dispatch(updateScrollToUnreadNote(bool)),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
