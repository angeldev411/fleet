import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  Container,
  Column,
  Row,
} from 'styled-components-reactive-grid';
import { px2rem } from 'decisiv-ui-utils';

import TextDiv from 'elements/TextDiv';

import messages from './messages';

function NoNotes({ requesting }) {
  const message = requesting ? 'loadingNotes' : 'noNotesYet';

  return (
    <Container>
      <Row modifiers={['middle']} style={{ height: px2rem(100) }}>
        <Column modifiers={['col', 'center']}>
          <TextDiv modifiers={['midGreyText', 'smallText']}>
            <FormattedMessage {...messages[message]} />
          </TextDiv>
        </Column>
      </Row>
    </Container>
  );
}

NoNotes.propTypes = {
  requesting: PropTypes.bool.isRequired,
};

export default NoNotes;
