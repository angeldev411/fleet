import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';

import {
  cancelServiceRequestErrorSelector,
  cancelServiceRequestPendingSelector,
} from 'redux/requests/selectors';

import {
  cancelServiceRequest,
} from 'redux/serviceRequests/actions';
import {
  currentServiceRequestSelector,
} from 'redux/serviceRequests/selectors';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    cancelServiceRequest: PropTypes.func.isRequired,
    cancelServiceRequestError: PropTypes.shape({
      error: PropTypes.bool.isRequired,
    }),
    cancelServiceRequestPending: PropTypes.bool.isRequired,
    serviceRequestInfo: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
  };

  Container.defaultProps = {
    cancelServiceRequestError: undefined,
  };

  function mapStateToProps(state) {
    return {
      cancelServiceRequestError: cancelServiceRequestErrorSelector(state),
      cancelServiceRequestPending: cancelServiceRequestPendingSelector(state),
      serviceRequestInfo: currentServiceRequestSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      cancelServiceRequest: serviceRequestId =>
        dispatch(cancelServiceRequest({ serviceRequestId })),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
