import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'compositions.CancelServiceRequestButton.title',
    defaultMessage: 'Cancel Request',
  },
});

export default messages;
