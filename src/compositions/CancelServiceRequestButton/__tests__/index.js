import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import { CancelServiceRequestButton } from '../index';

const defaultProps = {
  cancelServiceRequest: noop,
  cancelServiceRequestError: {
    error: true,
    description: 'there was an alien attack',
  },
  cancelServiceRequestPending: true,
  serviceRequestInfo: {
    id: '123',
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<CancelServiceRequestButton {...props} />);
}

// usually we don't do this type of test, but this test is to confirm that
// this component uses `QuickActionButton` from `base-components`
test('renders QuickActionButton with correct props', () => {
  const component = shallowRender();
  expect(component).toBeA('QuickActionButton');
});

test('calls cancelServiceRequest prop on clicking the composition', () => {
  const cancelServiceRequest = createSpy();
  const component = shallowRender({ ...defaultProps, cancelServiceRequest });
  component.simulate('click');
  expect(cancelServiceRequest).toHaveBeenCalledWith('123');
});

test('renders a Redirect if requesting state changes to false and error prop is falsy', () => {
  const nextProps = {
    ...defaultProps,
    cancelServiceRequestError: undefined,
    cancelServiceRequestPending: false,
  };
  const component = shallowRender();
  component.setProps(nextProps);
  expect(component).toBeA('Redirect');
});
