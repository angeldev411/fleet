import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import { QuickActionButton } from 'base-components';
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import withConnectedData from './Container';
import messages from './messages';

export class CancelServiceRequestButton extends Component {
  state = {
    serviceRequestCancelled: false,
  };

  componentWillReceiveProps(nextProps) {
    const { cancelServiceRequestError, cancelServiceRequestPending } = nextProps;
    if (!cancelServiceRequestPending && this.props.cancelServiceRequestPending) {
      if (cancelServiceRequestError) {
        console.log('TODO: Cancel Service Request failed. Implement correct error handling.', cancelServiceRequestError);
      }

      // Sets the success flag as a local state
      this.setState({ serviceRequestCancelled: !cancelServiceRequestError });
    }
  }

  render() {
    const { cancelServiceRequest, serviceRequestInfo } = this.props;
    if (this.state.serviceRequestCancelled) {
      // FIXME: This is a temporary fix - handle the success case better
      return (
        <Redirect to="/service-requests/filter/open-service-request" />
      );
    }
    return (
      <QuickActionButton onClick={() => cancelServiceRequest(serviceRequestInfo.id)}>
        <QuickActionButton.Text>
          <FormattedMessage {...messages.title} />
        </QuickActionButton.Text>
      </QuickActionButton>
    );
  }
}

CancelServiceRequestButton.propTypes = {
  cancelServiceRequest: PropTypes.func.isRequired,
  cancelServiceRequestError: PropTypes.shape({
    error: PropTypes.bool.isRequired,
  }),
  cancelServiceRequestPending: PropTypes.bool.isRequired,
  serviceRequestInfo: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
};

CancelServiceRequestButton.defaultProps = {
  cancelServiceRequestError: undefined,
};

export default withConnectedData(CancelServiceRequestButton);
