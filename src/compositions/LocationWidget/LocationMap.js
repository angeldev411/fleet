import React from 'react';
import PropTypes from 'prop-types';
import { mapValues, get } from 'lodash';
import { injectIntl } from 'react-intl';

import MapWrapper from './MapWrapper';
import Cartographer from './Cartographer';
import messages from './messages';

/**
 * Get the lat/lon location values from the given entity.  The entity is expected to have
 * an object at the given path (e.g. 'address', 'location' mapping to entity.address.location)
 * which contains string-valued latitude and longitude values.
 */
function entityLocationInfo(entity, locationPath) {
  /* istanbul ignore else  */
  if (entity) {
    const location = get(entity, locationPath);
    if (location) {
      // parse the string value lat/lon to floats
      return mapValues(location, val => parseFloat(val));
    }
  }
  return null;
}

/**
 * The LocationMap shows the map part of the Location widget for the given asset and
 * service provider.  An `onDirectionsUpdate` callback can be provided as a prop, which
 * will be called when the driving directions have been calculated between the asset
 * and the service provider.  The directions returned from the Google Maps API will
 * be passed to `onDirectionsUpdate`.
 */
export function LocationMap({
  assetInfo,
  serviceProviderInfo,
  onDirectionsUpdate,
  intl: { formatMessage },
}) {
  const markers = [];
  const assetLocation = entityLocationInfo(assetInfo, ['location']);
  const providerLocation = entityLocationInfo(serviceProviderInfo, ['address', 'location']);

  if (assetLocation) {
    markers.push({
      label: formatMessage(messages.unit, { unitNumber: assetInfo.unitNumber }),
      position: assetLocation,
    });
  }

  if (providerLocation) {
    markers.push({
      label: serviceProviderInfo.companyName,
      position: providerLocation,
    });
  }

  return (assetLocation || providerLocation) &&
    (
      <MapWrapper>
        <Cartographer
          markers={markers}
          onDirectionsUpdate={onDirectionsUpdate}
        />
      </MapWrapper>
    );
}

LocationMap.propTypes = {
  assetInfo: PropTypes.shape({
    location: PropTypes.shape({
      lat: PropTypes.string,
      lon: PropTypes.string,
    }),
  }).isRequired,
  serviceProviderInfo: PropTypes.shape({
    location: PropTypes.shape({
      lat: PropTypes.string,
      lon: PropTypes.string,
    }),
  }).isRequired,
  onDirectionsUpdate: PropTypes.func,
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
};

LocationMap.defaultProps = {
  onDirectionsUpdate: null,
};

export default injectIntl(LocationMap);
