import React from 'react';
import { noop } from 'lodash';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { LocationMap } from '../LocationMap';
import Cartographer from '../Cartographer';

const intlMock = {
  formatMessage: ({ defaultMessage }) => defaultMessage,
};

const defaultProps = {
  assetInfo: {},
  serviceProviderInfo: {},
  onDirectionsUpdate: noop,
  intl: intlMock,
};

function shallowRender(props = defaultProps) {
  return shallow(
    <LocationMap {...props} />,
  );
}

// ------------------------------ basic rendering ------------------------------

test('LocationMap renders a Cartographer if given an asset with a location', () => {
  const assetInfo = { location: { lat: '123', lon: '45' } };
  const component = shallowRender({ ...defaultProps, assetInfo });
  expect(component.find('MapWrapper')).toContain(Cartographer);
});

// ------------------------------ basic props to the Cartographer ------------------------------

function cartographer(component) {
  return component.find('MapWrapper').children().first();
}

function cartographerProps(component) {
  return cartographer(component).props();
}

test('the onDirectionsUpdate function is passed through to the Cartographer', () => {
  const assetInfo = { location: { lat: '123', lon: '45' } };
  const onDirectionsUpdate = () => 'hey-oh!';
  const component = shallowRender({ ...defaultProps, onDirectionsUpdate, assetInfo });
  expect(cartographerProps(component).onDirectionsUpdate).toEqual(onDirectionsUpdate);
});

// ------------------------------ markers for the Cartographer ------------------------------

test('markers are created for the asset and service provider locations', () => {
  const assetUnit = '4077';
  const assetInfo = { unitNumber: assetUnit, location: { lat: '123', lon: '45' } };
  const providerCompany = 'Service Provider';
  const serviceProviderInfo = {
    companyName: providerCompany,
    address: { location: { lat: '66', lon: '42' } },
  };
  const component = shallowRender({ ...defaultProps, assetInfo, serviceProviderInfo });
  const markersProp = cartographerProps(component).markers;
  expect(markersProp.length).toEqual(2);

  // this expectation may seem strange, but we are not importing the
  // internationalized version of the LocationMap for these tests, and therefore
  // the message is not interpolated inline here.
  expect(markersProp[0]).toEqual({ label: 'Unit #{unitNumber}', position: { lat: 123, lon: 45 } });

  expect(markersProp[1]).toEqual({ label: providerCompany, position: { lat: 66, lon: 42 } });
});
