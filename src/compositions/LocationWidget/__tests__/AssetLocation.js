import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import WithGeocoding from 'utils/mapping/WithGeocoding';
import AssetLocation, { RenderAddress } from '../AssetLocation';

const defaultProps = {
  location: { lat: 22.33, lon: 44.55 },
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetLocation {...props} />);
}

test('renders a WithGeocoding component', () => {
  const component = shallowRender();
  // asserting against the actual component, instead of just the name, because the
  // name is something like "withProps(withScriptjs(mapProps(WithGeocoding)))"
  expect(component).toContain(WithGeocoding);
});

test('passes RenderAddress as the render function for WithGeocoding', () => {
  const component = shallowRender();
  const geocoder = component.find(WithGeocoding);
  expect(geocoder.props().render).toExist();
  expect(geocoder.props().render).toEqual(RenderAddress);
});

// ------------ RenderAddress ------------

test('RenderAddress renders the address in a span', () => {
  const address = '123 Broadway';
  const component = shallow(<RenderAddress address={address} />);
  const span = component.find('span');
  expect(span.text()).toEqual(address);
});
