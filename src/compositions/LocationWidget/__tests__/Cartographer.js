import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';
import { filter, noop } from 'lodash';

import { Cartographer } from '../Cartographer';

const defaultProps = {
  markers: [],
  onDirectionsUpdate: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<Cartographer {...props} />);
}

// ------------------------------ basic rendering ------------------------------

test('renders a map for the given markers', () => {
  const lat = 123.21;
  const lon = 45.67;
  const markerOne = {
    position: { lat, lon },
    label: 'First Marker',
  };
  const component = shallowRender({
    ...defaultProps,
    markers: [markerOne],
  });
  const map = component.find('MapWithDirections');
  expect(map.props().markers).toEqual([{
    label: 'First Marker',
    position: { lat, lng: lon },
    showInfo: false,
  }]);
});

test('converts markers to lat/lng format', () => {
  const markers = [
    { position: { lat: 22, lon: 33 }, label: 'One' },
    { position: { lat: 55, lon: 44 }, label: 'Two' },
    { position: { lat: 55, lon: 44 }, label: 'Three' },
  ];
  const component = shallowRender({
    ...defaultProps,
    markers,
  });
  const map = component.find('MapWithDirections');
  expect(map.props().markers).toEqual([
    { label: 'One', position: { lat: 22, lng: 33 }, showInfo: false },
    { label: 'Two', position: { lat: 55, lng: 44 }, showInfo: false },
    { label: 'Three', position: { lat: 55, lng: 44 }, showInfo: false },
  ]);
});

// ------------------------------ showing / hiding marker popups ------------------------------

test('clicking a marker sets showInfo=true, closing it sets showInfo=false', () => {
  const markerOneLabel = 'First Marker';
  const markerTwoLabel = 'Second Marker';
  const markers = [
    { position: { lat: 123, lon: 45 }, label: markerOneLabel },
    { position: { lat: 111, lon: 22 }, label: markerTwoLabel },
  ];

  const component = shallowRender({
    ...defaultProps,
    markers,
  });
  const instance = component.instance();

  const shownMarkers = () => filter(component.state().markers, m => m.showInfo);

  expect(shownMarkers().length).toEqual(0);

  // click the second marker
  instance.handleMarkerClick(component.state().markers[1]);
  const shownMarkersOne = shownMarkers();
  expect(shownMarkersOne.length).toEqual(1);
  expect(shownMarkersOne[0].label).toEqual(markerTwoLabel);

  // click the first marker
  instance.handleMarkerClick(component.state().markers[0]);
  expect(shownMarkers().length).toEqual(2);
  expect(component.state().markers[0].showInfo).toEqual(true);

  // close the second marker
  instance.handleMarkerClose(component.state().markers[1]);

  // this leaves only the first marker open
  const shownMarkersTwo = shownMarkers();
  expect(shownMarkersTwo.length).toEqual(1);
  expect(shownMarkersTwo[0].label).toEqual(markerOneLabel);
});

test('markers are created initially hidden (showInfo=false)', () => {
  const markers = [
    { position: { lat: 123, lon: 45 }, label: 'one' },
    { position: { lat: 111, lon: 22 }, label: 'two' },
  ];

  const component = shallowRender({
    ...defaultProps,
    markers,
  });

  const markerState = component.state.markers;
  expect(filter(markerState, m => m.showInfo).length).toEqual(0);
});
