import React from 'react';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { LocationWidget } from '../index';
import messages from '../messages';

const assetInfo = {};

const serviceProviderInfo = {
  address: {
    location: {
      lat: 123,
      lon: 321,
    },
  },
};

const defaultProps = {
  assetInfo,
  serviceProviderInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(
    <LocationWidget
      assetInfo={props.assetInfo}
      serviceProviderInfo={props.serviceProviderInfo}
    />,
  );
}

// ------------------------------ basic rendering ------------------------------

test('renders a widget with "case-location" expand key', () => {
  const widget = shallowRender();
  expect(widget.find('Widget')).toHaveProp('expandKey', 'case-location');
});

test('renders a LocationMap', () => {
  const widget = shallowRender();
  const locationMap = widget.find('InjectIntl(LocationMap)');
  expect(locationMap).toHaveProps({ assetInfo, serviceProviderInfo });
});

test('renders an AssetLocation with the asset location', () => {
  const component = shallowRender({ assetInfo: {} });
  expect(component.find('AssetLocation').props().location).toEqual({});

  const location = { lat: 'new lat', lng: 'new lng' };
  component.setProps({ assetInfo: { location } });
  expect(component.find('AssetLocation').props().location).toEqual(location);
});

// ------------------------------ handling directions update ------------------------------

const distanceText = '800 miles';
const durationText = '42 minutes';
const testDirections = {
  routes: [{
    legs: [{
      distance: { text: distanceText },
      duration: { text: durationText },
    }],
  }],
};

test('the onDirectionsUpdate function passed to LocationMap updates the directions state', () => {
  const widget = shallowRender();
  const locationMap = widget.find('InjectIntl(LocationMap)');
  const updater = locationMap.prop('onDirectionsUpdate');
  expect(widget).toHaveState({ directions: null });
  updater(testDirections);
  expect(widget).toHaveState({ directions: testDirections });
});

test('once directions are available, the distance and duration are displayed', () => {
  const noDistanceMsg = `EmptyValue FormattedMessage[id="${messages.notAvailable.id}"]`;

  const widget = shallowRender();
  expect(widget).toContain(noDistanceMsg);

  widget.instance().onDirectionsUpdate(testDirections);

  expect(widget).toNotContain(noDistanceMsg);

  const distanceSpan = widget.find('TableRow').last().find('span');
  expect(distanceSpan.text()).toContain(distanceText);
  expect(distanceSpan.text()).toContain(durationText);
});
