import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { isEmpty } from 'lodash';

import Widget from 'elements/Widget';

import { getOutputText } from 'utils/widget';

import messages from './messages';
import LocationMap from './LocationMap';
import AssetLocation from './AssetLocation';
import withConnectedData from './Container';

export class LocationWidget extends Component {
  static propTypes = {
    assetInfo: PropTypes.shape({
      location: PropTypes.shape({
        lat: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        lng: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      }),
    }),
    clearLocationWidget: PropTypes.func.isRequired,
    serviceProviderInfo: PropTypes.shape({
      address: PropTypes.shape({
        location: PropTypes.shape({
          lat: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
          lng: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        }),
      }),
    }),
  };

  static defaultProps = {
    assetInfo: {},
    serviceProviderInfo: {},
  };

  state = {
    directions: null,
  };

  componentWillUnmount() {
    this.props.clearLocationWidget();
  }

  onDirectionsUpdate = (directions) => {
    this.setState({ directions });
  };

  renderDistance = () => {
    if (this.state.directions) {
      const routeLeg = this.state.directions.routes[0].legs[0];
      return <span>{routeLeg.distance.text}<br />{routeLeg.duration.text}</span>;
    }
    return getOutputText(null, messages.notAvailable);
  };

  render() {
    const { assetInfo, serviceProviderInfo } = this.props;

    const assetLocation = assetInfo.location;

    return (
      <Widget expandKey="case-location">
        <Widget.Header>
          <FormattedMessage {...messages.title} />
        </Widget.Header>
        <Widget.Item>
          <LocationMap
            assetInfo={assetInfo}
            serviceProviderInfo={serviceProviderInfo}
            onDirectionsUpdate={this.onDirectionsUpdate}
          />
        </Widget.Item>
        <Widget.Item>
          <Widget.Table>
            <tbody>
              <Widget.TableRow>
                <th>
                  <FormattedMessage {...messages.assetLocation} />
                </th>
                <td>
                  <AssetLocation location={assetLocation || {}} />
                </td>
              </Widget.TableRow>
              { !isEmpty(serviceProviderInfo) &&
                <Widget.TableRow modifiers={['topGap']}>
                  <th>
                    <FormattedMessage {...messages.distanceToServiceLocation} />
                  </th>
                  <td>
                    {this.renderDistance()}
                  </td>
                </Widget.TableRow>
              }
            </tbody>
          </Widget.Table>
        </Widget.Item>
      </Widget>
    );
  }
}

export default withConnectedData(LocationWidget);
