import React, { Component } from 'react';
import PropTypes from 'prop-types';

import MapWithDirections from 'components/MapWithDirections';

/**
 * The Cartographer draws the map.
 *
 * The markers will be used for plotting driving directions (from the first marker to the last).
 * The directions will be rendered without markers of their own; instead, the custom markers
 * will be rendered. The `onDirectionsUpdate` function will be called with the DirectionService
 * response when it is received.
 */
export class Cartographer extends Component {
  static propTypes = {
    markers: PropTypes.arrayOf(
      PropTypes.shape({
        position: PropTypes.shape({
          lat: PropTypes.number.isRequired,
          lon: PropTypes.number.isRequired,
        }).isRequired,
        label: PropTypes.string.isRequired,
      }),
    ),
    onDirectionsUpdate: PropTypes.func,
  };

  static defaultProps = {
    markers: [],
    onDirectionsUpdate: null,
  };

  componentWillMount() {
    const { markers } = this.props;

    this.setState({
      directions: null,
      markers: this.makeMarkers(markers),
    });
  }

  /**
   * Converts an object with { lat, lon } into a GoogleMap-friendly { lat, lng }.
   * Returns `null` if `latLon` is not given.
   */
  toLatLng = latLon => latLon && { lat: latLon.lat, lng: latLon.lon };

  /**
   * Creates marker objects in the appropriate format for the `Map` based on the
   * marker data provided as props to this component.
   */
  makeMarkers = markerData => markerData.map(m => ({
    ...m,
    showInfo: false, // indicates whether the marker info window is currently open
    position: this.toLatLng(m.position), // make the position google-friendly
  }));

  /**
   * Sets `showInfo` to true for the given marker, causing a re-render and
   * display of the info window.
   */
  handleMarkerClick = targetMarker => this.toggleMarker(targetMarker, true);

  /**
   * Sets `showInfo` to false for the given marker.
   */
  handleMarkerClose = targetMarker => this.toggleMarker(targetMarker, false);

  toggleMarker = (targetMarker, showInfo) =>
    this.setState({
      markers: this.state.markers.map((marker) => {
        if (marker === targetMarker) {
          return { ...marker, showInfo };
        }
        return marker;
      }),
    });

  render() {
    const { onDirectionsUpdate } = this.props;
    return (
      <MapWithDirections
        markers={this.state.markers}
        onMarkerClick={this.handleMarkerClick}
        onMarkerClose={this.handleMarkerClose}
        onDirectionsUpdate={onDirectionsUpdate}
      />
    );
  }
}

export default Cartographer;
