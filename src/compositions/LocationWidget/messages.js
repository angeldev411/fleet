import { defineMessages } from 'react-intl';

const messages = defineMessages({
  assetLocation: {
    id: 'compositions.LocationWidget.label.assetLocation',
    defaultMessage: 'Asset Location',
  },
  distanceToServiceLocation: {
    id: 'compositions.LocationWidget.label.distanceToServiceLocation',
    defaultMessage: 'Distance to Service Location',
  },
  notAvailable: {
    id: 'compositions.LocationWidget.value.NotAvailable',
    defaultMessage: 'Not Provided',
  },
  title: {
    id: 'compositions.LocationWidget.header.title',
    defaultMessage: 'Location',
  },
  unit: {
    id: 'compositions.LocationWidget.label.unit',
    defaultMessage: 'Unit #{unitNumber}',
  },
});


export default messages;
