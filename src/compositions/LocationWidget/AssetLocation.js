import PropTypes from 'prop-types';
import React from 'react';

import WithGeocoding from 'utils/mapping/WithGeocoding';
import { getOutputText } from 'utils/widget';

export function RenderAddress({ address }) {
  return (
    <span>{getOutputText(address)}</span>
  );
}

RenderAddress.propTypes = {
  address: PropTypes.string,
};

RenderAddress.defaultProps = {
  address: null,
};

function AssetLocation({ location }) {
  return (
    <WithGeocoding
      location={location}
      render={RenderAddress}
    />
  );
}

AssetLocation.propTypes = {
  location: PropTypes.shape({
    lat: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    lon: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  }),
};

AssetLocation.defaultProps = {
  location: {},
};

export default AssetLocation;
