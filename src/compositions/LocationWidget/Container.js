import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';

import {
  clearLocationWidget,
} from 'redux/app/actions';
import {
  locationWidgetSelector,
} from 'redux/app/selectors';

const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    assetInfo: PropTypes.shape({
      location: PropTypes.shape({
        lat: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        lng: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      }),
    }),
    clearLocationWidget: PropTypes.func.isRequired,
    serviceProviderInfo: PropTypes.shape({
      address: PropTypes.shape({
        location: PropTypes.shape({
          lat: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
          lng: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        }),
      }),
    }),
  };

  Container.defaultProps = {
    assetInfo: {},
    serviceProviderInfo: {},
  };

  function mapStateToProps(state) {
    return {
      assetInfo: locationWidgetSelector(state).assetInfo,
      serviceProviderInfo: locationWidgetSelector(state).serviceProviderInfo,
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      clearLocationWidget: () => dispatch(clearLocationWidget()),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
