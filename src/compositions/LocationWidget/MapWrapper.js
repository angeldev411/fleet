import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const MapWrapper = styled.div`
  width: 100%;
  height: ${px2rem(265)};
`;

export default MapWrapper;
