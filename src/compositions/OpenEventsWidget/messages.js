import { defineMessages } from 'react-intl';

const messages = defineMessages({
  caseTitle: {
    id: 'compositions.OpenEventsWidget.caseTitle',
    defaultMessage: 'Case {caseId}',
  },
  complaint: {
    id: 'compositions.OpenEventsWidget.data.complaint',
    defaultMessage: 'Complaint',
  },
  downtime: {
    id: 'compositions.OpenEventsWidget.data.downtime',
    defaultMessage: 'Downtime',
  },
  estimate: {
    id: 'compositions.OpenEventsWidget.data.estimate',
    defaultMessage: 'Estimate',
  },
  etr: {
    id: 'compositions.OpenEventsWidget.data.etr',
    defaultMessage: 'ETR',
  },
  followUp: {
    id: 'compositions.OpenEventsWidget.data.followUp',
    defaultMessage: 'Follow-Up',
  },
  notes: {
    id: 'compositions.OpenEventsWidget.data.notes',
    defaultMessage: 'Notes',
  },
  serviceProvider: {
    id: 'compositions.OpenEventsWidget.data.serviceProvider',
    defaultMessage: 'Service Provider',
  },
  status: {
    id: 'compositions.OpenEventsWidget.data.status',
    defaultMessage: 'Status',
  },
  title: {
    id: 'compositions.OpenEventsWidget.title',
    defaultMessage: 'Open Events',
  },
  undefinedComplaint: {
    id: 'compositions.OpenEventsWidget.undefined.complaint',
    defaultMessage: 'No complaints yet',
  },
  unreadNotesCount: {
    id: 'compositions.OpenEventsWidget.data.unreadNotesCount',
    defaultMessage: '{count} Unread',
  },
});

export default messages;
