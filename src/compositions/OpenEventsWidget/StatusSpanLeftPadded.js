import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

import StatusSpan from 'elements/StatusSpan';

/* istanbul ignore next */
const styles = `
  margin-left: ${px2rem(12)};
`;

export default buildStyledComponent(
  'StatusSpanLeftPadded',
  styled(StatusSpan),
  styles,
);
