import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  padding: ${px2rem(18)} ${px2rem(15)};
`;

export default buildStyledComponent(
  'TabContentWrapper',
  styled.div,
  styles,
);
