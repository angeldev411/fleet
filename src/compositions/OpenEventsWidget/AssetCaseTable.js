import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedRelative } from 'react-intl';
import FontAwesome from 'react-fontawesome';
import { camelCase, compact, isEmpty } from 'lodash';
import moment from 'moment';
import { fromJS } from 'immutable';

import { userTimeZone } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';

import Widget from 'elements/Widget';
import TextDiv from 'elements/TextDiv';
import Popover, {
  PopoverTarget,
  PopoverContent,
  ScrollingPopoverContentWrapper,
} from 'elements/Popover';
import Span from 'elements/Span';

import ETR from 'components/_common/ETR';

import UnreadNoteLabel from 'containers/UnreadNoteLabelContainer';

import AssetCaseTableWrapper from './AssetCaseTableWrapper';
import UnreadNotesIcon from './UnreadNotesIcon';
import FollowUpIcon from './FollowUpIcon';
import StatusSpanLeftPadded from './StatusSpanLeftPadded';

import messages from './messages';

function AssetCaseTable({ assetCase, serviceProvider }) {
  const {
    approvalStatus,
    complaint,
    description: caseDescription,
    followUpColor,
    followUpTime,
    unreadNotesCount,
  } = assetCase;
  const {
    code: complaintCode,
    description: complaintDescription,
  } = complaint || {};

  const complaintTitle = complaintCode && complaintDescription &&
    `${complaintCode} - ${complaintDescription}`;

  const approvalStatusModifier = approvalStatus && camelCase(approvalStatus);

  const unreadNotesIcon = unreadNotesCount ? 'envelope' : 'envelope-o';
  const unreadNotesCountModifier = unreadNotesCount ? 'hasUnreadNotes' : '';

  const followUpLabel = followUpTime &&
    <FormattedRelative value={moment.tz(followUpTime, userTimeZone())} />;

  if (isEmpty(assetCase)) { return null; }

  return (
    <AssetCaseTableWrapper>
      <Widget.Table modifiers={['noFlex', 'padRight']}>
        <tbody>
          <Widget.TableRow>
            <th>
              <FormattedMessage {...messages.serviceProvider} />
            </th>
            <td>
              {/* TODO: This will eventually be converted into a Link */}
              {serviceProvider.companyName}
            </td>
          </Widget.TableRow>
          <Widget.TableRow modifiers={['topGap']}>
            <th>
              <FormattedMessage {...messages.complaint} />
            </th>
            <td>
              <Popover showOnHover>
                <PopoverTarget>
                  <TextDiv
                    modifiers={[
                      'bottomGap',
                      'heavyText',
                      'ellipsis',
                      'oneLine',
                      'smallText',
                      'smallWidth',
                    ]}
                  >
                    {getOutputText(complaintTitle, messages.undefinedComplaint)}
                  </TextDiv>
                  <TextDiv modifiers={['ellipsis', 'short', 'smallWidth']}>
                    {complaintCode && getOutputText(caseDescription)}
                  </TextDiv>
                </PopoverTarget>
                <PopoverContent>
                  <ScrollingPopoverContentWrapper modifiers={['smallText']}>
                    <TextDiv modifiers={['bold', 'bright', 'bottomGap']}>
                      {getOutputText(complaintTitle, messages.undefinedComplaint)}
                    </TextDiv>
                    <TextDiv modifiers={['bottomGap']}>
                      {complaintCode && getOutputText(caseDescription)}
                    </TextDiv>
                  </ScrollingPopoverContentWrapper>
                </PopoverContent>
              </Popover>
            </td>
          </Widget.TableRow>
        </tbody>
      </Widget.Table>
      <Widget.Table modifiers={['noFlex', 'padRight']}>
        <tbody>
          <Widget.TableRow>
            <th>
              <FormattedMessage {...messages.status} />
            </th>
            <td>
              {getOutputText(assetCase.repairStatus)}
            </td>
          </Widget.TableRow>
          <Widget.TableRow modifiers={['topGapSmall']}>
            <th>
              <FormattedMessage {...messages.estimate} />
            </th>
            <td>
              <span>
                {getOutputText(assetCase.estimateTotal)}
              </span>
              <StatusSpanLeftPadded
                modifiers={compact([approvalStatusModifier, 'bold', 'uppercase'])}
              >
                {approvalStatus}
              </StatusSpanLeftPadded>
            </td>
          </Widget.TableRow>
          <Widget.TableRow modifiers={['topGapSmall']}>
            <th>
              <FormattedMessage {...messages.etr} />
            </th>
            <td>
              <ETR caseInfo={fromJS(assetCase)} />
            </td>
          </Widget.TableRow>
          <Widget.TableRow modifiers={['topGapSmall']}>
            <th>
              <FormattedMessage {...messages.downtime} />
            </th>
            <td>
              <Span modifiers={['capitalize']}>
                {getOutputText(assetCase.downtime)}
              </Span>
            </td>
          </Widget.TableRow>
        </tbody>
      </Widget.Table>
      <Widget.Table modifiers={['noFlex']} >
        <tbody>
          <Widget.TableRow>
            <th>
              <UnreadNotesIcon modifiers={compact([unreadNotesCountModifier])}>
                <FontAwesome name={unreadNotesIcon} />
              </UnreadNotesIcon>
              <FormattedMessage {...messages.notes} />
            </th>
            <td>
              <UnreadNoteLabel hasModal hideIcon caseInfo={fromJS(assetCase)} />
            </td>
          </Widget.TableRow>
          <Widget.TableRow modifiers={['topGapSmall']}>
            <th>
              <FollowUpIcon modifiers={compact([followUpColor])}>
                <FontAwesome name="flag" />
              </FollowUpIcon>
              <FormattedMessage {...messages.followUp} />
            </th>
            <td>
              {getOutputText(followUpLabel)}
            </td>
          </Widget.TableRow>
        </tbody>
      </Widget.Table>
    </AssetCaseTableWrapper>
  );
}

AssetCaseTable.propTypes = {
  assetCase: PropTypes.shape({
    approvalStatus: PropTypes.string,
    complaint: PropTypes.shape({
      code: PropTypes.string,
      description: PropTypes.string,
    }),
    description: PropTypes.string,
    downtime: PropTypes.string,
    estimateTotal: PropTypes.string,
    followUpColor: PropTypes.string,
    followUpTime: PropTypes.string,
    repairStatus: PropTypes.string,
    unreadNotesCount: PropTypes.number,
  }).isRequired,
  serviceProvider: PropTypes.shape({
    companyName: PropTypes.string,
  }).isRequired,
};

export default AssetCaseTable;
