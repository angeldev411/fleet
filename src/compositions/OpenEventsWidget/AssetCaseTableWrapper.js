import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const AssetCaseTableWrapper = styled.div`
  display: flex;
  margin-left: ${px2rem(-2)};
  margin-top: ${px2rem(12)};
`;

export default AssetCaseTableWrapper;
