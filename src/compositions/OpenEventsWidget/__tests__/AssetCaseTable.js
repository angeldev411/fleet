import React from 'react';
import { fromJS } from 'immutable';

import {
  test,
  expect,
  shallow,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import AssetCaseTable from '../AssetCaseTable';
import messages from '../messages';

const assetCase = {
  id: '9994854',
  complaint: {
    code: '010',
    description: 'Warning Light On',
    component: null,
  },
  description: 'My Check Engine Light turned on today',
  approvalStatus: 'Approved',
  repairStatus: 'Pending',
  etr: '2017-01-12T05:57:17Z',
  downtime: '3 days, 3 hours',
  estimateTotal: '$23,848.23',
  followUpColor: 'yellow',
  followUpTime: '2017-01-10T06:02:17Z',
  unreadNotesCount: 0,
};

const serviceProvider = {
  id: '1030',
  companyName: 'Company 7',
};

const defaultProps = { assetCase, serviceProvider };

function shallowRender(props = defaultProps) {
  return shallow(<AssetCaseTable {...props} />);
}

function mountComponent(props = defaultProps) {
  return mount(
    <MountableTestComponent>
      <AssetCaseTable {...props} />
    </MountableTestComponent>,
  );
}

test('should render nothing if assetCase is empty', () => {
  const component = shallowRender({ assetCase: {}, serviceProvider });
  expect(component.node).toEqual(null);
});

test('renders the correct values in the table', () => {
  const component = mountComponent().render();
  const componentText = component.text();

  expect(componentText).toInclude(assetCase.description);
  expect(componentText).toInclude(assetCase.repairStatus);
  expect(componentText).toInclude(assetCase.estimateTotal);
  expect(componentText).toInclude(assetCase.approvalStatus);
  expect(componentText).toInclude(assetCase.downtime);
  expect(componentText).toInclude(serviceProvider.companyName);
});

/* -------------------- Complaint -------------------- */

test('renders the correct complaint title if there are both complaint code and description', () => {
  const component = shallowRender();
  const titleComponent = component.find('PopoverTarget').find('TextDiv').first();
  expect(titleComponent.render().text()).toInclude('010 - Warning Light On');
});

test('renders the placeholder if the complaint code or description is empty', () => {
  const testAssetCase = {
    ...assetCase,
    complaint: {
      code: null,
    },
  };
  const component = shallowRender({ ...defaultProps, assetCase: testAssetCase });
  const titleComponent = component.find('PopoverTarget').find('TextDiv');
  expect(titleComponent.find('FormattedMessage')).toHaveProps({
    ...messages.undefinedComplaint,
  });
});

/* -------------------- ETR -------------------- */

test('renders ETR and passes down assetCase prop', () => {
  const component = shallowRender();
  expect(component).toContain('ETR');
  expect(component.find('ETR').props().caseInfo).toEqual(fromJS(assetCase));
});

/* -------------------- Notes -------------------- */

test('renders UnreadNotesIcon with correct modifier and icon when count = 0', () => {
  const component = shallowRender();
  const unreadNotesIcon = component.find('UnreadNotesIcon');
  expect(unreadNotesIcon.props().modifiers).toEqual('');
  expect(unreadNotesIcon.find('FontAwesome')).toHaveProp('name', 'envelope-o');
});

test('renders UnreadNotesIcon with correct modifier and icon when count > 0', () => {
  const testAssetCase = {
    ...assetCase,
    unreadNotesCount: 100,
  };
  const component = shallowRender({ ...defaultProps, assetCase: testAssetCase });
  const unreadNotesIcon = component.find('UnreadNotesIcon');
  expect(unreadNotesIcon.props().modifiers).toEqual('hasUnreadNotes');
  expect(unreadNotesIcon.find('FontAwesome')).toHaveProp('name', 'envelope');
});

test('renders UnreadNoteLabel', () => {
  const testAssetCase = {
    ...assetCase,
    unreadNotesCount: 11,
  };
  const thirdTable = shallowRender({ ...defaultProps, assetCase: testAssetCase })
    .find('Table')
    .at(2);
  const firstTd = thirdTable.find('td').at(0);
  expect(firstTd).toContain('UnreadNoteLabelContainer');
});

/* -------------------- Follow-Up -------------------- */

test('renders FollowUpIcon with correct modifier and icon', () => {
  const component = shallowRender();
  const followUpIcon = component.find('FollowUpIcon');
  expect(followUpIcon.props().modifiers).toEqual(assetCase.followUpColor);
  expect(followUpIcon.find('FontAwesome')).toHaveProp('name', 'flag');
});
