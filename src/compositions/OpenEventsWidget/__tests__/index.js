import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  spyOn,
  createSpy,
} from '__tests__/helpers/test-setup';

import { OpenEventsWidget } from '../index';
import messages from '../messages';

const defaultProps = {
  assetCases: ['123', '234', '345'],
  currentAssetCase: {
    id: '234',
    approvalStatus: 'Pending',
    repairStatus: 'Pending',
    unreadNotesCount: 2,
  },
  currentAssetCaseServiceProvider: {
    id: 'abc',
    companyName: 'test company',
  },
  loadAssetCases: noop,
  match: {
    params: {
      assetId: '12345',
    },
  },
  setCurrentAssetCaseId: noop,
};

const defaultState = {
  currentTab: '1',
  tabs: [
    { name: '0', label: {} },
    { name: '1', label: {} },
  ],
};

function shallowRender(props = defaultProps) {
  return shallow(<OpenEventsWidget {...props} />);
}

/* ------------------------- componentDidMount --------------------------- */

test('loads asset cases on component mount', () => {
  const loadAssetCasesSpy = createSpy();
  const testProps = {
    ...defaultProps,
    loadAssetCases: loadAssetCasesSpy,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.componentDidMount();
  expect(loadAssetCasesSpy).toHaveBeenCalledWith(defaultProps.match.params.assetId);
});

/* ------------------------- componentWillReceiveProps --------------------------- */

test('componentWillReceiveProps does not call buildTabs if assetCases prop has not changed', () => {
  const component = shallowRender();
  const instance = component.instance();
  const buildTabsSpy = spyOn(instance, 'buildTabs');
  const nextProps = {
    ...defaultProps,
    assetCases: ['123', '234', '345'],
  };
  component.setProps(nextProps);
  expect(buildTabsSpy).toNotHaveBeenCalled();
});

test('componentWillReceiveProps calls buildTabs if assetCases prop has changed', () => {
  const nextProps = {
    ...defaultProps,
    assetCases: ['111', '222'],
  };
  const component = shallowRender();
  const instance = component.instance();
  const buildTabsSpy = spyOn(instance, 'buildTabs');
  component.setProps(nextProps);
  expect(buildTabsSpy).toHaveBeenCalledWith(nextProps);
});

/* ------------------------- buildTabs --------------------------- */

test('buildTabs activates the first tab and sends the assetCases to handleTabChange', () => {
  const component = shallowRender();
  const instance = component.instance();
  const handleTabChangeSpy = spyOn(instance, 'handleTabChange');

  component.setState(defaultState);
  instance.buildTabs(defaultProps);

  expect(handleTabChangeSpy).toHaveBeenCalledWith('0', { assetCases: defaultProps.assetCases });
});

test('buildTabs sets correct tabs', () => {
  const component = shallowRender();
  const instance = component.instance();

  component.setState(defaultState);
  instance.buildTabs(defaultProps);
  const tabs = component.state().tabs;

  expect(tabs.length).toEqual(3);
  expect(tabs[0].name).toBe('0');
  expect(tabs[0].label.props).toEqual({
    ...messages.caseTitle,
    values: {
      caseId: '123',
    },
  });
  expect(tabs[1].name).toBe('1');
  expect(tabs[1].label.props).toEqual({
    ...messages.caseTitle,
    values: {
      caseId: '234',
    },
  });
  expect(tabs[2].name).toBe('2');
  expect(tabs[2].label.props).toEqual({
    ...messages.caseTitle,
    values: {
      caseId: '345',
    },
  });
});

/* ------------------------- handleTabChange --------------------------- */

test('handleTabChange sets the correct currentTab local state and currentAssetCaseId global state', () => {
  const setCurrentAssetCaseId = createSpy();
  const component = shallowRender({ ...defaultProps, setCurrentAssetCaseId });
  const instance = component.instance();
  instance.handleTabChange('2');
  expect(component.state().currentTab).toEqual('2');
  expect(setCurrentAssetCaseId).toHaveBeenCalledWith(defaultProps.assetCases[2]);
});

/* ------------------------- render --------------------------- */

test('does not render the Widget if no tabs are available', () => {
  const component = shallowRender();
  component.setState({
    ...defaultState,
    tabs: [],
  });
  expect(component).toNotContain('Widget');
});

test('renders the Widget if tabs are available', () => {
  const component = shallowRender();
  component.setState(defaultState);
  expect(component).toContain('Widget');
});

test('renders the number of tabs on the widget header', () => {
  const component = shallowRender();
  component.setState(defaultState);
  const widgetHeader = component.find('Header');
  const lengthSpan = widgetHeader.find('span').last();
  expect(lengthSpan.text()).toEqual(' (2)');
});

test('renders a TabBar with correct props', () => {
  const component = shallowRender();
  component.setState(defaultState);
  expect(component).toContain('TabBar');
  expect(component.find('TabBar')).toHaveProps({
    currentTab: defaultState.currentTab,
    onChangeTab: component.instance().handleTabChange,
    tabs: defaultState.tabs,
  });
});

test('renders a link with case id that is linked to the case detail page', () => {
  const component = shallowRender();
  component.setState(defaultState);
  const caseLink = component.find('Link');
  expect(caseLink).toHaveProp('to', '/cases/234');

  const caseLinkValue = caseLink.find('FormattedMessage');
  expect(caseLinkValue).toHaveProps({ ...messages.caseTitle });
  expect(caseLinkValue.props().values).toEqual({ caseId: '234' });
});

test('renders AssetCaseTable and passes down correct assetCase and serviceProvider props', () => {
  const component = shallowRender();
  component.setState(defaultState);
  expect(component).toContain('AssetCaseTable');
  expect(component.find('AssetCaseTable')).toHaveProps({
    assetCase: defaultState.currentAssetCase,
    serviceProvider: defaultProps.currentAssetCaseServiceProvider,
  });
});
