import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  green: ({ theme }) => ({ styles: `color: ${theme.colors.status.success};` }),
  red: ({ theme }) => ({ styles: `color: ${theme.colors.status.danger};` }),
  yellow: ({ theme }) => ({ styles: `color: ${theme.colors.status.warning};` }),
};

/* istanbul ignore next */
const styles = `
  font-size: ${px2rem(16)};
  margin-right: ${px2rem(8)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
      success: PropTypes.string.isRequired,
      warning: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'FollowUpIcon',
  styled.span,
  styles,
  { modifierConfig, themePropTypes },
);
