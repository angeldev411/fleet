import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router-dom';
import { isEqual } from 'lodash';

import Widget from 'elements/Widget';
import Link from 'elements/Link';

import TabBar from 'components/TabBar';

import AssetCaseTable from './AssetCaseTable';
import TabContentWrapper from './TabContentWrapper';

import withConnectedData from './Container';
import messages from './messages';

export class OpenEventsWidget extends Component {
  static propTypes = {
    assetCases: PropTypes.arrayOf(
      PropTypes.string.isRequired,
    ).isRequired,
    currentAssetCase: PropTypes.shape({
      approvalStatus: PropTypes.string,
      complaint: PropTypes.shape({
        code: PropTypes.string,
        description: PropTypes.string,
      }),
      description: PropTypes.string,
      downtime: PropTypes.string,
      estimateTotal: PropTypes.string,
      followUpColor: PropTypes.string,
      followUpTime: PropTypes.string,
      repairStatus: PropTypes.string,
      unreadNotesCount: PropTypes.number,
    }).isRequired,
    currentAssetCaseServiceProvider: PropTypes.shape({
      companyName: PropTypes.string,
    }).isRequired,
    loadAssetCases: PropTypes.func.isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        assetId: PropTypes.string.isRequired,
      }),
    }).isRequired,
    setCurrentAssetCaseId: PropTypes.func.isRequired,
  };

  state = {
    currentTab: '0',
    tabs: [],
  };

  componentDidMount() {
    const { match: { params: { assetId } } } = this.props;
    this.props.loadAssetCases(assetId);
  }

  componentWillReceiveProps(nextProps) {
    if (!isEqual(nextProps.assetCases, this.props.assetCases)) {
      this.buildTabs(nextProps);
    }
  }

  buildTabs = ({ assetCases }) => {
    const tabs = assetCases.map((assetCase, index) => (
      {
        name: index.toString(),
        label: <FormattedMessage
          {...messages.caseTitle}
          values={{ caseId: assetCase }}
        />,
      }
    ));

    this.setState({ tabs });
    this.handleTabChange('0', { assetCases }); // activates the first tab by default
  }

  handleTabChange = (tabName, { assetCases } = this.props) => {
    const { setCurrentAssetCaseId } = this.props;
    this.setState({
      currentTab: tabName,
    });
    setCurrentAssetCaseId(assetCases[tabName]);
  }

  render() {
    const { currentAssetCase, currentAssetCaseServiceProvider } = this.props;
    const { currentTab, tabs } = this.state;
    const caseId = currentAssetCase.id;

    return !!tabs.length &&
      <Widget expandKey="open-events">
        <Widget.Header>
          <FormattedMessage {...messages.title} />
          <span> ({tabs.length})</span>
        </Widget.Header>
        <Widget.Item>
          <TabBar
            currentTab={currentTab}
            onChangeTab={this.handleTabChange}
            tabs={tabs}
          />
          <TabContentWrapper>
            <Link to={`/cases/${caseId}`} modifiers={['bold', 'small', 'uppercase']}>
              <FormattedMessage {...messages.caseTitle} values={{ caseId }} />
            </Link>
            <AssetCaseTable
              assetCase={currentAssetCase}
              serviceProvider={currentAssetCaseServiceProvider}
            />
          </TabContentWrapper>
        </Widget.Item>
      </Widget>;
  }
}

export default withConnectedData(
  withRouter(OpenEventsWidget),
);
