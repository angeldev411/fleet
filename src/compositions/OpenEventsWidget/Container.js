import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';

import {
  loadAssetCases as loadAssetCasesRequest,
  setCurrentAssetCaseId,
} from 'redux/assets/actions';
import {
  currentAssetCasesSelector,
} from 'redux/assets/selectors';

import {
  currentAssetCaseSelector,
  currentAssetCaseServiceProviderSelector,
} from 'redux/cases/selectors';

const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    assetCases: PropTypes.arrayOf(
      PropTypes.string.isRequired,
    ),
    currentAssetCase: PropTypes.shape({
      approvalStatus: PropTypes.string,
      complaint: PropTypes.shape({
        code: PropTypes.string,
        description: PropTypes.string,
      }),
      description: PropTypes.string,
      downtime: PropTypes.string,
      estimateTotal: PropTypes.string,
      followUpColor: PropTypes.string,
      followUpTime: PropTypes.string,
      repairStatus: PropTypes.string,
      unreadNotesCount: PropTypes.number,
    }),
    currentAssetCaseServiceProvider: PropTypes.shape({
      companyName: PropTypes.string,
    }),
    loadAssetCases: PropTypes.func.isRequired,
  };

  Container.defaultProps = {
    assetCases: [],
    currentAssetCase: {
      approvalStatus: '',
      repairStatus: '',
      unreadNotesCount: 0,
    },
    currentAssetCaseServiceProvider: {},
  };

  function mapStateToProps(state) {
    return {
      currentAssetCase: currentAssetCaseSelector(state),
      currentAssetCaseServiceProvider: currentAssetCaseServiceProviderSelector(state),
      assetCases: currentAssetCasesSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      loadAssetCases: assetId => dispatch(loadAssetCasesRequest({ assetId })),
      setCurrentAssetCaseId: caseId => dispatch(setCurrentAssetCaseId(caseId)),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
