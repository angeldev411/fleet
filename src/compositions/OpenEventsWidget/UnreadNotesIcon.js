import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const modifierConfig = {
  hasUnreadNotes: ({ theme }) => ({
    styles: `color: ${theme.colors.status.info};`,
  }),
};

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.textLight};
  font-size: ${px2rem(16)};
  margin-right: ${px2rem(8)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      textLight: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      info: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'UnreadNotesIcon',
  styled.span,
  styles,
  { modifierConfig, themePropTypes },
);
