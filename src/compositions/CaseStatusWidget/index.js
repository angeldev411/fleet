import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import Widget from 'elements/Widget';
import Table from 'elements/Table';
import Span from 'elements/Span';
import BorderedStatus from 'elements/BorderedStatus';

import { formatDate } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';

import messages from './messages';
import withConnectedData from './Container';

export function CaseStatusWidget({ caseInfo }) {
  const {
    closedAt,
    repairStatus,
    etr,
    downtime,
  } = caseInfo;

  const openOrClosed = closedAt ? 'closed' : 'open';
  const formattedEtr = etr ? formatDate(etr) : getOutputText(null);

  return (
    <Widget expandKey="case-status">
      <Widget.Header>
        <FormattedMessage {...messages.title} />
      </Widget.Header>
      <Widget.Item>
        <Widget.SubHeader>
          <BorderedStatus modifiers={[openOrClosed]}>{openOrClosed}</BorderedStatus>
        </Widget.SubHeader>
      </Widget.Item>
      <Widget.Item>
        <Table modifiers={['smallText']} trModifiers={['flex', 'wide']}>
          <tbody>
            <tr>
              <th><FormattedMessage {...messages.repairStatus} /></th>
              <td>{getOutputText(repairStatus)}</td>
              <th><FormattedMessage {...messages.etr} /></th>
              <td>{formattedEtr}</td>
              <th><FormattedMessage {...messages.downtime} /></th>
              <td>
                <Span modifiers={['capitalize']}>
                  {getOutputText(downtime)}
                </Span>
              </td>
            </tr>
          </tbody>
        </Table>
      </Widget.Item>
    </Widget>
  );
}

CaseStatusWidget.propTypes = {
  caseInfo: PropTypes.shape({
    closedAt: PropTypes.string,
    repairStatus: PropTypes.string,
    etr: PropTypes.string,
    downtime: PropTypes.string,
  }).isRequired,
};

export default withConnectedData(CaseStatusWidget);
