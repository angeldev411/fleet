import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import {
  currentCaseSelector,
} from 'redux/cases/selectors';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function WithConnectedData(props) {
    return <WrappedComponent {...props} />;
  }

  WithConnectedData.propTypes = {
    caseInfo: PropTypes.shape({
      id: PropTypes.string,
    }).isRequired,
  };

  function mapStateToProps(state) {
    return {
      caseInfo: currentCaseSelector(state),
    };
  }

  return connect(mapStateToProps)(
    immutableToJS(WithConnectedData),
  );
};

export default withConnectedData;
