import React from 'react';

import {
  test,
  expect,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import { formatDate } from 'utils/timeUtils';
import messages from 'utils/messages';

import { CaseStatusWidget } from '../index';

const caseInfo = {
  closedAt: '2017-01-13T16:46:22Z',
  downtime: '3 days, 3 hours',
  etr: '2017-01-13T16:46:22Z',
  repairStatus: 'Pending',
};

const defaultProps = { caseInfo };

function mountComponent(props = defaultProps) {
  return mount(
    <MountableTestComponent>
      <CaseStatusWidget {...props} />
    </MountableTestComponent>,
  );
}

test('renders correct open/closed status and correct values into the table', () => {
  const component = mountComponent();

  const borderedStatus = component.find('BorderedStatus');
  const borderedStatusText = borderedStatus.render().text();
  expect(borderedStatus.props().modifiers).toInclude('closed');

  // The element has capitalize CSS, and the actual value is not in Title Case
  expect(borderedStatusText).toEqual('closed');

  const table = component.find('Table').render();
  const tableText = table.text();
  expect(tableText).toInclude(caseInfo.repairStatus);
  expect(tableText).toInclude(formatDate(caseInfo.etr));
  expect(tableText).toInclude(caseInfo.downtime);
});

test('renders correct open/closed status and placeholders into the table if value is null', () => {
  const testCaseInfo = {
    closedAt: null,
    downtime: null,
    etr: null,
    repairStatus: null,
  };

  const component = mountComponent({ caseInfo: testCaseInfo });

  const borderedStatus = component.find('BorderedStatus');
  const borderedStatusText = borderedStatus.render().text();
  expect(borderedStatus.props().modifiers).toInclude('open');
  expect(borderedStatusText).toInclude('open');

  // Testing placeholder values here...
  // Notice that the indexes are put taking table headers into account
  const table = component.find('Table');
  const repairStatusPlaceholder = table.find('FormattedMessage').at(1);
  const etrPlaceholder = table.find('FormattedMessage').at(3);
  const downtimePlaceholder = table.find('FormattedMessage').at(5);
  expect(repairStatusPlaceholder).toHaveProps({ ...messages.noValue });
  expect(etrPlaceholder).toHaveProps({ ...messages.noValue });
  expect(downtimePlaceholder).toHaveProps({ ...messages.noValue });
});
