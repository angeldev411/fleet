import { defineMessages } from 'react-intl';

const messages = defineMessages({
  downtime: {
    id: 'compositions.CaseStatusWidget.data.downtime',
    defaultMessage: 'Downtime',
  },
  etr: {
    id: 'compositions.CaseStatusWidget.data.etr',
    defaultMessage: 'ETR',
  },
  repairStatus: {
    id: 'compositions.CaseStatusWidget.data.repairStatus',
    defaultMessage: 'Repair Status',
  },
  title: {
    id: 'compositions.CaseStatusWidget.title',
    defaultMessage: 'Case Status',
  },
});

export default messages;
