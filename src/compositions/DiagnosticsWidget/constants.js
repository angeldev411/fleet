import messages from './messages';

export const DISPLAY_OPTIONS = [
  {
    value: 'expand',
    label: messages.expandAll,
  },
  {
    value: 'collapse',
    label: messages.collapseAll,
  },
];

export const FAULT_TYPE_MAPS = [
  {
    type: 'VehicleNotification::CumminsFault',
    name: 'cummins',
    label: 'Cummins',
  },
  {
    type: 'VehicleNotification::OsVolvoLinkFault-Mack',
    name: 'guard-dog',
    label: 'GuardDog',
  },
  {
    type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
    name: 'volvo-rd',
    label: 'Volvo RD',
  },
  {
    type: 'VehicleNotification::NoregonFault',
    name: 'tds',
    label: 'TDS',
  },
];
