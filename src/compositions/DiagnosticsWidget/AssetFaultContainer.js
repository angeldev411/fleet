import React from 'react';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';

import {
  loadAssetFaults,
  setCurrentAssetFaults,
} from 'redux/assets/actions';

import {
  currentAssetSelector,
  currentAssetIdSelector,
  currentAssetFaultsSelector,
} from 'redux/assets/selectors';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  function mapStateToProps(state) {
    return {
      assetInfo: currentAssetSelector(state),
      faults: currentAssetFaultsSelector(state),
      currentId: currentAssetIdSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      loadFaults: assetId => dispatch(loadAssetFaults({ assetId })),
      clearFaults: caseFaults => dispatch(setCurrentAssetFaults(caseFaults)),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
