import { defineMessages } from 'react-intl';

const messages = defineMessages({
  collapseAll: {
    id: 'containers.DiagnosticsContainer.notesOptions.collapseAll',
    defaultMessage: 'Collapse All',
  },
  expandAll: {
    id: 'containers.DiagnosticsContainer.notesOptions.expandAll',
    defaultMessage: 'Expand All',
  },
});

export default messages;
