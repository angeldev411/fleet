import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  spyOn,
  actionAndSpy,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import * as casesActions from 'redux/cases/actions';

import { DiagnosticsWidget, getFormattedFaultType } from '../';

const defaultProps = {
  currentId: '1234',
  assetInfo: {},
  faults: [],
  intl: { formatMessage: msg => msg.defaultMessage },
  loadFaults: noop,
  clearFaults: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<DiagnosticsWidget {...props} />);
}

function mountPageWithProps(props = defaultProps) {
  return mount(
    <MountableTestComponent authorized>
      <DiagnosticsWidget {...props} />
    </MountableTestComponent>,
  );
}

// /* ------------------------- componentWillReceiveProps -------------------------------*/

test('componentWillReceiveProps updates values in state when new props are received', () => {
  const component = shallowRender();
  const instance = component.instance();
  const setStateSpy = spyOn(instance, 'setState');

  expect(instance.state).toContain({
    allDisplayFaults: [],
    currentFaultType: '',
    displayFaults: [],
    faultTypeMaps: [],
  });
  component.setProps({
    assetInfo: {
      id: '12',
      vinNumber: '4v4',
    },
    faults: [
      { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
      { id: '42', faultType: 'VehicleNotification::OsVolvoLinkFault' },
    ],
  });
  expect(setStateSpy).toHaveBeenCalledWith({
    allDisplayFaults: [
      {
        fault: { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: true,
        showDetails: false,
      },
      {
        fault: { id: '42', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: true,
        showDetails: false,
      },
    ],
    currentFaultType: 'volvo-rd',
    displayFaults: [
      {
        fault: { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: true,
        showDetails: false,
      },
      {
        fault: { id: '42', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: true,
        showDetails: false,
      },
    ],
    faultTypeMaps: [
      {
        type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
        name: 'volvo-rd',
        label: 'Volvo RD',
      },
    ],
  });
});

test('componentWillReceiveProps updates values in state when new props are received', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.state).toContain({
    allDisplayFaults: [],
    currentFaultType: '',
    displayFaults: [],
    faultTypeMaps: [],
  });
  component.setProps({
    assetInfo: {
      id: '12',
      vinNumber: '4v4',
    },
    faults: [],
  });
  expect(instance.state).toContain({
    displayFaults: [],
  });
});

test(
  'componentWillReceiveProps does not update values in state when no asset info is provided',
  () => {
    const component = shallowRender();
    const instance = component.instance();
    const setStateSpy = spyOn(instance, 'setState');
    expect(instance.state).toContain({
      allDisplayFaults: [],
      currentFaultType: '',
      displayFaults: [],
      faultTypeMaps: [],
    });
    component.setProps({
      faults: [
        { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
      ],
    });
    expect(setStateSpy).toNotHaveBeenCalled();
  },
);

test(
  'componentWillReceiveProps does not update values in state when no faults are provided',
  () => {
    const component = shallowRender();
    const instance = component.instance();
    const setStateSpy = spyOn(instance, 'setState');
    expect(instance.state).toContain({
      allDisplayFaults: [],
      currentFaultType: '',
      displayFaults: [],
      faultTypeMaps: [],
    });
    component.setProps({
      assetInfo: {
        id: '12',
        vinNumber: '4v4',
      },
    });
    expect(setStateSpy).toNotHaveBeenCalled();
  },
);

// /* ------------------------- buildAllDisplayFaults -------------------------------*/

test('buildAllDisplayFaults returns a correctly formatted display fault', () => {
  const assetInfo = {
    id: '12',
    vinNumber: '1M1',
  };
  const faults = [
    { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
  ];

  const component = shallowRender();
  const instance = component.instance();

  const displayFaults = instance.buildAllDisplayFaults(faults, assetInfo);
  expect(displayFaults).toEqual([
    {
      fault: { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
      faultTypeMap: {
        type: 'VehicleNotification::OsVolvoLinkFault-Mack',
        name: 'guard-dog',
        label: 'GuardDog',
      },
      isExpanded: true,
      showDetails: false,
    },
  ]);
});

test('buildAllDisplayFaults uses the existing display faults isExpanded and showDetails values if present', () => {
  const assetInfo = {
    id: '12',
    vinNumber: '4V4',
  };
  const faults = [
    { id: '21', faultType: 'VehicleNotification::CumminsFault' },
  ];

  const component = shallowRender();
  const instance = component.instance();

  instance.state.allDisplayFaults = [
    {
      fault: { id: '21', faultType: 'VehicleNotification::CumminsFault' },
      faultTypeMap: {
        type: 'VehicleNotification::CumminsFault',
        name: 'cummins',
        label: 'Cummins',
      },
      isExpanded: false,
      showDetails: true,
    },
  ];

  const displayFaults = instance.buildAllDisplayFaults(faults, assetInfo);
  expect(displayFaults[0]).toContain({
    isExpanded: false,
    showDetails: true,
  });
});

// /* ------------------------- options -------------------------------*/

test('options returns a correctly formatted array of options', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.options()).toEqual([
    { value: 'expand', label: 'Expand All' },
    { value: 'collapse', label: 'Collapse All' },
  ]);
});

// /* ------------------------- handleChangeDiagnosticsOption -------------------------------*/

test(
  'handleChangeDiagnosticsOption calls updateDisplayFaults with expected value when option value = collapse',
  () => {
    const component = shallowRender();
    const instance = component.instance();
    const updateDisplayFaultsSpy = spyOn(instance, 'updateDisplayFaults');
    instance.handleChangeDiagnosticsOption({ value: 'collapse' });
    expect(updateDisplayFaultsSpy).toHaveBeenCalledWith({ isExpanded: false });
  },
);

test(
  'handleChangeDiagnosticsOption calls updateDisplayFaults with expected value when option value = expand',
  () => {
    const component = shallowRender();
    const instance = component.instance();
    const updateDisplayFaultsSpy = spyOn(instance, 'updateDisplayFaults');
    instance.handleChangeDiagnosticsOption({ value: 'expand' });
    expect(updateDisplayFaultsSpy).toHaveBeenCalledWith({ isExpanded: true });
  },
);

// /* ------------------------- handleChangeDiagnosticsModalOption -------------------------------*/

test(
  'handleChangeDiagnosticsModalOption calls updateDisplayFaults with expected value when option value = collapse',
  () => {
    const component = shallowRender();
    const instance = component.instance();
    const updateDisplayFaultsSpy = spyOn(instance, 'updateDisplayFaults');
    instance.handleChangeDiagnosticsModalOption({ value: 'collapse' });
    expect(updateDisplayFaultsSpy).toHaveBeenCalledWith({ showDetails: false });
  },
);

test(
  'handleChangeDiagnosticsModalOption calls updateDisplayFaults with expected value when option value = expand',
  () => {
    const component = shallowRender();
    const instance = component.instance();
    const updateDisplayFaultsSpy = spyOn(instance, 'updateDisplayFaults');
    instance.handleChangeDiagnosticsModalOption({ value: 'expand' });
    expect(updateDisplayFaultsSpy).toHaveBeenCalledWith({ showDetails: true });
  },
);

// /* ------------------------- handleExpandFault -------------------------------*/

test(
  'handleExpandFault updates isExpanded in the displayFaults based on the ID and value given',
  () => {
    const component = shallowRender();
    const instance = component.instance();

    instance.state.displayFaults = [
      {
        fault: { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: false,
        showDetails: true,
      },
      {
        fault: { id: '42', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: false,
        showDetails: true,
      },
    ];

    instance.handleExpandFault('42', true);
    const { displayFaults } = instance.state;
    expect(displayFaults[0].isExpanded).toEqual(false);
    expect(displayFaults[displayFaults.length - 1].isExpanded).toEqual(true);
  },
);

// /* ------------------------- handleFaultTypeChange -------------------------------*/

test('handleFaultTypeChange sets the provided type in state', () => {
  const component = shallowRender();
  const instance = component.instance();
  instance.handleFaultTypeChange('test');
  expect(instance.state.currentFaultType).toEqual('test');
});

// /* ------------------------- handleHideDetailsModal -------------------------------*/

test(
  'handleHideDetailsModal updates showDetails in the displayFaults',
  () => {
    const component = shallowRender();
    const instance = component.instance();

    instance.state.showDetailsModal = true;
    instance.state.displayFaults = [
      {
        fault: { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: false,
        showDetails: true,
      },
      {
        fault: { id: '42', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: false,
        showDetails: true,
      },
    ];

    instance.handleHideDetailsModal();
    const { displayFaults } = instance.state;
    expect(instance.state.showDetailsModal).toEqual(false);
    expect(displayFaults[0].showDetails).toEqual(false);
    expect(displayFaults[displayFaults.length - 1].showDetails).toEqual(false);
  },
);

// /* ------------------------- handleShowDetailsModal -------------------------------*/

test(
  'handleShowDetailsModal updates showDetails in the displayFaults based on the ID',
  () => {
    const component = shallowRender();
    const instance = component.instance();

    instance.state.showDetailsModal = false;
    instance.state.displayFaults = [
      {
        fault: { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: false,
        showDetails: false,
      },
      {
        fault: { id: '42', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: false,
        showDetails: false,
      },
    ];

    instance.handleShowDetailsModal('42');
    const { displayFaults } = instance.state;
    expect(instance.state.showDetailsModal).toEqual(true);
    expect(displayFaults[0].showDetails).toEqual(false);
    expect(displayFaults[displayFaults.length - 1].showDetails).toEqual(true);
  },
);

// /* ------------------------- handleShowFaultDetails -------------------------------*/

test(
  'handleShowFaultDetails updates isExpanded in the displayFaults based on the ID and value given',
  () => {
    const component = shallowRender();
    const instance = component.instance();

    instance.state.displayFaults = [
      {
        fault: { id: '21', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: false,
        showDetails: false,
      },
      {
        fault: { id: '42', faultType: 'VehicleNotification::OsVolvoLinkFault' },
        faultTypeMap: {
          type: 'VehicleNotification::OsVolvoLinkFault-Volvo',
          name: 'volvo-rd',
          label: 'Volvo RD',
        },
        isExpanded: false,
        showDetails: false,
      },
    ];

    instance.handleShowFaultDetails('42', true);
    const { displayFaults } = instance.state;
    expect(displayFaults[0].showDetails).toEqual(false);
    expect(displayFaults[displayFaults.length - 1].showDetails).toEqual(true);
  },
);

// /* ------------------------- getFormattedFaultType -------------------------------*/
test('getFormattedFaultType returns faultType when it is not `VehicleNotification::OsVolvoLinkFault`', () => {
  const faultType = 'TestType';
  const formattedFaultType = getFormattedFaultType(faultType);
  expect(formattedFaultType, faultType);
});

test('returns `VehicleNotification::OsVolvoLinkFault-Volvo` when vinNumber is null', () => {
  const assetInfo = {
    vinNumber: null,
  };
  const formattedFaultType = getFormattedFaultType('VehicleNotification::OsVolvoLinkFault', assetInfo);
  expect(formattedFaultType).toEqual('VehicleNotification::OsVolvoLinkFault-Volvo');
});

test('returns `VehicleNotification::OsVolvoLinkFault-Volvo` when vinNumber starts with `4V4` or assetMake is `Volvo`', () => {
  let assetInfo = {
    vinNumber: '4V4-Whatever',
    make: 'Whatever',
  };
  let formattedFaultType = getFormattedFaultType('VehicleNotification::OsVolvoLinkFault', assetInfo);
  expect(formattedFaultType).toEqual('VehicleNotification::OsVolvoLinkFault-Volvo');
  assetInfo = {
    vinNumber: 'Whatever',
    make: 'Volvo',
  };
  formattedFaultType = getFormattedFaultType('VehicleNotification::OsVolvoLinkFault', assetInfo);
  expect(formattedFaultType).toEqual('VehicleNotification::OsVolvoLinkFault-Volvo');
});

test('returns `VehicleNotification::OsVolvoLinkFault-Mack` when vinNumber starts with `1M1` or assetMake is `Mack`', () => {
  let assetInfo = {
    vinNumber: '1M1-Whatever',
    make: 'Whatever',
  };
  let formattedFaultType = getFormattedFaultType('VehicleNotification::OsVolvoLinkFault', assetInfo);
  expect(formattedFaultType).toEqual('VehicleNotification::OsVolvoLinkFault-Mack');
  assetInfo = {
    vinNumber: 'Whatever',
    make: 'Mack',
  };
  formattedFaultType = getFormattedFaultType('VehicleNotification::OsVolvoLinkFault', assetInfo);
  expect(formattedFaultType).toEqual('VehicleNotification::OsVolvoLinkFault-Mack');
});

// Below test are WIP

test.skip('Leaving the case page clears the current faults state', () => {
  const clearFaults = actionAndSpy(casesActions, 'clearFaults');
  const page = mountPageWithProps();
  page.unmount();
  expect(clearFaults).toHaveBeenCalled();
});

test.skip('loadCaseFaults is called when a previously stored case hasFaults', () => {
  const loadCaseFaults = actionAndSpy(casesActions, 'loadCaseFaults');
  mountPageWithProps({
    ...defaultProps,
    caseInfo: { id: '1234', hasFaults: true },
  });
  expect(loadCaseFaults).toHaveBeenCalled();
});

test.skip('loadCaseFaults is called when a no case is stored', () => {
  const loadCaseFaults = actionAndSpy(casesActions, 'loadCaseFaults');
  mountPageWithProps({ ...defaultProps, caseInfo: {} });
  expect(loadCaseFaults).toHaveBeenCalled();
});
