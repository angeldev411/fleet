import { find, has, uniqBy } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { injectIntl } from 'react-intl';

import AssetFaultContainer from './AssetFaultContainer';
import CaseFaultContainer from './CaseFaultContainer';

import { DISPLAY_OPTIONS, FAULT_TYPE_MAPS } from './constants';
import DiagnosticsComponent from './DiagnosticsComponent';

/**
 * Filters allDisplayFaults and returns only those matching the currentFaultType
 * @param  {Array} allDisplayFaults   An Array of all the display fault objects
 * @param  {String} currentFaultType The name of the currently viewed fault type
 * @return {Array}                    An Array of display fault objects matching the
 *                                   currentFaultType
 */
function filterDisplayFaults(allDisplayFaults, currentFaultType) {
  return allDisplayFaults.filter(displayFault =>
    displayFault.faultTypeMap.name === currentFaultType,
  );
}

/**
 * Collects and returns unique fault type maps from allDisplayFaults
 * @param  {Array} allDisplayFaults   An Array of all the display fault objects
 * @return {Array}                   An array of unique fault type maps that represent the types
 *                                   found in allDisplayFaults
 */
function getFaultTypeMaps(allDisplayFaults) {
  const allFaultTypes = allDisplayFaults.map(f => f.faultTypeMap);
  return uniqBy(allFaultTypes, value => value.type);
}

/**
 * Formats the fault types based on asset make and model. Specifically, converts Volvo faults to
 * Mack branding on Mack assets.
 * @param  {String} faultType The core application's fault type
 * @param  {Object} assetInfo    The current asset data as held in state
 * @return {String}           The updated fault type (may be the original fault type);
 */
export function getFormattedFaultType(faultType, assetInfo) {
  if (faultType !== 'VehicleNotification::OsVolvoLinkFault') {
    return faultType;
  }

  const vinNumber = assetInfo.vinNumber;
  if (vinNumber) {
    const assetMake = assetInfo.make;
    if (vinNumber.startsWith('1M1') || assetMake === 'Mack') {
      return 'VehicleNotification::OsVolvoLinkFault-Mack';
    }
    if (vinNumber.startsWith('4V4') || assetMake === 'Volvo') {
      return 'VehicleNotification::OsVolvoLinkFault-Volvo';
    }
  }
  return 'VehicleNotification::OsVolvoLinkFault-Volvo';
}

export class DiagnosticsWidget extends Component {
  static propTypes = {
    assetInfo: PropTypes.shape({
      vinNumber: PropTypes.string,
      make: PropTypes.string,
    }).isRequired,
    currentId: PropTypes.string.isRequired,
    faults: PropTypes.arrayOf(PropTypes.object).isRequired,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.string.isRequired,
        label: PropTypes.shape({
          id: PropTypes.string.isRequired,
          defaultMessage: PropTypes.string.isRequired,
        }).isRequired,
      }),
    ).isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    clearFaults: PropTypes.func.isRequired,
    loadFaults: PropTypes.func.isRequired,
  };

  static defaultProps = {
    options: DISPLAY_OPTIONS,
    assetInfo: {},
  };

  state = {
    allDisplayFaults: [],
    currentFaultType: '',
    displayFaults: [],
    faultTypeMaps: [],
    showDetailsModal: false,
  };

  componentDidMount() {
    this.props.loadFaults(this.props.currentId);
  }

  /**
   * Parses case faults with asset info to determine the fault types and the current type.
   * @param {Object} nextProps
   */
  componentWillReceiveProps(nextProps) {
    const { faults, assetInfo } = nextProps;
    if (assetInfo.id && faults.length > 0 && this.state.allDisplayFaults.length === 0) {
      const allDisplayFaults = this.buildAllDisplayFaults(faults, assetInfo);
      const faultTypeMaps = getFaultTypeMaps(allDisplayFaults);
      const currentFaultType = faultTypeMaps[0] && faultTypeMaps[0].name;
      const displayFaults = filterDisplayFaults(allDisplayFaults, currentFaultType);

      this.setState({ allDisplayFaults, currentFaultType, displayFaults, faultTypeMaps });
    }
  }

  componentWillUnmount() {
    this.props.clearFaults();
  }

  /**
   * Builds a collection of objects defining some properties concerning the display of case faults
   * @param  {Array} faults The case faults as they exist in redux state
   * @param  {Object} assetInfo The asset info for the current case, also held in redux state.
   * @return {Array}  An Array with the following fields:
   *                  fault {Object} The original fault as held in redux state.
   *                  faultTypeMap {Object} Contains data concerning the remapping of the fault type
   *                  isExpanded {Boolean} The expanded state of the case fault.
   *                  showDetails {Boolean} The state of details display within the modal view.
   */
  buildAllDisplayFaults = (faults, assetInfo) =>
    faults.map((fault) => {
      const oldFault = this.state.allDisplayFaults.find(f =>
        f.fault.id === fault.id,
      ) || {};
      let faultTypeMap = oldFault.faultTypeMap;
      if (!faultTypeMap) {
        const formattedFaultType = getFormattedFaultType(fault.faultType, assetInfo);
        faultTypeMap = find(FAULT_TYPE_MAPS, map => map.type === formattedFaultType);
      }

      return {
        fault,
        faultTypeMap,
        isExpanded: has(oldFault, 'isExpanded') ? !!oldFault.expanded : true,
        showDetails: has(oldFault, 'showDetails') ? !!oldFault.showDetails : false,
      };
    });

  /**
   * Builds a list of options with translated labels
   * @return {Array} Option objects for the options DropDownSelector
   */
  options = () => {
    const {
      options,
      intl: { formatMessage },
    } = this.props;
    return options.map(opt => ({
      ...opt,
      label: formatMessage(opt.label),
    }));
  }

  /**
   * Calls the appropriate method based on the newly selected diagnostics option,
   * then clears that option.
   * @param {Object} options An object with data concerning which option has been selected.
   * @param {String} options.value 'expand' or 'collpase'. Indicates which action to perform.
   */
  handleChangeDiagnosticsOption = ({ value }) => {
    if (value === 'collapse') {
      this.updateDisplayFaults({ isExpanded: false });
    }
    if (value === 'expand') {
      this.updateDisplayFaults({ isExpanded: true });
    }
  }

  /**
   * Calls the appropriate method based on the newly selected diagnostics option,
   * then clears that option.
   * @param {Object} options An object with data concerning which option has been selected.
   * @param {String} options.value 'expand' or 'collpase'. Indicates which action to perform.
   */
  handleChangeDiagnosticsModalOption = ({ value }) => {
    if (value === 'collapse') {
      this.updateDisplayFaults({ showDetails: false });
    }
    if (value === 'expand') {
      this.updateDisplayFaults({ showDetails: true });
    }
  }

  /**
   * Updates the expanded state of a fault in this.state.displayFaults
   * @param  {String}  faultId    The id of the fault that requires updating
   * @param  {Boolean} isExpanded The expanded state of that fault.
   */
  handleExpandFault = (faultId, isExpanded) => {
    this.updateDisplayFault(faultId, { isExpanded });
  }

  /**
   * Updates the type of fault currently being displayed in local state.
   * @param  {String} type The name of the fault type that was selected.
   */
  handleFaultTypeChange = (type) => {
    this.setState({ currentFaultType: type });
  }

  /**
   * Closes the fault details modal and sets all faults to hide the full details.
   */
  handleHideDetailsModal = () => {
    this.updateDisplayFaults({ showDetails: false });
    this.setState({ showDetailsModal: false });
  }

  /**
   * Opens the fault details modal with the selected fault expanded to show all details.
   * @param  {string} faultId The id of the fault that should be fully expanded.
   */
  handleShowDetailsModal = (faultId) => {
    this.updateDisplayFault(faultId, { showDetails: true });
    this.setState({ showDetailsModal: true });
  }

  /**
   * Opens the fault details within the modal.
   * @param  {String} faultId      The id of the fault that should have the details displayed
   * @param  {Boolean} showDetails The state of the details view
   */
  handleShowFaultDetails = (faultId, showDetails) => {
    this.updateDisplayFault(faultId, { showDetails });
  }

  /**
   * Updates a single display fault within the List of displayFaults
   * @param  {String} faultId The id of the fault that should have the details updated
   * @param  {Object} options An object with the options to update on the display fault
   */
  updateDisplayFault = (faultId, options) => {
    const displayFaults = this.state.displayFaults.map((f) => {
      if (f.fault.id === faultId) {
        return { ...f, ...options };
      }
      return f;
    });
    this.setState({ displayFaults });
  }

  /**
   * Updates properties on all display faults within the List of displayFaults.
   * @param  {Object} options An object with options to update on all display faults
   */
  updateDisplayFaults = (options) => {
    const displayFaults = this.state.displayFaults.map(f => ({ ...f, ...options }));
    this.setState({ displayFaults });
  }

  render() {
    const {
      currentFaultType,
      displayFaults,
      faultTypeMaps,
      showDetailsModal,
    } = this.state;

    return (
      <DiagnosticsComponent
        currentFaultType={currentFaultType}
        displayFaults={displayFaults}
        faultTypes={faultTypeMaps}
        options={this.options()}
        handleChangeDiagnosticsOption={this.handleChangeDiagnosticsOption}
        handleChangeDiagnosticsModalOption={this.handleChangeDiagnosticsModalOption}
        handleExpandFault={this.handleExpandFault}
        handleFaultTypeChange={this.handleFaultTypeChange}
        handleHideDetailsModal={this.handleHideDetailsModal}
        handleShowDetailsModal={this.handleShowDetailsModal}
        handleShowFaultDetails={this.handleShowFaultDetails}
        showDetailsModal={showDetailsModal}
      />
    );
  }
}

const DiagnosticsWidgetWithCommonHOCS = injectIntl(DiagnosticsWidget);

export const AssetFaultDiagnosticsWidget = AssetFaultContainer(DiagnosticsWidgetWithCommonHOCS);
export const CaseFaultDiagnosticsWidget = CaseFaultContainer(DiagnosticsWidgetWithCommonHOCS);
