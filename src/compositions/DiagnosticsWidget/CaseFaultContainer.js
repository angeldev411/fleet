import React from 'react';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';

import {
  loadCaseFaults,
  setCurrentCaseFaults,
} from 'redux/cases/actions';

import {
  currentCaseAssetSelector,
  currentCaseFaultsSelector,
  currentCaseIdSelector,
} from 'redux/cases/selectors';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  function mapStateToProps(state) {
    return {
      assetInfo: currentCaseAssetSelector(state),
      faults: currentCaseFaultsSelector(state),
      currentId: currentCaseIdSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      loadFaults: caseId => dispatch(loadCaseFaults({ caseId })),
      clearFaults: caseFaults => dispatch(setCurrentCaseFaults(caseFaults)),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
