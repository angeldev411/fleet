import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import ModalWindow from 'components/ModalWindow';
import TabBar, { TabBarOptionsWrapper } from 'components/TabBar';

import Widget from 'elements/Widget';

import CaseFault, { ModalCaseFault } from './CaseFault';
import DiagnosticsOptionsBar from './DiagnosticsOptionsBar';

import messages from './messages';

function DiagnosticsComponent({
  currentFaultType,
  displayFaults,
  options,
  faultTypes,
  handleChangeDiagnosticsOption,
  handleChangeDiagnosticsModalOption,
  handleExpandFault,
  handleFaultTypeChange,
  handleHideDetailsModal,
  handleShowDetailsModal,
  handleShowFaultDetails,
  showDetailsModal,
}) {
  function renderListOfFaults({ Component, handleTitleClick, buildExtraProps = noop }) {
    return displayFaults.map((displayFault, index) => {
      const caseFaultId = displayFault.fault.id;
      return (
        <Component
          key={caseFaultId}
          bottom={index === displayFaults.size - 1}
          displayFault={displayFault}
          handleTitleClick={handleTitleClick}
          {...buildExtraProps(displayFault)}
        />
      );
    });
  }

  function caseFaults() {
    const buildExtraProps = displayFault => ({
      onExpand: expanded => handleExpandFault(displayFault.fault.id, expanded),
    });

    return renderListOfFaults({
      Component: CaseFault,
      handleTitleClick: handleShowDetailsModal,
      buildExtraProps,
    });
  }

  function modalCaseFaults() {
    return renderListOfFaults({
      Component: ModalCaseFault,
      handleTitleClick: handleShowFaultDetails,
    });
  }

  return (
    <Widget id="diagnostics" width="full" expandKey="diagnostics">
      <Widget.Header>
        <FormattedMessage {...messages.title} />
      </Widget.Header>
      <Widget.Item >
        <TabBar
          currentTab={currentFaultType}
          emptyMessage={<FormattedMessage {...messages.noFaults} />}
          onChangeTab={handleFaultTypeChange}
          tabs={faultTypes}
        >
          <TabBarOptionsWrapper>
            <DiagnosticsOptionsBar
              options={options}
              onChangeOption={handleChangeDiagnosticsOption}
            />
          </TabBarOptionsWrapper>
        </TabBar>
        {caseFaults()}
      </Widget.Item>
      {showDetailsModal &&
        <ModalWindow
          headerInfo={{ title: <FormattedMessage {...messages.modalTitle} /> }}
          hideModal={handleHideDetailsModal}
          hideByClickingBackground
        >
          <TabBar
            currentTab={currentFaultType}
            emptyMessage={<FormattedMessage {...messages.noFaults} />}
            onChangeTab={handleFaultTypeChange}
            tabs={faultTypes}
          >
            <TabBarOptionsWrapper>
              <DiagnosticsOptionsBar
                options={options}
                onChangeOption={handleChangeDiagnosticsModalOption}
              />
            </TabBarOptionsWrapper>
          </TabBar>
          {modalCaseFaults()}
        </ModalWindow>
      }
    </Widget>
  );
}

DiagnosticsComponent.propTypes = {
  currentFaultType: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }),
  ).isRequired,
  displayFaults: PropTypes.arrayOf(
    PropTypes.shape({
      isExpanded: PropTypes.bool.isRequired,
      faultTypeMap: PropTypes.object.isRequired,
      fault: PropTypes.shape({
        id: PropTypes.string.isRequired,
      }).isRequired,
    }),
  ).isRequired,
  faultTypes: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }),
  ).isRequired,
  handleChangeDiagnosticsOption: PropTypes.func.isRequired,
  handleChangeDiagnosticsModalOption: PropTypes.func.isRequired,
  handleExpandFault: PropTypes.func.isRequired,
  handleFaultTypeChange: PropTypes.func.isRequired,
  handleHideDetailsModal: PropTypes.func.isRequired,
  handleShowDetailsModal: PropTypes.func.isRequired,
  handleShowFaultDetails: PropTypes.func.isRequired,
  showDetailsModal: PropTypes.bool.isRequired,
};

DiagnosticsComponent.defaultProps = {
  currentFaultType: '',
};

export default DiagnosticsComponent;
