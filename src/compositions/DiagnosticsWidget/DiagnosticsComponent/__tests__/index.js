import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import DiagnosticsComponent from '../index';

const defaultProps = {
  currentFaultType: 'Volvo',
  handleFaultTypeChange: () => {},
  faultTypes: [
    { type: 'first', name: 'first', label: 'First' },
    { type: 'second', name: 'second', label: 'Second' },
  ],
  displayFaults: [],
  handleChangeDiagnosticsOption: () => {},
  handleChangeDiagnosticsModalOption: () => {},
  handleExpandFault: () => {},
  handleHideDetailsModal: () => {},
  handleShowDetailsModal: () => {},
  handleShowFaultDetails: () => {},
  options: [],
  showDetailsModal: false,
};

function shallowRender(props = defaultProps) {
  return shallow(<DiagnosticsComponent {...props} />);
}

test('renders a TabBar with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('TabBar');
  expect(component.find('TabBar')).toHaveProps({
    currentTab: defaultProps.currentFaultType,
    onChangeTab: defaultProps.handleFaultTypeChange,
    tabs: defaultProps.faultTypes,
  });
});

test('renders a DiagnosticsOptionsBar with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('DiagnosticsOptionsBar');
  expect(component.find('DiagnosticsOptionsBar')).toHaveProps({
    options: defaultProps.options,
    onChangeOption: defaultProps.handleChangeDiagnosticsOption,
  });
});

test('renders the expected number of CaseFaults for the display faults', () => {
  const displayFaults = [
    { fault: { id: '1' }, faultTypeMap: {}, isExpanded: true },
    { fault: { id: '2' }, faultTypeMap: {}, isExpanded: true },
    { fault: { id: '3' }, faultTypeMap: {}, isExpanded: true },
  ];
  const component = shallowRender({ ...defaultProps, displayFaults });
  expect(component.find('CaseFault').length).toEqual(3);
});

test('runs the handleExpandFault when onExpand is called', () => {
  const expanded = true;
  const displayFaults = [
    { fault: { id: '1' }, faultTypeMap: {}, isExpanded: true },
    { fault: { id: '2' }, faultTypeMap: {}, isExpanded: true },
    { fault: { id: '3' }, faultTypeMap: {}, isExpanded: true },
  ];
  const handleExpandFault = createSpy();
  const component = shallowRender({ ...defaultProps, displayFaults, handleExpandFault });
  const firstCaseFault = component.find('CaseFault').at(0);
  firstCaseFault.prop('onExpand')(expanded);
  expect(handleExpandFault)
    .toHaveBeenCalledWith(displayFaults[0].fault.id, expanded);
});

test('does not render ModalWindow if showDetailsModal = false', () => {
  const component = shallowRender();
  expect(component).toNotContain('ModalWindow');
});

test('renders ModalWindow with expected props if showDetailsModal = true', () => {
  const component = shallowRender({ ...defaultProps, showDetailsModal: true });
  expect(component).toContain('ModalWindow');
  expect(component.find('ModalWindow')).toHaveProps({
    hideModal: defaultProps.handleHideDetailsModal,
    hideByClickingBackground: true,
  });
});

test('renders a TabBar in ModalWindow with expected props', () => {
  const component = shallowRender({ ...defaultProps, showDetailsModal: true });
  const modalWindow = component.find('ModalWindow');
  expect(modalWindow).toContain('TabBar');
  expect(modalWindow.find('TabBar')).toHaveProps({
    currentTab: defaultProps.currentFaultType,
    onChangeTab: defaultProps.handleFaultTypeChange,
    tabs: defaultProps.faultTypes,
  });
});

test('renders a DiagnosticsOptionsBar in ModalWindow with expected props', () => {
  const component = shallowRender({ ...defaultProps, showDetailsModal: true });
  const modalWindow = component.find('ModalWindow');
  expect(modalWindow).toContain('DiagnosticsOptionsBar');
  expect(modalWindow.find('DiagnosticsOptionsBar')).toHaveProps({
    options: defaultProps.options,
    onChangeOption: defaultProps.handleChangeDiagnosticsModalOption,
  });
});

test('renders the expected number of ModalCaseFaults for the display faults', () => {
  const displayFaults = [
    { fault: { id: '1' }, faultTypeMap: {}, isExpanded: true },
    { fault: { id: '2' }, faultTypeMap: {}, isExpanded: true },
    { fault: { id: '3' }, faultTypeMap: {}, isExpanded: true },
  ];
  const component = shallowRender({ ...defaultProps, displayFaults, showDetailsModal: true });
  expect(component.find('ModalCaseFault').length).toEqual(3);
});
