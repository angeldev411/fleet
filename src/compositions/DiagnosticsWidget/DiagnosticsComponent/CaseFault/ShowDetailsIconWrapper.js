import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  hidden: () => ({
    styles: `
      visibility: hidden;
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.chrome300};
  left: -${px2rem(4)};
  position: absolute;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'ShowDetailsIconWrapper',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
