import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  bottom: ({ theme }) => ({
    styles: `
      border-bottom: 1px solid ${theme.colors.base.chrome200};
    `,
  }),
};

/* istanbul ignore next */
const styles = () => `
  font-size: ${px2rem(12)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome200: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Wrapper',
  styled.section,
  styles,
  { modifierConfig, themePropTypes },
);
