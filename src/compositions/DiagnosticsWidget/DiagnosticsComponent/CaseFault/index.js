import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { compact, noop } from 'lodash';

import Expandable, { ExpandableHeader, ExpandableContent } from 'containers/Expandable';

import SeverityLabel from 'components/SeverityLabel';

import Link from 'elements/Link';
import { SplitBlock, SplitBlockPart } from 'elements/SplitBlock';
import Widget from 'elements/Widget';

import { formatDate } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';

import messages from './messages';
import Wrapper from './Wrapper';
import HeadingWrapper from './HeadingWrapper';
import RepairInstructionsLink from './RepairInstructionsLink';
import RightPanelWrapper from './RightPanelWrapper';
import StatusItemWrapper from './StatusItemWrapper';
import TitleWrapper from './TitleWrapper';
import ContentWrapper from './ContentWrapper';

function CaseFault({
  bottom,
  displayFault,
  onExpand,
  handleTitleClick,
}) {
  const { fault, isExpanded } = displayFault;
  const componentId = fault.componentId;
  const componentIdComponent = (
    <Link
      to="#"
      onClick={e => e.preventDefault()}
      modifiers={['hoverCaret']}
    >
      {componentId}
    </Link>
  );

  const status = fault.status;
  const rawReportedAt = fault.reportedAt;
  const reportedAt = rawReportedAt ? formatDate(rawReportedAt) : null;
  const repairInstructionsUrl = fault.repairInstructionsUrl;
  const ecu = fault.ecuId;
  const fmi = fault.failureMode;
  const count = fault.count;
  const severity = fault.severity;
  const severityColor = severity.color;
  const severityLevel = severity.level;
  const severityName = severity.name;

  const onTitleClick = (e) => {
    e.preventDefault();
    handleTitleClick(fault.id, !isExpanded);
  };

  const isBottom = bottom && 'bottom';

  return (
    <Wrapper modifiers={compact([isBottom])}>
      <Expandable isExpanded={isExpanded} onExpand={onExpand}>
        <ExpandableHeader>
          <SplitBlock modifiers={['pad']}>
            <HeadingWrapper onClick={onTitleClick}>
              <TitleWrapper>
                {getOutputText(componentIdComponent)}
              </TitleWrapper>
            </HeadingWrapper>
          </SplitBlock>
        </ExpandableHeader>
        <ExpandableContent>
          <ContentWrapper>
            <SplitBlock modifiers={['pad']}>
              <SplitBlockPart modifiers={['left']}>
                <Widget.Table>
                  <tbody>
                    <Widget.TableRow>
                      <th>
                        <FormattedMessage {...messages.status} />
                      </th>
                      <td>
                        {getOutputText(status)}
                      </td>
                    </Widget.TableRow>
                    <Widget.TableRow modifiers={['topGap']}>
                      <th>
                        <FormattedMessage {...messages.datetime} />
                      </th>
                      <td>
                        {getOutputText(reportedAt)}
                      </td>
                    </Widget.TableRow>
                    <Widget.TableRow modifiers={['topGap']}>
                      <th>
                        <FormattedMessage {...messages.ecu} />
                      </th>
                      <td>
                        {getOutputText(ecu)}
                      </td>
                    </Widget.TableRow>
                    <Widget.TableRow modifiers={['topGap']}>
                      <th>
                        <FormattedMessage {...messages.fmi} />
                      </th>
                      <td>
                        {getOutputText(fmi)}
                      </td>
                    </Widget.TableRow>
                    <Widget.TableRow modifiers={['topGap']}>
                      <th>
                        <FormattedMessage {...messages.count} />
                      </th>
                      <td>
                        {getOutputText(count)}
                      </td>
                    </Widget.TableRow>
                  </tbody>
                </Widget.Table>
              </SplitBlockPart>
              <SplitBlockPart modifiers={['right']}>
                <RightPanelWrapper>
                  <StatusItemWrapper>
                    <SeverityLabel
                      color={severityColor}
                      value={severityLevel}
                      name={severityName}
                      fontSize={12}
                    />
                  </StatusItemWrapper>
                  {repairInstructionsUrl &&
                    <StatusItemWrapper>
                      <RepairInstructionsLink href={repairInstructionsUrl} />
                    </StatusItemWrapper>
                  }
                </RightPanelWrapper>
              </SplitBlockPart>
            </SplitBlock>
          </ContentWrapper>
        </ExpandableContent>
      </Expandable>
    </Wrapper>
  );
}

CaseFault.propTypes = {
  bottom: PropTypes.bool,
  displayFault: PropTypes.shape({
    fault: PropTypes.object.isRequired,
    isExpanded: PropTypes.bool,
  }).isRequired,
  onExpand: PropTypes.func,
  handleTitleClick: PropTypes.func.isRequired,
};

CaseFault.defaultProps = {
  bottom: false,
  onExpand: noop,
};

export default CaseFault;
export ModalCaseFault from './ModalCaseFault';
