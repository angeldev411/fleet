import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import SmoothCollapse from 'react-smooth-collapse';
import { compact } from 'lodash';

import SeverityLabel from 'components/SeverityLabel';

import Container from 'elements/Container';
import { SplitBlock, SplitBlockPart } from 'elements/SplitBlock';
import Widget from 'elements/Widget';

import { formatDate } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';

import messages from './messages';
import Wrapper from './Wrapper';
import HeadingWrapper from './HeadingWrapper';
import RepairInstructionsLink from './RepairInstructionsLink';
import RightPanelWrapper from './RightPanelWrapper';
import StatusItemWrapper from './StatusItemWrapper';
import TitleWrapper from './TitleWrapper';
import ContentWrapper from './ContentWrapper';
import ShowDetailsIcon from './ShowDetailsIcon';
import FaultDetails from './FaultDetails';

class ModalCaseFault extends Component {
  static propTypes = {
    bottom: PropTypes.bool,
    displayFault: PropTypes.shape({
      fault: PropTypes.object.isRequired,
      isExpanded: PropTypes.bool,
      showDetails: PropTypes.bool,
    }).isRequired,
    handleTitleClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    bottom: false,
  };

  state = {
    displayDetailsChevron: false,
  };

  setDisplayDetailsChevron = ({ displayDetailsChevron }) => {
    this.setState({ displayDetailsChevron });
  }

  toggleDetailView = (e) => {
    e.preventDefault();
    const { fault, showDetails } = this.props.displayFault;
    this.props.handleTitleClick(fault.id, !showDetails);
  };

  render() {
    const { fault, showDetails } = this.props.displayFault;
    const {
      componentId,
      count,
      ecuId: ecu,
      failureMode: fmi,
      repairInstructionsUrl,
      reportedAt: rawReportedAt,
      severity: {
        color: severityColor,
        level: severityLevel,
        name: severityName,
      } = {},
      status,
    } = fault;
    const componentIdComponent = <span>{componentId}</span>;
    const reportedAt = rawReportedAt ? formatDate(rawReportedAt) : null;

    const isBottom = this.props.bottom && 'bottom';

    return (
      <Wrapper modifiers={compact([isBottom])}>
        <SplitBlock modifiers={['pad']}>
          <HeadingWrapper
            onClick={this.toggleDetailView}
            onMouseEnter={() => this.setDisplayDetailsChevron({ displayDetailsChevron: true })}
            onMouseLeave={() => this.setDisplayDetailsChevron({ displayDetailsChevron: false })}
          >
            <ShowDetailsIcon
              modifiers={compact([!this.state.displayDetailsChevron && 'hidden'])}
              showDetails={showDetails}
            />
            <TitleWrapper modifiers={['padLeft']}>
              {getOutputText(componentIdComponent)}
            </TitleWrapper>
          </HeadingWrapper>
        </SplitBlock>
        <ContentWrapper>
          <SplitBlock modifiers={['pad']}>
            <SplitBlockPart modifiers={['left']}>
              <Widget.Table>
                <tbody>
                  <Widget.TableRow>
                    <th>
                      <FormattedMessage {...messages.status} />
                    </th>
                    <td>
                      {getOutputText(status)}
                    </td>
                  </Widget.TableRow>
                  <Widget.TableRow modifiers={['topGap']}>
                    <th>
                      <FormattedMessage {...messages.datetime} />
                    </th>
                    <td>
                      {getOutputText(reportedAt)}
                    </td>
                  </Widget.TableRow>
                  <Widget.TableRow modifiers={['topGap']}>
                    <th>
                      <FormattedMessage {...messages.ecu} />
                    </th>
                    <td>
                      {getOutputText(ecu)}
                    </td>
                  </Widget.TableRow>
                </tbody>
              </Widget.Table>
            </SplitBlockPart>
            <SplitBlockPart modifiers={['left']}>
              <Widget.Table>
                <tbody>
                  <Widget.TableRow modifiers={['topGap']}>
                    <th>
                      <FormattedMessage {...messages.fmi} />
                    </th>
                    <td>
                      {getOutputText(fmi)}
                    </td>
                  </Widget.TableRow>
                  <Widget.TableRow modifiers={['topGap']}>
                    <th>
                      <FormattedMessage {...messages.count} />
                    </th>
                    <td>
                      {getOutputText(count)}
                    </td>
                  </Widget.TableRow>
                </tbody>
              </Widget.Table>
            </SplitBlockPart>
            <SplitBlockPart modifiers={['right']}>
              <Container>
                <RightPanelWrapper>
                  <StatusItemWrapper>
                    <SeverityLabel
                      color={severityColor}
                      value={severityLevel}
                      name={severityName}
                      fontSize={12}
                    />
                  </StatusItemWrapper>
                  {repairInstructionsUrl &&
                    <StatusItemWrapper>
                      <RepairInstructionsLink href={repairInstructionsUrl} />
                    </StatusItemWrapper>
                  }
                </RightPanelWrapper>
              </Container>
            </SplitBlockPart>
          </SplitBlock>
        </ContentWrapper>
        <SmoothCollapse expanded={showDetails}>
          <FaultDetails fault={fault} />
        </SmoothCollapse>
      </Wrapper>
    );
  }
}

export default ModalCaseFault;
