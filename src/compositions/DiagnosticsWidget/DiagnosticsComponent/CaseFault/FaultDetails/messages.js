import { defineMessages } from 'react-intl';

const messages = defineMessages({
  item: {
    id: 'compositions.DiagnosticsWidget.CaseFault.FaultDetails.SnapShot.item',
    defaultMessage: 'Item',
  },
  value: {
    id: 'compositions.DiagnosticsWidget.CaseFault.FaultDetails.SnapShot.value',
    defaultMessage: 'Value',
  },
  measure: {
    id: 'compositions.DiagnosticsWidget.CaseFault.FaultDetails.SnapShot.measure',
    defaultMessage: 'Measure',
  },
  other: {
    id: 'compositions.DiagnosticsWidget.CaseFault.FaultDetails.SnapShot.other',
    defaultMessage: 'Other',
  },
  relatedFaults: {
    id: 'compositions.DiagnosticsWidget.CaseFault.FaultDetails.relatedFaults.title',
    defaultMessage: 'Related Faults ({count})',
  },
});

export default messages;
