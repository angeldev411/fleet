import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import Container from 'elements/Container';
import Divider from 'elements/Divider';
import SeverityLabel from 'components/SeverityLabel';
import { SplitBlock, SplitBlockPart } from 'elements/SplitBlock';
import TextDiv from 'elements/TextDiv';
import Widget from 'elements/Widget';

import { formatDate } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';

import ContentWrapper from '../ContentWrapper';
import faultMessages from '../messages';
import RightPanelWrapper from '../RightPanelWrapper';
import StatusItemWrapper from '../StatusItemWrapper';

function RelatedFault({ fault }) {
  const {
    componentId,
    count,
    ecuId: ecu,
    failureMode: fmi,
    reportedAt: rawReportedAt,
    severity,
    status,
  } = fault;
  const reportedAt = rawReportedAt ? formatDate(rawReportedAt) : null;

  return (
    <Container modifiers={['fullWidth', 'noPad']}>
      <Divider modifiers={['extraLightGray', 'gutter']} />
      <SplitBlock>
        <SplitBlockPart modifiers={['column', 'left', 'wide']}>
          <Container>
            <TextDiv modifiers={['bold']}>
              {getOutputText(componentId)}
            </TextDiv>
          </Container>
          <ContentWrapper modifiers={['smallPadLeft']}>
            <SplitBlock pad="none">
              <SplitBlockPart modifiers={['left']}>
                <Widget.Table>
                  <tbody>
                    <Widget.TableRow>
                      <th>
                        <FormattedMessage {...faultMessages.status} />
                      </th>
                      <td>
                        {getOutputText(status)}
                      </td>
                    </Widget.TableRow>
                    <Widget.TableRow modifiers={['topGap']}>
                      <th>
                        <FormattedMessage {...faultMessages.datetime} />
                      </th>
                      <td>
                        {getOutputText(reportedAt)}
                      </td>
                    </Widget.TableRow>
                    <Widget.TableRow modifiers={['topGap']}>
                      <th>
                        <FormattedMessage {...faultMessages.ecu} />
                      </th>
                      <td>
                        {getOutputText(ecu)}
                      </td>
                    </Widget.TableRow>
                  </tbody>
                </Widget.Table>
              </SplitBlockPart>
              <SplitBlockPart modifiers={['left']}>
                <Widget.Table>
                  <tbody>
                    <Widget.TableRow modifiers={['topGap']}>
                      <th>
                        <FormattedMessage {...faultMessages.fmi} />
                      </th>
                      <td>
                        {getOutputText(fmi)}
                      </td>
                    </Widget.TableRow>
                    <Widget.TableRow modifiers={['topGap']}>
                      <th>
                        <FormattedMessage {...faultMessages.count} />
                      </th>
                      <td>
                        {getOutputText(count)}
                      </td>
                    </Widget.TableRow>
                  </tbody>
                </Widget.Table>
              </SplitBlockPart>
            </SplitBlock>
          </ContentWrapper>
        </SplitBlockPart>
        <SplitBlockPart modifiers={['right']}>
          <Container>
            <RightPanelWrapper>
              {severity &&
                <StatusItemWrapper>
                  <SeverityLabel
                    color={severity.color}
                    value={severity.level}
                    name={severity.name}
                    fontSize={12}
                  />
                </StatusItemWrapper>
              }
            </RightPanelWrapper>
          </Container>
        </SplitBlockPart>
      </SplitBlock>
    </Container>
  );
}

RelatedFault.propTypes = {
  fault: PropTypes.shape({
    componentId: PropTypes.string.isRequired,
  }).isRequired,
};

export default RelatedFault;
