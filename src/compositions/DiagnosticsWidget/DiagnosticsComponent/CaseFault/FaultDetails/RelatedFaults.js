import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import Container from 'elements/Container';
import Divider from 'elements/Divider';
import TextDiv from 'elements/TextDiv';

import messages from './messages';
import RelatedFault from './RelatedFault';

function RelatedFaults({ data }) {
  if (data.length <= 0) { return null; }

  return (
    <Container modifiers={['fullWidth', 'noPad']} >
      <Divider modifiers={['extraWide', 'light']} />
      <Container>
        <TextDiv modifiers={['bold', 'brand', 'mediumText']}>
          <FormattedMessage {...messages.relatedFaults} values={{ count: data.length }} />
        </TextDiv>
      </Container>
      {
        // eslint-disable-next-line react/no-array-index-key
        data.map((fault, index) => <RelatedFault key={index} fault={fault} />)
      }
    </Container>
  );
}

RelatedFaults.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      componentId: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

export default RelatedFaults;
