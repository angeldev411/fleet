import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  border-collapse: collapse;
  border: 2px solid ${props.theme.colors.base.chrome200};
  padding-left: ${px2rem(32)};
  text-align: left;
  width: 100%;
  thead {
    th {
      font-size: ${px2rem(14)};
      padding: ${px2rem(16)};
      text-transform: uppercase;
    }
  }
  tbody {
    color: ${props.theme.colors.base.textLight};
    font-size: ${px2rem(12)};
    tr:nth-child(odd) {
      background-color: ${props.theme.colors.base.chrome100};
    }
    th,
    td {
      padding: ${px2rem(8)} ${px2rem(16)};
    }
    td {
      font-weight: 200;
    }
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
      textLight: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'SnapShotTable',
  styled.table,
  styles,
  { themePropTypes },
);
