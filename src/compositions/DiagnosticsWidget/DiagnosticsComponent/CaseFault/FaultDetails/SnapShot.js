import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import Container from 'elements/Container';
import Divider from 'elements/Divider';
import TextDiv from 'elements/TextDiv';

import messages from './messages';
import SnapShotTable from './SnapShotTable';

function SnapShot({ data }) {
  const title = data.category.toLowerCase() === 'unknown' ?
    <FormattedMessage {...messages.other} /> :
    data.category;

  return (
    <Container
      modifiers={['fullWidth', 'noPad']}
    >
      <Container>
        <TextDiv modifiers={['bold', 'darkGreyText', 'mediumText']}>
          {title}
        </TextDiv>
      </Container>
      <Divider modifiers={['extraLightGray', 'gutter']} />
      <Container modifiers={['fullWidth']}>
        <SnapShotTable>
          <thead>
            <tr>
              <th><FormattedMessage {...messages.item} /></th>
              <th><FormattedMessage {...messages.value} /></th>
              <th><FormattedMessage {...messages.measure} /></th>
            </tr>
          </thead>
          <tbody>
            {data.items.map(item => (
              <tr key={item.description}>
                <th>
                  {item.description}
                </th>
                <td>
                  {item.value}
                </td>
                <td>
                  {item.unit}
                </td>
              </tr>
            ))}
          </tbody>
        </SnapShotTable>
      </Container>
    </Container>
  );
}

SnapShot.propTypes = {
  data: PropTypes.shape({
    category: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired,
  }).isRequired,
};

export default SnapShot;
