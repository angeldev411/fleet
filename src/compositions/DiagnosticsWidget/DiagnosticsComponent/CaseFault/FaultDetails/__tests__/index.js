import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import FaultDetails from '../index';

const fault = {
  relatedFaults: [
    { fault: 'one' },
  ],
  snapshots: [
    { category: 'one' },
    { category: 'two' },
    { category: 'three' },
  ],
};

const defaultProps = {
  fault,
};

function shallowRender(props = defaultProps) {
  return shallow(<FaultDetails {...props} />);
}

test('Renders the expected number of SnapShots for the fault', () => {
  const component = shallowRender();
  expect(component.find('SnapShot').length).toEqual(3);
});

test('Renders RelatedFaults with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('RelatedFaults');
  expect(component.find('RelatedFaults')).toHaveProp('data', fault.relatedFaults);
});
