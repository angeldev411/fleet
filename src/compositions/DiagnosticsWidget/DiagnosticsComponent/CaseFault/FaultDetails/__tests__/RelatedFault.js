import moment from 'moment';
import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import messages from 'utils/messages';

import RelatedFault from '../RelatedFault';

const fault = {
  reportedAt: moment().toISOString(),
  severity: {
    color: 'red',
    level: 3,
    name: 'severe',
  },
};

const defaultProps = {
  fault,
};

function shallowRender(props = defaultProps) {
  return shallow(<RelatedFault {...props} />);
}

test('renders the default datetime message with no reportedAt', () => {
  const testFault = { ...fault };
  delete testFault.reportedAt;
  const component = shallowRender({ fault: testFault });
  const tableRows = component.find('TableRow');
  expect(tableRows.length).toEqual(5);
  const tableCell = tableRows.at(1).find('td');
  expect(tableCell).toContain('FormattedMessage');
  expect(tableCell.find('FormattedMessage')).toHaveProps({ ...messages.noValue });
});

test('renders a SeverityLabel with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('SeverityLabel');
  expect(component.find('SeverityLabel')).toHaveProps({
    color: fault.severity.color,
    value: fault.severity.level,
    name: fault.severity.name,
  });
});

test('RelatedFault does not render the SeverityLabel if no severity is present', () => {
  const faultWithoutSeverity = { reportedAt: moment().toISOString() };
  const component = shallowRender({ fault: faultWithoutSeverity });
  expect(component).toNotContain('SeverityLabel');
});
