import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import SnapShot from '../SnapShot';
import messages from '../messages';

const snapshot = {
  category: 'one',
  items: [
    { description: 'test1', value: 'one', measure: 'miles' },
    { description: 'test2', value: 'two', measure: 'hrs' },
  ],
};

const defaultProps = {
  data: snapshot,
};

function shallowRender(props = defaultProps) {
  return shallow(<SnapShot {...props} />);
}

test('renders the category as the title', () => {
  const component = shallowRender();
  expect(component).toContain('TextDiv');
  expect(component.find('TextDiv').render().text()).toContain(snapshot.category);
});

test('renders "Other" as the title if the category is "Unknown"', () => {
  const testSnapShot = {
    ...snapshot,
    category: 'Unknown',
  };
  const testProps = { data: testSnapShot };
  const component = shallowRender(testProps);
  const textDiv = component.find('TextDiv');
  expect(textDiv).toContain('FormattedMessage');
  expect(textDiv.find('FormattedMessage')).toHaveProps({ ...messages.other });
});

test('renders a SnapShotTable with the expected number of rows in the body', () => {
  const component = shallowRender();
  expect(component).toContain('SnapShotTable');
  const table = component.find('SnapShotTable');
  const tbody = table.find('tbody');
  expect(tbody.find('tr').length).toEqual(snapshot.items.length);
});
