import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import RelatedFaults from '../RelatedFaults';
import messages from '../messages';

const relatedFaults = [
  { componentId: '1' },
  { componentId: '2' },
  { componentId: '3' },
];

const defaultProps = {
  data: relatedFaults,
};

function shallowRender(props = defaultProps) {
  return shallow(<RelatedFaults {...props} />);
}

test('returns null with no faults present', () => {
  const testProps = {
    data: [],
  };
  const component = shallowRender(testProps);
  expect(component.node).toEqual(null);
});

test('renders a title message with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('TextDiv');
  const title = component.find('TextDiv');
  expect(title).toContain('FormattedMessage');
  expect(title.find('FormattedMessage')).toHaveProps({
    ...messages.relatedFaults,
  });
  expect(title.find('FormattedMessage').props().values).toEqual({ count: relatedFaults.length });
});
