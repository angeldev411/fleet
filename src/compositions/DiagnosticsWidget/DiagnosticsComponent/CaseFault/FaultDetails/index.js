import PropTypes from 'prop-types';
import React from 'react';
import { sizes } from 'reactive-container';

import Card from 'elements/Card';
import { ReactiveContainer } from 'elements/Container';

import FaultDetailsWrapper from './FaultDetailsWrapper';
import RelatedFaults from './RelatedFaults';
import SnapShot from './SnapShot';

function FaultDetails({ fault }) {
  const { snapshots, relatedFaults } = fault;

  return (
    <FaultDetailsWrapper>
      <Card>
        <ReactiveContainer
          breakpoints={[
            { name: sizes.LG, minWidth: 800 },
          ]}
          modifiers={['fullWidth', 'noPad']}
        >
          {snapshots.map(snapshot => <SnapShot key={snapshot.category} data={snapshot} />)}
          <RelatedFaults data={relatedFaults} />
        </ReactiveContainer>
      </Card>
    </FaultDetailsWrapper>
  );
}

FaultDetails.propTypes = {
  fault: PropTypes.shape({
    relatedFaults: PropTypes.array.isRequired,
    snapshots: PropTypes.array.isRequired,
  }).isRequired,
};

export default FaultDetails;
