import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  height: ${px2rem(30)};
`;

export default buildStyledComponent(
  'StatusItemWrapper',
  styled.div,
  styles,
);
