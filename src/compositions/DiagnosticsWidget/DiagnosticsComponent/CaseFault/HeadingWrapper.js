import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.linkHover};
  font-size: ${px2rem(12)};
  font-weight: 600;
  padding: ${px2rem(3)};
  position: relative;
  text-transform: uppercase;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      linkHover: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'HeadingWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
