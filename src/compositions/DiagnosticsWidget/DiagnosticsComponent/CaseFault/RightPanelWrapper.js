import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  padding-right: ${px2rem(8)};
  text-align: right;
`;


export default buildStyledComponent(
  'RightPanelWrapper',
  styled.div,
  styles,
);
