import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  padLeft: () => ({
    styles: `
      padding-left: ${px2rem(12)};
    `,
  }),
};

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.text};
  cursor: pointer;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'TitleWrapper',
  styled.div,
  styles,
  { modifierConfig, themePropTypes },
);
