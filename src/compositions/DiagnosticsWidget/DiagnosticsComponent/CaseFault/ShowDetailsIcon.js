import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';

import ShowDetailsIconWrapper from './ShowDetailsIconWrapper';

function ShowDetailsIcon({ modifiers, showDetails }) {
  return (
    <ShowDetailsIconWrapper modifiers={modifiers}>
      <FontAwesome name={showDetails ? 'chevron-up' : 'chevron-down'} />
    </ShowDetailsIconWrapper>
  );
}

ShowDetailsIcon.propTypes = {
  modifiers: PropTypes.arrayOf(
    PropTypes.string,
  ),
  showDetails: PropTypes.bool.isRequired,
};

ShowDetailsIcon.defaultProps = {
  modifiers: [],
};

export default ShowDetailsIcon;
