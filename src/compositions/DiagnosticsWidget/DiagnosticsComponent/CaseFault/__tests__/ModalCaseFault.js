import moment from 'moment';
import React from 'react';

import {
  test,
  expect,
  createSpy,
  shallow,
} from '__tests__/helpers/test-setup';

import ModalCaseFault from '../ModalCaseFault';
import faultMessages from '../../messages';

const fault = {
  reportedAt: moment().toISOString(),
  severity: {
    color: 'red',
    level: 3,
    name: 'severe',
  },
};

const defaultProps = {
  displayFault: {
    fault,
    isExpanded: true,
    showDetails: false,
  },
  handleTitleClick: () => {},
  onExpand: () => {},
};

function shallowRender(props = defaultProps) {
  return shallow(<ModalCaseFault {...props} />);
}

test('modifiers includes `bottom` when bottom prop is true', () => {
  const component = shallowRender({ ...defaultProps, bottom: true });
  const wrapper = component.find('Wrapper');
  expect(wrapper.props().modifiers).toInclude('bottom');
});

test('clicking on HeadingWrapper calls handleTitleClick', () => {
  const handleTitleClick = createSpy();
  const preventDefaultSpy = createSpy();
  const component = shallowRender({ ...defaultProps, handleTitleClick });
  expect(component).toContain('HeadingWrapper');
  component.find('HeadingWrapper').simulate('click', { preventDefault: preventDefaultSpy });
  expect(preventDefaultSpy).toHaveBeenCalled();
  expect(handleTitleClick).toHaveBeenCalled();
});

test('mouse enter and leave events on HeadingWrapper update state correctly', () => {
  const component = shallowRender();
  const instance = component.instance();
  expect(instance.state.displayDetailsChevron).toEqual(false);
  component.find('HeadingWrapper').simulate('mouseEnter');
  expect(instance.state.displayDetailsChevron).toEqual(true);
  component.find('HeadingWrapper').simulate('mouseLeave');
  expect(instance.state.displayDetailsChevron).toEqual(false);
});

test('renders the default datetime message with no reportedAt', () => {
  const testFault = { ...fault };
  delete testFault.reportedAt;
  const component = shallowRender({
    ...defaultProps,
    displayFault: {
      fault: testFault,
      isExpanded: true,
      showDetails: false,
    },
  });
  const tableRows = component.find('TableRow');
  expect(tableRows.length).toEqual(5);
  const tableCell = tableRows.at(1).find('td');
  expect(tableCell).toContain('FormattedMessage');
  expect(tableCell.find('FormattedMessage')).toHaveProps({ ...faultMessages.datetime });
});

test('renders a SeverityLabel with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('SeverityLabel');
  expect(component.find('SeverityLabel')).toHaveProps({
    color: fault.severity.color,
    value: fault.severity.level,
    name: fault.severity.name,
  });
});

test('renders a repair instructions link if fault includes a repairInstructionsUrl', () => {
  const testFault = {
    ...fault,
    repairInstructionsUrl: 'http://google.com',
  };
  const component = shallowRender({
    ...defaultProps,
    displayFault: {
      fault: testFault,
      isExpanded: true,
      showDetails: false,
    },
  });
  expect(component.find('StatusItemWrapper').last()).toContain('RepairInstructionsLink');
  const link = component.find('StatusItemWrapper').last().find('RepairInstructionsLink');
  expect(link).toHaveProp('href', testFault.repairInstructionsUrl);
});
