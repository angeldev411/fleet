import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import RepairInstructionsLink from '../RepairInstructionsLink';
import messages from '../messages';

const defaultProps = { href: 'test' };

function shallowRender(props = defaultProps) {
  return shallow(<RepairInstructionsLink {...props} />);
}

test('renders an A with href', () => {
  const component = shallowRender();
  expect(component.find('A')).toHaveProp('href', defaultProps.href);
});

test('renders file pdf icon', () => {
  const component = shallowRender();
  expect(component.find('FontAwesome')).toHaveProp('name', 'file-pdf-o');
});

test('renders the view repair instructions message', () => {
  const component = shallowRender();
  expect(component.find('FormattedMessage')).toHaveProps({ ...messages.viewRepairInstructions });
});
