import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import ShowDetailsIcon from '../ShowDetailsIcon';

const defaultProps = {
  showDetails: false,
};

function shallowRender(props = defaultProps) {
  return shallow(<ShowDetailsIcon {...props} />);
}

test('renders an ShowDetailsIconWrapper with modifiers', () => {
  const testProps = { showDetails: false, modifiers: ['hidden'] };
  const component = shallowRender(testProps);
  expect(component.find('ShowDetailsIconWrapper')).toHaveProp('modifiers', testProps.modifiers);
});

test('renders chevron-down icon by default', () => {
  const component = shallowRender();
  expect(component.find('FontAwesome')).toHaveProp('name', 'chevron-down');
});

test('renders a chevron-up icon with showDetails = true', () => {
  const component = shallowRender({ showDetails: true });
  expect(component.find('FontAwesome')).toHaveProp('name', 'chevron-up');
});
