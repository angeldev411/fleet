import moment from 'moment';
import React from 'react';

import {
  test,
  expect,
  createSpy,
  shallow,
} from '__tests__/helpers/test-setup';

import CaseFault from '../index';
import faultMessages from '../../messages';

const fault = {
  reportedAt: moment().toISOString(),
  severity: {
    color: 'red',
    level: 3,
    name: 'severe',
  },
};

const defaultProps = {
  displayFault: {
    fault,
    isExpanded: true,
  },
  handleTitleClick: () => {},
  onExpand: () => {},
};

function shallowRender(props = defaultProps) {
  return shallow(<CaseFault {...props} />);
}

test('modifiers includes `bottom` when bottom prop is true', () => {
  const component = shallowRender({ ...defaultProps, bottom: true });
  const wrapper = component.find('Wrapper');
  expect(wrapper.props().modifiers).toInclude('bottom');
});

test('disallow defaultEvent when componentId Link is clicked', () => {
  const component = shallowRender();
  const titleWrapper = component.find('TitleWrapper');
  const preventDefault = createSpy();
  titleWrapper.find('Link').simulate('click', { preventDefault });
  expect(preventDefault).toHaveBeenCalled();
});

test('renders Expandable with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('Connect(Expandable)');
  expect(component.find('Connect(Expandable)')).toHaveProps({
    isExpanded: true,
    onExpand: defaultProps.onExpand,
  });
});

test('clicking on HeadingWrapper calls handleTitleClick', () => {
  const handleTitleClick = createSpy();
  const preventDefaultSpy = createSpy();
  const component = shallowRender({ ...defaultProps, handleTitleClick });
  expect(component).toContain('HeadingWrapper');
  component.find('HeadingWrapper').simulate('click', { preventDefault: preventDefaultSpy });
  expect(preventDefaultSpy).toHaveBeenCalled();
  expect(handleTitleClick).toHaveBeenCalled();
});

test('renders the default datetime message with no reportedAt', () => {
  delete fault.reportedAt;
  const component = shallowRender({
    ...defaultProps,
    displayFault: {
      fault,
    },
  });
  expect(component.find('TableRow').length).toEqual(5);
  expect(component.find('TableRow').at(1).find('td')).toContain('FormattedMessage');
  expect(component.find('TableRow').at(1).find('td').find('FormattedMessage'))
    .toHaveProps({ ...faultMessages.datetime });
});

test('renders a SeverityLabel with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('SeverityLabel');
  expect(component.find('SeverityLabel')).toHaveProps({
    color: fault.severity.color,
    value: fault.severity.level,
    name: fault.severity.name,
  });
});

test('renders a repair instructions link if fault includes a repairInstructionsUrl', () => {
  fault.repairInstructionsUrl = 'http://google.com';
  const component = shallowRender({
    ...defaultProps,
    displayFault: {
      fault,
    },
  });
  expect(component.find('StatusItemWrapper').last()).toContain('RepairInstructionsLink');
  const link = component.find('StatusItemWrapper').last().find('RepairInstructionsLink');
  expect(link).toHaveProp('href', fault.repairInstructionsUrl);
});
