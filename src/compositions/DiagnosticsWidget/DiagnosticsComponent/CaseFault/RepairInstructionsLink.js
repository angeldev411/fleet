import PropTypes from 'prop-types';
import React from 'react';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage } from 'react-intl';

import { A } from 'base-components';

import RepairInstructionsIconWrapper from './RepairInstructionsIconWrapper';
import messages from './messages';


function RepairInstructionsLink({ href }) {
  return (
    <A href={href}>
      <RepairInstructionsIconWrapper>
        <FontAwesome name="file-pdf-o" />
      </RepairInstructionsIconWrapper>
      <FormattedMessage {...messages.viewRepairInstructions} />
    </A>
  );
}

RepairInstructionsLink.propTypes = {
  href: PropTypes.string.isRequired,
};

export default RepairInstructionsLink;
