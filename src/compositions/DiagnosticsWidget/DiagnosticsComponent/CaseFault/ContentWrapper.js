
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const modifierConfig = {
  smallPadLeft: () => ({
    styles: `padding-left: ${px2rem(6)};`,
  }),
};

/* istanbul ignore next */
const styles = () => `
  padding-left: ${px2rem(12)};
  width: auto;
`;

export default buildStyledComponent(
  'ContentWrapper',
  styled.div,
  styles,
  { modifierConfig },
);
