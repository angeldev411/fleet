import { defineMessages } from 'react-intl';

const messages = defineMessages({
  additionalWarnings: {
    id: 'compositions.DiagnosticsWidget.DiagnosticsComponent.CaseFault.data.additionalWarnings',
    defaultMessage: 'Additional Warnings',
  },
  componentId: {
    id: 'compositions.DiagnosticsWidget.DiagnosticsComponent.CaseFault.data.componentId',
    defaultMessage: 'Component Id',
  },
  count: {
    id: 'compositions.DiagnosticsWidget.DiagnosticsComponent.CaseFault.data.count',
    defaultMessage: 'Count',
  },
  datetime: {
    id: 'compositions.DiagnosticsWidget.DiagnosticsComponent.CaseFault.data.datetime',
    defaultMessage: 'Date/Time',
  },
  ecu: {
    id: 'compositions.DiagnosticsWidget.DiagnosticsComponent.CaseFault.data.ecu',
    defaultMessage: 'ECU',
  },
  fmi: {
    id: 'compositions.DiagnosticsWidget.DiagnosticsComponent.CaseFault.data.fmi',
    defaultMessage: 'FMI',
  },
  status: {
    id: 'compositions.DiagnosticsWidget.DiagnosticsComponent.CaseFault.data.status',
    defaultMessage: 'Status',
  },
  viewRepairInstructions: {
    id: 'compositions.DiagnosticsWidget.DiagnosticsComponent.CaseFault.data.viewRepairInstructions',
    defaultMessage: 'View Repair Instructions',
  },
});

export default messages;
