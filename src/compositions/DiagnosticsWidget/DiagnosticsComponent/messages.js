import { defineMessages } from 'react-intl';

const messages = defineMessages({
  modalTitle: {
    id: 'compositions.DiagnosticsWidget.modalTitle',
    defaultMessage: 'Diagnostics Details',
  },
  noFaults: {
    id: 'compositions.DiagnosticsWidget.noFaults',
    defaultMessage: 'Not available. Please check again later.',
  },
  title: {
    id: 'compositions.DiagnosticsWidget.title',
    defaultMessage: 'Diagnostics',
  },
});

export default messages;
