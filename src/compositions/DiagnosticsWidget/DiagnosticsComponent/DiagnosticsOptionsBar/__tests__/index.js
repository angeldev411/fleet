import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import DiagnosticsOptionsBar from '../index';

const onChangeOption = () => {};

const defaultProps = {
  onChangeOption,
  options: [{ label: 'Test', value: 'test' }],
};

function shallowRender(props = defaultProps) {
  return shallow(<DiagnosticsOptionsBar {...props} />);
}

test('renders a DropDownSelector with the expected props', () => {
  const component = shallowRender();
  expect(component).toContain('DropDownSelector');
  expect(component.find('DropDownSelector')).toHaveProps({
    onChange: onChangeOption,
    options: defaultProps.options,
  });
});
