import React from 'react';
import PropTypes from 'prop-types';

import DropDownSelector from 'components/_common/DropDownSelector';

import {
  SplitBlock,
  SplitBlockPart,
} from 'elements/SplitBlock';

function DiagnosticsOptionsBar({
  options,
  onChangeOption,
}) {
  return (
    <SplitBlock modifiers={['pad']}>
      <SplitBlockPart modifiers={['right']}>
        <DropDownSelector
          onChange={onChangeOption}
          options={options}
        />
      </SplitBlockPart>
    </SplitBlock>
  );
}

DiagnosticsOptionsBar.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  })).isRequired,
  onChangeOption: PropTypes.func.isRequired,
};

DiagnosticsOptionsBar.defaultProps = {
  selectedDiagnosticsOption: undefined,
};

export default DiagnosticsOptionsBar;
