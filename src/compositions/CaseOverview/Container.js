import React from 'react';
import { connect } from 'react-redux';

import {
  currentCaseAssetSelector,
  currentCaseSelector,
} from 'redux/cases/selectors';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return <WrappedComponent {...props} />;
  }

  function mapStateToProps(state) {
    return {
      assetInfo: currentCaseAssetSelector(state),
      caseInfo: currentCaseSelector(state),
    };
  }

  return connect(mapStateToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
