import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { CaseOverview } from '../index';

const defaultProps = {
  collapsed: false,
};

function shallowRender(props = defaultProps) {
  return shallow(<CaseOverview {...props} />);
}

test('Renders a CaseOverview', () => {
  const component = shallowRender();
  expect(component).toContain('#case-overview');
});

test('Includes an asset information table', () => {
  const component = shallowRender();
  expect(component).toContain('Table');
});

test('Includes a SeverityLabel with expected props', () => {
  const caseInfo = {
    severityColor: 'yellow',
    severityCount: '2',
  };
  const component = shallowRender({ ...defaultProps, caseInfo });
  expect(component).toContain('SeverityLabel');
  expect(component.find('SeverityLabel')).toHaveProps({
    color: caseInfo.severityColor,
    value: Number(caseInfo.severityCount),
  });
});

test('Includes UnreadNoteLabel if there are unread notes', () => {
  const count = 2;
  const caseInfo = { unreadNotesCount: `${count}` };
  const component = shallowRender({ ...defaultProps, caseInfo });
  expect(component).toContain('UnreadNoteLabelContainer');
  expect(component.find('UnreadNoteLabelContainer')).toHaveProp('caseInfo', caseInfo);
});

test('Does not include UnreadNoteLabel if no unread notes', () => {
  const caseInfo = { unreadNotesCount: '0' };
  const component = shallowRender({ ...defaultProps, caseInfo });
  expect(component).toNotContain('UnreadNoteLabelContainer');
});
