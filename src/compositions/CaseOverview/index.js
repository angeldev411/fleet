import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { compose, setDisplayName } from 'recompose';

import { Column, Container, Row } from 'styled-components-reactive-grid';

import SeverityLabel from 'components/SeverityLabel';
import UnreadNoteLabel from 'containers/UnreadNoteLabelContainer';

import BreadCrumbLink from 'compositions/BreadCrumbLink';

import Table from 'elements/Table';

import { getOutputText } from 'utils/widget';

import withConnectedData from './Container';
import messages from './messages';

export function CaseOverview({ assetInfo, caseInfo }) {
  const unreadNoteCount = Number(caseInfo.unreadNotesCount) || 0;
  const severityColor = caseInfo.severityColor;
  const severityCount = Number(caseInfo.severityCount);
  const { unitNumber, year, make, model } = assetInfo;
  // NOTE: The extra padding on top is required to correct other display issues on this page.
  // This must be removed when this component is exported to a composition, and brought in via
  // the config file.
  return (
    <Container id="case-overview" style={{ paddingTop: '16px' }}>
      <BreadCrumbLink />
      <Row modifiers={['middle']}>
        <Column modifiers={['col']}>
          <Table modifiers={['mediumGrey', 'xLargeText']}>
            <tbody>
              <tr>
                <th><FormattedMessage {...messages.unit} /></th>
                <td>{getOutputText(unitNumber)}</td>
                <th><FormattedMessage {...messages.year} /></th>
                <td>{getOutputText(year)}</td>
                <th><FormattedMessage {...messages.make} /></th>
                <td>{getOutputText(make)}</td>
                <th><FormattedMessage {...messages.model} /></th>
                <td>{getOutputText(model)}</td>
              </tr>
            </tbody>
          </Table>
        </Column>
      </Row>
      <Row modifiers={['end']}>
        <Column>
          <SeverityLabel
            color={severityColor}
            value={severityCount}
          />
        </Column>
        {unreadNoteCount > 0 &&
          <Column>
            <UnreadNoteLabel caseInfo={caseInfo} />
          </Column>
        }
      </Row>
    </Container>
  );
}

CaseOverview.propTypes = {
  assetInfo: PropTypes.shape({
    id: PropTypes.string,
    unitNumber: PropTypes.string,
    year: PropTypes.string,
    make: PropTypes.string,
    model: PropTypes.string,
  }).isRequired,
  caseInfo: PropTypes.shape({
    severityColor: PropTypes.string,
    severityCount: PropTypes.string,
  }).isRequired,
};

CaseOverview.defaultProps = {
  assetInfo: {},
  caseInfo: {
    severityColor: 'grey',
    severityCount: '0',
  },
};

export default compose(
  setDisplayName('CaseOverview'),
  withConnectedData,
)(CaseOverview);
