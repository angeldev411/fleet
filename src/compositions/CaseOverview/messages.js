import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  make: {
    id: 'compositions.CaseOverview.subtitle.make',
    defaultMessage: 'Make',
  },
  model: {
    id: 'compositions.CaseOverview.subtitle.model',
    defaultMessage: 'Model',
  },
  unit: {
    id: 'compositions.CaseOverview.subtitle.unit',
    defaultMessage: 'Unit',
  },
  year: {
    id: 'compositions.CaseOverview.subtitle.year',
    defaultMessage: 'Year',
  },
});

export default formattedMessages;
