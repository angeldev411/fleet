import numeral from 'numeral';
import PropTypes from 'prop-types';
import React from 'react';
import { withTheme } from 'styled-components';
import { compose, setDisplayName } from 'recompose';

import { getOutputText } from 'utils/widget';

export function calculatePercentage(estimateTotalString, invoiceTotalString) {
  const invoiceTotal = numeral(invoiceTotalString).value();
  const estimateTotal = numeral(estimateTotalString).value();
  const percentageFloat = (((invoiceTotal - estimateTotal) / estimateTotal) * 100);
  return parseInt(percentageFloat, 10);
}

export function colorForPercentage(percentage, theme) {
  switch (true) {
    case (percentage > 0):
      return theme.colors.status.danger;
    case (percentage < 0):
      return theme.colors.status.success;
    default:
      return 'inherit';
  }
}

export function formatPercentage(percentage) {
  switch (true) {
    case (percentage > 0):
      return ` (+${percentage}%)`;
    case (percentage < 0):
      return ` (${percentage}%)`;
    default:
      return null;
  }
}

function InvoiceInfo({ estimateTotal, invoiceTotal, theme }) {
  const percentage = estimateTotal && invoiceTotal ?
    calculatePercentage(estimateTotal, invoiceTotal) : 0;

  const color = colorForPercentage(percentage, theme);
  const formattedPercentage = formatPercentage(percentage);

  return (
    <div style={{ color }}>
      {getOutputText(invoiceTotal)}
      {formattedPercentage}
    </div>
  );
}

InvoiceInfo.propTypes = {
  estimateTotal: PropTypes.string,
  invoiceTotal: PropTypes.string,
  theme: PropTypes.shape({
    colors: PropTypes.shape({
      status: PropTypes.shape({
        danger: PropTypes.string.isRequired,
        success: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
};

InvoiceInfo.defaultProps = {
  estimateTotal: null,
  invoiceTotal: null,
};

export default compose(
  setDisplayName('InvoiceInfo'),
  withTheme,
)(InvoiceInfo);
