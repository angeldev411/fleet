import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose, setDisplayName } from 'recompose';
import { FormattedMessage } from 'react-intl';
import { compact } from 'lodash';

import InlineEditableField from 'components/_common/InlineEditableField';
import Widget from 'elements/Widget';

import PdfLink from 'components/PdfLink';

import { formatDate } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';
import cardType from 'utils/cardType';

import messages from './messages';
import InvoiceInfo from './InvoiceInfo';

import withConnectedData from './Container';

export class EstimateInvoiceDecidedWidget extends Component {
  state = {
    poNumber: '',
    initialPoNumber: '',
  }

  onChangePONumber = (e) => {
    this.setState({
      poNumber: e.target.value,
    });
  }

  onUpdatePONumber = (isSubmitting) => {
    if (!isSubmitting) {
      this.setState({
        poNumber: this.state.initialPoNumber,
      });
    }
  }

  setInitialPONumber = (value) => {
    this.setState({
      initialPoNumber: value,
    });
  }

  render() {
    const { caseInfo } = this.props;
    const {
      approvalStatus,
      etr,
      roUrl,
      roNumber,
      invoiceId,
      invoiceTotal,
      estimateTotal,
      approvalComment,
      invoiceDate,
    } = caseInfo;

    const formattedEtr = etr ? formatDate(etr) : getOutputText(etr);
    const poNumber = this.state.poNumber || caseInfo.poNumber || '';
    const invoiceDateStr = invoiceDate ?
      formatDate(invoiceDate) : getOutputText(invoiceDate);
    const isApprovedOrDeclined = approvalStatus === 'approved'
      || approvalStatus === 'declined';
    const approvalCommentModifier = roUrl && 'topGap';
    const statusCardType = cardType(approvalStatus);

    return (
      approvalStatus !== 'Pending' &&
        <Widget
          id="estimate-invoice"
          className="estimate-invoice-decided"
          expandKey="estimate-invoice"
        >
          <Widget.Header>
            <FormattedMessage {...messages.title} />
          </Widget.Header>
          <Widget.Item>
            <Widget.SubHeader modifiers={compact([statusCardType])}>
              {approvalStatus}
            </Widget.SubHeader>
          </Widget.Item>
          <Widget.Item horizontal>
            <Widget.Table>
              <tbody>
                <Widget.TableRow>
                  <th>
                    <FormattedMessage {...messages.amount} />
                  </th>
                  <td>
                    {estimateTotal}
                  </td>
                </Widget.TableRow>
                <Widget.TableRow modifiers={['topGap']}>
                  <th>
                    <FormattedMessage {...messages.etr} />
                  </th>
                  <td>
                    {formattedEtr}
                  </td>
                </Widget.TableRow>
                <Widget.TableRow modifiers={['topGap']}>
                  <th>
                    <FormattedMessage {...messages.repairOrder} />
                  </th>
                  <td>
                    {roNumber && '#'}
                    {getOutputText(roNumber)}
                  </td>
                </Widget.TableRow>
                <Widget.TableRow modifiers={['topGap']}>
                  <th>
                    <FormattedMessage {...messages.po} />
                  </th>
                  {isApprovedOrDeclined ?
                    <td>
                      <InlineEditableField
                        value={poNumber}
                        onUpdate={this.onUpdatePONumber}
                        onChange={this.onChangePONumber}
                        getInitialValue={this.setInitialPONumber}
                      />
                    </td>
                    : <td>
                      {poNumber && '#'}
                      {getOutputText(poNumber)}
                    </td>
                  }
                </Widget.TableRow>
                <Widget.TableRow modifiers={['topGap']}>
                  <th>
                    <FormattedMessage {...messages.invoice} />
                  </th>
                  <td>
                    {invoiceId && '#'}
                    {getOutputText(invoiceId)}
                  </td>
                </Widget.TableRow>
                <Widget.TableRow>
                  <td />
                  <td>
                    <InvoiceInfo estimateTotal={estimateTotal} invoiceTotal={invoiceTotal} />
                  </td>
                </Widget.TableRow>
                <Widget.TableRow>
                  <th />
                  <td>
                    {invoiceDateStr}
                  </td>
                </Widget.TableRow>
              </tbody>
            </Widget.Table>
            <Widget.Table>
              <tbody>
                <Widget.TableRow>
                  <th>
                    {roUrl &&
                      <FormattedMessage {...messages.estimates} />
                    }
                  </th>
                  <td>
                    <PdfLink url={roUrl} message={messages.viewLatestEstimate} />
                  </td>
                </Widget.TableRow>
                <Widget.TableRow modifiers={compact([approvalCommentModifier])}>
                  <th>
                    {approvalComment &&
                      <FormattedMessage {...messages.comments} />
                    }
                  </th>
                  <td>
                    {approvalComment}
                  </td>
                </Widget.TableRow>
              </tbody>
            </Widget.Table>
          </Widget.Item>
          <Widget.Item />
        </Widget>
    );
  }
}

EstimateInvoiceDecidedWidget.propTypes = {
  caseInfo: PropTypes.shape({
    approvalStatus: PropTypes.string,
    estimateTotal: PropTypes.string,
    invoiceId: PropTypes.string,
    invoiceTotal: PropTypes.string,
    roNumber: PropTypes.string,
    poNumber: PropTypes.string,
  }).isRequired,
};

export default compose(
  setDisplayName('EstimateInvoiceDecidedWidget'),
  withConnectedData,
)(EstimateInvoiceDecidedWidget);
