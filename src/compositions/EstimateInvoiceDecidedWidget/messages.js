import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  accept: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.accept',
    defaultMessage: 'Accept',
  },
  amount: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.amount',
    defaultMessage: 'Amount',
  },
  approve: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.approve',
    defaultMessage: 'Approve',
  },
  comments: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.comments',
    defaultMessage: 'Comments',
  },
  decline: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.decline',
    defaultMessage: 'Decline',
  },
  error: {
    id: 'compositions.EstimateInvoiceDecidedWidget.error',
    defaultMessage: 'Error',
  },
  estimates: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.estimates',
    defaultMessage: 'Estimates',
  },
  estimateTotal: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.estimateTotal',
    defaultMessage: 'Estimate Total',
  },
  etr: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.etr',
    defaultMessage: 'ETR',
  },
  invoice: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.invoice',
    defaultMessage: 'Invoice',
  },
  invoiceDate: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.invoiceDate',
    defaultMessage: 'Invoice Date',
  },
  invoiceId: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.invoiceId',
    defaultMessage: 'Invoice Id',
  },
  optional: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.optional',
    defaultMessage: 'Optional',
  },
  po: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.po',
    defaultMessage: 'Purchase Order',
  },
  provideANote: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.provideANote',
    defaultMessage: 'Provide a Note about your decision',
  },
  repairOrder: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.repairOrder',
    defaultMessage: 'Repair Order',
  },
  submissionFailed: {
    id: 'compositions.EstimateInvoiceDecidedWidget.submissionFailed',
    defaultMessage: 'Your submission has failed.',
  },
  submit: {
    id: 'compositions.EstimateInvoiceDecidedWidget.submit',
    defaultMessage: 'SUBMIT',
  },
  title: {
    id: 'compositions.EstimateInvoiceDecidedWidget.title',
    defaultMessage: 'Estimate/Invoice',
  },
  viewEstimate: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.viewEstimate',
    defaultMessage: 'View Estimate',
  },
  viewLatestEstimate: {
    id: 'compositions.EstimateInvoiceDecidedWidget.data.viewLatestEstimate',
    defaultMessage: 'View Latest Estimate',
  },
});

export default formattedMessages;
