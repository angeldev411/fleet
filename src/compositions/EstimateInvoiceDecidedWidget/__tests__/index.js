import React from 'react';
import { FormattedMessage } from 'react-intl';
import { test, expect, shallow, mount, MountableTestComponent } from '__tests__/helpers/test-setup';

import { EstimateInvoiceDecidedWidget } from 'compositions/EstimateInvoiceDecidedWidget';

import { formatDate } from 'utils/timeUtils';
import messages from '../messages';

const testCaseInfo = {
  estimateTotal: '$1232.00',
  invoiceId: '456',
  invoiceTotal: '$1500.00',
  roNumber: '987',
  poNumber: '654321',
};

function shallowRender(caseInfo = testCaseInfo) {
  return shallow(<EstimateInvoiceDecidedWidget caseInfo={caseInfo} />);
}

test('EstimateInvoiceDecidedWidget renders a Widget component if approvalStatus is NOT `Pending`', () => {
  const caseInfo = {
    approvalStatus: 'Approved',
  };
  const component = shallowRender({ ...testCaseInfo, ...caseInfo });
  expect(component).toContain('Widget');
});

test('EstimateInvoiceDecidedWidget renders nothing if approvalStatus is `Pending`', () => {
  const caseInfo = {
    approvalStatus: 'Pending',
  };
  const component = shallowRender({ ...testCaseInfo, ...caseInfo });
  expect(component.children().length).toEqual(0);
});

test('EstimateInvoiceDecidedWidget renders the correct approval status', () => {
  const caseInfo = {
    approvalStatus: 'Approved',
  };
  const component = mount(
    <MountableTestComponent>
      <EstimateInvoiceDecidedWidget caseInfo={caseInfo} />
    </MountableTestComponent>,
  );
  const subHeader = component.find('SubHeader').render();
  expect(subHeader.text()).toEqual(caseInfo.approvalStatus);
});

test('EstimateInvoiceDecidedWidget renders the correct values into the table', () => {
  const caseInfoOverrides = {
    etr: '2015-04-07',
    invoiceDate: '2017-04-07',
  };
  const component = mount(
    <MountableTestComponent>
      <EstimateInvoiceDecidedWidget caseInfo={{ ...testCaseInfo, ...caseInfoOverrides }} />
    </MountableTestComponent>,
  );

  const table = component.find('Table').first().render();
  const tableText = table.text();
  expect(tableText).toInclude(testCaseInfo.estimateTotal);
  expect(tableText).toInclude(testCaseInfo.invoiceId);
  expect(tableText).toInclude(testCaseInfo.invoiceTotal);
  expect(tableText).toInclude(testCaseInfo.roNumber);
  expect(tableText).toInclude(testCaseInfo.poNumber);
  expect(tableText).toInclude(formatDate(caseInfoOverrides.etr));
  expect(tableText).toInclude(formatDate(caseInfoOverrides.invoiceDate));
});

test('EstimateInvoiceDecidedWidget renders an InvoiceInfo with the estimate and invoice totals', () => {
  const component = shallowRender();
  expect(component).toContain('InvoiceInfo');
  const invoiceInfo = component.find('InvoiceInfo');
  expect(invoiceInfo).toHaveProps({
    estimateTotal: testCaseInfo.estimateTotal,
    invoiceTotal: testCaseInfo.invoiceTotal,
  });
});

test('EstimateInvoiceDecidedWidget renders `View Latest Estimate` when roUrl exists', () => {
  const roUrl = { roUrl: 'http://example.com/foobar.pdf' };
  const component = shallowRender({ ...testCaseInfo, ...roUrl });
  const secondColumn = component.find('Table').last();
  expect(secondColumn).toContain('PdfLink');
  const pdfLink = secondColumn.find('PdfLink');
  expect(pdfLink).toHaveProp('url', roUrl.roUrl);
  expect(pdfLink.props().message).toEqual(messages.viewLatestEstimate);
});

test('EstimateInvoiceDecidedWidget does NOT render `View Latest Estimate` when roUrl does NOT exist', () => {
  const component = shallowRender();
  const secondColumn = component.find('Table').last();
  expect(secondColumn).toNotContain('FontAwesome[name="file-pdf-o"]');
});

test('EstimateInvoiceDecidedWidget renders comments when approvalComment exists', () => {
  const caseInfo = {
    approvalComment: 'Comments',
  };
  const component = shallowRender({ ...testCaseInfo, ...caseInfo });

  const table = component.find('Table').last();
  const row = table.find('TableRow').last();
  const th = row.find('th');
  expect(th).toContain('FormattedMessage');
  expect(th.find('FormattedMessage')).toHaveProps({ ...messages.comments });

  const td = row.find('td');
  const tdChildren = td.props().children;
  expect(tdChildren).toContain(caseInfo.approvalComment);
});

test('EstimateInvoiceDecidedWidget does NOT render comments when approvalComment does NOT exist', () => {
  const component = shallowRender();
  const secondColumn = component.find('Table').last();
  expect(secondColumn).toNotContain(<FormattedMessage {...messages.comments} />);
});

test('EstimateInvoiceDecidedWidget passes topGap props for approvalComment when roUrl exists', () => {
  const caseInfo = {
    roUrl: 'roUrl',
    approvalComment: 'Some comments',
  };
  const component = shallowRender({ ...testCaseInfo, ...caseInfo });
  const secondColumn = component.find('Table').last();
  const approvalCommentRow = secondColumn.find('TableRow').last();
  expect(approvalCommentRow.props().modifiers).toInclude('topGap');
});

test('`onChangePONumber` sets the component state', () => {
  const caseInfo = {
    approvalStatus: 'approved',
  };
  const e = { target: { value: '12345' } };
  const component = shallowRender({ ...testCaseInfo, ...caseInfo });
  component.instance().onChangePONumber(e);
  expect(component).toHaveState({ poNumber: e.target.value });
});

test('`onUpdatePONumber` sets the component state', () => {
  const caseInfo = {
    approvalStatus: 'declined',
  };
  const isSubmitting = false;
  const component = shallowRender({ ...testCaseInfo, ...caseInfo });
  const initialPoNumber = '123456';

  component.instance().setState({ initialPoNumber });
  component.instance().onUpdatePONumber(isSubmitting);
  expect(component).toHaveState({ poNumber: initialPoNumber });
});

test('`setInitialPONumber` sets the component state', () => {
  const caseInfo = {
    approvalStatus: 'approved',
  };
  const value = '12345';
  const component = shallowRender({ ...testCaseInfo, ...caseInfo });
  component.instance().setInitialPONumber(value);
  expect(component).toHaveState({ initialPoNumber: value });
});
