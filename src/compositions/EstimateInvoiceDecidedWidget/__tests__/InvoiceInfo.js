import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import {
  calculatePercentage,
  colorForPercentage,
  formatPercentage,
} from '../InvoiceInfo';

// ----------------------------- calculatePercentage -----------------------------

test('calculatePercentage returns the... calculated percentage', () => {
  const estStr = '$123.45';
  const invStr = '$142.42';
  expect(calculatePercentage(estStr, invStr)).toEqual(15);

  const estStr2 = '$911.00';
  const invStr2 = '$867.53';
  expect(calculatePercentage(estStr2, invStr2)).toEqual(-4);
});

// ----------------------------- colorForPercentage -----------------------------

const danger = 'status RED!';
const success = 'gweene';
const testTheme = { colors: { status: { danger, success } } };

test('colorForPercentage returns red for a positive percentage', () => {
  expect(colorForPercentage(1, testTheme)).toEqual(danger);
});

test('colorForPercentage returns success for a negative percentage', () => {
  expect(colorForPercentage(-1, testTheme)).toEqual(success);
});

test('colorForPercentage returns "inherit" for a zero percentage', () => {
  expect(colorForPercentage(0, testTheme)).toEqual('inherit');
});

// ----------------------------- formatPercentage -----------------------------

test('formatPercentage adds a + for positive percentages', () => {
  expect(formatPercentage(13)).toEqual(' (+13%)');
});

test('formatPercentage renders negative percentages with the minus sign', () => {
  expect(formatPercentage(-7)).toEqual(' (-7%)');
});

test('formatPercentage returns null for a zero percentage', () => {
  expect(formatPercentage(0)).toEqual(null);
});
