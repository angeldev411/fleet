import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'compositions.RequestServiceButton.title',
    defaultMessage: 'Request Service',
  },
});

export default messages;
