import { QuickActionButton } from 'base-components';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import messages from './messages';

/**
 * This is a temporary component that requires additional work.
 */
// istanbul ignore next
function RequestServiceButton() {
  return (
    <QuickActionButton
      onClick={() => console.log('TODO: Build RequestServiceButton functionality')}
    >
      <QuickActionButton.Icon modifiers={['left']} name="wrench" />
      <QuickActionButton.Text>
        <FormattedMessage {...messages.title} />
      </QuickActionButton.Text>
    </QuickActionButton>
  );
}

export default RequestServiceButton;
