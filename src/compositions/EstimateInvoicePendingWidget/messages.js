import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  accept: {
    id: 'compositions.EstimateInvoicePendingWidget.data.accept',
    defaultMessage: 'Accept',
  },
  amount: {
    id: 'compositions.EstimateInvoicePendingWidget.data.amount',
    defaultMessage: 'Amount',
  },
  approve: {
    id: 'compositions.EstimateInvoicePendingWidget.data.approve',
    defaultMessage: 'Approve',
  },
  comments: {
    id: 'compositions.EstimateInvoicePendingWidget.data.comments',
    defaultMessage: 'Comments',
  },
  decline: {
    id: 'compositions.EstimateInvoicePendingWidget.data.decline',
    defaultMessage: 'Decline',
  },
  error: {
    id: 'compositions.EstimateInvoicePendingWidget.error',
    defaultMessage: 'Error',
  },
  estimates: {
    id: 'compositions.EstimateInvoicePendingWidget.data.estimates',
    defaultMessage: 'Estimates',
  },
  estimateTotal: {
    id: 'compositions.EstimateInvoicePendingWidget.data.estimateTotal',
    defaultMessage: 'Estimate Total',
  },
  etr: {
    id: 'compositions.EstimateInvoicePendingWidget.data.etr',
    defaultMessage: 'ETR',
  },
  invoice: {
    id: 'compositions.EstimateInvoicePendingWidget.data.invoice',
    defaultMessage: 'Invoice',
  },
  invoiceDate: {
    id: 'compositions.EstimateInvoicePendingWidget.data.invoiceDate',
    defaultMessage: 'Invoice Date',
  },
  invoiceId: {
    id: 'compositions.EstimateInvoicePendingWidget.data.invoiceId',
    defaultMessage: 'Invoice Id',
  },
  optional: {
    id: 'compositions.EstimateInvoicePendingWidget.data.optional',
    defaultMessage: 'Optional',
  },
  po: {
    id: 'compositions.EstimateInvoicePendingWidget.data.po',
    defaultMessage: 'Purchase Order',
  },
  provideANote: {
    id: 'compositions.EstimateInvoicePendingWidget.data.provideANote',
    defaultMessage: 'Provide a Note about your decision',
  },
  repairOrder: {
    id: 'compositions.EstimateInvoicePendingWidget.data.repairOrder',
    defaultMessage: 'Repair Order',
  },
  submissionFailed: {
    id: 'compositions.EstimateInvoicePendingWidget.submissionFailed',
    defaultMessage: 'Your submission has failed.',
  },
  submit: {
    id: 'compositions.EstimateInvoicePendingWidget.submit',
    defaultMessage: 'SUBMIT',
  },
  title: {
    id: 'compositions.EstimateInvoicePendingWidget.title',
    defaultMessage: 'Estimate/Invoice',
  },
  viewEstimate: {
    id: 'compositions.EstimateInvoicePendingWidget.data.viewEstimate',
    defaultMessage: 'View Estimate',
  },
  viewLatestEstimate: {
    id: 'compositions.EstimateInvoicePendingWidget.data.viewLatestEstimate',
    defaultMessage: 'View Latest Estimate',
  },
});

export default formattedMessages;
