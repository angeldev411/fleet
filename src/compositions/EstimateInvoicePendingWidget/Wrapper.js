import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';
import PropTypes from 'prop-types';
import styled from 'styled-components';

/* istanbul ignore next */
const styles = props => `
  font-size: ${props.theme.dimensions.fontSizeNormal};

  .estimate-label {
    background-color: ${props.theme.colors.base.chrome100};
    flex: 1;
    font-weight: 700;
    padding: ${props.theme.dimensions.fontSizeNormal} ${px2rem(10)};
  }

  .estimate-value {
    background-color: ${props.theme.colors.base.chrome200};
    margin: 0 ${px2rem(15)} 0 ${px2rem(2)};
    padding: ${props.theme.dimensions.fontSizeNormal} 0;
    text-align: center;
    width: ${px2rem(135)};
  }

  .Dropdown-control {
    height: 37px;
  }

  .error {
    color: ${props.theme.colors.status.danger};
    margin-right: 15px;
  }
  .bold {
    font-weight: bold;
  }
  .fa-ban {
    margin-right: 10px;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
      chrome200: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  dimensions: PropTypes.shape({
    fontSizeNormal: PropTypes.string.isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'PendingEstimateInvoiceWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
