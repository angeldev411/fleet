import React from 'react';
import {
  test,
  expect,
  shallow,
  mount,
  MountableTestComponent,
  createSpy,
} from '__tests__/helpers/test-setup';
import { FormattedMessage } from 'react-intl';
import { theme as defaultTheme } from 'decisiv-ui-utils';
import { noop } from 'lodash';

import { formatDate } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';

import { EstimateInvoicePendingWidget, getApprovalStatus, getOkLabel } from '../index';
import messages from '../messages';

const roUrl = 'http://example.com/foobar.pdf';
const intlMock = {
  formatMessage: ({ defaultMessage }) => defaultMessage,
};

const testProps = {
  caseInfo: {
    id: '100',
    version: '1',
    approvalStatus: 'Pending',
    estimateTotal: '$1232.00',
    invoiceId: '456',
    invoiceTotal: '$1500.00',
    roNumber: '987',
    poNumber: '654321',
    roUrl,
  },
  intl: intlMock,
  updateCase: noop,
};

function shallowRender(props = testProps) {
  return shallow(<EstimateInvoicePendingWidget {...props} />);
}

test('EstimateInvoicePendingWidget renders Pending component if approvalStatus is `Pending`', () => {
  const caseInfo = { ...testProps.caseInfo, approvalStatus: 'Pending' };
  const component = shallowRender({ ...testProps, caseInfo });
  expect(component).toContain('Widget');
});

test('EstimateInvoicePendingWidget renders nothing if approvalStatus is NOT `Pending`', () => {
  const caseInfo = { ...testProps.caseInfo, approvalStatus: 'Approved' };
  const component = shallowRender({ ...testProps, caseInfo });
  expect(component.children().length).toEqual(0);
});

test('Pending renders a PendingEstimateInvoiceWrapper', () => {
  const component = shallowRender();
  expect(component).toBeA('PendingEstimateInvoiceWrapper');
});

test('Pending renders a WidgetHeader with title', () => {
  const component = shallowRender();
  const widgetHeader = component.find('Header').first();

  expect(widgetHeader.contains(<FormattedMessage {...messages.title} />)).toEqual(true);
});

test('Pending renders the correct approval status', () => {
  const caseInfo = {
    approvalStatus: 'Pending',
  };
  const props = {
    ...testProps,
    caseInfo,
  };
  const component = mount(
    <MountableTestComponent>
      <EstimateInvoicePendingWidget {...props} />
    </MountableTestComponent>,
  );
  const subHeader = component.find('SubHeader').render();
  const subHeaderText = subHeader.text();
  expect(subHeaderText).toInclude('Pending');
});

test('Pending renders `View Estimate`', () => {
  const component = shallowRender();
  const lastRow = component.find('Item').last();
  expect(lastRow).toContain('PdfLink');
  const pdfLink = lastRow.find('PdfLink');
  expect(pdfLink).toHaveProp('url', roUrl);
  expect(pdfLink.props().message).toEqual(messages.viewEstimate);
});

test('Pending renders etr when it is provided', () => {
  const etr = '2016-09-28T08:19:06Z';
  const caseInfo = { ...testProps.caseInfo, etr };
  const component = shallowRender({ ...testProps, caseInfo });
  const labelValue = component.find('LabelValue').first();
  expect(labelValue.props().value).toEqual(formatDate(etr));
});

test('Pending renders etr placeholder when it is NOT provided', () => {
  const etr = null;
  const caseInfo = { ...testProps.caseInfo, etr };
  const component = shallowRender({ ...testProps, caseInfo });
  const labelValue = component.find('LabelValue').first();
  expect(labelValue.props().label).toEqual(<FormattedMessage {...messages.etr} />);
  expect(labelValue.props().value).toEqual(getOutputText(etr));
});

test('When `onChangeApproveOption` is called, `deferredApproveValue` changes to the given value', () => {
  const component = shallowRender();
  const value = 'Approve';
  component.instance().onChangeApproveOption({ value });
  expect(component.instance().deferredApproveValue).toEqual(value);
});

test('`getApprovalStatus returns status correctly', () => {
  expect(getApprovalStatus('Approve')).toEqual('approved');
  expect(getApprovalStatus('Decline')).toEqual('declined');
  expect(getApprovalStatus('')).toEqual('');
});

test('`getOkLabel` returns label for ok button', () => {
  expect(getOkLabel('Approve', intlMock.formatMessage)).toEqual('Accept');
  expect(getOkLabel('Decline', intlMock.formatMessage)).toEqual('Decline');
  expect(getOkLabel('', intlMock.formatMessage)).toEqual('');
});

test('`updateCase` is called with correct params when submit button is clicked', () => {
  const updateCase = createSpy();
  const component = shallowRender({ ...testProps, updateCase });
  const note = 'note';
  const poNumber = 'poNumber';

  component.instance().deferredApproveValue = 'Approve';
  component.setState({
    note,
    poNumber,
    selectedApproveValue: 'approve',
  });

  component.find('QuickActionButton').simulate('click');
  const params = {
    caseId: '100',
    versionId: '1',
    approvalStatus: 'approved',
    comments: note,
    poNumber,
  };
  expect(updateCase).toHaveBeenCalledWith(params);
});

test('`updateCase` is called with initial poNumber when input field untouched', () => {
  const updateCase = createSpy();
  const component = shallowRender({ ...testProps, updateCase });
  const poNumber = '654321';

  component.instance().deferredApproveValue = 'Approve';
  component.setState({
    selectedApproveValue: 'approve',
    poNumber,
  });

  component.find('QuickActionButton').simulate('click');
  const params = {
    caseId: '100',
    versionId: '1',
    approvalStatus: 'approved',
    comments: '',
    poNumber,
  };
  expect(updateCase).toHaveBeenCalledWith(params);
});

test('Pending renders an error when casesError is NOT empty', () => {
  const casesError = { message: 'error' };
  const component = shallowRender({ ...testProps, casesError });
  expect(component).toContain('.error');
});

test('`handleOk` sets the component state', () => {
  const deferredApproveValue = 'Approve';
  const note = 'note';
  const component = shallowRender();
  component.instance().deferredApproveValue = deferredApproveValue;
  component.instance().handleOk(note);
  expect(component).toHaveState({
    selectedApproveValue: deferredApproveValue,
    dialogVisible: false,
    note,
  });
});

test('`handleCancel` sets the component state', () => {
  const component = shallowRender();
  component.instance().handleCancel();
  expect(component).toHaveState({
    selectedApproveValue: undefined,
    dialogVisible: false,
    note: '',
  });
});

test('`onChangePONumber` sets the component state', () => {
  const e = { target: { value: 'number' } };
  const component = shallowRender();
  component.instance().onChangePONumber(e);
  expect(component).toHaveState({ poNumber: e.target.value });
});

test('`getDropdownColors` returns colors correctly based on theme props', () => {
  const component = shallowRender({ ...testProps });
  const instance = component.instance();
  expect(instance.getDropdownColors('Approve')).toEqual({
    fgColor: defaultTheme.colors.base.chrome100,
    bgColor: defaultTheme.colors.brand.primary,
  });
  expect(instance.getDropdownColors('Decline')).toEqual({
    fgColor: defaultTheme.colors.base.chrome100,
    bgColor: defaultTheme.colors.base.chrome600,
  });
  expect(instance.getDropdownColors('')).toEqual({
    fgColor: undefined,
    bgColor: undefined,
  });
});

test('handleSubmit should NOT work when button is disabled.', () => {
  const props = {
    ...testProps,
    updateCase: createSpy(),
  };
  const component = shallowRender(props);
  component.setState({
    selectedApproveValue: null,
  });
  component.instance().handleSubmit();
  expect(props.updateCase).toNotHaveBeenCalled();
});

test('handleSubmit should work when button is enabled.', () => {
  const props = {
    ...testProps,
    updateCase: createSpy(),
  };
  const component = shallowRender(props);
  component.setState({
    selectedApproveValue: 'Approved',
    poNumber: null,
  });
  component.instance().handleSubmit();
  expect(props.updateCase).toHaveBeenCalled();
});
