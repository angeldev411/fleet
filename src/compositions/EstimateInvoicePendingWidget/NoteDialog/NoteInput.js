import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  border-radius: 2px;
  border: none;
  box-shadow: 0 0 2px 0 ${props.theme.colors.base.shadow};
  box-sizing: border-box;
  color: ${props.theme.colors.base.text};
  font-weight: 500;
  height: ${px2rem(92)};
  line-height: ${px2rem(19)};
  margin-top: ${px2rem(6)};
  outline: none;
  overflow-y: auto;
  padding: ${px2rem(7.8)} ${px2rem(14)} ${px2rem(9.8)} ${px2rem(11)};
  resize: none;
  width: 100%;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      shadow: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'NoteInput',
  styled.textarea,
  styles,
  { themePropTypes },
);
