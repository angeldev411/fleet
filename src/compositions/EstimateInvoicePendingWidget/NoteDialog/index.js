import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import { FormattedMessage } from 'react-intl';

import { ButtonLink } from 'base-components';

import Wrapper from './Wrapper';
import DialogTitle from './DialogTitle';
import DialogHelp from './DialogHelp';
import NoteInput from './NoteInput';
import NoteSubmitLinks from './NoteSubmitLinks';

import messages from '../messages';

class NoteDialog extends Component {
  static propTypes = {
    title: PropTypes.string,
    note: PropTypes.string,
    okLabel: PropTypes.string,
    cancelLabel: PropTypes.string,
    onOk: PropTypes.func,
    onCancel: PropTypes.func,
  };

  static defaultProps = {
    title: 'Approve',
    note: '',
    okLabel: 'OK',
    cancelLabel: 'Cancel',
    onOk: noop,
    onCancel: noop,
  };

  componentWillMount() {
    this.setState({
      note: this.props.note,
    });
  }

  componentDidMount() {
    setTimeout(this.setFocus, 100);
  }

  setFocus = () => {
    this.noteTextArea.focus();
  }

  handleNoteChange = (ev) => {
    this.setState({
      note: ev.target.value,
    });
  }

  render() {
    const { title, okLabel, cancelLabel, onOk, onCancel } = this.props;
    const { note } = this.state;
    return (
      <Wrapper>
        <DialogTitle>{title}</DialogTitle>
        <DialogHelp>
          <strong>
            <FormattedMessage {...messages.optional} />:&nbsp;
          </strong>
          <span>
            <FormattedMessage {...messages.provideANote} />:
          </span>
        </DialogHelp>
        <NoteInput
          value={note}
          onChange={this.handleNoteChange}
          innerRef={(input) => { this.noteTextArea = input; }}
        />
        <NoteSubmitLinks>
          <ButtonLink onClick={onCancel}>
            {cancelLabel}
          </ButtonLink>
          <ButtonLink onClick={() => onOk(note)}>
            {okLabel}
          </ButtonLink>
        </NoteSubmitLinks>
      </Wrapper>
    );
  }
}

export default NoteDialog;
