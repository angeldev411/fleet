import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const NoteSubmitLinks = styled.div`
  margin-top: ${px2rem(12)};
  display: flex;
  justify-content: space-between;
`;

export default NoteSubmitLinks;
