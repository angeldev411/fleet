import React from 'react';
import { test, expect, shallow, createSpy, spyOn } from '__tests__/helpers/test-setup';

import NoteDialog from '../index';

const testProps = {
  title: 'title',
  note: 'note',
};

function renderWidget(props = testProps) {
  return shallow(<NoteDialog {...props} />);
}

test('NoteDialog sets default state based on props', () => {
  const component = renderWidget();
  expect(component).toHaveState({ note: testProps.note });
});

test('NoteDialog renders props `title`', () => {
  const props = { title: 'title' };
  const component = renderWidget(props);
  const titleWrapper = component.find('DialogTitle').render();
  expect(titleWrapper.text()).toContain(props.title);
});

test('NoteDialog renders props `okLabel`, `okCancel`', () => {
  const props = { okLabel: 'Approve', cancelLabel: 'Cancel' };
  const component = renderWidget(props);
  const buttonBox = component.find('NoteSubmitLinks');
  const cancelButton = buttonBox.find('ButtonLink').first().render();
  const okButton = buttonBox.find('ButtonLink').last().render();
  expect(cancelButton.text()).toContain(props.cancelLabel);
  expect(okButton.text()).toContain(props.okLabel);
});

test('`onOk` is called with note params when `Approve/Decline` button is clicked', () => {
  const onOk = createSpy();
  const note = 'note';
  const component = renderWidget({ onOk });
  component.setState({ note });
  const buttonBox = component.find('NoteSubmitLinks');
  const okButton = buttonBox.find('ButtonLink').last();
  okButton.simulate('click');
  expect(onOk).toHaveBeenCalledWith(note);
});

test('state is changed properly as note changes', () => {
  const component = renderWidget();
  const ev = {
    target: {
      value: 'note',
    },
  };
  const noteInput = component.find('NoteInput');
  noteInput.simulate('change', ev);
  expect(component).toHaveState({ note: ev.target.value });
});

test.cb('setFocus has been called after component is mounted.', (t) => {
  t.plan(1);
  const instance = renderWidget().instance();
  const setFocus = spyOn(instance, 'setFocus');
  instance.componentDidMount();
  setTimeout(() => {
    expect(setFocus).toHaveBeenCalled();
    t.pass();
    t.end();
  }, 500);
});

test('noteTextArea is focused when setFocus is called', () => {
  const component = renderWidget();
  const instance = component.instance();
  instance.noteTextArea = {
    focus: createSpy(),
  };
  instance.setFocus();
  expect(instance.noteTextArea.focus).toHaveBeenCalled();
});

test('set noteTextArea by innerRef', () => {
  const component = renderWidget();
  const noteInput = component.find('NoteInput');
  const input = 'TestInput';
  noteInput.props().innerRef(input);
  expect(component.instance().noteTextArea).toEqual(input);
});
