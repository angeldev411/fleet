import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.chrome500};
  font-style: italic;
  font-weight: 300;
  line-height: ${px2rem(15)};
  margin-top: ${px2rem(2)};
  strong {
    font-weight: 600;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome500: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'DialogHelp',
  styled.div,
  styles,
  { themePropTypes },
);
