import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose, setDisplayName } from 'recompose';
import { find, compact, isEmpty } from 'lodash';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage, injectIntl } from 'react-intl';
import { withTheme } from 'styled-components';
import { theme as defaultTheme } from 'decisiv-ui-utils';
import { ButtonLink, QuickActionButton } from 'base-components';

import cardType from 'utils/cardType';
import PdfLink from 'components/PdfLink';

import Dialog from 'elements/Dialog';
import Divider from 'elements/Divider';
import DropDown from 'elements/DropDown';
import {
  SplitBlock,
  SplitBlockPart,
  SplitBlockElement,
} from 'elements/SplitBlock';
import Widget from 'elements/Widget';

import { formatDate } from 'utils/timeUtils';
import { getOutputText } from 'utils/widget';
import LabelValue from 'components/LabelValue';

import DropdownWrapper from './DropdownWrapper';
import Wrapper from './Wrapper';
import NoteDialog from './NoteDialog';
import messages from './messages';

import withConnectedData from './Container';

export function getApprovalStatus(approveValue) {
  switch (approveValue) {
    case 'Approve':
      return 'approved';
    case 'Decline':
      return 'declined';
    default:
      return '';
  }
}

export function getOkLabel(title, formatMessage) {
  switch (title) {
    case 'Approve':
      return formatMessage(messages.accept);
    case 'Decline':
      return formatMessage(messages.decline);
    default:
      return title;
  }
}

export class EstimateInvoicePendingWidget extends Component {
  static propTypes = {
    caseInfo: PropTypes.shape({
      approvalStatus: PropTypes.string,
      estimateTotal: PropTypes.string,
      invoiceId: PropTypes.string,
      invoiceTotal: PropTypes.string,
      roNumber: PropTypes.string,
      poNumber: PropTypes.string,
    }).isRequired,
    casesError: PropTypes.shape(),
    intl: PropTypes.shape({
      formatMessage: PropTypes.func,
    }).isRequired,
    theme: PropTypes.shape({
      colors: PropTypes.shape({
        base: PropTypes.shape({
          chrome100: PropTypes.string.isRequired,
          chrome200: PropTypes.string.isRequired,
          chrome600: PropTypes.string.isRequired,
        }).isRequired,
        brand: PropTypes.shape({
          primary: PropTypes.string.isRequired,
        }).isRequired,
      }).isRequired,
    }).isRequired,
    updateCase: PropTypes.func.isRequired,
  };

  static defaultProps = {
    cardType: '',
    caseInfo: {},
    casesError: {},
    casesRequesting: false,
    theme: defaultTheme,
  };

  state = {
    selectedApproveValue: undefined,
    dialogVisible: false,
    note: '',
    poNumber: null,
  };

  onChangeApproveOption = ({ value }) => {
    this.deferredApproveValue = value;
    this.setState({
      dialogVisible: true,
    });
  }

  onChangePONumber = (ev) => {
    this.setState({
      poNumber: ev.target.value,
    });
  }

  getApproveOptions = () => {
    const { intl: { formatMessage } } = this.props;
    return [
      {
        value: 'Approve',
        label: (
          <ButtonLink>
            {formatMessage(messages.approve)}
          </ButtonLink>
        ),
      },
      {
        value: 'Decline',
        label: (
          <ButtonLink>
            {formatMessage(messages.decline)}
          </ButtonLink>
        ),
      },
    ];
  }

  getDropdownColors = (selectedApproveValue) => {
    const { theme } = this.props;
    if (selectedApproveValue === 'Approve') {
      return {
        fgColor: theme.colors.base.chrome100,
        bgColor: theme.colors.brand.primary,
      };
    } else if (selectedApproveValue === 'Decline') {
      return {
        fgColor: theme.colors.base.chrome100,
        bgColor: theme.colors.base.chrome600,
      };
    }

    return {
      fgColor: undefined,
      bgColor: undefined,
    };
  }

  handleOk = (note) => {
    this.setState({
      selectedApproveValue: this.deferredApproveValue,
      dialogVisible: false,
      note,
    });
  }

  handleCancel = () => {
    this.setState({
      dialogVisible: false,
    });
  }

  handleSubmit = () => {
    const { caseInfo, updateCase } = this.props;
    const { poNumber, note, selectedApproveValue } = this.state;

    if (!selectedApproveValue) {
      return;
    }
    const approvalStatus = getApprovalStatus(this.deferredApproveValue);

    updateCase({
      caseId: caseInfo.id,
      versionId: caseInfo.version,
      approvalStatus,
      comments: note,
      poNumber: poNumber === null ? caseInfo.poNumber : poNumber,
    });
  }

  render() {
    const {
      casesError,
      caseInfo,
      theme,
      intl: { formatMessage },
    } = this.props;
    const {
      selectedApproveValue,
      dialogVisible,
      note,
    } = this.state;

    const {
      approvalStatus,
      etr,
      roUrl,
      roNumber,
      estimateTotal,
      poNumber,
    } = caseInfo;

    const formattedEtr = etr ? formatDate(etr) : getOutputText(etr);
    const approveOptions = this.getApproveOptions();
    const selectedApproveOption = find(
      approveOptions, { value: selectedApproveValue },
    );
    const dropdownColors = this.getDropdownColors(selectedApproveValue);
    const submitDisabled = !selectedApproveValue && 'disabled';
    const statusCardType = cardType(approvalStatus);

    return (
      approvalStatus === 'Pending' &&
      <Wrapper>
        <Widget
          id="estimate-invoice"
          className="estimate-invoice-pending"
          expandKey="estimate-invoice"
        >
          <Widget.Header>
            <FormattedMessage {...messages.title} />
          </Widget.Header>

          <SplitBlock modifiers={['pad']}>
            <SplitBlock modifiers={['pad']}>
              <Widget.SubHeader modifiers={compact([statusCardType])}>
                {approvalStatus}
              </Widget.SubHeader>
            </SplitBlock>

            <SplitBlockPart modifiers={['right']}>
              <LabelValue
                label={<FormattedMessage {...messages.etr} />}
                value={formattedEtr}
                marginRight={80}
              />
              <LabelValue
                label={<FormattedMessage {...messages.repairOrder} />}
                value={
                  <span>
                    {roNumber && '#'}
                    {getOutputText(roNumber)}
                  </span>
                }
              />
            </SplitBlockPart>
          </SplitBlock>

          <Divider color={theme.colors.base.chrome200} marginBottom={15} />

          <SplitBlock modifiers={['pad']}>
            <SplitBlock modifiers={['pad']}>
              <div className="estimate-label">
                <FormattedMessage {...messages.estimateTotal} />
              </div>
              <div className="estimate-value">
                {estimateTotal}
              </div>
            </SplitBlock>
            <DropdownWrapper className="pending-estimate-options">
              <DropDown
                onChange={this.onChangeApproveOption}
                options={approveOptions}
                value={selectedApproveOption}
                widgetBGColor={dropdownColors.bgColor}
                widgetFGColor={dropdownColors.fgColor}
                showIcon={!!note}
              />
              {dialogVisible &&
              <Dialog>
                <NoteDialog
                  title={this.deferredApproveValue}
                  note={note}
                  theme={theme}
                  okLabel={getOkLabel(this.deferredApproveValue, formatMessage)}
                  onOk={this.handleOk}
                  onCancel={this.handleCancel}
                />
              </Dialog>
              }
            </DropdownWrapper>
          </SplitBlock>

          <Widget.Item>
            <SplitBlock modifiers={['pad']}>
              <LabelValue
                label={<FormattedMessage {...messages.po} />}
                value={<input
                  placeholder="Optional: PO Number"
                  style={{ width: '150px', height: '25px', paddingLeft: '10px' }}
                  defaultValue={poNumber}
                  onChange={this.onChangePONumber}
                />}
              />
            </SplitBlock>
          </Widget.Item>

          <Divider color={theme.colors.base.chrome200} marginTop={15} />

          <Widget.Item>
            <SplitBlock modifiers={['pad']}>
              <SplitBlockPart modifiers={['left']}>
                <PdfLink url={roUrl} message={messages.viewEstimate} />
              </SplitBlockPart>
              <SplitBlockPart modifiers={['right']}>
                <SplitBlockElement modifiers={['padLeft']}>
                  {!isEmpty(casesError) &&
                    <span className="error">
                      <span className="bold">
                        <FontAwesome name="ban" />
                        <FormattedMessage {...messages.error} />:&nbsp;
                      </span>
                    </span>
                  }
                </SplitBlockElement>
                <QuickActionButton
                  modifiers={compact([
                    submitDisabled,
                    'secondary',
                    'hoverSuccess',
                  ])}
                  onClick={this.handleSubmit}
                >
                  <QuickActionButton.Text>
                    <FormattedMessage {...messages.submit} />
                  </QuickActionButton.Text>
                </QuickActionButton>
              </SplitBlockPart>
            </SplitBlock>
          </Widget.Item>
        </Widget>
      </Wrapper>
    );
  }
}


export default compose(
  setDisplayName('EstimateInvoicePendingWidget'),
  withConnectedData,
  withTheme,
  injectIntl,
)(EstimateInvoicePendingWidget);
