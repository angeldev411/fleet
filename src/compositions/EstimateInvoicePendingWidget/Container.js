import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';

import {
  currentCaseSelector,
  casesErrorSelector,
  casesRequestingSelector,
} from 'redux/cases/selectors';
import {
  updateCaseRequest,
} from 'redux/cases/actions';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    caseInfo: PropTypes.shape({
      approvalStatus: PropTypes.string,
    }).isRequired,
  };

  Container.defaultProps = {
    caseInfo: {
      approvalStatus: '',
    },
  };

  function mapStateToProps(state) {
    return {
      caseInfo: currentCaseSelector(state),
      casesError: casesErrorSelector(state),
      casesRequesting: casesRequestingSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      updateCase: payload => dispatch(updateCaseRequest(payload)),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
