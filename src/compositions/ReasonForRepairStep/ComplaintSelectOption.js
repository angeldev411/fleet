import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = ({ theme: { colors } }) => `
  border-radius: ${px2rem(2)};
  padding: ${px2rem(2)} 0 ${px2rem(1)} ${px2rem(10)};
  color: ${colors.base.text};
  font-size: ${px2rem(12)};

  &:hover {
    background-color: ${colors.status.info};
    color: ${colors.base.chrome000};
    text-decoration: underline;
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome000: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      info: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'ComplaintSelectOption',
  styled.div,
  styles,
  { themePropTypes },
);
