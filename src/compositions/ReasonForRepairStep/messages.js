import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  title: {
    id: 'compositions.ReasonForRepairStep.title',
    defaultMessage: 'What\'s the complaint?',
  },
  subtitle: {
    id: 'compositions.ReasonForRepairStep.subtitle',
    defaultMessage: 'You can select a complaint code or enter a description. When possible, fill in both. That helps the service provider expedite your case.',
  },
  complaintCode: {
    id: 'compositions.ReasonForRepairStep.label.complaintCode',
    defaultMessage: 'Complaint Code',
  },
  description: {
    id: 'compositions.ReasonForRepairStep.label.description',
    defaultMessage: 'Description',
  },
  continueButton: {
    id: 'compositions.ReasonForRepairStep.button.continue',
    defaultMessage: 'Continue',
  },
  charactersLeft: {
    id: 'compositions.ReasonForRepairStep.label.charactersLeft',
    defaultMessage: '{value} {value, plural, one {Character} other {Characters}} left',
  },
  characterCountWarning: {
    id: 'compositions.ReasonForRepairStep.label.characterCountWarning',
    defaultMessage: 'You reached the max character count.',
  },
});

export default formattedMessages;
