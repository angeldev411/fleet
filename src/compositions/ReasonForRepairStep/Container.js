import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, setDisplayName } from 'recompose';

import {
  loadDataLists,
} from 'redux/dataLists/actions';
import {
  complaintsSelector,
} from 'redux/dataLists/selectors';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    complaints: PropTypes.arrayOf(
      PropTypes.shape({
        code: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
      }),
    ),
    loadDataLists: PropTypes.func.isRequired,
  };

  Container.defaultProps = {
    complaints: [],
  };

  function mapStateToProps(state) {
    return {
      complaints: complaintsSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      loadDataLists: () => dispatch(loadDataLists()),
    };
  }

  return compose(
    setDisplayName('ReasonForRepairStepContainer'),
    connect(mapStateToProps, mapDispatchToProps),
    immutableToJS,
  )(Container);
};

export default withConnectedData;
