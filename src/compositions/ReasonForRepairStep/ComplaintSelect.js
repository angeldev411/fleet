import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

const height = px2rem(25);

/* istanbul ignore next */
const styles = ({ theme: { colors } }) => `
  position: relative;
  padding-left: 0;

  .Select--multi {
    &:focus {
      outline: none;
    }
    .Select-control {
      font-size: ${px2rem(14)};
      height: ${height};
      border: none;
    }
    .Select-multi-value-wrapper {
    }
    .Select-placeholder {
      font-size: ${px2rem(12)};
      font-weight: 300;
      line-height: ${px2rem(23)};
      padding-left: 0;
      top: initial;
      color: ${colors.base.chrome300};
    }
    .Select-input {
      height: ${height};
      margin-left: 0;
      input {
        padding: ${px2rem(5)} 0;
      }
    }
    .Select-value {
      border: 1px solid ${colors.base.chrome300};
      background-color: ${colors.base.chrome100};
      color: ${colors.base.text};
      height: ${px2rem(18)};
      margin-left: 0;
      margin-top: ${px2rem(4)};
      pointer-events: none;

      .Select-value-label {
        font-size: ${px2rem(10)};
        padding-right: ${px2rem(2)};
        vertical-align: top;
      }
      .Select-value-icon {
        border: none;
        float: right;
        font-size: ${px2rem(20)};
        height: ${px2rem(15)};
        line-height: ${px2rem(15)};
        color: ${colors.base.chrome500};
        pointer-events: auto;
      }

      &:hover {
        background-color: ${colors.status.danger};
        color: ${colors.base.chrome000};
        cursor: pointer;
        .Select-value-label {
          text-decoration: underline;
          cursor: inherit;
        }
        .Select-value-icon {
          background-color: inherit;
          color: ${colors.base.chrome000};
        }
      }
    }
    
    .Select-option {
      padding: ${px2rem(4)};
      font-size: ${px2rem(12)};
      color: ${colors.base.text};

      &:hover {
        background-color: inherit;
      }
    }
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome000: PropTypes.string.isRequired,
      chrome100: PropTypes.string.isRequired,
      chrome300: PropTypes.string.isRequired,
      chrome400: PropTypes.string.isRequired,
      chrome500: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'ComplaintSelect',
  styled.div,
  styles,
  { themePropTypes },
);
