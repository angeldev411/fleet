import { compact, delay } from 'lodash';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Container,
  Row,
  Column,
} from 'styled-components-reactive-grid';
import { H3, P, QuickActionButton } from 'base-components';
import { px2rem } from 'decisiv-ui-utils';

import WizardInput from 'components/WizardInput';
import WarningLabel from 'components/WarningLabel';
import Select from 'elements/Select';
import TextareaAutosize from 'elements/TextareaAutosize';

import ComplaintSelect from './ComplaintSelect';
import ComplaintSelectOption from './ComplaintSelectOption';
import withConnectedData from './Container';
import messages from './messages';

export const MAX_DESCRIPTION_LENGTH = 6000;

export function makeSelectOptions(complaints) {
  return complaints && complaints.map(complaint => ({
    code: complaint.code,
    description: complaint.description,
    label: `${complaint.code} - ${complaint.description}`,
  }));
}

export class ReasonForRepairStep extends Component {
  static propTypes = {
    complaints: PropTypes.arrayOf(
      PropTypes.shape({
        code: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
      }),
    ).isRequired,
    loadDataLists: PropTypes.func.isRequired,
    submitInputData: PropTypes.func.isRequired,
  };

  state = {
    charactersLeft: MAX_DESCRIPTION_LENGTH,
    charactersLeftVisible: false,
    description: '',
    selectedOption: undefined,
    selectOptions: makeSelectOptions(this.props.complaints),
  };

  componentDidMount() {
    this.props.loadDataLists();
  }

  componentWillReceiveProps(nextProps) {
    // it is fine to do it just the first time, because the list of complaints won't change often
    if (this.props.complaints.length === 0 && nextProps.complaints.length > 0) {
      this.setState({ selectOptions: makeSelectOptions(nextProps.complaints) });
    }
  }

  handleClickContinue = () => {
    const { submitInputData } = this.props;
    const { selectedOption = {}, description } = this.state;
    if (selectedOption.code || description) {
      const reasonForRepair = {
        description,
        complaintCode: selectedOption.code || '',
        complaintDescription: selectedOption.description || '',
      };
      submitInputData(reasonForRepair);
    }
  }

  handleSelectChange = (options) => {
    const selectedOption = options[options.length - 1];
    this.setState({ selectedOption });
  }

  handleTextChange = (event) => {
    const { target: { value } } = event;

    const charactersLeft = MAX_DESCRIPTION_LENGTH - value.length;
    if (charactersLeft >= 0) {
      this.setState({
        charactersLeft,
        description: value,
      });
    }

    clearTimeout(this.charactersLeftTimer);
    this.setState({ charactersLeftVisible: true });
    this.charactersLeftTimer = delay(() => this.setState({ charactersLeftVisible: false }), 1000);
  }

  renderSelectOption = option =>
    <ComplaintSelectOption>{option.label}</ComplaintSelectOption>;

  render() {
    const {
      selectedOption,
      selectOptions,
      description,
      charactersLeft,
      charactersLeftVisible,
    } = this.state;

    const continueButtonDisabled = !selectedOption && !description;
    const noCharactersLeft = charactersLeft <= 0;

    return (
      <Container style={{ padding: `${px2rem(30)} 0` }}>
        <Row>
          <Column modifiers={['col_offset_3', 'col_6']}>
            <Row>
              <Column modifiers={['col']}>
                <H3 modifiers={['fontWeightRegular']}>
                  <FormattedMessage {...messages.title} />
                </H3>
              </Column>
            </Row>
            <Row>
              <Column modifiers={['col']}>
                <P>
                  <FormattedMessage {...messages.subtitle} />
                </P>
              </Column>
            </Row>
            <Row style={{ marginTop: `${px2rem(30)}` }}>
              <Column modifiers={['col']}>
                <WizardInput label={messages.complaintCode}>
                  <ComplaintSelect>
                    <Select
                      autoBlur
                      multi
                      onChange={this.handleSelectChange}
                      optionRenderer={this.renderSelectOption}
                      options={selectOptions}
                      placeholder="Select Item..."
                      value={selectedOption}
                    />
                  </ComplaintSelect>
                </WizardInput>
                <WizardInput
                  label={messages.description}
                  rightLabel={messages.charactersLeft}
                  rightLabelValue={charactersLeft}
                  modifiers={compact([
                    noCharactersLeft && 'warning',
                    charactersLeftVisible && 'rightLabelVisible',
                    'flexibleHeight',
                    'topOverlap',
                  ])}
                >
                  <TextareaAutosize
                    placeholder="Enter Description..."
                    onChange={this.handleTextChange}
                    value={description}
                  />
                </WizardInput>
              </Column>
            </Row>
            {noCharactersLeft &&
              <Row>
                <Column>
                  <WarningLabel label={messages.characterCountWarning} />
                </Column>
              </Row>
            }
            <Row style={{ marginTop: `${px2rem(30)}` }}>
              <Column>
                <QuickActionButton
                  modifiers={compact([
                    continueButtonDisabled && 'disabled',
                    'secondary',
                  ])}
                  onClick={this.handleClickContinue}
                >
                  <QuickActionButton.Text>
                    <FormattedMessage {...messages.continueButton} />
                  </QuickActionButton.Text>
                </QuickActionButton>
              </Column>
            </Row>
          </Column>
        </Row>
      </Container>
    );
  }
}

export default withConnectedData(ReasonForRepairStep);
