import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
  spyOn,
} from '__tests__/helpers/test-setup';

import { ReasonForRepairStep, makeSelectOptions, MAX_DESCRIPTION_LENGTH } from '../index';
import messages from '../messages';

const defaultProps = {
  complaints: [
    {
      code: '190',
      description: 'ABS, Light Stays On',
    },
    {
      code: '191',
      description: 'ABS, System Cycles Constantly',
    },
  ],
  loadDataLists: noop,
  submitInputData: noop,
};

const selectOptions = [
  {
    code: '190',
    description: 'ABS, Light Stays On',
    label: '190 - ABS, Light Stays On',
  },
  {
    code: '191',
    description: 'ABS, System Cycles Constantly',
    label: '191 - ABS, System Cycles Constantly',
  },
];

function shallowRender(props = defaultProps) {
  return shallow(<ReasonForRepairStep {...props} />);
}

test('sets `charactersLeft` state to MAX_DESCRIPTION_LENGTH initially (6000 chars)', () => {
  const component = shallowRender();
  expect(component.state('charactersLeft')).toEqual(MAX_DESCRIPTION_LENGTH);
});

test('hides the character remaining label by default', () => {
  const component = shallowRender();
  expect(component.state('charactersLeftVisible')).toBe(false);
});

test('sets `selectOptions` state after component is initialized', () => {
  const component = shallowRender();
  expect(component.state('selectOptions')).toEqual(
    makeSelectOptions(defaultProps.complaints),
  );
});

test('makeSelectOptions function returns the correctly formatted options', () => {
  const options = makeSelectOptions(defaultProps.complaints);
  expect(options).toEqual(selectOptions);
});

test('loads data lists on component mount', () => {
  const loadDataLists = createSpy();
  const component = shallowRender({ ...defaultProps, loadDataLists });
  const instance = component.instance();
  instance.componentDidMount();
  expect(loadDataLists).toHaveBeenCalled();
});

test('sets `selectOptions` state on receiving the complaints prop first time', () => {
  const component = shallowRender({ ...defaultProps, complaints: [] });
  const instance = component.instance();
  component.setState({ selectOptions: undefined });

  instance.componentWillReceiveProps(defaultProps);
  expect(component.state('selectOptions')).toEqual(
    makeSelectOptions(defaultProps.complaints),
  );
});

// --------------------------- render ---------------------------
test('renders the title of the step', () => {
  const component = shallowRender();
  expect(component).toContain('H3');
  expect(component.find('H3 FormattedMessage')).toHaveProps({ ...messages.title });
});

test('renders the subtitle of the step', () => {
  const component = shallowRender();
  expect(component).toContain('P');
  expect(component.find('P FormattedMessage')).toHaveProps({ ...messages.subtitle });
});

// Complaint Select
test('renders WizardInput that wraps the Select component and has correct label', () => {
  const component = shallowRender();
  expect(component).toContain('WizardInput');

  const wizardInput = component.find('WizardInput').at(0);
  expect(wizardInput).toContain('Select');
  expect(wizardInput).toHaveProps({ label: messages.complaintCode });
});

test('renders the Complaint Select with expected props', () => {
  const component = shallowRender();
  component.setState({ selectedOption: selectOptions[0] });

  const select = component.find('Select');
  expect(select).toHaveProps({
    onChange: component.handleSelectChange,
    optionRenderer: component.renderSelectOption,
    value: selectOptions[0],
  });
  expect(select.props().options).toEqual(selectOptions);
});

test('calling handleSelectChange sets the currently selected option in state', () => {
  const component = shallowRender();
  const instance = component.instance();
  instance.handleSelectChange([selectOptions[0]]);
  expect(component.state('selectedOption')).toEqual(selectOptions[0]);
});

test('renderSelectOption correctly renders label', () => {
  const testOption = selectOptions[0];
  const component = shallowRender();
  const instance = component.instance();
  const renderedOption = shallow(instance.renderSelectOption(testOption));
  expect(renderedOption.text()).toEqual(testOption.label);
});

// Description Textarea (auto-resizing)
test('renders WizardInput that wraps the description textarea and has correct labels', () => {
  const charactersLeft = 100;
  const component = shallowRender();
  component.setState({ charactersLeft });
  const wizardInput = component.find('WizardInput').at(1);
  expect(wizardInput).toContain('TextareaAutosize');
  expect(wizardInput).toHaveProps({
    label: messages.description,
    rightLabel: messages.charactersLeft,
    rightLabelValue: charactersLeft,
  });
});

test('WizardInput for description has `warning` modifier if max characters are reached', () => {
  const charactersLeft = 0;
  const component = shallowRender();
  component.setState({ charactersLeft });
  const wizardInput = component.find('WizardInput').at(1);
  expect(wizardInput.props().modifiers).toInclude('warning');
});

test('WizardInput for description has `rightLabelVisible` modifier if charactersLeftVisible is true', () => {
  const component = shallowRender();
  component.setState({ charactersLeftVisible: true });
  const wizardInput = component.find('WizardInput').at(1);
  expect(wizardInput.props().modifiers).toInclude('rightLabelVisible');
});

test('TextareaAutosize has the value of the current `description` state', () => {
  const description = 'current state';
  const component = shallowRender();
  component.setState({ description });
  const descriptionTextarea = component.find('TextareaAutosize');
  expect(descriptionTextarea).toHaveProps({ value: description });
});

test('onChange event of TextareaAutosize triggers handleTextChange method', () => {
  const component = shallowRender();
  const descriptionTextarea = component.find('TextareaAutosize');
  expect(descriptionTextarea).toHaveProps({ onChange: component.handleTextChange });
});

test('handleTextChange does not update description and charactersLeft if the characters reached MAX_DESCRIPTION_LENGTH', () => {
  const syntheticEvent = {
    target: {
      value: {
        length: 6001,
      },
    },
  };
  const prevState = {
    descirption: 'hahaha',
    charactersLeft: 5994,
  };
  const component = shallowRender();
  component.setState(prevState);
  const instance = component.instance();
  instance.handleTextChange(syntheticEvent);
  expect(component).toHaveState(prevState);
});

test('handleTextChange updates the current description and charactersLeft in state if the length is less than MAX_DESCRIPTION_LENGTH', () => {
  const testDescription = `
    Lorem ipsum dolor sit amet, ius at omnis platonem comprehensam, ex legere aliquid honestatis eam. Mel ex essent utamur dissentiunt, in mel nulla nostro officiis. Et dicunt deserunt mea. Quo facer contentiones ex, an per oblique reprimique.
    Ad vide legimus vituperata nam, vel ea inani voluptua intellegat. At sed offendit praesent, eu mel veniam nostrum similique. Eam alia aeque id, per et aeterno dolorum molestie, aliquip ocurreret principes eu vim. No nisl incorrupte pro.
    Mea zril minimum ullamcorper id, sit ei vidit putent temporibus. Nominavi tractatos id eum, epicuri repudiare consequuntur vim no, pro laoreet alienum pericula ne. Eam dicat habemus complectitur at, cu sed animal aperiri euripidis, erant dictas persecuti pri id. Omnium gloriatur eloquentiam eam no, alienum intellegat per id. Eu labore vocibus gubergren sit, ut eros dolore duo.
    An vim fugit vocibus, te congue nonumy voluptaria ius. Ex equidem definiebas est, at modo labitur eum, sea ei errem tincidunt voluptatibus. Tritani volumus vix et, cu eos tempor graeco temporibus. In his sumo mediocrem, duo in quis blandit copiosae, sit in iusto feugiat feugait. Purto laudem fuisset te mel, elitr detracto democritum has ad, vim erant insolens expetenda ad.
    Petentium abhorreant ad per. Vim homero consectetuer no, ne his nihil affert persecuti. Nam ut accusam noluisse scripserit, eos an audire percipitur. Habeo nulla putant id eum, eu has stet delicatissimi, cibo instructior usu ex. Ex duo alia error, apeirian dissentiet sea te.
    Nam eu dictas omnesque dissentiunt, sea aliquip invidunt evertitur ex. Te pro nonumes conceptam, et usu integre diceret. Qui te audire praesent corrumpit. Ei eam alii mentitum sadipscing, in nam suas docendi forensibus. Mei soleat atomorum te.
    In est pertinax eleifend, id eloquentiam vituperatoribus vel. Vim te legendos recusabo. Verear fuisset vel id. Sea nostro verear in, mucius veritus epicuri sed an, dolor solet efficiantur nec no. Nisl prima commune sed ut, pro alterum ceteros dignissim no. Vix ex tota alienum blandit.
    Ut putant maiorum adipiscing cum, eum iriure feugait omittantur an. An malorum dolorum singulis pri, eu aperiam graecis offendit mea, elit option te vis. Mel tollit gubergren accommodare in. Augue deleniti perfecto mea an, mea te agam utinam possim. His ex habeo oporteat adipiscing.
    Nam sonet detraxit forensibus ut, illum posidonium vel eu, te facilis posidonium mea. Ius et posse accumsan. Ne mea decore exerci. Ad labore audiam ius, ut possim recusabo disputando mei, animal delectus an nec. Vis elitr consequuntur ne, prima impetus pericula et nam. Et electram abhorreant referrentur vim. Usu ridens senserit quaerendum cu, choro pertinacia deseruisse est ne.
    At mel altera persecuti scribentur, id nam doctus latine inciderint. Vim no epicuri praesent, vidit decore oportere an vix, menandri conceptam rationibus mea id. Libris impetus rationibus nam ex, liber eripuit mediocrem ei vim. Te mel error dolore laboramus. Ut mucius option denique pro. In mea euismod facilis, an debet impedit omittantur sed, has cu augue senserit delicatissimi.
    Lorem ipsum dolor sit amet, ius at omnis platonem comprehensam, ex legere aliquid honestatis eam. Mel ex essent utamur dissentiunt, in mel nulla nostro officiis. Et dicunt deserunt mea. Quo facer contentiones ex, an per oblique reprimique.
    Ad vide legimus vituperata nam, vel ea inani voluptua intellegat. At sed offendit praesent, eu mel veniam nostrum similique. Eam alia aeque id, per et aeterno dolorum molestie, aliquip ocurreret principes eu vim. No nisl incorrupte pro.
    Mea zril minimum ullamcorper id, sit ei vidit putent temporibus. Nominavi tractatos id eum, epicuri repudiare consequuntur vim no, pro laoreet alienum pericula ne. Eam dicat habemus complectitur at, cu sed animal aperiri euripidis, erant dictas persecuti pri id. Omnium gloriatur eloquentiam eam no, alienum intellegat per id. Eu labore vocibus gubergren sit, ut eros dolore duo.
    An vim fugit vocibus, te congue nonumy voluptaria ius. Ex equidem definiebas est, at modo labitur eum, sea ei errem tincidunt voluptatibus. Tritani volumus vix et, cu eos tempor graeco temporibus. In his sumo mediocrem, duo in quis blandit copiosae, sit in iusto feugiat feugait. Purto laudem fuisset te mel, elitr detracto democritum has ad, vim erant insolens expetenda ad.
    Petentium abhorreant ad per. Vim homero consectetuer no, ne his nihil affert persecuti. Nam ut accusam noluisse scripserit, eos an audire percipitur. Habeo nulla putant id eum, eu has stet delicatissimi, cibo instructior usu ex. Ex duo alia error, apeirian dissentiet sea te.
    Nam eu dictas omnesque dissentiunt, sea aliquip invidunt evertitur ex. Te pro nonumes conceptam, et usu integre diceret. Qui te audire praesent corrumpit. Ei eam alii mentitum sadipscing, in nam suas docendi forensibus. Mei soleat atomorum te.
    In est pertinax eleifend, id eloquentiam vituperatoribus vel. Vim te legendos recusabo. Verear fuisset vel id. Sea nostro verear in, mucius veritus epicuri sed an, dolor solet efficiantur nec no. Nisl prima commune sed ut, pro alterum ceteros dignissim no. Vix ex tota alienum blandit.
    Ut putant maiorum adipiscing cum, eum iriure feugait omittantur an. An malorum dolorum singulis pri, eu aperiam graecis offendit mea, elit option te vis. Mel tollit gubergren accommodare in. Augue deleniti perfecto mea an, mea te agam utinam possim. His ex habeo oporteat adipiscing.
    Nam sonet detraxit forensibus ut, illum posidonium vel eu, te facilis posidonium mea. Ius et posse accumsan. Ne mea decore exerci. Ad labore audiam ius, ut possim recusabo disputando mei, animal delectus an nec. Vis elitr consequuntur ne, prima impetus pericula et nam. Et electram abhorreant referrentur vim. Usu ridens senserit quaerendum cu, choro pertinacia deseruisse est ne.
    At mel altera persecuti scribentur, id nam doctus latine inciderint. Vim no epicuri praesent, vidit decore oportere an vix, m
  `;
  const syntheticEvent = {
    target: {
      value: testDescription,
    },
  };
  const component = shallowRender();
  component.setState({ description: 'previous description' });
  const instance = component.instance();
  instance.handleTextChange(syntheticEvent);

  expect(testDescription.length).toBe(6000);
  expect(component).toHaveState({
    description: testDescription,
    charactersLeft: 0,
  });
});

test('handleTextChange clears charactersLeftTimer', () => {
  const clearTimeoutSpy = spyOn(global, 'clearTimeout');
  const testFunction = () => true;
  const component = shallowRender();
  const instance = component.instance();
  instance.charactersLeftTimer = testFunction;
  instance.handleTextChange({ target: { value: 'test message' } });
  expect(clearTimeoutSpy).toHaveBeenCalledWith(testFunction);
});

test.cb('handleTextChange sets charactersLeftVisible to true, and then to false after 1 second', (t) => {
  t.plan(1);
  const component = shallowRender();
  component.setState({ charactersLeftVisible: false });
  const instance = component.instance();
  instance.handleTextChange({ target: { value: 'test message' } });
  expect(component).toHaveState({ charactersLeftVisible: true });
  setTimeout(() => {
    expect(component).toHaveState({ charactersLeftVisible: false });
    t.pass();
    t.end();
  }, 1000);
});

// Warning Label
test('does not render the character max reached warning label if `charactersLeft` > 0', () => {
  const component = shallowRender();
  component.setState({ charactersLeft: 1 });
  expect(component).toNotContain('WarningLabel');
});

test('renders the character max reached warning label with correct message if `charactersLeft` is 0', () => {
  const component = shallowRender();
  component.setState({ charactersLeft: 0 });
  expect(component).toContain('WarningLabel');
  expect(component.find('WarningLabel')).toHaveProps({ label: messages.characterCountWarning });
});

// Continue Button
test('renders Continue button with correct label and onClick handler prop', () => {
  const component = shallowRender();
  expect(component).toContain('QuickActionButton');

  const continueButton = component.find('QuickActionButton');
  expect(continueButton.find('FormattedMessage')).toHaveProps({ ...messages.continueButton });
  expect(continueButton).toHaveProps({ onClick: component.handleClickContinue });
});

test('disables Continue button if there is no selected option or description text', () => {
  const component = shallowRender();
  component.setState({
    selectedOption: undefined,
    description: '',
  });
  const continueButton = component.find('QuickActionButton');
  expect(continueButton.props().modifiers).toInclude('disabled');
});

test('enables Continue button if there is a selected option', () => {
  const component = shallowRender();
  component.setState({
    selectedOption: selectOptions[0],
    description: '',
  });
  const continueButton = component.find('QuickActionButton');
  expect(continueButton.props().modifiers).toNotInclude('disabled');
});

test('enables Continue button if there is a description input', () => {
  const component = shallowRender();
  component.setState({
    selectedOption: undefined,
    description: 'hey hey hey',
  });
  const continueButton = component.find('QuickActionButton');
  expect(continueButton.props().modifiers).toNotInclude('disabled');
});

test('handleClickContinue does not submit data if there is no selected option or description', () => {
  const submitInputData = createSpy();
  const component = shallowRender({ ...defaultProps, submitInputData });
  component.setState({
    selectedOption: undefined,
    description: '',
  });
  const instance = component.instance();
  instance.handleClickContinue();
  expect(submitInputData).toNotHaveBeenCalled();
});

test('handleClickContinue submits data if there is a selected option', () => {
  const submitInputData = createSpy();
  const component = shallowRender({ ...defaultProps, submitInputData });
  component.setState({
    selectedOption: selectOptions[0],
    description: '',
  });
  const instance = component.instance();
  instance.handleClickContinue();
  expect(submitInputData).toHaveBeenCalledWith({
    complaintCode: selectOptions[0].code,
    complaintDescription: selectOptions[0].description,
    description: '',
  });
});

test('handleClickContinue submits data if there is a description input', () => {
  const testDescription = 'test complaint description';
  const submitInputData = createSpy();
  const component = shallowRender({ ...defaultProps, submitInputData });
  component.setState({
    selectedOption: undefined,
    description: testDescription,
  });
  const instance = component.instance();
  instance.handleClickContinue();
  expect(submitInputData).toHaveBeenCalledWith({
    complaintCode: '',
    complaintDescription: '',
    description: testDescription,
  });
});
