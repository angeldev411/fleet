import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { convertRailsTz } from 'utils/timeUtils';

import Widget from 'elements/Widget';
import EmptyValue from 'elements/EmptyValue';
import postalAddressFormatter from 'utils/postalAddressFormatter';

import CaseServiceProviderContainer from './CaseServiceProviderContainer';
import ServiceRequestContainer from './ServiceRequestContainer';
import OperatingHoursTable from './OperatingHoursTable';
import messages from './messages';

export function getAddressHtml(addressMap) {
  const addressLines = addressMap ? postalAddressFormatter(addressMap) : [];
  return addressLines.map(line => <div key={line}>{line}</div>);
}

export function ServiceProviderWidget({ serviceProviderInfo }) {
  const serviceProviderTimeZone = convertRailsTz(serviceProviderInfo.timezone);
  const operatingHours = serviceProviderInfo.operatingHours;
  const operatingHoursProvided = operatingHours && operatingHours.length;

  const addressHtml = getAddressHtml(serviceProviderInfo.address);
  return (
    <Widget expandKey="service-provider">
      <Widget.Header>
        <FormattedMessage {...messages.title} />
      </Widget.Header>
      <Widget.Item>
        <Widget.SubHeader>
          {serviceProviderInfo.companyName}
        </Widget.SubHeader>
      </Widget.Item>
      <Widget.Item>
        <Widget.Table>
          <tbody>
            <Widget.TableRow>
              <th>
                <FormattedMessage {...messages.address} />
              </th>
              <td>
                {addressHtml}
              </td>
            </Widget.TableRow>
            <Widget.TableRow modifiers={['topGap']}>
              <th>
                <FormattedMessage {...messages.phone} />
              </th>
              <td>
                {serviceProviderInfo.phone}<br />
                {serviceProviderInfo.fax}
              </td>
            </Widget.TableRow>
            {/* Contact is not shown in alpha version
              <WidgetTableRow>
                <th>Contact</th>
                <td />
              </WidgetTableRow>
            */}
            <Widget.TableRow modifiers={['topGap']}>
              <th>
                <FormattedMessage {...messages.hours} />
              </th>
              <td>
                {
                  operatingHoursProvided ?
                    <OperatingHoursTable
                      operatingHours={operatingHours}
                      timeZone={serviceProviderTimeZone}
                    />
                    :
                    <EmptyValue>
                      <FormattedMessage {...messages.noHoursAvailable} />
                    </EmptyValue>
                }
              </td>
            </Widget.TableRow>
          </tbody>
        </Widget.Table>
      </Widget.Item>
    </Widget>
  );
}

ServiceProviderWidget.propTypes = {
  serviceProviderInfo: PropTypes.shape({
    address: PropTypes.shape({
      street: PropTypes.string,
      street2: PropTypes.string,
      city: PropTypes.string,
      region: PropTypes.string,
      postalCode: PropTypes.string,
    }),
    phone: PropTypes.string,
    fax: PropTypes.string,
    operatingHours: PropTypes.arrayOf(PropTypes.shape({
      day: PropTypes.string,
      open: PropTypes.string,
      close: PropTypes.string,
    })),
  }),
};

ServiceProviderWidget.defaultProps = {
  serviceProviderInfo: {
    address: {
      street: '',
      street2: '',
      city: '',
      region: '',
      postalCode: '',
    },
    phone: '',
    fax: '',
    operatingHours: [],
  },
};

export const CaseServiceProviderWidget = CaseServiceProviderContainer(ServiceProviderWidget);
export const ServiceRequestServiceProviderWidget = ServiceRequestContainer(ServiceProviderWidget);
