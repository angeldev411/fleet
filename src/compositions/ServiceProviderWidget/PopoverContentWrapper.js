import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const PopoverContentWrapper = styled.div`
  position: relative;
  white-space: nowrap;
  padding: ${px2rem(10)};
`;

export default PopoverContentWrapper;
