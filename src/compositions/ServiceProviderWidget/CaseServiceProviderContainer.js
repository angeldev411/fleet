import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  currentCaseServiceProviderSelector,
} from 'redux/cases/selectors';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    serviceProviderInfo: PropTypes.shape({
      id: PropTypes.string,
    }),
  };

  Container.defaultProps = {
    serviceProviderInfo: {},
  };

  function mapStateToProps(state) {
    return {
      serviceProviderInfo: currentCaseServiceProviderSelector(state),
    };
  }

  return connect(mapStateToProps)(immutableToJS(Container));
};

export default withConnectedData;
