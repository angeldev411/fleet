import { defineMessages } from 'react-intl';

const messages = defineMessages({
  address: {
    id: 'compositions.ServiceProvider.data.address',
    defaultMessage: 'Address',
  },
  closed: {
    id: 'compositions.ServiceProvider.data.closed',
    defaultMessage: 'Closed',
  },
  friday: {
    id: 'compositions.ServiceProvider.dayAbbreviation.friday',
    defaultMessage: 'Fri',
  },
  hours: {
    id: 'compositions.ServiceProvider.data.hours',
    defaultMessage: 'Hours',
  },
  monday: {
    id: 'compositions.ServiceProvider.dayAbbreviation.monday',
    defaultMessage: 'Mon',
  },
  noHoursAvailable: {
    id: 'compositions.ServiceProvider.undefined.noHoursAvailable',
    defaultMessage: 'Not Provided',
  },
  open: {
    id: 'compositions.ServiceProvider.data.open',
    defaultMessage: 'Open',
  },
  phone: {
    id: 'compositions.ServiceProvider.data.phone',
    defaultMessage: 'Phone',
  },
  saturday: {
    id: 'compositions.ServiceProvider.dayAbbreviation.saturday',
    defaultMessage: 'Sat',
  },
  sunday: {
    id: 'compositions.ServiceProvider.dayAbbreviation.sunday',
    defaultMessage: 'Sun',
  },
  thursday: {
    id: 'compositions.ServiceProvider.dayAbbreviation.thursday',
    defaultMessage: 'Thu',
  },
  title: {
    id: 'compositions.ServiceProvider.title',
    defaultMessage: 'Service Provider',
  },
  tuesday: {
    id: 'compositions.ServiceProvider.dayAbbreviation.tuesday',
    defaultMessage: 'Tue',
  },
  wednesday: {
    id: 'compositions.ServiceProvider.dayAbbreviation.wednesday',
    defaultMessage: 'Wed',
  },
});

export default messages;
