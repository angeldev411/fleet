import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.brand.tertiary};
  font-size: ${px2rem(10)};
  line-height: ${px2rem(15)};
  text-align: right;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    brand: PropTypes.shape({
      tertiary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};


export default buildStyledComponent(
  'TimezoneWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
