import React from 'react';
import PropTypes from 'prop-types';
import { last, initial } from 'lodash';
import moment from 'moment-timezone';
import { FormattedMessage, injectIntl } from 'react-intl';
import Widget from 'elements/Widget';
import Popover, { PopoverTarget, PopoverContent } from 'elements/Popover';
import { getCurrentTime } from 'utils/timeUtils';
import PopoverContentWrapper from './PopoverContentWrapper';
import TimezoneWrapper from './TimezoneWrapper';
import messages from './messages';

/**
 * Clean up any random extra whitespace in the given time string (expected to
 * be roughly in a format like " 5:00AM") for the service provider operating
 * hours.
 */
export function normalizeTimeString(str) {
  return str.replace(/\s/g, '') // remove all whitespace
    .replace('AM', ' AM')
    .replace('PM', ' PM');
}

/**
 * Determine if the service provider (which is located in the given `timeZone`) is
 * currently open, based on the open/close hours of a given day of the week.
 */
function evalOpenStatus(targetDay, openingTime, closingTime, timeZone) {
  const timeNowAtProvider = getCurrentTime(timeZone);
  const targetDayAtProvider = moment(timeNowAtProvider).day(targetDay);

  const dayNameAtProvider = targetDayAtProvider.format('dddd');

  // fail fast if `targetDay` is not the current day of the week at the provider
  if (dayNameAtProvider !== targetDay) return false;

  // create time objects for opening/closing times of the target day in the provider's time-zone:
  const ymd = targetDayAtProvider.format('YYYY-MM-DD');
  const parseFormat = 'YYYY-MM-DD h:mm A';
  const targetDayOpenAt = moment.tz(`${ymd} ${openingTime}`, parseFormat, timeZone);
  const targetDayCloseAt = moment.tz(`${ymd} ${closingTime}`, parseFormat, timeZone);

  // If targetDayOpenAt and targetDayCloseAt are same,
  // it is supposed that targetDayCloseAt is one day more.
  if (targetDayOpenAt.isSame(targetDayCloseAt)) targetDayCloseAt.add(1, 'day');
  // determine if the service provider is open right now, due to the current time
  // in the provider's time-zone falling between the open and close times for
  // the day of the week we're currently processing:
  return timeNowAtProvider.isBetween(targetDayOpenAt, targetDayCloseAt);
}

/**
 * This function serves two purposes:
 *   1. Process the incoming array of service provider operating hours to collapse multiple
 *      days having the same open/close hours into single entries.
 *   2. Determine if the service provider is currently open, based on the specified days
 *      and opening/closing hours.
 * It returns an object with two properties:
 *    * `hours`: the list of processed day-ranges with open/close hours
 *    * `isOpen`: a boolean indicating whether the provider is *currently* open
 */
function processOperatingHours(operatingHours, timeZone) {
  // incoming operatingHours will look something like this:
  // [  {:day=>"Monday", :open=>" 5:00AM", :close=>" 5:00PM"},
  //    {:day=>"Tuesday", :open=>" 5:00AM", :close=>" 5:00PM"},
  //    {:day=>"Wednesday", :open=>" 5:00AM", :close=>" 5:00PM"},
  //    {:day=>"Thursday", :open=>" 5:00AM", :close=>" 5:00PM"},
  //    {:day=>"Friday", :open=>" 5:00AM", :close=>" 5:00PM"},
  //    {:day=>"Saturday", :open=>" 5:00AM", :close=>" 3:00PM"},
  //    {:day=>"Sunday", :open=>" 5:00AM", :close=>" 3:00PM"} ]
  //
  // output should be...
  // [  { start: 'Monday', end: 'Friday', open: '5 AM', close: '5 PM' },
  //    { start: 'Saturday', end: 'Sunday', open: '5 AM', close: '3 PM' } ]

  let isOpen = false;

  const processedHours = operatingHours.reduce((processed, dayInfo) => {
    // get the last processed day, and all the other processed days before that one:
    const lastDay = processed.length ?
      last(processed) :
      { start: '', end: '', open: '', close: '' };
    const otherDays = initial(processed);

    // this will be the full English name of the day of the week we're processing now:
    const targetDay = dayInfo.day;

    // clean up the open/close times:
    const open = normalizeTimeString(dayInfo.open);
    const close = normalizeTimeString(dayInfo.close);

    // if we haven't already found that the provider is open on a previous
    // day, check whether they're open on this day:
    isOpen = isOpen || evalOpenStatus(targetDay, open, close, timeZone);

    // compare this day's open/close with the last day's open/close:
    if (open === lastDay.open && close === lastDay.close) {
      // same open/close hours so merge this day into the last day
      return [...otherDays, {
        start: lastDay.start || targetDay,
        end: targetDay,
        open,
        close,
      }];
    }
    // create a new day since this one has different hours from the last one
    return [...processed, {
      start: targetDay,
      end: targetDay,
      open,
      close,
    }];
  }, []);

  return { hours: processedHours, isOpen };
}

/**
 * Takes an English-language day of the week (e.g. "Monday") and
 * translates it into the current language's abbreviated representation
 * of that day of the week (e.g. "Mon" for English, "Lu" or "Lun" for
 * Spanish).
 */
function translateDay(englishDayName, formatMessage) {
  return formatMessage(messages[englishDayName.toLowerCase()]);
}

/**
 * Render a day or a range of days in the current locale.
 */
export function renderDayRange({ start, end }, formatMessage) {
  return start === end
    ? translateDay(start, formatMessage)
    : `${translateDay(start, formatMessage)} - ${translateDay(end, formatMessage)}`;
}

/**
 * Remove :00 from the hours (but leave meaningful minutes) and render the
 * `open` and `close` values with a dash between.
 */
export function renderTimes({ open, close }) {
  const openString = open.replace(':00', '');
  const closeString = close.replace(':00', '');
  return `${openString} - ${closeString}`;
}

/**
 * Iterate through the given daily hours specification and render one row for
 * each day or range of days.
 */
function renderDailyOpeningHours(hours, formatMessage, shortTimezone) {
  return (
    <PopoverContentWrapper>
      <table>
        <tbody>
          <tr>
            <td>
              <FormattedMessage {...messages.hours} />
            </td>
            <td>
              <TimezoneWrapper>
                ({shortTimezone})
              </TimezoneWrapper>
            </td>
          </tr>
          {
            hours.map((day, index) => (
              <Widget.TableRow key={day.start} modifiers={[index ? '' : 'topGap']}>
                <th>
                  {renderDayRange(day, formatMessage)}
                </th>
                <td>
                  {renderTimes(day)}
                </td>
              </Widget.TableRow>
            ))
          }
        </tbody>
      </table>
    </PopoverContentWrapper>
  );
}

export function OperatingHoursTable({ operatingHours, timeZone, intl: { formatMessage } }) {
  const { hours, isOpen } = processOperatingHours(operatingHours, timeZone);
  const colorModifier = isOpen ? 'statusSuccess' : 'statusDanger';
  const target = (
    <Widget.SubHeader modifiers={['small', colorModifier]}>
      <FormattedMessage {...(isOpen ? messages.open : messages.closed)} />
    </Widget.SubHeader>
  );
  const shortTimezone = moment().tz(timeZone).format('z');
  const popOverContent = renderDailyOpeningHours(hours, formatMessage, shortTimezone);
  return (
    <table>
      <tbody>
        <tr>
          <td>
            <Popover
              showOnHover
            >
              <PopoverTarget>
                {target}
              </PopoverTarget>
              <PopoverContent>
                {popOverContent}
              </PopoverContent>
            </Popover>
          </td>
        </tr>
      </tbody>
    </table>
  );
}

OperatingHoursTable.propTypes = {
  operatingHours: PropTypes.arrayOf(PropTypes.shape({
    day: PropTypes.string,
    open: PropTypes.string,
    close: PropTypes.string,
  })),
  timeZone: PropTypes.string,
  intl: PropTypes.shape({
    formatMessage: PropTypes.func.isRequired,
  }).isRequired,
};

OperatingHoursTable.defaultProps = {
  operatingHours: [],
  timeZone: '',
};

export default injectIntl(OperatingHoursTable);
