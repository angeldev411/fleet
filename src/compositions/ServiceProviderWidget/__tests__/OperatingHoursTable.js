import {
  test,
  expect,
  shallow,
  mount,
  MountableTestComponent,
  spyOn,
  createSpy,
} from '__tests__/helpers/test-setup';
import React from 'react';
import moment from 'moment-timezone';

import * as timeUtils from 'utils/timeUtils';

import messages from '../messages';
import {
  OperatingHoursTable,
  normalizeTimeString,
  renderTimes,
  renderDayRange,
} from '../OperatingHoursTable';

const defaultOperatingHours = [
  { day: 'Monday', open: ' 5:00AM', close: ' 5:00PM' },
  { day: 'Tuesday', open: ' 5:00AM', close: ' 5:00PM' },
  { day: 'Wednesday', open: ' 5:00AM', close: ' 5:00PM' },
  { day: 'Thursday', open: ' 5:00AM', close: ' 5:00PM' },
  { day: 'Friday', open: ' 5:00AM', close: ' 5:00PM' },
  { day: 'Saturday', open: ' 5:00AM', close: ' 3:00PM' },
  { day: 'Sunday', open: ' 5:00AM', close: ' 3:00PM' },
];

const intlMock = {
  formatMessage: ({ defaultMessage }) => defaultMessage,
};

const defaultTimeZone = 'America/New_York';

function shallowRender({
  operatingHours = defaultOperatingHours,
  timeZone = defaultTimeZone,
  intl = intlMock,
} = {}) {
  return shallow(
    <OperatingHoursTable
      operatingHours={operatingHours}
      timeZone={timeZone}
      intl={intl}
    />);
}

function fullRender({
  operatingHours = defaultOperatingHours,
  timeZone = defaultTimeZone,
  intl = intlMock,
} = {}) {
  return mount(
    <MountableTestComponent authorized>
      <OperatingHoursTable
        operatingHours={operatingHours}
        timeZone={timeZone}
        intl={intl}
      />
    </MountableTestComponent>,
  );
}

test('Renders the table', () => {
  expect(shallowRender({ operatingHours: defaultOperatingHours })).toBeA('table');
});

test('Renders the exact number of TableRow', () => {
  const table = shallowRender({ operatingHours: defaultOperatingHours });
  expect(table.find('TableRow').length).toEqual(2);
});

test('Renders label for Mon-Fri', () => {
  expect(fullRender({ operatingHours: defaultOperatingHours }).text()).toInclude('5 AM - 5 PM');
});

test('Renders label for Sat-Sun', () => {
  expect(fullRender({ operatingHours: defaultOperatingHours }).text()).toInclude('5 AM - 3 PM');
});

// ----- helpers for testing whether widget shows "OPEN" or "CLOSED" -----

function renderSubHeader(operatingHours, timeZone = defaultTimeZone) {
  return shallowRender({ operatingHours, timeZone }).find('SubHeader');
}

function expectProviderIsOpen(operatingHours, timeZone = defaultTimeZone) {
  const subHeader = renderSubHeader(operatingHours, timeZone);
  expect(subHeader.props().modifiers).toContain('statusSuccess');
  expect(subHeader.find('FormattedMessage')).toHaveProps(messages.open);
}

function expectProviderIsClosed(operatingHours, timeZone = defaultTimeZone) {
  const subHeader = renderSubHeader(operatingHours, timeZone);
  expect(subHeader.props().modifiers).toContain('statusDanger');
  expect(subHeader.find('FormattedMessage')).toHaveProps(messages.closed);
}

// ----- tests for "OPEN" vs "CLOSED" ----

test('Renders closed status', () => {
  const operatingHoursEmpty = [];
  expectProviderIsClosed(operatingHoursEmpty);
});

test('Renders open status', () => {
  const operatingHoursFull = [
    { day: 'Monday', open: ' 0:00AM', close: ' 11:59PM' },
    { day: 'Tuesday', open: ' 0:00AM', close: ' 11:59PM' },
    { day: 'Wednesday', open: ' 0:00AM', close: ' 11:59PM' },
    { day: 'Thursday', open: ' 0:00AM', close: ' 11:59PM' },
    { day: 'Friday', open: ' 0:00AM', close: ' 11:59PM' },
    { day: 'Saturday', open: ' 0:00AM', close: ' 11:59PM' },
    { day: 'Sunday', open: ' 0:00AM', close: ' 11:59PM' },
  ];
  expectProviderIsOpen(operatingHoursFull);
});

test('Renders open status when open and close are all same to 12:00AM', () => {
  const operatingHoursFull = [
    { day: 'Monday', open: ' 12:00AM', close: ' 12:00AM' },
    { day: 'Tuesday', open: ' 12:00AM', close: ' 12:00AM' },
    { day: 'Wednesday', open: ' 12:00AM', close: ' 12:00AM' },
    { day: 'Thursday', open: ' 12:00AM', close: ' 12:00AM' },
    { day: 'Friday', open: ' 12:00AM', close: ' 12:00AM' },
    { day: 'Saturday', open: ' 12:00AM', close: ' 12:00AM' },
    { day: 'Sunday', open: ' 12:00AM', close: ' 12:00AM' },
  ];
  expectProviderIsOpen(operatingHoursFull);
});

test('The service provider is open if open in their local timezone', () => {
  const providerTZ = 'Europe/Vienna';
  const nowInVienna = moment.tz('2017-04-19T11:12:13', providerTZ);
  spyOn(timeUtils, 'getCurrentTime').andReturn(nowInVienna);
  const operatingHours = [
    {
      day: nowInVienna.format('dddd'),
      open: moment(nowInVienna).subtract(1, 'hours').format('h:mm A'),
      close: moment(nowInVienna).add(1, 'hours').format('h:mm A'),
    },
  ];
  expectProviderIsOpen(operatingHours, providerTZ);
});

test('The service provider is closed if closed in their local timezone', () => {
  const providerTZ = 'Europe/Vienna';
  const nowInVienna = moment.tz('2017-04-19T11:12:13', providerTZ);
  spyOn(timeUtils, 'getCurrentTime').andReturn(nowInVienna);
  const operatingHours = [
    {
      day: nowInVienna.format('dddd'),
      open: moment(nowInVienna).add(1, 'hours').format('h:mm A'),
      close: moment(nowInVienna).add(2, 'hours').format('h:mm A'),
    },
  ];
  expectProviderIsClosed(operatingHours, providerTZ);
});

// -------------------------------- normalizeTimeString --------------------------------

test('normalizeTimeString() Returns "5:00 AM" for " 5:00AM"', () => {
  expect(normalizeTimeString(' 5:00AM')).toEqual('5:00 AM');
});

test('normalizeTimeString() Return "3:30 PM" for "3:30PM "', () => {
  expect(normalizeTimeString('3:30PM ')).toEqual('3:30 PM');
});

// -------------------------------- renderTimes -----------------------

test('renderTimes() removes :00 from the hours', () => {
  expect(renderTimes({ open: '3:00 PM', close: '5:00 PM' })).toEqual('3 PM - 5 PM');
});

test('renderTimes() leaves non-:00 minutes in the hours', () => {
  expect(renderTimes({ open: '3:30 PM', close: '5:01 PM' })).toEqual('3:30 PM - 5:01 PM');
});

// -------------------------------- renderDayRange -----------------------

const formatMessageMock = createSpy().andCall(x => x);

test('renderDayRange() only renders the first day if last day is the same', () => {
  const day = { start: 'Tuesday', end: 'Tuesday' };
  expect(renderDayRange(day, formatMessageMock)).toEqual(messages.tuesday);
});

test('renderDayRange() renders both start and end days if they are different', () => {
  const day = { start: 'Tuesday', end: 'Friday' };
  const expected = `${messages.tuesday} - ${messages.friday}`;
  expect(renderDayRange(day, formatMessageMock)).toEqual(expected);
});
