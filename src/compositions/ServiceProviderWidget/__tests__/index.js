import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import messages from '../messages';
import { ServiceProviderWidget, getAddressHtml } from '../index';

const defaultServiceProviderInfo = {
  id: 'PVN646',
  companyName: 'Hall\'s Towing Service - PVN646',
  relativeDistance: null,
  address: {
    street: '1161 Weems Street ',
    street2: 'Payment Via E-Mail (To Amanda Tagert) ',
    city: 'Pearl',
    region: 'Test Region',
    postalCode: '39208',
    location: {
      lat: '32.265804290771484',
      lon: '-90.09285736083984',
    },
  },
  operatingHours: [
    {
      day: 'Monday',
      open: ' 4:00AM',
      close: '3:00PM',
    },
    {
      day: 'Tuesday',
      open: '  7:00AM',
      close: '5:00PM',
    },
    {
      day: 'Friday',
      open: '7:00AM',
      close: '10:00PM',
    },
    {
      day: 'Saturday',
      open: '7:00AM',
      close: '10:00PM',
    },
    {
      day: 'Sunday',
      open: '1:00PM',
      close: '5:30PM',
    },
  ],
  phone: '(800) 748-9960',
  fax: '(601) 939-0609',
  timezone: 'Pacific Time (US & Canada)',
};

function buildServiceProvider(serviceProviderInfo = defaultServiceProviderInfo) {
  return shallow(<ServiceProviderWidget serviceProviderInfo={serviceProviderInfo} />);
}

test('Renders the correct component', () => {
  expect(buildServiceProvider()).toBeA('Widget');
});

test('Renders the sub header with expected props', () => {
  const widgetSubHeader = buildServiceProvider().find('SubHeader');
  const companyName = defaultServiceProviderInfo.companyName;
  const widgetSubHeaderChildren = widgetSubHeader.props().children;
  expect(widgetSubHeaderChildren).toContain(companyName);
});

test('Renders Exact Address', () => {
  const addressHtml = getAddressHtml(defaultServiceProviderInfo.address);
  expect(buildServiceProvider().contains(addressHtml)).toEqual(true);
});

test('Renders OperatingHoursTable when there is an operating Hours provided', () => {
  expect(buildServiceProvider()).toContain('InjectIntl(OperatingHoursTable)');
});

test('Does not render OperatingHoursTable when operatingHours is not provided', () => {
  const serviceProviderWithoutOperatingHours = {
    ...defaultServiceProviderInfo,
    operatingHours: [],
  };
  const component = buildServiceProvider(serviceProviderWithoutOperatingHours);
  expect(component).toNotContain('InjectIntl(OperatingHoursTable)');
});

test('Does not render EmptyMessage when there is an operating Hours provided', () => {
  const { id } = messages.noHoursAvailable;
  expect(buildServiceProvider()).toNotContain(`EmptyValue FormattedMessage[id="${id}"]`);
});

test('Render EmptyMessage when operatingHours is not provided', () => {
  const serviceProviderWithoutOperatingHours = {
    ...defaultServiceProviderInfo,
    operatingHours: null,
  };
  const component = buildServiceProvider(serviceProviderWithoutOperatingHours);
  const { id } = messages.noHoursAvailable;
  expect(component).toContain(`EmptyValue FormattedMessage[id="${id}"]`);
});
