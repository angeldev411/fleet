import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  title: {
    id: 'compositions.ServiceRequest.title',
    defaultMessage: 'Service Request',
  },
  reasonForRepair: {
    id: 'compositions.ServiceRequest.data.reasonForRepair',
    defaultMessage: 'Reason For Repair',
  },
});

export default formattedMessages;
