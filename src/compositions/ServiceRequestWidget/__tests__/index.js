import React from 'react';

import {
  test,
  expect,
  shallow,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import { ServiceRequestWidget } from '../index';

const requestCode = '123';
const requestDescription = 'Request description';
const caseInfo = {
  reasonForRepair: {
    code: requestCode,
    description: requestDescription,
  },
};

const defaultProps = { caseInfo };

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestWidget {...props} />);
}

test('ServiceRequestWidget renders the correct content', () => {
  const component = mount(
    <MountableTestComponent >
      <ServiceRequestWidget caseInfo={caseInfo} />
    </MountableTestComponent>,
  );
  const requestContent = component.find('Table').render().text();
  expect(requestContent).toInclude(`${requestCode} - ${requestDescription}`);
});

test('ServiceRequestWidget renders when reasonForRepair is null', () => {
  const testProps = { caseInfo: { reasonForRepair: null } };
  const component = shallowRender(testProps);
  expect(component).toContain('Table');
});
