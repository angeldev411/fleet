import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Widget from 'elements/Widget';

import { getOutputText } from 'utils/widget';

import withConnectedData from './Container';
import messages from './messages';

export function ServiceRequestWidget({
  caseInfo: {
    reasonForRepair,
  },
}) {
  const { code, description } = reasonForRepair || {};
  const content = code && description && `${code} - ${description}`;

  return (
    <Widget id="service-request" expandKey="service-request">
      <Widget.Header>
        <FormattedMessage {...messages.title} />
      </Widget.Header>
      <Widget.Item>
        <Widget.Table>
          <tbody>
            <Widget.TableRow modifiers={['topGap']}>
              <th>
                <FormattedMessage {...messages.reasonForRepair} />
              </th>
              <td>
                {getOutputText(content)}
              </td>
            </Widget.TableRow>
          </tbody>
        </Widget.Table>
      </Widget.Item>
    </Widget>
  );
}

ServiceRequestWidget.propTypes = {
  caseInfo: PropTypes.shape({
    reasonForRepair: PropTypes.shape({
      code: PropTypes.string,
      description: PropTypes.string,
    }),
  }).isRequired,
};

export default withConnectedData(ServiceRequestWidget);
