import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';

import {
  currentCaseSelector,
} from 'redux/cases/selectors';

const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    caseInfo: PropTypes.shape({
      reasonForRepair: PropTypes.shape({
        code: PropTypes.string,
        description: PropTypes.string,
      }),
    }),
  };

  Container.defaultProps = {
    caseInfo: {},
  };

  function mapStateToProps(state) {
    return {
      caseInfo: currentCaseSelector(state),
    };
  }

  return connect(mapStateToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
