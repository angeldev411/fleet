import React from 'react';

import { test, expect, shallow, mount } from '__tests__/helpers/test-setup';
import MountableTestComponent from '__tests__/helpers/mountable_test_component';

import { getOdometerText } from 'utils/asset';

import { AssetsWidget, getStatuses, getTooltip } from '../index';

const assetInfo = {
  unitNumber: '1234567',
  vinNumber: '3GBKC34F41M111912',
  year: '2015',
  make: 'Volvo',
  model: 'Truck',
  engine: 'MP10',
  pmStatus: 'green',
  warrantyStatus: 'green',
  campaignsStatus: 'red',
  odometer: {
    unit: 'kilometers',
    reading: '3242',
    readAt: '2017-01-10T16:46:22Z',
  },
};

const mountComponent = () => mount(
  <MountableTestComponent>
    <AssetsWidget assetInfo={assetInfo} />
  </MountableTestComponent>,
);

const setupComponent = () => shallow(<AssetsWidget assetInfo={assetInfo} />);

test('Renders a widget', () => {
  const component = setupComponent();
  expect(component).toBeA('Widget');
});

test('Renders unit number in the header', () => {
  const component = setupComponent();
  const unitHeader = component.find('SubHeader');
  expect(unitHeader.find('span').first().text()).toInclude(assetInfo.unitNumber);
});

test('Renders the correct values in the table', () => {
  const odometer = assetInfo.odometer;
  const odometerUnit = odometer.unit;
  const odometerReading = odometer.reading;
  const odometerReadAt = odometer.readAt;
  const component = mountComponent();
  const table = component.find('Table').render();
  const tableText = table.text();
  const mileageValue = getOdometerText(odometerReading, odometerUnit, odometerReadAt);
  expect(tableText).toInclude(assetInfo.vinNumber);
  expect(tableText).toInclude(assetInfo.year);
  expect(tableText).toInclude(assetInfo.make);
  expect(tableText).toInclude(assetInfo.model);
  expect(tableText).toInclude(assetInfo.engine);
  expect(tableText).toInclude('Odometer');
  expect(tableText).toInclude(mileageValue);
});

test('Renders the Status component and its props correctly', () => {
  const statuses = getStatuses(assetInfo);
  const shallowComponent = setupComponent();
  const statusComponent = shallowComponent.find('Status');
  expect(statusComponent.length).toEqual(1);
  expect(statusComponent.props().statuses).toEqual(statuses);
});

test('Renders 2 columns', () => {
  const component = setupComponent();
  const container = component.find('Container').first();
  const row = container.find('withSize(Row)').first();
  expect(row.find('withSize(Column)').length).toEqual(2);
});

// ------------------------ getTooltip ----------------------------
const tooltips = {
  tooltip1: 'ToolTip1',
  tooltip2: 'ToolTip2',
  tooltip3: 'ToolTip3',
  green: 'ToolTipGreen',
};

test('getTooltip returns proper tooltip', () => {
  expect(getTooltip(tooltips, 'tooltip1')).toEqual('ToolTip1');
});

test('getTooltip returns default green tooltip when there is no matching key', () => {
  expect(getTooltip(tooltips, 'tooltipRandom')).toEqual(tooltips.green);
});
