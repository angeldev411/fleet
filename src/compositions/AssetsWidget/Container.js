import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';

import {
  currentCaseAssetSelector,
} from 'redux/cases/selectors';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    assetInfo: PropTypes.shape(),
  };

  Container.defaultProps = {
    assetInfo: {},
  };

  function mapStateToProps(state) {
    return {
      assetInfo: currentCaseAssetSelector(state),
    };
  }

  return connect(mapStateToProps)(
    immutableToJS(Container),
  );
};

export default withConnectedData;
