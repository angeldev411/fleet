import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  Container,
  Row,
  Column,
} from 'styled-components-reactive-grid';

import { getOdometerText } from 'utils/asset';
import { getOutputText } from 'utils/widget';

import Widget from 'elements/Widget';
import Link from 'elements/Link';

import withConnectedData from './Container';
import messages from './messages';
import Status from './Status';

export const getTooltip = (tooltips, key) =>
  tooltips[key] || tooltips.green;

export const getStatuses = assetInfo => ([
  {
    label: messages.pmStatus,
    value: assetInfo.pmStatus,
    tooltip: getTooltip({
      green: messages.pmStatusTooltipGreen,
      yellow: messages.pmStatusTooltipYellow,
      red: messages.pmStatusTooltipRed,
    }, assetInfo.pmStatus),
  },
  {
    label: messages.warranty,
    value: assetInfo.warrantyStatus,
    tooltip: getTooltip({
      green: messages.warrantyTooltipGreen,
      yellow: messages.warrantyTooltipYellow,
      red: messages.warrantyTooltipRed,
      grey: messages.warrantyTooltipGrey,
    }, assetInfo.warrantyStatus),
  },
  {
    label: messages.campaigns,
    value: assetInfo.campaignsStatus,
    tooltip: getTooltip({
      green: messages.campaignsTooltipGreen,
      red: messages.campaignsTooltipRed,
    }, assetInfo.campaignsStatus),
  },
]);

export function AssetsWidget({ assetInfo }) {
  const {
    id,
    unitNumber,
    vinNumber,
    year,
    make,
    model,
    engine,
    odometer: {
      unit: odometerUnit,
      reading: odometerReading,
      readAt: odometerReadAt,
    } = {},
  } = assetInfo;

  return (
    <Widget className="AssetsWidget" expandKey="assets">
      <Widget.Header>
        <FormattedMessage {...messages.title} />
      </Widget.Header>
      <Container>
        <Row>
          <Column>
            <Container>
              <Widget.SubHeader>
                <FormattedMessage
                  {...messages.unit}
                />
                &nbsp;&nbsp;
                <Link to={`/assets/${id}`} modifiers={['hoverCaret']}>
                  <span className="value">
                    {getOutputText(unitNumber)}
                  </span>
                </Link>
              </Widget.SubHeader>
            </Container>
            <Container>
              <Widget.Table>
                <tbody>
                  <Widget.TableRow>
                    <th>
                      <FormattedMessage {...messages.vin} />
                    </th>
                    <td>
                      {getOutputText(vinNumber)}
                    </td>
                  </Widget.TableRow>
                  <Widget.TableRow modifiers={['topGap']}>
                    <th>
                      <FormattedMessage {...messages.year} />
                    </th>
                    <td>
                      {getOutputText(year)}
                    </td>
                  </Widget.TableRow>
                  <Widget.TableRow modifiers={['topGap']}>
                    <th>
                      <FormattedMessage {...messages.make} />
                    </th>
                    <td>
                      {getOutputText(make)}
                    </td>
                  </Widget.TableRow>
                  <Widget.TableRow modifiers={['topGap']}>
                    <th>
                      <FormattedMessage {...messages.model} />
                    </th>
                    <td>
                      {getOutputText(model)}
                    </td>
                  </Widget.TableRow>
                  <Widget.TableRow modifiers={['topGap']}>
                    <th>
                      <FormattedMessage {...messages.engine} />
                    </th>
                    <td>
                      {getOutputText(engine)}
                    </td>
                  </Widget.TableRow>
                  <Widget.TableRow modifiers={['topGap']}>
                    <th>
                      <FormattedMessage {...messages.odometer} />
                    </th>
                    <td>
                      {
                        getOutputText(
                          getOdometerText(
                            odometerReading,
                            odometerUnit,
                            odometerReadAt,
                          ),
                        )
                      }
                    </td>
                  </Widget.TableRow>
                </tbody>
              </Widget.Table>
            </Container>
          </Column>
          <Column>
            <Status statuses={getStatuses(assetInfo)} />
          </Column>
        </Row>
      </Container>
    </Widget>
  );
}

AssetsWidget.propTypes = {
  assetInfo: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
};

export default withConnectedData(AssetsWidget);
