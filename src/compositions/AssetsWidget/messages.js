import { defineMessages } from 'react-intl';

const messages = defineMessages({
  campaigns: {
    id: 'compositions.AssetsWidget.status.campaigns',
    defaultMessage: 'Campaigns',
  },
  engine: {
    id: 'compositions.AssetsWidget.data.engine',
    defaultMessage: 'Engine',
  },
  make: {
    id: 'compositions.AssetsWidget.data.make',
    defaultMessage: 'Make',
  },
  model: {
    id: 'compositions.AssetsWidget.data.model',
    defaultMessage: 'Model',
  },
  odometer: {
    id: 'compositions.AssetsWidget.data.odometer',
    defaultMessage: 'Odometer',
  },
  pmStatus: {
    id: 'compositions.AssetsWidget.status.pm',
    defaultMessage: 'PM Status',
  },
  serial: {
    id: 'compositions.AssetsWidget.subtitle.serial',
    defaultMessage: 'Serial',
  },
  title: {
    id: 'compositions.AssetsWidget.header.title',
    defaultMessage: 'Asset',
  },
  unit: {
    id: 'compositions.AssetsWidget.subtitle.unit',
    defaultMessage: 'Unit',
  },
  warranty: {
    id: 'compositions.AssetsWidget.status.warranty',
    defaultMessage: 'Warranty',
  },
  vin: {
    id: 'compositions.AssetsWidget.data.vin',
    defaultMessage: 'VIN',
  },
  year: {
    id: 'compositions.AssetsWidget.data.year',
    defaultMessage: 'Year',
  },
  pmStatusTooltipGreen: {
    id: 'compositions.AssetsWidget.status.pm.tooltips.green',
    defaultMessage: 'Current',
  },
  pmStatusTooltipYellow: {
    id: 'compositions.AssetsWidget.status.pm.tooltips.yellow',
    defaultMessage: 'Due',
  },
  pmStatusTooltipRed: {
    id: 'compositions.AssetsWidget.status.pm.tooltips.red',
    defaultMessage: 'Overdue',
  },
  warrantyTooltipGreen: {
    id: 'compositions.AssetsWidget.status.warranty.tooltips.green',
    defaultMessage: 'Fully Covered',
  },
  warrantyTooltipYellow: {
    id: 'compositions.AssetsWidget.status.warranty.tooltips.yellow',
    defaultMessage: 'Partially Covered',
  },
  warrantyTooltipRed: {
    id: 'compositions.AssetsWidget.status.warranty.tooltips.red',
    defaultMessage: 'Expired',
  },
  warrantyTooltipGrey: {
    id: 'compositions.AssetsWidget.status.warranty.tooltips.grey',
    defaultMessage: 'No Coverage',
  },
  campaignsTooltipGreen: {
    id: 'compositions.AssetsWidget.status.campaigns.tooltips.green',
    defaultMessage: 'Current',
  },
  campaignsTooltipRed: {
    id: 'compositions.AssetsWidget.status.campaigns.tooltips.red',
    defaultMessage: 'Campaign Due',
  },
});


export default messages;
