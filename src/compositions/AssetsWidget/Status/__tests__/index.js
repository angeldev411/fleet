import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import StatusLabel from 'components/StatusLabel';

import Status from '../index';

const defaultStatuses = [
  {
    value: 'status1',
    tooltip: { id: 'tooltip1', defaultMessage: 'ToolTip1' },
    label: { id: 'label1', defaultMessage: 'Default Message1' },
  },
  {
    value: 'status2',
    tooltip: { id: 'tooltip2', defaultMessage: 'ToolTip2' },
    label: { id: 'label2', defaultMessage: 'Default Message2' },
  },
  {
    value: 'status3',
    tooltip: { id: 'tooltip3', defaultMessage: 'ToolTip3' },
    label: { id: 'label3', defaultMessage: 'Default Message3' },
  },
];

const renderComponent = (statuses = defaultStatuses) => shallow(<Status statuses={statuses} />);

test('Renders the exact number of StatusLabel', () => {
  const component = renderComponent();
  expect(component.find(StatusLabel).length).toEqual(defaultStatuses.length);
});

test('Renders the StatusLabel with the proper props', () => {
  const component = renderComponent();
  const firstStatusLabel = component.find(StatusLabel).first();
  expect(firstStatusLabel).toHaveProps({
    color: defaultStatuses[0].value,
    label: defaultStatuses[0].label,
    tooltip: defaultStatuses[0].tooltip,
  });
});

test('Renders the StatusLabel without tooltip', () => {
  const statuses = [
    {
      value: 'status5',
      label: { id: 'label-5', defaultMessage: 'Default Message5' },
    },
  ];
  const component = renderComponent(statuses);
  const firstStatusLabel = component.find(StatusLabel).first();
  expect(firstStatusLabel).toHaveProps({
    color: statuses[0].value,
    label: statuses[0].label,
    tooltip: undefined,
  });
});

// TODO: Below tests don't work because SeverityLabel component is passed as a prop to Popover
// If Popover is removed (it's not there for demo purpose), this test is available again.

/*
test('Renders the status colors correctly', () => {
  const component = renderComponent();
  statuses.map((status, index) => {
    const statusLabel = component.find('StatusLabel').at(index);
    return expect(statusLabel).toHaveProp('color', statuses[index].value);
  });
});

test('Renders the status labels correctly', () => {
  const component = renderComponent();
  statuses.map((status, index) => {
    const statusLabel = component.find('StatusLabel').at(index);
    return expect(statusLabel).toHaveProp('label', statuses[index].label);
  });
});
*/
