import React from 'react';
import PropTypes from 'prop-types';
import { messageDescriptorPropTypes } from 'react-intl';

import StatusLabel from 'components/StatusLabel';
import {
  Container,
  Row,
} from 'styled-components-reactive-grid';

function Status({ statuses }) {
  return (
    <Container>
      {
        statuses.map(status => (
          <Row key={status.label.id}>
            <StatusLabel
              color={status.value}
              label={status.label}
              tooltip={status.tooltip}
              showIcon
            />
          </Row>
        ))
      }
    </Container>
  );
}

Status.propTypes = {
  statuses: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.shape({
      id: PropTypes.string.isRequired,
      defaultMessage: PropTypes.string.isRequired,
    }),
    value: PropTypes.string,
    tooltip: PropTypes.shape(messageDescriptorPropTypes),
  })).isRequired,
};

export default Status;
