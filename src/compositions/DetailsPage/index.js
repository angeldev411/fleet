import PropTypes from 'utils/prop-types';
import React, { Component } from 'react';
import { Element } from 'react-scroll';
import getSlug from 'speakingurl';
import { compact, throttle } from 'lodash';
import {
  Container,
  Row,
  Column,
} from 'styled-components-reactive-grid';

import { getScrollTop } from 'utils/document';

import Page from 'elements/Page';
import FloatingHeader from 'elements/FloatingHeader';

import withConnectedData from './Container';

// minimum length to scroll before the floating header is shown/hidden
const FLOATING_HEADER_SCROLL_DOWN_LENGTH = 100;
const FLOATING_HEADER_SCROLL_UP_LENGTH = 500;
const SCROLL_DEBOUNCE_TIME_MS = 250;

export class DetailsPage extends Component {
  static propTypes = {
    configKey: PropTypes.string.isRequired,
    leftNavExpanded: PropTypes.bool.isRequired,
    mappedPageConfig: PropTypes.shape({
      fixed: PropTypes.registeredCompositions,
      top: PropTypes.registeredCompositions,
      quick_action_items: PropTypes.shape({
        left: PropTypes.registeredCompositions,
        right: PropTypes.registeredCompositions,
      }).isRequired,
      left: PropTypes.registeredCompositions,
      right: PropTypes.registeredCompositions,
    }).isRequired,
  };

  state = {
    floatingHeaderVisible: false,
    currentScrollPosition: 0,
    scrollingDown: true, // if this state is false, it means scrolling up
    startScrollPosition: 0,
  };

  componentWillMount() {
    this.scrollListener = throttle(this.scrollHandler, SCROLL_DEBOUNCE_TIME_MS);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.scrollListener);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollListener);
  }

  buildComponents = config =>
    config.map(({ name, composition: Composition }) => (
      <Row key={name}>
        <Column modifiers={['col']}>
          <Element name={name}>
            <Composition />
          </Element>
        </Column>
      </Row>
    ));

  buildQuickActionItems = config => (
    <Row>
      <Column modifiers={['col']}>
        <Container>
          <Row modifiers={['start']}>
            {config.left.map(({ name, composition: Composition }) => (
              <Column key={name}>
                <Composition />
              </Column>
            ))}
          </Row>
        </Container>
      </Column>
      <Column modifiers={['col']}>
        <Container>
          <Row modifiers={['end']}>
            {config.right.map(({ name, composition: Composition }) => (
              <Column key={name}>
                <Composition />
              </Column>
            ))}
          </Row>
        </Container>
      </Column>
    </Row>
  );

  buildHeaderContent = mappedPageConfig => (
    <div>
      <Container className="fixed">
        {this.buildComponents(mappedPageConfig.fixed)}
      </Container>
      <Container className="quick-action-items">
        {this.buildQuickActionItems(mappedPageConfig.quick_action_items)}
      </Container>
    </div>
  );

  scrollHandler = () => {
    const scrollTop = getScrollTop(document);

    // update the scroll states
    const scrollingDown = scrollTop > this.state.currentScrollPosition;
    let startScrollPosition = this.state.startScrollPosition;
    if (scrollingDown !== this.state.scrollingDown) {
      startScrollPosition = scrollTop;
    }
    this.setState({
      currentScrollPosition: scrollTop,
      scrollingDown,
      startScrollPosition,
    });

    // set the header visibility states
    let floatingHeaderVisible = this.state.floatingHeaderVisible;
    if (scrollTop === 0) {
      floatingHeaderVisible = false;
    } else if (
      scrollingDown && scrollTop - startScrollPosition > FLOATING_HEADER_SCROLL_DOWN_LENGTH
    ) {
      floatingHeaderVisible = false;
    } else if (
      !scrollingDown && startScrollPosition - scrollTop > FLOATING_HEADER_SCROLL_UP_LENGTH
    ) {
      floatingHeaderVisible = true;
    }
    this.setState({
      floatingHeaderVisible,
    });
  }

  render() {
    const { floatingHeaderVisible, currentScrollPosition } = this.state;
    const { configKey, leftNavExpanded, mappedPageConfig } = this.props;

    const pageKey = getSlug(configKey);
    const headerFloatingModifier = floatingHeaderVisible && 'visible';
    const leftNavExpandedModifier = leftNavExpanded && 'leftNavExpanded';
    const noTransitionModifier = currentScrollPosition === 0 && 'noTransition';
    const headerContent = this.buildHeaderContent(mappedPageConfig);

    return (
      <Page id={pageKey}>
        <FloatingHeader
          modifiers={compact([
            headerFloatingModifier,
            leftNavExpandedModifier,
            noTransitionModifier,
          ])}
        >
          {headerContent}
        </FloatingHeader>
        {headerContent}
        <Container className="top">
          {this.buildComponents(mappedPageConfig.top)}
        </Container>
        <Container modifiers={['fluid']}>
          <Row>
            <Column modifiers={['col_8']}>
              <Container className="left" modifiers={['fluid']}>
                {this.buildComponents(mappedPageConfig.left)}
              </Container>
            </Column>
            <Column modifiers={['col_4']}>
              <Container className="right" modifiers={['fluid']}>
                {this.buildComponents(mappedPageConfig.right)}
              </Container>
            </Column>
          </Row>
        </Container>
      </Page>
    );
  }
}

export default withConnectedData(DetailsPage);
