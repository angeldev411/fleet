import { merge } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { DetailsPage } from '../index';

const defaultProps = {
  configKey: 'TEST',
  leftNavExpanded: false,
  mappedPageConfig: {
    fixed: [],
    top: [],
    quick_action_items: {
      left: [],
      right: [],
    },
    left: [],
    right: [],
  },
  pageConfig: {
    fixed: [],
    top: [],
    quick_action_items: {
      left: [],
      right: [],
    },
    left: [],
    right: [],
  },
};

const TestComposition = () => <div />;
const TestComposition2 = () => <section />;

function shallowRender(props = defaultProps) {
  return shallow(<DetailsPage {...props} />);
}

function buildTestDocument(scrollTop) {
  return {
    documentElement: { scrollTop },
    body: { scrollTop },
  };
}

test('renders a component with expected id', () => {
  const component = shallowRender();
  expect(component).toHaveProp('id', 'test');
});

test('renders expected components into the fixed container', () => {
  const testProps = merge(
    defaultProps,
    {
      mappedPageConfig: {
        fixed: [{ name: 'TEST_COMPOSITION', composition: TestComposition }],
      },
    },
  );
  const component = shallowRender(testProps);
  const container = component.find('Container.fixed');
  expect(container).toContain('TestComposition');
});

test('renders expected components into the quick action items container', () => {
  const testProps = merge(
    defaultProps,
    {
      mappedPageConfig: {
        quick_action_items: {
          left: [{ name: 'TEST_COMPOSITION', composition: TestComposition }],
          right: [{ name: 'TEST_COMPOSITION_2', composition: TestComposition2 }],
        },
      },
    },
  );
  const component = shallowRender(testProps);
  const container = component.find('Container.quick-action-items');
  expect(container).toContain('TestComposition');
  expect(container).toContain('TestComposition2');
});

test('renders expected components into the top container', () => {
  const testProps = merge(
    defaultProps,
    {
      mappedPageConfig: {
        top: [{ name: 'TEST_COMPOSITION', composition: TestComposition }],
      },
    },
  );
  const component = shallowRender(testProps);
  const container = component.find('Container.top');
  expect(container).toContain('TestComposition');
});

test('renders expected components into the left container', () => {
  const testProps = merge(
    defaultProps,
    {
      mappedPageConfig: {
        left: [{ name: 'TEST_COMPOSITION', composition: TestComposition }],
      },
    },
  );
  const component = shallowRender(testProps);
  const container = component.find('Container.left');
  expect(container).toContain('TestComposition');
});

test('renders expected components into the right container', () => {
  const testProps = merge(
    defaultProps,
    {
      mappedPageConfig: {
        right: [{ name: 'TEST_COMPOSITION', composition: TestComposition }],
      },
    },
  );
  const component = shallowRender(testProps);
  const container = component.find('Container.right');
  expect(container).toContain('TestComposition');
});

// -------------------------- Sticky header --------------------------
test('scrollHandler sets currentScrollPosition state with the current scrollTop value', () => {
  const previousDocument = global.document;
  global.document = buildTestDocument(300);

  const component = shallowRender();
  const instance = component.instance();
  instance.scrollHandler();
  expect(component).toHaveState({ currentScrollPosition: 300 });

  global.document = previousDocument; // put back the previous document value
});

test('scrollHandler sets the current scrolling direction from next scrollTop value', () => {
  const previousDocument = global.document;
  global.document = buildTestDocument(299);

  const component = shallowRender();
  const instance = component.instance();
  component.setState({ currentScrollPosition: 300 });
  instance.scrollHandler();
  expect(component).toHaveState({ scrollingDown: false });

  global.document = previousDocument;
});

test('scrollHandler updates startScrollPosition when scroll direction changes', () => {
  const previousDocument = global.document;
  global.document = buildTestDocument(299);

  const component = shallowRender();
  const instance = component.instance();
  component.setState({
    currentScrollPosition: 300,
    scrollingDown: true,
  });
  instance.scrollHandler();
  expect(component).toHaveState({ startScrollPosition: 299 });

  global.document = previousDocument;
});

test('scrollHandler hides floating header if scrollTop is 0', () => {
  const previousDocument = global.document;
  global.document = buildTestDocument(0);

  const component = shallowRender();
  const instance = component.instance();
  component.setState({ floatingHeaderVisible: true });
  instance.scrollHandler();
  expect(component).toHaveState({ floatingHeaderVisible: false });

  global.document = previousDocument;
});

test('scrollHandler hides floating header if scrolled down continuously for >100 pixels', () => {
  const previousDocument = global.document;
  global.document = buildTestDocument(301);

  const component = shallowRender();
  const instance = component.instance();
  component.setState({
    currentScrollPosition: 300,
    floatingHeaderVisible: true,
    scrollingDown: true,
    startScrollPosition: 200,
  });
  instance.scrollHandler();
  expect(component).toHaveState({ floatingHeaderVisible: false });

  global.document = previousDocument;
});

test('scrollHandler does not hide floating header if NOT scrolled down continuously for >100 pixels', () => {
  const previousDocument = global.document;
  global.document = buildTestDocument(299);

  const component = shallowRender();
  const instance = component.instance();
  component.setState({
    currentScrollPosition: 298,
    floatingHeaderVisible: true,
    scrollingDown: true,
    startScrollPosition: 200,
  });
  instance.scrollHandler();
  expect(component).toHaveState({ floatingHeaderVisible: true });

  global.document = previousDocument;
});

test('scrollHandler shows floating header if scrolled up continuously for >500 pixels', () => {
  const previousDocument = global.document;
  global.document = buildTestDocument(99);

  const component = shallowRender();
  const instance = component.instance();
  component.setState({
    currentScrollPosition: 100,
    floatingHeaderVisible: false,
    scrollingDown: false, // it means scrolling up
    startScrollPosition: 600,
  });
  instance.scrollHandler();
  expect(component).toHaveState({ floatingHeaderVisible: true });

  global.document = previousDocument;
});

test('scrollHandler does not show floating header if NOT scrolled up continuously for >500 pixels', () => {
  const previousDocument = global.document;
  global.document = buildTestDocument(101);

  const component = shallowRender();
  const instance = component.instance();
  component.setState({
    currentScrollPosition: 102,
    floatingHeaderVisible: false,
    scrollingDown: false,
    startScrollPosition: 600,
  });
  instance.scrollHandler();
  expect(component).toHaveState({ floatingHeaderVisible: false });

  global.document = previousDocument;
});

test('renders floating header with `fixed` and `quick-action-items` content', () => {
  const component = shallowRender();
  expect(component).toContain('FloatingHeader');
  expect(component.find('FloatingHeader')).toContain('Container.fixed');
  expect(component.find('FloatingHeader')).toContain('Container.quick-action-items');
});

test('renders floating header with correct visibility & leftNavExpanded modifiers', () => {
  const component = shallowRender({ ...defaultProps, leftNavExpanded: true });
  component.setState({
    floatingHeaderVisible: true,
  });
  const floatingHeader = component.find('FloatingHeader');
  expect(floatingHeader.props().modifiers).toInclude('visible');
  expect(floatingHeader.props().modifiers).toInclude('leftNavExpanded');
});

test('floating header does not have transition animation if scrollTop = 0', () => {
  const component = shallowRender();
  component.setState({ currentScrollPosition: 0 });
  expect(component.find('FloatingHeader').props().modifiers).toInclude('noTransition');
});
