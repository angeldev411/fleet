import PropTypes from 'utils/prop-types';
import React from 'react';
import { connect } from 'react-redux';

import { setCurrentPageConfigKey } from 'redux/pageConfigs/actions';
import { makeGetPageConfigByKey } from 'redux/pageConfigs/selectors';

import { leftNavExpandedSelector } from 'redux/ui/selectors';

import immutableToJS from 'utils/immutableToJS';
import mapPageConfigToRegistry from 'utils/mapPageConfigToRegistry';

import Component from './Component';

/* istanbul ignore next */
const withConnectedData = (DetailsPage) => {
  function WithConnectedData(props) {
    const { configKey, pageConfig } = props;
    const mappedPageConfig = mapPageConfigToRegistry(pageConfig, configKey);

    return (
      <Component
        {...props}
        mappedPageConfig={mappedPageConfig}
        ChildComponent={DetailsPage}
      />
    );
  }

  WithConnectedData.propTypes = {
    configKey: PropTypes.string.isRequired,
    pageConfig: PropTypes.shape({
      data: PropTypes.string,
      fixed: PropTypes.compositionConfig,
      top: PropTypes.compositionConfig,
      quick_action_items: PropTypes.shape({
        left: PropTypes.compositionConfig,
        right: PropTypes.compositionConfig,
      }).isRequired,
      left: PropTypes.compositionConfig,
      right: PropTypes.compositionConfig,
    }).isRequired,
  };

  function mapStateToProps() {
    const getPageConfigByKey = makeGetPageConfigByKey();

    return (state, props) => ({
      leftNavExpanded: leftNavExpandedSelector(state),
      pageConfig: getPageConfigByKey(state, props.configKey),
    });
  }

  function mapDispatchToProps(dispatch) {
    return {
      setCurrentPageConfigKey: configKey => dispatch(setCurrentPageConfigKey(configKey)),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(
    immutableToJS(WithConnectedData),
  );
};

export default withConnectedData;
