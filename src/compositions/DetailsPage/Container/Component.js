import PropTypes from 'utils/prop-types';
import React, { Component } from 'react';

class Container extends Component {
  static propTypes = {
    ChildComponent: PropTypes.func.isRequired,
    configKey: PropTypes.string.isRequired,
    mappedPageConfig: PropTypes.shape({
      fixed: PropTypes.registeredCompositions,
      top: PropTypes.registeredCompositions,
      quick_action_items: PropTypes.shape({
        left: PropTypes.registeredCompositions,
        right: PropTypes.registeredCompositions,
      }).isRequired,
      left: PropTypes.registeredCompositions,
      right: PropTypes.registeredCompositions,
    }).isRequired,
    pageConfig: PropTypes.shape({
      data: PropTypes.string,
      fixed: PropTypes.compositionConfig,
      top: PropTypes.compositionConfig,
      quick_action_items: PropTypes.shape({
        left: PropTypes.compositionConfig,
        right: PropTypes.compositionConfig,
      }).isRequired,
      left: PropTypes.compositionConfig,
      right: PropTypes.compositionConfig,
    }).isRequired,
    setCurrentPageConfigKey: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.DetailsPageWithData = this.buildDetailsPageWithData(this.props);
  }

  componentDidMount() {
    const {
      configKey,
      setCurrentPageConfigKey,
    } = this.props;
    setCurrentPageConfigKey(configKey);
  }

  buildDetailsPageWithData = ({ ChildComponent, mappedPageConfig }) => {
    const { data } = mappedPageConfig;
    return data.hoc(ChildComponent);
  }

  render() {
    const { DetailsPageWithData } = this;

    const {
      ChildComponent,
      setCurrentPageConfigKey,
      ...rest
    } = this.props;

    return (
      <DetailsPageWithData
        componentProps={{ ...rest }}
      />
    );
  }
}

export default Container;
