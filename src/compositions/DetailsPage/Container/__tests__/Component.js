import { merge, noop } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import Container from '../Component';

const ChildComponent = p => <div {...p} />;

const defaultProps = {
  ChildComponent,
  configKey: 'TEST_CONFIG_KEY',
  pageConfig: {
    data: 'TEST_DATA_HOC',
    quick_action_items: {},
  },
  mappedPageConfig: {
    data: { hoc: c => c, name: 'TEST_DATA_HOC' },
    fixed: [],
    quick_action_items: { left: [], right: [] },
    top: [],
    left: [],
    right: [],
  },
  setCurrentPageConfigKey: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<Container {...props} />);
}

test('renders the ChildComponent', () => {
  const component = shallowRender();
  expect(component).toContain(ChildComponent);
});

test('calls setCurrentPageConfigKey with configKey after mounting', () => {
  const setCurrentPageConfigKey = createSpy();
  const testProps = merge(defaultProps, { setCurrentPageConfigKey });
  const component = shallowRender(testProps);
  const instance = component.instance();
  instance.componentDidMount();
  expect(setCurrentPageConfigKey).toHaveBeenCalledWith(testProps.configKey);
});

test('buildDetailsPageWithData returns the ChildComponent wrapped with the data hoc', () => {
  const component = shallowRender();
  const instance = component.instance();
  const hoc = createSpy();
  const testProps = merge(defaultProps, { mappedPageConfig: { data: { hoc } } });
  instance.buildDetailsPageWithData(testProps);
  expect(hoc).toHaveBeenCalledWith(ChildComponent);
});
