import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  title: {
    id: 'compositions.ComplaintWidget.title',
    defaultMessage: 'Complaint',
  },
  undefinedComplaint: {
    id: 'compositions.ComplaintWidget.undefined.complaint',
    defaultMessage: 'No complaints yet',
  },
  undefinedComplaintTitle: {
    id: 'compositions.ComplaintWidget.undefined.complaintTitle',
    defaultMessage: 'Code - Description',
  },
});

export default formattedMessages;
