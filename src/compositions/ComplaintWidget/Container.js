import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  currentCaseSelector,
} from 'redux/cases/selectors';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    caseInfo: PropTypes.shape({
      complaint: PropTypes.shape({
        code: PropTypes.string,
        description: PropTypes.string,
      }),
      description: PropTypes.string,
    }).isRequired,
  };

  Container.defaultProps = {
    caseInfo: {},
  };

  function mapStateToProps(state) {
    return {
      caseInfo: currentCaseSelector(state),
    };
  }

  return connect(mapStateToProps)(immutableToJS(Container));
};

export default withConnectedData;
