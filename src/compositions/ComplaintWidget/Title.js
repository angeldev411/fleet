import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.text};
  font-size: ${px2rem(14)};
  font-weight: 600;
  height: ${px2rem(19)};
  line-height: ${px2rem(19)};
  margin: 0;
  padding-left: ${px2rem(20)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'Title',
  styled.p,
  styles,
  { themePropTypes },
);
