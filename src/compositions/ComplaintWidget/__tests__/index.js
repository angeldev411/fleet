import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import messages from '../messages';
import { ComplaintWidget } from '../index';

const caseDescription = 'case description';
const complaintCode = '123';
const complaintDescription = 'complaint description';

const defaultProps = {
  description: caseDescription,
  complaint: {
    code: complaintCode,
    description: complaintDescription,
  },
};

function renderComponent(props = defaultProps) {
  return shallow(<ComplaintWidget caseInfo={props} />);
}

test('ComplaintWidget renders the correct title text', () => {
  const titleLink = renderComponent().find('Title Link');
  const titleLinkChildren = titleLink.props().children;
  expect(titleLinkChildren).toContain(complaintCode);
  expect(titleLinkChildren).toContain(complaintDescription);
});

test('ComplaintWidget renders empty value when complaint is `null` value', () => {
  const caseInfo = ({
    complaint: null,
  });
  const component = renderComponent(caseInfo);
  const titleDiv = component.find('Title');
  expect(titleDiv.find('FormattedMessage')).toHaveProps({
    ...messages.undefinedComplaint,
  });
});

test('ComplaintWidget renders the correct detail text', () => {
  const contentText = renderComponent().find('Content').render().text();
  expect(contentText).toEqual(caseDescription);
});
