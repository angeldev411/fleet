import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { getOutputText } from 'utils/widget';

import Widget from 'elements/Widget';
import Link from 'elements/Link';

import withConnectedData from './Container';

import Title from './Title';
import Content from './Content';
import messages from './messages';

export function ComplaintWidget({
  caseInfo: {
    complaint,
    description,
  },
}) {
  const complaintCode = complaint && complaint.code;
  const complaintDescription = complaint && complaint.description;
  const title = complaintCode && complaintDescription &&
    <Link to="#" modifiers={['hoverCaret']}>
      {complaintCode} - {complaintDescription}
    </Link>;

  return (
    <Widget id="complaints" expandKey="complaints">
      <Widget.Header>
        <FormattedMessage {...messages.title} />
      </Widget.Header>
      <Widget.Item>
        <Title>
          {getOutputText(title, messages.undefinedComplaint)}
        </Title>
        <Content>
          {complaintCode && getOutputText(description)}
        </Content>
      </Widget.Item>
    </Widget>
  );
}

ComplaintWidget.propTypes = {
  caseInfo: PropTypes.shape({
    complaint: PropTypes.shape({
      code: PropTypes.string,
      description: PropTypes.string,
    }),
    description: PropTypes.string,
  }).isRequired,
};

ComplaintWidget.defaultProps = {
  caseInfo: {},
};

export default withConnectedData(ComplaintWidget);
