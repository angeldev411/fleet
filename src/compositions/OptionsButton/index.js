import PropTypes from 'prop-types';
import React from 'react';
import { compose, setDisplayName } from 'recompose';

import QuickActionSelector from 'components/QuickActionSelector';

import withConnectedData from './Container';
import { options } from './constants';
import messages from './messages';

export function OptionsButton({ handleActionBarItemClick }) {
  return (
    <QuickActionSelector
      item={{
        id: 'OPTIONS_BUTTON',
        icon: 'caret-down',
        iconPressed: 'caret-up',
        label: messages.title,
        options,
      }}
      onClick={handleActionBarItemClick}
    />
  );
}

OptionsButton.propTypes = {
  handleActionBarItemClick: PropTypes.func.isRequired,
};

export default compose(
  setDisplayName('OptionsButton'),
  withConnectedData,
)(OptionsButton);
