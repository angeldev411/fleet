import PropTypes from 'utils/prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { compose, setDisplayName } from 'recompose';

import immutableToJS from 'utils/immutableToJS';

import { handleActionBarItemClick } from 'redux/ui/actions';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function WithConnectedData(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  WithConnectedData.propTypes = {
    handleActionBarItemClick: PropTypes.func.isRequired,
  };

  function mapDispatchToProps(dispatch) {
    return {
      handleActionBarItemClick: payload => dispatch(handleActionBarItemClick(payload)),
    };
  }

  return compose(
    setDisplayName('OptionsButtonContainer'),
    connect(undefined, mapDispatchToProps),
    immutableToJS,
  )(WithConnectedData);
};

export default withConnectedData;
