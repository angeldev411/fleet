import { compact } from 'lodash';

import oldIEVersion from 'utils/oldIEVersion';

import messages from './messages';

// eslint-disable-next-line import/prefer-default-export
export const options = compact([
  {
    id: 'printPage',
    label: messages.printPage,
  },
  !oldIEVersion() && {
    id: 'viewInFullScreenMode',
    label: messages.viewInFullScreenMode,
  },
]);
