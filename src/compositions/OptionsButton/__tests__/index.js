import { noop } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import { OptionsButton } from '../index';

const defaultProps = {
  handleActionBarItemClick: noop,
};

function shallowRender(props = defaultProps) {
  return shallow(<OptionsButton {...props} />);
}

test('clicking QuickActionSelector calls handleActionBarItemClick', () => {
  const handleActionBarItemClick = createSpy();
  const testProps = {
    ...defaultProps,
    handleActionBarItemClick,
  };
  const component = shallowRender(testProps);
  component.find('QuickActionSelector').simulate('click');
  expect(handleActionBarItemClick).toHaveBeenCalled();
});
