import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'compositions.OptionsButton.title',
    defaultMessage: 'Options',
  },
  printPage: {
    id: 'compositions.OptionsButton.printPage',
    defaultMessage: 'Print Page',
  },
  viewInFullScreenMode: {
    id: 'compositions.OptionsButton.viewInFullScreenMode',
    defaultMessage: 'View In Full-Screen Mode',
  },
});

export default messages;
