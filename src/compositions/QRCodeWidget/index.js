import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Widget from 'elements/Widget';
import Img from 'elements/Img';

import withConnectedData from './Container';
import QRCodeWrapper from './QRCodeWrapper';
import messages from './messages';

export function QRCodeWidget({ assetInfo }) {
  const { qrCodeUrl } = assetInfo;

  return (
    <Widget expandKey="qr-code">
      <Widget.Header>
        <FormattedMessage {...messages.title} />
      </Widget.Header>
      <Widget.Item>
        <QRCodeWrapper>
          {qrCodeUrl && <Img src={qrCodeUrl} alt="QRCode" />}
        </QRCodeWrapper>
      </Widget.Item>
    </Widget>
  );
}

QRCodeWidget.propTypes = {
  assetInfo: PropTypes.shape({
    qrCodeUrl: PropTypes.string,
  }).isRequired,
};

export default withConnectedData(QRCodeWidget);
