import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { QRCodeWidget } from '../index';
import messages from '../messages';

const QRCodeURL = 'qrcode';

const defaultProps = {
  assetInfo: {
    qrCodeUrl: QRCodeURL,
  },
};

function buildWidget(props = defaultProps) {
  return shallow(<QRCodeWidget {...props} />,
  );
}

test('QRCodeWidget renders a widget with "qr-code" expand key', () => {
  const widget = buildWidget();
  expect(widget.find('Widget')).toHaveProp('expandKey', 'qr-code');
});

test('QRCodeWidget renders a Header with title', () => {
  const widget = buildWidget();
  const widgetHeader = widget.find('Header').first();

  expect(widgetHeader.contains(<FormattedMessage {...messages.title} />)).toEqual(true);
});

test('render `Img` with the expected qrcodeURL', () => {
  const widget = buildWidget();
  const Img = widget.find('Img').first();

  expect(Img).toHaveProp('src', QRCodeURL);
});
