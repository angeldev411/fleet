import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'compositions.QRCodeWidget.header.title',
    defaultMessage: 'QR Code',
  },
});

export default messages;
