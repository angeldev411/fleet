import styled from 'styled-components';
import { px2rem } from 'decisiv-ui-utils';

const QRCodeWrapper = styled.div`
  padding: ${px2rem(20)} 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default QRCodeWrapper;
