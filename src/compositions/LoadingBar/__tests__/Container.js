import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import { LoadingBarContainer } from '../Container';

const defaultProps = {
  loading: false,
  loadingStarted: false,
  resetLoadingStarted: noop,
};

const defaultState = {
  started: false,
  finished: false,
};

function renderComponent(props = defaultProps) {
  return shallow(<LoadingBarContainer {...props} />);
}

test('renders and passed down `started` and `finished` states to LoadingBarComponent', () => {
  const component = renderComponent();
  const testState = {
    started: true,
    finished: false,
  };
  component.setState(testState);
  expect(component).toBeA('LoadingBarComponent');
  expect(component).toHaveProps(testState);
});

// -------------------- componentWillReceiveProps --------------------

test('on receiving props, if loadingStarted is true, resets started and finished states, and after a delay, sets started to true.', () => {
  const resetLoadingStartedSpy = createSpy();
  const testProps = {
    ...defaultProps,
    loadingStarted: true,
    resetLoadingStarted: resetLoadingStartedSpy,
  };
  const component = renderComponent();
  const instance = component.instance();
  component.setState({
    started: true,
    finished: true,
  });
  instance.componentWillReceiveProps(testProps);
  expect(component.state()).toEqual({
    started: false,
    finished: false,
  });
  setTimeout(() => {
    expect(component.state().started).toBe(true);
  }, 100); // to be safe
});

test('on receiving props, if loadingStarted is true, resets started and finished states, and after a delay, sets started to true.', () => {
  const testProps = {
    ...defaultProps,
    loadingStarted: true,
  };
  const component = renderComponent();
  const instance = component.instance();
  component.setState({
    started: true,
    finished: true,
  });
  instance.componentWillReceiveProps(testProps);
  expect(component.state()).toEqual({
    started: false,
    finished: false,
  });
  setTimeout(() => {
    expect(component.state().started).toBe(true);
  }, 100); // to be safe
});

test('on receiving props, if loadingStarted is true, calls resetLoadingStarted prop function', () => {
  const resetLoadingStartedSpy = createSpy();
  const testProps = {
    ...defaultProps,
    resetLoadingStarted: resetLoadingStartedSpy,
  };
  const nextProps = {
    ...defaultProps,
    loadingStarted: true,
  };
  const component = renderComponent(testProps);
  const instance = component.instance();
  instance.componentWillReceiveProps(nextProps);
  expect(resetLoadingStartedSpy).toHaveBeenCalled();
});

test('on receiving props, if loading has stopped, sets started to false and finished true', () => {
  const prevProps = {
    ...defaultProps,
    loading: true,
  };
  const nextProps = {
    ...defaultProps,
    loading: false,
  };
  const component = renderComponent(prevProps);
  const instance = component.instance();
  component.setState(defaultState);
  instance.componentWillReceiveProps(nextProps);
  expect(component.state()).toEqual({
    started: false,
    finished: true,
  });
});

test('on receiving props, if previous loading has stopped and current loading has started, loading should not be finished ', () => {
  const prevProps = {
    ...defaultProps,
    loading: false,
  };
  const nextProps = {
    ...defaultProps,
    loading: true,
  };
  const testState = {
    started: true,
    finished: false,
  };
  const component = renderComponent(prevProps);
  const instance = component.instance();

  component.setState(testState);
  instance.componentWillReceiveProps(nextProps);
  expect(component.state()).toEqual(testState);
});

test('on receiving props, if previous loading has stopped and current loading has started, loading should be finished ', () => {
  const prevProps = {
    ...defaultProps,
    loading: true,
  };
  const nextProps = {
    ...defaultProps,
    loading: false,
  };
  const testState = {
    started: false,
    finished: true,
  };
  const component = renderComponent(prevProps);
  const instance = component.instance();
  component.setState(testState);
  instance.componentWillReceiveProps(nextProps);
  expect(component.state()).toEqual(testState);
});
