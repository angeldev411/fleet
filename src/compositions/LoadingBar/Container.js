import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { delay } from 'lodash';

import {
  setLoadingStarted,
} from 'redux/app/actions';
import {
  loadingStartedSelector,
} from 'redux/app/selectors';

import {
  loadingSelector,
} from 'redux/combinedSelectors';

import LoadingBarComponent from './Component';

export class LoadingBarContainer extends Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    loadingStarted: PropTypes.bool.isRequired,
    resetLoadingStarted: PropTypes.func.isRequired,
  };

  static defaultProps = {
    loadingStarted: false, // required for testing
  };

  state = {
    started: false,
    finished: false,
  };

  componentWillReceiveProps(nextProps) {
    const { loading, loadingStarted } = nextProps;

    if (loadingStarted) {
      // we need to reset `started` and `finished` first in order to start CSS transition
      this.setState({
        started: false,
        finished: false,
      });

      delay(() => {
        this.setState({
          started: true,
        });
      }, 50);

      this.props.resetLoadingStarted();
    }

    // if the previous loading has stopped and current loading has started, `started` should be
    // settled by the previous loadingStarted.
    if (!this.props.loading && loading) {
      this.setState({
        started: !this.props.loadingStarted,
        finished: false,
      });
    }

    // if the previous loading has started, `finished` should be settled by current loading.
    if (this.props.loading) {
      this.setState({
        started: false,
        finished: !loading,
      });
    }
  }

  render() {
    const { started, finished } = this.state;
    return (
      <LoadingBarComponent
        started={started}
        finished={finished}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: loadingSelector(state),
    loadingStarted: loadingStartedSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    resetLoadingStarted: () => dispatch(setLoadingStarted(false)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoadingBarContainer);
