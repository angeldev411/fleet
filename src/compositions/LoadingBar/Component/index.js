import React from 'react';
import PropTypes from 'prop-types';

import LoadingBarContent from './LoadingBarContent';
import LoadingBarWrapper from './LoadingBarWrapper';

function LoadingBarComponent({ started, finished }) {
  return (
    <LoadingBarWrapper>
      <LoadingBarContent
        started={started}
        finished={finished}
      />
    </LoadingBarWrapper>
  );
}

LoadingBarComponent.propTypes = {
  started: PropTypes.bool.isRequired,
  finished: PropTypes.bool.isRequired,
};

export default LoadingBarComponent;
