import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

export function getWidth({ started, finished }) {
  if (started) {
    return 90;
  } else if (finished) {
    return 100;
  }
  return 0;
}

export function getTransitionDuration({ started, finished }) {
  if (started) {
    return 3;
  } else if (finished) {
    return 0.5;
  }
  return 0;
}

/* istanbul ignore next */
const styles = props => `
  background: ${props.theme.colors.brand.primary};
  height: 100%;
  transition: width ${getTransitionDuration(props)}s linear;
  width: ${getWidth(props)}%;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    brand: PropTypes.shape({
      primary: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

const propTypes = {
  started: PropTypes.bool.isRequired,
  finished: PropTypes.bool.isRequired,
};

export default buildStyledComponent(
  'LoadingBarContent',
  styled.div,
  styles,
  { propTypes, themePropTypes },
);
