import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  background: ${props.theme.colors.base.chrome200};
  bottom: 0;
  height: 6px;
  left: 0;
  position: absolute;
  width: 100%;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome200: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'LoadingBarWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
