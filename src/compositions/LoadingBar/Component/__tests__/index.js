import React from 'react';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import LoadingBarComponent from '../index';

const defaultProps = {
  started: false,
  finished: true,
};

function renderComponent(props = defaultProps) {
  return shallow(<LoadingBarComponent {...props} />);
}

test('renders and passes down the props to LoadingBarContent', () => {
  const component = renderComponent();
  expect(component.find('LoadingBarContent')).toHaveProps({
    started: defaultProps.started,
    finished: defaultProps.finished,
  });
});
