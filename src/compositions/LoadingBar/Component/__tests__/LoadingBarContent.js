import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import { getWidth, getTransitionDuration } from '../LoadingBarContent';

const defaultProps = {
  started: false,
  finished: false,
};

// ----------------------- getWidth -----------------------

test('getWidth returns 0 percent if loading has been reset', () => {
  const width = getWidth(defaultProps);
  expect(width).toEqual(0);
});

test('getWidth returns 90 percent if loading has started', () => {
  const width = getWidth({ started: true });
  expect(width).toEqual(90);
});

test('getWidth returns 100 percent if loading has finished', () => {
  const width = getWidth({ finished: true });
  expect(width).toEqual(100);
});

// ----------------------- getTransitionDuration -----------------------

test('getTransitionDuration returns 0 seconds (instant) if loading has been reset', () => {
  const width = getTransitionDuration(defaultProps);
  expect(width).toEqual(0);
});

test('getTransitionDuration returns 3 seconds if loading has started', () => {
  const width = getTransitionDuration({ started: true });
  expect(width).toEqual(3);
});

test('getTransitionDuration returns 0.5 seconds if loading has finished', () => {
  const width = getTransitionDuration({ finished: true });
  expect(width).toEqual(0.5);
});
