import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  align-items: center;
  background-color: ${props.theme.colors.base.chrome100};
  display: flex;
  font-size: ${px2rem(10)};
  padding: ${px2rem(30)} ${px2rem(84)};

  a {
    margin-left: ${px2rem(6)};
  }
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome100: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'LoginFormFooter',
  styled.div,
  styles,
  { themePropTypes },
);
