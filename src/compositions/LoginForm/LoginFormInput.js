import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  border: 0 solid;
  border-bottom: 2px solid ${props.theme.colors.base.chrome300};
  color: ${props.theme.colors.base.text};
  flex: 1;
  font-size: ${px2rem(14)};
  line-height: ${px2rem(14)};
  padding: ${px2rem(11)} 0;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome300: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'LoginFormInput',
  styled.input,
  styles,
  { themePropTypes },
);
