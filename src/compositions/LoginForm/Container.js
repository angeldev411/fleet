import PropTypes from 'prop-types';
import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';
import { FormattedMessage, injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import { compact } from 'lodash';

import { login, clearLoginRequestError } from 'redux/user/actions';
import {
  loginRequestErrorSelector,
} from 'redux/user/selectors';

import { A } from 'base-components';
import Card from 'elements/Card';
import {
  FormSection,
  FormInlineRadioSection,
  FormRadioGroup,
} from 'elements/Form';
import RectangleButton, { RectangleButtonText } from 'elements/RectangleButton';

import capslockActive from 'utils/capslockActive';

import messages from './messages';
import LoginFormInput from './LoginFormInput';
import LoginButtonWrapper from './LoginButtonWrapper';
import LoginFormFooter from './LoginFormFooter';
import LoginFormWrapper from './LoginFormWrapper';
import PasswordInputWrapper from './PasswordInputWrapper';
import UserInputWrapper from './UserInputWrapper';
import ShowPasswordWrapper from './ShowPasswordWrapper';
import ErrorPopoverWrapper from './ErrorPopoverWrapper';

const oems = [
  { value: 'decisiv', label: messages.decisiv },
  { value: 'mack', label: messages.mack },
  { value: 'volvo', label: messages.volvo },
];

export class LoginFormContainer extends Component {
  static propTypes = {
    loginRequestError: PropTypes.bool.isRequired,
    clearLoginRequestError: PropTypes.func.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func,
    }).isRequired,
    login: PropTypes.func.isRequired,
  };

  static defaultProps = {
    loginRequestError: false,
  };

  state = {
    capslockSpyEnabled: true,
    password: '',
    passwordError: 'passwordShort',
    selectedOem: memoryDB.DECISIV__oem || oems[0].value,
    showPassword: false,
    touched: false,
    username: '',
    usernameError: 'usernameShort',
  };

  buildRadios = () => (
    oems.map(oem => (
      <FormRadioGroup key={oem.value}>
        <input
          type="radio"
          value={oem.value}
          checked={this.state.selectedOem === oem.value}
          onChange={this.updateSelectedOem}
        />
        <FormattedMessage {...oem.label} />
      </FormRadioGroup>
    ))
  );

  submitLoginForm = (event) => {
    const { selectedOem, username, password } = this.state;
    event.preventDefault();
    this.props.login({
      selectedOem,
      username,
      password,
    });
    this.setState({ touched: true });
  };

  disableCapslockSpy = () => {
    const { passwordError } = this.state;
    if (passwordError === 'capslockActive') {
      this.setState({ capslockSpyEnabled: false, passwordError: '' });
    }
  };

  updateSelectedOem = (event) => {
    const { loginRequestError } = this.props;
    if (loginRequestError) this.props.clearLoginRequestError();
    this.setState({ selectedOem: event.target.value });
  };

  updateUsername = (event) => {
    const { loginRequestError } = this.props;
    this.setState({ usernameError: event.target.value.length > 3 ? '' : 'usernameShort' });
    if (loginRequestError) this.props.clearLoginRequestError();
    this.setState({ username: event.target.value });
  };

  keyPressPassword = (event) => {
    const { capslockSpyEnabled } = this.state;
    if (capslockActive(event) && capslockSpyEnabled) {
      this.setState({ passwordError: 'capslockActive' });
    } else if (this.state.passwordError === 'capslockActive') {
      this.setState({ passwordError: '' });
    }
  };

  updatePassword = (event) => {
    const { loginRequestError } = this.props;
    const { passwordError } = this.state;
    if (event.target.value.length < 4 && passwordError !== 'capslockActive') {
      this.setState({ passwordError: 'passwordShort' });
    } else if (passwordError === 'passwordShort') {
      this.setState({ passwordError: '' });
    }
    if (loginRequestError) this.props.clearLoginRequestError();
    this.setState({ password: event.target.value });
  };

  toggleShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };

  render() {
    const { username, password, showPassword, touched, usernameError, passwordError } = this.state;
    const { intl: { formatMessage }, loginRequestError } = this.props;
    const loginDisabled = (usernameError || passwordError) && 'disabled';
    return (
      <Card id="login-form" modifiers={['noPad']}>
        <LoginFormWrapper>
          <form onSubmit={this.submitLoginForm}>
            <FormSection>
              <FormInlineRadioSection>
                {this.buildRadios()}
              </FormInlineRadioSection>
            </FormSection>
            <FormSection>
              <UserInputWrapper>
                <LoginFormInput
                  type="text"
                  name="username"
                  placeholder={formatMessage(messages.emailOrUsername)}
                  value={username}
                  onChange={this.updateUsername}
                />
                {touched && <ErrorPopoverWrapper error={usernameError} />}
              </UserInputWrapper>
            </FormSection>
            <FormSection>
              <PasswordInputWrapper>
                <LoginFormInput
                  type={showPassword ? 'text' : 'password'}
                  name="password"
                  placeholder={formatMessage(messages.passphrase)}
                  value={password}
                  onChange={this.updatePassword}
                  onKeyPress={this.keyPressPassword}
                />
                <ShowPasswordWrapper id="show-password-wraper" onClick={this.toggleShowPassword}>
                  <FontAwesome name={showPassword ? 'eye' : 'eye-slash'} />
                </ShowPasswordWrapper>
                {(touched || passwordError === 'capslockActive') &&
                  <ErrorPopoverWrapper
                    clickOutside={this.disableCapslockSpy}
                    onClickPopover={this.disableCapslockSpy}
                    error={loginRequestError ? 'auth' : passwordError}
                  />}
              </PasswordInputWrapper>
            </FormSection>
            <FormSection>
              <LoginButtonWrapper>
                <RectangleButton
                  id="login-button"
                  modifiers={compact([
                    'blue',
                    loginDisabled,
                    'small',
                    'xtraWide',
                    'hoverBrandBright',
                    'hoverShadow',
                    'hoverUnderline',
                  ])}
                  disabled={loginDisabled}
                  onClick={this.submitLoginForm}
                >
                  <RectangleButtonText modifiers={['uppercase']}>
                    <FormattedMessage {...messages.login} />
                  </RectangleButtonText>
                </RectangleButton>
              </LoginButtonWrapper>
            </FormSection>
          </form>
        </LoginFormWrapper>
        <LoginFormFooter>
          <FormattedMessage {...messages.signInAgreement} />
          <A href="https://www.decisiv.com/product-agreements">
            <FormattedMessage {...messages.termsAndConditions} />
          </A>
        </LoginFormFooter>
      </Card>
    );
  }
}

function mapStateToProps(state) {
  return {
    loginRequestError: loginRequestErrorSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    login: loginOptions => dispatch(login(loginOptions)),
    clearLoginRequestError: () => dispatch(clearLoginRequestError()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(LoginFormContainer));
