import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  display: flex;
  flex: 1;
  position: relative;
  input {
    padding-right: ${px2rem(20)};
  }
`;

export default buildStyledComponent(
  'PasswordInputWrapper',
  styled.div,
  styles,
);
