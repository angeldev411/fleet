import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  font-size: 1.5em;
  margin: 0.5em 0;
  padding: 0 0.5em;
`;

export default buildStyledComponent(
  'LoginFormH1',
  styled.h1,
  styles,
);
