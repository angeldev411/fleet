import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  margin-left: auto;
  transition: opacity 0.3s;
`;

export default buildStyledComponent(
  'LoginButtonWrapper',
  styled.div,
  styles,
);
