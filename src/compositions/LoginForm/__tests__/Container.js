import React from 'react';
import { noop } from 'lodash';
import {
  test,
  expect,
  shallow,
  createSpy,
  spyOn,
} from '__tests__/helpers/test-setup';

import { LoginFormContainer } from '../Container';

const testProps = {
  loginRequestError: false,
  clearLoginRequestError: noop,
  intl: {
    formatMessage: str => str,
  },
  login: noop,
};

function renderForm(props = testProps) {
  return shallow(<LoginFormContainer {...props} />);
}

test('Clicking a radio button updates the selectedOem in the component state', () => {
  const oem = 'decisiv';
  const component = renderForm();
  component
    .find(`input[type="radio"][value="${oem}"]`)
    .simulate('change', { target: { value: oem } });
  expect(component.state().selectedOem).toEqual(oem);
});

test('Updating the username input updates the username in the component state', () => {
  const username = 'test_username';
  const component = renderForm();

  component
    .find('[name="username"]')
    .simulate('change', { target: { value: username } });

  expect(component.state().username).toEqual(username);
});

test('Updating the password input updates the password in the component state', () => {
  const password = 'test_password';
  const component = renderForm();

  component
    .find('[name="password"]')
    .simulate('change', { target: { value: password } });

  expect(component.state().password).toEqual(password);
});

test('Clicking the submit button calls the login prop with the current values in state', () => {
  const state = {
    selectedOem: 'decisiv',
    username: 'test_username',
    password: 'test_password',
  };
  const loginSpy = createSpy();
  const preventDefaultSpy = createSpy();

  const component = renderForm({ ...testProps, login: loginSpy });
  component.setState(state);

  component
    .find('RectangleButton#login-button')
    .simulate('click', { preventDefault: preventDefaultSpy });

  expect(preventDefaultSpy).toHaveBeenCalled();
  expect(loginSpy).toHaveBeenCalledWith(state);
});

test('Clicking the ShowPasswordIcon updates showPassword in the component state', () => {
  // default state of showPassword is false
  const component = renderForm();
  component
    .find('#show-password-wraper')
    .simulate('click');
  expect(component.state().showPassword).toEqual(true);
  component
    .find('#show-password-wraper')
    .simulate('click');
  expect(component.state().showPassword).toEqual(false);
});

test('Type of Password Input Field is password when showPassword is false', () => {
  const component = renderForm();
  component.setState({ showPassword: false });
  expect(component.find('[name="password"]').props().type).toEqual('password');
});

test('Type of Password Input Field is text when showPassword is true', () => {
  const component = renderForm();
  component.setState({ showPassword: true });
  expect(component.find('[name="password"]').props().type).toEqual('text');
});

const event = {
  preventDefault: noop,
  target: {
    value: 'shr',
  },
};

test('submitLoginForm makes state touched as true', () => {
  const component = renderForm();
  component.instance().submitLoginForm(event);
  expect(component.state().touched).toEqual(true);
});

test('when updateSelectedOem is called if loginRequestError is true, clearLoginRequestError is called to make it false', () => {
  const clearLoginRequestError = createSpy();
  const component = renderForm({
    ...testProps,
    loginRequestError: true,
    clearLoginRequestError,
  });
  component.instance().updateSelectedOem(event);
  expect(clearLoginRequestError).toHaveBeenCalled();
});

test('when updateUsername is called if loginRequestError is true, clearLoginRequestError is called to make it false', () => {
  const clearLoginRequestError = createSpy();
  const component = renderForm({
    ...testProps,
    loginRequestError: true,
    clearLoginRequestError,
  });
  component.instance().updateUsername(event);
  expect(clearLoginRequestError).toHaveBeenCalled();
});

test('when updatePassword is called if loginRequestError is true, clearLoginRequestError is called to make it false', () => {
  const clearLoginRequestError = createSpy();
  const component = renderForm({
    ...testProps,
    loginRequestError: true,
    clearLoginRequestError,
  });
  component.instance().updatePassword(event);
  expect(clearLoginRequestError).toHaveBeenCalled();
});

test('when updatePassword is called if passwordError of state is capslockActive, it keeps the state', () => {
  const component = renderForm();
  const instance = component.instance();
  const testPasswordError = 'capslockActive';
  instance.setState({ passwordError: testPasswordError });
  instance.updatePassword(event);
  expect(instance.state.passwordError).toEqual(testPasswordError);
});

test('if username is shorter than 4 characters, usernameError state is set to "usernameShort"', () => {
  const component = renderForm({
    ...testProps,
  });
  component.instance().updateUsername(event);
  expect(component.state().usernameError).toEqual('usernameShort');
});

test('if password is shorter than 4 characters, passwordError state is set to "passwordShort"', () => {
  const component = renderForm();
  component.instance().updatePassword(event);
  expect(component.state().passwordError).toEqual('passwordShort');
});

test('if password is longer than 4 characters and passwordError state is "passwordShort", make it empty', () => {
  const component = renderForm();
  component.setState({ passwordError: 'passwordShort' });
  component.instance().updatePassword({
    ...event,
    target: {
      value: 'longEnoughPassword',
    },
  });
  expect(component.state().passwordError).toEqual('');
});

test('if username has an error, disable submit button', () => {
  const component = renderForm();
  component.setState({ usernameError: 'testError' });
  const disabled = component.find('RectangleButton#login-button').props().disabled;
  expect(disabled).toBeTruthy();
});

test('if password has an error, disable submit button', () => {
  const component = renderForm();
  component.setState({ passwordError: 'testError' });
  const disabled = component.find('RectangleButton#login-button').props().disabled;
  expect(disabled).toBeTruthy();
});

test('if password has an error and it is capslockActive, remove that state when disableCapslockSpy is called', () => {
  const component = renderForm();
  component.setState({ passwordError: 'capslockActive', capslockSpyEnabled: true });
  component.instance().disableCapslockSpy();
  expect(component.state().capslockSpyEnabled).toBe(false);
  expect(component.state().passwordError).toBe('');
});

test('if password has an error and it is not capslockActive, setState is not called', () => {
  const component = renderForm();
  const instance = component.instance();
  component.setState({ passwordError: 'whateverError', capslockSpyEnabled: true });
  instance.disableCapslockSpy();
  expect(component.state().capslockSpyEnabled).toBe(true);
  expect(component.state().passwordError).toBe('whateverError');
});

test('if capslockActive returns true and capslockSpyEnabled set passwordError as capslockActive', () => {
  const component = renderForm();
  component.setState({ capslockSpyEnabled: true });
  const e = {
    key: 'B',
    shiftKey: false,
  };
  component.instance().keyPressPassword(e);
  expect(component.state().passwordError).toEqual('capslockActive');
});

test('if capslockActive returns false and passwordError state is "capslockActive", set passwordError as empty', () => {
  const component = renderForm();
  component.setState({ passwordError: 'capslockActive' });
  const e = {
    key: 'B',
    shiftKey: true,
  };
  component.instance().keyPressPassword(e);
  expect(component.state().passwordError).toEqual('');
});

test('if capslockActive returns false and passwordError state is not "capslockActive", setState is not called.', () => {
  const component = renderForm();
  const instance = component.instance();
  component.setState({ passwordError: 'whateverError' });
  const setStateSpy = spyOn(instance, 'setState');
  const e = {
    key: 'B',
    shiftKey: true,
  };
  instance.keyPressPassword(e);
  expect(setStateSpy).toNotHaveBeenCalled();
});
