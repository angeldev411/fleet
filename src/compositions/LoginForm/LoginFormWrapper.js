import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  padding: ${px2rem(120)} ${px2rem(84)};
  width: ${px2rem(406)};
`;

export default buildStyledComponent(
  'LoginFormWrapper',
  styled.section,
  styles,
);
