import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  display: flex;
  flex: 1;
  position: relative;
`;

export default buildStyledComponent(
  'UserInputWrapper',
  styled.div,
  styles,
);
