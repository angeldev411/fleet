import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import IconWrapper, { colorStyle } from '../IconWrapper';

const theme = {
  colors: {
    status: {
      danger: 'danger',
      success: 'success',
    },
  },
};

/* ------------------------ IconWrapper ----------------------- */
test('Renders the IconWrapper as a div', () => {
  expect(shallow(<IconWrapper />)).toBeA('div');
});

/* ------------------------ color function ----------------------- */
test('Renders status.danger when type is error', () => {
  expect(colorStyle({ type: 'error', theme })).toInclude(`color: ${theme.colors.status.danger}`);
});

test('Renders status.success when type is success', () => {
  expect(colorStyle({ type: 'success', theme })).toInclude(`color: ${theme.colors.status.success}`);
});
