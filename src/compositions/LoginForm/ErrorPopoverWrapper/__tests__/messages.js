import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import messages, { errorMessageTitle, errorMessageDescription } from '../messages';

test('check title and description for usernameShort error', () => {
  expect(errorMessageTitle('usernameShort')).toEqual(messages.usernameShortErrorTitle);
  expect(errorMessageDescription('usernameShort')).toEqual(messages.usernameShortErrorDescription);
});

test('check title and description for passwordShort error', () => {
  expect(errorMessageTitle('passwordShort')).toEqual(messages.passwordShortErrorTitle);
  expect(errorMessageDescription('passwordShort')).toEqual(messages.passwordShortErrorDescription);
});

test('check title and description for auth error', () => {
  expect(errorMessageTitle('auth')).toEqual(messages.authErrorTitle);
  expect(errorMessageDescription('auth')).toEqual(messages.authErrorDescription);
});

test('check title and description for capslockActive error', () => {
  expect(errorMessageTitle('capslockActive')).toEqual(messages.capslockActiveErrorTitle);
  expect(errorMessageDescription('capslockActive')).toEqual(messages.capslockActiveErrorDescription);
});

test('check title and description for other errors', () => {
  expect(errorMessageTitle('test')).toEqual(messages.defaultInputErrorTitle);
  expect(errorMessageDescription('test')).toEqual(messages.defaultInputErrorDescription);
});
