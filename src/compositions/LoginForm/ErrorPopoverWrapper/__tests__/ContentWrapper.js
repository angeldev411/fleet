import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import ContentWrapper from '../ContentWrapper';

test('Renders the ContentWrapper as a div', () => {
  expect(shallow(<ContentWrapper />)).toBeA('div');
});
