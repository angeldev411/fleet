import React from 'react';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
} from '__tests__/helpers/test-setup';

import { ErrorPopoverWrapper } from '../index';

const defaultTheme = {
  colors: {
    status: {
      danger: 'testColor',
    },
  },
};

const defaultProps = {
  error: '',
  theme: defaultTheme,
};

function shallowRender(props = defaultProps) {
  return shallow(<ErrorPopoverWrapper {...props} />);
}

test('Renders Popover with right props', () => {
  const component = shallowRender();
  expect(component
    .find({
      position: 'right',
      arrowCenter: true,
      bgColor: defaultTheme.colors.status.danger,
      arrowSize: 12,
      showAlways: false,
      showOnHover: true,
    })
    .exists(),
  ).toBe(true);
});


test('Renders Popover with right props', () => {
  const clickOutside = noop;
  const component = shallowRender({ ...defaultProps, error: 'testError', clickOutside });
  expect(component
    .find({
      position: 'right',
      arrowCenter: true,
      bgColor: defaultTheme.colors.status.danger,
      arrowSize: 12,
      showAlways: true,
      showOnHover: true,
      clickOutside,
    })
    .exists(),
  ).toBe(true);
});
