import React from 'react';
import { test, expect, shallow } from '__tests__/helpers/test-setup';

import Wrapper from '../Wrapper';

test('Renders the Wrapper as a div', () => {
  expect(shallow(<Wrapper />)).toBeA('div');
});
