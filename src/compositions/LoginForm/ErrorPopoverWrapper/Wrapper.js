import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  position: absolute;
  right: ${px2rem(-25)};
  top: ${px2rem(7)};
`;

export default buildStyledComponent(
  'Wrapper',
  styled.div,
  styles,
);
