import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = props => `
  color: ${props.theme.colors.base.chrome000};
  font-size: ${px2rem(14)};
  font-style: italic;
  padding: ${px2rem(5)} ${px2rem(10)};
  strong {
    font-weight: 600;
  }
  width: ${px2rem(350)};
`;

const themePropTypes = {
  colors: PropTypes.shape({
    base: PropTypes.shape({
      chrome000: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'ContentWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
