import PropTypes from 'prop-types';
import styled from 'styled-components';
import { buildStyledComponent } from 'decisiv-ui-utils';

export function colorStyle({ type, theme }) {
  if (type === 'error') {
    return `
      color: ${theme.colors.status.danger};
    `;
  }
  return `
    color: ${theme.colors.status.success};
  `;
}

/* istanbul ignore next */
const styles = props => `
  ${colorStyle(props)}
  display: block;
`;

const themePropTypes = {
  colors: PropTypes.shape({
    status: PropTypes.shape({
      danger: PropTypes.string.isRequired,
      success: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default buildStyledComponent(
  'IconWrapper',
  styled.div,
  styles,
  { themePropTypes },
);
