import { defineMessages } from 'react-intl';

const messages = defineMessages({
  authErrorDescription: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.authErrorDescription',
    defaultMessage: 'Please try again.',
  },
  authErrorTitle: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.authErrorTitle',
    defaultMessage: 'Sorry, we don\'t recognize that passphrase.',
  },
  defaultInputErrorDescription: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.defaultInputErrorDescription',
    defaultMessage: 'There is an error in your input. Please try again.',
  },
  defaultInputErrorTitle: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.defaultInputErrorTitle',
    defaultMessage: 'Something\'s not right.',
  },
  passwordShortErrorDescription: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.passwordShortErrorDescription',
    defaultMessage: 'Enter a passphrase at least four characters long.',
  },
  passwordShortErrorTitle: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.passwordShortErrorTitle',
    defaultMessage: 'Your passphrase is too short.',
  },
  usernameShortErrorTitle: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.usernameShortErrorTitle',
    defaultMessage: 'Your username is too short.',
  },
  usernameShortErrorDescription: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.usernameShortErrorDescription',
    defaultMessage: 'Enter a username at least four characters long.',
  },
  capslockActiveErrorTitle: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.capslockActiveErrorTitle',
    defaultMessage: 'Caps Lock is Enabled',
  },
  capslockActiveErrorDescription: {
    id: 'components.LoginForm.ErrorPopoverWrapper.error.capslockActiveErrorDescription',
    defaultMessage: 'You may want to disable it before entering your password.',
  },
});

export function errorMessageTitle(error) {
  return messages[`${error}ErrorTitle`] || errorMessageTitle('defaultInput');
}

export function errorMessageDescription(error) {
  return messages[`${error}ErrorDescription`] || errorMessageDescription('defaultInput');
}

export default messages;
