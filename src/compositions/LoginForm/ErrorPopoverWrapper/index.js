import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import FontAwesome from 'react-fontawesome';
import { withTheme } from 'styled-components';

import Popover, { PopoverContent, PopoverTarget } from 'elements/Popover';

import Wrapper from './Wrapper';
import IconWrapper from './IconWrapper';
import ContentWrapper from './ContentWrapper';
import { errorMessageTitle, errorMessageDescription } from './messages';

export function ErrorPopoverWrapper({
  error,
  clickOutside,
  onClickPopover,
  theme,
}) {
  return (
    <Wrapper onClick={onClickPopover}>
      <Popover
        position="right"
        arrowCenter
        bgColor={theme.colors.status.danger}
        arrowSize={12}
        showAlways={!!error}
        showOnHover
        clickOutside={clickOutside}
        noShadow
      >
        <PopoverTarget>
          <IconWrapper type={error ? 'error' : 'success'}>
            <FontAwesome name={error ? 'times' : 'check'} />
          </IconWrapper>
        </PopoverTarget>
        <PopoverContent>
          {
            error ? (
              <ContentWrapper>
                <strong>
                  <FormattedMessage {...errorMessageTitle(error)} />
                </strong><br />
                <FormattedMessage {...errorMessageDescription(error)} />
              </ContentWrapper>
            ) : null
          }
        </PopoverContent>
      </Popover>
    </Wrapper>
  );
}

ErrorPopoverWrapper.propTypes = {
  error: PropTypes.string,
  theme: PropTypes.shape({
    colors: PropTypes.shape({
      status: PropTypes.shape({
        danger: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
  clickOutside: PropTypes.func,
  onClickPopover: PropTypes.func,
};

ErrorPopoverWrapper.defaultProps = {
  error: '',
  clickOutside: noop,
  onClickPopover: noop,
};

export default withTheme(ErrorPopoverWrapper);
