import styled from 'styled-components';
import { buildStyledComponent, px2rem } from 'decisiv-ui-utils';

/* istanbul ignore next */
const styles = () => `
  cursor: pointer;
  opacity: 0.6;
  position: absolute;
  right: ${px2rem(3)};
  top: ${px2rem(7)};
  transition: opacity 0.3s;
  &:hover {
    opacity: 1;
  }
`;

export default buildStyledComponent(
  'ShowPasswordWrapper',
  styled.div,
  styles,
);
