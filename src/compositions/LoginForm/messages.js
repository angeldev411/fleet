import { defineMessages } from 'react-intl';

const messages = defineMessages({
  decisiv: {
    id: 'components.LoginForm.oem.decisiv',
    defaultMessage: 'Decisiv',
  },
  emailOrUsername: {
    id: 'components.LoginForm.message.emailOrUsername',
    defaultMessage: 'Email Address or Username',
  },
  login: {
    id: 'components.LoginForm.message.login',
    defaultMessage: 'Log in',
  },
  mack: {
    id: 'components.LoginForm.oem.mack',
    defaultMessage: 'Mack',
  },
  passphrase: {
    id: 'components.LoginForm.message.passphrase',
    defaultMessage: 'Passphrase',
  },
  signInAgreement: {
    id: 'components.LoginForm.message.signInAgreement',
    defaultMessage: 'By signing in you agree to all applicable ',
  },
  termsAndConditions: {
    id: 'components.LoginForm.message.termsAndConditions',
    defaultMessage: 'Terms and Conditions',
  },
  volvo: {
    id: 'components.LoginForm.oem.volvo',
    defaultMessage: 'Volvo',
  },
});

export default messages;
