import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  selectItem: {
    id: 'compositions.AssetSelectionStep.selectItem',
    defaultMessage: 'Select Item...',
  },
  unit: {
    id: 'compositions.AssetSelectionStep.unit',
    defaultMessage: 'Unit',
  },
  whichAsset: {
    id: 'compositions.AssetSelectionStep.whichAsset',
    defaultMessage: 'Which asset?',
  },
});

export default formattedMessages;
