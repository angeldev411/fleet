import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  Container,
  Row,
  Column,
} from 'styled-components-reactive-grid';
import { px2rem } from 'decisiv-ui-utils';
import { debounce } from 'lodash';
import { H3 } from 'base-components';
import {
  compose,
  setDisplayName,
} from 'recompose';

import Select from 'elements/Select';

import WizardInput from 'components/WizardInput';

import Option from './Option';
import DropdownWrapper from './DropdownWrapper';
import withConnectedData from './Container';
import messages from './messages';

export function makeOptions(assets) {
  return assets.map(asset => ({
    label: `${asset.unitNumber} ${asset.make} - ${asset.model} - ${asset.engine} - ${asset.year}`,
    value: asset,
  }));
}

export class AssetSelectionStep extends Component {
  static propTypes = {
    assets: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
      }),
    ).isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    clearAssets: PropTypes.func.isRequired,
    loadAssets: PropTypes.func.isRequired,
    submitInputData: PropTypes.func.isRequired,
  }

  state = {
    currentAsset: null,
    options: makeOptions(this.props.assets),
  }

  componentDidMount() {
    this.loadItems();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.assets !== nextProps.assets) {
      const options = makeOptions(nextProps.assets);

      this.setState({ options });
    }
  }

  loadItems = (value) => {
    this.props.clearAssets();
    const params = { per_page: 150 };
    if (value && value.length >= 3) {
      params.unit_no = value;
    }
    this.props.loadAssets(params);
  }

  handleInputChange = debounce(this.loadItems, 1000)

  handleAssetChange = (options) => {
    const { submitInputData } = this.props;
    const currentAsset = options[options.length - 1];
    this.setState({ currentAsset });
    if (currentAsset) {
      submitInputData(currentAsset.value);
    }
  }

  render() {
    const {
      currentAsset,
      options,
    } = this.state;
    const {
      intl: { formatMessage },
    } = this.props;

    return (
      <Container style={{ padding: `${px2rem(30)} 0` }}>
        <Row>
          <Column modifiers={['col_offset_3', 'col_6']}>
            <Row>
              <Column modifiers={['col']}>
                <H3 modifiers={['fontWeightRegular']}>
                  <FormattedMessage {...messages.whichAsset} />
                </H3>
              </Column>
            </Row>
            <Row style={{ marginTop: px2rem(20) }}>
              <Column modifiers={['col']}>
                <WizardInput label={messages.unit}>
                  <DropdownWrapper>
                    <Select
                      autoBlur
                      multi
                      onChange={this.handleAssetChange}
                      onInputChange={this.handleInputChange}
                      optionRenderer={Option}
                      options={options}
                      placeholder={formatMessage(messages.selectItem)}
                      value={currentAsset}
                    />
                  </DropdownWrapper>
                </WizardInput>
              </Column>
            </Row>
          </Column>
        </Row>
      </Container>
    );
  }
}

export default compose(
  setDisplayName('AssetSelectionStep'),
  withConnectedData,
)(AssetSelectionStep);
