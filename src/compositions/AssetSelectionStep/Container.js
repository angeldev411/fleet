import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, setDisplayName } from 'recompose';
import { injectIntl } from 'react-intl';

import {
  clearAssets,
  loadAssets,
} from 'redux/assets/actions';
import {
  assetsSelector,
} from 'redux/assets/selectors';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  Container.propTypes = {
    assets: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
      }),
    ).isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
    clearAssets: PropTypes.func.isRequired,
    loadAssets: PropTypes.func.isRequired,
  };

  function mapStateToProps(state) {
    return {
      assets: assetsSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      clearAssets: () => dispatch(clearAssets()),
      loadAssets: filter => dispatch(loadAssets(filter)),
    };
  }

  return compose(
    setDisplayName('AssetSelectionStepContainer'),
    connect(mapStateToProps, mapDispatchToProps),
    injectIntl,
    immutableToJS,
  )(Container);
};

export default withConnectedData;
