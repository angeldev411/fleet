import React from 'react';
import {
  Container,
  Row,
  Column,
} from 'styled-components-reactive-grid';

import SeverityLabel from 'components/SeverityLabel';
import StatusLabel from 'components/StatusLabel';
import TextDiv from 'elements/TextDiv';

/* istanbul ignore next */
function Option(option) {
  const assetInfo = option.value;

  return (
    <Container>
      <Row modifiers={['middle']}>
        <Column modifiers={['col']}>
          <TextDiv modifiers={['bold', 'darkGreyText', 'smallText']}>
            {option.label}
          </TextDiv>
        </Column>
        <Column>
          <SeverityLabel
            color={assetInfo.severityColor}
            value={Number(assetInfo.severityCount)}
          />
        </Column>
        <Column>
          <StatusLabel
            color={assetInfo.pmStatus}
            showIcon
          />
        </Column>
        <Column>
          <StatusLabel
            color={assetInfo.warrantyStatus}
            showIcon
          />
        </Column>
        <Column>
          <StatusLabel
            color={assetInfo.campaignsStatus}
            showIcon
          />
        </Column>
      </Row>
    </Container>
  );
}

export default Option;
