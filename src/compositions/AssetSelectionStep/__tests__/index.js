import React from 'react';
import { noop } from 'lodash';
import {
  test,
  expect,
  shallow,
  createSpy,
  spyOn,
} from '__tests__/helpers/test-setup';

import { AssetSelectionStep, makeOptions } from '../index';
import Option from '../Option';
import messages from '../messages';

const formatMessage = message => message.defaultMessage;

const testProps = {
  assets: [
    {
      id: '12345678',
      unitNumber: '1214',
      make: 'Mack',
      model: 'CH613',
      engine: 'E7',
      year: '2003',
    },
    {
      id: '12345679',
      unitNumber: '1233',
      make: 'Mack',
      model: 'CH613',
      engine: 'E7',
      year: '2003',
    },
  ],
  intl: {
    formatMessage,
  },
  clearAssets: noop,
  loadAssets: noop,
  submitInputData: noop,
};

function shallowRender(props = testProps) {
  return shallow(<AssetSelectionStep {...props} />);
}

test('makeOptions returns correct options', () => {
  const options = [
    {
      label: '1214 Mack - CH613 - E7 - 2003',
      value: testProps.assets[0],
    },
    {
      label: '1233 Mack - CH613 - E7 - 2003',
      value: testProps.assets[1],
    },
  ];
  expect(makeOptions(testProps.assets)).toEqual(options);
});

test('renders title', () => {
  const component = shallowRender();
  const firstRow = component.find('withSize(Row)').first()
    .find('withSize(Row)').first();
  expect(firstRow).toContain('FormattedMessage');
  const FormattedMessage = firstRow.find('FormattedMessage');
  expect(FormattedMessage).toHaveProps({
    ...messages.whichAsset,
  });
});

test('passes label prop correctly', () => {
  const component = shallowRender();
  const row = component.find('withSize(Row)').first()
    .find('withSize(Row)').last();
  expect(row).toContain('WizardInput');
  const WizardInput = row.find('WizardInput');
  expect(WizardInput).toHaveProp('label', messages.unit);
});

test('renders dropdown and pass correct props', () => {
  const component = shallowRender();
  const row = component.find('withSize(Row)').last();
  const column = row.find('withSize(Column)');
  expect(column).toContain('Select');
  const select = column.find('Select');
  const instance = component.instance();

  expect(select).toHaveProps({
    autoBlur: true,
    multi: true,
    onChange: instance.handleAssetChange,
    onInputChange: instance.handleInputChange,
    optionRenderer: Option,
    options: component.state.options,
    placeholder: formatMessage(messages.selectItem),
    value: null,
  });
});

test('componentWillReceiveProps updates new options when assets change', () => {
  const component = shallowRender();
  const nextProps = {
    assets: [
      {
        id: '12345678',
        unitNumber: '1214',
        make: 'Mack',
        model: 'CH613',
        engine: 'E7',
        year: '2003',
      },
    ],
  };
  component.instance().componentWillReceiveProps(nextProps);
  const newOptions = makeOptions(nextProps.assets);
  expect(component.state().options).toEqual(newOptions);
});

test('componentWillReceiveProps does NOT fire setState when assets NOT change', () => {
  const component = shallowRender();
  const nextProps = {
    assets: testProps.assets,
  };
  const spy = spyOn(component, 'setState');
  component.instance().componentWillReceiveProps(nextProps);
  expect(spy).toNotHaveBeenCalled();
});

test('handleAssetChange selects a new option and submits', () => {
  const submitInputData = createSpy();
  const component = shallowRender({
    ...testProps,
    submitInputData,
  });
  expect(component).toHaveState({ currentAsset: null });
  const options = [{ label: 'label', value: 'value' }];
  component.instance().handleAssetChange(options);
  expect(component).toHaveState({ currentAsset: options[0] });
  expect(submitInputData).toHaveBeenCalledWith('value');
});

test('loadItem is called when page mounted.', () => {
  const page = shallowRender();
  const instance = page.instance();
  const spy = spyOn(instance, 'loadItems');
  instance.componentDidMount();
  expect(spy).toHaveBeenCalled();
});

test('loadItem clears and loads assets', () => {
  const clearAssets = createSpy();
  const loadAssets = createSpy();
  const page = shallowRender({
    ...testProps,
    clearAssets,
    loadAssets,
  });
  page.instance().loadItems('1234');
  expect(clearAssets).toHaveBeenCalled();
  expect(loadAssets).toHaveBeenCalledWith({
    unit_no: '1234',
    per_page: 150,
  });
});

test('loadAssets does NOT add search filter when unit_no has less than 3 characters', () => {
  const loadAssets = createSpy();
  const page = shallowRender({
    ...testProps,
    loadAssets,
  });
  page.instance().loadItems('12');
  expect(loadAssets).toHaveBeenCalledWith({
    per_page: 150,
  });
});
