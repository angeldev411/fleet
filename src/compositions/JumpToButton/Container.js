import React from 'react';
import { connect } from 'react-redux';
import { compose, setDisplayName } from 'recompose';

import immutableToJS from 'utils/immutableToJS';
import PropTypes from 'utils/prop-types';

import { currentPageConfigSelector } from 'redux/pageConfigs/selectors';

import { handleActionBarItemClick } from 'redux/ui/actions';
import {
  actionBarItemClickedSelector,
} from 'redux/ui/selectors';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function WithConnectedData(props) {
    return (
      <WrappedComponent {...props} />
    );
  }

  WithConnectedData.propTypes = {
    actionBarItemClicked: PropTypes.shape({
      buttonId: PropTypes.string,
      dropdownItemId: PropTypes.string,
    }).isRequired,
    handleActionBarItemClick: PropTypes.func.isRequired,
    pageConfig: PropTypes.shape({
      top: PropTypes.compositionConfig,
      left: PropTypes.compositionConfig,
      right: PropTypes.compositionConfig,
    }).isRequired,
  };

  function mapStateToProps(state) {
    return {
      actionBarItemClicked: actionBarItemClickedSelector(state),
      pageConfig: currentPageConfigSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      handleActionBarItemClick: payload => dispatch(handleActionBarItemClick(payload)),
    };
  }

  return compose(
    setDisplayName('JumpToButtonContainer'),
    connect(mapStateToProps, mapDispatchToProps),
    immutableToJS,
  )(WithConnectedData);
};

export default withConnectedData;
