import { noop } from 'lodash';
import React from 'react';

import {
  test,
  expect,
  shallow,
  spyOn,
  createSpy,
} from '__tests__/helpers/test-setup';

import { buildJumpToOptions, JumpToButton } from '../index';

// -------------------------- buildJumpToOptions ------------------------------------

test('returns empty array when page config is empty', () => {
  expect(buildJumpToOptions({})).toEqual([]);
});

test('returns array with top components first, then left and right alternating in order', () => {
  const pageConfig = {
    top: ['TOP_FIRST', 'TOP_SECOND'],
    left: ['LEFT_FIRST', 'LEFT_SECOND'],
    right: ['RIGHT_FIRST', 'RIGHT_SECOND'],
  };
  const jumpToOptions = buildJumpToOptions(pageConfig);
  expect(jumpToOptions).toEqual([
    { id: 'TOP_FIRST', label: undefined },
    { id: 'TOP_SECOND', label: undefined },
    { id: 'LEFT_FIRST', label: undefined },
    { id: 'RIGHT_FIRST', label: undefined },
    { id: 'LEFT_SECOND', label: undefined },
    { id: 'RIGHT_SECOND', label: undefined },
  ]);
});

// ------------------------- JumpToButton ---------------------------------------------

const defaultProps = {
  actionBarItemClicked: {},
  handleActionBarItemClick: noop,
  pageConfig: {
    top: ['TOP_FIRST', 'TOP_SECOND'],
    left: ['LEFT_FIRST', 'LEFT_SECOND'],
    right: ['RIGHT_FIRST', 'RIGHT_SECOND'],
  },
  theme: {
    dimensions: {
      topNavHeightInPixel: 72,
    },
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<JumpToButton {...props} />);
}

test('with new actionBarItemClicked prop, calls scrollTo if JUMP_TO_BUTTON and new dropdownItemId', () => {
  const component = shallowRender();
  const instance = component.instance();
  const scrollToWidget = spyOn(instance, 'scrollToWidget');
  const dropdownItemId = 'LEFT_SECOND';
  instance.componentWillReceiveProps({
    actionBarItemClicked: {
      buttonId: 'JUMP_TO_BUTTON',
      dropdownItemId,
    },
  });
  expect(scrollToWidget).toHaveBeenCalledWith(dropdownItemId);
});

test('with new actionBarItemClicked prop, does not call scrollTo if buttonId not JUMP_TO_BUTTON', () => {
  const component = shallowRender();
  const instance = component.instance();
  const scrollToWidget = spyOn(instance, 'scrollToWidget');
  const dropdownItemId = 'viewFullScreen';
  instance.componentWillReceiveProps({
    actionBarItemClicked: {
      buttonId: 'OPTIONS_BUTTON',
      dropdownItemId,
    },
  });
  expect(scrollToWidget).toNotHaveBeenCalled();
});

test('with new actionBarItemClicked prop, does not call scrollTo if same dropdownItemId', () => {
  const dropdownItemId = 'LEFT_SECOND';
  const actionBarItemClicked = {
    buttonId: 'JUMP_TO_BUTTON',
    dropdownItemId,
  };
  const testProps = {
    ...defaultProps,
    actionBarItemClicked,
  };
  const component = shallowRender(testProps);
  const instance = component.instance();
  const scrollToWidget = spyOn(instance, 'scrollToWidget');
  instance.componentWillReceiveProps({ actionBarItemClicked });
  expect(scrollToWidget).toNotHaveBeenCalled();
});

test('clicking QuickActionSelector calls handleActionBarItemClick', () => {
  const handleActionBarItemClick = createSpy();
  const testProps = {
    ...defaultProps,
    handleActionBarItemClick,
  };
  const component = shallowRender(testProps);
  component.find('QuickActionSelector').simulate('click');
  expect(handleActionBarItemClick).toHaveBeenCalled();
});
