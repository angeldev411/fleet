import { defineMessages } from 'react-intl';

const messages = defineMessages({
  jumpTo: {
    id: 'compositions.JumpToButton.label',
    defaultMessage: 'Jump To...',
  },
  ASSET_FAULT_DIAGNOSTIC_WIDGET: {
    id: 'compositions.JumpToButton.diagnostics',
    defaultMessage: 'Diagnostics',
  },
  ASSET_SUMMARY_WIDGET: {
    id: 'compositions.JumpToButton.assetSummary',
    defaultMessage: 'Asset Summary',
  },
  ASSETS_WIDGET: {
    id: 'compositions.JumpToButton.asset',
    defaultMessage: 'Asset',
  },
  CASE_FAULT_DIAGNOSTIC_WIDGET: {
    id: 'compositions.JumpToButton.diagnostics',
    defaultMessage: 'Diagnostics',
  },
  CASE_SERVICE_PROVIDER_WIDGET: {
    id: 'compositions.JumpToButton.serviceProvider',
    defaultMessage: 'Service Provider',
  },
  CASE_STATUS_WIDGET: {
    id: 'compositions.JumpToButton.caseStatus',
    defaultMessage: 'Case Status',
  },
  COMPLAINT_WIDGET: {
    id: 'compositions.JumpToButton.complaint',
    defaultMessage: 'Complaint',
  },
  ESTIMATE_INVOICE_DECIDED_WIDGET: {
    id: 'compositions.JumpToButton.estimateInvoiceDecided',
    defaultMessage: 'Estimate Invoice',
  },
  ESTIMATE_INVOICE_PENDING_WIDGET: {
    id: 'compositions.JumpToButton.estimateInvoicePending',
    defaultMessage: 'Estimate Invoice',
  },
  LOCATION_WIDGET: {
    id: 'compositions.JumpToButton.location',
    defaultMessage: 'Location',
  },
  NOTES_WIDGET: {
    id: 'compositions.JumpToButton.notes',
    defaultMessage: 'Notes',
  },
  QR_CODE_WIDGET: {
    id: 'compositions.JumpToButton.qrCodeWidger',
    defaultMessage: 'QR Code',
  },
  OPEN_EVENTS_WIDGET: {
    id: 'compositions.JumpToButton.openEventsWidget',
    defaultMessage: 'Open Events',
  },
  SERVICE_REQUEST_SERVICE_PROVIDER_WIDGET: {
    id: 'compositions.JumpToButton.serviceProvider',
    defaultMessage: 'Service Provider',
  },
  SERVICE_REQUEST_WIDGET: {
    id: 'compositions.JumpToButton.serviceRequest',
    defaultMessage: 'Service Request',
  },
});

export default messages;
