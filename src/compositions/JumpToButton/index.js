import { compact, flattenDeep, zip } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { compose, setDisplayName } from 'recompose';
import { scroller } from 'react-scroll';
import { withTheme } from 'styled-components';

import QuickActionSelector from 'components/QuickActionSelector';

import withConnectedData from './Container';
import messages from './messages';

export function buildJumpToOptions(pageConfig) {
  const top = pageConfig.top || [];
  const left = pageConfig.left || [];
  const right = pageConfig.right || [];

  const zippedLROptions = zip(left, right);
  const allOptions = top.concat(zippedLROptions);
  const combinedOptions = compact(flattenDeep(allOptions));

  return combinedOptions.reduce((acc, opt) => acc.concat({
    id: opt,
    label: messages[opt],
  }), []);
}

const JUMP_TO_BUTTON = 'JUMP_TO_BUTTON';

export class JumpToButton extends Component {
  static propTypes = {
    actionBarItemClicked: PropTypes.shape({
      buttonId: PropTypes.string,
      dropdownItemId: PropTypes.string,
    }).isRequired,
    handleActionBarItemClick: PropTypes.func.isRequired,
    pageConfig: PropTypes.shape({
      top: PropTypes.compositionConfig,
      left: PropTypes.compositionConfig,
      right: PropTypes.compositionConfig,
    }).isRequired,
    theme: PropTypes.shape({
      dimensions: PropTypes.shape({
        topNavHeightInPixel: PropTypes.number.isRequired,
      }).isRequired,
    }).isRequired,
  };

  componentWillReceiveProps({ actionBarItemClicked }) {
    const {
      buttonId,
      dropdownItemId,
    } = actionBarItemClicked;
    if (buttonId === JUMP_TO_BUTTON) {
      const {
        actionBarItemClicked: {
          dropdownItemId: currentDropdownItemId,
        },
      } = this.props;
      if (dropdownItemId && dropdownItemId !== currentDropdownItemId) {
        this.scrollToWidget(dropdownItemId);
      }
    }
  }

  scrollToWidget = (dropdownItemId) => {
    const {
      theme: {
        dimensions: {
          topNavHeightInPixel,
        },
      },
    } = this.props;
    scroller.scrollTo(dropdownItemId, {
      duration: 500,
      smooth: true,
      offset: -topNavHeightInPixel,
    });
  }

  render() {
    return (
      <QuickActionSelector
        item={{
          id: JUMP_TO_BUTTON,
          icon: 'caret-down',
          iconPressed: 'caret-up',
          label: messages.jumpTo,
          options: buildJumpToOptions(this.props.pageConfig),
        }}
        onClick={this.props.handleActionBarItemClick}
      />
    );
  }
}

export default compose(
  setDisplayName('JumpToButton'),
  withConnectedData,
  withTheme,
)(JumpToButton);
