import { defineMessages } from 'react-intl';

const messages = defineMessages({
  make: {
    id: 'compositions.ServiceRequestOverview.subtitle.make',
    defaultMessage: 'Make',
  },
  model: {
    id: 'compositions.ServiceRequestOverview.subtitle.model',
    defaultMessage: 'Model',
  },
  unit: {
    id: 'compositions.ServiceRequestOverview.subtitle.unit',
    defaultMessage: 'Unit',
  },
  year: {
    id: 'compositions.ServiceRequestOverview.subtitle.year',
    defaultMessage: 'Year',
  },
});

export default messages;
