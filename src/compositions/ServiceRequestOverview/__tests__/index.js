import React from 'react';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import { ServiceRequestOverview } from '../index';

const defaultProps = {
  assetInfo: {
    unitNumber: 'abc',
    year: '2000',
    make: 'Mack',
    model: 'MXZ721',
  },
};

function shallowRender(props = defaultProps) {
  return shallow(<ServiceRequestOverview {...props} />);
}

test('Renders a ServiceRequestOverview', () => {
  const component = shallowRender();
  expect(component).toContain('#service-request-overview');
});

test('Includes a BreadCrumbLink with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('BreadCrumbLink');
});

test('Includes an asset information table', () => {
  const component = shallowRender();
  expect(component).toContain('Table');
});
