import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
  Column,
  Container,
  Row,
} from 'styled-components-reactive-grid';

import BreadCrumbLink from 'compositions/BreadCrumbLink';

import Table from 'elements/Table';

import { getOutputText } from 'utils/widget';

import withConnectedData from './Container';
import messages from './messages';

export function ServiceRequestOverview({
  assetInfo,
}) {
  const {
    unitNumber,
    year,
    make,
    model,
  } = assetInfo;

  return (
    <Container id="service-request-overview">
      <BreadCrumbLink />
      <Row modifiers={['middle']}>
        <Column modifiers={['col']}>
          <Table modifiers={['mediumGrey', 'xLargeText']}>
            <tbody>
              <tr>
                <th><FormattedMessage {...messages.unit} /></th>
                <td>{getOutputText(unitNumber)}</td>
                <th><FormattedMessage {...messages.year} /></th>
                <td>{getOutputText(year)}</td>
                <th><FormattedMessage {...messages.make} /></th>
                <td>{getOutputText(make)}</td>
                <th><FormattedMessage {...messages.model} /></th>
                <td>{getOutputText(model)}</td>
              </tr>
            </tbody>
          </Table>
        </Column>
      </Row>
    </Container>
  );
}

ServiceRequestOverview.propTypes = {
  assetInfo: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
};

export default withConnectedData(ServiceRequestOverview);
