import { QuickActionButton } from 'base-components';
import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import messages from './messages';
import withConnectedData from './Container';

const BUTTON_ID = 'ADD_NOTE';

export function AddNoteButton({ handleActionBarItemClick }) {
  return (
    <QuickActionButton onClick={() => handleActionBarItemClick(BUTTON_ID)}>
      <QuickActionButton.Icon modifiers={['left']} name="envelope-o" />
      <QuickActionButton.Text>
        <FormattedMessage {...messages.title} />
      </QuickActionButton.Text>
    </QuickActionButton>
  );
}

AddNoteButton.propTypes = {
  handleActionBarItemClick: PropTypes.func.isRequired,
};

export default withConnectedData(AddNoteButton);
