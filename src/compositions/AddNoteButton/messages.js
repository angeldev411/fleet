import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'compositions.AddNoteButton.title',
    defaultMessage: 'Add Note',
  },
});

export default messages;
