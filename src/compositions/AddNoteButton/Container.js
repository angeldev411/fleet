import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import { handleActionBarItemClick } from 'redux/ui/actions';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function WithConnectedData(props) {
    return <WrappedComponent {...props} />;
  }

  WithConnectedData.propTypes = {
    handleActionBarItemClick: PropTypes.func.isRequired,
  };

  function mapStateToProps() {
    return {};
  }

  function mapDispatchToProps(dispatch) {
    return {
      handleActionBarItemClick: payload => dispatch(handleActionBarItemClick(payload)),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(immutableToJS(WithConnectedData));
};

export default withConnectedData;
