import React from 'react';

import {
  test,
  expect,
  shallow,
  createSpy,
} from '__tests__/helpers/test-setup';

import { AddNoteButton } from '../index';

const defaultProps = {
  handleActionBarItemClick: () => {},
};

function shallowRender(props = defaultProps) {
  return shallow(<AddNoteButton {...props} />);
}

test('clicking the QuickActionButton calls handleActionBarItemClick', () => {
  const handleActionBarItemClick = createSpy();
  const component = shallowRender({ handleActionBarItemClick });
  component.simulate('click');
  expect(handleActionBarItemClick).toHaveBeenCalledWith('ADD_NOTE');
});
