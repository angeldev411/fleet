import { defineMessages } from 'react-intl';

const messages = defineMessages({
  make: {
    id: 'compositions.AssetOverview.make',
    defaultMessage: 'Make',
  },
  model: {
    id: 'compositions.AssetOverview.model',
    defaultMessage: 'Model',
  },
  serial: {
    id: 'compositions.AssetOverview.serial',
    defaultMessage: 'Serial',
  },
  year: {
    id: 'compositions.AssetOverview.year',
    defaultMessage: 'Year',
  },
});

export default messages;
