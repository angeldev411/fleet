import React from 'react';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import { AssetOverview } from '../index';

const assetInfo = {
  severityColor: 'yellow',
  severityCount: '2',
};

const defaultProps = {
  assetInfo,
};

function shallowRender(props = defaultProps) {
  return shallow(<AssetOverview {...props} />);
}

test('Renders a PageHeadingPanel', () => {
  const component = shallowRender();
  expect(component).toContain('PageHeadingPanel');
});

test('Renders a AssetOverview', () => {
  const component = shallowRender();
  expect(component).toContain('#asset-overview');
});

test('Includes a BreadCrumbLink with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('BreadCrumbLink');
});

test('Includes a SeverityLabel with expected props', () => {
  const component = shallowRender();
  expect(component).toContain('SeverityLabel');
  expect(component.find('SeverityLabel')).toHaveProps({
    color: assetInfo.severityColor,
    value: Number(assetInfo.severityCount),
  });
});
