import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  addPageToBreadcrumbs,
} from 'redux/app/actions';
import {
  currentAssetSelector,
} from 'redux/assets/selectors';

import immutableToJS from 'utils/immutableToJS';

/* istanbul ignore next */
const withConnectedData = (WrappedComponent) => {
  function Container(props) {
    return <WrappedComponent {...props} />;
  }

  Container.propTypes = {
    assetInfo: PropTypes.shape({
      make: PropTypes.string,
      model: PropTypes.string,
      severityColor: PropTypes.string,
      severityCount: PropTypes.string,
      vinNumber: PropTypes.string,
      year: PropTypes.string,
    }).isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    addPageToBreadcrumbs: PropTypes.func.isRequired,
  };

  Container.defaultProps = {
    assetInfo: {},
  };

  function mapStateToProps(state) {
    return {
      assetInfo: currentAssetSelector(state),
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      addPageToBreadcrumbs: breadcrumbs => dispatch(addPageToBreadcrumbs(breadcrumbs)),
    };
  }

  return connect(mapStateToProps, mapDispatchToProps)(
    withRouter(
      immutableToJS(Container),
    ),
  );
};

export default withConnectedData;
