import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
  Column,
  Container,
  Row,
} from 'styled-components-reactive-grid';
import {
  compose,
  setDisplayName,
} from 'recompose';

import SeverityLabel from 'components/SeverityLabel';

import BreadCrumbLink from 'compositions/BreadCrumbLink';

import PageHeadingPanel from 'elements/PageHeadingPanel';
import Table from 'elements/Table';

import { getSerialNumber } from 'utils/asset';
import { getOutputText } from 'utils/widget';

import withConnectedData from './Container';
import messages from './messages';

export function AssetOverview({
  assetInfo: {
    make,
    model,
    severityColor,
    severityCount,
    vinNumber,
    year,
  },
}) {
  // NOTE: The extra padding on top is required to correct other display issues on this page.
  // This must be removed when this component is exported to a composition, and brought in via
  // the config file.
  return (
    <PageHeadingPanel>
      <Container id="asset-overview" style={{ paddingTop: '16px' }}>
        <BreadCrumbLink />
        <Row modifiers={['middle']}>
          <Column modifiers={['col']}>
            <Table modifiers={['mediumGrey', 'xLargeText']}>
              <tbody>
                <tr>
                  <th><FormattedMessage {...messages.serial} /></th>
                  <td>{getSerialNumber(vinNumber, messages.serial)}</td>
                  <th><FormattedMessage {...messages.make} /></th>
                  <td>{getOutputText(make)}</td>
                  <th><FormattedMessage {...messages.model} /></th>
                  <td>{getOutputText(model)}</td>
                  <th><FormattedMessage {...messages.year} /></th>
                  <td>{getOutputText(year)}</td>
                </tr>
              </tbody>
            </Table>
          </Column>
        </Row>
        <Row modifiers={['end']}>
          <Column>
            <SeverityLabel
              color={severityColor}
              value={Number(severityCount)}
            />
          </Column>
        </Row>
      </Container>
    </PageHeadingPanel>
  );
}

AssetOverview.propTypes = {
  assetInfo: PropTypes.shape({
    make: PropTypes.string,
    model: PropTypes.string,
    severityColor: PropTypes.string,
    severityCount: PropTypes.string,
    vinNumber: PropTypes.string,
    year: PropTypes.string,
  }).isRequired,
};

AssetOverview.defaultProps = {
  assetInfo: {
    severityColor: 'grey',
    severityCount: '0',
  },
};

export default compose(
  setDisplayName('AssetOverview'),
  withConnectedData,
)(AssetOverview);
