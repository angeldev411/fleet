import React from 'react';
import moment from 'moment';
import { join, compact } from 'lodash';
import { FormattedMessage } from 'react-intl';

import messages from './messages';
import { renderEmptyValue } from './widget';

const ASSET_STATUSES = {
  campaignStatus: {
    green: messages.campaignStatusGreen,
    red: messages.campaignStatusRed,
  },
  pmStatus: {
    green: messages.pmStatusGreen,
    yellow: messages.pmStatusYellow,
    red: messages.pmStatusRed,
  },
  warrantyStatus: {
    green: messages.warrantyStatusGreen,
    yellow: messages.warrantyStatusYellow,
    red: messages.warrantyStatusRed,
    grey: messages.warrantyStatusGrey,
  },
};

function sanitizeNumber(number) {
  // toLocaleString was breaking on ie10
  try {
    return number && Number(number).toLocaleString();
  } catch (e) {
    return number && Number(number);
  }
}

export function getSerialNumber(vinNumber, placeholder = messages.noValue) {
  if (!vinNumber) { return renderEmptyValue(placeholder); }
  return vinNumber.substr(-8);
}

export function getAssetStatusText(type, color, placeholder = messages.noValue) {
  if (!ASSET_STATUSES[type] || !ASSET_STATUSES[type][color]) {
    return renderEmptyValue(placeholder);
  }
  const message = ASSET_STATUSES[type][color];
  return <FormattedMessage {...message} />;
}

export function getOdometerText(reading, unit, readAt) {
  const cleanReading = sanitizeNumber(reading);
  const cleanReadAt = readAt && `(${moment(readAt).format('D MMM YYYY')})`;
  const parts = [].concat(cleanReading, unit, cleanReadAt);
  return join(compact(parts), ' ');
}
