/**
 * getScrollTop returns a scroll-top value across all browsers (ie, in Firefox
 * scrollTop is on documentElement but in Chrome, scrollTop is on the body)
 */
// eslint-disable-next-line import/prefer-default-export
export function getScrollTop({ documentElement, body }) {
  return (
    Math.max(documentElement.scrollTop, body.scrollTop)
  );
}
