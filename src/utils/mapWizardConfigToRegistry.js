import {
  isArray,
  isNull,
  isString,
  mapValues,
} from 'lodash';

import wizardRegistry from 'setup/WizardRegistry';

/**
 * `mapWizardConfigToRegistry` is a memoized closure that efficiently handles converting the
 * component step names stored in the runtime config to a map of registered steps.
 * An input config will probably look like:
 * {
 *   start: 'STEP_NAME_1'
 *   steps: {
 *     STEP_NAME_1: ['STEP_NAME_2'],
 *     STEP_NAME_2: ['REVIEW'],
 *   },
 * }
 *
 * You should expect to get back from this config:
 * {
 *   SpecificWizardController: REACT COMPONENT,
 *   start: 'STEP_NAME_1',
 *   steps: {
 *     STEP_NAME_1: {
 *       component: REACT COMPONENT,
 *       name: 'STEP_NAME_1'
 *       nextSteps: ['STEP_NAME_2'],
 *     },
 *     STEP_NAME_2: {
 *      component: REACT COMPONENT,
 *      name: 'STEP_NAME_2',
 *      nextSteps: ['REVIEW'],
 *     },
 *     REVIEW: {
 *       component: REACT COMPONENT,
 *       name: 'REVIEW',
 *     },
 *   },
 * }
 *
 * When calling this function, you must provide the config and the configKey that was used.
 */
const mapWizardConfigToRegistry = (() => {
  // Setup the cache. This will hold previously run configuration maps.
  const cache = {};

  // These keys trigger specific requirements in the processing of a wizard
  // config. They are required in all wizard configs.
  const reviewStepKey = 'REVIEW';
  const stepsKey = 'steps';

  const process = (config, configKey, registry = wizardRegistry, stepKey) => {
    if (configKey && cache[configKey]) {
      // This wizard config has already been processed. Return previous results.
      return cache[configKey];
    }

    // Strings and null values in a wizard config should simply be returned.
    if (isNull(config) || isString(config)) {
      return config;
    }

    // If the config currently being processed is an array, return an object with
    // the component, component name, and nextSteps for this config.
    if (isArray(config)) {
      if (stepKey) {
        return {
          component: registry[stepKey],
          name: stepKey,
          nextSteps: config,
        };
      }
      return config;
    }

    // The config currently being processed is an object, handle processing it recursively.
    // Note that no `configKey` is being used, so this call will not check or save to the cache.
    const obj = mapValues(config, (value, key) => process(value, undefined, registry, key));

    // Every wizard will require the REVIEW step, so it should be added by default.
    if (stepKey === stepsKey && !obj[reviewStepKey]) {
      obj[reviewStepKey] = {
        component: registry[reviewStepKey],
        name: reviewStepKey,
      };
    }

    // Only the initial call contains a `configKey`. This block handles top level work, like
    // saving the result of the initial call to the cache under the `configKey`.
    if (configKey) {
      // Every wizard will require a specific wizard controller to handle things like submission.
      obj.SpecificWizardController = registry[configKey];

      cache[configKey] = obj;
    }

    return obj;
  };

  return process;
})();

export default mapWizardConfigToRegistry;
