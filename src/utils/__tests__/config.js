import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import { configEnabled } from '../config';

test('configEnabled returns true if a truthy value is given', () => {
  ['true', 'True', 'TRUE', 'yes', 'YES', 'Yes', 'y', 'Y', 't', 'T'].forEach((value) => {
    process.env.TEST_VAR = value;
    expect(
      configEnabled('TEST_VAR'),
    ).toEqual(true, `Expected ${value} to indicate a truthy config value`);
  });
});

test('configEnabled returns false if a non-truthy value is given', () => {
  ['false', 'no', 'No', '', 'foo', '1'].forEach((value) => {
    process.env.TEST_VAR = value;
    expect(
      configEnabled('TEST_VAR'),
    ).toEqual(false, `Expected ${value} to indicate a falsey config value`);
  });
});

test('configEnabled returns false if the environment variable is not present', () => {
  expect(configEnabled('INVALID_TEST_VAR')).toEqual(false);
});
