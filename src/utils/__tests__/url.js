import { test, expect } from '__tests__/helpers/test-setup';

import { parseQueryParams } from '../url';

test('parseQueryParams returns expected object from query string', () => {
  const queryString = '?param1=hi&param2=hello';
  const params = parseQueryParams(queryString);
  expect(params).toEqual({
    param1: 'hi',
    param2: 'hello',
  });
});
