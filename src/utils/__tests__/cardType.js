import { test, expect } from '__tests__/helpers/test-setup';

import cardType from '../cardType';

test('cardType returns type `statusSuccess` if approvalStatus is `Approved`', () => {
  const input = 'Approved';
  const output = cardType(input);
  expect(output).toEqual('statusSuccess');
});

test('cardType returns type `statusDanger` if approvalStatus is `Declined`', () => {
  const input = 'Declined';
  const output = cardType(input);
  expect(output).toEqual('statusDanger');
});

test('cardType returns type `statusWarning` if approvalStatus is `Pending`', () => {
  const input = 'Pending';
  const output = cardType(input);
  expect(output).toEqual('statusWarning');
});

test('cardType returns type `statusWarning` if approvalStatus is `Requested`', () => {
  const input = 'Requested';
  const output = cardType(input);
  expect(output).toEqual('statusWarning');
});

test('cardType returns type `` if approvalStatus does NOT have a match', () => {
  const input = 'Undefined';
  const output = cardType(input);
  expect(output).toEqual('');
});
