import React from 'react';
import { fromJS, Map } from 'immutable';
import { noop } from 'lodash';

import {
  test,
  expect,
  shallow,
  createSpy,
  mount,
} from '__tests__/helpers/test-setup';

import {
  SetsLatestFavorite,
  getFavorite,
  setFavoriteAndRender,
  setSearchFavoriteAndRender,
} from '../routes';

import messages from '../messages';

/* ---------------------------- SetsLatestFavorite ----------------------------------- */

const testComponent = () => <div>test</div>;
const componentProps = { displayName: 'test' };
const favorite = fromJS({
  count: '642',
  message: {
    id: 'totalRepairCases',
    defaultMessage: 'Total Repair Cases',
  },
  params: { scope: '' },
  path: '/cases/filter/all-repair-cases',
  slug: 'all-repair-cases',
  title: 'allRepairCases',
});
const defaultSetLatestFavorite = noop;

const defaultProps = {
  Component: testComponent,
  componentProps,
  favorite,
  setLatestFavorite: defaultSetLatestFavorite,
};

function shallowRender(props = defaultProps) {
  return shallow(<SetsLatestFavorite {...props} />);
}

function fullRender(props = defaultProps) {
  return mount(<SetsLatestFavorite {...props} />);
}

test('returns the component provided in props with the componentProps as props', () => {
  const component = shallowRender();
  expect(component).toContain(testComponent);
  expect(component.find(testComponent)).toHaveProps(componentProps);
});

test('calls setLatestFavorite with the favorite in props on mount', () => {
  const setLatestFavorite = createSpy();
  shallowRender({ ...defaultProps, setLatestFavorite });
  expect(setLatestFavorite).toHaveBeenCalled();
});

test('calls setLatestFavorite with the favorite in props on update', () => {
  const setLatestFavorite = createSpy();
  const component = fullRender({ ...defaultProps, setLatestFavorite });
  const newFavorite = fromJS({
    count: '42',
    message: {
      id: 'downtime',
      defaultMessage: 'Downtime',
    },
    params: { scope: '' },
    path: '/cases/filter/downtime',
    slug: 'downtime',
    title: 'downtime',
  });
  component.setProps({ favorite: newFavorite });
  expect(setLatestFavorite.calls.length).toEqual(2);
});

/* --------------------------------- getFavorite ------------------------------------ */

const favorites = fromJS({
  cases: [
    {
      count: '642',
      message: {
        id: 'totalRepairCases',
        defaultMessage: 'Total Repair Cases',
      },
      params: { scope: '' },
      path: '/cases/filter/all-repair-cases',
      slug: 'all-repair-cases',
      title: 'allRepairCases',
    },
    {
      count: '76',
      message: {
        id: 'downtime2Days',
        defaultMessage: 'Downtime > 2 Days',
      },
      params: { scope: 'downtime > 2 days' },
      path: '/cases/filter/downtime-greater-than-2-days',
      slug: 'downtime-greater-than-2-days',
      title: 'downtime2Days',
    },
  ],
  assets: [
    {
      count: '360',
      message: {
        id: 'totalAssets',
        defaultMessage: 'Total Assets',
      },
      params: { scope: '' },
      path: '/assets/filter/total-assets',
      slug: 'total-assets',
      title: 'totalAssets',
    },
  ],
});

test('getFavorite returns the expected matching result', () => {
  const result = getFavorite(favorites, 'cases', 'all-repair-cases');
  expect(result.get('title')).toEqual('allRepairCases');
});

test('getFavorite returns an empty Map with no match', () => {
  const result = getFavorite(favorites, 'noMatch', 'noSlugMatch');
  expect(result.toJS()).toEqual({});
});

/* ------------------------------- setFavoriteAndRender ---------------------------------- */

test('setFavoriteAndRender returns SetsLatestFavorite component with expected props', () => {
  const setLatestFavorite = createSpy();
  const compProps = {
    submenu: 'cases',
    match: {
      params: {
        slug: 'all-repair-cases',
      },
    },
  };
  const component = shallow(setFavoriteAndRender(
    favorites,
    setLatestFavorite,
    testComponent,
    compProps,
  ));
  expect(component).toBeA(testComponent);
  expect(setLatestFavorite).toHaveBeenCalledWith(favorites.get('cases').first());
  expect(component).toHaveProps(compProps);
});

/* ------------------------------- setSearchFavoriteAndRender ---------------------------------- */

test('setSearchFavoriteAndRender returns SetsLatestFavorite component with expected props', () => {
  const setLatestFavorite = createSpy();
  const compProps = {
    location: {
      pathname: 'pathname',
      search: '?search',
    },
  };
  const component = shallow(setSearchFavoriteAndRender(
    setLatestFavorite,
    testComponent,
    compProps,
  ));
  expect(component).toBeA(testComponent);
  expect(setLatestFavorite).toHaveBeenCalledWith(Map({
    path: `${compProps.location.pathname}${compProps.location.search}`,
    title: 'search-results',
    message: messages.searchResults,
  }));
  expect(component).toHaveProps(compProps);
});
