import {
  expect,
  test,
} from '__tests__/helpers/test-setup';

import {
  dataNotFound,
} from '../httpUtils';

test('dataNotFound does not fail if no response provided', () => {
  const result = dataNotFound();
  expect(result).toBe(undefined);
});

test('dataNotFound returns false if the status is less than 400', () => {
  const response = {
    status: 200,
  };
  const result = dataNotFound(response);
  expect(result).toBe(false);
});

test('dataNotFound returns true if the status is more than 400', () => {
  const response = {
    status: 400,
  };
  const result = dataNotFound(response);
  expect(result).toBe(true);
});
