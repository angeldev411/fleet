import moment from 'moment-timezone';

import {
  expect,
  shallow,
  test,
} from '__tests__/helpers/test-setup';

import {
  getAssetStatusText,
  getOdometerText,
  getSerialNumber,
} from '../asset';

test('getSerialNumber returns the serial number from VIN number', () => {
  const output = getSerialNumber('3GBKC34F41M111912');
  expect(output).toEqual('1M111912');
});

test('getAssetStatusText returns the correct text for campainStatus', () => {
  const statusText = getAssetStatusText('campaignStatus', 'red', 'Campaign');
  expect(statusText.props.defaultMessage).toEqual('campaign due');
});

test('getAssetStatusText returns the correct text for pmStatus', () => {
  const statusText = getAssetStatusText('pmStatus', 'green', 'Campaign');
  expect(statusText.props.defaultMessage).toEqual('current');
});

test('getAssetStatusText returns the correct text for warrantyStatus', () => {
  const statusText = getAssetStatusText('warrantyStatus', 'yellow', 'Campaign');
  expect(statusText.props.defaultMessage).toEqual('partially covered');
});

test('getAssetStatusText returns the provided placeholder text if no status is found', () => {
  const id = 'campaignStatus';
  const defaultMessage = 'Campaign';
  const statusText = getAssetStatusText('campaignStatus', 'purple', { id, defaultMessage });
  const outputComponent = shallow(statusText);
  expect(outputComponent).toContain(
    `FormattedMessage[id="${id}"][defaultMessage="${defaultMessage}"]`,
  );
});

test('getOdometerText returns a properly formatted reading value', () => {
  const odometerText = getOdometerText('1000000');
  expect(odometerText).toEqual('1,000,000');
});

test('getOdometerText returns a properly formatted date', () => {
  moment.tz.setDefault('UTC'); // sets the default zone to UTC for testing

  const odometer = {
    unit: 'kilometers',
    reading: '3242',
    readAt: '2017-01-10T16:46:22Z',
  };
  const odometer1 = {
    unit: 'kilometers',
    reading: '1333214',
    readAt: '2016-11-19T16:46:22Z',
  };
  const odometer2 = {
    unit: 'miles',
    reading: '100',
    readAt: '2015-07-09T16:46:22Z',
  };
  const output = getOdometerText(odometer.reading, odometer.unit, odometer.readAt);
  const output1 = getOdometerText(odometer1.reading, odometer1.unit, odometer1.readAt);
  const output2 = getOdometerText(odometer2.reading, odometer2.unit, odometer2.readAt);
  expect(output).toEqual('3,242 kilometers (10 Jan 2017)');
  expect(output1).toEqual('1,333,214 kilometers (19 Nov 2016)');
  expect(output2).toEqual('100 miles (9 Jul 2015)');

  // Set default zone to default
  moment.tz.setDefault(null);
});
