import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import mapWizardConfigToRegistry from '../mapWizardConfigToRegistry';

const testRegistry = {
  TEST: () => {},
  REVIEW: () => {},
  TEST_CONFIG_KEY: () => {},
};

test('mapWizardConfigToRegistry produces the expected map', () => {
  const testWizardConfig = {
    start: 'TEST',
    steps: {
      TEST: ['REVIEW'],
    },
  };


  const mappedConfig = mapWizardConfigToRegistry(
    testWizardConfig,
    'TEST_CONFIG_KEY',
    testRegistry,
  );

  expect(mappedConfig).toEqual({
    SpecificWizardController: testRegistry.TEST_CONFIG_KEY,
    start: 'TEST',
    steps: {
      TEST: {
        component: testRegistry.TEST,
        name: 'TEST',
        nextSteps: ['REVIEW'],
      },
      REVIEW: {
        component: testRegistry.REVIEW,
        name: 'REVIEW',
      },
    },
  });
});
