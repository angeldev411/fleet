import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import postalAddressFormatter from '../postalAddressFormatter';

test('postalAddressFormatter includes the street', () => {
  const fakeStreet = 'the street';
  expect(postalAddressFormatter({ street: fakeStreet })).toEqual([fakeStreet]);
});

test('postalAddressFormatter includes the second street line', () => {
  const fakeStreet1 = 'first street line';
  const fakeStreet2 = 'street line 2';
  expect(postalAddressFormatter({
    street: fakeStreet1,
    street2: fakeStreet2,
  })).toEqual([fakeStreet1, fakeStreet2]);
});

test('postalAddressFormatter properly collapses the city/state/zip', () => {
  expect(postalAddressFormatter({
    city: 'Richmond',
    region: 'VA',
    postalCode: '23219',
  })).toEqual('Richmond, VA 23219');
});

test('postalAddressFormatter handles missing data in city/state/zip', () => {
  expect(postalAddressFormatter({
    city: 'London',
    postalCode: 'SE1 7PB',
  })).toEqual('London SE1 7PB');
});
