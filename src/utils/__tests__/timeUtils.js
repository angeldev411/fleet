import {
  test,
  expect,
  spyOn,
} from '__tests__/helpers/test-setup';
import { tz } from 'moment-timezone';

import {
  formatDate,
  userTimeZone,
  convertRailsTz,
} from '../timeUtils';

// ------------------------- userTimeZone -------------------------

test('userTimeZone() returns the results of moment.tz.guess() if successful.', () => {
  const fakeResults = 'fake results';
  spyOn(tz, 'guess').andReturn(fakeResults);
  expect(userTimeZone()).toEqual(fakeResults);
});

test('userTimeZone() defaults to America/New_York if time zone cannot be determined', () => {
  spyOn(tz, 'guess').andReturn(null);
  expect(userTimeZone()).toEqual('America/New_York');
});

// ------------------------- formatDate -------------------------

test('formatDate formats the given date in the user time-zone', () => {
  const dateString = '2017-01-13T16:46:22-04:00';
  const formattedDate = formatDate(dateString);
  const zone = userTimeZone();
  const expectedDateString = tz(dateString, zone).format('D MMM YYYY, h:mm A z');
  expect(formattedDate).toEqual(expectedDateString);
});

test('formatDate can format a date in an alternative time-zone', () => {
  const dateString = '2017-12-31T11:12:13+06:00';
  const formattedDate = formatDate(dateString, { tz: 'Australia/Brisbane' });
  const expectedDateString = '31 Dec 2017, 3:12 PM AEST';
  expect(formattedDate).toEqual(expectedDateString);
});

test('formatDate can format a date with a different format', () => {
  const dateString = '2014-01-23T04:05:06-08:00';
  const formattedDate = formatDate(dateString, {
    format: 'h:mm:ss a, dddd, MMMM Do YYYY',
    tz: 'America/Los_Angeles',
  });
  const expectedDateString = '4:05:06 am, Thursday, January 23rd 2014';
  expect(formattedDate).toEqual(expectedDateString);
});

// ------------------------- getCurrentTime -------------------------

// TODO: find some way to "freeze" time - otherwise, the following test sometimes fails
//       because occasionally the expected and actual are a second or so apart
//
// test('getCurrentTime returns the current time in the current time-zone', () => {
//   const resultDate = getCurrentTime().toDate();
//   const expectedDate = new Date();
//   expect(resultDate).toEqual(expectedDate);
// });

// ------------------------- convertRailsTz -------------------------

test('convertRailsTz() returns the equivalent timezone for a Rails TZ', () => {
  expect(convertRailsTz('Pacific Time (US & Canada)')).toEqual('America/Los_Angeles');
  expect(convertRailsTz('Samoa')).toEqual('Pacific/Apia');
  expect(convertRailsTz('International Date Line West')).toEqual('Pacific/Midway');
});

test('convertRailsTz() returns the given original timezone for an unknown Rails TZ', () => {
  const unknownTz = 'Mare Tranquillitatis (The Moon)';
  expect(convertRailsTz(unknownTz)).toEqual(unknownTz);
});
