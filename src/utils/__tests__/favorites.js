import { Map } from 'immutable';
import { noop } from 'lodash';

import {
  test,
  expect,
  createSpy,
  withJSDomURL,
} from '__tests__/helpers/test-setup';

import {
  shouldClearAndLoadFavorites,
  clearAndLoadFavorites,
  loadFavorite,
  shouldBuildLoadingGhosts,
} from '../favorites';

/* --------------------------- shouldClearAndLoadFavorites ------------------------------- */

// Scenario: initial load on a hard coded favorite
test(
  'shouldClearAndLoadFavorites returns true if no prior favorite and new favorite',
  () => {
    const currentProps = { latestFavorite: {} };
    const nextProps = { latestFavorite: { title: 'totalRepairCases' } };
    expect(shouldClearAndLoadFavorites(currentProps, nextProps)).toEqual(true);
  },
);

// Scenario: transition from one favorite to another, clicking refresh, going from case found
// via favorite to a different favorite.
test(
  'shouldClearAndLoadFavorites returns true if prior favorite different from new favorite',
  () => {
    const currentProps = { latestFavorite: { title: 'totalRepairCases' } };
    const nextProps = { latestFavorite: { title: 'uptimeCases' } };
    expect(shouldClearAndLoadFavorites(currentProps, nextProps)).toEqual(true);
  },
);

// Scenario: first call on full page load with favorite that must be loaded from API
test(
  'shouldClearAndLoadFavorites returns false if no prior favorite and no new latest favorite',
  () => {
    const currentProps = { latestFavorite: {} };
    const nextProps = { latestFavorite: {} };
    expect(shouldClearAndLoadFavorites(currentProps, nextProps)).toBeFalsy();
  },
);

// Scenario: returning to the same favorite page
test(
  'shouldClearAndLoadFavorites returns false if prior favorite and new latestFavorite are the same',
  () => {
    const currentProps = { latestFavorite: { title: 'uptimeCases' } };
    const nextProps = { latestFavorite: { title: 'uptimeCases' } };
    expect(shouldClearAndLoadFavorites(currentProps, nextProps)).toBeFalsy();
  },
);

/* --------------------------- clearAndLoadFavorites ------------------------------- */

test(
  'clearAndLoadFavorites calls clearAction if favorites exist and favorite is changing', () => {
    const clearAction = createSpy();
    const loadAction = noop;
    const priorFavorite = {};
    const latestFavorite = { title: 'totalRepairCases' };
    clearAndLoadFavorites({ clearAction, loadAction, priorFavorite, latestFavorite });
    expect(clearAction).toHaveBeenCalled();
  },
);

test(
  'clearAndLoadFavorites does not call clearAction if favorites exist and favorite is not changing',
  () => {
    const clearAction = createSpy();
    const loadAction = noop;
    const priorFavorite = { title: 'totalRepairCases' };
    const latestFavorite = { title: 'totalRepairCases' };
    clearAndLoadFavorites({ clearAction, loadAction, priorFavorite, latestFavorite });
    expect(clearAction).toNotHaveBeenCalled();
  },
);

test(
  'clearAndLoadFavorites calls clear action if forceClear is true',
  () => {
    const clearAction = createSpy();
    const loadAction = noop;
    const priorFavorite = { title: 'totalRepairCases' };
    const latestFavorite = { title: 'totalRepairCases' };
    const forceClear = true;
    clearAndLoadFavorites({
      clearAction,
      loadAction,
      priorFavorite,
      latestFavorite,
      forceClear,
    });
    expect(clearAction).toHaveBeenCalled();
  },
);

test(
  'clearAndLoadFavorites calls load action with expected params if latestFavorite is present and url matches actual url',
  () => {
    const clearAction = noop;
    const loadAction = createSpy();
    const priorFavorite = { title: 'totalRepairCases' };
    const latestFavorite = {
      title: 'totalRepairCases',
      params: { scope: 'test' },
      path: '/cases/filter/all-repair-cases',
    };
    withJSDomURL('http://example.com/cases/filter/all-repair-cases', () => {
      clearAndLoadFavorites({
        clearAction,
        loadAction,
        priorFavorite,
        latestFavorite,
      });
      expect(loadAction).toHaveBeenCalledWith({
        scope: 'test',
        page: 1,
        per_page: 20,
      });
    });
  },
);

test(
  'clearAndLoadFavorites does not call load action with no latestFavorite present',
  () => {
    const clearAction = noop;
    const loadAction = createSpy();
    const priorFavorite = { title: 'totalRepairCases' };
    const latestFavorite = {};
    clearAndLoadFavorites({
      clearAction,
      loadAction,
      priorFavorite,
      latestFavorite,
    });
    expect(loadAction).toNotHaveBeenCalled();
  },
);

test(
  'clearAndLoadFavorites does not call load action if favorite url does not match actual url',
  () => {
    const clearAction = noop;
    const loadAction = createSpy();
    const priorFavorite = { title: 'totalRepairCases' };
    const latestFavorite = {
      title: 'totalRepairCases',
      params: { scope: 'test' },
      path: '/cases/filter/all-repair-cases',
    };
    withJSDomURL('http://example.com/assets/filter/all-assets', () => {
      clearAndLoadFavorites({
        clearAction,
        loadAction,
        priorFavorite,
        latestFavorite,
      });
      expect(loadAction).toNotHaveBeenCalled();
    });
  },
);

/* --------------------------- loadFavorite ------------------------------- */

test('loadFavorite passes through the favorite params to the loadAction', () => {
  const loadAction = createSpy();
  const theParams = { starsky: 'hutch' };
  const favorite = { params: theParams };
  loadFavorite({ favorite, loadAction });
  expect(loadAction).toHaveBeenCalled();
  expect(loadAction.calls[0].arguments[0]).toInclude(theParams);
});

test('loadFavorite defaults to the first page', () => {
  const loadAction = createSpy();
  loadFavorite({ favorite: {}, loadAction });
  expect(loadAction.calls[0].arguments[0]).toInclude({ page: 1 });
});

test('loadFavorite allows specifying a different page', () => {
  const loadAction = createSpy();
  const page = 234;
  loadFavorite({ favorite: {}, page, loadAction });
  expect(loadAction.calls[0].arguments[0]).toInclude({ page });
});

test('loadFavorite loads 20 results per page', () => {
  const loadAction = createSpy();
  loadFavorite({ favorite: {}, loadAction });
  expect(loadAction.calls[0].arguments[0]).toInclude({ per_page: 20 });
});

/* --------------------------- shouldBuildLoadingGhosts ------------------------------- */

test('shouldBuildLoadingGhosts returns true if requesting', () => {
  const value = shouldBuildLoadingGhosts({ requesting: true, favoritePagination: Map() });
  expect(value).toBe(true);
});

test(
  'shouldBuildLoadingGhosts returns true if currentCount is 0 and totalCount does not exist',
  () => {
    const value = shouldBuildLoadingGhosts({
      favoritePagination: Map(),
      currentCount: 0,
    });
    expect(value).toBe(true);
  },
);

test('shouldBuildLoadingGhosts returns false by default', () => {
  expect(shouldBuildLoadingGhosts({})).toBe(false);
});
