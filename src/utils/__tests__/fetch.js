import PromiseMock from 'promise-mock';

import {
  expect,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import {
  get,
  post,
} from '../fetch';

test.beforeEach(() => {
  PromiseMock.install();
});

test.afterEach(() => {
  PromiseMock.uninstall();
});

function spyOnFetch() {
  const response = new global.Response('{"hello":"world"}', {
    status: 200,
    headers: { 'Content-type': 'application/json' },
  });
  return spyOn(global, 'fetch').andReturn(Promise.resolve(response));
}

function authFake(accessToken) {
  return {
    authHeaders: () => ({ authorization: accessToken }),
  };
}

test('GET with auth (the default) includes access token headers', () => {
  const fetchSpy = spyOnFetch();

  const accessToken = 'test_GET_token';
  const authHeaderSpy = authFake(accessToken);

  get('http://my.api.com', { authHandler: authHeaderSpy });

  expect(fetchSpy.calls.length).toEqual(1);
  const fetchOpts = fetchSpy.calls[0].arguments[1];
  expect(fetchOpts.headers).toIncludeKey('authorization');
  expect(fetchOpts.headers.authorization).toEqual(accessToken);
});

test('GET with auth (explicit) and a query string', () => {
  const fetchSpy = spyOnFetch();

  const accessToken = 'test_GET_2_token';
  const authHeaderSpy = authFake(accessToken);

  const query = { ggg: 999 };
  const endpoint = 'http://my.api.com';

  // with a query-string and auth enabled
  get(endpoint, { query, auth: true, authHandler: authHeaderSpy });

  const url = fetchSpy.calls[0].arguments[0];
  expect(url).toEqual(`${endpoint}?ggg=999`);
  const fetchOpts = fetchSpy.calls[0].arguments[1];
  expect(fetchOpts.headers).toIncludeKey('authorization');
  expect(fetchOpts.headers.authorization).toEqual(accessToken);
});

test('GET with auth disabled does not include access token headers', () => {
  const fetchSpy = spyOnFetch();

  const authHeaderSpy = authFake('test_GET_3_token');

  get('http://my.api.com', { auth: false, authHandler: authHeaderSpy });

  expect(fetchSpy.calls.length).toEqual(1);
  const fetchOpts = fetchSpy.calls[0].arguments[1];
  expect(fetchOpts.headers).toNotIncludeKey('authorization');
});

test('POST with auth (explicit) and a query string', () => {
  const fetchSpy = spyOnFetch();

  const accessToken = 'test_POST_token_1';
  const authHeaderSpy = authFake(accessToken);

  const query = { abc: 123 };
  const endpoint = 'http://my.api.com';

  post(endpoint, { query, auth: true, authHandler: authHeaderSpy });

  const url = fetchSpy.calls[0].arguments[0];
  expect(url).toEqual(`${endpoint}?abc=123`);
  const fetchOpts = fetchSpy.calls[0].arguments[1];
  expect(fetchOpts.headers).toIncludeKey('authorization');
  expect(fetchOpts.headers.authorization).toEqual(accessToken);
});

test('POST with auth (default) and a request body', () => {
  const fetchSpy = spyOnFetch();

  const accessToken = 'test_POST_token_2';
  const authHeaderSpy = authFake(accessToken);

  const requestBody = { xyz: 777, 'with a space': 'and it works' };
  const requestAsJson = '{"xyz":777,"with a space":"and it works"}';
  const endpoint = 'http://my.api.com';

  // with a query-string and auth enabled
  post(endpoint, { requestBody, authHandler: authHeaderSpy });

  const url = fetchSpy.calls[0].arguments[0];
  expect(url).toEqual(url);
  const fetchOpts = fetchSpy.calls[0].arguments[1];
  expect(fetchOpts.headers).toIncludeKey('authorization');
  expect(fetchOpts.headers.authorization).toEqual(accessToken);
  expect(fetchOpts.body).toEqual(requestAsJson);
});

test('POST with auth disabled does not include access token headers', () => {
  const fetchSpy = spyOnFetch();
  const authHeaderSpy = authFake('test_POST_token_3');

  const query = { abc: 123 };
  post('http://my.api.com', { query, auth: false, authHandler: authHeaderSpy });

  expect(fetchSpy.calls.length).toEqual(1);
  const fetchOpts = fetchSpy.calls[0].arguments[1];
  expect(fetchOpts.headers).toNotIncludeKey('authorization');
});
