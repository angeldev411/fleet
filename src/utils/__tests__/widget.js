import React from 'react';

import { test, expect, shallow } from '__tests__/helpers/test-setup';

import { getOutputLink, getOutputText } from 'utils/widget';

const testData1 = {
  value: 'I am so cool!',
  placeholder: {
    id: 'placeholder1',
    defaultMessage: 'This is the placeholder 1',
  },
};

const testData2 = {
  value: (<div>test element</div>),
  placeholder: {
    id: 'placeholder2',
    defaultMessage: 'This is the placeholder 2',
  },
};

const testData3 = {
  value: '',
  placeholder: {
    id: 'placeholder3',
    defaultMessage: 'This is the placeholder 3',
  },
};

const testData4 = {
  value: null,
  placeholder: {
    id: 'placeholder4',
    defaultMessage: 'This is the placeholder 4',
  },
};

test('getOutputLink returns the placeholder element if the value is null', () => {
  const { id, defaultMessage } = testData4.placeholder;
  const outputComponent = shallow(getOutputText(testData4.value, testData4.placeholder));
  expect(outputComponent).toBeA('span');
  expect(outputComponent).toContain(
    `FormattedMessage[id="${id}"][defaultMessage="${defaultMessage}"]`,
  );
});

test('getOutputLink returns the link with the value if it is not null', () => {
  const outputLink = getOutputLink(testData1.value, testData1.placeholder);
  expect(outputLink.props.children).toEqual(testData1.value);
});

test('getOutputLink will add the proper href for a link if provided', () => {
  const route = '/link-route';
  const outputLink = getOutputLink(testData1.value, testData1.placeholder, route);
  expect(outputLink.props.href).toEqual(route);
});

test('getOutputText returns the value if it is not null', () => {
  const outputText = getOutputText(testData1.value, testData1.placeholder);
  expect(outputText).toEqual(testData1.value);
});

test('getOutputText returns the element if the value is set as an element', () => {
  const outputText = getOutputText(testData2.value, testData2.placeholder);
  expect(outputText).toEqual(testData2.value);
});

test('getOutputText returns the placeholder element if the value is null', () => {
  const { id, defaultMessage } = testData3.placeholder;
  const outputComponent = shallow(getOutputText(testData3.value, testData3.placeholder));
  expect(outputComponent).toBeA('span');
  expect(outputComponent).toContain(
    `FormattedMessage[id="${id}"][defaultMessage="${defaultMessage}"]`,
  );
});

test('getOutputText returns the placeholder element if the value is empty', () => {
  const { id, defaultMessage } = testData4.placeholder;
  const outputComponent = shallow(getOutputText(testData4.value, testData4.placeholder));
  expect(outputComponent).toBeA('span');
  expect(outputComponent).toContain(
    `FormattedMessage[id="${id}"][defaultMessage="${defaultMessage}"]`,
  );
});
