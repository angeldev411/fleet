import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import mapPageConfigToRegistry from '../mapPageConfigToRegistry';

const testRegistry = {
  TEST: () => {},
  TEST_CONFIG_KEY_DATA: () => {},
  TEST_QA: () => {},
};

test('mapPageConfigToRegistry produces the expected composition map', () => {
  const testPageConfig = {
    data: 'TEST_CONFIG_KEY_DATA',
    fixed: ['TEST'],
    quick_action_items: {
      left: ['TEST_QA'],
      right: null,
    },
    top: ['NO_MATCH'],
  };

  const mappedConfig = mapPageConfigToRegistry(
    testPageConfig,
    'TEST_CONFIG_KEY',
    testRegistry,
  );

  expect(mappedConfig).toEqual({
    data: { name: 'TEST_CONFIG_KEY_DATA', hoc: testRegistry.TEST_CONFIG_KEY_DATA },
    fixed: [{ name: 'TEST', composition: testRegistry.TEST }],
    quick_action_items: {
      left: [{ name: 'TEST_QA', composition: testRegistry.TEST_QA }],
      right: [],
    },
    top: [],
  });
});

test('mapPageConfigToRegistry defaults to pulling a data hoc from {key}_DATA', () => {
  const testPageConfig = {
    fixed: ['TEST'],
    quick_action_items: {
      left: ['TEST_QA'],
      right: null,
    },
    top: ['NO_MATCH'],
  };

  const mappedConfig = mapPageConfigToRegistry(
    testPageConfig,
    'TEST_CONFIG_KEY',
    testRegistry,
  );

  expect(mappedConfig).toEqual({
    data: { name: 'TEST_CONFIG_KEY_DATA', hoc: testRegistry.TEST_CONFIG_KEY_DATA },
    fixed: [{ name: 'TEST', composition: testRegistry.TEST }],
    quick_action_items: {
      left: [{ name: 'TEST_QA', composition: testRegistry.TEST_QA }],
      right: [],
    },
    top: [],
  });
});
