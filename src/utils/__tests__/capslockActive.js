import {
  test,
  expect,
} from '__tests__/helpers/test-setup';
import capslockActive from '../capslockActive';

test('true if shiftKey is pressed and letter is small', () => {
  const e = {
    key: 's',
    shiftKey: true,
  };
  expect(capslockActive(e)).toBe(true);
});

test('true if shiftKey is not pressed and letter is big', () => {
  const e = {
    key: 'B',
    shiftKey: false,
  };
  expect(capslockActive(e)).toBe(true);
});

test('false if shiftKey is pressed and letter is big', () => {
  const e = {
    key: 'B',
    shiftKey: true,
  };
  expect(capslockActive(e)).toBe(false);
});

test('false if shiftKey is not pressed and letter is small', () => {
  const e = {
    key: 's',
    shiftKey: false,
  };
  expect(capslockActive(e)).toBe(false);
});
