import { noop } from 'lodash';

import {
  test,
  expect,
} from '__tests__/helpers/test-setup';

import { createApiAction } from '../redux-api-actions';

test('createApiAction throws error if apiCall is not a function', () => {
  const types = ['request', 'success', 'failure'];
  expect(() => {
    createApiAction(types);
  }).toThrow('Expected apiCall to be a function');
});

test('createApiAction throws error if payloadCreator is not a function or null', () => {
  const types = ['request', 'success', 'failure'];
  const apiCall = noop;
  const payloadCreator = 'test';

  expect(() => {
    createApiAction(types, apiCall, payloadCreator);
  }).toThrow('Expected payloadCreator to be a function, undefined or null');
});

test('createApiAction sets the expected payload value if payloadCreator is empty', () => {
  const types = ['request', 'success', 'failure'];
  const apiCall = noop;
  const action = createApiAction(types, apiCall)();

  expect(action).toEqual({ types, apiCall });
});

test('createApiAction sets the expected payload value if payload provided to action', () => {
  const types = ['request', 'success', 'failure'];
  const apiCall = noop;
  const payload = { success: true };
  const action = createApiAction(types, apiCall)(payload);

  expect(action).toEqual({ types, apiCall, payload });
});

test('createApiAction sets the expected payload and error value if payload is error', () => {
  const types = ['request', 'success', 'failure'];
  const apiCall = noop;
  const payload = new Error('wtf?');
  const action = createApiAction(types, apiCall)(payload);

  expect(action).toEqual({ types, apiCall, error: true, payload });
});

test(
  'createApiAction sets the expected payload and error value if payload is error and payloadCreator defined',
  () => {
    const types = ['request', 'success', 'failure'];
    const apiCall = noop;
    const payload = new Error('wtf?');
    const action = createApiAction(
      types,
      apiCall,
      p => p,
    )(payload);
    expect(action).toEqual({ types, apiCall, error: true, payload });
  },
);

test('createApiAction sets the expected payload value if payloadCreator defined', () => {
  const types = ['request', 'success', 'failure'];
  const apiCall = noop;
  const payload = 'working for the weekend';
  const action = createApiAction(
    types,
    apiCall,
    p => p,
  )(payload);
  expect(action).toEqual({ types, apiCall, payload });
});

test('createApiAction sets the expected meta values when passed to action creator', () => {
  const types = ['request', 'success', 'failure'];
  const apiCall = noop;
  const meta = { status: 'awesome' };
  const payload = { success: true, meta };
  const action = createApiAction(types, apiCall, null, p => p.meta)(payload);

  expect(action).toEqual({ types, apiCall, payload, meta });
});
