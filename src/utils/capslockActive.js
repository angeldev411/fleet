const capslockActive = (event) => {
  const e = event || window.event;
  const charCode = e.key.charCodeAt(0);
  const shiftKey = e.shiftKey;

  const lowerCaseChar = charCode >= 97 && charCode <= 122;
  const upperCaseChar = charCode >= 65 && charCode <= 90;

  return !!((upperCaseChar && !shiftKey) || (lowerCaseChar && shiftKey));
};

export default capslockActive;
