import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  campaignStatusGreen: {
    id: 'AssetStatus.campaignStatus.green',
    defaultMessage: 'current',
  },
  campaignStatusRed: {
    id: 'AssetStatus.campaignStatus.red',
    defaultMessage: 'campaign due',
  },
  noValue: {
    id: 'App.noValue',
    defaultMessage: 'Not Provided',
  },
  pmStatusGreen: {
    id: 'AssetStatus.pmStatus.green',
    defaultMessage: 'current',
  },
  pmStatusYellow: {
    id: 'AssetStatus.pmStatus.yellow',
    defaultMessage: 'due',
  },
  pmStatusRed: {
    id: 'AssetStatus.pmStatus.red',
    defaultMessage: 'overdue',
  },
  warrantyStatusGreen: {
    id: 'AssetStatus.warrantyStatus.green',
    defaultMessage: 'fully covered',
  },
  warrantyStatusYellow: {
    id: 'AssetStatus.warrantyStatus.yellow',
    defaultMessage: 'partially covered',
  },
  warrantyStatusRed: {
    id: 'AssetStatus.warrantyStatus.red',
    defaultMessage: 'expired',
  },
  warrantyStatusGrey: {
    id: 'AssetStatus.warrantyStatus.grey',
    defaultMessage: 'no coverage',
  },
  searchResults: {
    id: 'favorites.searchResults',
    defaultMessage: 'Search Results',
  },
});

export default formattedMessages;
