import PropTypes from 'prop-types';

import registryTypes from 'setup/ComponentRegistry/propTypes';
import mapTypes from './mapping/propTypes';

export default {
  ...PropTypes,
  ...mapTypes,
  ...registryTypes,
};
