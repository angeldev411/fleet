/* eslint-disable import/prefer-default-export */

import qs from 'qs';

/**
 * Returns an object after parsing a query string of a url
 * @param {string} queryString
 * @returns {object} parsed object
 * @example parseQueryParams('?id=1234&param=hello') will return { id: '1234', param: 'hello' }
 */
export function parseQueryParams(queryString) {
  const paramString = queryString.split('?').pop();
  return qs.parse(paramString);
}
