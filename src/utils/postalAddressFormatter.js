/**
 * Takes an object containing address data fields and returns
 * an array of the formatted lines for displaying the address.
 *
 * Valid properties of the addressData:
 *  * street
 *  * street2
 *  * city
 *  * region (i.e. the state or province)
 *  * postalCode (i.e. ZIP code)
 *
 *  TODO: extend this to accept a country or country code
 *  TODO: support non-US address style formatting
 */

function postalAddressFormatter(addressData) {
  const output = [];
  if (addressData.street) {
    output.push(addressData.street);
  }
  if (addressData.street2) {
    output.push(addressData.street2);
  }
  if (addressData.city || addressData.region || addressData.postalCode) {
    let cityLine = [];
    if (addressData.city) {
      cityLine.push(addressData.city);
    }
    if (addressData.region) {
      cityLine.push(addressData.region);
    }
    if (cityLine.length > 0) {
      cityLine = [cityLine.join(', ')];
    }
    if (addressData.postalCode) {
      cityLine.push(addressData.postalCode);
    }
    output.push(cityLine.join(' '));
  }
  return output;
}

export default postalAddressFormatter;
