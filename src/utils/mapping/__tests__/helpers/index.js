import { noop } from 'lodash';

/*---------------------------------------------------------------------------
 * Various helpers that come into play for the testing of features that
 * use the Google Maps API.
 *---------------------------------------------------------------------------*/

/**
 * Build a mock for the Google Maps API DirectionsService.
 * @param {function} route
 */
export function mockDirectionsService({
  route = noop,
} = {}) {
  return () => ({
    route,
  });
}

/**
 * Build a mock for the Google Maps API Geocoder.
 * @param {function} geocode
 */
export function mockGeocoder({
  geocode = noop,
} = {}) {
  return () => ({
    geocode,
  });
}

/**
 * A mock for the Google Maps API LatLng function.
 * @param {number} lat
 * @param {number} lng
 */
export function mockLatLng(lat, lng) {
  return { lat, lng };
}

/* constants for various return status values from the API */

export const DIRECTIONS_OK = 'direction status ok';
export const GEOCODER_OK = 'geocoder ok';
export const TRAVEL_MODE_DRIVING = 'travel mode driving';

/**
 * Build a mock for the entire Google Maps API (or at least the parts we use within
 * this application).
 *
 * @param DirectionsService
 * @param Geocoder
 * @param LatLng
 * @returns {Object}
 */
export function mockGoogleMaps({
  DirectionsService = mockDirectionsService(),
  Geocoder = mockGeocoder(),
  LatLng = mockLatLng,
} = {}) {
  return {
    DirectionsService,
    DirectionsStatus: {
      OK: DIRECTIONS_OK,
    },
    Geocoder,
    GeocoderStatus: {
      OK: GEOCODER_OK,
    },
    LatLng,
    TravelMode: {
      DRIVING: TRAVEL_MODE_DRIVING,
    },
  };
}

/**
 * Return a dummy "DirectionsResult" object for test purposes.
 * @returns {Object}
 */
export function mockDirectionResult() {
  return {
    routes: [{
      legs: [{
        distance: {
          text: '3 miles',
          value: 3,
        },
        duration: {
          text: '5 minutes',
          value: 5,
        },
      }],
    }],
    status: DIRECTIONS_OK,
  };
}
