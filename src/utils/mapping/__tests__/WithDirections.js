import React from 'react';
import {
  createSpy,
  expect,
  shallow,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import { mockGoogleMaps, mockDirectionsService, mockLatLng, DIRECTIONS_OK } from './helpers';
import { WithDirections } from '../WithDirections';

const renderFn = createSpy().andReturn(null);

const markerOne = { position: { lat: 123.45, lng: 88.76 } };
const markerTwo = { position: { lat: 55.432, lng: 66.789 } };

const defaultProps = {
  markers: [markerOne, markerTwo],
  render: renderFn,
  googleMaps: mockGoogleMaps(),
};

function shallowRender(props = defaultProps) {
  return shallow(<WithDirections {...props} />);
}

test('it calculates directions from the first marker to the second marker', () => {
  const route = createSpy();
  const component = shallowRender({
    ...defaultProps,
    googleMaps: mockGoogleMaps({
      DirectionsService: mockDirectionsService({ route }),
    }),
  });

  component.instance().componentDidMount();

  const routeCalls = route.calls;
  expect(routeCalls.length).toEqual(1);

  const directionsArgs = routeCalls[0].arguments[0];
  expect(directionsArgs.origin).toEqual(
    mockLatLng(markerOne.position.lat, markerOne.position.lng));
  expect(directionsArgs.destination).toEqual(
    mockLatLng(markerTwo.position.lat, markerTwo.position.lng));
});

test('the directions service is not called if given only one marker', () => {
  const route = createSpy();
  const component = shallowRender({
    ...defaultProps,
    markers: [markerOne],
    googleMaps: mockGoogleMaps({
      DirectionsService: mockDirectionsService({ route }),
    }),
  });

  component.instance().componentDidMount();

  expect(route).toNotHaveBeenCalled();
});

test('nothing fails if maps API is not available', () => {
  expect(() => {
    const component = shallowRender({
      ...defaultProps,
      googleMaps: null,
    });

    component.instance().componentDidMount();
  }).toNotThrow();
});

/**
 * Helper used by the following tests for creating a Google Maps API mock with a spied-upon
 * directions route function.
 */
function mapWithRouteSpy(result, status) {
  const route = (inputs, callback) => callback(result, status);
  return mockGoogleMaps({
    DirectionsService: mockDirectionsService({ route }),
  });
}

test('if the routing is OK, the state is set to the directions', () => {
  spyOn(console, 'error');
  const result = 'direction routing results';

  const component = shallowRender({
    ...defaultProps,
    googleMaps: mapWithRouteSpy(result, DIRECTIONS_OK),
  });

  component.instance().componentDidMount();

  expect(renderFn).toHaveBeenCalledWith({ directions: result });
  expect(console.error).toNotHaveBeenCalled();
});

test('an error is logged if the routing is not OK', () => {
  spyOn(console, 'error');
  const result = 'direction routing results';

  const component = shallowRender({
    ...defaultProps,
    googleMaps: mapWithRouteSpy(result, 'BORKED'),
  });

  component.instance().componentDidMount();

  expect(renderFn).toHaveBeenCalledWith({ directions: null });
  expect(console.error).toHaveBeenCalled();
});

test('the onDirectionsUpdate prop is called with the direction routing results', () => {
  const onDirectionsUpdate = createSpy();
  const result = 'direction routing results';

  const component = shallowRender({
    ...defaultProps,
    onDirectionsUpdate,
    googleMaps: mapWithRouteSpy(result, DIRECTIONS_OK),
  });

  component.instance().componentDidMount();

  expect(onDirectionsUpdate).toHaveBeenCalledWith(result);
});
