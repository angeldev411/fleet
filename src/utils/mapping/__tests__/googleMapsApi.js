import {
  expect,
  test,
} from '__tests__/helpers/test-setup';

import googleMapsApi from '../googleMapsApi';

test('fails if not provided an apiKey or clientID', () => {
  expect(() => {
    googleMapsApi({ apiKey: null, clientID: null });
  }).toThrow(/requires a either an apiKey or a clientID/);
});

test('fails if provided both an apiKey AND a clientID', () => {
  expect(() => {
    googleMapsApi({ apiKey: 'abc', clientID: '123' });
  }).toThrow(/cannot accept BOTH an apiKey and a clientID/);
});

test('includes the apiKey in the final URL', () => {
  expect(googleMapsApi({ apiKey: 'my-api-key' })).toMatch(/\?.*key=my-api-key/);
});

test('defaults to the current GOOGLE_MAPS_API_KEY', () => {
  process.env.GOOGLE_MAPS_API_KEY = 'test-api-key';
  expect(googleMapsApi()).toMatch(/\?.*key=test-api-key/);
});

test('defaults to version 3.exp', () => {
  expect(googleMapsApi()).toMatch(/\?.*v=3.exp/);
});

test('includes expected default libraries while passing through version argument', () => {
  const url = googleMapsApi({ version: 'foo' });
  expect(url).toMatch(/\?.*libraries=geometry%2Cdrawing%2Cplaces/);
  expect(url).toMatch(/\?.*v=foo/);
});
