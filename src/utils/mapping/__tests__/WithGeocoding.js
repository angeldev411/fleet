import React from 'react';
import {
  createSpy,
  expect,
  shallow,
  spyOn,
  test,
} from '__tests__/helpers/test-setup';

import { mockGoogleMaps, mockGeocoder, GEOCODER_OK } from './helpers';
import { WithGeocoding } from '../WithGeocoding';

const renderFn = createSpy().andReturn(null);

const LAT = 123.45;
const LON = 88.76;

const intlMock = {
  formatMessage: message => message.defaultMessage,
};

const defaultProps = {
  location: { lat: `${LAT}`, lon: `${LON}` },
  render: renderFn,
  googleMaps: mockGoogleMaps(),
  intl: intlMock,
};

function shallowRender(props = defaultProps) {
  return shallow(<WithGeocoding {...props} />);
}

test('it geocodes the location when props are set', () => {
  const component = shallowRender();

  const geocode = createSpy();
  const props = {
    ...defaultProps,
    googleMaps: mockGoogleMaps({
      Geocoder: mockGeocoder({ geocode }),
    }),
  };

  component.setProps(props);

  const geocodeCalls = geocode.calls;
  expect(geocodeCalls.length).toEqual(1);

  const geocodeArgs = geocodeCalls[0].arguments[0];
  expect(geocodeArgs.location).toEqual({ lat: LAT, lng: LON });
});

test('geocoding does not happen if there is no location', () => {
  const component = shallowRender();

  const geocode = createSpy();
  const props = {
    ...defaultProps,
    location: undefined,
    googleMaps: mockGoogleMaps({
      Geocoder: mockGeocoder({ geocode }),
    }),
  };

  component.setProps(props);
  expect(geocode).toNotHaveBeenCalled();
});

test('geocoding does not happen if the location is not changing', () => {
  const component = shallowRender();

  const geocode = createSpy();
  const props = {
    ...defaultProps,
    googleMaps: mockGoogleMaps({
      Geocoder: mockGeocoder({ geocode }),
    }),
    onAddressUpdate: null,
  };

  component.setProps(props);
  expect(geocode.calls.length).toEqual(1);
  geocode.reset();

  // change some other prop, but not the location
  component.setProps({
    ...props,
    onAddressUpdate: () => 'foo',
  });

  expect(geocode).toNotHaveBeenCalled();
});

test('geocoding does not happen if geocoding is already in progress', () => {
  const component = shallowRender();

  const geocode = createSpy();
  const props = {
    ...defaultProps,
    googleMaps: mockGoogleMaps({
      Geocoder: mockGeocoder({ geocode }),
    }),
  };

  component.instance().geocoding = true;
  component.setProps(props);
  expect(geocode).toNotHaveBeenCalled();
});

test('nothing fails if maps API is not available', () => {
  expect(() => {
    const component = shallowRender();
    component.setProps({
      ...defaultProps,
      googleMaps: null,
    });
  }).toNotThrow();
});

/**
 * Helper used by the following tests for creating a Google Maps API mock with a spied-upon
 * geocoder function.
 */
function mapWithGeocodeSpy(address, status) {
  const result = [
    { formatted_address: address },
    { formatted_address: '2nd Address' },
  ];

  const geocode = (inputs, callback) => callback(result, status);
  return mockGoogleMaps({
    Geocoder: mockGeocoder({ geocode }),
  });
}

test('if the geocode is OK, the state is set to the first formatted address', () => {
  spyOn(console, 'error');
  const formattedAddress = '123 Main St';
  const component = shallowRender();

  component.setProps({
    ...defaultProps,
    googleMaps: mapWithGeocodeSpy(formattedAddress, GEOCODER_OK),
  });

  expect(renderFn).toHaveBeenCalledWith({ address: formattedAddress });
  expect(console.error).toNotHaveBeenCalled();
});

test('an error is logged if the geocoding is not OK', () => {
  spyOn(console, 'error');
  const component = shallowRender();

  component.setProps({
    ...defaultProps,
    googleMaps: mapWithGeocodeSpy('123 Main St', 'EPIC FAILZ'),
  });

  expect(renderFn).toHaveBeenCalledWith({ address: `${LAT}, ${LON}` });
  expect(console.error).toHaveBeenCalled();
});

test('the onAddressUpdate prop is called with the geocoded address', () => {
  const onAddressUpdate = createSpy();
  const formattedAddress = '123 Main St';

  const component = shallowRender();

  component.setProps({
    ...defaultProps,
    onAddressUpdate,
    googleMaps: mapWithGeocodeSpy(formattedAddress, GEOCODER_OK),
  });

  expect(onAddressUpdate).toHaveBeenCalledWith(formattedAddress);
});
