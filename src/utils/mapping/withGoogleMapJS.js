import React from 'react';
import { withScriptjs } from 'react-google-maps';
import { FormattedMessage } from 'react-intl';
import { compose, withProps, mapProps } from 'recompose';

import googleMapsApi from './googleMapsApi';
import messages from './messages';

// ------------------------ withGoogleMapJS ------------------------

/**
 * `withGoogleMapJS` is a simple extension to react-google-maps' `withScriptjs` which
 * ensures that a wrapped component has access to the Google Maps API. The wrapped
 * component will not be rendered until the API's JavaScript has been loaded and the
 * global `google.maps` API is available.
 */
const withGoogleMapJS = compose(
  withProps(() => ({
    // props for withScriptJs:
    googleMapURL: googleMapsApi(),
    loadingElement: (
      <div style={{ height: '100%' }}>
        <FormattedMessage {...messages.loading} />
      </div>
    ),
  })),
  withScriptjs,
  /* istanbul ignore next */
  mapProps(ownerProps => ({
    // provide the Google Maps API as a prop to avoid using the global and
    // to allow test injection:
    googleMaps: window && window.google && window.google.maps,
    // provide a useful default for `location`:
    location: { lat: null, lon: null },
    // allow incoming props to override either of those:
    ...ownerProps,
  })),
);

export default withGoogleMapJS;
