import { noop } from 'lodash';
import React from 'react';
import { injectIntl } from 'react-intl';
import { compose } from 'recompose';

import PropTypes from 'utils/prop-types';

import messages from './messages';
import withGoogleMapJS from './withGoogleMapJS';

/**
 * `WithGeocoding` is a render-prop component that has a location prop
 * containing lat/lon values, and will provide reverse-geocoding of the
 * address of this lat/lon. On each prop update, if the lat/lon has changed
 * (or no geocoding has yet happened), the HOC starts a geocode process,
 * and upon successful completion does two things:
 * 1. provides an `address` prop value to the wrapped component, set
 *    to the first formatted address provided by the geocoder
 * 2. if an `onAddressUpdate` function prop is provided, calls this
 *    function with that formatted address as well
 */
export class WithGeocoding extends React.Component {
  static propTypes = {
    googleMaps: PropTypes.googleMapsApi,
    location: PropTypes.shape({
      lat: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      lon: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    }),
    onAddressUpdate: PropTypes.func,
    render: PropTypes.func.isRequired,
    intl: PropTypes.shape({
      formatMessage: PropTypes.func.isRequired,
    }).isRequired,
  };

  static defaultProps = {
    googleMaps: null,
    location: {},
    onAddressUpdate: noop,
  };

  state = {
    address: undefined,
  };

  componentWillReceiveProps(nextProps) {
    const {
      location: { lat: oldLat, lon: oldLon } = {},
    } = this.props;
    const {
      location: { lat, lon } = {},
      onAddressUpdate,
      googleMaps,
      intl,
    } = nextProps;

    if (!googleMaps) {
      // mapping API has not yet been initialized
      return;
    }

    // unless we're currently geocoding, then if we have a location and it is different
    // from the previous location (or the current address has not been set yet), then
    // let's geocode now...
    const currentAddress = this.state && this.state.address;
    if (!this.geocoding && lat && lon && (!currentAddress || oldLat !== lat || oldLon !== lon)) {
      this.geocoding = true;
      this.setAddress(intl.formatMessage(messages.loading));

      const geocoder = new googleMaps.Geocoder();
      const location = { lat: parseFloat(lat), lng: parseFloat(lon) };

      // kick off the (asynchronous) geocode process:
      geocoder.geocode({
        location,
      }, (result, status) => {
        this.geocoding = false;
        if (status === googleMaps.GeocoderStatus.OK) {
          const address = result[0].formatted_address;
          // set the address in state so it is passed down to the child component
          this.setAddress(address);
          // call the callback so the address is passed up to any interested parent components
          onAddressUpdate(address);
        } else {
          this.setAddress(`${lat}, ${lon}`);
          console.error(`Geocoder failed for [${lat}, ${lon}]: `, status);
        }
      });
    }
  }

  setAddress = (address) => {
    this.setState({ address });
  };

  render() {
    return this.props.render(this.state);
  }
}

export default compose(
  withGoogleMapJS,
  injectIntl,
)(WithGeocoding);
