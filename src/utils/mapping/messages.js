import { defineMessages } from 'react-intl';

const formattedMessages = defineMessages({
  loading: {
    id: 'utils.mapping.loading',
    defaultMessage: 'Loading...',
  },
});

export default formattedMessages;
