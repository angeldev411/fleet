import { noop } from 'lodash';
import React from 'react';
import {
  withGoogleMap,
} from 'react-google-maps';
import {
  compose,
  withProps,
} from 'recompose';

import PropTypes from 'utils/prop-types';

import withGoogleMapJS from './withGoogleMapJS';

/**
 * `withDirections` is a render-prop component that can be applied to
 * a mapping component to provide directions between two markers. On
 * mount, the HOC starts a DirectionsService route process, and upon
 * successful completion does two things:
 * 1. provides a `directions` prop value to the wrapped component, set
 *    to the result from the DirectionsService
 * 2. if an `onDirectionsUpdate` function prop is provided, calls this
 *    function with those results as well
 */
export class WithDirections extends React.Component {
  static propTypes = {
    markers: PropTypes.arrayOf(PropTypes.shape({
      position: PropTypes.latLng.isRequired,
    })).isRequired,
    onDirectionsUpdate: PropTypes.func,
    render: PropTypes.func.isRequired,
    googleMaps: PropTypes.googleMapsApi,
  };

  static defaultProps = {
    onDirectionsUpdate: noop,
    googleMaps: null,
  };

  state = {
    directions: null,
  };

  componentDidMount() {
    const {
      markers: [markerOne, markerTwo],
      onDirectionsUpdate,
      googleMaps,
    } = this.props;
    if (googleMaps && markerOne && markerTwo) {
      const DirectionsService = new googleMaps.DirectionsService();

      const start = markerOne.position;
      const end = markerTwo.position;

      const origin = new googleMaps.LatLng(start.lat, start.lng);
      const destination = new googleMaps.LatLng(end.lat, end.lng);
      // kick off the routing (which is asynchronous):
      DirectionsService.route({
        origin,
        destination,
        travelMode: googleMaps.TravelMode.DRIVING,
      }, (result, status) => {
        if (status === googleMaps.DirectionsStatus.OK) {
          // set the directions in state so they're passed down to the child component
          this.setState({ directions: result });
          // call the callback so directions are passed up to any interested parent components
          onDirectionsUpdate(result);
        } else {
          console.error(`error fetching directions ${result}`);
        }
      });
    }
  }

  render() {
    return this.props.render(this.state);
  }
}

export default compose(
  withProps({
    // props for withGoogleMap:
    containerElement: <div style={{ height: '100%' }} />,
    mapElement: <div style={{ height: '100%' }} />,
  }),
  withGoogleMapJS,
  withGoogleMap,
)(WithDirections);
