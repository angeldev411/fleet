import { stringify } from 'qs';
import Bugsnag from 'bugsnag-js';
import { curry, isEmpty } from 'lodash';

import Authentication from 'plugins/Authentication';

const API_VERSION = 1;
const ACCEPT_HEADER = { Accept: `application/json; version=${API_VERSION}` };
const CONTENT_TYPE_HEADER = { 'Content-type': 'application/json' };

function makeApiUrl(endpoint, query) {
  const queryString = stringify(query);
  return `${endpoint}${queryString ? '?' : ''}${queryString}`;
}

function checkStatus(response) {
  if (response.status < 400) {
    return response;
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

function processError({ endpoint, query, auth, url, method }) {
  const metadata = {
    endpoint,
    query,
    auth,
    url,
    method,
  };

  // const name = `API Error: ${method} ${endpoint}`;
  const severity = 'error';

  return (error) => {
    // console.error(`[FETCH] ${name}`, error);
    const message = error.message || `${error.response.status} error calling ${endpoint}`;
    Bugsnag.notifyException(error, message, metadata, severity);
    return ({ error });
  };
}

function processResponse(response) {
  return response
    .json()
    .then(body => ({
      response: {
        headers: response.headers,
        body,
        status: response.status,
      },
    }));
}

function buildFetchArgs({
  method, auth, headers = {}, requestBody = {}, authHandler = Authentication,
}) {
  const fetchOpts = {
    method,
    headers: {
      ...ACCEPT_HEADER,
      ...CONTENT_TYPE_HEADER,
      ...(auth ? authHandler.authHeaders() : {}),
      ...headers,
    },
  };
  return isEmpty(requestBody) ? fetchOpts : { ...fetchOpts, body: JSON.stringify(requestBody) };
}

function execute(method, endpoint, {
  query, auth = true, headers = {}, requestBody = {}, authHandler = Authentication,
} = {}) {
  const url = makeApiUrl(endpoint, query);
  const options = buildFetchArgs({ method, auth, headers, requestBody, authHandler });
  return fetch(url, options)
    .then(checkStatus)
    .then(processResponse)
    .catch(processError({ endpoint, query, auth, url, method }));
}

// These are the functions that are exported for use:
export { makeApiUrl };
export const get = curry(execute)('GET');
export const post = curry(execute)('POST');
export const patch = curry(execute)('PATCH');
export const put = curry(execute)('PUT');
// Any other functions exported above are only exported for test purposes.
