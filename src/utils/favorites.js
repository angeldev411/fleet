/**
 * How many favorites to load per page.
 */
const RESULTS_PER_PAGE = 20;

/**
 * Determine if a favorite is currently present and has changed. This is a helper method that
 * assists with favorite data caching.
 * @param  {Object}  oldFavorite A favorite, converted from a Map into a JS object.
 * @param  {Object}  newFavorite A favorite, converted from a Map into a JS object.
 * @return {Boolean} Indicates whether the latest favorite is present and new.
 */
function isFavoriteChanging(oldFavorite, newFavorite) {
  return newFavorite.title && oldFavorite.title !== newFavorite.title;
}

/**
 * Called from componentWillReceiveProps with the current and next props values to evaluate
 * whether the favorites should be cleared and/or loaded.
 * @param  {Object} currentProps The current props of the calling component, must have
 * latestFavorite
 * @param  {Object} nextProps    The new props of the calling component, must have latestFavorite
 * @return {Boolean} Indicates whether the favorites should be cleared and/or loaded.
 */
export function shouldClearAndLoadFavorites(currentProps, nextProps) {
  const newFavorite = nextProps.latestFavorite;
  const oldFavorite = currentProps.latestFavorite;
  return isFavoriteChanging(oldFavorite, newFavorite);
}

/**
 * Load a page of the results for a given favorite from the API.
 * @param favorite A "favorite" containing params for loading the favorite
 * @param loadAction A function that triggers loading the favorite when called
 * @param page Which page of favorite results to retrieve (defaults to the first page).
 */
export function loadFavorite({ favorite, loadAction, page = 1 }) {
  loadAction({ ...(favorite.params || {}), page, per_page: RESULTS_PER_PAGE });
}

/**
 * Performs logic to determine is passed in clearAction and/or loadAction should be called, and
 * calls them accordingly.
 * @param {Object} args An object containing the following keys:
 *   clearAction: A function that clears the favorites when called.
 *   loadAction: A function that triggers loading the favorites when called.
 *   priorFavorite: The priorFavorite.
 *   latestFavorite: The latestFavorite.
 *   favorites: The List of favorites, used to determine presence.
 *   forceClear: A boolean that will force the clear action to fire if true.
 */
export function clearAndLoadFavorites({
  clearAction,
  loadAction,
  priorFavorite,
  latestFavorite,
  forceClear,
}) {
  if (forceClear || isFavoriteChanging(priorFavorite, latestFavorite)) {
    clearAction();
  }

  // The title must exist, and the favorite's pathname must match the actual url before the API
  // is called. This prevents calling an incorrect API endpoint on componentDidMount.
  if (latestFavorite.title && latestFavorite.path === window.location.pathname) {
    loadFavorite({ favorite: latestFavorite, loadAction });
  }
}

/**
 * Determine if the ghost style loading indicators should be added to the favorites display
 * @param  {Object} args An object containing the required arguments for evaluation. Required keys:
 * currentCount {Number} The current number of favorites stored in state.
 * favoritePagination {ImmutableMap} A Map containing data concerning pagination of the favorite.
 * requesting {Boolean} Indicates if a request is currently active.
 * @return {Boolean}
 */
export function shouldBuildLoadingGhosts({ currentCount, favoritePagination, requesting }) {
  const totalCount = favoritePagination && favoritePagination.get('totalCount');
  if (requesting || (currentCount === 0 && !totalCount)) {
    return true;
  }
  return false;
}
