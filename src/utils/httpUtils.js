export function httpBadRequest({ status } = {}) {
  return status && status >= 400;
}

export function dataNotFound(result) {
  return httpBadRequest(result);
}
