import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropType from 'react-immutable-proptypes';
import { List, Map } from 'immutable';
import { curry } from 'lodash';

import messages from './messages';

/**
 * SetsLatestFavorite is an HOC React component whose sole purpose is to dispatch a redux
 * action on certain lifecycle events.
 * @type {Component}
 */
export class SetsLatestFavorite extends React.Component {
  static propTypes = {
    Component: PropTypes.func.isRequired,
    componentProps: PropTypes.shape().isRequired,
    favorite: ImmutablePropType.map.isRequired,
    setLatestFavorite: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.setNewFavorite();
  }

  componentDidUpdate() {
    this.setNewFavorite();
  }

  // setLatestFavorite is a redux action. Calling it outside of a react component throws errors
  // in the console.
  setNewFavorite = () => {
    const {
      favorite,
      setLatestFavorite,
    } = this.props;
    setLatestFavorite(favorite);
  }

  render() {
    const {
      Component,
      componentProps,
    } = this.props;
    return <Component {...componentProps} />;
  }
}

/**
 * Looks up the filter currently being viewed based on a url match with the slugs in the favorites.
 * @param  {Map} favorites   The Dashboard API response after processing and being stored in state.
 * @param  {string} submenu  The name of the submenu
 * @param  {string} slug     The slug as extracted from the current URL
 * @return {Map}             The matching favorite, or an empty Map.
 */
export function getFavorite(favorites, submenu, slug) {
  const subFavorites = favorites.get(submenu) || List();
  const index = subFavorites.findIndex(f => f.get('slug') === slug);

  if (index < 0) {
    return Map();
  }

  return subFavorites.get(index);
}

export const renderPageFromConfig = curry(
  (PageComponent, configKey, otherProps) => (
    <PageComponent
      configKey={configKey}
      {...otherProps}
    />
  ),
);

/**
 * Curried function that locates active favorite and returns an HOC for managing it in state.
 */
export const setFavoriteAndRender = curry(
  (favorites, setLatestFavorite, Component, componentProps) => {
    const {
      submenu,
      match: {
        params: {
          slug,
        },
      },
    } = componentProps;
    const favorite = getFavorite(favorites, submenu, slug);

    let isValidSlug = true;

    if (favorites.size !== 0 && favorite.size === 0) {
      isValidSlug = false;
    }

    return (
      <SetsLatestFavorite
        favorite={favorite}
        setLatestFavorite={setLatestFavorite}
        Component={Component}
        componentProps={{ ...componentProps, isValidSlug }}
      />
    );
  },
);

export const setSearchFavoriteAndRender = curry(
  (setLatestFavorite, Component, componentProps) => {
    const {
      location: {
        pathname,
        search,
      },
    } = componentProps;

    const favorite = Map({
      path: `${pathname}${search}`,
      title: 'search-results',
      message: messages.searchResults,
    });

    return (
      <SetsLatestFavorite
        favorite={favorite}
        setLatestFavorite={setLatestFavorite}
        Component={Component}
        componentProps={componentProps}
      />
    );
  },
);
