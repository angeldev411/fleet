
export default function cardType(approvalStatus) {
  switch (approvalStatus && approvalStatus.toLowerCase()) {
    case 'approved':
      return 'statusSuccess';
    case 'declined':
      return 'statusDanger';
    case 'pending':
      return 'statusWarning';
    case 'requested':
      return 'statusWarning';
    default:
      return '';
  }
}
