import {
  compact,
  isArray,
  isNull,
  isString,
  mapValues,
} from 'lodash';

import componentRegistry from 'setup/ComponentRegistry';

/**
 * `mapPageConfigToRegistry` is a memoized closure that efficiently handles converting the
 * composition names stored in the runtime config to a map of registered composition objects.
 * An input config will probably look like:
 * {
 *   data: 'wrapper_hoc_data',
 *   fixed: ['feature1']
 *   quick_action_items: {
 *     left: ['button1'],
 *     right: ['button2', 'button3'],
 *   },
 *   top: null,
 *   // etc...
 * }
 *
 * You should expect to get back from this config:
 * {
 *   data: {name: wrapper_hoc_data, hoc: REACT COMPONENT},
 *   fixed: [{name: 'feature1', composition: REACT COMPONENT}],
 *   quick_action_items: {
 *     left: [{name: 'button1', composition: REACT COMPONENT}],
 *     right: [
 *       {name: 'button2', composition: REACT COMPONENT},
 *       {name: 'button3', composition: REACT COMPONENT},
 *     ],
 *   },
 *   top: [],
 *   // etc...
 * }
 *
 * When calling this function, you must also provide the configKey that was used.
 * This enables the caching.
 */
const mapPageConfigToRegistry = (() => {
  // Setup the cache. This will hold previously run configuration maps.
  const cache = {};

  const process = (config, key, registry = componentRegistry) => {
    if (key && cache[key]) {
      // This key has already been processed. Return previous results.
      return cache[key];
    }

    // The config currently being processed is `null` if no definition was provided.
    if (isNull(config)) {
      return [];
    }

    // The config currently being processed is a string in cases where a
    // data wrapping HOC is defined.
    if (isString(config)) {
      return { name: config, hoc: registry[config] };
    }

    // If the config currently being processed is an array, return a collection
    // with each object containing the following values:
    // the constant it was found under (key: name)
    // the composition from the registry (key: composition)
    if (isArray(config)) {
      return compact(config.map((name) => {
        const composition = registry[name];
        return composition && { name, composition };
      }));
    }

    // The config currently being processed is an object, handle processing it recursively.
    // Note that no `key` is being used, so this call will not check or save to the cache.
    const obj = mapValues(config, value => process(value, undefined, registry));

    // Only the initial call contains a `key`. This saves the result of the initial call
    // to the cache under the key.
    if (key) {
      // If no `data` key was provided in the config, fallback to retrieving the data hoc under
      // `${PAGE_NAME}_DATA`.
      if (!obj.data && registry[`${key}_DATA`]) {
        obj.data = { name: `${key}_DATA`, hoc: registry[`${key}_DATA`] };
      }

      cache[key] = obj;
    }

    return obj;
  };

  return process;
})();

export default mapPageConfigToRegistry;
