import React from 'react';
import { FormattedMessage } from 'react-intl';

import { A } from 'base-components';
import EmptyValue from 'elements/EmptyValue';

import messages from './messages';

/*
 * This is a utility function for returning the value when it is valid
 * and EmptyValue placeholder when its value is invalid (null or empty).
 *
 * params:
 *   value (string): Value to be assessed
 *   placeholder (object): `messageDescriptorPropTypes` shaped object
 *     to be used in the FormattedMessage element as the placeholder
 *     in place of the null value
 *
 * return value:
 *   - If the value is valid, it simply returns the value
 *   - If it is null or empty, it returns a FormattedMessage that has
 *     the placeholder as its content. EmptyValue is simply a styled-component
 *     wrapper for styling the placeholder as an italic, light, gray text.
 *
 * Example:
 *   // messages.js
 *   const messages = defineMessages({
 *     unitNumber: {
 *       id: 'unitNumber',
 *       defaultMessage: 'Unit',
 *     },
 *   });
 *
 *   // widget.js
 *   getOutputText('12345', messages.unitNumber); // Displays '12345'
 *   getOutputText('', messages.unitNumber); // Displays 'Unit' in italic gray font
 */

export function renderEmptyValue(placeholder) {
  return (
    <EmptyValue>
      <FormattedMessage {...placeholder} />
    </EmptyValue>
  );
}

export function getOutputText(value, placeholder = messages.noValue) {
  if (!value) { return renderEmptyValue(placeholder); }
  return value;
}

export function getOutputLink(value, placeholder, link = '') {
  if (!value) { return renderEmptyValue(placeholder); }
  return (
    <A href={link}>{value}</A>
  );
}
