import { theme } from 'decisiv-ui-utils';

/**
 * Applies the theme to styled component as a defaultProp in test env only.
 * @param  {Component} StyledComponent A styled component
 * @return {Component}                 The styled component with the expected defaultProps
 */
/* eslint-disable import/prefer-default-export */
export const withTestTheme = (StyledComponent) => {
  if (process.env.NODE_ENV === 'test') {
    // eslint-disable-next-line no-param-reassign
    StyledComponent.defaultProps = {
      ...StyledComponent.defaultProps,
      theme,
    };
  }

  return StyledComponent;
};
