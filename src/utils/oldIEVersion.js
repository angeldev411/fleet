// returns true if the browser is IE 11 or older (pre-Edge)
// otherwise, it returns false

export default function oldIEVersion() {
  const ua = navigator.userAgent;
  const ie11 = ua.indexOf('Trident/7.0') !== -1;
  const ie10 = ua.indexOf('Trident/6.0') !== -1;
  const reallyOldIE = ua.indexOf('Trident/5.0') !== -1;
  return ie11 || ie10 || reallyOldIE;
}
